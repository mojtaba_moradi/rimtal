const APP_NAME = "ریمتال";
const EXPLORE = "کاوش کردن";
const TRACKS = "آهنگ ها";
const PLAYLISTS = "لیست های پخش";
const ALBUMS = "آلبوم ها";
const ARTISTS = "هنرمندان";
const VIDEOS = "ویدیو ها";
const SIGN_IN = "ورود";

const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN,
};
export default navbar;
