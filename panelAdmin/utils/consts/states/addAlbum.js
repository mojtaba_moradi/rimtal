const addAlbum = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی",
      },
      value: "",
      validation: {
        required: true,
        isFa: true,
      },
      valid: false,
      touched: false,
    },
    titleEn: {
      label: "عنوان انگلیسی :",

      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی",
      },
      value: "",
      validation: {
        required: true,
        isEn: true,
      },
      valid: false,
      touched: false,
    },
    artist: {
      label: "هنرمند :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "هنرمند",
      },
      value: "",
      validation: {
        minLength: 1,
        required: true,
      },
      valid: false,
      touched: false,
    },
    genres: {
      label: "ژانر :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "ژانر",
      },
      value: "",
      validation: {
        minLength: 1,
        required: true,
      },
      valid: false,
      touched: false,
    },

    songs: {
      label: "آهنگ ها :",
      elementType: "InputFileArray",
      kindOf: "voice",
      value: [],
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    image: {
      label: " عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addAlbum;
