import React from "react";
import formatMoney from "../../formatMoney";

const ShowDiscount = (data) => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "دسته بندی  ", "قیمت اصلی", "قیمت جدید", " تخفیف", "فروش", "بازدید ", "وضعیت", "ویرایش"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let active = <i style={{ fontSize: "1em", color: "green", fontWeight: "900" }} className="far fa-check-circle"></i>;
    let deActive = <i style={{ fontSize: "1em", color: "red", fontWeight: "900" }} className="fas fa-ban"></i>;

    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let realPrice = data[index].realPrice ? formatMoney(data[index].realPrice) : NotEntered;
    let newPrice = data[index].newPrice ? formatMoney(data[index].newPrice) : NotEntered;
    let rating = data[index].rating ? data[index].rating : 0;
    let percent = data[index].percent ? "%" + data[index].percent : NotEntered;
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : NotEntered;
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";

    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, { option: { star: true, value: rating } }, categoryTitleFa, realPrice, newPrice, percent, boughtCount, viewCount, isActive, { option: { edit: true } }],
      style: {},
    });
  }
  return [thead, tbody];
};

export default ShowDiscount;
