import Cookie from "js-cookie";

const checkAuth = () => {
  return Cookie.get("DrToken") !== undefined;
  // return true;
};

export default checkAuth;
