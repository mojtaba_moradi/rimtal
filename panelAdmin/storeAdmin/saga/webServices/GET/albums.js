import { put } from "redux-saga/effects";
import actions from "../../../actions";
import panelAdmin from "../../../../index";

export function* albumsData({ page }) {
  yield put(actions.reduxActions.startGetAlbum());

  try {
    const res = yield panelAdmin.api.get.albums({ page });
    console.log({ resalbumData: res });
    yield put(actions.reduxActions.setAlbumData(res.data));
  } catch (err) {
    console.log({ erralbumData: err });
    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}

export function* albumSearchData({ page, title }) {
  yield put(actions.reduxActions.startSearchAlbum());

  //   try {
  //     const res = yield panelAdmin.api.get.albumSearch({ page, title });
  //     console.log({ resAlbumSearchData: res });

  //     yield put(actions.reduxActions.setSearchAlbumData(res.data));
  //   } catch (err) {
  //     console.log({ errAlbumSearchData: err });

  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
