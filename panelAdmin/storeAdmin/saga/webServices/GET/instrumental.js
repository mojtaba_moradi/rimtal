import { put } from "redux-saga/effects";
import actions from "../../../actions";
import panelAdmin from "../../../../index";

export function* instrumentData({ page }) {
  yield put(actions.reduxActions.startGetInstrument());

  console.log({ mojtaba3: page });
  try {
    const res = yield panelAdmin.api.get.instrument({ page });
    console.log({ resArtistData: res });
    yield put(actions.reduxActions.setArtistData(res.data));
  } catch (err) {
    console.log({ errArtistData: err });
    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}

export function* instrumentSearchData({ page, title }) {
  yield put(actions.reduxActions.startSearchInstrument());

  //   try {
  //     const res = yield panelAdmin.api.get.artistSearch({ page, title });
  //     console.log({ resArtistSearchData: res });

  //     yield put(actions.reduxActions.setSearchArtistData(res.data));
  //   } catch (err) {
  //     console.log({ errArtistSearchData: err });

  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
