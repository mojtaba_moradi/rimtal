import atSaga from "../../actionTypes/saga";

export function uploadImageData(data) {
  return { type: atSaga.POST_UPLOAD_IMAGE, data: data };
}
export function getGalleryData(data) {
  return { type: atSaga.GET_GALLERY_DATA, data: data };
}
// ========================================================= ARTIST

export function getArtistData({ page }) {
  console.log({ mojtaba1: page });
  return { type: atSaga.GET_ARTIST_DATA, page };
}
export function getSearchArtistData({ title, page }) {
  return { type: atSaga.GET_SEARCH_ARTIST_DATA, page, title };
}
// ========================================================= INSTRUMENT

export function getInstrumentData({ page }) {
  return { type: atSaga.GET_INSTRUMENT_DATA, page };
}
export function getSearchInstrumentData({ title, page }) {
  return { type: atSaga.GET_SEARCH_INSTRUMENT_DATA, page, title };
}
// ========================================================= COUNTRY
export function getCountryData({ page }) {
  return { type: atSaga.GET_COUNTRY_DATA, page };
}
export function getSearchCountryData({ title, page }) {
  return { type: atSaga.GET_SEARCH_COUNTRY_DATA, page, title };
}
// ========================================================= MOOD
export function getMoodData({ page }) {
  return { type: atSaga.GET_MOOD_DATA, page };
}
export function getSearchMoodData({ title, page }) {
  return { type: atSaga.GET_SEARCH_MOOD_DATA, page, title };
}
// ========================================================= ALBUM
export function getAlbumData({ page }) {
  return { type: atSaga.GET_ALBUM_DATA, page };
}
export function getSearchAlbumData({ title, page }) {
  return { type: atSaga.GET_SEARCH_ALBUM_DATA, page, title };
}
// ========================================================= HASHTAG
export function getHashtagData({ page }) {
  return { type: atSaga.GET_HASHTAG_DATA, page };
}
export function getSearchHashtagData({ title, page }) {
  return { type: atSaga.GET_SEARCH_HASHTAG_DATA, page, title };
}

// ========================================================= GENRE
export function getGenreData({ page }) {
  return { type: atSaga.GET_GENRE_DATA, page };
}
export function getSearchGenreData({ title, page }) {
  return { type: atSaga.GET_SEARCH_GENRE_DATA, page, title };
}