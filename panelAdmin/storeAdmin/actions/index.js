import * as reduxActions from "./redux";
import * as sagaActions from "./saga";

const actions = {
  reduxActions,
  sagaActions,
};

export default actions;
