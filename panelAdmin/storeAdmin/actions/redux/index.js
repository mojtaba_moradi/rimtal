import atRedux from "../../actionTypes/redux";
// =================================================== NAVBAR
export function setPageName(data) {
  return { type: atRedux.SET_PAGE_NAME, data };
}
// =================================================== UPLOAD
export function setUploadImage(data) {
  return { type: atRedux.SET_UPLOAD_IMAGE, data };
}
// =================================================== GALLERY
export function setGalleryData(data) {
  return { type: atRedux.SET_GALLERY_DATA, data };
}
export function changeAddGalleryData(data) {
  return { type: atRedux.CHANGE_ADD_GALLERY_DATA, data };
}
// =================================================== ARTIST
export function setArtistData(data) {
  return { type: atRedux.SET_ARTIST_DATA, data };
}
export function setSearchArtistData(data) {
  return { type: atRedux.SET_SEARCH_ARTIST_DATA, data };
}
export function startGetArtist(data) {
  return { type: atRedux.START_ARTIST_DATA, data };
}
export function startSearchArtist(data) {
  return { type: atRedux.START_SEARCH_ARTIST_DATA, data };
}
// =================================================== INSTRUMENT
export function setInstrumentData(data) {
  return { type: atRedux.SET_INSTRUMENT_DATA, data };
}
export function setSearchInstrumentData(data) {
  return { type: atRedux.SET_SEARCH_INSTRUMENT_DATA, data };
}
export function startGetInstrument(data) {
  return { type: atRedux.START_INSTRUMENT_DATA, data };
}
export function startSearchInstrument(data) {
  return { type: atRedux.START_SEARCH_INSTRUMENT_DATA, data };
}
// =================================================== COUNTRY
export function setCountryData(data) {
  return { type: atRedux.SET_COUNTRY_DATA, data };
}
export function setSearchCountryData(data) {
  return { type: atRedux.SET_SEARCH_COUNTRY_DATA, data };
}
export function startGetCountry(data) {
  return { type: atRedux.START_COUNTRY_DATA, data };
}
export function startSearchCountry(data) {
  return { type: atRedux.START_SEARCH_COUNTRY_DATA, data };
}
// =================================================== MOOD
export function setMoodData(data) {
  return { type: atRedux.SET_MOOD_DATA, data };
}
export function setSearchMoodData(data) {
  return { type: atRedux.SET_SEARCH_MOOD_DATA, data };
}
export function startGetMood(data) {
  return { type: atRedux.START_MOOD_DATA, data };
}
export function startSearchMood(data) {
  return { type: atRedux.START_SEARCH_MOOD_DATA, data };
}
// =================================================== HASHTAG
export function setHashtagData(data) {
  return { type: atRedux.SET_HASHTAG_DATA, data };
}
export function setSearchHashtagData(data) {
  return { type: atRedux.SET_SEARCH_HASHTAG_DATA, data };
}
export function startGetHashtag(data) {
  return { type: atRedux.START_HASHTAG_DATA, data };
}
export function startSearchHashtag(data) {
  return { type: atRedux.START_SEARCH_HASHTAG_DATA, data };
}
// =================================================== GENRE
export function setGenreData(data) {
  return { type: atRedux.SET_GENRE_DATA, data };
}
export function setSearchGenreData(data) {
  return { type: atRedux.SET_SEARCH_GENRE_DATA, data };
}
export function startGetGenre(data) {
  return { type: atRedux.START_GENRE_DATA, data };
}
export function startSearchGenre(data) {
  return { type: atRedux.START_SEARCH_GENRE_DATA, data };
}
