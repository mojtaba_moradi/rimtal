import panelAdmin from "../..";

export const instrumentInitialState = {
  instrumentData: null,
  searchInstrumentData: null,
  loading: false,
  searchLoading: false,
};

function instrumentReducer(state = instrumentInitialState, action) {
  const atRedux = panelAdmin.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_INSTRUMENT_DATA:
      return { ...state, ...{ instrumentData: action.data, loading: false, searchLoading: false } };
    case atRedux.SET_SEARCH_INSTRUMENT_DATA:
      return { ...state, ...{ searchInstrumentData: action.data, searchLoading: false } };
    case atRedux.START_INSTRUMENT_DATA:
      return { ...state, ...{ loading: true } };
    case atRedux.START_SEARCH_INSTRUMENT_DATA:
      return { ...state, ...{ searchLoading: true } };
    default:
      return state;
  }
}

export default instrumentReducer;
