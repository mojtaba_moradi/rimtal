// import galleryReducer from "./galleryReducer";
import uploadReducer from "./uploadReducer";
import galleryReducer from "./galleryReducer";
import artistReducer from "./artistReducer";
import panelNavbarReducer from "./panelNavbarReducer";
import instrumentReducer from "./instrumentReducer";
import countryReducer from "./countryReducer";
import moodReducer from "./moodReducer";
import hashtagReducer from "./hashtagReducer";
import genreReducer from "./genreReducer";
import albumReducer from "./albumReducer";
const reducer = {
  upload: uploadReducer,
  gallery: galleryReducer,
  artist: artistReducer,
  panelNavbar: panelNavbarReducer,
  instrument: instrumentReducer,
  country: countryReducer,
  mood: moodReducer,
  hashtag: hashtagReducer,
  genre: genreReducer,
  album: albumReducer,
};

export default reducer;
