import panelAdmin from "../..";

export const genreInitialState = {
  genreData: null,
  searchGenreData: null,
  loading: false,
  searchLoading: false,
};

function genreReducer(state = genreInitialState, action) {
  const atRedux = panelAdmin.actionTypes.atRedux;
  console.log({ action });

  switch (action.type) {
    case atRedux.SET_GENRE_DATA:
      return { ...state, ...{ genreData: action.data, loading: false, searchLoading: false } };
    case atRedux.SET_SEARCH_GENRE_DATA:
      return { ...state, ...{ searchGenreData: action.data, searchLoading: false } };
    case atRedux.START_GENRE_DATA:
      return { ...state, ...{ loading: true } };
    case atRedux.START_SEARCH_GENRE_DATA:
      return { ...state, ...{ searchLoading: true } };
    default:
      return state;
  }
}

export default genreReducer;
