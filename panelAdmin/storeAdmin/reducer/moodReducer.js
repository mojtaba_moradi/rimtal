import panelAdmin from "../..";

export const moodInitialState = {
  moodData: null,
  searchMoodData: null,
  loading: false,
  searchLoading: false,
};

function moodReducer(state = moodInitialState, action) {
  const atRedux = panelAdmin.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_MOOD_DATA:
      return { ...state, ...{ moodData: action.data, loading: false, searchLoading: false } };
    case atRedux.SET_SEARCH_MOOD_DATA:
      return { ...state, ...{ searchMoodData: action.data, searchLoading: false } };
    case atRedux.START_MOOD_DATA:
      return { ...state, ...{ loading: true } };
    case atRedux.START_SEARCH_MOOD_DATA:
      return { ...state, ...{ searchLoading: true } };
    default:
      return state;
  }
}

export default moodReducer;
