import panelAdmin from "../..";

export const albumInitialState = {
  albumData: null,
  searchAlbumData: null,
  loading: false,
  searchLoading: false,
};

function albumReducer(state = albumInitialState, action) {
  const atRedux = panelAdmin.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_ALBUM_DATA:
      return { ...state, ...{ albumData: action.data, loading: false, searchLoading: false } };
    case atRedux.SET_SEARCH_ALBUM_DATA:
      return { ...state, ...{ searchAlbumData: action.data, searchLoading: false } };
    case atRedux.START_ALBUM_DATA:
      console.log("START_ALBUM_DATA");

      return { ...state, ...{ loading: true } };
    case atRedux.START_SEARCH_ALBUM_DATA:
      console.log("START_SEARCH_ALBUM_DATA");

      return { ...state, ...{ searchLoading: true } };
    default:
      return state;
  }
}

export default albumReducer;
