// import atRedux from "../../../store/actionTypes/redux";
import panelAdmin from "../..";
export const artistInitialState = {
  pageName: null,
};

function panelNavbarReducer(state = artistInitialState, action) {
  const atRedux = panelAdmin.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_PAGE_NAME:
      return { ...state, ...{ pageName: action.data } };
    default:
      return state;
  }
}

export default panelNavbarReducer;
