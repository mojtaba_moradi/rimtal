import React, { useRef, useEffect, useState, Fragment } from "react";
import { post } from "../../../api";
import FormInputCountry from "./FormInputMood";
import panelAdmin from "../../..";
const AddHashtag = () => {
  const states = panelAdmin.utils.consts.states;
  const onChanges = panelAdmin.utils.onChanges;
  const [data, setData] = useState({ ...states.addHashtag });
  const [state, setState] = useState({
    progressPercentImage: null,
    progressPercentSongs: null,
    remove: { value: "", name: "" },
  });
  const [Loading, setLoading] = useState(false);

  const [ModalInpts, setModalInpts] = useState({
    show: false,
    kindOf: false,
    data: { src: false, type: false, name: false },
    name: null,
  });
  const [checkSubmited, setCheckSubmited] = useState(false);

  // ============================= submited
  const _onSubmited = async (e) => {
    setCheckSubmited(!checkSubmited);
    e.preventDefault();
    const formData = {};
    for (let formElementIdentifier in data.Form)
      formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    if (await post.hashtag(formData)) setData({ ...states.addHashtag });
  };
  // ========================= End submited =================

  const inputChangedHandler = async (event) =>
    await onChanges.globalChange({ event, data, setData, setState, setLoading, imageType: "flag" });

  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = (
    <FormInputCountry
      _onSubmited={_onSubmited}
      stateArray={stateArray}
      data={data}
      state={state}
      setData={setData}
      Loading={Loading}
      setLoading={setLoading}
      inputChangedHandler={inputChangedHandler}
      checkSubmited={checkSubmited}
    />
  );
  return (
    <div className="countainer-main centerAll ">
      <div className="form-countainer">
        <div className="form-subtitle">{"افزودن هشتگ جدید"}</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid} onClick={_onSubmited}>
              افزودن{" "}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddHashtag;
