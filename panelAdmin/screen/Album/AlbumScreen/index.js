import React, { useState } from "react";
import { useSelector } from "react-redux";
import SpinnerRotate from "../../../component/UI/Loadings/SpinnerRotate";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";
import panelAdmin from "../../..";
const AlbumScreen = (props) => {
  const { onDataChange, onDataSearch, data } = props;
  const [state, setState] = useState({
    remove: { index: "", name: "" },
    AlbumTitle: "",
  });
  const [editData, setEditData] = useState(false);
  const [InitialState, setInitialState] = useState(false);
  const store = useSelector((state) => {
    return state.album;
  });
  const card = panelAdmin.utils.consts.card;
  const loading = store.loading;
  const searchLoading = store.searchLoading;
  const AlbumData = store.albumData;
  const searchAlbumData = store.searchAlbumData;
  const searchAlbum = searchAlbumData ? (searchAlbumData.docs.length ? true : false) : false;
  const showDataElement = AlbumData && AlbumData.docs.length && <ShowCardInformation data={card.album(AlbumData.docs)} onClick={null} optionClick={null} />;
  return (
    <React.Fragment>
      {showDataElement}
      {/* <CountryElement Country={CountrySearch ? CountrySearch : Country} handelPage={_handelPage} tableOnclick={_tableOnclick} handelchange={handelchange} /> */}
      {loading && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )}
    </React.Fragment>
  );
};

export default AlbumScreen;
