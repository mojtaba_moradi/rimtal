import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const ownersSearch = async (param, returnData, loading) => {
  return axios
    .get(Strings.ApiString.OWNER_SEARCH + "/" + param)
    .then(ownersSearch => {
      console.log({ ownersSearch });
      returnData(ownersSearch.data);
      // loading(false);
    })
    .catch(error => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default ownersSearch;
