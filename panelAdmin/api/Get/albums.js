import axios from "../axios-orders";
import panelAdmin from "../..";

const albums = async ({ page }) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;
  return axios.get(strings.ALBUM + "/" + page);
};

export default albums;
