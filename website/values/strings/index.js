import en from "./en";
import fa from "./fa";
const Lang = "ltr";
let string;
if (Lang === "rtl") string = fa;
else string = en;

export default string;
