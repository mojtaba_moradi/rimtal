import React, { useState } from "react";
import AwesomeScroll from "../../../../components/AwesomeScroll";
import themeColor from "../../../values/theme/themeColor";
import Link from "next/link";
import placeholder from "../../../../public/assets/images/placeholder/singleplaceholder.png";
import website from "../../..";
import MusicCardDouble from "../../cards/cardElements/MusicCardDouble";

const SingleContainer = (props) => {
  const [state, setState] = useState({ dotModal: false });
  const { singles } = props;
  // console.log({ singles });

  const newData = website.utils.convert.singleCard(singles);
  // console.log({ SingleContainer: newData });
  const onShowNodal = () => {};
  return (
    <section className="row-container row-main-wrapper">
      <div className="top-card-head">
        {" "}
        <div className="cardHead-headline">
          <h3>{website.values.strings.SINGLES}</h3>
        </div>
        <div className="cardHead-change-Location ">
          <Link href="#" as="#">
            <a className="pointer">
              <span style={{ color: themeColor.mainColor }}>{website.values.strings.VIEW_ALL}</span>
            </a>
          </Link>
        </div>
      </div>
      <AwesomeScroll scrollBar={true}>
        <ul onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper row mx-0">
          {newData.map((data, index) => {
            return <MusicCardDouble key={"single-" + index} data={data} click={onShowNodal} state={state} placeholder={placeholder} />;
          })}
        </ul>
      </AwesomeScroll>
    </section>
  );
};

export default SingleContainer;
