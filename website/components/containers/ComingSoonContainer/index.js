import React, { useState } from "react";

import AwesomeScroll from "../../../../components/AwesomeScroll";

import Link from "next/link";
import themeColor from "../../../values/theme/themeColor";
import placeholder from "../../../../public/assets/images/placeholder/playlistplaceholder.png";
import website from "../../..";
import MusicCard from "../../cards/cardElements/MusicCard";

const ComingSoonContainer = (props) => {
  const { comingSoon } = props;

  // console.log({ comingSoonContainer:comingSoon });
  const newData = website.utils.convert.comingSoonCard(comingSoon);
  // console.log({ newDataContainer: newData });

  return (
    <section className="row-container row-main-wrapper">
      <div className="top-card-head">
        <div className="cardHead-headline">
          <h3>{website.values.strings.COMMING_SOON}</h3>
        </div>
        <div className="cardHead-change-Location">
          <Link href="#" as="#">
            <a className="pointer">
              <span style={{ color: themeColor.mainColor }}>{website.values.strings.VIEW_ALL}</span>
            </a>
          </Link>
        </div>
      </div>
      <AwesomeScroll scrollBar={true}>
        <ul onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper row mx-0">
          {newData.map((data, index) => {
            return <MusicCard key={"MusicCard-" + index} data={data} placeholder={placeholder} parentClass={" col-lg-2 col-md-3 col-5  px-0"} />;
          })}
        </ul>
      </AwesomeScroll>
    </section>
  );
};

export default ComingSoonContainer;
