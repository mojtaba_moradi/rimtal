import React from "react";
import AwesomeScroll from "../../../../components/AwesomeScroll";

import themeColor from "../../../values/theme/themeColor";
import Link from "next/link";
import website from "../../..";
import MoodCard from "../../cards/cardElements/MoodCard";

const MoodCardContainer = (props) => {
  const { moods } = props;
  const newData = website.utils.convert.moodCard(moods);
  // console.log({ newData });

  return (
    <section className="row-container row-main-wrapper">
      <div className="top-card-head">
        {" "}
        <div className="cardHead-headline">
          <h3>{website.values.strings.MOOD}</h3>
        </div>
        <div className="cardHead-change-Location ">
          <Link href="#" as="#">
            <a className="pointer">
              <span style={{ color: themeColor.mainColor }}>{website.values.strings.VIEW_ALL}</span>
            </a>
          </Link>
        </div>
      </div>

      <AwesomeScroll scrollBar={true}>
        <ul onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper row mx-0">
          {newData.map((data, index) => {
            return <MoodCard key={"MoodCard-" + index} data={data} />;
          })}
        </ul>
      </AwesomeScroll>
    </section>
  );
};

export default MoodCardContainer;
