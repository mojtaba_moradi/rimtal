import React, { useRef } from "react";
// import "./index.scss";

import PlayIcon from "../../../Ui/Icons/PlayIcon";
import HeartIcon from "../../../Ui/Icons/HeartIcon";
import DotIcon from "../../../Ui/Icons/DotIcon";
import Link from "next/link";
import LazyImage from "../../../../../components/LazyImage";

const MusicCard = (props) => {
  // console.log({ MusicCard: props });
  const { dotIconClick, dotModalInfo, data, placeholder, parentClass } = props;
  const { titleTop, titleMiddle, titleBottom, images } = data;

  const optionRef = useRef(null);
  const liRef = useRef(null);
  const imageOnload = () => {
    optionRef.current.style.display = "flex";
  };

  const preventDragHandler = (e) => e.preventDefault();

  return (
    <li ref={liRef} onDragStart={preventDragHandler} className={`${parentClass ? parentClass : "card-row  col-lg-2 col-md-3 col-5  px-0"}`}>
      <div className="play-Card-container  play-Card-wrapper">
        <div className="imageCard-top">
          <picture>
            <source media="(max-width: 375px)" srcSet={images.phone} />

            {/* <img id="myImage" className="noSelect noEvent" src={images.web} alt={titleTop} /> */}
            <LazyImage imageOnload={imageOnload} src={images.web} defaultImage={placeholder} alt={titleTop} />
          </picture>
          <div ref={optionRef} className="card-options">
            {/* <Link {...location}> */}
            <a>
              <PlayIcon
                // className={classNameNew}
                style={{ fontSize: "0.6em" }}
              />
            </a>
            {/* </Link> */}

            <HeartIcon style={{ fontSize: "0.6em" }} />
            <DotIcon click={dotIconClick} />
          </div>
        </div>
        <div className="descriptionCard-bottom noSelect">
          <h4 className="play-Card-title">{titleTop}</h4>
          <span
            className="play-Card-subTitle"
            // style={colorStyle}
          >
            {titleMiddle}
          </span>
          <h3
            className="play-Card-text"
            //  style={colorStyle}
          >
            {titleBottom[0]}
            <i className="fas fa-circle" />
            {titleBottom[1]}
          </h3>
        </div>
      </div>
    </li>
  );
};

export default MusicCard;
