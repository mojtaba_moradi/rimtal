import React from "react";
// import "./index.scss";
// import Link from "next/link";
const MoreExploreCard = (props) => {
  const { title, directionWeb, themeColor, href } = props;
  let styleColor = {
    color: themeColor.accentColor,
  };
  let style = {
    marginRight: "1.5em",
  };
  if (directionWeb === "rtl") {
    style = { marginLeft: "1.5em" };
  }
  let iconClassName =
    directionWeb === "ltr" ? "fas fa-chevron-right" : "fas fa-chevron-left";
  const preventDragHandler = (e) => e.preventDefault();

  return (
    <li
      onDragStart={preventDragHandler}
      style={styleColor}
      className="box-border-Card-wrapper  "
    >
      <a
        style={style}
        className="box-border-Card-container alignCenter borderSolid alignCenter spaceBetween"
      >
        <div className="box-border-Card-title">{title}</div>
        <div className="box-border-Card-icon">
          <i
            style={{ color: themeColor.mainColor }}
            className={iconClassName}
          />{" "}
        </div>
      </a>
    </li>
  );
};

export default MoreExploreCard;
