import React from "react";
import Header from "../components/Header/index";

const WebsiteScreen = (props) => {
  return (
    <div>
      <Header />
      {props.children}
    </div>
  );
};

export default WebsiteScreen;
