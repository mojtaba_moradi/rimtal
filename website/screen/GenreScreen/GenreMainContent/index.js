import React from "react";
import NewRelease from "./NewRelease";
import TopTracksConitainer from "./TopTracks/TopTracksContainer";
import EssentialAlbum from "./EssentialAlbum";
import ArtistGenreContainer from "./ArtistsGenre/ArtistGenreContainer";

const GenreMainContainer = () => {
  return (
    <div className="genre-main-container">
      <NewRelease />
      <TopTracksConitainer />
      <EssentialAlbum />
      <ArtistGenreContainer />
    </div>
  );
};

export default GenreMainContainer;
