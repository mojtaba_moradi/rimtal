import React from "react";
import ArtistGenreCard from "../ArtistGenreCard";
import AwesomeScroll from "../../../../../../components/AwesomeScroll";

const ArtistGenreContainer = () => {
  const artists = [
    {
      image: "https://picsum.photos/200",
      artistName: "pink floyd",
      fans: "90k",
    },
    { image: "https://picsum.photos/200", artistName: "archive", fans: "4k" },
    {
      image: "https://picsum.photos/200",
      artistName: "linkin park",
      fans: "75k",
    },
    {
      image: "https://picsum.photos/200",
      artistName: "radio head",
      fans: "80k",
    },
    {
      image: "https://picsum.photos/200",
      artistName: "london grammer",
      fans: "14k",
    },
    {
      image: "https://picsum.photos/200",
      artistName: "broken bells",
      fans: "7k",
    },
    {
      image: "https://picsum.photos/200",
      artistName: "nick cave",
      fans: "65k",
    },
    { image: "https://picsum.photos/200", artistName: "soen", fans: "17k" },
    { image: "https://picsum.photos/200", artistName: "shamrain", fans: "3k" },
    { image: "https://picsum.photos/200", artistName: "u2", fans: "50k" },
    { image: "https://picsum.photos/200", artistName: "gorillaz", fans: "23k" },
    { image: "https://picsum.photos/200", artistName: "muse", fans: "20k" },
    {
      image: "https://picsum.photos/200",
      artistName: "lunatic soul",
      fans: "8k",
    },
    {
      image: "https://picsum.photos/200",
      artistName: "steven wilson",
      fans: "10k",
    },
  ];
  const renderArtists = () => (
    <div className="artist-genre-container">
      <h1>artist</h1>
      <AwesomeScroll scrollBar={true}>
        <div className="row artist-wrapper">
          {artists.map((artist) => {
            return <ArtistGenreCard artist={artist} />;
          })}
        </div>
      </AwesomeScroll>
    </div>
  );

  return renderArtists();
};

export default ArtistGenreContainer;

{
  /* <div className="artist-genre-container">
  <h1>artist</h1>
  <div className="row artist-wrapper">
    <ArtistGenreCard artist={artist} />
  </div>
</div>; */
}
