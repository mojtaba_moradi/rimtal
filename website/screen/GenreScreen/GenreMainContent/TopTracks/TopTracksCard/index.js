import React, { Fragment } from "react";
import TopTracksConitainer from "../TopTracksContainer";

const TopTracksCard = ({ data }) => {
  return (
    <Fragment>
      <div className="col-lg-12 tracks">
        <div className="left-side">
          <ul>
            <li>
              <img style={{ width: "unset" }} src={data.image} alt="blaw" />
              <span className="overlay">
                <i class="fas fa-play"></i>
              </span>
            </li>
            <li>
              <figure>
                <figcaption>
                  <p>{data.titleTop}</p>
                  <div className="spans">
                    <span>{data.titleBottom}</span>
                  </div>
                </figcaption>
              </figure>
            </li>
          </ul>
        </div>
        <div className="right-side">
          <ul>
            <li>
              <i class="far fa-heart">
                <span class="tooltiptext">Favorite Track</span>
              </i>
            </li>
            <li>
              <i class="far fa-plus">
                <span class="tooltiptext">add to playlist</span>
              </i>
            </li>
            <li>
              <i class="far fa-ellipsis-h"></i>
            </li>
          </ul>
        </div>
      </div>
    </Fragment>
  );
};

export default TopTracksCard;
