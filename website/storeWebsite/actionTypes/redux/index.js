const atRedux = {
  //======================================================== Redux

  SET_HOME_DATA: "SET_HOME_DATA_REDUX",
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX",
  SET_FILTER_TYPE: "SET_FILTER_TYPE_REDUX",
  REMOVE_FILTER_TYPE: "REMOVE_FILTER_TYPE_REDUX",
};

export default atRedux;
