import atRedux from "../actionTypes/redux";

export const filterInisialState = {
  filterData: [],
};

export const setFilter = (state, action) => {
  if (state.filterData.length > 0) {
    const filter = state.filterData.filter((item) => {
      if (item.type !== action.data.type) return item;
    });
    return { ...state, filterData: [...filter, action.data] };
  } else {
    return { ...state, filterData: [...state.filterData, action.data] };
  }
};
export const removeFilter = (state, action) => {
  return { ...state, filterData: [] };
};
function filterReducer(state = filterInisialState, action) {
  switch (action.type) {
    case atRedux.SET_FILTER_TYPE:
      return setFilter(state, action);
    case atRedux.REMOVE_FILTER_TYPE:
      return removeFilter();
    default:
      return state;
  }
}

export default filterReducer;
