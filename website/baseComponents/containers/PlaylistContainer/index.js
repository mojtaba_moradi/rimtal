import React, { useState } from "react";
import string from "../../values/strings";
import convertData from "../../../utils/convert";
import Link from "next/link";
import themeColor from "../../values/theme/themeColor";
import placeholder from "../../../../public/assets/images/placeholder/playlistplaceholder.png";
import MusicCard from "../../cards/MusicCard";
import AwesomeScroll from "../../../../components/AwesomeScroll";

const PlaylistContainer = (props) => {
  const { playlists } = props;

  // console.log({ singles });
  const newData = website.convert.playlistCard(playlists);

  return (
    <section className="row-container row-main-wrapper">
      <div className="top-card-head">
        {" "}
        <div className="cardHead-headline">
          <h3>{string.PLAY_LISTS}</h3>
        </div>
        <div className="cardHead-change-Location ">
          <Link href="#" as="#">
            <a className="pointer">
              <span style={{ color: themeColor.mainColor }}>{string.VIEW_ALL}</span>
            </a>
          </Link>
        </div>
      </div>

      <AwesomeScroll scrollBar={true}>
        <ul onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper row mx-0">
          {newData.map((data, index) => {
            return <MusicCard key={"PlaylistCard-" + index} data={data} placeholder={placeholder} />;
          })}
        </ul>
      </AwesomeScroll>
    </section>
  );
};

export default PlaylistContainer;
