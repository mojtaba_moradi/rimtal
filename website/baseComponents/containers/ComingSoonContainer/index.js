import React, { useState } from "react";
import string from "../../values/strings";
import MusicCard from "../../cards/MusicCard";
import Link from "next/link";
import themeColor from "../../values/theme/themeColor";
import placeholder from "../../../../public/assets/images/placeholder/playlistplaceholder.png";
import AwesomeScroll from "../../../../components/AwesomeScroll";

const ComingSoonContainer = (props) => {
  const { comingSoon } = props;

  // console.log({ comingSoonContainer:comingSoon });
  const newData = website.convert.comingSoonCard(comingSoon);
  // console.log({ newDataContainer: newData });

  return (
    <section className="row-container row-main-wrapper">
      <div className="top-card-head">
        {" "}
        <div className="cardHead-headline">
          <h3>{string.COMMING_SOON}</h3>
        </div>
        <div className="cardHead-change-Location">
          <Link href="#" as="#">
            <a className="pointer">
              <span style={{ color: themeColor.mainColor }}>{string.VIEW_ALL}</span>
            </a>
          </Link>
        </div>
      </div>
      <AwesomeScroll scrollBar={true}>
        <ul onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper row mx-0">
          {newData.map((data, index) => {
            return <MusicCard key={"MusicCard-" + index} data={data} placeholder={placeholder} />;
          })}
        </ul>
      </AwesomeScroll>
    </section>
  );
};

export default ComingSoonContainer;
