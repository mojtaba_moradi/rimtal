import React, { Fragment } from "react";
import Link from "next/link";
// import "./SliderCardStructure.scss";
import AwesomeScroll from "../AwesomeScroll";
const SliderCardStructure = (props) => {
  console.log({ props });

  const { data, Row, themeColor } = props;

  // console.log({ SliderCardStructure: data });

  return (
    <Fragment>
      {props.headerDetails ? (
        <div className="top-card-head">
          {" "}
          <div className="cardHead-headline">
            <h3>{props.headerDetails.headline}</h3>
          </div>
          <div className="cardHead-change-Location pointer">
            <Link {...props.headerDetails.locationDetails.location}>
              <a>
                <span style={{ color: themeColor.mainColor }}>
                  {props.headerDetails.locationDetails.title}
                </span>
              </a>
            </Link>
          </div>
        </div>
      ) : (
        ""
      )}
      <AwesomeScroll data={data} Row={Row} />
    </Fragment>
  );
};

export default SliderCardStructure;
