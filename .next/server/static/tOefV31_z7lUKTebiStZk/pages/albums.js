module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "/jkW":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
}); // Identify /[param]/ in route string

const TEST_ROUTE = /\/\[[^/]+?\](?=\/|$)/;

function isDynamicRoute(route) {
  return TEST_ROUTE.test(route);
}

exports.isDynamicRoute = isDynamicRoute;

/***/ }),

/***/ "0Bsm":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__("TqRt");

exports.__esModule = true;
exports.default = withRouter;

var _react = _interopRequireDefault(__webpack_require__("cDcd"));

var _router = __webpack_require__("nOHt");

function withRouter(ComposedComponent) {
  function WithRouterWrapper(props) {
    return _react.default.createElement(ComposedComponent, Object.assign({
      router: (0, _router.useRouter)()
    }, props));
  }

  WithRouterWrapper.getInitialProps = ComposedComponent.getInitialProps // This is needed to allow checking for custom getInitialProps in _app
  ;
  WithRouterWrapper.origGetInitialProps = ComposedComponent.origGetInitialProps;

  if (false) { var name; }

  return WithRouterWrapper;
}

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("jl0i");


/***/ }),

/***/ "284h":
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__("cDf5");

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ "HJQg":
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ }),

/***/ "Lhaj":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAYAAAA9zQYyAAAId0lEQVR4nO3dW3ITRxjF8bYxBsxVLmB1yRbylrxlA8kW8sbiqOJiMDdzJ3WEuhBo+puRNSOpT/9/VS6qDInHmjPf9HT3dB88fvz4OKX0b0rpt5TSaQLq8yKl9Cil9OdRSumflNIfnERUbLbI8IUq9HMqM0ycHRJmGDk95GzCCYGGFQINKwQaVgg0rBBoWCHQsEKgYYVAwwqBhhUCDSsEGlYINKwQaFgh0LBCoGGFQMMKgYYVAg0rBBpWCDSsEGhYIdCwQqBhhUDDCoGGFQINKwQaVgg0rBBoWCHQsEKgYYVAwwqBhhUCDSsEGlYINKwQaFgh0LBCoGGFQMPKUY2/zOfPn9OXL19Wvo9+165ds/6Uqgr0p0+f0vn5efrw4cPK32GYw8PDdOvWrXT79u2Vv3NQTaDfvn2bXr58ufJ9rOfr16/p1atX6eLiIt2/f38ecCdV/DZqYugkYDy6271588buE60i0PrgVVkwrtevX8+LhZMqAq3bI/hsh6gi0FRnDFVlt12mhxr3bqixPHv2rIneIQZWYIVAwwqBhhUCDSsEGlaq7uXYJfUYfPz4MX379m3+Z4mGlq9evTr/Uz0yR0d85FPi0x1II2oahFCQ1+3+Wh68UKAVbIX85s2bK/8WmyHQAQ3oKIyaGKW5D2PQhZGHmzU/5caNG/NgK+DYHIHuoCArxFPPIck/R1+q2prSyUDRZgj0L1SRNU1128PtuSmjan3nzh27aZ3bQqAX1AxQkHc9PKxqrYvKeRL+lAj0CFVZD3pXrlxZ+X5aVN515Un4arffu3ePar2G5gOtOcHrvjygdq6+jo+PB7V5Vf0VbAVUfw6dg6wLTf92Npvx0DhQ04FWVdYtfghV4ZOTk3mvxLp9yfr3y/9N7v4b8rN1EWimnGYWEup+zQZ6aJgVRLVnx+wz1kWhL/1/1ZPSdxxqghDqYZpsnA0Ns3obHjx4MNkAiC4WtZGHBDWHeqz+cFfNBVpt5r4wK2gPHz6c9zJs44FM7fD88yIK9dnZGW/wBJoKtNqtfQ+AqpSqyru4teuOoIod0UOiQo1uzQQ6V7eImha7XqtCx6BqHR2DLkzdabCq/KmZUWWObtV6SNuXPl/dHfouLP0+bksQjKH8iRnp6yJTgPpu9dumY1ITJMJKUquaCHR04lUF93VJrDyvo0QXKmuW/Mw+0KrM0a359PR0r4eW+2bgsUTaz+wDHa3fpgoYhWVfRM0hXaxRc6o11oGO5k2oKke3832ifvHoWGl2/GAd6OhEa9i5pllsupuUjje6cFvT/QkZyK9PdVEwanufLy9UXlL6XVtjG2hVrVK/s/qcS9Vun+m4Swj0d7aBjibxRMHYZ/mN8S76fUsXcEtsA/3+/fuV76XFrbsUihpEx87eM6aBVqUqVegoEDW4fv168ShLv3NLLCf4Ryd2yll0y9vNTXXh5FWYupoX0QpOrbAMdNSFpfcAx9a13dyU26cp1F3NC/ZuNG5ylIxdoXXxdK2On9/cnmJounRRRhdyK5p7Y2Xs7rq+1ZWm2Gnq4OBg5Xv4jgUfNjSk/5c+4u0h0BuKqjO2j0BviFWN9gtnY0NDRh1rHZmsUXOBHruJ0DdrT912Y6/ar10D0K18JioWBSwadLkMhbVrA9A83zqax3xZpQEUtrswHViJTqzCMPYoXn5LexsjhSm4KEsroLakfOYrFg2elMIwhl8XZZxCNKuuNODSkvK9uWJ556kuXUPGNSnNIkw9F3IrbB8KS7PSVN1qDnV07OzPYhzoqFrVOnKXF07vkmfhtc72E1C1Kp1gBbrUDt1n0YVIX/d33WfcgMJcOsl5O7Wa6JijNUZKv2trrO9R0Umeeg/CsekCLB0vWy7/YB3o6ETn+co1UNs5Ola2WP7B/ikiWstCVa/0kLVPosUmdcFGd6LW2Ae6bx/tfd/iQS8IRBddLcuZbUsT/Tx3795d+V6WN+PZx1DrDhI1NdSkojr/rIlA68RH7UwNJ0fB2YUhxxStStqqZnri+zaEVzWM2qrblDfbjO4a+n1KD7wtK59hMwqzFjePKNRPnjwJgzS1IcegZgYb23draqxUTY++hyhVx6dPn+5kFSI1MfruEnmzTnRrbvBflS1qT6dFv6+qpHoYoko5FvVi5J8XyXeZqOnUuiYbYbnC9Q1/q2K+e/du9L2+M104Q/b6TkubG0VdkGh48/qhoVbo1AxQ8E5OTubt100fxjTJqG+ruWWEebimH5MVar22NKTLLg8/60ttcX3pDZEhc5DztE+1y9fdPkIhns1mhHmg5vt98lvZqsJD28sK5fLonf770vt80Shfn7xPIW3m4ZoPdFp0g6nSahj8MgFUxR1z/br8xjiTjtZHoBdyO1Xt213uo01V3gyB/kWu1npg2+acaf3Mvl1j0Y9Ad1B1zP3VqtgK9xQDLfmtGnULMow9Dj7FQN7PUF9qguTutk0e9PJOVuq1oI08PgI9kIKoqp3nUCjUWoVJ68yVluZKS2uE5N23qMTT4tO9pNwXjf3CozSsEGhYIdCwQqBhhUDDStW9HHrvDlhWRYVmXgOGqiIprD0xHbfPtopA9+00hcuZYoeuXasiJfrQWfJqXBqOj9b9q1U1l6cm8uiVp/Pz8+r3Sdkl3ekUZNd1Paq633Rtn4b1uM8/qbIBtY3t01AnnrRghUDDCoGGFQINKwQaVgg0rBBoWCHQsEKgYYVAwwqBhhUCDSsEGlYINKwQaFgh0LBCoGGFQMMKgYYVAg0rBBpWCDSsEGhYIdCwQqBhhUDDCoGGFQINKwQaVgg0rBBoWCHQsEKgYYVAwwqBhhUCDSsEGlYINKwQaFgh0LBCoGGFQMOKAn3GKYWJMwX6EWcTJv7TDvB/pZQOUkq/p5RmnFlU6MW8MKf09/8mzn7LxDoZywAAAABJRU5ErkJggg=="

/***/ }),

/***/ "Osoz":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/router-context.js");

/***/ }),

/***/ "TqRt":
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "VkSw":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Ui_Icons_PlayIcon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("gfwP");
/* harmony import */ var _Ui_Icons_HeartIcon__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("oviB");
/* harmony import */ var _Ui_Icons_DotIcon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("rYez");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("YFqc");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_LazyImage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("q7pC");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
 // import "./index.scss";







const MusicCard = props => {
  // console.log({ MusicCard: props });
  const {
    dotIconClick,
    dotModalInfo,
    data,
    placeholder,
    parentClass
  } = props;
  const {
    titleTop,
    titleMiddle,
    titleBottom,
    images
  } = data;
  const optionRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  const liRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  const imageOnload = () => {
    optionRef.current.style.display = "flex";
  };

  const preventDragHandler = e => e.preventDefault();

  return __jsx("li", {
    ref: liRef,
    onDragStart: preventDragHandler,
    className: `${parentClass ? parentClass : "card-row  col-lg-2 col-md-3 col-5  px-0"}`
  }, __jsx("div", {
    className: "play-Card-container  play-Card-wrapper"
  }, __jsx("div", {
    className: "imageCard-top"
  }, __jsx("picture", null, __jsx("source", {
    media: "(max-width: 375px)",
    srcSet: images.phone
  }), __jsx(_components_LazyImage__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"], {
    imageOnload: imageOnload,
    src: images.web,
    defaultImage: placeholder,
    alt: titleTop
  })), __jsx("div", {
    ref: optionRef,
    className: "card-options"
  }, __jsx("a", null, __jsx(_Ui_Icons_PlayIcon__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"] // className={classNameNew}
  , {
    style: {
      fontSize: "0.6em"
    }
  })), __jsx(_Ui_Icons_HeartIcon__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], {
    style: {
      fontSize: "0.6em"
    }
  }), __jsx(_Ui_Icons_DotIcon__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
    click: dotIconClick
  }))), __jsx("div", {
    className: "descriptionCard-bottom noSelect"
  }, __jsx("h4", {
    className: "play-Card-title"
  }, titleTop), __jsx("span", {
    className: "play-Card-subTitle" // style={colorStyle}

  }, titleMiddle), __jsx("h3", {
    className: "play-Card-text" //  style={colorStyle}

  }, titleBottom[0], __jsx("i", {
    className: "fas fa-circle"
  }), titleBottom[1]))));
};

/* harmony default export */ __webpack_exports__["a"] = (MusicCard);

/***/ }),

/***/ "WLqv":
/***/ (function(module, exports) {

module.exports = require("react-infinite-scroller");

/***/ }),

/***/ "YFqc":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("cTJO")


/***/ }),

/***/ "YTqd":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteRegex(normalizedRoute) {
  // Escape all characters that could be considered RegEx
  const escapedRoute = (normalizedRoute.replace(/\/$/, '') || '/').replace(/[|\\{}()[\]^$+*?.-]/g, '\\$&');
  const groups = {};
  let groupIndex = 1;
  const parameterizedRoute = escapedRoute.replace(/\/\\\[([^/]+?)\\\](?=\/|$)/g, (_, $1) => {
    const isCatchAll = /^(\\\.){3}/.test($1);
    groups[$1 // Un-escape key
    .replace(/\\([|\\{}()[\]^$+*?.-])/g, '$1').replace(/^\.{3}/, '') // eslint-disable-next-line no-sequences
    ] = {
      pos: groupIndex++,
      repeat: isCatchAll
    };
    return isCatchAll ? '/(.+?)' : '/([^/]+?)';
  });
  return {
    re: new RegExp('^' + parameterizedRoute + '(?:/)?$', 'i'),
    groups
  };
}

exports.getRouteRegex = getRouteRegex;

/***/ }),

/***/ "akFz":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./website/values/strings/index.js + 8 modules
var strings = __webpack_require__("wtyS");

// EXTERNAL MODULE: ./website/values/theme/themeColor.js
var themeColor = __webpack_require__("vEqB");

// CONCATENATED MODULE: ./website/values/consts/headerMiddle.js


let color = themeColor["a" /* default */].mainColor;
const headerMiddle = [{
  text: strings["a" /* default */].EXPLORE,
  icon: "far fa-compass",
  location: {
    href: "#"
  },
  color
}, {
  text: strings["a" /* default */].TRACKS,
  icon: "far fa-music",
  location: {
    href: "#"
  },
  color
}, {
  text: strings["a" /* default */].PLAYLISTS,
  icon: "far fa-list-music",
  location: {
    href: "#"
  },
  color
}, {
  text: strings["a" /* default */].ALBUMS,
  icon: "far fa-album",
  location: {
    href: "/albums"
  },
  color
}, {
  text: strings["a" /* default */].ARTISTS,
  icon: "far fa-user-music",
  location: {
    href: "#"
  },
  color
}, {
  text: strings["a" /* default */].VIDEOS,
  icon: "far fa-video",
  location: {
    href: "#"
  },
  color
}];
/* harmony default export */ var consts_headerMiddle = (headerMiddle);
// CONCATENATED MODULE: ./website/values/consts/constJson.js
const constJson = [{
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI8dfxNQMO5mcaq3AC4IxG5N-u2GQtfT-gS7Va9HT9IICnHwEhTA&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}];
/* harmony default export */ var consts_constJson = (constJson);
// CONCATENATED MODULE: ./website/values/consts/constJsonDouble.js
const constJsonDouble = [{
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUTEhMVFhUVFxcXFxUXFRYVFxcXFxcXFxUXFxUYHSggGBolHRUVITEhJSkrLi4uFx81ODMtNygtLisBCgoKDg0OFxAQGi0dICUrLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKy0tLS0tLS0tLS0tLS0tLf/AABEIAMIBAwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAAEFBgcDBAj/xABJEAACAQIDBQUECAIHBQkBAAABAgMAEQQSIQUGMUFREyJhcZEHMoGhFCNCUmKx0fBywTNTgpKiwuFDY4OT8RYXJHOjsrPD0xX/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQIDBAX/xAAkEQACAgIDAAEEAwAAAAAAAAAAAQIRAyESMUETBBQyUSJhcf/aAAwDAQACEQMRAD8AgHY3Op4nr1pBz1PrTNxPmacCug4Qgx6n1ow56n1oFowKACDHqfU0QY9T60IFGBSGOCep9aIMep9aYCjAoAQY9TRBj1PrSAogKAECep9ae56n1pAUWWkMFXvpfXzornqfWozbmJVFAHvnVTqLfiNtba/I1ExbwyAd4i4uNLcR1Fjp46fCk2UoNlqBPU+tcMfKVika57qMePgag4N7kuBJGydSO8Pkb/KvVtfakb4WXIwuy5RYk8SBzA8eNFj4tFp9kmOzQhb+9Gh484SYW+QiPxrQVNYR7NttjDSBXvbPcWv7rrlkBJ05Iw/hrXo95MOzBFlRmJAABHPr00BPp1rzM0amz0ce4k5mNPc9a5g3ojWVlni23h5JMNNHEQJHidELEgBmUqCSL2te9ZG/s4xcSZey7T8SyKWv4Xtp4VtBa1NmrXHmePoyyY1M+bNpbr46FrNh58v3gjsPVb145FtoxytaxDM6nl1UWr6gU1zmgV1IdVYE8GUMONudbL6v9oh4H+z5pTBkoWDA5GUACUG+bMxvfj7tdYYZ42AjlljLR9oWV2AACNIR3GF9ANfGvoDFbp4CUkPhIbaE2QLrrY922vH1qHm9nmAJDKkiM6mMlJG0TJlsA1wO6AK0+4i+0T8UkZJht9Npworri5CMzJlktJ7qoSe8Dp3xzq07O9sbBVE+GzGwu8clrnrkZdPK9TWO9lMEi9mmJlURs1iyo984Um9svCwHwqG/7oZUHdxMbnlmRl/mam8M+yryRRPbJ9qWDnkSLLOjuyqoZQRmYgDVWPM9Ku5Y/s1k+xvZzi4cdh5nVGjSRWdlccFuQbHXjataK1zZoQi/4muOTa2AxNC1+tdGFMwrGiwE4U1dEXSlQFmQMNT5n86cCnYanzP509q908YdRRBaQFdAKQDAUQFOBRqKBjAUYFJRRgUDGAowKa1FSAYCuWJkygkECwvc6ADmTXPG4+OEZpDYdbacbcaou3d5ZJrqvdj1sBoxHK55UmzSMWzybTxZZ7qzMdSSbnTy5DjpXlGfmH425gX8+tcIiR3gSPEafCpbC7WZkETG1rhWBI4/ZYDRh000rNtnQkiOkUi+oPiDfx+FdYnciwN78r3ufLiDSxKsrEHW3O1cTGdDlPna3zpAEC3Dn0vU5u9jUjkHbRqy3Gjq5UEkC4KHSwubm/Co6ePMobQW0J5npm/K9FH7ttT+E/5T+/jSltDWnZ9AbsYvDvf6LPnjFs0RYsY25ZSdQOII1FxpU/evn7dfbsmEYN2QZAbZrWy66DPlNutvGtW2BvimKB7MAFdCruFbwIHTx0rhnBx/w6IyUiyy3oVNNHJm/Pr6HnXSMa1lYw1olHu+f6mgJ0rsF1UdAf5CihjH7R/fD/Why6qOg/QUZ90+Jt6m1I+8x6D9TT8A4wjQnqzH52FPaniFlHkPnrXS1UloTZxIoCK7EUDCk0FgWpmWuhFMakLEi6UqQlUaFgD0JApqoVmQMNT5mnApNxPnRAV7J5AgK6AUwFGBQMQWjUUlFGBQAgKILTgUVIoYm1Q+19oWjYrowNiLXIKkMNByIv8ACpY4hLEE8tb6fnVF3jxvfKi+ZdA9+KG5Ct1tfQ0rLjEiNoYt5DqW46LckD4VwfBNoPtHl++dS6bNKZHkuCfs81FtP7R105DjTr3pwwFltlAHTl8f1rFzOlQo8EWBJUgDgLk+P7/KvIcMeJrTN3NjRlGJVXYnS/uKNNT94+FebamwCSQBwBNx1J5j48P0rD7hWdH27qygWbTXU6cflXrIKizXuDbXl4a8ONe3aGzDGx8OP76muCxtLlQk2HA6m3QE1ryTVmXBp0c8N3ja3/Tp4cqGeNraFsvi3ytfSrCuz8sZIFgBlv1IvcanSoWddMvIcel/PlxtURyKT0aTxOK2eM4xiDe5Btm10vwzW5Gu2zSqyrnJCn7Wvyym/EAUo0yhlYWOh8COh+Vc8ehFuNrX05edaaejCq2a1udtOWPEjDs/aRSAlDnD5CADbNp1tYi/ia0VUr502FtN1kja7WHA3UZSRYHMRwva4Nb1sbHAxCR5FK5ST7wyZQC2YubjjfXhbpXHOHFmylaJIiuwHePgB8yf0rGN8Pa27O0eAsiD/bst2bxRDoo8Tc+VUmLeDGTzR9pi8Q2d1B+te1i1j3QbDS9VHBJ7YfIkfTltF8Tf5XoJuD+n8q+cdm74Y+GOSWPFS/0iBQ7GRRmEjEZXuOCAfGtO3e9ogkkOFxahHVY2aYECO5EZcMD7nfew5eVEsLSGppmgk8qe1CK6EVAAGhIroRQGkMC1RO8G2kwyHgXylgCbKqjjJIfsoPnwFcN5N448Mr95cyi7u3uRA8M1tWY8kGprBt7N6XxTMqlhGWzHMe/Kw4NIR05INF5da0x4uWyJTromsbvsrSMRAs1yfrZHdWfqco0UdByFhSqlwjQUq6uETGzUmGp86ICkRqfP+dEBXScQ6ijAplFdAKB0OoolFICjFIBAUQFOBTgUFUQO8yIy2JyuPcP536iqVGbydQuvW4HAk8emtXzei4gJyhrEEg6gqD3h6XPwqg7MhLElb2sT45V1P5AVnM2xkx2jSsAx4XJ1vqTYADmbDX4U8EYDDlpqSfy8fGoqDEtwvwJ4G2vnzqY2UoLasVA5gXPpXNNNI7MbTZYtn4goFHDoP1FTcb5xwGoOvH46XHrUDIq5hYlh5EC19AL61O4F0tbmOQvl8unKuGR6MYkRjNmPKRHrkFr2AvwIHer1bN3VsLgfZ4+J/KpGWYrfLqTbT5D871O7IwgdFDAmwuVJbLc8AVva3h4U+UnoXBJ8im7dwGWOKCLUk2aTldtDlbnz1rybxbvDCwRrmHaSnNlA91Rc8eXL1rRsbswGRXfgvK3T8gKpm0pVnxLSufqoxZb89baDncggdbGhNrQOKlsoeKwJjytJYZuvK518tOVefa+HKItwAyqnC/Frn8hUztnaInm1UkKwPIC+oy/sVz2WwxWLKy+4SRYdFBCgetdcZOrZwzgrqJ5tywjv2UiB1DBrcTZtNANTY8vKrDvKpw+CxEUU1w7Il9RZNZMgJNwTlKm/3QKqJQ4fFfVkjKRbip68v2bVeJNmDaOAm7ASGWPvXJHeyWJUgm5bKTbjrSn+al4RH8WvTIq9+xzaUMR7qu392NiPnarBs3dQte4J0HIjU/CtR3c3HiGFivGCXQ3JHEMT/KtnlXSI+KVWzEYo2aKKMf7WY+oEar/72r34yUE4+S9wziMeTTdoPlBVkxm74TEQkKQsbvJblYSyPb+6i1UZ4CmFsR3nnNz/AOXGP5zmqjJS6JcXHs2b2M7wticK0MjZnw5CgniY2ByX8rMPICtEFZH7AcC4TFTEWRjHGviVzM3pmX1rXAK5siqTo1j0Mar+923VwsRObKxUsW49mg0L25kkhVHNiKsJrE/bRj2zMlz35Qp/hhjQhfi0pbzApQjykKTpFD3k3gfFPzWJSSkd76ni7n7Uh5sfLhUPEhYhQLkkADqToBTVfdyN3SlsRKLMR9Wp5A/bPj0/6V2peGEnSJ/Y278MUKI6Kzgd4/iJJPzNqVSycKVaUjn5SIsjU+ZowKTDU+ZohVGY6iui0KiugFIpDgUQFIUQoGPaipKKcCgCsb9YgpCACO8bEWJJ9OH+tVfCYvIl107QGPrZbC48+fxq078QRmMF1ObgrcuttOB/Ws/RsrZPHS3U25VnNWbY3SPZhAOut+Hh1qf2ZAxYBbj98yah9mwlpcqqTcC1vHn5aVqGw9hroZHVV01JA/u9T41z5X4dmBeg7H2E0pHdY/eYkKunlxq14rAxxRhbagacNfLp5n516cLtCJAI4ihA07p187866rhDKbyHh8/0rk+M7vk98IPZOzu0ckjQai3C/nVuwuC7NeH75UEcKrqLAAcKafbUIsC4q4xUeyJzc+jx7Zw6MjdwMSNdSF+J4W9apaYcODlUE8SxFtRwCg6KANB/rV6faMDK13vcHj5cq8GJSIxdwroPDlzPQ1Eop7TKjKlTRjG8adiWBVhrcdB4i2h8zUHsbEkShr8xr5G4qzb+2DXGvhfTlVQwisxLHhbXkPAACuvErhs4szrJok8Ue1kzA6s2nrx/OtL9k2EX6VPIAxyrlDDhZjcg+N+XnwrKsIxuABcXGnh+9a2z2RwFIZWJuXcEnxUWNx6fvjGVUkhRd2zntXa+y1lkCYmKKQMQ6OHRS6Eq2ViuUajlpVt2XtvCNGoixMD5EA7sqNYheGh46GsTwG0ZMRKUg2pKGdmYRyQM0a5mJsGzOAovbgOWmtqlNtbLxMKWeXZs7ZyG7aKGLXKuQBsi66vrm10tWiwRjtekPPJqn4WyWDML5b9y3Ak6rbTT8VQg9nsuKVEb6qNXdndh3+8w0VepCrqeFxx4VDZp4+3ePCuO8FVsFipCWGe+aySSKpCoOCjjatG9neMlljlMrTnKYltOQXDGJZH72UEnvqDccVrH4ZY1aZrLMsmqJ/Y+yosLCkEC5Y0FgOZ5kk8yTqTXsAogKcCpJOZFZD7bNlkqZAODJJ8GHZP/AIhF61sVqgN7cIGjV7XyMA2l+6/dvbwbIfhVY3UiZ9GKbpbo2tNiV14pEfkzj/L61c2NE1+B4jSgNegkcEpNnRDpSpR8KVAiPPE+ZogKTDU+Z/OiUUxBCjAoVowKBhAUYFCKMUDCFPakKIUDPFtXCpLGUdSw/D7w8RzvWV4/DBZHtfT3c9wza2uP0rYSL1RN89jpGhdc2p93iAeJJvyqWVB7KthsY5AsbZb6g2NvPnUlFNKwKx52spYm7BdBc89TVg9lexUnL51vY21HxrUMHusYTeEgKfeUgEG3A6jjqawc0nVHZHG2rujEcNi8VFdwr2QZmIzd0XsCb308a1L2abytjPqmPeUXv1FXHDbGRFKhI1U6kKigE/AV4th7PhjxbGMDNlNzp9or0/hFRNxfhrjjOPbs9m8WHdI+6SSeQ51nEuwMbLKq2KZtc7BmsPLh8PDlWw4i2YXp3w+YaEjyNqlR2W5vjRiEewNpLP2KnXORcppkv7x0tbLrfNbW3GrJs7ZGPRrOiMo0LIwsPNT+YrRU2a3ORiOhtXqSAKLD51U6fSoiDce3Zi2826plkFhxuWFuGnG3wqlbX2aMNLlvYjRtdL+FfRe0411OUE29axnezAGfGEJzsbHlew/SsotxdPo0nFTVpbKsQEVW6sNL20A1s1ud63bdyIwbOZ2Iv2TyGx7ijISoQW0WwB0HMnnWP7R2QXxS4VLERWVyTZSzHveNgMvxJrXtro2H2TMtu0KQZAtj3r2RUsupGoGmtObtxRjVJlL3Vw+KTvzT4SaMAIRaINYAllLSRKQdF0J5V7toxSlosPJg45o2YPJLHmsgVsyqFje5IsLmwBPLjaP2VjoJUlk+jWdMskqZnGZjrlCSI1gSGU25W11qxbZeOSIstwVjMkVo0ktmUhSLlWOgawuL2tXWzjTKJvVsXC4VEVosQhmlveORW76L3bdooNj2raX0txrWNwUH0VmDMweeaxYWNkfsgALnT6vTrx0rMtl7OxROESHGsVt2xVzLGXiMi3sCCraWFr/a8a1rc6JlwOGzkFzErMRaxZxnYi2mpY1lmejXES9qe1PalXObAkV5dpQdpDIn3kYfG2nztXrtQyaAnoDQIy3EHW/3grfFlBPzJrga9E49z+BfnqPkRXEivQj0jz5dhR8KVEi6UqoR4SNT5miApm4nzohQIMUYFCtGoplDiugoBRikAQFEKaiFAx7VwxuEWWNo2GjAjxB6ivQKIUDKxuNIcJjJYGOpysDwDAj9+lbHg5swF7Vhu+kjQ4qGdemXlyN/1rQd2d5RIgPUfsVyZlTs9D6eSlGvS64h7KT0FQG6OHYs0jixJtboOOvjrTbc2p2eGllH2UYjzsbVQNzvapHGHXEqQSxKsovcHkehHzrNJvaNm1HTNixg9afBYjiDxFZhtT2sKZQIoHkTS7AEG/S1qv8AsctJCsrAo0gzFTxW/AHxtahppiVOJN9pXCaSo44ll0avFi8ceA/etHIFAPaWK0NqyHb+1Po2LaW2ZhcKp0F+APkLVo+NxJtpwFibdTyrIN8ZzJiyoBIGugv8bDxvSgrkGSXGOjvurhsViMSHiuZM+dnI7gN7jOeFvDjpWte0V2XZjDtFiZ3hXtLuqqc4Y2ZQWHukDSvB7NcIohXKVJ4mxBN/EcRXT2xPGMLBHIJCsk40iy5+6jkWzAg620oi+WTowlqBAbn4PHRNIJsUsquoyg4nMQRmawEwBViFta1tdfCzAYgSMWQNGRdcsSO4Ol1LAWCj3ha5tw4VRH2/hJcsMwxCHgqtCq2a9kbuOCALE8Na6bJx8ky5UxOHlZjljJSWFgQrsScyFb3KHoBeuuSZyo9u0N4UV58N9HAJw5kQqMrCRzl0LZgmjKLganiNNNfw0IjRUHBFVR5KAB+VZBhosW+0GTtu0gMuHj0kSUEdpG0pKXJTRHHAca2Q1hlXRti6GNI0qasjQVRm8WK7OBre8/cW/VtGJ6ALc17cXikiUvIwVRz/ACAHM+ArMd5d5PpGLXDLo1iXXj2MI1Ksf62Q5Qw+yptxOlQjyZMpUh52BYkcNAP4QAF+QFcSK7OKAiu5I4HsJBpTUUY0pU6CzwnifOiApm4nzNEKBBLRChFGopjQQoxQiiFIYQo1oKKgYQp70IpwaAK9v7hO0wjMOMZD/AcfkTVf3G2iyyBb6HUDl5VoLqCCDqCCCPOsq2phHwM+XXJmzI1jqOg8eVZzjaNsUuLL1tLfaMg4fEIyKwIvbunl6VTX2LgHl7mMCIeN1Jt5HTSrvh8Rhto4fLKAWHA6Ai/Cx5VHYbYuPwr/APh4Enj4r3QGHhrXNCl/TO+uTXLaLDu2dm4VcsUxnPG0UbSnxuFHGp6PeeIkLFFimbhY4eVRwtqWAA9ar2Bn23IcownZA8ywC+gNW3ZuyJYgHxEhdrXIuAo62AFZySWzf+NVFjbNfFO318QjGpALKxtyvbgfia6YjDAtc8tL17J8aNL+YPDS3yqs7e3hWEEn4G/HXlUqn0Zu/Tz7w7TTDwk8zwB5nUWrENq4h+3Y3IZTa40II8fWrnHjGx+MQH3UOY+QuRf41G787tSxH6SoLRSM1yBfs31zA2+zoSDXRjSXZz5ZcuujlszfnGRWuyyAcpUDN/f9751Z/wDt3LiYpZGgQth0FgWd0BldVLAFwyWCkXU373KsvU/sVKbF2xJhw/Zsoz2DBkjcEDwdTprWsYqznk9FzwO98EzAYvCKcisVkRmJjAUsbBybcOR4mpTAbc2ZE8H0aKSNXZiBlzKTYRsHJdWyki9vw1UcLtwFZC+FwjWTiIezY5mRLExMunePpXtjxOEzRn6MylMO8l452soZZGtllV7nvjW/EjpWjRBN+z6KH6Yk4xCylppJTZGia6QyCwR9NDiVOhPEVsK7YjP2Zf8AlOfyFfMu3xAsUAhMhB7R8siqD3mCe8psdYugqHjmI4Fh5EisZw5M1jKkfWMm2EH2JT/wmHzawqu7a3/w8AN5IkI5PIJH/wCVCWPqRXzc0pI1JPmSfzrmTUrEh8maDvX7TJJiRh86nUds9g4H+6jHdi/i1bxFF7M8H3Jp2uS7ZATxIHeY353JHpWdohYgAXJIAHUnQCtq2Fs/6Ph44uar3v4ySzfMmtYJGWR6PawoGFdDQGtTnY6cKVFGNKVAjwHifM060zcT50QpjCFGKFaMUDCFPTU4pAEDT0N6V6Bh3pXoL0E0qqpZiFUcWJAA+JoA6568G24IJYiuIKhPvMQuU9QTzqs7a35RLrh1zn77XCDyHFvlVI2ptWbENeVy1uA4Kvko0FS2i4wZK7A2uMNMQGzx5iAeFxfRrHhy9a2Ddze9HsMwsLD1H/X0rAEr0Q4l11ViPIkcOFc88fLZ148vHT2j6Wk3ijDFS3AXseNeXEbwLfvN3e9x6af618/Da8/324W48qCTGSMLMxI6XrN4JP02X1K/RfN5t9C75YfcUnXraqljtoy4h7XJJ0AF7V59n7PeVgqj9K03c/c5Y7O4u3lw8q0UY40YucsjC3I3eMEWZh3m1P6Vc4MMPohuNRLGR59tHb87fGvSYAq2r0dn3I0+9NF8cjiU/KM1Ftuy+lRgu/eyUw2PxECKBGGDIOiuqvYdACxA8qjdlbXmwylYpmRSxJW+hPDVToeHSrZ7YkttR/GKE/Ij+VVjZ23MTEoSKd1XU5A111OvcNx8q7Y7SOKXbPcN45OykMkeHkIZAM+Hi1Bzk3KgE+4vOvVPtCC85fCIMkaRns5JIybmNCLMWUWseA4CrFuzh/puFvI0Xbdsf6XCRMjxKApKdkFYsC471+ZHjVbx+0MOwnLYVTmnCho3liLW7Rr5XzAH3dLfa8KQFe3meLPGIw6hYkBDsGILDP7wAv7/AEFQ5NTG8UatiZclwFbJYkMe53OIA07ulPs7Y8MtgcVHGekiMP8AF7p9ahxZaaSIS9FGhYgKCSdAALknoBzrQMB7P4Tq2JMg/wB2FA/vXarPsvYmHw39FGA33z3nP9o6+lHETmkV7czdMwkTzj6z7Cfc/E34vDl58LjTGmq0qMW2xzTUjTGmI6oNKemQ6UqQjwEanzNOKZuJ86cVQghRigFEDQUHSoQae9ABUqjtq7Zhww+sbvHgi6sfG3IeJsKpu1d7p5brH9UngbuR4vy+HrQNKy2ba3hhw1wTnk5Rrxv+I/ZHz8KoG2drTYk3kOn2UGir8OZ8TXjA/f8AOnAootaPI8dcJI7VIOtcpI6mUTRM8aGuq1zkS1HBqQOFRRQYqZ2Fsd8QwsO7zNS+xN0DIAx4Vf8AY2yxEAAo9BUSnXRUY8g93N2VjUWX41dMNhgo4cK82BQgCveVJFY9m/R5pBmYV0U5sUicoYzI38UpMcf+FZvUV6cLh+Zqgbwb/R4SGVoiHxeKcsi3zCGIARwM/Q5FDhOrm+lXGNmcpFK9rGMWXac2U3EapGT4ot29CxHwqFw23p0RUzIyKNFeGGQaDh3kJ+dRTMTdmJJa5JJuSTqSTzJNTH/9ePg+Dw7WAF17WNviUcD5V1pUjkk7ZLYPeZlOHUwQgAPKOzEkOU3kDMqxOFBKx2Omt64YWXCyfR80MiZ5XkOWbOL3jVi2dSx9w8+vWpbF7EjUZ2wsgRMNmMkOKSRFVkN1s63zDOTa+vGoxIcMMpjmcdnA7hZIhoGEjXLox1u45dKkGVeUhmZhezMSLgA2J5gG16ApTqdKerGNA7RnNGzI3VSVPyq0bH3zdbLiRnX+sAGceYGjfI+dVi9MVpUJ7Ndw+ISRQ6MGU8GGoNdL1mWwNtPhX5tGfeS/+JejD51o8UyuqupurAEHqDwpENUdb0jQXpwaCTtHwp6aMaU9AzwtxPmacUzDU+dOBTEEKKhpUAEKqu8G9oS8eH1YaGTQqD+EfaPjw868u923iWMELWAuJGHEnmoPIDn6VUlWg0jH0KR2ZizMWZjckm5J8SeNOKa1EKZQ9KlSoENT20pGlQM4tHXEw17rUNqVD5ElsDenFYTRCHT7kgzD4Hiv5eFXbZ/tSh07bCOD1jdWHo2W3rWcWprVDxxZSyNGwx+1jAAf0OKH9iL/APSuWK9sWGA+qwk7H8bRoP8ACWrJLUqPiiHysue3fanj8SpjjCYdGBB7O7SEHQjtG4fAA1SUitRkimzCrUUiG2wkYXW+ouLgGxIuLgG2htUzIMAb2bFR3NjdIZhf4FLjSojB4ftJEQMi5j7ztlUWBOp5cKncNu1OWTL2Ugzgns54X0uOQa/XlQSya2ntJJYsRGuIw+U9nHaSCWHJkuoHcDIx+rJDcio5aVD7Q2W6DEOGjYJFHH3JEY6GJWut8w0VjqOdcpdhYgL9ZDKrSzC90awGupNrWvKfSvDjpLpK39ZiOPgvaEj/ANRPlU0VdkeKc01PViFSFMaEUAItVv3H2pxw7Hq0f+df5+tUy9ejBYkxusi8UII+HEfEXHxpA0azenBrlFIGAYcGAI8iLj86O9IyPVG2lKhiJtSoGeVuJ86cUjxPmaQpiFXk2rjOxhkk+6pt/EdF+ZFes1VN/sXZI4QdXOdvJdAPU3/s0DStlMF+J1PM9SeNPTUiaDU6CnNcYW1I+IruaYMYU9MKRNAhU4FNenoGFTU6mhoEI0jSp6AGBpWpjSBoAWShKCulMKBigUBwegJ+VWXdDCJJiY1kAZcrue+EICgm4PM6DT9Khdl4x4ZC8ZF8tu8quCCfuuCOVTMe3rh+0gw72j/qhGbsVW14iunePpR4Q9smtu7PXAthfo0sxibtJe1EgMbqBmsCti1gijhbU6m+lV2ptGaSCBZZC+rv3rFuIQd46/ZbnzqTbF4Y+9h2TJh+Mcx0Eg4BZA2v1x586idtiIGIRZ7CJdHAuMxLjVePvdBUop9kbSvSNKqAY01O1A1IYJ404NIimFAzRt0sRnwqdULJ6HT5EVMA1Vtwpfq5V6Orf3lt/lq0XpGMtM9EZ0pUMfClQAZUXNOFHSmpUAIqOlUDfgD6V/w1/NqalTLh2QYUdBSyjoKVKgoB1GZdBXpyi3ClSpIb8BCjoKcqOgpqVMTHyjoKdlHQUqVMBKo6CllHQUqVIXoig6CnCjoKVKmN9A5R0FNlHQUqVAMIKOgpFR0FKlQIUajvaDgPzr2YdBlk0HBOX4hT0qQju6i8+n+yh/8AprzbWUdrwH9HF/8AGtKlQM8eUdBSKjoKalTGM6joKHKOlKlSGDkFhoKQUdBSpUDfRatwx3pvJPzareFF6VKgwl2dFFNSpUhH/9k="
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRF9IIwP_vI4I30S2hhzXBtsMQQ5IDg4mUlCJG5YADjyk1iZtzD&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ2lWkUYUjSisqChVAJ2uaMwWIVb2aSP55jx-MTTxL2OIHY1TWX&usqp=CAU"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}];
/* harmony default export */ var consts_constJsonDouble = (constJsonDouble);
// CONCATENATED MODULE: ./website/values/consts/moreExploreConst.js
const moreExploreConst = [{
  title: "Geners"
}, {
  title: "Instruments"
}, {
  title: "Videos"
}, {
  title: "Artists"
}, {
  title: "PlayList"
}, {
  title: "example"
}, {
  title: "example2"
}, {
  title: "example3"
}, {
  title: "example4"
}, {
  title: "example5"
}, {
  title: "example6"
}];
/* harmony default export */ var consts_moreExploreConst = (moreExploreConst);
// CONCATENATED MODULE: ./website/values/consts/instrumentals.js
const instrumentals = [{
  titleFa: "پیانو",
  titleEn: "Piano",
  icon: "far fa-piano"
}, {
  titleFa: "چمپن استیک",
  titleEn: "Chapman Stick",
  icon: "far fa-guitar-electric"
}, {
  titleFa: "گیتار",
  titleEn: "Guitar",
  icon: "fal fa-guitar"
}, {
  titleFa: "ساکسیفون",
  titleEn: "Saxophone",
  icon: "fal fa-saxophone"
}, {
  titleFa: "پیانو",
  titleEn: "Piano",
  icon: "far fa-piano"
}, {
  titleFa: "چمپن استیک",
  titleEn: "Chapman Stick",
  icon: "far fa-guitar-electric"
}, {
  titleFa: "گیتار",
  titleEn: "Guitar",
  icon: "fal fa-guitar"
}, {
  titleFa: "ساکسیفون",
  titleEn: "Saxophone",
  icon: "fal fa-saxophone"
}, {
  titleFa: "پیانو",
  titleEn: "Piano",
  icon: "far fa-piano"
}, {
  titleFa: "چمپن استیک",
  titleEn: "Chapman Stick",
  icon: "far fa-guitar-electric"
}, {
  titleFa: "گیتار",
  titleEn: "Guitar",
  icon: "fal fa-guitar"
}, {
  titleFa: "ساکسیفون",
  titleEn: "Saxophone",
  icon: "fal fa-saxophone"
}];
/* harmony default export */ var consts_instrumentals = (instrumentals);
// CONCATENATED MODULE: ./website/values/consts/index.js





const consts = {
  headerMiddle: consts_headerMiddle,
  constJson: consts_constJson,
  constJsonDouble: consts_constJsonDouble,
  moreExploreConst: consts_moreExploreConst,
  instrumentals: consts_instrumentals
};
/* harmony default export */ var values_consts = (consts);
// CONCATENATED MODULE: ./website/values/index.js



const values = {
  consts: values_consts,
  strings: strings["a" /* default */],
  themeColor: themeColor["a" /* default */]
};
/* harmony default export */ var website_values = (values);
// CONCATENATED MODULE: ./website/utils/convert/sliderCard.js


const sliderCard = (data, direction) => {
  let convertData = []; // console.log({PAPPAAPA:data});

  for (const index in data) {
    let noEntries = website_0.values.strings.NO_ENTRIED;
    let title = data[index].title ? data[index].title : noEntries;
    let parentName = data[index].parentName ? data[index].parentName : noEntries;
    let parentArtist = data[index].parentArtist ? data[index].parentArtist : noEntries;
    convertData.push({
      titleTop: title,
      titleMiddle: parentName,
      titleBottom: parentArtist,
      images: data[index].images,
      location: {
        href: "/slider",
        as: `/slider#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_sliderCard = (sliderCard);
// CONCATENATED MODULE: ./website/utils/chunkArray.js
const chunkArray = (myArray, chunkSize) => {
  let results = [];

  while (myArray.length) {
    results.push(myArray.splice(0, chunkSize));
  }

  return results;
};

/* harmony default export */ var utils_chunkArray = (chunkArray);
// CONCATENATED MODULE: ./website/utils/convert/albumCard.js


const albumCard = (data, chunkNumber, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (let index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleMiddle: data[index].artist,
        titleBottom: [data[index].releaseDate, data[index].genres[0]],
        images: data[index].images,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    if (chunkNumber) convertData = utils_chunkArray(convertData, chunkNumber); // convertData = chunkArray(convertData, Math.ceil(data.length / 2));

    return convertData;
  }
};

/* harmony default export */ var convert_albumCard = (albumCard);
// CONCATENATED MODULE: ./website/utils/convert/topTracksCard.js


const topTracksCard = (data, chunkNumber, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (let index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleBottom: [data[index].artist, ", ", data[index].artist],
        image: data[index].cover,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    if (chunkNumber) convertData = utils_chunkArray(convertData, chunkNumber); // convertData = chunkArray(convertData, Math.ceil(data.length / 2));

    return convertData;
  }
};

/* harmony default export */ var convert_topTracksCard = (topTracksCard);
// CONCATENATED MODULE: ./website/utils/convert/singleCard.js


const singleCard = (data, direction) => {
  if (data) {
    // console.log({ data });
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (const index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleMiddle: data[index].artist,
        titleBottom: [data[index].releaseDate, data[index].genres[0]],
        images: data[index].images,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    convertData = utils_chunkArray(convertData, 2); // convertData = chunkArray(convertData, Math.ceil(data.length / 2));
    // console.log({ convertData });

    return convertData;
  }
};

/* harmony default export */ var convert_singleCard = (singleCard);
// CONCATENATED MODULE: ./website/utils/convert/comingSoonCard.js


const comingSoonCard = (data, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (const index in data) {
      let noEntries = website_0.values.strings.NO_ENTRIED;
      let releaseDate = data[index].releaseDate;
      let year = releaseDate.year + "/" + releaseDate.month + "/" + releaseDate.day;
      let title = data[index].title ? data[index].title : noEntries;
      let artist = data[index].artist ? data[index].artist : noEntries;
      let genres = data[index].genres.length ? data[index].genres[0] : noEntries;
      convertData.push({
        titleTop: title,
        titleMiddle: artist,
        titleBottom: [year, genres],
        images: data[index].images,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    return convertData;
  }
};

/* harmony default export */ var convert_comingSoonCard = (comingSoonCard);
// CONCATENATED MODULE: ./website/utils/convert/moodCard.js
const moodCard = (data, direction) => {
  let convertData = []; // console.log({ mood: data });

  let dir = false;
  if (direction === "rtl") dir = true;

  for (const index in data) {
    convertData.push({
      title: data[index].title,
      images: data[index].images,
      location: {
        href: "/slider",
        as: `/slider#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_moodCard = (moodCard);
// CONCATENATED MODULE: ./website/utils/convert/playlistCard.js


const playlistCard = (data, direction) => {
  let convertData = []; // console.log({PAPPAAPA:data});

  let noEntries = website_0.values.strings.NO_ENTRIED;

  for (const index in data) {
    let title = data[index].title ? data[index].title : noEntries;
    let publisher = data[index].publisher ? data[index].publisher : noEntries;
    let tracksCount = data[index].tracksCount ? data[index].tracksCount : "0";
    let genres = data[index].genres ? data[index].genres : "0";
    convertData.push({
      titleTop: title,
      titleMiddle: publisher,
      titleBottom: [tracksCount + " " + website_0.values.strings.TRACKS, genres[0] + " " + website_0.values.strings.FOLLOWERS],
      images: data[index].images,
      location: {
        href: "/playlist",
        as: `/playlist#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_playlistCard = (playlistCard);
// CONCATENATED MODULE: ./website/utils/convert/videoCard.js


const videoCard = data => {
  let convertData = []; // console.log({ mood: data });

  for (const index in data) {
    let noEntries = website_0.values.strings.NO_ENTRIED;
    let title = data[index].title ? data[index].title : noEntries;
    let artist = data[index].artist ? data[index].artist : noEntries;
    let releaseDate = data[index].releaseDate ? data[index].releaseDate : "0";
    let genres = data[index].genres ? data[index].genres : "0";
    convertData.push({
      titleTop: title,
      titleMiddle: artist,
      titleBottom: [releaseDate, genres[0]],
      images: data[index].images,
      location: {
        href: "/video",
        as: `/video#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_videoCard = (videoCard);
// CONCATENATED MODULE: ./website/utils/convert/PlaylistForAutuminCard.js


const PlaylistForAutuminCard = (data, direction) => {
  let convertData = []; // console.log({PAPPAAPA:data});

  let noEntries = website_0.values.strings.NO_ENTRIED;

  for (const index in data) {
    let title = data[index].title ? data[index].title : noEntries;
    let publisher = data[index].publisher ? data[index].publisher : noEntries;
    let tracksCount = data[index].tracksCount ? data[index].tracksCount : "0";
    let genres = data[index].genres ? data[index].genres : "0";
    convertData.push({
      titleTop: title,
      titleMiddle: publisher,
      titleBottom: [tracksCount + " " + website_0.values.strings.TRACKS, genres[0] + " " + website_0.values.strings.FOLLOWERS],
      images: data[index].images,
      location: {
        href: "/playlist",
        as: `/playlist#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_PlaylistForAutuminCard = (PlaylistForAutuminCard);
// CONCATENATED MODULE: ./website/utils/convert/index.js








 // import moreExploreCard from "./moreExploreCard";

const convert = {
  sliderCard: convert_sliderCard,
  albumCard: convert_albumCard,
  singleCard: convert_singleCard,
  comingSoonCard: convert_comingSoonCard,
  moodCard: convert_moodCard,
  playlistCard: convert_playlistCard,
  videoCard: convert_videoCard,
  PlaylistForAutuminCard: convert_PlaylistForAutuminCard,
  topTrackCard: convert_topTracksCard
};
/* harmony default export */ var utils_convert = (convert);
// CONCATENATED MODULE: ./website/utils/index.js

const utils = {
  convert: utils_convert
};
/* harmony default export */ var website_utils = (utils);
// EXTERNAL MODULE: external "redux"
var external_redux_ = __webpack_require__("rKB8");

// EXTERNAL MODULE: ./website/storeWebsite/actionTypes/redux/index.js
var redux = __webpack_require__("lmmF");

// CONCATENATED MODULE: ./website/storeWebsite/reducer/errorReducer.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const errorInitialState = {
  error: null
};
const addFailure = (state, action) => {
  return _objectSpread({}, state, {
    error: action.error
  });
};
const removeFailure = state => {
  return _objectSpread({}, state, {
    error: null
  });
};

function errorReducer(state = errorInitialState, action) {
  switch (action.type) {
    case redux["a" /* default */].ADD_FAILURE:
      return addFailure(state, action);

    case redux["a" /* default */].REMOVE_FAILURE:
      return removeFailure(state);

    default:
      return state;
  }
}

/* harmony default export */ var reducer_errorReducer = (errorReducer);
// CONCATENATED MODULE: ./website/storeWebsite/reducer/homeReducer.js
function homeReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function homeReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { homeReducer_ownKeys(Object(source), true).forEach(function (key) { homeReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { homeReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function homeReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const homeInitialState = {
  homeData: []
};
const setHomeData = (state, action) => {
  return homeReducer_objectSpread({}, state, {
    homeData: action.data
  });
};

function homeReducer(state = homeInitialState, action) {
  switch (action.type) {
    case redux["a" /* default */].SET_HOME_DATA:
      return setHomeData(state, action);

    default:
      return state;
  }
}

/* harmony default export */ var reducer_homeReducer = (homeReducer);
// CONCATENATED MODULE: ./website/storeWebsite/reducer/themeColorReducer.js
function themeColorReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function themeColorReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { themeColorReducer_ownKeys(Object(source), true).forEach(function (key) { themeColorReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { themeColorReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function themeColorReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const webThemeColorInitialstate = {
  light: {
    primaryColor: "#0070ef",
    accentColor: "#666666",
    mainColor: "#ffff",
    titleColor: "#333333"
  },
  dark: {
    primaryColor: "#0070ef",
    accentColor: "#666666",
    mainColor: "#000",
    titleColor: "#ffff"
  },
  currentTheme: "light"
};
const setThemeColor = (state, action) => {
  return themeColorReducer_objectSpread({}, state, {
    homeData: action.data
  });
};

function themeColorReducer(state = webThemeColorInitialstate, action) {
  //   switch (action.type) {
  //     case atRedux.:
  //       return setThemeColor(state, action);
  //     default:
  //       return state;
  //   }
  return state;
}

/* harmony default export */ var reducer_themeColorReducer = (themeColorReducer);
// CONCATENATED MODULE: ./website/storeWebsite/reducer/filterReducer.js
function filterReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function filterReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { filterReducer_ownKeys(Object(source), true).forEach(function (key) { filterReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { filterReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function filterReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const filterInisialState = {
  filterData: []
};
const setFilter = (state, action) => {
  if (state.filterData.length > 0) {
    const filter = state.filterData.filter(item => {
      if (item.type !== action.data.type) return item;
    });
    return filterReducer_objectSpread({}, state, {
      filterData: [...filter, action.data]
    });
  } else {
    return filterReducer_objectSpread({}, state, {
      filterData: [...state.filterData, action.data]
    });
  }
};
const removeFilter = (state, action) => {
  return filterReducer_objectSpread({}, state, {
    filterData: []
  });
};

function filterReducer(state = filterInisialState, action) {
  switch (action.type) {
    case redux["a" /* default */].SET_FILTER_TYPE:
      return setFilter(state, action);

    case redux["a" /* default */].REMOVE_FILTER_TYPE:
      return removeFilter();

    default:
      return state;
  }
}

/* harmony default export */ var reducer_filterReducer = (filterReducer);
// CONCATENATED MODULE: ./website/storeWebsite/reducer/index.js





const rootReducer = {
  error: reducer_errorReducer,
  home: reducer_homeReducer,
  themeColor: reducer_themeColorReducer,
  filter: reducer_filterReducer
};
/* harmony default export */ var reducer = (rootReducer);
// CONCATENATED MODULE: ./website/index.js



const website = {
  values: website_values,
  utils: website_utils,
  reducer: reducer
};
/* harmony default export */ var website_0 = __webpack_exports__["a"] = (website);

/***/ }),

/***/ "bzos":
/***/ (function(module, exports) {

module.exports = require("url");

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "cDf5":
/***/ (function(module, exports) {

function _typeof2(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

function _typeof(obj) {
  if (typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return _typeof2(obj);
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "cTJO":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__("TqRt");

var _interopRequireWildcard = __webpack_require__("284h");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__("cDcd"));

var _url = __webpack_require__("bzos");

var _utils = __webpack_require__("kYf9");

var _router = _interopRequireDefault(__webpack_require__("nOHt"));

var _router2 = __webpack_require__("elyg");

function isLocal(href) {
  var url = (0, _url.parse)(href, false, true);
  var origin = (0, _url.parse)((0, _utils.getLocationOrigin)(), false, true);
  return !url.host || url.protocol === origin.protocol && url.host === origin.host;
}

function memoizedFormatUrl(formatFunc) {
  var lastHref = null;
  var lastAs = null;
  var lastResult = null;
  return (href, as) => {
    if (lastResult && href === lastHref && as === lastAs) {
      return lastResult;
    }

    var result = formatFunc(href, as);
    lastHref = href;
    lastAs = as;
    lastResult = result;
    return result;
  };
}

function formatUrl(url) {
  return url && typeof url === 'object' ? (0, _utils.formatWithValidation)(url) : url;
}

var observer;
var listeners = new Map();
var IntersectionObserver = false ? undefined : null;
var prefetched = {};

function getObserver() {
  // Return shared instance of IntersectionObserver if already created
  if (observer) {
    return observer;
  } // Only create shared IntersectionObserver if supported in browser


  if (!IntersectionObserver) {
    return undefined;
  }

  return observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (!listeners.has(entry.target)) {
        return;
      }

      var cb = listeners.get(entry.target);

      if (entry.isIntersecting || entry.intersectionRatio > 0) {
        observer.unobserve(entry.target);
        listeners.delete(entry.target);
        cb();
      }
    });
  }, {
    rootMargin: '200px'
  });
}

var listenToIntersections = (el, cb) => {
  var observer = getObserver();

  if (!observer) {
    return () => {};
  }

  observer.observe(el);
  listeners.set(el, cb);
  return () => {
    try {
      observer.unobserve(el);
    } catch (err) {
      console.error(err);
    }

    listeners.delete(el);
  };
};

class Link extends _react.Component {
  constructor(props) {
    super(props);
    this.p = void 0;

    this.cleanUpListeners = () => {};

    this.formatUrls = memoizedFormatUrl((href, asHref) => {
      return {
        href: (0, _router2.addBasePath)(formatUrl(href)),
        as: asHref ? (0, _router2.addBasePath)(formatUrl(asHref)) : asHref
      };
    });

    this.linkClicked = e => {
      var {
        nodeName,
        target
      } = e.currentTarget;

      if (nodeName === 'A' && (target && target !== '_self' || e.metaKey || e.ctrlKey || e.shiftKey || e.nativeEvent && e.nativeEvent.which === 2)) {
        // ignore click for new tab / new window behavior
        return;
      }

      var {
        href,
        as
      } = this.formatUrls(this.props.href, this.props.as);

      if (!isLocal(href)) {
        // ignore click if it's outside our scope (e.g. https://google.com)
        return;
      }

      var {
        pathname
      } = window.location;
      href = (0, _url.resolve)(pathname, href);
      as = as ? (0, _url.resolve)(pathname, as) : href;
      e.preventDefault(); //  avoid scroll for urls with anchor refs

      var {
        scroll
      } = this.props;

      if (scroll == null) {
        scroll = as.indexOf('#') < 0;
      } // replace state instead of push if prop is present


      _router.default[this.props.replace ? 'replace' : 'push'](href, as, {
        shallow: this.props.shallow
      }).then(success => {
        if (!success) return;

        if (scroll) {
          window.scrollTo(0, 0);
          document.body.focus();
        }
      });
    };

    if (false) {}

    this.p = props.prefetch !== false;
  }

  componentWillUnmount() {
    this.cleanUpListeners();
  }

  getPaths() {
    var {
      pathname
    } = window.location;
    var {
      href: parsedHref,
      as: parsedAs
    } = this.formatUrls(this.props.href, this.props.as);
    var resolvedHref = (0, _url.resolve)(pathname, parsedHref);
    return [resolvedHref, parsedAs ? (0, _url.resolve)(pathname, parsedAs) : resolvedHref];
  }

  handleRef(ref) {
    if (this.p && IntersectionObserver && ref && ref.tagName) {
      this.cleanUpListeners();
      var isPrefetched = prefetched[this.getPaths().join( // Join on an invalid URI character
      '%')];

      if (!isPrefetched) {
        this.cleanUpListeners = listenToIntersections(ref, () => {
          this.prefetch();
        });
      }
    }
  } // The function is memoized so that no extra lifecycles are needed
  // as per https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html


  prefetch(options) {
    if (!this.p || true) return; // Prefetch the JSON page if asked (only in the client)

    var paths = this.getPaths(); // We need to handle a prefetch error here since we may be
    // loading with priority which can reject but we don't
    // want to force navigation since this is only a prefetch

    _router.default.prefetch(paths[
    /* href */
    0], paths[
    /* asPath */
    1], options).catch(err => {
      if (false) {}
    });

    prefetched[paths.join( // Join on an invalid URI character
    '%')] = true;
  }

  render() {
    var {
      children
    } = this.props;
    var {
      href,
      as
    } = this.formatUrls(this.props.href, this.props.as); // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

    if (typeof children === 'string') {
      children = _react.default.createElement("a", null, children);
    } // This will return the first child, if multiple are provided it will throw an error


    var child = _react.Children.only(children);

    var props = {
      ref: el => {
        this.handleRef(el);

        if (child && typeof child === 'object' && child.ref) {
          if (typeof child.ref === 'function') child.ref(el);else if (typeof child.ref === 'object') {
            child.ref.current = el;
          }
        }
      },
      onMouseEnter: e => {
        if (child.props && typeof child.props.onMouseEnter === 'function') {
          child.props.onMouseEnter(e);
        }

        this.prefetch({
          priority: true
        });
      },
      onClick: e => {
        if (child.props && typeof child.props.onClick === 'function') {
          child.props.onClick(e);
        }

        if (!e.defaultPrevented) {
          this.linkClicked(e);
        }
      }
    }; // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
    // defined, we specify the current 'href', so that repetition is not needed by the user

    if (this.props.passHref || child.type === 'a' && !('href' in child.props)) {
      props.href = as || href;
    } // Add the ending slash to the paths. So, we can serve the
    // "<page>/index.html" directly.


    if (false) { var rewriteUrlForNextExport; }

    return _react.default.cloneElement(child, props);
  }

}

if (false) { var exact, PropTypes, warn; }

var _default = Link;
exports.default = _default;

/***/ }),

/***/ "dZ6Y":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/*
MIT License

Copyright (c) Jason Miller (https://jasonformat.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

Object.defineProperty(exports, "__esModule", {
  value: true
});

function mitt() {
  const all = Object.create(null);
  return {
    on(type, handler) {
      ;
      (all[type] || (all[type] = [])).push(handler);
    },

    off(type, handler) {
      if (all[type]) {
        // tslint:disable-next-line:no-bitwise
        all[type].splice(all[type].indexOf(handler) >>> 0, 1);
      }
    },

    emit(type, ...evts) {
      // eslint-disable-next-line array-callback-return
      ;
      (all[type] || []).slice().map(handler => {
        handler(...evts);
      });
    }

  };
}

exports.default = mitt;

/***/ }),

/***/ "elyg":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__("bzos");

const mitt_1 = __importDefault(__webpack_require__("dZ6Y"));

const utils_1 = __webpack_require__("g/15");

const is_dynamic_1 = __webpack_require__("/jkW");

const route_matcher_1 = __webpack_require__("gguc");

const route_regex_1 = __webpack_require__("YTqd");

const basePath =  false || '';

function addBasePath(path) {
  return path.indexOf(basePath) !== 0 ? basePath + path : path;
}

exports.addBasePath = addBasePath;

function delBasePath(path) {
  return path.indexOf(basePath) === 0 ? path.substr(basePath.length) || '/' : path;
}

function toRoute(path) {
  return path.replace(/\/$/, '') || '/';
}

const prepareRoute = path => toRoute(!path || path === '/' ? '/index' : path);

function fetchNextData(pathname, query, isServerRender, cb) {
  let attempts = isServerRender ? 3 : 1;

  function getResponse() {
    return fetch(utils_1.formatWithValidation({
      // @ts-ignore __NEXT_DATA__
      pathname: `/_next/data/${__NEXT_DATA__.buildId}${pathname}.json`,
      query
    }), {
      // Cookies are required to be present for Next.js' SSG "Preview Mode".
      // Cookies may also be required for `getServerSideProps`.
      //
      // > `fetch` won’t send cookies, unless you set the credentials init
      // > option.
      // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
      //
      // > For maximum browser compatibility when it comes to sending &
      // > receiving cookies, always supply the `credentials: 'same-origin'`
      // > option instead of relying on the default.
      // https://github.com/github/fetch#caveats
      credentials: 'same-origin'
    }).then(res => {
      if (!res.ok) {
        if (--attempts > 0 && res.status >= 500) {
          return getResponse();
        }

        throw new Error(`Failed to load static props`);
      }

      return res.json();
    });
  }

  return getResponse().then(data => {
    return cb ? cb(data) : data;
  }).catch(err => {
    // We should only trigger a server-side transition if this was caused
    // on a client-side transition. Otherwise, we'd get into an infinite
    // loop.
    if (!isServerRender) {
      ;
      err.code = 'PAGE_LOAD_ERROR';
    }

    throw err;
  });
}

class Router {
  constructor(pathname, query, as, {
    initialProps,
    pageLoader,
    App,
    wrapApp,
    Component,
    err,
    subscription,
    isFallback
  }) {
    // Static Data Cache
    this.sdc = {};

    this.onPopState = e => {
      if (!e.state) {
        // We get state as undefined for two reasons.
        //  1. With older safari (< 8) and older chrome (< 34)
        //  2. When the URL changed with #
        //
        // In the both cases, we don't need to proceed and change the route.
        // (as it's already changed)
        // But we can simply replace the state with the new changes.
        // Actually, for (1) we don't need to nothing. But it's hard to detect that event.
        // So, doing the following for (1) does no harm.
        const {
          pathname,
          query
        } = this;
        this.changeState('replaceState', utils_1.formatWithValidation({
          pathname,
          query
        }), utils_1.getURL());
        return;
      } // Make sure we don't re-render on initial load,
      // can be caused by navigating back from an external site


      if (e.state && this.isSsr && e.state.as === this.asPath && url_1.parse(e.state.url).pathname === this.pathname) {
        return;
      } // If the downstream application returns falsy, return.
      // They will then be responsible for handling the event.


      if (this._bps && !this._bps(e.state)) {
        return;
      }

      const {
        url,
        as,
        options
      } = e.state;

      if (false) {}

      this.replace(url, as, options);
    };

    this._getStaticData = asPath => {
      const pathname = prepareRoute(url_1.parse(asPath).pathname);
      return  true && this.sdc[pathname] ? Promise.resolve(this.sdc[pathname]) : fetchNextData(pathname, null, this.isSsr, data => this.sdc[pathname] = data);
    };

    this._getServerData = asPath => {
      let {
        pathname,
        query
      } = url_1.parse(asPath, true);
      pathname = prepareRoute(pathname);
      return fetchNextData(pathname, query, this.isSsr);
    }; // represents the current component key


    this.route = toRoute(pathname); // set up the component cache (by route keys)

    this.components = {}; // We should not keep the cache, if there's an error
    // Otherwise, this cause issues when when going back and
    // come again to the errored page.

    if (pathname !== '/_error') {
      this.components[this.route] = {
        Component,
        props: initialProps,
        err,
        __N_SSG: initialProps && initialProps.__N_SSG,
        __N_SSP: initialProps && initialProps.__N_SSP
      };
    }

    this.components['/_app'] = {
      Component: App
    }; // Backwards compat for Router.router.events
    // TODO: Should be remove the following major version as it was never documented

    this.events = Router.events;
    this.pageLoader = pageLoader;
    this.pathname = pathname;
    this.query = query; // if auto prerendered and dynamic route wait to update asPath
    // until after mount to prevent hydration mismatch

    this.asPath = // @ts-ignore this is temporarily global (attached to window)
    is_dynamic_1.isDynamicRoute(pathname) && __NEXT_DATA__.autoExport ? pathname : as;
    this.basePath = basePath;
    this.sub = subscription;
    this.clc = null;
    this._wrapApp = wrapApp; // make sure to ignore extra popState in safari on navigating
    // back from external site

    this.isSsr = true;
    this.isFallback = isFallback;

    if (false) {}
  } // @deprecated backwards compatibility even though it's a private method.


  static _rewriteUrlForNextExport(url) {
    if (false) {} else {
      return url;
    }
  }

  update(route, mod) {
    const Component = mod.default || mod;
    const data = this.components[route];

    if (!data) {
      throw new Error(`Cannot update unavailable route: ${route}`);
    }

    const newData = Object.assign(Object.assign({}, data), {
      Component,
      __N_SSG: mod.__N_SSG,
      __N_SSP: mod.__N_SSP
    });
    this.components[route] = newData; // pages/_app.js updated

    if (route === '/_app') {
      this.notify(this.components[this.route]);
      return;
    }

    if (route === this.route) {
      this.notify(newData);
    }
  }

  reload() {
    window.location.reload();
  }
  /**
   * Go back in history
   */


  back() {
    window.history.back();
  }
  /**
   * Performs a `pushState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  push(url, as = url, options = {}) {
    return this.change('pushState', url, as, options);
  }
  /**
   * Performs a `replaceState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  replace(url, as = url, options = {}) {
    return this.change('replaceState', url, as, options);
  }

  change(method, _url, _as, options) {
    return new Promise((resolve, reject) => {
      if (!options._h) {
        this.isSsr = false;
      } // marking route changes as a navigation start entry


      if (utils_1.ST) {
        performance.mark('routeChange');
      } // If url and as provided as an object representation,
      // we'll format them into the string version here.


      let url = typeof _url === 'object' ? utils_1.formatWithValidation(_url) : _url;
      let as = typeof _as === 'object' ? utils_1.formatWithValidation(_as) : _as;
      url = addBasePath(url);
      as = addBasePath(as); // Add the ending slash to the paths. So, we can serve the
      // "<page>/index.html" directly for the SSR page.

      if (false) {}

      this.abortComponentLoad(as); // If the url change is only related to a hash change
      // We should not proceed. We should only change the state.
      // WARNING: `_h` is an internal option for handing Next.js client-side
      // hydration. Your app should _never_ use this property. It may change at
      // any time without notice.

      if (!options._h && this.onlyAHashChange(as)) {
        this.asPath = as;
        Router.events.emit('hashChangeStart', as);
        this.changeState(method, url, as, options);
        this.scrollToHash(as);
        Router.events.emit('hashChangeComplete', as);
        return resolve(true);
      }

      const {
        pathname,
        query,
        protocol
      } = url_1.parse(url, true);

      if (!pathname || protocol) {
        if (false) {}

        return resolve(false);
      } // If asked to change the current URL we should reload the current page
      // (not location.reload() but reload getInitialProps and other Next.js stuffs)
      // We also need to set the method = replaceState always
      // as this should not go into the history (That's how browsers work)
      // We should compare the new asPath to the current asPath, not the url


      if (!this.urlIsNew(as)) {
        method = 'replaceState';
      }

      const route = toRoute(pathname);
      const {
        shallow = false
      } = options;

      if (is_dynamic_1.isDynamicRoute(route)) {
        const {
          pathname: asPathname
        } = url_1.parse(as);
        const routeRegex = route_regex_1.getRouteRegex(route);
        const routeMatch = route_matcher_1.getRouteMatcher(routeRegex)(asPathname);

        if (!routeMatch) {
          const missingParams = Object.keys(routeRegex.groups).filter(param => !query[param]);

          if (missingParams.length > 0) {
            if (false) {}

            return reject(new Error(`The provided \`as\` value (${asPathname}) is incompatible with the \`href\` value (${route}). ` + `Read more: https://err.sh/zeit/next.js/incompatible-href-as`));
          }
        } else {
          // Merge params into `query`, overwriting any specified in search
          Object.assign(query, routeMatch);
        }
      }

      Router.events.emit('routeChangeStart', as); // If shallow is true and the route exists in the router cache we reuse the previous result

      this.getRouteInfo(route, pathname, query, as, shallow).then(routeInfo => {
        const {
          error
        } = routeInfo;

        if (error && error.cancelled) {
          return resolve(false);
        }

        Router.events.emit('beforeHistoryChange', as);
        this.changeState(method, url, as, options);

        if (false) {}

        this.set(route, pathname, query, as, routeInfo);

        if (error) {
          Router.events.emit('routeChangeError', error, as);
          throw error;
        }

        Router.events.emit('routeChangeComplete', as);
        return resolve(true);
      }, reject);
    });
  }

  changeState(method, url, as, options = {}) {
    if (false) {}

    if (method !== 'pushState' || utils_1.getURL() !== as) {
      window.history[method]({
        url,
        as,
        options
      }, // Most browsers currently ignores this parameter, although they may use it in the future.
      // Passing the empty string here should be safe against future changes to the method.
      // https://developer.mozilla.org/en-US/docs/Web/API/History/replaceState
      '', as);
    }
  }

  getRouteInfo(route, pathname, query, as, shallow = false) {
    const cachedRouteInfo = this.components[route]; // If there is a shallow route transition possible
    // If the route is already rendered on the screen.

    if (shallow && cachedRouteInfo && this.route === route) {
      return Promise.resolve(cachedRouteInfo);
    }

    const handleError = (err, loadErrorFail) => {
      return new Promise(resolve => {
        if (err.code === 'PAGE_LOAD_ERROR' || loadErrorFail) {
          // If we can't load the page it could be one of following reasons
          //  1. Page doesn't exists
          //  2. Page does exist in a different zone
          //  3. Internal error while loading the page
          // So, doing a hard reload is the proper way to deal with this.
          window.location.href = as; // Changing the URL doesn't block executing the current code path.
          // So, we need to mark it as a cancelled error and stop the routing logic.

          err.cancelled = true; // @ts-ignore TODO: fix the control flow here

          return resolve({
            error: err
          });
        }

        if (err.cancelled) {
          // @ts-ignore TODO: fix the control flow here
          return resolve({
            error: err
          });
        }

        resolve(this.fetchComponent('/_error').then(res => {
          const {
            page: Component
          } = res;
          const routeInfo = {
            Component,
            err
          };
          return new Promise(resolve => {
            this.getInitialProps(Component, {
              err,
              pathname,
              query
            }).then(props => {
              routeInfo.props = props;
              routeInfo.error = err;
              resolve(routeInfo);
            }, gipErr => {
              console.error('Error in error page `getInitialProps`: ', gipErr);
              routeInfo.error = err;
              routeInfo.props = {};
              resolve(routeInfo);
            });
          });
        }).catch(err => handleError(err, true)));
      });
    };

    return new Promise((resolve, reject) => {
      if (cachedRouteInfo) {
        return resolve(cachedRouteInfo);
      }

      this.fetchComponent(route).then(res => resolve({
        Component: res.page,
        __N_SSG: res.mod.__N_SSG,
        __N_SSP: res.mod.__N_SSP
      }), reject);
    }).then(routeInfo => {
      const {
        Component,
        __N_SSG,
        __N_SSP
      } = routeInfo;

      if (false) {}

      return this._getData(() => __N_SSG ? this._getStaticData(as) : __N_SSP ? this._getServerData(as) : this.getInitialProps(Component, // we provide AppTree later so this needs to be `any`
      {
        pathname,
        query,
        asPath: as
      })).then(props => {
        routeInfo.props = props;
        this.components[route] = routeInfo;
        return routeInfo;
      });
    }).catch(handleError);
  }

  set(route, pathname, query, as, data) {
    this.isFallback = false;
    this.route = route;
    this.pathname = pathname;
    this.query = query;
    this.asPath = as;
    this.notify(data);
  }
  /**
   * Callback to execute before replacing router state
   * @param cb callback to be executed
   */


  beforePopState(cb) {
    this._bps = cb;
  }

  onlyAHashChange(as) {
    if (!this.asPath) return false;
    const [oldUrlNoHash, oldHash] = this.asPath.split('#');
    const [newUrlNoHash, newHash] = as.split('#'); // Makes sure we scroll to the provided hash if the url/hash are the same

    if (newHash && oldUrlNoHash === newUrlNoHash && oldHash === newHash) {
      return true;
    } // If the urls are change, there's more than a hash change


    if (oldUrlNoHash !== newUrlNoHash) {
      return false;
    } // If the hash has changed, then it's a hash only change.
    // This check is necessary to handle both the enter and
    // leave hash === '' cases. The identity case falls through
    // and is treated as a next reload.


    return oldHash !== newHash;
  }

  scrollToHash(as) {
    const [, hash] = as.split('#'); // Scroll to top if the hash is just `#` with no value

    if (hash === '') {
      window.scrollTo(0, 0);
      return;
    } // First we check if the element by id is found


    const idEl = document.getElementById(hash);

    if (idEl) {
      idEl.scrollIntoView();
      return;
    } // If there's no element with the id, we check the `name` property
    // To mirror browsers


    const nameEl = document.getElementsByName(hash)[0];

    if (nameEl) {
      nameEl.scrollIntoView();
    }
  }

  urlIsNew(asPath) {
    return this.asPath !== asPath;
  }
  /**
   * Prefetch page code, you may wait for the data during page rendering.
   * This feature only works in production!
   * @param url the href of prefetched page
   * @param asPath the as path of the prefetched page
   */


  prefetch(url, asPath = url, options = {}) {
    return new Promise((resolve, reject) => {
      const {
        pathname,
        protocol
      } = url_1.parse(url);

      if (!pathname || protocol) {
        if (false) {}

        return;
      } // Prefetch is not supported in development mode because it would trigger on-demand-entries


      if (false) {}

      const route = delBasePath(toRoute(pathname));
      Promise.all([this.pageLoader.prefetchData(url, delBasePath(asPath)), this.pageLoader[options.priority ? 'loadPage' : 'prefetch'](route)]).then(() => resolve(), reject);
    });
  }

  async fetchComponent(route) {
    let cancelled = false;

    const cancel = this.clc = () => {
      cancelled = true;
    };

    route = delBasePath(route);
    const componentResult = await this.pageLoader.loadPage(route);

    if (cancelled) {
      const error = new Error(`Abort fetching component for route: "${route}"`);
      error.cancelled = true;
      throw error;
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    return componentResult;
  }

  _getData(fn) {
    let cancelled = false;

    const cancel = () => {
      cancelled = true;
    };

    this.clc = cancel;
    return fn().then(data => {
      if (cancel === this.clc) {
        this.clc = null;
      }

      if (cancelled) {
        const err = new Error('Loading initial props cancelled');
        err.cancelled = true;
        throw err;
      }

      return data;
    });
  }

  getInitialProps(Component, ctx) {
    const {
      Component: App
    } = this.components['/_app'];

    const AppTree = this._wrapApp(App);

    ctx.AppTree = AppTree;
    return utils_1.loadGetInitialProps(App, {
      AppTree,
      Component,
      router: this,
      ctx
    });
  }

  abortComponentLoad(as) {
    if (this.clc) {
      const e = new Error('Route Cancelled');
      e.cancelled = true;
      Router.events.emit('routeChangeError', e, as);
      this.clc();
      this.clc = null;
    }
  }

  notify(data) {
    this.sub(data, this.components['/_app'].Component);
  }

}

exports.default = Router;
Router.events = mitt_1.default();

/***/ }),

/***/ "g/15":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__("bzos");
/**
 * Utils
 */


function execOnce(fn) {
  let used = false;
  let result;
  return (...args) => {
    if (!used) {
      used = true;
      result = fn(...args);
    }

    return result;
  };
}

exports.execOnce = execOnce;

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

exports.getLocationOrigin = getLocationOrigin;

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

exports.getURL = getURL;

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

exports.getDisplayName = getDisplayName;

function isResSent(res) {
  return res.finished || res.headersSent;
}

exports.isResSent = isResSent;

async function loadGetInitialProps(App, ctx) {
  var _a;

  if (false) {} // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!App.getInitialProps) {
    if (ctx.ctx && ctx.Component) {
      // @ts-ignore pageProps default
      return {
        pageProps: await loadGetInitialProps(ctx.Component, ctx.ctx)
      };
    }

    return {};
  }

  const props = await App.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(App)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (false) {}

  return props;
}

exports.loadGetInitialProps = loadGetInitialProps;
exports.urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];

function formatWithValidation(url, options) {
  if (false) {}

  return url_1.format(url, options);
}

exports.formatWithValidation = formatWithValidation;
exports.SP = typeof performance !== 'undefined';
exports.ST = exports.SP && typeof performance.mark === 'function' && typeof performance.measure === 'function';

/***/ }),

/***/ "gfwP":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
 // import "./index.scss";

const PlayIcon = ({
  style,
  className
}) => {
  return __jsx("div", {
    className: `icon-wrapper ${className}`
  }, __jsx("i", {
    className: "fas fa-play"
  }));
};

/* harmony default export */ __webpack_exports__["a"] = (PlayIcon);

/***/ }),

/***/ "gguc":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteMatcher(routeRegex) {
  const {
    re,
    groups
  } = routeRegex;
  return pathname => {
    const routeMatch = re.exec(pathname);

    if (!routeMatch) {
      return false;
    }

    const decode = param => {
      try {
        return decodeURIComponent(param);
      } catch (_) {
        const err = new Error('failed to decode param');
        err.code = 'DECODE_FAILED';
        throw err;
      }
    };

    const params = {};
    Object.keys(groups).forEach(slugName => {
      const g = groups[slugName];
      const m = routeMatch[g.pos];

      if (m !== undefined) {
        params[slugName] = ~m.indexOf('/') ? m.split('/').map(entry => decode(entry)) : g.repeat ? [decode(m)] : decode(m);
      }
    });
    return params;
  };
}

exports.getRouteMatcher = getRouteMatcher;

/***/ }),

/***/ "h74D":
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "jl0i":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// NAMESPACE OBJECT: ./website/storeWebsite/actions/redux/index.js
var redux_namespaceObject = {};
__webpack_require__.r(redux_namespaceObject);
__webpack_require__.d(redux_namespaceObject, "increment", function() { return increment; });
__webpack_require__.d(redux_namespaceObject, "setHomeData", function() { return setHomeData; });
__webpack_require__.d(redux_namespaceObject, "setFailure", function() { return setFailure; });
__webpack_require__.d(redux_namespaceObject, "filterType", function() { return redux_filterType; });
__webpack_require__.d(redux_namespaceObject, "removeFilterType", function() { return removeFilterType; });

// NAMESPACE OBJECT: ./website/storeWebsite/actions/saga/index.js
var actions_saga_namespaceObject = {};
__webpack_require__.r(actions_saga_namespaceObject);
__webpack_require__.d(actions_saga_namespaceObject, "getHomeData", function() { return getHomeData; });

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: ./website/components/cards/cardElements/MusicCard/index.js
var MusicCard = __webpack_require__("VkSw");

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__("zr5I");
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// EXTERNAL MODULE: ./website/index.js + 24 modules
var website = __webpack_require__("akFz");

// EXTERNAL MODULE: ./public/assets/images/placeholder/albumPlaceholder.png
var albumPlaceholder = __webpack_require__("Lhaj");
var albumPlaceholder_default = /*#__PURE__*/__webpack_require__.n(albumPlaceholder);

// EXTERNAL MODULE: external "react-infinite-scroller"
var external_react_infinite_scroller_ = __webpack_require__("WLqv");
var external_react_infinite_scroller_default = /*#__PURE__*/__webpack_require__.n(external_react_infinite_scroller_);

// CONCATENATED MODULE: ./website/api/axios-orders.js
 // import Cookie from "js-cookie";

const instance = external_axios_default.a.create({
  baseURL: "https://rimtal.com/api/v1"
});
instance.defaults.headers.common["Authorization"] = "Bearer ";
/* harmony default export */ var axios_orders = (instance);
// CONCATENATED MODULE: ./website/api/POST/filterType.js

const filteredData = async filterType => axios_orders.post("albums/1", {
  filters: [filterType]
}).then(res => {
  return {
    data: res.data
  };
}).catch(e => {
  return {
    error: e
  };
});
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__("h74D");

// CONCATENATED MODULE: ./website/screen/AlbumScreen/AlbumContainer/index.js
var __jsx = external_react_default.a.createElement;









const AlbumContainer = () => {
  const {
    0: data,
    1: setData
  } = Object(external_react_["useState"])([]);
  const {
    0: fData,
    1: setFData
  } = Object(external_react_["useState"])(null);
  const {
    0: serverData,
    1: setServerData
  } = Object(external_react_["useState"])([]);
  const ref = Object(external_react_["useRef"])(null);
  const store = Object(external_react_redux_["useSelector"])(state => {
    return state.filter;
  });
  console.log({
    store
  });
  const filterType = {
    type: "time",
    from: "2020-01-01",
    days: 30
  };
  console.log({
    fData
  });
  Object(external_react_["useEffect"])(() => {
    postData();
  }, []);

  const postData = async () => {
    const res = await filteredData(filterType);
    res.data && setFData(res.data);
    res.error && setFData(res.error);
  };

  const getData = async (page = 1) => {
    // console.log(`=========== GET PAGE = ${page} =============`);
    const res = await external_axios_default.a.get(`https://rimtal.com/api/v1/albums/${page}`);

    if (res.data.docs.length > 0) {
      const newArman = data.concat(res.data.docs);
      setData(newArman);
      setServerData(res.data);
    } else {
      console.log("Nothing found");
    }
  };

  const newData = website["a" /* default */].utils.convert.albumCard(data);

  const loadMore = e => {
    if (e > (serverData === null || serverData === void 0 ? void 0 : serverData.pages)) {
      return;
    } else {
      getData(e);
    }
  };

  return __jsx("main", {
    className: "main"
  }, __jsx("div", {
    className: "data-main-wrapper",
    ref: ref
  }, __jsx(external_react_infinite_scroller_default.a, {
    className: "flexing",
    pageStart: 0,
    loadMore: loadMore,
    hasMore: (serverData === null || serverData === void 0 ? void 0 : serverData.page) ? parseInt(serverData.page) < serverData.pages : true
  }, newData && newData.map((info, index) => {
    return __jsx(MusicCard["a" /* default */], {
      key: "AlbumCard-" + index,
      data: info,
      placeholder: albumPlaceholder_default.a,
      parentClass: " col-lg-2 col-md-3 col-6 px-0"
    });
  }))));
};

/* harmony default export */ var AlbumScreen_AlbumContainer = (AlbumContainer);
// CONCATENATED MODULE: ./website/baseComponents/cards/AlbumFilterCard/index.js
var AlbumFilterCard_jsx = external_react_default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const AlbumFilterCard = ({
  filters,
  onFilterPress
}) => {
  //fot setting the title
  console.log({
    FOX: filters
  });
  const {
    0: titleNameState,
    1: setTitleNameState
  } = Object(external_react_["useState"])(filters.title); //for handling title name on change

  const changeHandler = title => {
    const filterData = {
      type: filters.title,
      value: title
    };
    onFilterPress(filterData);
    setTitleNameState(title);
  }; //for set dropdown to true and false


  const {
    0: dropclass,
    1: setDropClass
  } = Object(external_react_["useState"])({
    showDropDown: false,
    clickedComponent: false
  });
  const wrapperRef = Object(external_react_["useRef"])(null); //for handling dropdown menu on click

  const onclickHandler = () => {
    setDropClass(prev => _objectSpread({}, prev, {
      showDropDown: !dropclass.showDropDown,
      clickedComponent: true
    }));
  }; //for handling drop down menu on click on another element


  const outsideClickHandler = e => {
    if (wrapperRef.current && !wrapperRef.current.contains(e.target)) {
      onclickHandler();
    }
  }; //for handling the click in window


  Object(external_react_["useEffect"])(() => {
    if (dropclass.showDropDown) {
      document.addEventListener("click", outsideClickHandler);
      return () => {
        document.removeEventListener("click", outsideClickHandler);
      };
    }
  });
  return AlbumFilterCard_jsx("div", {
    className: "collapse navbar-collapse",
    id: "collapse-show"
  }, AlbumFilterCard_jsx("li", {
    className: dropclass.showDropDown ? "drp" : dropclass.clickedComponent ? "drp-deactive" : ""
  }, AlbumFilterCard_jsx("div", {
    onClick: () => {
      onclickHandler();
    },
    ref: wrapperRef,
    className: "title-wrapper"
  }, AlbumFilterCard_jsx("span", null, titleNameState), AlbumFilterCard_jsx("i", {
    className: "fal fa-chevron-down"
  })), AlbumFilterCard_jsx("ul", {
    className: "drop-down-wrapper"
  }, filters.child.map(chil => {
    return AlbumFilterCard_jsx("li", {
      className: `${chil.subTitle === titleNameState ? "active" : ""}`,
      onClick: () => changeHandler(chil.subTitle)
    }, chil.subTitle);
  }))));
};

/* harmony default export */ var cards_AlbumFilterCard = (AlbumFilterCard);
// EXTERNAL MODULE: ./website/storeWebsite/actionTypes/redux/index.js
var redux = __webpack_require__("lmmF");

// CONCATENATED MODULE: ./website/storeWebsite/actions/redux/index.js

function increment() {
  return {
    type: redux["a" /* default */].INCREMENT
  };
} // // =================================================== NAVBAR
// =================================================== HOME

function setHomeData(data) {
  return {
    type: redux["a" /* default */].SET_HOME_DATA,
    data
  };
} // =================================================== ERROR

function setFailure(error) {
  return {
    type: redux["a" /* default */].ADD_FAILURE,
    error
  };
}
function redux_filterType(type) {
  return {
    type: redux["a" /* default */].SET_FILTER_TYPE,
    data: type
  };
}
function removeFilterType() {
  return {
    type: redux["a" /* default */].REMOVE_FILTER_TYPE
  };
}
// CONCATENATED MODULE: ./website/storeWebsite/actionTypes/saga/index.js
const atSaga = {
  GET_HOME_SCREEN_DATA: "GET_HOME_SCREEN_DATA_SAGA"
};
/* harmony default export */ var saga = (atSaga);
// CONCATENATED MODULE: ./website/storeWebsite/actions/saga/index.js

function getHomeData() {
  console.log("getHomeData");
  return {
    type: saga.GET_HOME_SCREEN_DATA
  };
}
// CONCATENATED MODULE: ./website/storeWebsite/actions/index.js


const actions = {
  reduxActions: redux_namespaceObject,
  sagaActions: actions_saga_namespaceObject
};
/* harmony default export */ var storeWebsite_actions = (actions);
// CONCATENATED MODULE: ./website/api/Get/getAlbumsFilter.js

const getAlbumsFilter = () => {
  return axios_orders.get("/").then(res => {
    return {
      data: res.data
    };
  }).catch(e => {
    return {
      error: e
    };
  });
};
// CONCATENATED MODULE: ./website/screen/AlbumScreen/AlbumFilterContainer/index.js
var AlbumFilterContainer_jsx = external_react_default.a.createElement;






const AlbumFilterContainer = () => {
  const dispatch = Object(external_react_redux_["useDispatch"])();
  const filters = [{
    title: "All",
    child: [{
      subTitle: "All"
    }, {
      subTitle: "1week"
    }, {
      subTitle: "1month"
    }]
  }, {
    title: "Type",
    child: [{
      subTitle: "AllType"
    }, {
      subTitle: "most popular"
    }, {
      subTitle: "most played"
    }, {
      subTitle: "most liked"
    }, {
      subTitle: "most of rate"
    }, {
      subTitle: "album sales"
    }]
  }, {
    title: "All Genre",
    child: [{
      subTitle: "rock"
    }, {
      subTitle: "instrumental"
    }, {
      subTitle: "alternative"
    }, {
      subTitle: "trip hop"
    }, {
      subTitle: "hip hop"
    }, {
      subTitle: "pop"
    }, {
      subTitle: "electronic"
    }]
  }, {
    title: "All Instrument",
    child: [{
      subTitle: "piano"
    }, {
      subTitle: "guitar"
    }, {
      subTitle: "guitar electric"
    }, {
      subTitle: "setar"
    }]
  }, {
    title: "All hashtag",
    child: [{
      subTitle: "music"
    }, {
      subTitle: "rock"
    }, {
      subTitle: "rap"
    }]
  }, {
    title: "All mood",
    child: [{
      subTitle: "relax"
    }, {
      subTitle: "sleeppy"
    }, {
      subTitle: "chill"
    }]
  }];
  Object(external_react_["useEffect"])(() => {
    getfilerData();
  }, []);

  const getfilerData = async () => {
    const res = await getAlbumsFilter();
    console.log({
      FOXRES: res
    });
  };

  const onFilterPress = filter => {
    if (filter.type === "All") {
      dispatch(storeWebsite_actions.reduxActions.removeFilterType());
    } else {
      dispatch(storeWebsite_actions.reduxActions.filterType(filter));
    }
  };

  const renderItems = () => {
    console.log({
      Mehrad: filters
    });
    return filters.map(filter => {
      return AlbumFilterContainer_jsx(cards_AlbumFilterCard, {
        onFilterPress: onFilterPress,
        filters: filter
      });
    });
  };

  return AlbumFilterContainer_jsx("div", {
    className: "albums-container"
  }, AlbumFilterContainer_jsx("div", {
    className: "albums-header-wrappper navbar-expand-lg"
  }, AlbumFilterContainer_jsx("h1", null, "Albums"), AlbumFilterContainer_jsx("ul", {
    className: "albums-nav flex-column flex-lg-row"
  }, AlbumFilterContainer_jsx("button", {
    class: "navbar-toggler",
    type: "button",
    "data-toggle": "collapse",
    "data-target": "#collapse-show",
    "aria-controls": "navbarSupportedContent",
    "aria-expanded": "false",
    "aria-label": "Toggle navigation"
  }, AlbumFilterContainer_jsx("i", {
    class: "far fa-bars"
  })), renderItems())));
};

/* harmony default export */ var AlbumScreen_AlbumFilterContainer = (AlbumFilterContainer);
// CONCATENATED MODULE: ./website/screen/AlbumScreen/index.js
var AlbumScreen_jsx = external_react_default.a.createElement;




const AlbumScreen = () => {
  return AlbumScreen_jsx(external_react_default.a.Fragment, null, AlbumScreen_jsx(AlbumScreen_AlbumFilterContainer, null), AlbumScreen_jsx(AlbumScreen_AlbumContainer, null));
};

/* harmony default export */ var screen_AlbumScreen = (AlbumScreen);
// CONCATENATED MODULE: ./pages/albums.js
var albums_jsx = external_react_default.a.createElement;



const Albums = () => {
  return albums_jsx("div", {
    className: "row-container padding-top"
  }, albums_jsx(screen_AlbumScreen, null));
};

/* harmony default export */ var albums = __webpack_exports__["default"] = (Albums);

/***/ }),

/***/ "kYf9":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/utils.js");

/***/ }),

/***/ "lmmF":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const atRedux = {
  //======================================================== Redux
  SET_HOME_DATA: "SET_HOME_DATA_REDUX",
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX",
  SET_FILTER_TYPE: "SET_FILTER_TYPE_REDUX",
  REMOVE_FILTER_TYPE: "REMOVE_FILTER_TYPE_REDUX"
};
/* harmony default export */ __webpack_exports__["a"] = (atRedux);

/***/ }),

/***/ "nOHt":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__("284h");

var _interopRequireDefault = __webpack_require__("TqRt");

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__("cDcd"));

var _router2 = _interopRequireWildcard(__webpack_require__("elyg"));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__("Osoz");

var _withRouter = _interopRequireDefault(__webpack_require__("0Bsm"));

exports.withRouter = _withRouter.default;
/* global window */

var singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

var urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components', 'isFallback', 'basePath'];
var routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
var coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

Object.defineProperty(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  Object.defineProperty(singletonRouter, field, {
    get() {
      var router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = function () {
    var router = getRouter();
    return router[field](...arguments);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, function () {
      var eventField = "on" + event.charAt(0).toUpperCase() + event.substring(1);
      var _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...arguments);
        } catch (err) {
          // tslint:disable-next-line:no-console
          console.error("Error when running the Router event: " + eventField); // tslint:disable-next-line:no-console

          console.error(err.message + "\n" + err.stack);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    var message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


var createRouter = function createRouter() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  var _router = router;
  var instance = {};

  for (var property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = Object.assign({}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = function () {
      return _router[field](...arguments);
    };
  });
  return instance;
}

/***/ }),

/***/ "oviB":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const HeartIcon = ({
  style
}) => {
  const {
    0: actived,
    1: setActived
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);

  const _handelClick = () => {
    setActived(!actived);
  };

  return __jsx("div", {
    onClick: _handelClick,
    style: style,
    className: "icon-wrapper"
  }, actived ? __jsx("i", {
    style: {
      color: "red"
    },
    className: "fas fa-heart"
  }) : __jsx("i", {
    className: "far fa-heart"
  }));
};

/* harmony default export */ __webpack_exports__["a"] = (HeartIcon);

/***/ }),

/***/ "q7pC":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export LazyImage */
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("HJQg");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;

const LazyImage = ({
  src,
  alt,
  defaultImage,
  imageOnload
}) => {
  let placeHolder = defaultImage ? defaultImage : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSP-Ze14CTqFzlYouUOJ7OIGAN14YRJrmT1EwYEhsJTBrC92fhtTQ&s";
  const {
    0: imageSrc,
    1: setImageSrc
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(placeHolder); //   const [imageRef, setImageRef] = useState();
  //   console.log({ imageSrc });

  const imageRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  const onLoad = event => {
    // console.log({ onLoad: event });
    event.target.classList.add("loaded"); // imageOnload();
  };

  const onError = event => {
    // console.log({ error: event });
    event.target.classList.add("has-error");
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    let observer;
    let didCancel = false;

    if (imageRef.current && imageSrc !== src) {
      if (IntersectionObserver) {
        observer = new IntersectionObserver(entries => {
          // console.log({ observer, entries });
          entries.forEach(entry => {
            if (!didCancel && (entry.intersectionRatio > 0 || entry.isIntersecting)) {
              setImageSrc(src);
              observer.unobserve(imageRef.current) && observer.unobserve(imageRef.current);
            }
          });
        }, {
          threshold: 0.01,
          rootMargin: "75%"
        });
        observer.observe(imageRef.current);
      } else {
        // Old browsers fallback
        setImageSrc(src);
      }
    }

    return () => {
      didCancel = true; // on component cleanup, we remove the listner

      if (observer && observer.unobserve) {
        observer.unobserve(imageRef.current);
      }
    };
  }, [src, imageSrc, imageRef.current]); //   return <Image ref={setImageRef} src={imageSrc} alt={alt} onLoad={onLoad} onError={onError} />;

  return __jsx(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, __jsx("img", {
    ref: imageRef,
    src: imageSrc ? imageSrc : placeHolder,
    alt: alt,
    onLoadCapture: onLoad,
    onError: onError,
    className: styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a.dynamic([["3389864842", [placeHolder]]]) + " " + "imageComponent"
  }), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    id: "3389864842",
    dynamic: [placeHolder]
  }, [".imageComponent.__jsx-style-dynamic-selector{display:block;}", ".imageComponent.loaded.__jsx-style-dynamic-selector{-webkit-animation:loaded-__jsx-style-dynamic-selector 0.35s ease-in-out;animation:loaded-__jsx-style-dynamic-selector 0.35s ease-in-out;}", `.imageComponent.has-error.__jsx-style-dynamic-selector{content:url(${placeHolder});width:100%;height:100%;}`, ".image-lazyloading-container.__jsx-style-dynamic-selector{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;width:100%;height:100%;background-color:#ccc;}", "@-webkit-keyframes loaded-__jsx-style-dynamic-selector{0%{opacity:0.7;}100%{opacity:1;}}", "@keyframes loaded-__jsx-style-dynamic-selector{0%{opacity:0.7;}100%{opacity:1;}}"]));
};
/* harmony default export */ __webpack_exports__["a"] = (LazyImage);

/***/ }),

/***/ "rKB8":
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),

/***/ "rYez":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const DotIcon = ({
  click,
  overflowUnset
}) => {
  const wrapperRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  const modalData = [{
    icon: "far fa-stream",
    title: "Add To Quque"
  }, {
    icon: "far fa-album",
    title: "Go To Album"
  }, {
    icon: "fal fa-plus",
    title: "Add To Playlist",
    child: []
  }, {
    icon: "fas fa-share",
    title: "Share",
    child: []
  }];
  const {
    0: modal,
    1: setModal
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    show: false,
    data: modalData
  });

  const toggleDotIconClick = () => {
    console.log("toggleDotIconClick"); // console.log({ wrapperRef: wrapperRef.current });

    setModal(prev => _objectSpread({}, prev, {
      show: !prev.show
    }));
  };

  const handleClickOutside = event => {
    if (modal.show) if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      toggleDotIconClick();
    } //  else if (wrapperRef.current && wrapperRef.current.contains(event.target)) {
    //   console.log({ handleClickOutside: event, screenX: event.screenX, screenY: event.screenY });
    // }
  };

  const showModal = event => {
    console.log({
      showModal: event
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    document.addEventListener("click", handleClickOutside);
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  });

  const modalElement = __jsx("div", {
    className: `small-modal-card-container ${modal && modal.show ? "showModalElement" : ""}`
  }, __jsx("div", {
    className: "small-modal-card-wrapper"
  }, modal && modal.data.map((data, index) => {
    return __jsx("div", {
      key: "modal-" + index,
      className: "modal-element-details"
    }, __jsx("i", {
      className: data.icon
    }), __jsx("div", {
      className: "title-arrow"
    }, __jsx("div", {
      className: "small-modal-card-title"
    }, data.title), data.child ? __jsx("div", {
      className: "chevron-arrow-dropDown"
    }, __jsx("i", {
      className: "far fa-chevron-right"
    })) : ""));
  })));

  const moseleave = () => {
    if (modal.show) toggleDotIconClick();
  };

  return __jsx("div", {
    onMouseLeave: moseleave,
    ref: wrapperRef,
    className: " icon-ellipsis relative centerAll pointer",
    style: {
      color: "white"
    }
  }, __jsx("div", {
    onClick: toggleDotIconClick,
    className: ""
  }, __jsx("i", {
    className: "far fa-ellipsis-h"
  })), modalElement);
};

/* harmony default export */ __webpack_exports__["a"] = (DotIcon);

/***/ }),

/***/ "vEqB":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
  mainColor: "#0070ef",
  accentColor: "#666666",
  white: "#ffff",
  black: "#000",
  textColor: "#666666"
});

/***/ }),

/***/ "wtyS":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./website/values/strings/en/navbar.js
const APP_NAME = "Rimtal";
const EXPLORE = "Explore";
const TRACKS = "Tracks";
const PLAYLISTS = "Playlists";
const ALBUMS = "Albums";
const ARTISTS = "Artists";
const VIDEOS = "Videos";
const SIGN_IN = "Sign In";
const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN
};
/* harmony default export */ var en_navbar = (navbar);
// CONCATENATED MODULE: ./website/values/strings/en/homePage.js
const VIEW_ALL = "View All";
const SINGLES = "Singles";
const COMING_SOON = "Coming Soon";
const PLAY_LISTS = "Playlists";
const homePage_VIDEOS = "Videos";
const PLAYLIST_FOR_AUTUMN = "Playlist For Autumn";
const MOOD = "Mood";
const COMMING_SOON = "Comming Soon";
const homePage = {
  MOOD,
  SINGLES,
  VIEW_ALL,
  COMING_SOON,
  PLAY_LISTS,
  VIDEOS: homePage_VIDEOS,
  MOOD,
  PLAYLIST_FOR_AUTUMN,
  COMMING_SOON
};
/* harmony default export */ var en_homePage = (homePage);
// CONCATENATED MODULE: ./website/values/strings/en/global.js
const global_TRACKS = "Tracks";
const NO_ENTRIED = "no entried";
const FOLLOWERS = "followers";
const global = {
  TRACKS: global_TRACKS,
  NO_ENTRIED,
  FOLLOWERS
};
/* harmony default export */ var en_global = (global);
// CONCATENATED MODULE: ./website/values/strings/en/index.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const en = _objectSpread({}, en_navbar, {}, en_homePage, {}, en_global);

/* harmony default export */ var strings_en = (en);
// CONCATENATED MODULE: ./website/values/strings/fa/navbar.js
const navbar_APP_NAME = "ریمتال";
const navbar_EXPLORE = "کاوش کردن";
const navbar_TRACKS = "آهنگ ها";
const navbar_PLAYLISTS = "لیست های پخش";
const navbar_ALBUMS = "آلبوم ها";
const navbar_ARTISTS = "هنرمندان";
const navbar_VIDEOS = "ویدیو ها";
const navbar_SIGN_IN = "ورود";
const navbar_navbar = {
  APP_NAME: navbar_APP_NAME,
  EXPLORE: navbar_EXPLORE,
  TRACKS: navbar_TRACKS,
  PLAYLISTS: navbar_PLAYLISTS,
  ALBUMS: navbar_ALBUMS,
  ARTISTS: navbar_ARTISTS,
  VIDEOS: navbar_VIDEOS,
  SIGN_IN: navbar_SIGN_IN
};
/* harmony default export */ var fa_navbar = (navbar_navbar);
// CONCATENATED MODULE: ./website/values/strings/fa/homePage.js
const homePage_VIEW_ALL = "مشاهده همه";
const homePage_SINGLES = "تک آهنگ ها";
const homePage_COMING_SOON = "بزودی";
const homePage_PLAY_LISTS = "لیست پخش";
const fa_homePage_VIDEOS = "ویدیو ها";
const homePage_PLAYLIST_FOR_AUTUMN = "لیست پخش پیشنهادی ";
const homePage_MOOD = "حالت";
const homePage_COMMING_SOON = "بزودی";
const homePage_homePage = {
  MOOD: homePage_MOOD,
  SINGLES: homePage_SINGLES,
  VIEW_ALL: homePage_VIEW_ALL,
  SINGLES: homePage_SINGLES,
  COMING_SOON: homePage_COMING_SOON,
  PLAY_LISTS: homePage_PLAY_LISTS,
  VIDEOS: fa_homePage_VIDEOS,
  PLAYLIST_FOR_AUTUMN: homePage_PLAYLIST_FOR_AUTUMN,
  COMMING_SOON: homePage_COMMING_SOON
};
/* harmony default export */ var fa_homePage = (homePage_homePage);
// CONCATENATED MODULE: ./website/values/strings/fa/global.js
const fa_global_TRACKS = "آهنگ ها ";
const global_NO_ENTRIED = "وارد نشده";
const global_FOLLOWERS = "دنبال کنندگان";
const global_global = {
  TRACKS: fa_global_TRACKS,
  NO_ENTRIED: global_NO_ENTRIED,
  FOLLOWERS: global_FOLLOWERS
};
/* harmony default export */ var fa_global = (global_global);
// CONCATENATED MODULE: ./website/values/strings/fa/index.js
function fa_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function fa_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { fa_ownKeys(Object(source), true).forEach(function (key) { fa_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { fa_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function fa_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const fa = fa_objectSpread({}, fa_navbar, {}, fa_homePage, {}, fa_global);

/* harmony default export */ var strings_fa = (fa);
// CONCATENATED MODULE: ./website/values/strings/index.js


const Lang = "ltr";
let string;
if (Lang === "rtl") string = strings_fa;else string = strings_en;
/* harmony default export */ var strings = __webpack_exports__["a"] = (string);

/***/ }),

/***/ "zr5I":
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ })

/******/ });