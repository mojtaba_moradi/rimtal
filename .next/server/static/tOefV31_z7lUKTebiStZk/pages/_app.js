module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "/WIf":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/user-4ab57b30d27e4dd0fa03a9c0a9273140.png";

/***/ }),

/***/ "/jkW":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
}); // Identify /[param]/ in route string

const TEST_ROUTE = /\/\[[^/]+?\](?=\/|$)/;

function isDynamicRoute(route) {
  return TEST_ROUTE.test(route);
}

exports.isDynamicRoute = isDynamicRoute;

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("1TCz");


/***/ }),

/***/ "0Bsm":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__("TqRt");

exports.__esModule = true;
exports.default = withRouter;

var _react = _interopRequireDefault(__webpack_require__("cDcd"));

var _router = __webpack_require__("nOHt");

function withRouter(ComposedComponent) {
  function WithRouterWrapper(props) {
    return _react.default.createElement(ComposedComponent, Object.assign({
      router: (0, _router.useRouter)()
    }, props));
  }

  WithRouterWrapper.getInitialProps = ComposedComponent.getInitialProps // This is needed to allow checking for custom getInitialProps in _app
  ;
  WithRouterWrapper.origGetInitialProps = ComposedComponent.origGetInitialProps;

  if (false) { var name; }

  return WithRouterWrapper;
}

/***/ }),

/***/ "1TCz":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// NAMESPACE OBJECT: ./store/saga/index.js
var saga_namespaceObject = {};
__webpack_require__.r(saga_namespaceObject);
__webpack_require__.d(saga_namespaceObject, "watchGetHomeData", function() { return watchGetHomeData; });

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: ./node_modules/next/app.js
var app = __webpack_require__("8Bbg");
var app_default = /*#__PURE__*/__webpack_require__.n(app);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__("xnum");
var head_default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: ./public/styles/index.scss
var styles = __webpack_require__("on69");

// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__("YFqc");
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);

// EXTERNAL MODULE: external "react-dom"
var external_react_dom_ = __webpack_require__("faye");

// EXTERNAL MODULE: ./panelAdmin/assets/img/Rimtal.png
var Rimtal = __webpack_require__("EkH5");
var Rimtal_default = /*#__PURE__*/__webpack_require__.n(Rimtal);

// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/MenuTitle/index.js
var __jsx = external_react_default.a.createElement;


const MenuTitle = ({
  title
}) => {
  return __jsx("li", {
    className: "side-header change-position"
  }, __jsx("h6", null, title));
};

/* harmony default export */ var SideMenu_MenuTitle = (MenuTitle);
// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/SubMenu/index.js
var SubMenu_jsx = external_react_default.a.createElement;



const SubMenu = ({
  menu,
  setMenuTitle,
  showLi,
  selectedMenu,
  windowLocation
}) => {
  const location = windowLocation;
  const sideMenuLi = Object(external_react_["useRef"])(null);

  const onSubMenuClicked = (subMenu, menu) => {
    setMenuTitle(menu.menuTitle);
  };

  let liWidth = 0;
  if (sideMenuLi.current) liWidth = sideMenuLi.current.clientHeight;
  return SubMenu_jsx("ul", {
    style: {
      height: showLi === menu.menuTitle ? menu.subMenu.length * liWidth + "px" : ""
    },
    className: `side-child-navigation transition0-3 ${showLi === menu.menuTitle && menu.subMenu.length ? "showIn" : "showOut"}`
  }, menu.subMenu.map((child, i) => SubMenu_jsx("li", {
    ref: sideMenuLi,
    className: `side-iteme`,
    key: "subMenu-" + i,
    onClick: () => onSubMenuClicked(child, menu)
  }, SubMenu_jsx(link_default.a, {
    href: child.route,
    as: child.route
  }, SubMenu_jsx("a", {
    className: `side-link side-child-ling ${child.route === selectedMenu || location.includes(child.route) ? "activedSideChild" : ""} `,
    id: "sideChildTitle"
  }, child.title)))));
};

/* harmony default export */ var SideMenu_SubMenu = (SubMenu);
// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/Menu/index.js
var Menu_jsx = external_react_default.a.createElement;




const Menu = ({
  menus,
  showLi,
  setShowLi,
  selectedMenuTitle,
  setMenuTitle,
  selectedMenu,
  windowLocation
}) => {
  const location = windowLocation;

  const activedSideTitle = name => {
    let newName = name;

    if (showLi === name) {
      newName = "";
    } else {
      newName = name;
    }

    setShowLi(newName);
  };

  const classNameForMenu = (length, title) => {
    let classes;
    classes = [length ? "icon-right-open " : "", // selectedMenuTitle === title ? (showLi === title ? "" : "activeMenu") : "",
    showLi === title ? "arrowRotate" : "unsetRotate"].join(" ");
    return classes;
  };

  const _handelStateNull = () => {
    setShowLi("");
    setMenuTitle("");
  };

  return menus.map((menu, index) => {
    return Menu_jsx("li", {
      key: "menus-" + index,
      className: "side-iteme"
    }, Menu_jsx(link_default.a, {
      href: !menu.subMenu.length > 0 ? menu.route : "#"
    }, Menu_jsx("a", {
      onClick: menu.route ? _handelStateNull : () => activedSideTitle(menu.subMenu && menu.menuTitle),
      id: location.includes(menu.route) ? "activedSide" : selectedMenuTitle === menu.menuTitle ? showLi === menu.menuTitle ? "" : "activedSide" : "",
      className: `side-link ${classNameForMenu(menu.subMenu.length, menu.menuTitle)}`
    }, menu.menuIconImg ? Menu_jsx("img", {
      src: menu.menuIconImg,
      alt: "icon menu"
    }) : Menu_jsx("i", {
      className: menu.menuIconClass
    }), Menu_jsx("span", null, menu.menuTitle), menu.subMenu.length ? Menu_jsx("div", {
      className: "menu-arrow-icon"
    }, Menu_jsx("i", {
      className: "fas fa-angle-left"
    })) : "")), Menu_jsx(SideMenu_SubMenu, {
      windowLocation: windowLocation,
      menu: menu,
      setMenuTitle: setMenuTitle,
      showLi: showLi,
      selectedMenu: selectedMenu
    }));
  });
};

/* harmony default export */ var SideMenu_Menu = (Menu);
// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/MainMenu/index.js
var MainMenu_jsx = external_react_default.a.createElement;




const MainMenu = ({
  mainMenus,
  windowLocation
}) => {
  const {
    0: showLi,
    1: setShowLi
  } = Object(external_react_["useState"])("");
  const {
    0: selectedMenuTitle,
    1: setMenuTitle
  } = Object(external_react_["useState"])(); // console.log({ windowLocation });

  Object(external_react_["useEffect"])(() => {
    checkMenu();
  }, []);

  const checkMenu = () => {
    mainMenus.map(menu => {
      return menu.menus.map(menu => {
        return menu.subMenu.map(subMenu => {
          if (subMenu.route === "/" + windowLocation.substr(windowLocation.indexOf("panelAdmin"))) {
            setMenuTitle(menu.menuTitle);
            setShowLi(menu.menuTitle);
            return true;
          }
        });
      });
    });
  };

  Object(external_react_["useEffect"])(() => {
    checkMenu();
  }, [windowLocation]); // //console.log({ showLi, selectedMenuTitle });

  return mainMenus.map((mainMenu, index) => {
    return MainMenu_jsx("ul", {
      key: index + "m"
    }, MainMenu_jsx(SideMenu_MenuTitle, {
      title: mainMenu.title
    }), MainMenu_jsx(SideMenu_Menu, {
      windowLocation: windowLocation,
      menus: mainMenu.menus,
      showLi: showLi,
      setShowLi: setShowLi,
      selectedMenuTitle: selectedMenuTitle,
      setMenuTitle: setMenuTitle
    }));
  });
};

/* harmony default export */ var SideMenu_MainMenu = (MainMenu);
// EXTERNAL MODULE: external "react-scrollbars-custom"
var external_react_scrollbars_custom_ = __webpack_require__("YPTf");
var external_react_scrollbars_custom_default = /*#__PURE__*/__webpack_require__.n(external_react_scrollbars_custom_);

// EXTERNAL MODULE: external "react-perfect-scrollbar"
var external_react_perfect_scrollbar_ = __webpack_require__("RfFk");
var external_react_perfect_scrollbar_default = /*#__PURE__*/__webpack_require__.n(external_react_perfect_scrollbar_);

// EXTERNAL MODULE: ./panelAdmin/index.js + 104 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/index.js
var SideMenu_jsx = external_react_default.a.createElement;




 // import "./index.scss";
// import "react-perfect-scrollbar/dist/css/styles.css";

 // import ScrollArea from "react-scrollbar";

 // const ScrollArea = require("react-scrollbar");



const SideMenu = ({
  windowLocation
}) => {
  // return useMemo(() => {
  return SideMenu_jsx("div", {
    className: `panelAdmin-sideBar-container `
  }, SideMenu_jsx(link_default.a, {
    href: "/panelAdmin",
    as: "/panelAdmin"
  }, SideMenu_jsx("a", {
    className: "lgHolder"
  }, SideMenu_jsx("img", {
    src: Rimtal_default.a,
    alt: "logo"
  }))), SideMenu_jsx(external_react_perfect_scrollbar_default.a, null, SideMenu_jsx(SideMenu_MainMenu, {
    mainMenus: panelAdmin["a" /* default */].menuFormat,
    windowLocation: windowLocation
  }))); // }, []);
};

/* harmony default export */ var component_SideMenu = (SideMenu);
// EXTERNAL MODULE: ./panelAdmin/assets/Images/icons/user.png
var user = __webpack_require__("/WIf");
var user_default = /*#__PURE__*/__webpack_require__.n(user);

// CONCATENATED MODULE: ./panelAdmin/component/Header/HeaderProfile/index.js
var HeaderProfile_jsx = external_react_default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


 // import "./index.scss";

 // import Cookies from "js-cookie";

const HeaderProfile = () => {
  const {
    0: state,
    1: setState
  } = Object(external_react_["useState"])({
    showModal: false,
    clickedComponent: false
  });
  const wrapperRef = Object(external_react_["useRef"])(null);

  const showModalprofile = () => {
    // let noting = ;
    setState(prev => _objectSpread({}, prev, {
      showModal: !state.showModal,
      clickedComponent: true
    }));
  }; // console.log({ state: state.showModal });
  // const logOut = () => Cookies.remove("RimtalToken");


  const adminTitleModal = [{
    title: "پروفایل",
    iconClass: "fas fa-user",
    href: "#",
    onClick: null
  }, {
    title: "پیام ها",
    iconClass: "fas fa-envelope",
    href: "#",
    value: "",
    onClick: null
  }, {
    title: "تنظیمات",
    iconClass: "fas fa-cog",
    href: "#",
    onClick: null
  }, {
    title: "خروج",
    iconClass: " fas fa-sign-out-alt",
    href: "#",
    onClick: null
  }];

  const handleClickOutside = event => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      showModalprofile();
    }
  };

  Object(external_react_["useEffect"])(() => {
    if (state.showModal) {
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }
  });

  const adminTitleModal_map = HeaderProfile_jsx("ul", {
    className: `profile-modal-container ${state.showModal ? "actived" : state.clickedComponent ? "deActive" : "showOutStatic"}`
  }, adminTitleModal.map((admin, index) => {
    return HeaderProfile_jsx("li", {
      onClick: admin.onClick,
      key: index
    }, HeaderProfile_jsx("i", {
      className: admin.iconClass
    }), HeaderProfile_jsx(link_default.a, {
      href: admin.href,
      as: admin.href
    }, HeaderProfile_jsx("a", null, admin.title)), admin.value ? HeaderProfile_jsx("span", {
      className: "show-modal-icon-value"
    }, admin.value) : "");
  }));

  return HeaderProfile_jsx("ul", {
    onClick: showModalprofile,
    ref: wrapperRef,
    className: "panel-navbar-profile "
  }, HeaderProfile_jsx("li", {
    className: "pointer hoverColorblack normalTransition"
  }, HeaderProfile_jsx("div", {
    className: "centerAll"
  }, HeaderProfile_jsx("i", {
    className: "fas fa-angle-down"
  })), HeaderProfile_jsx("div", {
    className: "admin-profile-name  icon-up-dir "
  }, HeaderProfile_jsx("span", null, "\u0627\u062F\u0645\u06CC\u0646"), " "), HeaderProfile_jsx("div", {
    className: "admin-profile-image"
  }, HeaderProfile_jsx("img", {
    src: user_default.a,
    alt: "profile"
  }))), adminTitleModal_map);
};

/* harmony default export */ var Header_HeaderProfile = (HeaderProfile);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__("h74D");

// CONCATENATED MODULE: ./panelAdmin/component/Header/index.js
var Header_jsx = external_react_default.a.createElement;




const Header = ({
  _handelSidebarToggle
}) => {
  const NavbarStore = Object(external_react_redux_["useSelector"])(state => {
    return state.panelNavbar;
  });
  console.log({
    headerState: NavbarStore
  });
  return Header_jsx("nav", {
    className: "panelAdmin-navbar-container"
  }, Header_jsx("div", {
    className: "panel-navbar-box"
  }, Header_jsx("div", {
    className: "panel-navbar-side-element smallDisplay"
  }, Header_jsx("i", {
    onClick: _handelSidebarToggle,
    className: "fas fa-bars"
  }), Header_jsx("span", {
    className: "page-accepted-name"
  }, NavbarStore.pageName)), Header_jsx("div", {
    className: "panel-navbar-side-element"
  }, Header_jsx("ul", {
    className: "panel-navbar-notifications"
  }, Header_jsx("li", {
    className: "pointer hoverColorblack normalTransition"
  }, Header_jsx("i", {
    className: "icon-search"
  })), Header_jsx("li", {
    className: "navbar-icon-massege pointer hoverColorblack normalTransition"
  }, Header_jsx("div", {
    className: "massege-icon"
  }, Header_jsx("i", {
    className: "far fa-bell"
  }), Header_jsx("span", {
    className: "show-modal-icon-value"
  }, "4")))), Header_jsx(Header_HeaderProfile, null))));
};

/* harmony default export */ var component_Header = (Header);
// EXTERNAL MODULE: ./panelAdmin/component/UI/BackgrandCover/index.js
var BackgrandCover = __webpack_require__("7NiY");

// EXTERNAL MODULE: external "react-toastify"
var external_react_toastify_ = __webpack_require__("oAEb");

// EXTERNAL MODULE: external "next-redux-wrapper"
var external_next_redux_wrapper_ = __webpack_require__("JMOJ");
var external_next_redux_wrapper_default = /*#__PURE__*/__webpack_require__.n(external_next_redux_wrapper_);

// CONCATENATED MODULE: ./panelAdmin/screen/PanelScreen.js
var PanelScreen_jsx = external_react_default.a.createElement;









const PanelScreen = props => {
  const {
    router
  } = props;
  const {
    0: sidebarToggle,
    1: setSidebarToggle
  } = Object(external_react_["useState"])(false);

  const _handelSidebarToggle = () => {
    setSidebarToggle(!sidebarToggle);
  };

  return PanelScreen_jsx("div", {
    className: `panelAdmin-wrapper  ${sidebarToggle ? "fadeIn" : "fadeOut"}`
  }, PanelScreen_jsx("div", {
    style: {
      direction: "rtl",
      display: "flex"
    }
  }, PanelScreen_jsx(component_SideMenu, {
    windowLocation: router.asPath
  }), PanelScreen_jsx("div", {
    className: "panelAdmin-container"
  }, PanelScreen_jsx(component_Header, {
    _handelSidebarToggle: _handelSidebarToggle
  }), PanelScreen_jsx(external_react_scrollbars_custom_default.a, {
    style: {
      width: "100%",
      height: " 100% "
    }
  }, PanelScreen_jsx("div", {
    className: "panelAdmin-content"
  }, props.children)))), PanelScreen_jsx(BackgrandCover["a" /* default */], {
    fadeIn: sidebarToggle,
    onClick: _handelSidebarToggle
  }), PanelScreen_jsx(external_react_toastify_["ToastContainer"], {
    rtl: true
  }));
};

/* harmony default export */ var screen_PanelScreen = (PanelScreen);
// EXTERNAL MODULE: ./website/components/Header/index/index.js + 3 modules
var Header_index = __webpack_require__("vfdN");

// CONCATENATED MODULE: ./website/screen/WebsiteScreen.js
var WebsiteScreen_jsx = external_react_default.a.createElement;



const WebsiteScreen = props => {
  return WebsiteScreen_jsx("div", null, WebsiteScreen_jsx(Header_index["a" /* default */], null), props.children);
};

/* harmony default export */ var screen_WebsiteScreen = (WebsiteScreen);
// EXTERNAL MODULE: external "redux"
var external_redux_ = __webpack_require__("rKB8");

// EXTERNAL MODULE: external "redux-saga"
var external_redux_saga_ = __webpack_require__("1fKG");
var external_redux_saga_default = /*#__PURE__*/__webpack_require__.n(external_redux_saga_);

// EXTERNAL MODULE: ./store/actionTypes/redux/index.js
var redux = __webpack_require__("rUrl");

// CONCATENATED MODULE: ./store/reducer/errorReducer.js
function errorReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function errorReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { errorReducer_ownKeys(Object(source), true).forEach(function (key) { errorReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { errorReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function errorReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const errorInitialState = {
  error: null
};
const addFailure = (state, action) => {
  return errorReducer_objectSpread({}, state, {
    error: action.error
  });
};
const removeFailure = state => {
  return errorReducer_objectSpread({}, state, {
    error: null
  });
};

function errorReducer(state = errorInitialState, action) {
  switch (action.type) {
    case redux["a" /* default */].ADD_FAILURE:
      return addFailure(state, action);

    case redux["a" /* default */].REMOVE_FAILURE:
      return removeFailure(state);

    default:
      return state;
  }
}

/* harmony default export */ var reducer_errorReducer = (errorReducer);
// CONCATENATED MODULE: ./store/reducer/themeColorReducer.js
function themeColorReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function themeColorReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { themeColorReducer_ownKeys(Object(source), true).forEach(function (key) { themeColorReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { themeColorReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function themeColorReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const webThemeColorInitialstate = {
  light: {
    primaryColor: "#0070ef",
    accentColor: "#666666",
    mainColor: "#ffff",
    titleColor: "#333333"
  },
  dark: {
    primaryColor: "#0070ef",
    accentColor: "#666666",
    mainColor: "#000",
    titleColor: "#ffff"
  },
  currentTheme: "light"
};
const setThemeColor = (state, action) => {
  return themeColorReducer_objectSpread({}, state, {
    homeData: action.data
  });
};

function themeColorReducer(state = webThemeColorInitialstate, action) {
  //   switch (action.type) {
  //     case atRedux.:
  //       return setThemeColor(state, action);
  //     default:
  //       return state;
  //   }
  return state;
}

/* harmony default export */ var reducer_themeColorReducer = (themeColorReducer);
// EXTERNAL MODULE: ./website/index.js + 24 modules
var website = __webpack_require__("akFz");

// CONCATENATED MODULE: ./store/reducer/index.js
function reducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function reducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { reducer_ownKeys(Object(source), true).forEach(function (key) { reducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { reducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function reducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const rootReducer = Object(external_redux_["combineReducers"])(reducer_objectSpread({
  error: reducer_errorReducer,
  themeColor: reducer_themeColorReducer
}, panelAdmin["a" /* default */].reducer, {}, website["a" /* default */].reducer));
/* harmony default export */ var reducer = (rootReducer);
// EXTERNAL MODULE: external "redux-saga/effects"
var effects_ = __webpack_require__("RmXt");

// EXTERNAL MODULE: ./store/actionTypes/saga/index.js
var saga = __webpack_require__("sHut");

// EXTERNAL MODULE: ./store/actions/redux/index.js
var actions_redux = __webpack_require__("D8kl");

// EXTERNAL MODULE: ./store/actions/saga/index.js
var actions_saga = __webpack_require__("BpoA");

// CONCATENATED MODULE: ./store/actions/index.js



const actions = {
  reduxActions: actions_redux,
  sagaActions: actions_saga
};
/* harmony default export */ var store_actions = (actions);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__("zr5I");
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// CONCATENATED MODULE: ./store/saga/webServices/GET/homeData.js



function* homeData() {
  console.log("home get khord");

  try {
    const res = yield external_axios_default.a.get("https://rimtal.com/api/v1/home");
    console.log({
      res
    });
    yield Object(effects_["put"])(store_actions.reduxActions.setHomeData(res.data)); //********** Level 5 **********//
  } catch (err) {
    if (!err.response) yield Object(effects_["put"])(store_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(store_actions.reduxActions.setFailure(err.response.data.CODE)); //********** Level 6 **********//
  }
}
// CONCATENATED MODULE: ./store/saga/webServices/GET/galleryApi.js



function* galleryData({
  data
}) {
  try {
    const res = yield external_axios_default.a.get(`https://rimtal.com/api/v1/admin/gallery/${data.type}/${data.page} `);
    console.log({
      res
    });
    yield Object(effects_["put"])(store_actions.reduxActions.setGalleryData(res.data)); //********** Level 5 **********//
  } catch (err) {
    if (!err.response) yield Object(effects_["put"])(store_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(store_actions.reduxActions.setFailure(err.response.data.CODE)); //********** Level 6 **********//
  }
}
// CONCATENATED MODULE: ./store/saga/webServices/GET/artists.js




function* artistData({
  page
}) {
  try {
    const res = yield panelAdmin["a" /* default */].api.get.artists({
      page
    });
    console.log({
      resArtistData: res
    });
    yield Object(effects_["put"])(store_actions.reduxActions.setArtistData(res.data));
  } catch (err) {
    console.log({
      errArtistData: err
    });
    if (!err.response) yield Object(effects_["put"])(store_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(store_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* artistSearchData({
  page,
  title
}) {
  try {
    const res = yield panelAdmin["a" /* default */].api.get.artistSearch({
      page,
      title
    });
    console.log({
      resArtistSearchData: res
    });
    yield Object(effects_["put"])(store_actions.reduxActions.setSearchArtistData(res.data));
  } catch (err) {
    console.log({
      errArtistSearchData: err
    });
    if (!err.response) yield Object(effects_["put"])(store_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(store_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
// CONCATENATED MODULE: ./store/saga/webServices/GET/index.js



const GET = {
  homeData: homeData,
  galleryData: galleryData,
  artistData: artistData,
  artistSearchData: artistSearchData
};
/* harmony default export */ var webServices_GET = (GET);
// CONCATENATED MODULE: ./store/saga/webServices/POST/uploadImage.js




function* uploadImage(props) {
  console.log("uploaaaaaaaaad omad");
  const {
    imageName,
    type,
    files
  } = props;
  console.log({
    uploadUploadImage: props
  });
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.strings;
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total); // setState((prev) => ({ ...prev, progressPercentImage: percentCompleted }));
      // yield put(actions.reduxActions.uploadImageProcess());

      console.log(percentCompleted);
    },
    cancelToken: source.token
  };
  const URL = strings.ApiString.IMAGE_UPLOAD;
  const formData = new FormData();
  formData.append("imageName", imageName);
  formData.append("imageType", type);
  formData.append("image", files);

  try {
    const res = yield external_axios_default.a.post(URL, formData, settings);
    return res; // yield put(actions.reduxActions.setUploadImage(res.data));
  } catch (err) {
    console.log({
      err
    });
    if (!err.response) yield Object(effects_["put"])(store_actions.reduxActions.setFailure(1090)); // yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
// CONCATENATED MODULE: ./store/saga/webServices/POST/index.js

const POST = {
  uploadImage: uploadImage
};
/* harmony default export */ var webServices_POST = (POST);
// CONCATENATED MODULE: ./store/saga/webServices/index.js


const webServices = {
  GET: webServices_GET,
  POST: webServices_POST
};
/* harmony default export */ var saga_webServices = (webServices);
// CONCATENATED MODULE: ./store/saga/index.js



 // ============================= HOME

function* watchGetHomeData() {
  yield Object(effects_["takeEvery"])(saga["a" /* default */].GET_HOME_SCREEN_DATA, saga_webServices.GET.homeData);
} // ============================= HOME
// CONCATENATED MODULE: ./store/index.js
function store_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function store_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { store_ownKeys(Object(source), true).forEach(function (key) { store_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { store_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function store_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







const allWatchers = store_objectSpread({}, saga_namespaceObject, {}, panelAdmin["a" /* default */].saga);

console.log({
  allWatchers
});

const bindMiddleware = middleware => {
  if (false) {}

  return Object(external_redux_["applyMiddleware"])(...middleware);
}; //for redux devTools


function configureStore() {
  const sagaMiddleware = external_redux_saga_default()();
  const store = Object(external_redux_["createStore"])(reducer, bindMiddleware([sagaMiddleware]));

  for (let watcher in allWatchers) {
    store.sagaTask = sagaMiddleware.run(allWatchers[watcher]);
  }

  return store;
}

/* harmony default export */ var store_0 = (configureStore);
// CONCATENATED MODULE: ./website/baseComponents/ErrorHandler/index.js
var ErrorHandler_jsx = external_react_default.a.createElement;



const ErrorHandler = () => {
  const error = Object(external_react_redux_["useSelector"])(state => {
    // console.log({ErrorHandler: state.error});
    return state.error;
  });
  Object(external_react_["useEffect"])(() => {
    switch (error.error) {
      case 1098:
        alert("خطایی در سرور رخ داده");
        break;

      case 1090:
        alert("اتصال به اینترنت را بررسی کنید");
        break;
    }
  }, [error]);
  return ErrorHandler_jsx(external_react_default.a.Fragment, null);
};

/* harmony default export */ var baseComponents_ErrorHandler = (ErrorHandler);
// CONCATENATED MODULE: ./pages/_app.js

var _app_jsx = external_react_default.a.createElement;










class _app_RimtalApp extends app_default.a {
  static async getInitialProps({
    Component,
    ctx
  }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps({
        ctx
      });
    }

    return {
      pageProps
    };
  }

  render() {
    const {
      Component,
      pageProps,
      store,
      router
    } = this.props;
    let body;

    if (router.asPath.includes("/panelAdmin")) {
      body = _app_jsx(screen_PanelScreen, {
        router: router
      }, _app_jsx(Component, pageProps));
    } else {
      body = _app_jsx(screen_WebsiteScreen, null, _app_jsx(Component, pageProps));
    }

    return _app_jsx("div", null, _app_jsx(head_default.a, null, _app_jsx("title", null, "Rimtal"), _app_jsx("meta", {
      name: "description",
      content: "\u062C\u0627\u0645\u0639 \u062A\u0631\u06CC\u0646 \u067E\u0644\u062A\u0641\u0631\u0645 \u0645\u0648\u0633\u06CC\u0642\u06CC \u0627\u06CC\u0631\u0627\u0646 ."
    }), _app_jsx("meta", {
      name: "keywords",
      content: "\u0631\u06CC\u0645\u062A\u0627\u0644\u060Crimtal,RIMTAL ,\u0645\u0648\u0633\u06CC\u0642\u06CC \u0648 ,IPTV,\u0645\u0648\u0633\u06CC\u0642\u06CC,\u067E\u0631\u062F\u0627\u062E\u062A \u0627\u062C\u062A\u0645\u0627\u0639\u06CC\u060C\u067E\u062E\u0634 \u0632\u0646\u062F\u0647\u060C\u0627\u067E\u0644\u06CC\u06A9\u06CC\u0634\u0646,\u0633\u0631\u06AF\u0631\u0645\u06CC,"
    }), _app_jsx("meta", {
      content: "width=device-width, initial-scale=1",
      name: "viewport"
    }), _app_jsx("script", {
      src: "https://code.jquery.com/jquery-3.4.1.slim.min.js",
      integrity: "sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n",
      crossOrigin: "anonymous"
    }), _app_jsx("script", {
      src: "https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js",
      integrity: "sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo",
      crossOrigin: "anonymous"
    }), _app_jsx("script", {
      src: "https://code.jquery.com/jquery-3.3.1.slim.min.js",
      integrity: "sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo",
      crossOrigin: "anonymous"
    }), _app_jsx("script", {
      src: "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js",
      integrity: "sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6",
      crossOrigin: "anonymous"
    }), _app_jsx("link", {
      href: "/styles/css/styles.css",
      rel: "stylesheet"
    })), _app_jsx("div", {
      className: "base-page"
    }, _app_jsx(external_react_redux_["Provider"], {
      store: store
    }, body, " ", _app_jsx(baseComponents_ErrorHandler, null))));
  }

}

/* harmony default export */ var _app = __webpack_exports__["default"] = (external_next_redux_wrapper_default()(store_0)(_app_RimtalApp));

/***/ }),

/***/ "1fKG":
/***/ (function(module, exports) {

module.exports = require("redux-saga");

/***/ }),

/***/ "284h":
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__("cDf5");

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ "2FOw":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAXCAYAAADHqJcNAAAGwUlEQVRogb2aeahVRRzHP/pepZlttlFR2WpiWUm9tL2o1GwjbKesBNMwzIoMaTda7I8kWohKIgiL9sUWyHYqLZdS20XL1MQ2l3pu78UPvhM/pjnnnnPufX7gcu/MnTNnzpzf/LaZToz5g4i9gBOBXsCW8Z9iPbAY+Aj4LKON50zgGuAd4C5gQ0a7PsBoYD4wqUC/RdkKuAPYBrhZYw/YMw4DdmrQvX4CntIcpWgGTgUGAUcAPYF5wHTgFc1pWYZpfp8B7gbaynbgBWEX4FbgAmDrEn28C9wOvJfxv03+t8DOKh8LfJho1wR8Ahyu8nnAsyXGkcd9wLX6f4qeMfAQMLJB9wnYfNySqB+s+iNyrn0TuAH4suC9TIAXAN1UPgSYU3bAnfW9LzANGFFSCIwTgLeASzP+7+4GaWyf0a5ZwhgwoexSciwprM/hrn6PqM2BDbhHzAGJuiuBV2sIgTFQi+r4gvey97WFK/eoMuBmvaiXExNiq3hl4pp2YHNgbyc0Vn4cWJTQDNZ+oytnqa32yGTYeIZKzdbDSGmlQGyWbPXdnzOB7RLeHVU2lb9Q9Sl+kUbwmBl4OKoz8/QpsALYDTga2E7/2ffzQH/guxrP3qb5bXbl0tjFY4He7kLTDOOBmcC6jA47aWWZlI9Tnan2B6Xa/64ymATjNSFV+zO1OSoxdo/Z5gHRqvLYC78amKi6pUALsCbRl7E2Km+reQm0yTyYSfrd1e8J3ARcobIJ3z3A2TWeMRbI1Jhq0lkvM2CO3xBJapYQhJvb6r8RuM3V95apqEr8UKZiL6+jP3OidijYdm3GZ11iLlpVl7om5lw5hAFbOBMiIUDzaSbsSVd3hmx+LbK0U2E6R3bZXuo/Jfu4E/jBlU+uYzypBxob+RhF2UYruRHEq6ypRJ+D3O9ZclzzGOdMcmc5zWWorBECFj7MqNDH+iiE7JnTtgo9I61VlBGyvTFVJsoLaJnrzdnt68ovFFi9y4D3XXlIDcGrWxsQCUJrTnxfC69FNq/YR94DjY0cvlp064CQsAr7RFHKzIJ9POd+9yoQ2VQV1P/wgtCpaifRdQ2RUOADl5TZVSu8KJcoMYaEe0nGWItS1SE71K1m8ym+L3jd2848mEN/Wk7b9kYLQj3kOZZl8A80RZnIwPUuKZVHF5c8Qkmq1xo0vrL4nMFCfYqwTA574OycF9yQqKG5QJt9ldzYXdKdWvH9q9w8ge+7Vd71QJXN+79KKeI8hkolB+5RNjOwKX2Eg9zv2Tlp5xQmvKeovp9MxNflhl2cPEHoosTIqJJeexmPOg9LdH0srXCS2pndfyRS9Z4tXF4DTdzrkede1fyVxfY39nPXTC95vY37Xr2HZoXlWYLQYabBOntM6rhs6JbKRlYhjG2Cu9a0wqNA14z+JkbJsbBx1SgTGCgy2fvLtwmUjcgWyKwFshJLsY9QiSyNYLHrRa7cpoGtrzEJy7XL10gsZf0ScJb6PE2bVvcqbN0gFTxGu3qBOS45U/eKiSjSx2Gu3coCqeIUb7gE3VHKPi6K2sWCUMTc/4/4otChT8sulWDMiPYMUpSxgTF53u9oZdhCJNBPW65rJKTdo75WKUvXmrjPpvIRWtzvhVokZXlL2/ZN0oKDE3sWfwJ/uTkwjTi17I1ilblBpsDHrZO0Alv1ovM+9ZI14YulCeLt1W4JIVgsNfq5q2tUSFuUTtIIgdkVN4Pm6trA6Yk2qyNtc3EVPy0WhHY5XF5T/Fy20w7CDqsco42ZHxO3WKJdxJYo7IzZFBphV/kIgSKHd1K0ySwGjnS7oB6fgOpbILL6H7EgNEnNrHB1w3N25jqS1ISvkvN4sFbcEG3MtOh00zUZEUW9GqFsrH6AoobAF3Xc+xVnkrfL2MuxrfpvXNkE4YmMcxFJYh+hSTc1KbxOdSco923x+It1PFAj+VsbOLMK9rmpTUMf9/tXne2oynyp/mCuzwGejvparRzLVLdoLwPOl4CsdsIbf5spHZ/lYU7UDcMGUos2TN7QyaGyMXFRGu3ddwRFxuUzivPk0FVlg3IKQRCOk2aID5tO0xG8yW5fpqvS3LVY5U2DF4rlOnA6N+pgkOzd5Ch7l4dN3Gbu/7yJ9GNoVGKKyARultMuCz/mWuFZcyQIpc8PJvCauIeEIavdAGn0MhuIy5vdAYkV0Yr8Ssenhiuc3Fb19oIulOPygGxRKkwLbJRghWNtqcMb6N4r3JnGRp1yQnF8eM74QEgRbHc1XPdbjQssFW/RTGjfCO1pEZD1Y+l+w06ZeyfSY6bEoibzo+wdWe7BNIMPz/33EuCJfwFWG3TGQDr4PQAAAABJRU5ErkJggg=="

/***/ }),

/***/ "4Q3z":
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "7NiY":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const BackgrandCover = props => {
  return __jsx("div", {
    id: "coverContainer",
    onClick: props.onClick,
    className: props.fadeIn ? " fadeIn" : " fadeOut"
  });
};

/* harmony default export */ __webpack_exports__["a"] = (BackgrandCover);

/***/ }),

/***/ "8Bbg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("B5Ud")


/***/ }),

/***/ "B5Ud":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__("TqRt");

exports.__esModule = true;
exports.Container = Container;
exports.createUrl = createUrl;
exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__("cDcd"));

var _utils = __webpack_require__("kYf9");

exports.AppInitialProps = _utils.AppInitialProps;
/**
* `App` component is used for initialize of pages. It allows for overwriting and full control of the `page` initialization.
* This allows for keeping state between navigation, custom error handling, injecting additional data.
*/

async function appGetInitialProps(_ref) {
  var {
    Component,
    ctx
  } = _ref;
  var pageProps = await (0, _utils.loadGetInitialProps)(Component, ctx);
  return {
    pageProps
  };
}

class App extends _react.default.Component {
  // Kept here for backwards compatibility.
  // When someone ended App they could call `super.componentDidCatch`.
  // @deprecated This method is no longer needed. Errors are caught at the top level
  componentDidCatch(error, _errorInfo) {
    throw error;
  }

  render() {
    var {
      router,
      Component,
      pageProps,
      __N_SSG,
      __N_SSP
    } = this.props;
    return _react.default.createElement(Component, Object.assign({}, pageProps, // we don't add the legacy URL prop if it's using non-legacy
    // methods like getStaticProps and getServerSideProps
    !(__N_SSG || __N_SSP) ? {
      url: createUrl(router)
    } : {}));
  }

}

exports.default = App;
App.origGetInitialProps = appGetInitialProps;
App.getInitialProps = appGetInitialProps;
var warnContainer;
var warnUrl;

if (false) {} // @deprecated noop for now until removal


function Container(p) {
  if (false) {}
  return p.children;
}

function createUrl(router) {
  // This is to make sure we don't references the router object at call time
  var {
    pathname,
    asPath,
    query
  } = router;
  return {
    get query() {
      if (false) {}
      return query;
    },

    get pathname() {
      if (false) {}
      return pathname;
    },

    get asPath() {
      if (false) {}
      return asPath;
    },

    back: () => {
      if (false) {}
      router.back();
    },
    push: (url, as) => {
      if (false) {}
      return router.push(url, as);
    },
    pushTo: (href, as) => {
      if (false) {}
      var pushRoute = as ? href : '';
      var pushUrl = as || href;
      return router.push(pushRoute, pushUrl);
    },
    replace: (url, as) => {
      if (false) {}
      return router.replace(url, as);
    },
    replaceTo: (href, as) => {
      if (false) {}
      var replaceRoute = as ? href : '';
      var replaceUrl = as || href;
      return router.replace(replaceRoute, replaceUrl);
    }
  };
}

/***/ }),

/***/ "BpoA":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getHomeData", function() { return getHomeData; });
/* harmony import */ var _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("sHut");

function getHomeData() {
  console.log("getHomeData");
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].GET_HOME_SCREEN_DATA
  };
} // export function uploadImageData(data) {
//   return { type: atSaga.POST_UPLOAD_IMAGE, data: data };
// }
// export function getGalleryData(data) {
//   return { type: atSaga.GET_GALLERY_DATA, data: data };
// }
// export function getArtistData({ page }) {
//   return { type: atSaga.GET_ARTIST_DATA, page };
// }
// export function getSearchArtistData({ page }) {
//   return { type: atSaga.GET_SEARCH_ARTIST_DATA, page };
// }

/***/ }),

/***/ "D8kl":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "increment", function() { return increment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setHomeData", function() { return setHomeData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setFailure", function() { return setFailure; });
/* harmony import */ var _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("rUrl");

function increment() {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].INCREMENT
  };
} // // =================================================== NAVBAR
// export function setPageName(data) {
//   return { type: atRedux.SET_PAGE_NAME, data };
// }
// =================================================== HOME

function setHomeData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].SET_HOME_DATA,
    data
  };
} // =================================================== ERROR

function setFailure(error) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].ADD_FAILURE,
    error
  };
} // // =================================================== UPLOAD
// export function setUploadImage(data) {
//   return { type: atRedux.SET_UPLOAD_IMAGE, data };
// }
// // =================================================== GALLERY
// export function setGalleryData(data) {
//   return { type: atRedux.SET_GALLERY_DATA, data };
// }
// export function changeAddGalleryData(data) {
//   return { type: atRedux.CHANGE_ADD_GALLERY_DATA, data };
// }
// // =================================================== END GALLERY
// // =================================================== ARTIST
// export function setArtistData(data) {
//   return { type: atRedux.SET_ARTIST_DATA, data };
// }
// export function setSearchArtistData(data) {
//   return { type: atRedux.SET_SEARCH_ARTIST_DATA, data };
// }
// export function startGetArtist(data) {
//   return { type: atRedux.START_ARTIST_DATA, data };
// }
// export function startSearchArtist(data) {
//   return { type: atRedux.START_SEARCH_ARTIST_DATA, data };
// }
// // ================================================= END ARTIST

/***/ }),

/***/ "EkH5":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAXCAYAAADHqJcNAAAGwUlEQVRogb2aeahVRRzHP/pepZlttlFR2WpiWUm9tL2o1GwjbKesBNMwzIoMaTda7I8kWohKIgiL9sUWyHYqLZdS20XL1MQ2l3pu78UPvhM/pjnnnnPufX7gcu/MnTNnzpzf/LaZToz5g4i9gBOBXsCW8Z9iPbAY+Aj4LKON50zgGuAd4C5gQ0a7PsBoYD4wqUC/RdkKuAPYBrhZYw/YMw4DdmrQvX4CntIcpWgGTgUGAUcAPYF5wHTgFc1pWYZpfp8B7gbaynbgBWEX4FbgAmDrEn28C9wOvJfxv03+t8DOKh8LfJho1wR8Ahyu8nnAsyXGkcd9wLX6f4qeMfAQMLJB9wnYfNySqB+s+iNyrn0TuAH4suC9TIAXAN1UPgSYU3bAnfW9LzANGFFSCIwTgLeASzP+7+4GaWyf0a5ZwhgwoexSciwprM/hrn6PqM2BDbhHzAGJuiuBV2sIgTFQi+r4gvey97WFK/eoMuBmvaiXExNiq3hl4pp2YHNgbyc0Vn4cWJTQDNZ+oytnqa32yGTYeIZKzdbDSGmlQGyWbPXdnzOB7RLeHVU2lb9Q9Sl+kUbwmBl4OKoz8/QpsALYDTga2E7/2ffzQH/guxrP3qb5bXbl0tjFY4He7kLTDOOBmcC6jA47aWWZlI9Tnan2B6Xa/64ymATjNSFV+zO1OSoxdo/Z5gHRqvLYC78amKi6pUALsCbRl7E2Km+reQm0yTyYSfrd1e8J3ARcobIJ3z3A2TWeMRbI1Jhq0lkvM2CO3xBJapYQhJvb6r8RuM3V95apqEr8UKZiL6+jP3OidijYdm3GZ11iLlpVl7om5lw5hAFbOBMiIUDzaSbsSVd3hmx+LbK0U2E6R3bZXuo/Jfu4E/jBlU+uYzypBxob+RhF2UYruRHEq6ypRJ+D3O9ZclzzGOdMcmc5zWWorBECFj7MqNDH+iiE7JnTtgo9I61VlBGyvTFVJsoLaJnrzdnt68ovFFi9y4D3XXlIDcGrWxsQCUJrTnxfC69FNq/YR94DjY0cvlp064CQsAr7RFHKzIJ9POd+9yoQ2VQV1P/wgtCpaifRdQ2RUOADl5TZVSu8KJcoMYaEe0nGWItS1SE71K1m8ym+L3jd2848mEN/Wk7b9kYLQj3kOZZl8A80RZnIwPUuKZVHF5c8Qkmq1xo0vrL4nMFCfYqwTA574OycF9yQqKG5QJt9ldzYXdKdWvH9q9w8ge+7Vd71QJXN+79KKeI8hkolB+5RNjOwKX2Eg9zv2Tlp5xQmvKeovp9MxNflhl2cPEHoosTIqJJeexmPOg9LdH0srXCS2pndfyRS9Z4tXF4DTdzrkede1fyVxfY39nPXTC95vY37Xr2HZoXlWYLQYabBOntM6rhs6JbKRlYhjG2Cu9a0wqNA14z+JkbJsbBx1SgTGCgy2fvLtwmUjcgWyKwFshJLsY9QiSyNYLHrRa7cpoGtrzEJy7XL10gsZf0ScJb6PE2bVvcqbN0gFTxGu3qBOS45U/eKiSjSx2Gu3coCqeIUb7gE3VHKPi6K2sWCUMTc/4/4otChT8sulWDMiPYMUpSxgTF53u9oZdhCJNBPW65rJKTdo75WKUvXmrjPpvIRWtzvhVokZXlL2/ZN0oKDE3sWfwJ/uTkwjTi17I1ilblBpsDHrZO0Alv1ovM+9ZI14YulCeLt1W4JIVgsNfq5q2tUSFuUTtIIgdkVN4Pm6trA6Yk2qyNtc3EVPy0WhHY5XF5T/Fy20w7CDqsco42ZHxO3WKJdxJYo7IzZFBphV/kIgSKHd1K0ySwGjnS7oB6fgOpbILL6H7EgNEnNrHB1w3N25jqS1ISvkvN4sFbcEG3MtOh00zUZEUW9GqFsrH6AoobAF3Xc+xVnkrfL2MuxrfpvXNkE4YmMcxFJYh+hSTc1KbxOdSco923x+It1PFAj+VsbOLMK9rmpTUMf9/tXne2oynyp/mCuzwGejvparRzLVLdoLwPOl4CsdsIbf5spHZ/lYU7UDcMGUos2TN7QyaGyMXFRGu3ddwRFxuUzivPk0FVlg3IKQRCOk2aID5tO0xG8yW5fpqvS3LVY5U2DF4rlOnA6N+pgkOzd5Ch7l4dN3Gbu/7yJ9GNoVGKKyARultMuCz/mWuFZcyQIpc8PJvCauIeEIavdAGn0MhuIy5vdAYkV0Yr8Ssenhiuc3Fb19oIulOPygGxRKkwLbJRghWNtqcMb6N4r3JnGRp1yQnF8eM74QEgRbHc1XPdbjQssFW/RTGjfCO1pEZD1Y+l+w06ZeyfSY6bEoibzo+wdWe7BNIMPz/33EuCJfwFWG3TGQDr4PQAAAABJRU5ErkJggg=="

/***/ }),

/***/ "FRaV":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "get", function() { return /* reexport */ Get; });
__webpack_require__.d(__webpack_exports__, "post", function() { return /* reexport */ Post; });
__webpack_require__.d(__webpack_exports__, "put", function() { return /* reexport */ Put; });
__webpack_require__.d(__webpack_exports__, "patch", function() { return /* reexport */ api_Patch; });
__webpack_require__.d(__webpack_exports__, "deletes", function() { return /* reexport */ api_Delete; });

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__("zr5I");
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// CONCATENATED MODULE: ./panelAdmin/api/axios-orders.js
 // import Cookie from "js-cookie";

const instance = external_axios_default.a.create({
  baseURL: "https://rimtal.com/api/v1"
});
instance.defaults.headers.common["Authorization"] = "Bearer ";
/* harmony default export */ var axios_orders = (instance);
// EXTERNAL MODULE: ./panelAdmin/index.js + 104 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/api/Get/genres.js



const genres = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  const axiosData = page ? strings.GENRES + "/" + page : strings.GENRES;
  return axios_orders.get(axiosData); // .then((genres) => {
  //   console.log({ genres });
  //   returnData(genres.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_genres = (genres);
// CONCATENATED MODULE: ./panelAdmin/api/Get/genreSearch.js



const genreSearch = async (param, page, returnData) => {
  // console.log({ param, page, returnData });
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  return axios_orders.get(strings.GENRES + "/" + param + "/" + page).then(genreSearch => {
    console.log({
      genreSearch
    });
    returnData(genreSearch.data); // loading(false);
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ var Get_genreSearch = (genreSearch);
// CONCATENATED MODULE: ./panelAdmin/api/Get/artistSearch.js



const artistSearch = async ({
  title,
  page
}) => {
  console.log({
    cattitle: title
  });
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  return axios_orders.get(strings.ARTIST_SEARCH + "/" + title + "/" + page); // .then((artistSearch) => {
  //   // console.log({ artistSearch });
  //   returnData(artistSearch.data);
  //   // loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   // else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_artistSearch = (artistSearch);
// CONCATENATED MODULE: ./panelAdmin/api/Get/country.js



const country = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.COUNTRY;
  const axiosData = page ? strings + "/" + page : strings;
  return axios_orders.get(axiosData); // .then((country) => {
  //   console.log({ country });
  //   returnData(country.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_country = (country);
// CONCATENATED MODULE: ./panelAdmin/api/Get/countrySearch.js



const countrySearch = async (title, page, returnData) => {
  console.log({
    title,
    page
  });
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.COUNTRY;
  return axios_orders.get(strings + "/" + title + "/" + page).then(countrySearch => {
    // console.log({ countrySearch });
    returnData(countrySearch.data); // loading(false);
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ var Get_countrySearch = (countrySearch);
// CONCATENATED MODULE: ./panelAdmin/api/Get/instrument.js



const instrument = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.INSTRUMENT;
  const axiosData = page ? strings + "/" + page : strings;
  return axios_orders.get(axiosData); // .then((instrument) => {
  //   console.log({ instrument });
  //   returnData(instrument.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_instrument = (instrument);
// CONCATENATED MODULE: ./panelAdmin/api/Get/instrumentSearch.js



const instrumentSearch = async (param, page, returnData) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.INSTRUMENT;
  return axios_orders.get(strings + "/" + param + "/" + page).then(instrumentSearch => {
    // console.log({ instrumentSearch });
    returnData(instrumentSearch.data); // loading(false);
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ var Get_instrumentSearch = (instrumentSearch);
// CONCATENATED MODULE: ./panelAdmin/api/Get/artists.js



const artists = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  return axios_orders.get(strings.ARTIST + "/" + page); // .then((artists) => {
  //   console.log({ artists });
  //   // return artists;
  //   // returnData(artists.data);
  //   // loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_artists = (artists);
// CONCATENATED MODULE: ./panelAdmin/api/Get/mood.js



const mood = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.MOOD;
  return axios_orders.get(strings + "/" + page); // .then((mood) => {
  //   console.log({ mood });
  //   returnData(mood.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_mood = (mood);
// CONCATENATED MODULE: ./panelAdmin/api/Get/hashtag.js



const hashtag = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.HASHTAG;
  return axios_orders.get(strings + "/" + page); // .then((hashtag) => {
  //   console.log({ hashtag });
  //   returnData(hashtag.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_hashtag = (hashtag);
// CONCATENATED MODULE: ./panelAdmin/api/Get/albums.js



const albums = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  return axios_orders.get(strings.ALBUM + "/" + page);
};

/* harmony default export */ var Get_albums = (albums);
// CONCATENATED MODULE: ./panelAdmin/api/Get/index.js
// import categoreis from "./categoreis";
// import sliders from "./sliders";
// import Owners from "./Owners";
// import discounts from "./discounts";
// import discount from "./discount";
// import categoreisSearch from "./categoreisSearch";
// import ownersSearch from "./ownersSearch";
// import discountSearch from "./discountSearch";
 // import club from "./club";

 // import category from "./category";
// import members from "./members";
// import banners from "./banners";
// import scenarios from "./scenarios";





 // import gallery from "./gallery";
// import song from "./song";





const get = {
  instrument: Get_instrument,
  instrumentSearch: Get_instrumentSearch,
  // categoreis,
  // sliders,
  // Owners,
  // discounts,
  // categoreisSearch,
  // category,
  // ownersSearch,
  // discountSearch,
  genres: Get_genres,
  genreSearch: Get_genreSearch,
  // discount,
  // club,
  // members,
  // banners,
  // scenarios,
  artistSearch: Get_artistSearch,
  country: Get_country,
  countrySearch: Get_countrySearch,
  // gallery,
  // song,
  artists: Get_artists,
  mood: Get_mood,
  hashtag: Get_hashtag,
  albums: Get_albums
};
/* harmony default export */ var Get = (get);
// CONCATENATED MODULE: ./panelAdmin/api/Put/index.js
// import editSection from "./editSection";
// import scenario from "./scenario";
const put = {};
/* harmony default export */ var Put = (put);
// CONCATENATED MODULE: ./panelAdmin/api/Post/imageUpload.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const imageUpload = async (files, setLoading, type, setState, imageName) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  console.log({
    files,
    setLoading,
    type,
    setState,
    imageName
  });
  setLoading(true); // ============================================= const

  const CancelToken = external_axios_default.a.CancelToken;
  const source = CancelToken.source();
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total);
      setState(prev => _objectSpread({}, prev, {
        progressPercentImage: percentCompleted
      }));
    },
    cancelToken: source.token
  };
  const URL = strings.IMAGE_UPLOAD;
  const formData = new FormData();
  formData.append("imageName", imageName);
  formData.append("imageType", type);
  formData.append("image", files); //=============================================== axios

  return axios_orders.post(URL, formData, settings).then(Response => {
    console.log({
      Response
    });
    setLoading(false);
    return Response.data;
  }).catch(error => {
    console.log({
      error
    });
    setLoading(false);
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_imageUpload = (imageUpload);
// CONCATENATED MODULE: ./panelAdmin/api/Post/artist.js



const artist = async (param, setLoading) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.ARTIST;
  return axios_orders.post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_artist = (artist);
// CONCATENATED MODULE: ./panelAdmin/api/Post/voiceUpload.js
function voiceUpload_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function voiceUpload_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { voiceUpload_ownKeys(Object(source), true).forEach(function (key) { voiceUpload_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { voiceUpload_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function voiceUpload_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const voiceUpload = async (files, setLoading, type, setState, songName) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.strings;
  setLoading(true);
  console.log({
    files,
    setLoading,
    type,
    setState,
    songName
  }); // ============================================= const

  const CancelToken = external_axios_default.a.CancelToken;
  const source = CancelToken.source();
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total);
      setState(prev => voiceUpload_objectSpread({}, prev, {
        progressPercentSongs: percentCompleted
      }));
    },
    cancelToken: source.token
  };
  const formData = new FormData();
  formData.append("songName", songName);
  formData.append("song", files);
  const URL = strings.ApiString.SONG_UPLOAD;

  for (var pair of formData.entries()) {
    console.log(pair[0] + ", " + pair[1]);
  } // ============================================== End const ==============================================
  // ============================================== log
  //console.log("Sending ...");
  //console.log({ AxiosCancelUpload: cancelUpload });


  console.log({
    formData
  }); //console.log({ update });
  // ============================================== End log ==============================================
  // if (cancelUpload) {
  //   source.cancel();
  // }
  //=============================================== axios

  return axios_orders.post(URL, formData, settings).then(Response => {
    console.log({
      Response
    });
    setLoading(false); // update[name] = Response.data.voiceUrl;
    // return Response.data.songUrl;

    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_voiceUpload = (voiceUpload);
// CONCATENATED MODULE: ./panelAdmin/api/Post/videoUpload.js
function videoUpload_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function videoUpload_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { videoUpload_ownKeys(Object(source), true).forEach(function (key) { videoUpload_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { videoUpload_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function videoUpload_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const videoUpload = async (files, setLoading, type, setState, cancelUpload) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  let Strings = panelAdmin["a" /* default */].values.ApiString; // //console.log({ update });

  setLoading(true); // ============================================= const

  const CancelToken = external_axios_default.a.CancelToken;
  const source = CancelToken.source();
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total);
      setState(prev => videoUpload_objectSpread({}, prev, {
        progressPercentVideo: percentCompleted
      }));
    },
    cancelToken: source.token
  };
  const formData = new FormData();
  const URL = Strings.ApiString.VIDEO_UPLOAD;
  formData.append("video", files); // ============================================== End const ==============================================
  // ============================================== log
  //console.log("Sending ...");
  //console.log({ AxiosCancelUpload: cancelUpload });
  //console.log({ formData });
  // ============================================== End log ==============================================
  // if (cancelUpload) {
  //   source.cancel();
  // }
  //=============================================== axios

  return axios_orders.post(URL, formData, settings).then(Response => {
    console.log({
      Response
    });
    setLoading(false);
    return Response.data.videoUrl; // update[name] = Response.data.videoUrl;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_videoUpload = (videoUpload);
// CONCATENATED MODULE: ./panelAdmin/api/Post/genres.js



const genres_genres = async param => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.GENRES;
  return axios_orders.post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_genres = (genres_genres);
// CONCATENATED MODULE: ./panelAdmin/api/Post/country.js



const country_country = async param => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.COUNTRY;
  return axios_orders.post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_country = (country_country);
// CONCATENATED MODULE: ./panelAdmin/api/Post/instrument.js



const instrument_instrument = async (param, setLoading) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.INSTRUMENT;
  return axios_orders.post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_instrument = (instrument_instrument);
// CONCATENATED MODULE: ./panelAdmin/api/Post/mood.js



const mood_mood = async param => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.MOOD;
  return axios_orders.post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_mood = (mood_mood);
// CONCATENATED MODULE: ./panelAdmin/api/Post/hashtag.js



const hashtag_hashtag = async param => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.HASHTAG;
  return axios_orders.post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_hashtag = (hashtag_hashtag);
// CONCATENATED MODULE: ./panelAdmin/api/Post/index.js
 // import owner from "./owner";
// import category from "./category";
// import slider from "./slider";
// import login from "./login";
// import album from "./album";
// import club from "./club";
// import banner from "./banner";








 // import gallery from "./gallery";

const post = {
  imageUpload: Post_imageUpload,
  // owner,
  // category,
  // slider,
  // login,
  // album,
  // club,
  // banner,
  artist: Post_artist,
  voiceUpload: Post_voiceUpload,
  videoUpload: Post_videoUpload,
  genres: Post_genres,
  country: Post_country,
  instrument: Post_instrument,
  // gallery,
  mood: Post_mood,
  hashtag: Post_hashtag
};
/* harmony default export */ var Post = (post);
// CONCATENATED MODULE: ./panelAdmin/api/Patch/index.js
// import category from "./category";
// import editDiscount from "./discount";
// import club from "./club";
// import owner from "./owner";
// import slider from "./slider";
// import banner from "./banner";
const Patch = {// category,
  // editDiscount,
  // club,
  // owner,
  // slider,
  // banner,
};
/* harmony default export */ var api_Patch = (Patch);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/index.js
// import owner from "./owner";
// import discount from "./discount";
// import club from "./club";
// import slider from "./slider";
// import banner from "./banner";
// import category from "./category";
const Delete = {};
/* harmony default export */ var api_Delete = (Delete);
// CONCATENATED MODULE: ./panelAdmin/api/index.js







/***/ }),

/***/ "JMOJ":
/***/ (function(module, exports) {

module.exports = require("next-redux-wrapper");

/***/ }),

/***/ "K3nj":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("RfFk");
/* harmony import */ var react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_useragent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("W0sL");
/* harmony import */ var next_useragent__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_useragent__WEBPACK_IMPORTED_MODULE_2__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const AwesomeScroll = props => {
  const {
    data,
    Row,
    ua,
    scrollBar
  } = props; // console.log({ data, Row });
  // console.log({ ua });

  const scrollRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null); // console.log({ AwesomeScroll: data });

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    if (scrollRef.current) {
      const slider = scrollRef.current.children[0];
      let isDown = false;
      let startX;
      let scrollLeft;
      slider.addEventListener("mousedown", e => {
        isDown = true; // slider.classList.add("active");

        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
      });
      slider.addEventListener("mouseleave", () => {
        isDown = false; // slider.classList.remove("active");
      });
      slider.addEventListener("mouseup", () => {
        isDown = false; // slider.classList.remove("active");
      });
      slider.addEventListener("mousemove", e => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = x - startX; //scroll-fast
        // console.log({ x, walk }, (slider.scrollLeft = scrollLeft - walk));

        slider.scrollLeft = scrollLeft - walk;
      });
    }
  }, []);
  return __jsx("div", null, !scrollBar ? __jsx("div", {
    ref: scrollRef,
    className: "awesome-scroll-container"
  }, props.children) : ua && ua.isMobile ? __jsx("div", {
    className: "awesome-scroll-container"
  }, props.children) : __jsx("div", {
    ref: scrollRef,
    className: "awesome-scroll-container"
  }, __jsx(react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1___default.a, null, props.children)));
};

AwesomeScroll.getInitialProps = async ctx => {
  return {
    useragent: ctx.ua.source
  };
};

/* harmony default export */ __webpack_exports__["a"] = (Object(next_useragent__WEBPACK_IMPORTED_MODULE_2__["withUserAgent"])(AwesomeScroll));

/***/ }),

/***/ "Osoz":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/router-context.js");

/***/ }),

/***/ "RfFk":
/***/ (function(module, exports) {

module.exports = require("react-perfect-scrollbar");

/***/ }),

/***/ "RmXt":
/***/ (function(module, exports) {

module.exports = require("redux-saga/effects");

/***/ }),

/***/ "TqRt":
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "VGcP":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// NAMESPACE OBJECT: ./panelAdmin/storeAdmin/actions/redux/index.js
var actions_redux_namespaceObject = {};
__webpack_require__.r(actions_redux_namespaceObject);
__webpack_require__.d(actions_redux_namespaceObject, "setPageName", function() { return setPageName; });
__webpack_require__.d(actions_redux_namespaceObject, "setUploadImage", function() { return setUploadImage; });
__webpack_require__.d(actions_redux_namespaceObject, "setGalleryData", function() { return redux_setGalleryData; });
__webpack_require__.d(actions_redux_namespaceObject, "changeAddGalleryData", function() { return changeAddGalleryData; });
__webpack_require__.d(actions_redux_namespaceObject, "setArtistData", function() { return setArtistData; });
__webpack_require__.d(actions_redux_namespaceObject, "setSearchArtistData", function() { return setSearchArtistData; });
__webpack_require__.d(actions_redux_namespaceObject, "startGetArtist", function() { return startGetArtist; });
__webpack_require__.d(actions_redux_namespaceObject, "startSearchArtist", function() { return startSearchArtist; });
__webpack_require__.d(actions_redux_namespaceObject, "setInstrumentData", function() { return setInstrumentData; });
__webpack_require__.d(actions_redux_namespaceObject, "setSearchInstrumentData", function() { return setSearchInstrumentData; });
__webpack_require__.d(actions_redux_namespaceObject, "startGetInstrument", function() { return startGetInstrument; });
__webpack_require__.d(actions_redux_namespaceObject, "startSearchInstrument", function() { return startSearchInstrument; });
__webpack_require__.d(actions_redux_namespaceObject, "setCountryData", function() { return setCountryData; });
__webpack_require__.d(actions_redux_namespaceObject, "setSearchCountryData", function() { return setSearchCountryData; });
__webpack_require__.d(actions_redux_namespaceObject, "startGetCountry", function() { return startGetCountry; });
__webpack_require__.d(actions_redux_namespaceObject, "startSearchCountry", function() { return startSearchCountry; });
__webpack_require__.d(actions_redux_namespaceObject, "setMoodData", function() { return setMoodData; });
__webpack_require__.d(actions_redux_namespaceObject, "setSearchMoodData", function() { return setSearchMoodData; });
__webpack_require__.d(actions_redux_namespaceObject, "startGetMood", function() { return startGetMood; });
__webpack_require__.d(actions_redux_namespaceObject, "startSearchMood", function() { return startSearchMood; });
__webpack_require__.d(actions_redux_namespaceObject, "setHashtagData", function() { return setHashtagData; });
__webpack_require__.d(actions_redux_namespaceObject, "setSearchHashtagData", function() { return setSearchHashtagData; });
__webpack_require__.d(actions_redux_namespaceObject, "startGetHashtag", function() { return startGetHashtag; });
__webpack_require__.d(actions_redux_namespaceObject, "startSearchHashtag", function() { return startSearchHashtag; });
__webpack_require__.d(actions_redux_namespaceObject, "setGenreData", function() { return setGenreData; });
__webpack_require__.d(actions_redux_namespaceObject, "setSearchGenreData", function() { return setSearchGenreData; });
__webpack_require__.d(actions_redux_namespaceObject, "startGetGenre", function() { return startGetGenre; });
__webpack_require__.d(actions_redux_namespaceObject, "startSearchGenre", function() { return startSearchGenre; });

// NAMESPACE OBJECT: ./panelAdmin/storeAdmin/actions/saga/index.js
var actions_saga_namespaceObject = {};
__webpack_require__.r(actions_saga_namespaceObject);
__webpack_require__.d(actions_saga_namespaceObject, "uploadImageData", function() { return uploadImageData; });
__webpack_require__.d(actions_saga_namespaceObject, "getGalleryData", function() { return getGalleryData; });
__webpack_require__.d(actions_saga_namespaceObject, "getArtistData", function() { return getArtistData; });
__webpack_require__.d(actions_saga_namespaceObject, "getSearchArtistData", function() { return getSearchArtistData; });
__webpack_require__.d(actions_saga_namespaceObject, "getInstrumentData", function() { return getInstrumentData; });
__webpack_require__.d(actions_saga_namespaceObject, "getSearchInstrumentData", function() { return getSearchInstrumentData; });
__webpack_require__.d(actions_saga_namespaceObject, "getCountryData", function() { return getCountryData; });
__webpack_require__.d(actions_saga_namespaceObject, "getSearchCountryData", function() { return getSearchCountryData; });
__webpack_require__.d(actions_saga_namespaceObject, "getMoodData", function() { return getMoodData; });
__webpack_require__.d(actions_saga_namespaceObject, "getSearchMoodData", function() { return getSearchMoodData; });
__webpack_require__.d(actions_saga_namespaceObject, "getAlbumData", function() { return getAlbumData; });
__webpack_require__.d(actions_saga_namespaceObject, "getSearchAlbumData", function() { return getSearchAlbumData; });
__webpack_require__.d(actions_saga_namespaceObject, "getHashtagData", function() { return getHashtagData; });
__webpack_require__.d(actions_saga_namespaceObject, "getSearchHashtagData", function() { return getSearchHashtagData; });
__webpack_require__.d(actions_saga_namespaceObject, "getGenreData", function() { return getGenreData; });
__webpack_require__.d(actions_saga_namespaceObject, "getSearchGenreData", function() { return getSearchGenreData; });

// NAMESPACE OBJECT: ./panelAdmin/storeAdmin/saga/index.js
var storeAdmin_saga_namespaceObject = {};
__webpack_require__.r(storeAdmin_saga_namespaceObject);
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchUploadImage", function() { return watchUploadImage; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGallery", function() { return watchGallery; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGetArtistData", function() { return watchGetArtistData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchSearchArtistData", function() { return watchSearchArtistData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGetInstrumentalData", function() { return watchGetInstrumentalData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchSearchInstrumentalData", function() { return watchSearchInstrumentalData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGetCountryData", function() { return watchGetCountryData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchSearchCountryData", function() { return watchSearchCountryData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGetMoodData", function() { return watchGetMoodData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchSearchMoodData", function() { return watchSearchMoodData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGetHashtagData", function() { return watchGetHashtagData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchSearchHashtagData", function() { return watchSearchHashtagData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGetGenreData", function() { return watchGetGenreData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchSearchGenreData", function() { return watchSearchGenreData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGetAlbumData", function() { return watchGetAlbumData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchSearchAlbumData", function() { return watchSearchAlbumData; });

// CONCATENATED MODULE: ./panelAdmin/values/strings/en/navbar.js
const APP_NAME = "Rimtal";
const EXPLORE = "Explore";
const TRACKS = "Tracks";
const PLAYLISTS = "Playlists";
const ALBUMS = "Albums";
const ARTISTS = "Artists";
const VIDEOS = "Videos";
const SIGN_IN = "Sign In";
const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN
};
/* harmony default export */ var en_navbar = (navbar);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/sideMenu.js
const DASHBOARD = "داشبورد";
const SIDEBAR_ONE_TITLE = "عمومی";
const SIDEBAR_TWO_TITLE = "کاربردی";
const SETTING_WEB = "تنظیمات سایت";
const CATEGORIES = "دسته بندی ها";
const SEE_ARTIST = "مشاهده هنرمندان";
const ARTIST = "هنرمند";
const ADD_ARTIST = "افزودن هنرمند";
const SEE_ALBUM = "مشاهده آلبوم ها";
const ALBUM = "آلبوم";
const ADD_ALBUM = "افزودن آلبوم";
const SEE_GENRES = "مشاهده ژانر ها";
const GENRES = "ژانر";
const ADD_GENRE = "افزودن ژانر";
const SEE_COUNTRY = "مشاهده کشور ها";
const COUNTRY = "کشور";
const ADD_COUNTRY = "افزودن کشور";
const SEE_INSTRUMENT = "مشاهده ساز ها";
const INSTRUMENT = "ساز";
const ADD_INSTRUMENT = "افزودن ساز";
const SEE_GALLERY = "مشاهده گالری ها";
const GALLERY = "گالری";
const SEE_SONG = "مشاهده آهنگ ها";
const SONG = "آهنگ";
const SEE_MOOD = "مشاهده حالت ها";
const MOOD = "حالت";
const sideMenu = {
  SEE_GALLERY,
  GALLERY,
  DASHBOARD,
  SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE,
  SETTING_WEB,
  CATEGORIES,
  SEE_ARTIST,
  ARTIST,
  ADD_ARTIST,
  ALBUM,
  SEE_ALBUM,
  ADD_ALBUM,
  SEE_GENRES,
  GENRES,
  ADD_GENRE,
  SEE_COUNTRY,
  COUNTRY,
  ADD_COUNTRY,
  SEE_INSTRUMENT,
  INSTRUMENT,
  ADD_INSTRUMENT,
  SEE_SONG,
  SONG,
  SEE_MOOD,
  MOOD
};
/* harmony default export */ var en_sideMenu = (sideMenu);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/global.js
const global_TRACKS = "Tracks";
const NO_ENTRIES = "no entries";
const FOLLOWERS = "followers";
const global = {
  TRACKS: global_TRACKS,
  NO_ENTRIES: NO_ENTRIES,
  FOLLOWERS
};
/* harmony default export */ var en_global = (global);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/constants.js
const ALBUM_CONSTANTS = "album";
const PLAY_LIST_CONSTANTS = "playlist";
const SONG_CONSTANTS = "song";
const FLAG_CONSTANTS = "flag";
const ARTIST_CONSTANTS = "artist";
const INSTRUMENT_CONSTANTS = "instrument";
const MUSIC_VIDEO_CONSTANTS = "musicvideo";
const MOOD_CONSTANTS = "mood";
const constants = {
  ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS,
  FLAG_CONSTANTS,
  ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS
};
/* harmony default export */ var en_constants = (constants);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/index.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const en = _objectSpread({}, en_navbar, {}, en_sideMenu, {}, en_global, {}, en_constants);

/* harmony default export */ var strings_en = (en);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/navbar.js
const navbar_APP_NAME = "ریمتال";
const navbar_EXPLORE = "کاوش کردن";
const navbar_TRACKS = "آهنگ ها";
const navbar_PLAYLISTS = "لیست های پخش";
const navbar_ALBUMS = "آلبوم ها";
const navbar_ARTISTS = "هنرمندان";
const navbar_VIDEOS = "ویدیو ها";
const navbar_SIGN_IN = "ورود";
const navbar_navbar = {
  APP_NAME: navbar_APP_NAME,
  EXPLORE: navbar_EXPLORE,
  TRACKS: navbar_TRACKS,
  PLAYLISTS: navbar_PLAYLISTS,
  ALBUMS: navbar_ALBUMS,
  ARTISTS: navbar_ARTISTS,
  VIDEOS: navbar_VIDEOS,
  SIGN_IN: navbar_SIGN_IN
};
/* harmony default export */ var fa_navbar = (navbar_navbar);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/sideMenu.js
const sideMenu_DASHBOARD = "داشبورد";
const sideMenu_SIDEBAR_ONE_TITLE = "عمومی";
const sideMenu_SIDEBAR_TWO_TITLE = "کاربردی";
const sideMenu_SETTING_WEB = "تنظیمات سایت";
const sideMenu_CATEGORIES = "دسته بندی ها";
const sideMenu_SEE_ARTIST = "مشاهده هنرمندان";
const sideMenu_ARTIST = "هنرمند";
const sideMenu_ADD_ARTIST = "افزودن هنرمند";
const sideMenu_SEE_ALBUM = "مشاهده آلبوم ها";
const sideMenu_ALBUM = "آلبوم";
const sideMenu_ADD_ALBUM = "افزودن آلبوم";
const sideMenu_SEE_GENRES = "مشاهده ژانر ها";
const sideMenu_GENRES = "ژانر";
const sideMenu_ADD_GENRE = "افزودن ژانر";
const sideMenu_SEE_COUNTRY = "مشاهده کشور ها";
const sideMenu_COUNTRY = "کشور";
const sideMenu_ADD_COUNTRY = "افزودن کشور";
const sideMenu_SEE_INSTRUMENT = "مشاهده ساز ها";
const sideMenu_INSTRUMENT = "ساز";
const sideMenu_ADD_INSTRUMENT = "افزودن ساز";
const SEE_GALLERIES = "مشاهده گالری ها";
const sideMenu_GALLERY = "گالری";
const sideMenu_SEE_SONG = "مشاهده آهنگ ها";
const sideMenu_SONG = "آهنگ";
const sideMenu_SEE_MOOD = "مشاهده حالت ها";
const sideMenu_MOOD = "حالت";
const ADD_MOOD = "افزودن حالت";
const SEE_HASHTAG = "مشاهده هشتگ ها";
const HASHTAG = "هشتگ";
const ADD_HASHTAG = "افزودن هشتگ";
const sideMenu_sideMenu = {
  SEE_GALLERIES,
  GALLERY: sideMenu_GALLERY,
  DASHBOARD: sideMenu_DASHBOARD,
  SIDEBAR_ONE_TITLE: sideMenu_SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE: sideMenu_SIDEBAR_TWO_TITLE,
  SETTING_WEB: sideMenu_SETTING_WEB,
  CATEGORIES: sideMenu_CATEGORIES,
  SEE_ARTIST: sideMenu_SEE_ARTIST,
  ARTIST: sideMenu_ARTIST,
  ADD_ARTIST: sideMenu_ADD_ARTIST,
  ALBUM: sideMenu_ALBUM,
  SEE_ALBUM: sideMenu_SEE_ALBUM,
  ADD_ALBUM: sideMenu_ADD_ALBUM,
  SEE_GENRES: sideMenu_SEE_GENRES,
  GENRES: sideMenu_GENRES,
  ADD_GENRE: sideMenu_ADD_GENRE,
  SEE_COUNTRY: sideMenu_SEE_COUNTRY,
  COUNTRY: sideMenu_COUNTRY,
  ADD_COUNTRY: sideMenu_ADD_COUNTRY,
  SEE_INSTRUMENT: sideMenu_SEE_INSTRUMENT,
  INSTRUMENT: sideMenu_INSTRUMENT,
  ADD_INSTRUMENT: sideMenu_ADD_INSTRUMENT,
  SEE_SONG: sideMenu_SEE_SONG,
  SONG: sideMenu_SONG,
  SEE_MOOD: sideMenu_SEE_MOOD,
  MOOD: sideMenu_MOOD,
  ADD_MOOD,
  SEE_HASHTAG,
  HASHTAG,
  ADD_HASHTAG
};
/* harmony default export */ var fa_sideMenu = (sideMenu_sideMenu);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/global.js
const fa_global_TRACKS = "آهنگ ها ";
const global_NO_ENTRIES = "وارد نشده";
const global_FOLLOWERS = "دنبال کنندگان";
const global_global = {
  TRACKS: fa_global_TRACKS,
  NO_ENTRIES: global_NO_ENTRIES,
  FOLLOWERS: global_FOLLOWERS
};
/* harmony default export */ var fa_global = (global_global);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/constants.js
const constants_ALBUM_CONSTANTS = "آلبوم";
const constants_PLAY_LIST_CONSTANTS = "لیست پخش";
const constants_SONG_CONSTANTS = "موزیک";
const constants_FLAG_CONSTANTS = "پرچم";
const constants_ARTIST_CONSTANTS = "هنرمند";
const constants_INSTRUMENT_CONSTANTS = "ساز";
const constants_MUSIC_VIDEO_CONSTANTS = "موزیک ویدئو";
const constants_MOOD_CONSTANTS = "حالت";
const constants_constants = {
  ALBUM_CONSTANTS: constants_ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS: constants_PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS: constants_SONG_CONSTANTS,
  FLAG_CONSTANTS: constants_FLAG_CONSTANTS,
  ARTIST_CONSTANTS: constants_ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS: constants_INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS: constants_MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS: constants_MOOD_CONSTANTS
};
/* harmony default export */ var fa_constants = (constants_constants);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/index.js
function fa_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function fa_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { fa_ownKeys(Object(source), true).forEach(function (key) { fa_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { fa_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function fa_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const fa = fa_objectSpread({}, fa_navbar, {}, fa_sideMenu, {}, fa_global, {}, fa_constants);

/* harmony default export */ var strings_fa = (fa);
// CONCATENATED MODULE: ./panelAdmin/values/strings/index.js


const Lang = "rtl";
let strings_strings;
if (Lang === "rtl") strings_strings = strings_fa;else strings_strings = strings_en;
/* harmony default export */ var values_strings = (strings_strings);
// CONCATENATED MODULE: ./panelAdmin/values/routes/index.js
const GS_PANEL_ADMIN_TITLE = "/panelAdmin";
const GS_ADMIN_DASHBOARD = GS_PANEL_ADMIN_TITLE + "/dashboard";
const GS_ADMIN_ARTIST = GS_PANEL_ADMIN_TITLE + "/artist";
const GS_ADMIN_ADD_ARTIST = GS_PANEL_ADMIN_TITLE + "/addArtist";
const GS_ADMIN_ALBUM = GS_PANEL_ADMIN_TITLE + "/album";
const GS_ADMIN_ADD_ALBUM = GS_PANEL_ADMIN_TITLE + "/addAlbum";
const GS_ADMIN_GENRE = GS_PANEL_ADMIN_TITLE + "/genre";
const GS_ADMIN_ADD_GENRE = GS_PANEL_ADMIN_TITLE + "/addGenre";
const GS_ADMIN_COUNTRY = GS_PANEL_ADMIN_TITLE + "/country";
const GS_ADMIN_ADD_COUNTRY = GS_PANEL_ADMIN_TITLE + "/addCountry";
const GS_ADMIN_INSTRUMENT = GS_PANEL_ADMIN_TITLE + "/instrument";
const GS_ADMIN_ADD_INSTRUMENT = GS_PANEL_ADMIN_TITLE + "/addInstrument";
const GS_ADMIN_GALLERY = GS_PANEL_ADMIN_TITLE + "/gallery";
const GS_ADMIN_SONG = GS_PANEL_ADMIN_TITLE + "/song";
const GS_ADMIN_MOOD = GS_PANEL_ADMIN_TITLE + "/mood";
const GS_ADMIN_ADD_MOOD = GS_PANEL_ADMIN_TITLE + "/addMood";
const GS_ADMIN_HASHTAG = GS_PANEL_ADMIN_TITLE + "/hashtag";
const GS_ADMIN_ADD_HASHTAG = GS_PANEL_ADMIN_TITLE + "/addHashtag"; // const GS_ADMIN_GALLERY = GS_PANEL_ADMIN_TITLE + "/gallery";
// const GS_ADMIN_ADD_GALLERY = GS_PANEL_ADMIN_TITLE + "/addGallery";

const routes = {
  GS_ADMIN_DASHBOARD,
  GS_PANEL_ADMIN_TITLE,
  GS_ADMIN_ARTIST,
  GS_ADMIN_ADD_ARTIST,
  GS_ADMIN_ALBUM,
  GS_ADMIN_ADD_ALBUM,
  GS_ADMIN_GENRE,
  GS_ADMIN_ADD_GENRE,
  GS_ADMIN_COUNTRY,
  GS_ADMIN_ADD_COUNTRY,
  GS_ADMIN_INSTRUMENT,
  GS_ADMIN_ADD_INSTRUMENT,
  GS_ADMIN_GALLERY,
  GS_ADMIN_SONG,
  GS_ADMIN_MOOD,
  GS_ADMIN_ADD_MOOD,
  GS_ADMIN_HASHTAG,
  GS_ADMIN_ADD_HASHTAG
};
/* harmony default export */ var values_routes = (routes);
// CONCATENATED MODULE: ./panelAdmin/values/apiString.js
const ADMIN = "/admin";
const SONG_UPLOAD = ADMIN + "/songupload";
const UPLOAD = ADMIN + "/upload";
const apiString_ALBUM = ADMIN + "/album";
const LOGIN = ADMIN + "/login";
const IMAGE_UPLOAD = ADMIN + "/upload/image";
const apiString_GENRES = ADMIN + "/genre";
const apiString_ARTIST = ADMIN + "/artist";
const ARTIST_SEARCH = apiString_ARTIST + "/search";
const apiString_COUNTRY = ADMIN + "/country";
const apiString_INSTRUMENT = ADMIN + "/instrument";
const apiString_GALLERY = ADMIN + "/gallery";
const apiString_SONG = ADMIN + "/songlibrary";
const apiString_MOOD = ADMIN + "/mood";
const apiString_HASHTAG = ADMIN + "/hashtag";
const apiString = {
  SONG_UPLOAD,
  ALBUM: apiString_ALBUM,
  LOGIN,
  UPLOAD,
  IMAGE_UPLOAD,
  GENRES: apiString_GENRES,
  ARTIST_SEARCH,
  COUNTRY: apiString_COUNTRY,
  INSTRUMENT: apiString_INSTRUMENT,
  GALLERY: apiString_GALLERY,
  SONG: apiString_SONG,
  ARTIST: apiString_ARTIST,
  MOOD: apiString_MOOD,
  HASHTAG: apiString_HASHTAG
};
/* harmony default export */ var values_apiString = (apiString);
// CONCATENATED MODULE: ./panelAdmin/values/strings/constants.js
const strings_constants_ALBUM_CONSTANTS = "album";
const strings_constants_PLAY_LIST_CONSTANTS = "playlist";
const strings_constants_SONG_CONSTANTS = "song";
const strings_constants_FLAG_CONSTANTS = "flag";
const strings_constants_ARTIST_CONSTANTS = "artist";
const strings_constants_INSTRUMENT_CONSTANTS = "instrument";
const strings_constants_MUSIC_VIDEO_CONSTANTS = "musicvideo";
const strings_constants_MOOD_CONSTANTS = "mood";
const strings_constants_constants = {
  ALBUM_CONSTANTS: strings_constants_ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS: strings_constants_PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS: strings_constants_SONG_CONSTANTS,
  FLAG_CONSTANTS: strings_constants_FLAG_CONSTANTS,
  ARTIST_CONSTANTS: strings_constants_ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS: strings_constants_INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS: strings_constants_MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS: strings_constants_MOOD_CONSTANTS
};
/* harmony default export */ var strings_constants = (strings_constants_constants);
// CONCATENATED MODULE: ./panelAdmin/values/index.js






const values = {
  routes: values_routes,
  strings: values_strings,
  apiString: values_apiString,
  constants: strings_constants,
  fa: strings_fa,
  en: strings_en
};
/* harmony default export */ var panelAdmin_values = (values);
// CONCATENATED MODULE: ./panelAdmin/utils/menuFormat.js


const menuFormat = [{
  title: "عمومی",
  menus: [{
    route: panelAdmin_values.routes.GS_ADMIN_DASHBOARD,
    menuTitle: panelAdmin_values.strings.DASHBOARD,
    menuIconImg: false,
    menuIconClass: "fas fa-tachometer-slowest",
    subMenu: [// { title: "SubDashboard", route: "/dashboard1" },
      // { title: "SubDashboard", route: "/dashboard2" }
    ]
  }]
}, {
  title: "کاربردی",
  menus: [{
    route: false,
    menuTitle: panelAdmin_values.strings.ARTIST,
    menuIconImg: false,
    menuIconClass: "fas fa-user-music",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_ARTIST,
      route: panelAdmin_values.routes.GS_ADMIN_ARTIST
    }, {
      title: panelAdmin_values.strings.ADD_ARTIST,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_ARTIST
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.ALBUM,
    menuIconImg: false,
    menuIconClass: "fas fa-album",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_ALBUM,
      route: panelAdmin_values.routes.GS_ADMIN_ALBUM
    }, {
      title: panelAdmin_values.strings.ADD_ALBUM,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_ALBUM
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.GENRES,
    menuIconImg: false,
    menuIconClass: "fas fa-album",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_GENRES,
      route: panelAdmin_values.routes.GS_ADMIN_GENRE
    }, {
      title: panelAdmin_values.strings.ADD_GENRE,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_GENRE
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.COUNTRY,
    menuIconImg: false,
    menuIconClass: "fas fa-flag",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_COUNTRY,
      route: panelAdmin_values.routes.GS_ADMIN_COUNTRY
    }, {
      title: panelAdmin_values.strings.ADD_COUNTRY,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_COUNTRY
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.INSTRUMENT,
    menuIconImg: false,
    menuIconClass: "fas fa-saxophone",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_INSTRUMENT,
      route: panelAdmin_values.routes.GS_ADMIN_INSTRUMENT
    }, {
      title: panelAdmin_values.strings.ADD_INSTRUMENT,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_INSTRUMENT
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.GALLERY,
    menuIconImg: false,
    menuIconClass: "fad fa-images",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_GALLERIES,
      route: panelAdmin_values.routes.GS_ADMIN_GALLERY
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.SONG,
    menuIconImg: false,
    menuIconClass: "fad fa-comment-music",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_SONG,
      route: panelAdmin_values.routes.GS_ADMIN_SONG
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.MOOD,
    menuIconImg: false,
    menuIconClass: "fas fa-album",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_MOOD,
      route: panelAdmin_values.routes.GS_ADMIN_MOOD
    }, {
      title: panelAdmin_values.strings.ADD_MOOD,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_MOOD
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.HASHTAG,
    menuIconImg: false,
    menuIconClass: "fas fa-album",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_HASHTAG,
      route: panelAdmin_values.routes.GS_ADMIN_HASHTAG
    }, {
      title: panelAdmin_values.strings.ADD_HASHTAG,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_HASHTAG
    }]
  }]
}];
/* harmony default export */ var utils_menuFormat = (menuFormat);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/genres.js


const genres = data => {
  const thead = ["#", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_genres = (genres);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowScenario.js
var __jsx = external_react_default.a.createElement;


const ShowScenario = scenario => {
  const thead = ["#", "نام", "تاریخ شروع ", " تاریخ پایان", "تعداد برندگان ", "شركت كنندگان", "برندگان", "جوایز", "پوچ ها", "زمان چرخش مجدد (روز)", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < scenario.length; index++) {
    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    tbody.push({
      data: [scenario[index].name, scenario[index].startDate, scenario[index].endDate, scenario[index].winnersCount, {
        option: {
          eye: true,
          name: "participants"
        }
      }, {
        option: {
          eye: true,
          name: "winners"
        }
      }, {
        option: {
          eye: true,
          name: "gifts"
        }
      }, {
        option: {
          eye: true,
          name: "empty"
        }
      }, scenario[index].spinRepeatTime, scenario[index].isActive ? active : deActive, {
        option: {
          edit: true
        }
      }],
      style: {
        background: scenario[index].isActive ? "rgb(100, 221, 23,0.3)" : ""
      }
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowScenario = (ShowScenario);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowScenarioDataInModal.js


const ShowScenarioDataInModal = (data, name) => {
  const NotEntered = "وارد نشده";
  let thead = null;
  const headGift = ["شماره", "نام", "شماره همراه", "جایزه", " تاریخ "];
  const headNotGift = ["شماره", "نام", "شماره همراه", " تاریخ "];
  name === "participants" ? thead = headNotGift : thead = headGift;
  let tbody = [];
  let body = null;

  for (let index = 0; index < data.length; index++) {
    let fullName = data[index].fullName ? data[index].fullName : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let gift = data[index].gift ? data[index].gift : NotEntered;
    let date = data[index].date ? data[index].date : NotEntered;
    const bodyGift = [fullName, phoneNumber, gift, date];
    const bodyNotGift = [fullName, phoneNumber, date];
    name === "participants" ? body = bodyNotGift : body = bodyGift;
    tbody.push({
      data: body,
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowScenarioDataInModal = (ShowScenarioDataInModal);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowScenarioDataInModalTwo.js


const ShowScenarioDataInModalTwo = (data, name) => {
  const NotEntered = "وارد نشده";
  const thead = ["شماره", name];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    tbody.push({
      data: [data[index] ? data[index] : NotEntered],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowScenarioDataInModalTwo = (ShowScenarioDataInModalTwo);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowOwner.js
var ShowOwner_jsx = external_react_default.a.createElement;


const ShowOwner = scenario => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "محدوده  ", " شماره همراه", "موجودی", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < scenario.length; index++) {
    let NotEntered = "وارد نشده";

    let active = ShowOwner_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = ShowOwner_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let thumbnail = scenario[index].thumbnail ? scenario[index].thumbnail : NotEntered;
    let title = scenario[index].title ? scenario[index].title : NotEntered;
    let rating = scenario[index].rating ? scenario[index].rating : NotEntered;
    let district = scenario[index].district ? scenario[index].district : NotEntered;
    let phoneNumber = scenario[index].phoneNumber ? scenario[index].phoneNumber : NotEntered;
    let balance = scenario[index].balance ? scenario[index].balance : "0";
    let isActive = scenario[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, district, phoneNumber, balance, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowOwner = (ShowOwner);
// CONCATENATED MODULE: ./panelAdmin/utils/formatMoney.js
const formatMoney = number => {
  return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

/* harmony default export */ var utils_formatMoney = (formatMoney);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowDiscount.js
var ShowDiscount_jsx = external_react_default.a.createElement;



const ShowDiscount = data => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "دسته بندی  ", "قیمت اصلی", "قیمت جدید", " تخفیف", "فروش", "بازدید ", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = ShowDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = ShowDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let realPrice = data[index].realPrice ? utils_formatMoney(data[index].realPrice) : NotEntered;
    let newPrice = data[index].newPrice ? utils_formatMoney(data[index].newPrice) : NotEntered;
    let rating = data[index].rating ? data[index].rating : 0;
    let percent = data[index].percent ? "%" + data[index].percent : NotEntered;
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : NotEntered;
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, categoryTitleFa, realPrice, newPrice, percent, boughtCount, viewCount, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowDiscount = (ShowDiscount);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowClub.js
var ShowClub_jsx = external_react_default.a.createElement;


const ShowClub = data => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "عضویت ", "دسته بندی ", " تخفیف", "فروش", "بازدید ", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let cardElement = ShowClub_jsx("i", {
      style: {
        fontSize: "1.2em"
      },
      className: "fal fa-credit-card"
    });

    let applicationElement = ShowClub_jsx("i", {
      style: {
        fontSize: "1.2em"
      },
      className: "fal fa-mobile-android"
    });

    let active = ShowClub_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = ShowClub_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let NotEntered = "وارد نشده";
    let thumbnail = data[index].slides[0] ? data[index].slides[0] : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : 0;
    let percent = data[index].percent ? data[index].percent + " %" : NotEntered;
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : "";
    let membership = data[index].membership.length ? data[index].membership : NotEntered;
    let membershipData = membership.includes("CARD") && membership.includes("APPLICATION") ? ShowClub_jsx("div", {
      style: {
        fontSize: "1.2em",
        fontWeight: "900",
        display: "flex",
        justifyContent: "space-around"
      }
    }, " ", ShowClub_jsx("div", null, cardElement, " "), ShowClub_jsx("div", null, " ", applicationElement)) : membership.includes("APPLICATION") ? applicationElement : membership.includes("CARD") ? cardElement : "";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, membershipData, categoryTitleFa, percent, boughtCount, viewCount, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowClub = (ShowClub);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowTransactions.js
var ShowTransactions_jsx = external_react_default.a.createElement;



const ShowTransactions = data => {
  const thead = ["#", "نام", " شماره همراه ", " تاریخ (زمان) ", " کل مبلغ", "تخفیف", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let NotEntered = "وارد نشده";
    let user = data[index].user ? data[index].user : NotEntered;
    let userName = user ? user.name : NotEntered;
    let userPhoneNumber = user ? user.phoneNumber : NotEntered;
    let date = dateSplit ? ShowTransactions_jsx("span", null, dateSplit[1] + " - " + dateSplit[0]) : NotEntered;
    let totalPrice = data[index].totalPrice ? utils_formatMoney(data[index].totalPrice) : "0";
    let showDiscount = {
      option: {
        eye: true,
        name: "discounts"
      }
    };
    let paymentStatus = data[index].paymentStatus ? ShowTransactions_jsx("span", {
      style: {
        color: "green"
      }
    }, "پرداخت شده") : ShowTransactions_jsx("span", {
      style: {
        color: "red"
      }
    }, " ", "پرداخت نشده");
    tbody.push({
      data: [userName, userPhoneNumber, date, totalPrice, showDiscount, paymentStatus],
      style: {
        background: data[index].isActive ? "rgb(100, 221, 23,0.3)" : ""
      }
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowTransactions = (ShowTransactions);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/transactionDiscount.js
var transactionDiscount_jsx = external_react_default.a.createElement;



const transactionDiscount = data => {
  // let data =datas.discount
  const thead = ["#", "عکس", "نام ", " امتیاز ", "دسته بندی  ", "قیمت اصلی", "قیمت جدید", " تخفیف", "فروش", "بازدید ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = transactionDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = transactionDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let dataIndex = data[index].discount;
    let thumbnail = dataIndex.thumbnail ? dataIndex.thumbnail : NotEntered;
    let title = dataIndex.title ? dataIndex.title : NotEntered;
    let realPrice = dataIndex.realPrice ? utils_formatMoney(dataIndex.realPrice) : NotEntered;
    let newPrice = dataIndex.newPrice ? utils_formatMoney(dataIndex.newPrice) : NotEntered;
    let rating = dataIndex.rating ? dataIndex.rating : 0;
    let percent = dataIndex.percent ? "%" + dataIndex.percent : NotEntered;
    let boughtCount = dataIndex.boughtCount ? dataIndex.boughtCount : "0";
    let category = dataIndex.category ? dataIndex.category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : NotEntered;
    let viewCount = dataIndex.viewCount ? dataIndex.viewCount : "0";
    let isActive = dataIndex.isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, categoryTitleFa, realPrice, newPrice, percent, boughtCount, viewCount, isActive],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_transactionDiscount = (transactionDiscount);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/members.js
var members_jsx = external_react_default.a.createElement;


const members = data => {
  const thead = ["#", "نام", "شماره همراه", "تراکنش ها", "تخفیف ها", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = members_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = members_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    }); // let avatar = data[index].avatar ? data[index].avatar : NotEntered;


    let name = data[index].name ? data[index].name : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let showTransaction = {
      option: {
        eye: true,
        name: "transactions"
      }
    };
    let showDiscount = {
      option: {
        eye: true,
        name: "discounts"
      }
    };
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [name, phoneNumber, showTransaction, showDiscount, isActive],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_members = (members);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/memberTransaction.js
var memberTransaction_jsx = external_react_default.a.createElement;



const memberTransaction = data => {
  console.log({
    data
  }); // let data =datas.discount

  const thead = ["#", "تاریخ", "قیمت ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dataIndex = data[index];
    let paymentStatus = data[index].paymentStatus ? memberTransaction_jsx("span", {
      style: {
        color: "green"
      }
    }, "پرداخت شده") : memberTransaction_jsx("span", {
      style: {
        color: "red"
      }
    }, " ", "پرداخت نشده");
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let date = dateSplit ? memberTransaction_jsx("span", null, dateSplit[1] + " - " + dateSplit[0]) : NotEntered;
    let totalPrice = dataIndex.totalPrice ? utils_formatMoney(dataIndex.totalPrice) : "0";
    tbody.push({
      data: [date, totalPrice, paymentStatus],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_memberTransaction = (memberTransaction);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/memberDiscount.js
var memberDiscount_jsx = external_react_default.a.createElement;



const memberDiscount = data => {
  console.log({
    data
  }); // let data =datas.discount

  const thead = ["#", "نام", "قیمت ", "تاریخ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dataIndex = data[index];
    let discount = data[index].discount;
    let discountName = discount.title;
    let dateSplit = data[index].usedDate ? data[index].usedDate.split(" ") : "";
    let date = dateSplit ? memberDiscount_jsx("span", null, dateSplit[1] + " - " + dateSplit[0]) : "استفاده نشده";
    let price = dataIndex.price ? utils_formatMoney(dataIndex.price) : "0";
    let isUsed = dataIndex.isUsed ? "استفاده شده" : "استفاده نشده";
    tbody.push({
      data: [discountName, price, date, isUsed],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_memberDiscount = (memberDiscount);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/country.js


const country = data => {
  const thead = ["#", "عکس", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let flag = data[index].flag ? data[index].flag : NotEntered;
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [flag, titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_country = (country);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/instrument.js
const instrument = data => {
  const thead = ["#", "عکس", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [thumbnail, titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_instrument = (instrument);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/song.js


const song = data => {
  const thead = ["#", "نام ", "پخش "];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let title = data[index].title ? data[index].title : NotEntered;
    tbody.push({
      data: [title, {
        option: {
          play: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_song = (song);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/artist.js


const artist = data => {
  console.log({
    data
  });
  const thead = ["#", "نام فارسی", "نام انگلیسی", "توضیحات فارسی ", "توضیحات انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", " لایک", " شنوندگان", " ساز", " بازدید", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let nameFa = data[index].nameFa ? data[index].nameFa : NotEntered;
    let nameEn = data[index].nameEn ? data[index].nameEn : NotEntered;
    let descriptionFa = data[index].descriptionFa ? data[index].descriptionFa : NotEntered;
    let descriptionEn = data[index].descriptionEn ? data[index].descriptionEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albums && data[index].albums.length ? data[index].albums.length : "0";
    let singlesCount = data[index].singles && data[index].singles.length ? data[index].singles.length : "0";
    let musicVideoCounts = data[index].musicVideos && data[index].musicVideos.length ? data[index].musicVideos.length : "0";
    let instrumentCounts = data[index].instruments && data[index].instruments.length ? data[index].instruments.length : "0";
    let likeCount = data[index].likeCount ? data[index].likeCount : "0";
    let listenCount = data[index].listenCount ? data[index].listenCount : "0";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    tbody.push({
      data: [nameFa, nameEn, descriptionFa, descriptionEn, followersCount, albumsCount, singlesCount, likeCount, listenCount, instrumentCounts, viewCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_artist = (artist);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/index.js
















const table = {
  instrument: table_instrument,
  country: table_country,
  memberDiscount: table_memberDiscount,
  memberTransaction: table_memberTransaction,
  members: table_members,
  transactionDiscount: table_transactionDiscount,
  showTransaction: table_ShowTransactions,
  ShowClub: table_ShowClub,
  ShowDiscount: table_ShowDiscount,
  showOwner: table_ShowOwner,
  showScenario: table_ShowScenario,
  genres: table_genres,
  ShowScenarioDataInModal: table_ShowScenarioDataInModal,
  ShowScenarioDataInModalTwo: table_ShowScenarioDataInModalTwo,
  song: table_song,
  artist: table_artist
};
/* harmony default export */ var consts_table = (table);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addGenres.js
const addGenres = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addGenres = (addGenres);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addCategory.js
const owner = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    weight: {
      label: "وزن :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "وزن"
      },
      value: "",
      validation: {
        minLength: 1,
        isNumeric: true,
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var addCategory = (owner);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addArtist.js
const addArtist = {
  Form: {
    nameFa: {
      label: "نام فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام فارسی"
      },
      value: "",
      validation: {
        required: false,
        isFa: false
      },
      valid: true,
      touched: false
    },
    nameEn: {
      label: "نام انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " نام انگلیسی"
      },
      value: "",
      validation: {
        required: false,
        isEn: true
      },
      valid: true,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: false,
        isFa: false
      },
      valid: true,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: false,
        isEn: true
      },
      valid: true,
      touched: false
    },
    defaultGenre: {
      label: "ژانر اصلی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "ژانر اصلی"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    genre: {
      label: "ژانر های دیگر :",
      elementType: "inputDropDownSearchArray",
      elementConfig: {
        type: "text",
        placeholder: "ژانر های دیگر"
      },
      value: [],
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    birthPlaceCountry: {
      label: "متولد در کشور  :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "متولد در کشور "
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    birthPlaceCity: {
      label: "متولد در شهر  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "متولد در شهر "
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    birthday: {
      label: "تاریخ تولد :",
      elementType: "dateInput",
      elementConfig: {
        type: "text",
        placeholder: "بر روی تقویم کلیک نمایید "
      },
      value: {
        month: "",
        day: "",
        year: ""
      },
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    instruments: {
      label: "ساز ها :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "ساز ها"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    website: {
      label: "وب سایت :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "وب سایت"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    instagram: {
      label: "اینستاگرام :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "اینستاگرام"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    youTube: {
      label: "یوتیوب :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "یوتیوب"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    twitter: {
      label: "توییتر :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توییتر"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    avatar: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addArtist = (addArtist);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addSlider.js
const addSlider = {
  Form: {
    parentType: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد"
      },
      childValue: [{
        name: "باشگاه",
        value: "CLUB"
      }, {
        name: "تخفیف",
        value: "DISCOUNT"
      }],
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    type: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    discount: {
      label: "تخفیف :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "تخفیف"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    club: {
      label: "باشگاه :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "باشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addSlider = (addSlider);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addClub.js
const addClub = {
  Form: {
    membership: {
      label: "عضویت :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عضویت"
      },
      childValue: [{
        value: "APPLICATION"
      }, {
        value: "CARD"
      }],
      value: [],
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    owner: {
      label: "فروشنده :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشنده"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    aboutClub: {
      label: " درباره باشگاه :",
      elementType: "textarea",
      elementConfig: {
        type: "text",
        placeholder: " درباره باشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    percent: {
      label: "درصد :",
      elementType: "input",
      value: "",
      validation: {
        minLength: 1,
        maxLength: 3,
        isNumeric: true,
        required: true
      },
      valid: false,
      touched: false
    },
    slides: {
      label: "اسلایدس :",
      elementType: "InputFileArray",
      kindOf: "image",
      value: [],
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addClub = (addClub);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/upload.js
const upload = {
  Form: {
    file: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_upload = (upload);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addBanner.js
const addBanner = {
  Form: {
    parentType: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد"
      },
      childValue: [{
        name: "باشگاه",
        value: "CLUB"
      }, {
        name: "تخفیف",
        value: "DISCOUNT"
      }],
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    discount: {
      label: "تخفیف :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "تخفیف"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    club: {
      label: "باشگاه :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "باشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addBanner = (addBanner);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addScenario.js
const addScenario = {
  Form: {
    name: {
      label: "نام :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    spinRepeatTime: {
      label: "زمان چرخش مجدد (روز):",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "روز"
      },
      value: "",
      validation: {
        required: true,
        minLength: 1,
        maxLength: 1,
        isNumeric: true
      },
      valid: false,
      touched: false
    },
    startDate: {
      label: "تاریخ شروع :",
      elementType: "date",
      elementConfig: {
        type: "text",
        placeholder: "بر روی تقویم کلیک نمایید "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    endDate: {
      label: "تاریخ پایان :",
      elementType: "date",
      elementConfig: {
        type: "text",
        placeholder: "بر روی تقویم کلیک نمایید "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    empty: {
      label: "پوچ ها :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "پوچ ها"
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true
      },
      value: [],
      valid: false,
      touched: false
    },
    gifts: {
      label: "جوایز :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "جوایز"
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true
      },
      value: [],
      valid: false,
      touched: false
    },
    winnersCount: {
      label: "تعداد برندگان :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "تعداد برندگان"
      },
      value: "",
      validation: {
        required: true,
        minLength: 1,
        isNumeric: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addScenario = (addScenario);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addAlbum.js
const addAlbum = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    artist: {
      label: "هنرمند :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "هنرمند"
      },
      value: "",
      validation: {
        minLength: 1,
        required: true
      },
      valid: false,
      touched: false
    },
    genres: {
      label: "ژانر :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "ژانر"
      },
      value: "",
      validation: {
        minLength: 1,
        required: true
      },
      valid: false,
      touched: false
    },
    songs: {
      label: "آهنگ ها :",
      elementType: "InputFileArray",
      kindOf: "voice",
      value: [],
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: " عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addAlbum = (addAlbum);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addCountry.js
const addCountry = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    flag: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addCountry = (addCountry);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/instruments.js
const addInstruments = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    thumbnail: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var instruments = (addInstruments);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addGallery.js
const addGallery = {
  Form: {
    imageName: {
      label: "نام عکس :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام عکس"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    imageType: {
      label: "دسته عکس :",
      elementType: "inputDropDown",
      elementConfig: {
        type: "text",
        placeholder: " دسته عکس"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: " عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addGallery = (addGallery);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addSong.js
const addSong = {
  Form: {
    songName: {
      label: "نام آهنگ :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام آهنگ"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    song: {
      label: " آهنگ :",
      elementType: "inputFile",
      kindOf: "voice",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addSong = (addSong);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addMood.js
const addMood = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    images: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addMood = (addMood);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addHashtag.js
const addHashtag = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addHashtag = (addHashtag);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/index.js















const states = {
  addScenario: states_addScenario,
  addGenres: states_addGenres,
  addCategory: addCategory,
  addArtist: states_addArtist,
  addSlider: states_addSlider,
  addClub: states_addClub,
  upload: states_upload,
  addBanner: states_addBanner,
  addAlbum: states_addAlbum,
  addCountry: states_addCountry,
  instrument: instruments,
  addGallery: states_addGallery,
  addSong: states_addSong,
  addMood: states_addMood,
  addHashtag: states_addHashtag
};
/* harmony default export */ var consts_states = (states);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/galleryConstants.js



const galleryConstants = () => {
  const ConstantsEn = panelAdmin_0.values.en;
  const string = panelAdmin_0.values.strings;
  return [{
    value: ConstantsEn.ALBUM_CONSTANTS,
    title: string.ALBUM_CONSTANTS
  }, {
    value: ConstantsEn.PLAY_LIST_CONSTANTS,
    title: string.PLAY_LIST_CONSTANTS
  }, {
    value: ConstantsEn.FLAG_CONSTANTS,
    title: string.FLAG_CONSTANTS
  }, {
    value: ConstantsEn.SONG_CONSTANTS,
    title: string.SONG_CONSTANTS
  }, {
    value: ConstantsEn.ARTIST_CONSTANTS,
    title: string.ARTIST_CONSTANTS
  }, {
    value: ConstantsEn.INSTRUMENT_CONSTANTS,
    title: string.INSTRUMENT_CONSTANTS
  }, {
    value: ConstantsEn.MUSIC_VIDEO_CONSTANTS,
    title: string.MUSIC_VIDEO_CONSTANTS
  }, {
    value: ConstantsEn.MOOD_CONSTANTS,
    title: string.MOOD_CONSTANTS
  }];
};

/* harmony default export */ var consts_galleryConstants = (galleryConstants);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/instrument.js
const instrument_instrument = data => {
  const cardFormat = [];

  for (let index in data) {
    let dataIndex = data[index];
    let description = dataIndex.descriptionFa ? dataIndex.descriptionFa : "";
    let price = dataIndex.price ? dataIndex.price : "";
    let title = dataIndex.titleFa ? dataIndex.titleFa : "";
    let images = data[index].thumbnail ? data[index].thumbnail : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }, {
        left: [{
          elementType: "price",
          value: price,
          direction: "ltr"
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_instrument = (instrument_instrument);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/gallery.js
const gallery_instrument = (data, acceptedCard) => {
  const cardFormat = [];
  console.log({
    insss: data
  });

  for (let index in data) {
    let dataIndex = data[index];
    let web = dataIndex.web ? dataIndex.web : "";
    let phone = dataIndex.phone ? dataIndex.phone : "";
    let title = dataIndex.title ? dataIndex.title : ""; // let images = { web, phone };
    // console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? acceptedCard.web === web || acceptedCard.phone === phone ? "activeImage" : "" : "",
      image: {
        value: {
          web,
          phone
        }
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var gallery = (gallery_instrument);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/country.js


const country_country = data => {
  const cardFormat = [];
  console.log({
    data
  });

  for (let index in data) {
    let dataIndex = data[index];
    let description = dataIndex.titleFa ? dataIndex.titleFa : "";
    let price = dataIndex.price ? dataIndex.price : "";
    let title = dataIndex.titleFa ? dataIndex.titleFa : "";
    let images = data[index].flag ? data[index].flag : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }, {
        left: [{
          elementType: "price",
          value: price,
          direction: "ltr"
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_country = (country_country);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/mood.js


const mood = data => {
  const cardFormat = [];
  console.log({
    data
  });

  for (let index in data) {
    let dataIndex = data[index];
    let description = dataIndex.titleFa ? dataIndex.titleFa : "";
    let price = dataIndex.price ? dataIndex.price : "";
    let title = dataIndex.titleFa ? dataIndex.titleFa : "";
    let images = dataIndex.images ? dataIndex.images : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }, {
        left: [{
          elementType: "price",
          value: price,
          direction: "ltr"
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_mood = (mood);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/hashtag.js



const hashtag = data => {
  const cardFormat = [];
  console.log({
    data
  });

  for (let index in data) {
    let noEntries = panelAdmin_0.values.strings.NO_ENTRIES;
    let dataIndex = data[index];
    let description = dataIndex.titleFa ? dataIndex.titleFa : noEntries;
    let title = dataIndex.titleFa ? dataIndex.titleFa : noEntries; // let images = dataIndex.images ? dataIndex.images : "";

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      // image: { value: images },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_hashtag = (hashtag);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/genre.js



const genre = data => {
  const cardFormat = [];
  console.log({
    data
  });

  for (let index in data) {
    let noEntries = panelAdmin_0.values.strings.NO_ENTRIES;
    let dataIndex = data[index];
    let description = dataIndex.titleFa ? dataIndex.titleFa : noEntries;
    let title = dataIndex.titleFa ? dataIndex.titleFa : noEntries; // let images = dataIndex.images ? dataIndex.images : "";

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      // image: { value: images },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_genre = (genre);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/artist.js
const artist_artist = (data, acceptedCard) => {
  const cardFormat = [];
  console.log({
    insss: data
  });

  for (let index in data) {
    let dataIndex = data[index];
    let web = dataIndex.web ? dataIndex.web : "";
    let phone = dataIndex.phone ? dataIndex.phone : "";
    let title = dataIndex.nameFa ? dataIndex.nameFa : "";
    let description = dataIndex.descriptionFa ? dataIndex.descriptionFa : "";
    let images = dataIndex.avatar ? dataIndex.avatar : ""; // console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      isActive: data[index].isActive,
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          style: {
            color: "#8c8181",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_artist = (artist_artist);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/index.js







const card = {
  instrument: card_instrument,
  gallery: gallery,
  country: card_country,
  mood: card_mood,
  hashtag: card_hashtag,
  artist: card_artist,
  genre: card_genre
};
/* harmony default export */ var consts_card = (card);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/index.js




const consts = {
  table: consts_table,
  states: consts_states,
  galleryConstants: consts_galleryConstants,
  card: consts_card
};
/* harmony default export */ var utils_consts = (consts);
// EXTERNAL MODULE: external "react-toastify"
var external_react_toastify_ = __webpack_require__("oAEb");

// CONCATENATED MODULE: ./panelAdmin/utils/toastify.js



const toastify_toastify = async (text, type) => {
  external_react_toastify_["toast"].configure();

  if (type === "error") {
    // alert("error");
    await external_react_toastify_["toast"].error(text, {
      position: "top-right",
      autoClose: 5000
    });
  } else if (type === "success") {
    // alert("success");
    await external_react_toastify_["toast"].success(text, {
      position: "top-right",
      autoClose: 5000
    });
  }
};

/* harmony default export */ var utils_toastify = (toastify_toastify);
// CONCATENATED MODULE: ./panelAdmin/utils/CelanderConvert/convertToCelander.js
const convertToCelander = value => {
  let valDate,
      day,
      month,
      year,
      selectedDay = null;

  if (value) {
    valDate = value.split("/");
    day = Number(valDate[2]);
    month = Number(valDate[1]);
    year = Number(valDate[0]);
    selectedDay = {
      day,
      month,
      year
    };
  }

  return selectedDay;
};

/* harmony default export */ var CelanderConvert_convertToCelander = (convertToCelander);
// CONCATENATED MODULE: ./panelAdmin/utils/CelanderConvert/convertToDate.js
const convertToDate = selectedDay => {
  let day = selectedDay.day;
  let month = selectedDay.month;
  let year = selectedDay.year;
  let date = year + "/" + month + "/" + day;
  return date;
};

/* harmony default export */ var CelanderConvert_convertToDate = (convertToDate);
// CONCATENATED MODULE: ./panelAdmin/utils/CelanderConvert/index.js


const CelanderConvert = {
  convertToCelander: CelanderConvert_convertToCelander,
  convertToDate: CelanderConvert_convertToDate
};
/* harmony default export */ var utils_CelanderConvert = (CelanderConvert);
// CONCATENATED MODULE: ./panelAdmin/utils/checkValidity.js
const checkValidity = (value, rules, array) => {
  let isValid = true;
  console.log({
    value,
    rules,
    array
  });
  console.log({
    mehrad: typeof value
  });

  if (!rules) {
    return true;
  }

  if (array) {
    return isValid = value.length > 0;
  }

  if (rules.required) {
    if (typeof value === "object") {
      isValid = true;
    } else {
      isValid = value.trim() !== "" && isValid;
    }
  }

  if (rules.minLength) {
    isValid = value.length >= rules.minLength && isValid;
  }

  if (rules.maxLength) {
    isValid = value.length <= rules.maxLength && isValid;
  }

  if (rules.isEmail) {
    const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isNumeric) {
    const pattern = /^\d+$/;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isPhone) {
    const pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isMobile) {
    const pattern = /[0,9]{2}\d{9}/g;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isEn) {
    const pattern = /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isFa) {
    const pattern = /^[\u0600-\u06FF\s]+$/;
    isValid = pattern.test(value) && isValid;
  }

  return isValid;
};
// EXTERNAL MODULE: ./panelAdmin/utils/updateObject.js
var updateObject = __webpack_require__("ge5p");

// EXTERNAL MODULE: ./panelAdmin/api/index.js + 26 modules
var api = __webpack_require__("FRaV");

// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/validUpload/voicevalid.js
let formats = ["mp3", "voice", "audio"];

const voiceValid = type => {
  let valid = formats.map(format => {
    if (type.includes(format)) return true;else return false;
  });
  let finalValid = valid.includes(true) ? true : false;
  return finalValid;
};

/* harmony default export */ var voicevalid = (voiceValid);
// EXTERNAL MODULE: ./store/actions/saga/index.js
var saga = __webpack_require__("BpoA");

// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/uploadChange.js
function uploadChange_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function uploadChange_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { uploadChange_ownKeys(Object(source), true).forEach(function (key) { uploadChange_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { uploadChange_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function uploadChange_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







const uploadChange = async props => {
  const {
    event,
    setLoading,
    imageType,
    setState,
    valid,
    fileName,
    dispatch
  } = props;
  console.log({
    eventUpload: event
  });
  let files = event.files[0];
  console.log({
    imageType,
    fileName
  });
  let returnData = false;

  if (files) {
    switch (valid) {
      case "image":
        if (files.type.includes("image")) {
          if (imageType && fileName) {
            if ( // dispatch(sagaActions.uploadImageData({ data: files, imageType, fileName }))
            await api["post"].imageUpload(files, setLoading, imageType, setState, fileName)) returnData = fileName;
          } else {
            utils_toastify("لطفا اطلاعات قبلی را پر کنید", "error");
          }
        }

        break;

      case "video":
        if (files.type.includes("video")) returnData = await api["post"].videoUpload(files, setLoading, imageType, setState);
        break;

      case "voice":
        if (voicevalid(files.type)) {
          if (fileName) {
            if (await api["post"].voiceUpload(files, setLoading, imageType, setState, fileName)) returnData = fileName;
          } else {
            utils_toastify("لطفا اطلاعات قبلی را پر کنید", "error");
          }
        }

        break;

      default:
        utils_toastify("فایل شما نباید " + files.type + " باشد", "error");
        break;
    }
  }

  console.log({
    files: files,
    returnData,
    fileName
  });
  if (!returnData && files) utils_toastify("فایل شما نباید " + files.type + " باشد", "error");
  setState(prev => uploadChange_objectSpread({}, prev, {
    progressPercentImage: null,
    progressPercentVideo: null,
    progressPercentSongs: null
  }));
  return returnData;
};

/* harmony default export */ var onChanges_uploadChange = (uploadChange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/handelOnchange.js
function handelOnchange_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function handelOnchange_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { handelOnchange_ownKeys(Object(source), true).forEach(function (key) { handelOnchange_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { handelOnchange_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function handelOnchange_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const handelOnchange = async ({
  event,
  data,
  setData,
  setState,
  setLoading,
  imageType,
  validName,
  checkValidity,
  updateObject,
  uploadChange,
  fileName
}) => {
  let changeValid,
      updatedForm,
      updatedFormElement = {};
  let formIsValid = true;
  let value = data.Form[event.name].value;

  let update = handelOnchange_objectSpread({}, value);

  let eventValue = event.value;
  let typeofData = typeof value;
  let isArray, isObject, isString;
  console.log({
    length: value.length
  });
  typeofData === "object" && value != undefined ? value && value.length >= 0 ? isArray = true : isObject = true : isString = true;
  console.log({
    typeofData,
    value,
    isArray,
    isObject,
    isString
  });

  const remove = index => value.splice(index, 1)[0];

  const push = (val, newVal) => newVal != undefined ? val.push(newVal) : "";

  console.log({
    eventValue,
    value
  });

  if (event.type === "file") {
    const uploadFile = await uploadChange({
      event,
      setLoading,
      imageType,
      setState,
      valid: data.Form[event.name].kindOf,
      fileName
    });

    if (uploadFile) {
      if (isArray) value.includes(uploadFile) ? remove(value.findIndex(d => d === uploadFile)) : push(value, uploadFile);else value = uploadFile;
    } else return;
  } else if (isArray) value.includes(eventValue) ? remove(value.findIndex(d => d === eventValue)) : push(value, eventValue);else if (isObject) {
    // value = eventValue;
    console.log({
      event
    });

    if (event.child) {
      console.log({
        eventValue,
        value
      });
      value[event.child] = eventValue;
    } else {
      value = eventValue;
    }
  } else if (isString) value = eventValue;

  let checkValidValue;
  if (typeofData === "object") checkValidValue = eventValue;else checkValidValue = value;
  updatedFormElement[event.name] = updateObject(data.Form[event.name], {
    value: value,
    valid: typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray),
    touched: true
  });

  if (validName) {
    changeValid = updateObject(data.Form[validName], {
      valid: true
    });
    updatedForm = updateObject(data.Form, handelOnchange_objectSpread({}, updatedFormElement, {
      [validName]: changeValid
    }));
  } else updatedForm = updateObject(data.Form, handelOnchange_objectSpread({}, updatedFormElement));

  console.log({
    updatedFormElement,
    updatedForm
  });

  for (let name in updatedForm) formIsValid = updatedForm[name].valid && formIsValid;

  return setData({
    Form: updatedForm,
    formIsValid: formIsValid
  });
};

/* harmony default export */ var onChanges_handelOnchange = (handelOnchange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/arrayOnchange.js
function arrayOnchange_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function arrayOnchange_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { arrayOnchange_ownKeys(Object(source), true).forEach(function (key) { arrayOnchange_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { arrayOnchange_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function arrayOnchange_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const arrayOnchange = async props => {
  const {
    event: e,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity,
    updateObject,
    uploadChange
  } = props;
  console.log({
    validName
  });
  let changeValid,
      updatedForm,
      updatedFormElement = {};
  e.map(async event => {
    let value = data.Form[event.name].value;

    let update = arrayOnchange_objectSpread({}, value);

    let eventValue = event.value;
    let typeofData = typeof value;
    let isArray, isObject, isString;
    typeofData === "object" && value != null ? value.length >= 0 ? isArray = true : isObject = true : isString = true;

    const remove = index => value.splice(index, 1)[0];

    const push = (val, newVal) => newVal != undefined ? val.push(newVal) : ""; // console.log({ eventValue, value });


    if (event.type === "file") {
      const uploadFile = await uploadChange(event, setLoading, imageType, setState);

      if (uploadFile) {
        if (isArray) value.includes(uploadFile) ? remove(value.findIndex(d => d === uploadFile)) : push(value, uploadFile);else value = uploadFile;
      } else return;
    } else if (isArray) value.includes(eventValue) ? remove(value.findIndex(d => d === eventValue)) : push(value, eventValue);else if (isObject) {
      value = eventValue;
      if (event.child) value = update[event.child] = eventValue;
    } else if (isString) value = eventValue;

    let checkValidValue;
    if (typeofData === "object") checkValidValue = eventValue;else checkValidValue = value;
    updatedFormElement[event.name] = updateObject(data.Form[event.name], {
      value: value,
      valid: typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray),
      touched: true
    });

    if (validName) {
      changeValid = updateObject(data.Form[validName], {
        valid: true
      });
      updatedForm = updateObject(data.Form, arrayOnchange_objectSpread({}, updatedFormElement, {
        [validName]: changeValid
      }));
    } else updatedForm = updateObject(data.Form, arrayOnchange_objectSpread({}, updatedFormElement));

    console.log({
      updatedFormElement,
      updatedForm
    });
    let formIsValid = true;

    for (let name in updatedForm) formIsValid = updatedForm[name].valid && formIsValid;

    return setData({
      Form: updatedForm,
      formIsValid: formIsValid
    });
  });
};

/* harmony default export */ var onChanges_arrayOnchange = (arrayOnchange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/globalChange.js






const globalChange = async props => {
  const {
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    fileName,
    dispatch
  } = props;
  let typeCheck = typeof event; // console.log(typeCheck === "object" && event.length > 0);

  if (typeCheck === "object" && event.length > 0) return onChanges_arrayOnchange({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity: checkValidity,
    updateObject: updateObject["a" /* default */],
    uploadChange: onChanges_uploadChange,
    fileName,
    dispatch
  });else if (typeCheck === "object") return onChanges_handelOnchange({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity: checkValidity,
    updateObject: updateObject["a" /* default */],
    uploadChange: onChanges_uploadChange,
    fileName,
    dispatch
  });
};

/* harmony default export */ var onChanges_globalChange = (globalChange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/index.js


const onChanges = {
  globalChange: onChanges_globalChange,
  arrayOnchange: onChanges_arrayOnchange
};
/* harmony default export */ var utils_onChanges = (onChanges);
// CONCATENATED MODULE: ./panelAdmin/utils/handleKey.js
const handleKey = event => {
  const form = event.target.form;
  const index = Array.prototype.indexOf.call(form, event.target);
  let keyCode = event.keyCode;
  let numberAccepted = [8, 13];

  if (numberAccepted.includes(keyCode)) {
    if (keyCode === 13) {
      if (form.elements[index + 1]) return form.elements[index + 1].focus();
    } else if (keyCode === 8) if (form.elements[index].value) return form.elements[index].value.length - 1;else if (form.elements[index - 1]) form.elements[index - 1].focus();

    event.preventDefault();
  }
};

/* harmony default export */ var utils_handleKey = (handleKey);
// CONCATENATED MODULE: ./panelAdmin/utils/index.js






const utils = {
  consts: utils_consts,
  toastify: utils_toastify,
  CelanderConvert: utils_CelanderConvert,
  onChanges: utils_onChanges,
  handleKey: utils_handleKey,
  formatMoney: utils_formatMoney
};
/* harmony default export */ var panelAdmin_utils = (utils);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/uploadReducer.js
function uploadReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function uploadReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { uploadReducer_ownKeys(Object(source), true).forEach(function (key) { uploadReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { uploadReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function uploadReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const uploadInitialState = {
  uploadImageData: ""
};
const setUploadImageData = (state, action) => {
  return uploadReducer_objectSpread({}, state, {
    uploadData: action.data
  });
};

function uploadReducer(state = uploadInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_UPLOAD_IMAGE:
      return setUploadImageData(state, action);

    default:
      return state;
  }
}

/* harmony default export */ var reducer_uploadReducer = (uploadReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/galleryReducer.js
function galleryReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function galleryReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { galleryReducer_ownKeys(Object(source), true).forEach(function (key) { galleryReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { galleryReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function galleryReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const galleryInitialState = {
  galleryData: []
};
const setGalleryData = (state, action) => {
  return galleryReducer_objectSpread({}, state, {
    galleryData: action.data
  });
};

function galleryReducer(state = galleryInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_GALLERY_DATA:
      return setGalleryData(state, action);

    default:
      return state;
  }
}

/* harmony default export */ var reducer_galleryReducer = (galleryReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/artistReducer.js
function artistReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function artistReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { artistReducer_ownKeys(Object(source), true).forEach(function (key) { artistReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { artistReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function artistReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const artistInitialState = {
  artistData: null,
  searchArtistData: null,
  loading: false,
  searchLoading: false
};

function artistReducer(state = artistInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_ARTIST_DATA:
      return artistReducer_objectSpread({}, state, {}, {
        artistData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_ARTIST_DATA:
      return artistReducer_objectSpread({}, state, {}, {
        searchArtistData: action.data,
        searchLoading: false
      });

    case atRedux.START_ARTIST_DATA:
      console.log("START_ARTIST_DATA");
      return artistReducer_objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_ARTIST_DATA:
      console.log("START_SEARCH_ARTIST_DATA");
      return artistReducer_objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_artistReducer = (artistReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/panelNavbarReducer.js
function panelNavbarReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function panelNavbarReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { panelNavbarReducer_ownKeys(Object(source), true).forEach(function (key) { panelNavbarReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { panelNavbarReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function panelNavbarReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// import atRedux from "../../../store/actionTypes/redux";

const panelNavbarReducer_artistInitialState = {
  pageName: null
};

function panelNavbarReducer(state = panelNavbarReducer_artistInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_PAGE_NAME:
      return panelNavbarReducer_objectSpread({}, state, {}, {
        pageName: action.data
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_panelNavbarReducer = (panelNavbarReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/instrumentReducer.js
function instrumentReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function instrumentReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { instrumentReducer_ownKeys(Object(source), true).forEach(function (key) { instrumentReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { instrumentReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function instrumentReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const instrumentInitialState = {
  instrumentData: null,
  searchInstrumentData: null,
  loading: false,
  searchLoading: false
};

function instrumentReducer(state = instrumentInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_INSTRUMENT_DATA:
      return instrumentReducer_objectSpread({}, state, {}, {
        instrumentData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_INSTRUMENT_DATA:
      return instrumentReducer_objectSpread({}, state, {}, {
        searchInstrumentData: action.data,
        searchLoading: false
      });

    case atRedux.START_INSTRUMENT_DATA:
      return instrumentReducer_objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_INSTRUMENT_DATA:
      return instrumentReducer_objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_instrumentReducer = (instrumentReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/countryReducer.js
function countryReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function countryReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { countryReducer_ownKeys(Object(source), true).forEach(function (key) { countryReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { countryReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function countryReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const countryInitialState = {
  countryData: null,
  searchCountryData: null,
  loading: false,
  searchLoading: false
};

function countryReducer(state = countryInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_COUNTRY_DATA:
      return countryReducer_objectSpread({}, state, {}, {
        countryData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_COUNTRY_DATA:
      return countryReducer_objectSpread({}, state, {}, {
        searchCountryData: action.data,
        searchLoading: false
      });

    case atRedux.START_COUNTRY_DATA:
      return countryReducer_objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_COUNTRY_DATA:
      return countryReducer_objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_countryReducer = (countryReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/moodReducer.js
function moodReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function moodReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { moodReducer_ownKeys(Object(source), true).forEach(function (key) { moodReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { moodReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function moodReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const moodInitialState = {
  moodData: null,
  searchMoodData: null,
  loading: false,
  searchLoading: false
};

function moodReducer(state = moodInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_MOOD_DATA:
      return moodReducer_objectSpread({}, state, {}, {
        moodData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_MOOD_DATA:
      return moodReducer_objectSpread({}, state, {}, {
        searchMoodData: action.data,
        searchLoading: false
      });

    case atRedux.START_MOOD_DATA:
      return moodReducer_objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_MOOD_DATA:
      return moodReducer_objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_moodReducer = (moodReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/hashtagReducer.js
function hashtagReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function hashtagReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { hashtagReducer_ownKeys(Object(source), true).forEach(function (key) { hashtagReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { hashtagReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function hashtagReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const hashtagInitialState = {
  hashtagData: null,
  searchMoodData: null,
  loading: false,
  searchLoading: false
};

function hashtagReducer(state = hashtagInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_HASHTAG_DATA:
      return hashtagReducer_objectSpread({}, state, {}, {
        hashtagData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_HASHTAG_DATA:
      return hashtagReducer_objectSpread({}, state, {}, {
        searchMoodData: action.data,
        searchLoading: false
      });

    case atRedux.START_HASHTAG_DATA:
      return hashtagReducer_objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_HASHTAG_DATA:
      return hashtagReducer_objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_hashtagReducer = (hashtagReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/genreReducer.js
function genreReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function genreReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { genreReducer_ownKeys(Object(source), true).forEach(function (key) { genreReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { genreReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function genreReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const genreInitialState = {
  genreData: null,
  searchGenreData: null,
  loading: false,
  searchLoading: false
};

function genreReducer(state = genreInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;
  console.log({
    action
  });

  switch (action.type) {
    case atRedux.SET_GENRE_DATA:
      return genreReducer_objectSpread({}, state, {}, {
        genreData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_GENRE_DATA:
      return genreReducer_objectSpread({}, state, {}, {
        searchGenreData: action.data,
        searchLoading: false
      });

    case atRedux.START_GENRE_DATA:
      return genreReducer_objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_GENRE_DATA:
      return genreReducer_objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_genreReducer = (genreReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/albumReducer.js
function albumReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function albumReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { albumReducer_ownKeys(Object(source), true).forEach(function (key) { albumReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { albumReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function albumReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const albumInitialState = {
  albumData: null,
  searchAlbumData: null,
  loading: false,
  searchLoading: false
};

function albumReducer(state = albumInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_ALBUM_DATA:
      return albumReducer_objectSpread({}, state, {}, {
        albumData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_ALBUM_DATA:
      return albumReducer_objectSpread({}, state, {}, {
        searchAlbumData: action.data,
        searchLoading: false
      });

    case atRedux.START_ALBUM_DATA:
      console.log("START_ALBUM_DATA");
      return albumReducer_objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_ALBUM_DATA:
      console.log("START_SEARCH_ALBUM_DATA");
      return albumReducer_objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_albumReducer = (albumReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/index.js
// import galleryReducer from "./galleryReducer";










const reducer = {
  upload: reducer_uploadReducer,
  gallery: reducer_galleryReducer,
  artist: reducer_artistReducer,
  panelNavbar: reducer_panelNavbarReducer,
  instrument: reducer_instrumentReducer,
  country: reducer_countryReducer,
  mood: reducer_moodReducer,
  hashtag: reducer_hashtagReducer,
  genre: reducer_genreReducer,
  album: reducer_albumReducer
};
/* harmony default export */ var storeAdmin_reducer = (reducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/actionTypes/redux/index.js
const redux_atRedux = {
  //======================================================== Redux
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX",
  // ======================================================== NAVBAR
  SET_PAGE_NAME: "SET_PAGE_NAME",
  // ======================================================== UPLOAD
  SET_UPLOAD_IMAGE: "SET_UPLOAD_IMAGE_REDUX",
  // ======================================================== GALLERY
  SET_GALLERY_DATA: "SET_GALLERY_DATA_REDUX",
  CHANGE_ADD_GALLERY_DATA: "CHANGE_ADD_GALLERY_DATA_REDUX",
  // ======================================================== ARTIST
  SET_ARTIST_DATA: "SET_GALLERY_DATA_REDUX",
  START_ARTIST_DATA: "START_GALLERY_DATA_REDUX",
  // ======================= SEARCH ARTIST
  SET_SEARCH_ARTIST_DATA: "SET_SEARCH_ARTIST_DATA_REDUX",
  START_SEARCH_ARTIST_DATA: "START_SEARCH_ARTIST_DATA_REDUX",
  // ======================================================== END ARTIST
  // ======================================================== INSTRUMENT
  SET_INSTRUMENT_DATA: "SET_GALLERY_DATA_REDUX",
  START_INSTRUMENT_DATA: "START_INSTRUMENT_DATA_REDUX",
  // ======================= SEARCH INSTRUMENT
  SET_SEARCH_INSTRUMENT_DATA: "SET_SEARCH_INSTRUMENT_DATA_REDUX",
  START_SEARCH_INSTRUMENT_DATA: "START_SEARCH_INSTRUMENT_DATA_REDUX",
  // ======================================================== END INSTRUMENT
  // ======================================================== COUNTRY
  SET_COUNTRY_DATA: "SET_GALLERY_DATA_REDUX",
  START_COUNTRY_DATA: "START_COUNTRY_DATA_REDUX",
  // ======================= SEARCH COUNTRY
  SET_SEARCH_COUNTRY_DATA: "SET_SEARCH_COUNTRY_DATA_REDUX",
  START_SEARCH_COUNTRY_DATA: "START_SEARCH_COUNTRY_DATA_REDUX",
  // ======================================================== END COUNTRY
  // ======================================================== MOOD
  SET_MOOD_DATA: "SET_GALLERY_DATA_REDUX",
  START_MOOD_DATA: "START_MOOD_DATA_REDUX",
  // ======================= SEARCH MOOD
  SET_SEARCH_MOOD_DATA: "SET_SEARCH_MOOD_DATA_REDUX",
  START_SEARCH_MOOD_DATA: "START_SEARCH_MOOD_DATA_REDUX",
  // ======================================================== END MOOD
  // ======================================================== HASHTAG
  SET_HASHTAG_DATA: "SET_GALLERY_DATA_REDUX",
  START_HASHTAG_DATA: "START_HASHTAG_DATA_REDUX",
  // ======================= SEARCH HASHTAG
  SET_SEARCH_HASHTAG_DATA: "SET_SEARCH_HASHTAG_DATA_REDUX",
  START_SEARCH_HASHTAG_DATA: "START_SEARCH_HASHTAG_DATA_REDUX",
  // ======================================================== END HASHTAG
  // ======================================================== GENRE
  SET_GENRE_DATA: "SET_GALLERY_DATA_REDUX",
  START_GENRE_DATA: "START_GENRE_DATA_REDUX",
  // ======================= SEARCH GENRE
  SET_SEARCH_GENRE_DATA: "SET_SEARCH_GENRE_DATA_REDUX",
  START_SEARCH_GENRE_DATA: "START_SEARCH_GENRE_DATA_REDUX" // ======================================================== END GENRE

};
/* harmony default export */ var redux = (redux_atRedux);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/actions/redux/index.js
 // =================================================== NAVBAR

function setPageName(data) {
  return {
    type: redux.SET_PAGE_NAME,
    data
  };
} // =================================================== UPLOAD

function setUploadImage(data) {
  return {
    type: redux.SET_UPLOAD_IMAGE,
    data
  };
} // =================================================== GALLERY

function redux_setGalleryData(data) {
  return {
    type: redux.SET_GALLERY_DATA,
    data
  };
}
function changeAddGalleryData(data) {
  return {
    type: redux.CHANGE_ADD_GALLERY_DATA,
    data
  };
} // =================================================== ARTIST

function setArtistData(data) {
  return {
    type: redux.SET_ARTIST_DATA,
    data
  };
}
function setSearchArtistData(data) {
  return {
    type: redux.SET_SEARCH_ARTIST_DATA,
    data
  };
}
function startGetArtist(data) {
  return {
    type: redux.START_ARTIST_DATA,
    data
  };
}
function startSearchArtist(data) {
  return {
    type: redux.START_SEARCH_ARTIST_DATA,
    data
  };
} // =================================================== INSTRUMENT

function setInstrumentData(data) {
  return {
    type: redux.SET_INSTRUMENT_DATA,
    data
  };
}
function setSearchInstrumentData(data) {
  return {
    type: redux.SET_SEARCH_INSTRUMENT_DATA,
    data
  };
}
function startGetInstrument(data) {
  return {
    type: redux.START_INSTRUMENT_DATA,
    data
  };
}
function startSearchInstrument(data) {
  return {
    type: redux.START_SEARCH_INSTRUMENT_DATA,
    data
  };
} // =================================================== COUNTRY

function setCountryData(data) {
  return {
    type: redux.SET_COUNTRY_DATA,
    data
  };
}
function setSearchCountryData(data) {
  return {
    type: redux.SET_SEARCH_COUNTRY_DATA,
    data
  };
}
function startGetCountry(data) {
  return {
    type: redux.START_COUNTRY_DATA,
    data
  };
}
function startSearchCountry(data) {
  return {
    type: redux.START_SEARCH_COUNTRY_DATA,
    data
  };
} // =================================================== MOOD

function setMoodData(data) {
  return {
    type: redux.SET_MOOD_DATA,
    data
  };
}
function setSearchMoodData(data) {
  return {
    type: redux.SET_SEARCH_MOOD_DATA,
    data
  };
}
function startGetMood(data) {
  return {
    type: redux.START_MOOD_DATA,
    data
  };
}
function startSearchMood(data) {
  return {
    type: redux.START_SEARCH_MOOD_DATA,
    data
  };
} // =================================================== HASHTAG

function setHashtagData(data) {
  return {
    type: redux.SET_HASHTAG_DATA,
    data
  };
}
function setSearchHashtagData(data) {
  return {
    type: redux.SET_SEARCH_HASHTAG_DATA,
    data
  };
}
function startGetHashtag(data) {
  return {
    type: redux.START_HASHTAG_DATA,
    data
  };
}
function startSearchHashtag(data) {
  return {
    type: redux.START_SEARCH_HASHTAG_DATA,
    data
  };
} // =================================================== GENRE

function setGenreData(data) {
  return {
    type: redux.SET_GENRE_DATA,
    data
  };
}
function setSearchGenreData(data) {
  return {
    type: redux.SET_SEARCH_GENRE_DATA,
    data
  };
}
function startGetGenre(data) {
  return {
    type: redux.START_GENRE_DATA,
    data
  };
}
function startSearchGenre(data) {
  return {
    type: redux.START_SEARCH_GENRE_DATA,
    data
  };
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/actionTypes/saga/index.js
const atSaga = {
  POST_UPLOAD_IMAGE: "POST_UPLOAD_IMAGE_SAGA",
  GET_GALLERY_DATA: "GET_GALLERY_DATA_SAGA",
  //============================================================= ARTIST
  GET_ARTIST_DATA: "GET_ARTIST_DATA_SAGA",
  GET_SEARCH_ARTIST_DATA: "GET_SEARCH_ARTIST_DATA_SAGA",
  //============================================================= INSTRUMENT
  GET_INSTRUMENT_DATA: "GET_INSTRUMENT_DATA_SAGA",
  GET_SEARCH_INSTRUMENT_DATA: "GET_SEARCH_INSTRUMENT_DATA_SAGA",
  //============================================================= COUNTRY
  GET_COUNTRY_DATA: "GET_COUNTRY_DATA_SAGA",
  GET_SEARCH_COUNTRY_DATA: "GET_SEARCH_COUNTRY_DATA_SAGA",
  //============================================================= ALBUM
  GET_ALBUM_DATA: "GET_ALBUM_DATA_SAGA",
  GET_SEARCH_ALBUM_DATA: "GET_SEARCH_ALBUM_DATA_SAGA",
  //============================================================= MOOD
  GET_MOOD_DATA: "GET_MOOD_DATA_SAGA",
  GET_SEARCH_MOOD_DATA: "GET_SEARCH_MOOD_DATA_SAGA",
  //============================================================= HASHTAG
  GET_HASHTAG_DATA: "GET_HASHTAG_DATA_SAGA",
  GET_SEARCH_HASHTAG_DATA: "GET_SEARCH_HASHTAG_DATA_SAGA",
  //============================================================= GENRE
  GET_GENRE_DATA: "GET_GENRE_DATA_SAGA",
  GET_SEARCH_GENRE_DATA: "GET_SEARCH_GENRE_DATA_SAGA"
};
/* harmony default export */ var actionTypes_saga = (atSaga);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/actions/saga/index.js

function uploadImageData(data) {
  return {
    type: actionTypes_saga.POST_UPLOAD_IMAGE,
    data: data
  };
}
function getGalleryData(data) {
  return {
    type: actionTypes_saga.GET_GALLERY_DATA,
    data: data
  };
} // ========================================================= ARTIST

function getArtistData({
  page
}) {
  console.log({
    mojtaba1: page
  });
  return {
    type: actionTypes_saga.GET_ARTIST_DATA,
    page
  };
}
function getSearchArtistData({
  title,
  page
}) {
  return {
    type: actionTypes_saga.GET_SEARCH_ARTIST_DATA,
    page,
    title
  };
} // ========================================================= INSTRUMENT

function getInstrumentData({
  page
}) {
  return {
    type: actionTypes_saga.GET_INSTRUMENT_DATA,
    page
  };
}
function getSearchInstrumentData({
  title,
  page
}) {
  return {
    type: actionTypes_saga.GET_SEARCH_INSTRUMENT_DATA,
    page,
    title
  };
} // ========================================================= COUNTRY

function getCountryData({
  page
}) {
  return {
    type: actionTypes_saga.GET_COUNTRY_DATA,
    page
  };
}
function getSearchCountryData({
  title,
  page
}) {
  return {
    type: actionTypes_saga.GET_SEARCH_COUNTRY_DATA,
    page,
    title
  };
} // ========================================================= MOOD

function getMoodData({
  page
}) {
  return {
    type: actionTypes_saga.GET_MOOD_DATA,
    page
  };
}
function getSearchMoodData({
  title,
  page
}) {
  return {
    type: actionTypes_saga.GET_SEARCH_MOOD_DATA,
    page,
    title
  };
} // ========================================================= ALBUM

function getAlbumData({
  page
}) {
  return {
    type: actionTypes_saga.GET_ALBUM_DATA,
    page
  };
}
function getSearchAlbumData({
  title,
  page
}) {
  return {
    type: actionTypes_saga.GET_SEARCH_ALBUM_DATA,
    page,
    title
  };
} // ========================================================= HASHTAG

function getHashtagData({
  page
}) {
  return {
    type: actionTypes_saga.GET_HASHTAG_DATA,
    page
  };
}
function getSearchHashtagData({
  title,
  page
}) {
  return {
    type: actionTypes_saga.GET_SEARCH_HASHTAG_DATA,
    page,
    title
  };
} // ========================================================= GENRE

function getGenreData({
  page
}) {
  return {
    type: actionTypes_saga.GET_GENRE_DATA,
    page
  };
}
function getSearchGenreData({
  title,
  page
}) {
  return {
    type: actionTypes_saga.GET_SEARCH_GENRE_DATA,
    page,
    title
  };
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/actions/index.js


const actions = {
  reduxActions: actions_redux_namespaceObject,
  sagaActions: actions_saga_namespaceObject
};
/* harmony default export */ var storeAdmin_actions = (actions);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/actionTypes/index.js


const actionTypes = {
  atRedux: redux,
  atSaga: actionTypes_saga
};
/* harmony default export */ var storeAdmin_actionTypes = (actionTypes);
// EXTERNAL MODULE: external "redux-saga/effects"
var effects_ = __webpack_require__("RmXt");

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__("zr5I");
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/homeData.js



function* homeData() {
  try {
    const res = yield external_axios_default.a.get("https://rimtal.com/api/v1/home");
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setHomeData(res.data)); //********** Level 5 **********//
  } catch (err) {
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE)); //********** Level 6 **********//
  }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/galleryApi.js



function* galleryData({
  data
}) {
  try {
    const res = yield external_axios_default.a.get(`https://rimtal.com/api/v1/admin/gallery/${data.type}/${data.page} `);
    console.log({
      res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setGalleryData(res.data)); //********** Level 5 **********//
  } catch (err) {
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE)); //********** Level 6 **********//
  }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/artists.js




function* artistData({
  page
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startGetArtist());
  console.log({
    mojtaba3: page
  });

  try {
    const res = yield panelAdmin_0.api.get.artists({
      page
    });
    console.log({
      resArtistData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setArtistData(res.data));
  } catch (err) {
    console.log({
      errArtistData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* artistSearchData({
  page,
  title
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startSearchArtist());

  try {
    const res = yield panelAdmin_0.api.get.artistSearch({
      page,
      title
    });
    console.log({
      resArtistSearchData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setSearchArtistData(res.data));
  } catch (err) {
    console.log({
      errArtistSearchData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/instrumental.js



function* instrumentData({
  page
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startGetInstrument());
  console.log({
    mojtaba3: page
  });

  try {
    const res = yield panelAdmin_0.api.get.instrument({
      page
    });
    console.log({
      resArtistData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setArtistData(res.data));
  } catch (err) {
    console.log({
      errArtistData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* instrumentSearchData({
  page,
  title
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startSearchInstrument()); //   try {
  //     const res = yield panelAdmin.api.get.artistSearch({ page, title });
  //     console.log({ resArtistSearchData: res });
  //     yield put(actions.reduxActions.setSearchArtistData(res.data));
  //   } catch (err) {
  //     console.log({ errArtistSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/country.js



function* countryData({
  page
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startGetCountry());
  console.log({
    mojtaba3: page
  });

  try {
    const res = yield panelAdmin_0.api.get.country({
      page
    });
    console.log({
      resCountryData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setCountryData(res.data));
  } catch (err) {
    console.log({
      errCountryData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* countrySearchData({
  page,
  title
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startSearchCountry()); //   try {
  //     const res = yield panelAdmin.api.get.CountrySearch({ page, title });
  //     console.log({ resCountrySearchData: res });
  //     yield put(actions.reduxActions.setSearchCountryData(res.data));
  //   } catch (err) {
  //     console.log({ errCountrySearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/mood.js



function* moodData({
  page
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startGetMood());
  console.log({
    mojtaba3: page
  });

  try {
    const res = yield panelAdmin_0.api.get.mood({
      page
    });
    console.log({
      resArtistData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setMoodData(res.data));
  } catch (err) {
    console.log({
      errArtistData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* moodSearchData({
  page,
  title
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startSearchMood()); //   try {
  //     const res = yield panelAdmin.api.get.artistSearch({ page, title });
  //     console.log({ resArtistSearchData: res });
  //     yield put(actions.reduxActions.setSearchArtistData(res.data));
  //   } catch (err) {
  //     console.log({ errArtistSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/hashtag.js



function* hashtagData({
  page
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startGetHashtag());

  try {
    const res = yield panelAdmin_0.api.get.hashtag({
      page
    });
    console.log({
      resArtistData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setHashtagData(res.data));
  } catch (err) {
    console.log({
      errArtistData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* hashtagSearchData({
  page,
  title
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startSearchHashtag()); //   try {
  //     const res = yield panelAdmin.api.get.artistSearch({ page, title });
  //     console.log({ resArtistSearchData: res });
  //     yield put(actions.reduxActions.setSearchArtistData(res.data));
  //   } catch (err) {
  //     console.log({ errArtistSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/genre.js



function* genreData({
  page
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startGetGenre());

  try {
    const res = yield panelAdmin_0.api.get.genres({
      page
    });
    console.log({
      resGenreData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setGenreData(res.data));
  } catch (err) {
    console.log({
      errGenreData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* genreSearchData({
  page,
  title
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startSearchGenre()); //   try {
  //     const res = yield panelAdmin.api.get.GenreSearch({ page, title });
  //     console.log({ resGenreSearchData: res });
  //     yield put(actions.reduxActions.setSearchGenreData(res.data));
  //   } catch (err) {
  //     console.log({ errGenreSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/albums.js



function* albumsData({
  page
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startGetAlbum());

  try {
    const res = yield panelAdmin_0.api.get.albums({
      page
    });
    console.log({
      resalbumData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setAlbumData(res.data));
  } catch (err) {
    console.log({
      erralbumData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* albumSearchData({
  page,
  title
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startSearchAlbum()); //   try {
  //     const res = yield panelAdmin.api.get.albumSearch({ page, title });
  //     console.log({ resAlbumSearchData: res });
  //     yield put(actions.reduxActions.setSearchAlbumData(res.data));
  //   } catch (err) {
  //     console.log({ errAlbumSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/index.js









const GET = {
  homeData: homeData,
  // ================================= GALLERY
  galleryData: galleryData,
  // ================================= ARTIST
  artistData: artistData,
  artistSearchData: artistSearchData,
  // ================================= INSTRUMENT
  instrumentData: instrumentData,
  instrumentSearchData: instrumentSearchData,
  // ================================= COUNTRY
  countryData: countryData,
  countrySearchData: countrySearchData,
  // ================================= MOOD
  moodData: moodData,
  moodSearchData: moodSearchData,
  // ================================= HASHTAG
  hashtagData: hashtagData,
  hashtagSearchData: hashtagSearchData,
  // ================================= GENRE
  genreData: genreData,
  genreSearchData: genreSearchData,
  // ================================= ALBUM
  albumsData: albumsData,
  albumSearchData: albumSearchData
};
/* harmony default export */ var webServices_GET = (GET);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/POST/uploadImage.js




function* uploadImage(props) {
  console.log("uploaaaaaaaaad omad");
  const {
    imageName,
    type,
    files
  } = props;
  console.log({
    uploadUploadImage: props
  });
  const toastify = panelAdmin_0.utils.toastify;
  const strings = panelAdmin_0.values.strings;
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total); // setState((prev) => ({ ...prev, progressPercentImage: percentCompleted }));
      // yield put(actions.reduxActions.uploadImageProcess());

      console.log(percentCompleted);
    },
    cancelToken: source.token
  };
  const URL = strings.ApiString.IMAGE_UPLOAD;
  const formData = new FormData();
  formData.append("imageName", imageName);
  formData.append("imageType", type);
  formData.append("image", files);

  try {
    const res = yield external_axios_default.a.post(URL, formData, settings);
    return res; // yield put(actions.reduxActions.setUploadImage(res.data));
  } catch (err) {
    console.log({
      err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090)); // yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/POST/index.js

const POST = {
  uploadImage: uploadImage
};
/* harmony default export */ var webServices_POST = (POST);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/index.js


const webServices = {
  GET: webServices_GET,
  POST: webServices_POST
};
/* harmony default export */ var saga_webServices = (webServices);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/index.js


 // ======================================================================================= UPLOAD

function* watchUploadImage() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.UPLOAD_IMAGE_DATA, saga_webServices.POST.uploadImage);
} // ======================================================================================= GALLERY

function* watchGallery() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_GALLERY_DATA, saga_webServices.GET.galleryData);
} // ======================================================================================= ARTIST
// export function* watchArtist() {
//   yield all([
//     takeEvery(atSaga.GET_ARTIST_DATA, webServices.GET.artistData),
//     takeEvery(atSaga.GET_SEARCH_ARTIST_DATA, webServices.GET.artistSearchData),
//   ]);
// }

function* watchGetArtistData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_ARTIST_DATA, saga_webServices.GET.artistData);
}
function* watchSearchArtistData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_SEARCH_ARTIST_DATA, saga_webServices.GET.artistSearchData);
} // ======================================================================================= INSTRUMENTAL

function* watchGetInstrumentalData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_INSTRUMENT_DATA, saga_webServices.GET.instrumentData);
}
function* watchSearchInstrumentalData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_SEARCH_INSTRUMENT_DATA, saga_webServices.GET.instrumentSearchData);
} // ======================================================================================= COUNTRY

function* watchGetCountryData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_COUNTRY_DATA, saga_webServices.GET.countryData);
}
function* watchSearchCountryData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_SEARCH_COUNTRY_DATA, saga_webServices.GET.countrySearchData);
} // ======================================================================================= MOOD

function* watchGetMoodData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_MOOD_DATA, saga_webServices.GET.moodData);
}
function* watchSearchMoodData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_SEARCH_MOOD_DATA, saga_webServices.GET.moodSearchData);
} // ======================================================================================= HASHTAG

function* watchGetHashtagData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_HASHTAG_DATA, saga_webServices.GET.hashtagData);
}
function* watchSearchHashtagData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_SEARCH_HASHTAG_DATA, saga_webServices.GET.hashtagSearchData);
} // ======================================================================================= GENRE

function* watchGetGenreData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_GENRE_DATA, saga_webServices.GET.genreData);
}
function* watchSearchGenreData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_SEARCH_GENRE_DATA, saga_webServices.GET.genreSearchData);
} // ======================================================================================= ALBUM

function* watchGetAlbumData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_ALBUM_DATA, saga_webServices.GET.albumsData);
}
function* watchSearchAlbumData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_SEARCH_ALBUM_DATA, saga_webServices.GET.albumSearchData);
}
// CONCATENATED MODULE: ./panelAdmin/index.js








const panelAdmin = {
  menuFormat: utils_menuFormat,
  values: panelAdmin_values,
  utils: panelAdmin_utils,
  api: api,
  reducer: storeAdmin_reducer,
  actions: storeAdmin_actions,
  actionTypes: storeAdmin_actionTypes,
  saga: storeAdmin_saga_namespaceObject
};
/* harmony default export */ var panelAdmin_0 = __webpack_exports__["a"] = (panelAdmin);

/***/ }),

/***/ "W0sL":
/***/ (function(module, exports) {

module.exports = require("next-useragent");

/***/ }),

/***/ "YFqc":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("cTJO")


/***/ }),

/***/ "YPTf":
/***/ (function(module, exports) {

module.exports = require("react-scrollbars-custom");

/***/ }),

/***/ "YTqd":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteRegex(normalizedRoute) {
  // Escape all characters that could be considered RegEx
  const escapedRoute = (normalizedRoute.replace(/\/$/, '') || '/').replace(/[|\\{}()[\]^$+*?.-]/g, '\\$&');
  const groups = {};
  let groupIndex = 1;
  const parameterizedRoute = escapedRoute.replace(/\/\\\[([^/]+?)\\\](?=\/|$)/g, (_, $1) => {
    const isCatchAll = /^(\\\.){3}/.test($1);
    groups[$1 // Un-escape key
    .replace(/\\([|\\{}()[\]^$+*?.-])/g, '$1').replace(/^\.{3}/, '') // eslint-disable-next-line no-sequences
    ] = {
      pos: groupIndex++,
      repeat: isCatchAll
    };
    return isCatchAll ? '/(.+?)' : '/([^/]+?)';
  });
  return {
    re: new RegExp('^' + parameterizedRoute + '(?:/)?$', 'i'),
    groups
  };
}

exports.getRouteRegex = getRouteRegex;

/***/ }),

/***/ "akFz":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./website/values/strings/index.js + 8 modules
var strings = __webpack_require__("wtyS");

// EXTERNAL MODULE: ./website/values/theme/themeColor.js
var themeColor = __webpack_require__("vEqB");

// CONCATENATED MODULE: ./website/values/consts/headerMiddle.js


let color = themeColor["a" /* default */].mainColor;
const headerMiddle = [{
  text: strings["a" /* default */].EXPLORE,
  icon: "far fa-compass",
  location: {
    href: "#"
  },
  color
}, {
  text: strings["a" /* default */].TRACKS,
  icon: "far fa-music",
  location: {
    href: "#"
  },
  color
}, {
  text: strings["a" /* default */].PLAYLISTS,
  icon: "far fa-list-music",
  location: {
    href: "#"
  },
  color
}, {
  text: strings["a" /* default */].ALBUMS,
  icon: "far fa-album",
  location: {
    href: "/albums"
  },
  color
}, {
  text: strings["a" /* default */].ARTISTS,
  icon: "far fa-user-music",
  location: {
    href: "#"
  },
  color
}, {
  text: strings["a" /* default */].VIDEOS,
  icon: "far fa-video",
  location: {
    href: "#"
  },
  color
}];
/* harmony default export */ var consts_headerMiddle = (headerMiddle);
// CONCATENATED MODULE: ./website/values/consts/constJson.js
const constJson = [{
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI8dfxNQMO5mcaq3AC4IxG5N-u2GQtfT-gS7Va9HT9IICnHwEhTA&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}];
/* harmony default export */ var consts_constJson = (constJson);
// CONCATENATED MODULE: ./website/values/consts/constJsonDouble.js
const constJsonDouble = [{
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUTEhMVFhUVFxcXFxUXFRYVFxcXFxcXFxUXFxUYHSggGBolHRUVITEhJSkrLi4uFx81ODMtNygtLisBCgoKDg0OFxAQGi0dICUrLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKy0tLS0tLS0tLS0tLS0tLf/AABEIAMIBAwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAAEFBgcDBAj/xABJEAACAQIDBQUECAIHBQkBAAABAgMAEQQSIQUGMUFREyJhcZEHMoGhFCNCUmKx0fBywTNTgpKiwuFDY4OT8RYXJHOjsrPD0xX/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQIDBAX/xAAkEQACAgIDAAEEAwAAAAAAAAAAAQIRAyESMUETBBQyUSJhcf/aAAwDAQACEQMRAD8AgHY3Op4nr1pBz1PrTNxPmacCug4Qgx6n1ow56n1oFowKACDHqfU0QY9T60IFGBSGOCep9aIMep9aYCjAoAQY9TRBj1PrSAogKAECep9ae56n1pAUWWkMFXvpfXzornqfWozbmJVFAHvnVTqLfiNtba/I1ExbwyAd4i4uNLcR1Fjp46fCk2UoNlqBPU+tcMfKVika57qMePgag4N7kuBJGydSO8Pkb/KvVtfakb4WXIwuy5RYk8SBzA8eNFj4tFp9kmOzQhb+9Gh484SYW+QiPxrQVNYR7NttjDSBXvbPcWv7rrlkBJ05Iw/hrXo95MOzBFlRmJAABHPr00BPp1rzM0amz0ce4k5mNPc9a5g3ojWVlni23h5JMNNHEQJHidELEgBmUqCSL2te9ZG/s4xcSZey7T8SyKWv4Xtp4VtBa1NmrXHmePoyyY1M+bNpbr46FrNh58v3gjsPVb145FtoxytaxDM6nl1UWr6gU1zmgV1IdVYE8GUMONudbL6v9oh4H+z5pTBkoWDA5GUACUG+bMxvfj7tdYYZ42AjlljLR9oWV2AACNIR3GF9ANfGvoDFbp4CUkPhIbaE2QLrrY922vH1qHm9nmAJDKkiM6mMlJG0TJlsA1wO6AK0+4i+0T8UkZJht9Npworri5CMzJlktJ7qoSe8Dp3xzq07O9sbBVE+GzGwu8clrnrkZdPK9TWO9lMEi9mmJlURs1iyo984Um9svCwHwqG/7oZUHdxMbnlmRl/mam8M+yryRRPbJ9qWDnkSLLOjuyqoZQRmYgDVWPM9Ku5Y/s1k+xvZzi4cdh5nVGjSRWdlccFuQbHXjataK1zZoQi/4muOTa2AxNC1+tdGFMwrGiwE4U1dEXSlQFmQMNT5n86cCnYanzP509q908YdRRBaQFdAKQDAUQFOBRqKBjAUYFJRRgUDGAowKa1FSAYCuWJkygkECwvc6ADmTXPG4+OEZpDYdbacbcaou3d5ZJrqvdj1sBoxHK55UmzSMWzybTxZZ7qzMdSSbnTy5DjpXlGfmH425gX8+tcIiR3gSPEafCpbC7WZkETG1rhWBI4/ZYDRh000rNtnQkiOkUi+oPiDfx+FdYnciwN78r3ufLiDSxKsrEHW3O1cTGdDlPna3zpAEC3Dn0vU5u9jUjkHbRqy3Gjq5UEkC4KHSwubm/Co6ePMobQW0J5npm/K9FH7ttT+E/5T+/jSltDWnZ9AbsYvDvf6LPnjFs0RYsY25ZSdQOII1FxpU/evn7dfbsmEYN2QZAbZrWy66DPlNutvGtW2BvimKB7MAFdCruFbwIHTx0rhnBx/w6IyUiyy3oVNNHJm/Pr6HnXSMa1lYw1olHu+f6mgJ0rsF1UdAf5CihjH7R/fD/Why6qOg/QUZ90+Jt6m1I+8x6D9TT8A4wjQnqzH52FPaniFlHkPnrXS1UloTZxIoCK7EUDCk0FgWpmWuhFMakLEi6UqQlUaFgD0JApqoVmQMNT5mnApNxPnRAV7J5AgK6AUwFGBQMQWjUUlFGBQAgKILTgUVIoYm1Q+19oWjYrowNiLXIKkMNByIv8ACpY4hLEE8tb6fnVF3jxvfKi+ZdA9+KG5Ct1tfQ0rLjEiNoYt5DqW46LckD4VwfBNoPtHl++dS6bNKZHkuCfs81FtP7R105DjTr3pwwFltlAHTl8f1rFzOlQo8EWBJUgDgLk+P7/KvIcMeJrTN3NjRlGJVXYnS/uKNNT94+FebamwCSQBwBNx1J5j48P0rD7hWdH27qygWbTXU6cflXrIKizXuDbXl4a8ONe3aGzDGx8OP76muCxtLlQk2HA6m3QE1ryTVmXBp0c8N3ja3/Tp4cqGeNraFsvi3ytfSrCuz8sZIFgBlv1IvcanSoWddMvIcel/PlxtURyKT0aTxOK2eM4xiDe5Btm10vwzW5Gu2zSqyrnJCn7Wvyym/EAUo0yhlYWOh8COh+Vc8ehFuNrX05edaaejCq2a1udtOWPEjDs/aRSAlDnD5CADbNp1tYi/ia0VUr502FtN1kja7WHA3UZSRYHMRwva4Nb1sbHAxCR5FK5ST7wyZQC2YubjjfXhbpXHOHFmylaJIiuwHePgB8yf0rGN8Pa27O0eAsiD/bst2bxRDoo8Tc+VUmLeDGTzR9pi8Q2d1B+te1i1j3QbDS9VHBJ7YfIkfTltF8Tf5XoJuD+n8q+cdm74Y+GOSWPFS/0iBQ7GRRmEjEZXuOCAfGtO3e9ogkkOFxahHVY2aYECO5EZcMD7nfew5eVEsLSGppmgk8qe1CK6EVAAGhIroRQGkMC1RO8G2kwyHgXylgCbKqjjJIfsoPnwFcN5N448Mr95cyi7u3uRA8M1tWY8kGprBt7N6XxTMqlhGWzHMe/Kw4NIR05INF5da0x4uWyJTromsbvsrSMRAs1yfrZHdWfqco0UdByFhSqlwjQUq6uETGzUmGp86ICkRqfP+dEBXScQ6ijAplFdAKB0OoolFICjFIBAUQFOBTgUFUQO8yIy2JyuPcP536iqVGbydQuvW4HAk8emtXzei4gJyhrEEg6gqD3h6XPwqg7MhLElb2sT45V1P5AVnM2xkx2jSsAx4XJ1vqTYADmbDX4U8EYDDlpqSfy8fGoqDEtwvwJ4G2vnzqY2UoLasVA5gXPpXNNNI7MbTZYtn4goFHDoP1FTcb5xwGoOvH46XHrUDIq5hYlh5EC19AL61O4F0tbmOQvl8unKuGR6MYkRjNmPKRHrkFr2AvwIHer1bN3VsLgfZ4+J/KpGWYrfLqTbT5D871O7IwgdFDAmwuVJbLc8AVva3h4U+UnoXBJ8im7dwGWOKCLUk2aTldtDlbnz1rybxbvDCwRrmHaSnNlA91Rc8eXL1rRsbswGRXfgvK3T8gKpm0pVnxLSufqoxZb89baDncggdbGhNrQOKlsoeKwJjytJYZuvK518tOVefa+HKItwAyqnC/Frn8hUztnaInm1UkKwPIC+oy/sVz2WwxWLKy+4SRYdFBCgetdcZOrZwzgrqJ5tywjv2UiB1DBrcTZtNANTY8vKrDvKpw+CxEUU1w7Il9RZNZMgJNwTlKm/3QKqJQ4fFfVkjKRbip68v2bVeJNmDaOAm7ASGWPvXJHeyWJUgm5bKTbjrSn+al4RH8WvTIq9+xzaUMR7qu392NiPnarBs3dQte4J0HIjU/CtR3c3HiGFivGCXQ3JHEMT/KtnlXSI+KVWzEYo2aKKMf7WY+oEar/72r34yUE4+S9wziMeTTdoPlBVkxm74TEQkKQsbvJblYSyPb+6i1UZ4CmFsR3nnNz/AOXGP5zmqjJS6JcXHs2b2M7wticK0MjZnw5CgniY2ByX8rMPICtEFZH7AcC4TFTEWRjHGviVzM3pmX1rXAK5siqTo1j0Mar+923VwsRObKxUsW49mg0L25kkhVHNiKsJrE/bRj2zMlz35Qp/hhjQhfi0pbzApQjykKTpFD3k3gfFPzWJSSkd76ni7n7Uh5sfLhUPEhYhQLkkADqToBTVfdyN3SlsRKLMR9Wp5A/bPj0/6V2peGEnSJ/Y278MUKI6Kzgd4/iJJPzNqVSycKVaUjn5SIsjU+ZowKTDU+ZohVGY6iui0KiugFIpDgUQFIUQoGPaipKKcCgCsb9YgpCACO8bEWJJ9OH+tVfCYvIl107QGPrZbC48+fxq078QRmMF1ObgrcuttOB/Ws/RsrZPHS3U25VnNWbY3SPZhAOut+Hh1qf2ZAxYBbj98yah9mwlpcqqTcC1vHn5aVqGw9hroZHVV01JA/u9T41z5X4dmBeg7H2E0pHdY/eYkKunlxq14rAxxRhbagacNfLp5n516cLtCJAI4ihA07p187866rhDKbyHh8/0rk+M7vk98IPZOzu0ckjQai3C/nVuwuC7NeH75UEcKrqLAAcKafbUIsC4q4xUeyJzc+jx7Zw6MjdwMSNdSF+J4W9apaYcODlUE8SxFtRwCg6KANB/rV6faMDK13vcHj5cq8GJSIxdwroPDlzPQ1Eop7TKjKlTRjG8adiWBVhrcdB4i2h8zUHsbEkShr8xr5G4qzb+2DXGvhfTlVQwisxLHhbXkPAACuvErhs4szrJok8Ue1kzA6s2nrx/OtL9k2EX6VPIAxyrlDDhZjcg+N+XnwrKsIxuABcXGnh+9a2z2RwFIZWJuXcEnxUWNx6fvjGVUkhRd2zntXa+y1lkCYmKKQMQ6OHRS6Eq2ViuUajlpVt2XtvCNGoixMD5EA7sqNYheGh46GsTwG0ZMRKUg2pKGdmYRyQM0a5mJsGzOAovbgOWmtqlNtbLxMKWeXZs7ZyG7aKGLXKuQBsi66vrm10tWiwRjtekPPJqn4WyWDML5b9y3Ak6rbTT8VQg9nsuKVEb6qNXdndh3+8w0VepCrqeFxx4VDZp4+3ePCuO8FVsFipCWGe+aySSKpCoOCjjatG9neMlljlMrTnKYltOQXDGJZH72UEnvqDccVrH4ZY1aZrLMsmqJ/Y+yosLCkEC5Y0FgOZ5kk8yTqTXsAogKcCpJOZFZD7bNlkqZAODJJ8GHZP/AIhF61sVqgN7cIGjV7XyMA2l+6/dvbwbIfhVY3UiZ9GKbpbo2tNiV14pEfkzj/L61c2NE1+B4jSgNegkcEpNnRDpSpR8KVAiPPE+ZogKTDU+Z/OiUUxBCjAoVowKBhAUYFCKMUDCFPakKIUDPFtXCpLGUdSw/D7w8RzvWV4/DBZHtfT3c9wza2uP0rYSL1RN89jpGhdc2p93iAeJJvyqWVB7KthsY5AsbZb6g2NvPnUlFNKwKx52spYm7BdBc89TVg9lexUnL51vY21HxrUMHusYTeEgKfeUgEG3A6jjqawc0nVHZHG2rujEcNi8VFdwr2QZmIzd0XsCb308a1L2abytjPqmPeUXv1FXHDbGRFKhI1U6kKigE/AV4th7PhjxbGMDNlNzp9or0/hFRNxfhrjjOPbs9m8WHdI+6SSeQ51nEuwMbLKq2KZtc7BmsPLh8PDlWw4i2YXp3w+YaEjyNqlR2W5vjRiEewNpLP2KnXORcppkv7x0tbLrfNbW3GrJs7ZGPRrOiMo0LIwsPNT+YrRU2a3ORiOhtXqSAKLD51U6fSoiDce3Zi2826plkFhxuWFuGnG3wqlbX2aMNLlvYjRtdL+FfRe0411OUE29axnezAGfGEJzsbHlew/SsotxdPo0nFTVpbKsQEVW6sNL20A1s1ud63bdyIwbOZ2Iv2TyGx7ijISoQW0WwB0HMnnWP7R2QXxS4VLERWVyTZSzHveNgMvxJrXtro2H2TMtu0KQZAtj3r2RUsupGoGmtObtxRjVJlL3Vw+KTvzT4SaMAIRaINYAllLSRKQdF0J5V7toxSlosPJg45o2YPJLHmsgVsyqFje5IsLmwBPLjaP2VjoJUlk+jWdMskqZnGZjrlCSI1gSGU25W11qxbZeOSIstwVjMkVo0ktmUhSLlWOgawuL2tXWzjTKJvVsXC4VEVosQhmlveORW76L3bdooNj2raX0txrWNwUH0VmDMweeaxYWNkfsgALnT6vTrx0rMtl7OxROESHGsVt2xVzLGXiMi3sCCraWFr/a8a1rc6JlwOGzkFzErMRaxZxnYi2mpY1lmejXES9qe1PalXObAkV5dpQdpDIn3kYfG2nztXrtQyaAnoDQIy3EHW/3grfFlBPzJrga9E49z+BfnqPkRXEivQj0jz5dhR8KVEi6UqoR4SNT5miApm4nzohQIMUYFCtGoplDiugoBRikAQFEKaiFAx7VwxuEWWNo2GjAjxB6ivQKIUDKxuNIcJjJYGOpysDwDAj9+lbHg5swF7Vhu+kjQ4qGdemXlyN/1rQd2d5RIgPUfsVyZlTs9D6eSlGvS64h7KT0FQG6OHYs0jixJtboOOvjrTbc2p2eGllH2UYjzsbVQNzvapHGHXEqQSxKsovcHkehHzrNJvaNm1HTNixg9afBYjiDxFZhtT2sKZQIoHkTS7AEG/S1qv8AsctJCsrAo0gzFTxW/AHxtahppiVOJN9pXCaSo44ll0avFi8ceA/etHIFAPaWK0NqyHb+1Po2LaW2ZhcKp0F+APkLVo+NxJtpwFibdTyrIN8ZzJiyoBIGugv8bDxvSgrkGSXGOjvurhsViMSHiuZM+dnI7gN7jOeFvDjpWte0V2XZjDtFiZ3hXtLuqqc4Y2ZQWHukDSvB7NcIohXKVJ4mxBN/EcRXT2xPGMLBHIJCsk40iy5+6jkWzAg620oi+WTowlqBAbn4PHRNIJsUsquoyg4nMQRmawEwBViFta1tdfCzAYgSMWQNGRdcsSO4Ol1LAWCj3ha5tw4VRH2/hJcsMwxCHgqtCq2a9kbuOCALE8Na6bJx8ky5UxOHlZjljJSWFgQrsScyFb3KHoBeuuSZyo9u0N4UV58N9HAJw5kQqMrCRzl0LZgmjKLganiNNNfw0IjRUHBFVR5KAB+VZBhosW+0GTtu0gMuHj0kSUEdpG0pKXJTRHHAca2Q1hlXRti6GNI0qasjQVRm8WK7OBre8/cW/VtGJ6ALc17cXikiUvIwVRz/ACAHM+ArMd5d5PpGLXDLo1iXXj2MI1Ksf62Q5Qw+yptxOlQjyZMpUh52BYkcNAP4QAF+QFcSK7OKAiu5I4HsJBpTUUY0pU6CzwnifOiApm4nzNEKBBLRChFGopjQQoxQiiFIYQo1oKKgYQp70IpwaAK9v7hO0wjMOMZD/AcfkTVf3G2iyyBb6HUDl5VoLqCCDqCCCPOsq2phHwM+XXJmzI1jqOg8eVZzjaNsUuLL1tLfaMg4fEIyKwIvbunl6VTX2LgHl7mMCIeN1Jt5HTSrvh8Rhto4fLKAWHA6Ai/Cx5VHYbYuPwr/APh4Enj4r3QGHhrXNCl/TO+uTXLaLDu2dm4VcsUxnPG0UbSnxuFHGp6PeeIkLFFimbhY4eVRwtqWAA9ar2Bn23IcownZA8ywC+gNW3ZuyJYgHxEhdrXIuAo62AFZySWzf+NVFjbNfFO318QjGpALKxtyvbgfia6YjDAtc8tL17J8aNL+YPDS3yqs7e3hWEEn4G/HXlUqn0Zu/Tz7w7TTDwk8zwB5nUWrENq4h+3Y3IZTa40II8fWrnHjGx+MQH3UOY+QuRf41G787tSxH6SoLRSM1yBfs31zA2+zoSDXRjSXZz5ZcuujlszfnGRWuyyAcpUDN/f9751Z/wDt3LiYpZGgQth0FgWd0BldVLAFwyWCkXU373KsvU/sVKbF2xJhw/Zsoz2DBkjcEDwdTprWsYqznk9FzwO98EzAYvCKcisVkRmJjAUsbBybcOR4mpTAbc2ZE8H0aKSNXZiBlzKTYRsHJdWyki9vw1UcLtwFZC+FwjWTiIezY5mRLExMunePpXtjxOEzRn6MylMO8l452soZZGtllV7nvjW/EjpWjRBN+z6KH6Yk4xCylppJTZGia6QyCwR9NDiVOhPEVsK7YjP2Zf8AlOfyFfMu3xAsUAhMhB7R8siqD3mCe8psdYugqHjmI4Fh5EisZw5M1jKkfWMm2EH2JT/wmHzawqu7a3/w8AN5IkI5PIJH/wCVCWPqRXzc0pI1JPmSfzrmTUrEh8maDvX7TJJiRh86nUds9g4H+6jHdi/i1bxFF7M8H3Jp2uS7ZATxIHeY353JHpWdohYgAXJIAHUnQCtq2Fs/6Ph44uar3v4ySzfMmtYJGWR6PawoGFdDQGtTnY6cKVFGNKVAjwHifM060zcT50QpjCFGKFaMUDCFPTU4pAEDT0N6V6Bh3pXoL0E0qqpZiFUcWJAA+JoA6568G24IJYiuIKhPvMQuU9QTzqs7a35RLrh1zn77XCDyHFvlVI2ptWbENeVy1uA4Kvko0FS2i4wZK7A2uMNMQGzx5iAeFxfRrHhy9a2Ddze9HsMwsLD1H/X0rAEr0Q4l11ViPIkcOFc88fLZ148vHT2j6Wk3ijDFS3AXseNeXEbwLfvN3e9x6af618/Da8/324W48qCTGSMLMxI6XrN4JP02X1K/RfN5t9C75YfcUnXraqljtoy4h7XJJ0AF7V59n7PeVgqj9K03c/c5Y7O4u3lw8q0UY40YucsjC3I3eMEWZh3m1P6Vc4MMPohuNRLGR59tHb87fGvSYAq2r0dn3I0+9NF8cjiU/KM1Ftuy+lRgu/eyUw2PxECKBGGDIOiuqvYdACxA8qjdlbXmwylYpmRSxJW+hPDVToeHSrZ7YkttR/GKE/Ij+VVjZ23MTEoSKd1XU5A111OvcNx8q7Y7SOKXbPcN45OykMkeHkIZAM+Hi1Bzk3KgE+4vOvVPtCC85fCIMkaRns5JIybmNCLMWUWseA4CrFuzh/puFvI0Xbdsf6XCRMjxKApKdkFYsC471+ZHjVbx+0MOwnLYVTmnCho3liLW7Rr5XzAH3dLfa8KQFe3meLPGIw6hYkBDsGILDP7wAv7/AEFQ5NTG8UatiZclwFbJYkMe53OIA07ulPs7Y8MtgcVHGekiMP8AF7p9ahxZaaSIS9FGhYgKCSdAALknoBzrQMB7P4Tq2JMg/wB2FA/vXarPsvYmHw39FGA33z3nP9o6+lHETmkV7czdMwkTzj6z7Cfc/E34vDl58LjTGmq0qMW2xzTUjTGmI6oNKemQ6UqQjwEanzNOKZuJ86cVQghRigFEDQUHSoQae9ABUqjtq7Zhww+sbvHgi6sfG3IeJsKpu1d7p5brH9UngbuR4vy+HrQNKy2ba3hhw1wTnk5Rrxv+I/ZHz8KoG2drTYk3kOn2UGir8OZ8TXjA/f8AOnAootaPI8dcJI7VIOtcpI6mUTRM8aGuq1zkS1HBqQOFRRQYqZ2Fsd8QwsO7zNS+xN0DIAx4Vf8AY2yxEAAo9BUSnXRUY8g93N2VjUWX41dMNhgo4cK82BQgCveVJFY9m/R5pBmYV0U5sUicoYzI38UpMcf+FZvUV6cLh+Zqgbwb/R4SGVoiHxeKcsi3zCGIARwM/Q5FDhOrm+lXGNmcpFK9rGMWXac2U3EapGT4ot29CxHwqFw23p0RUzIyKNFeGGQaDh3kJ+dRTMTdmJJa5JJuSTqSTzJNTH/9ePg+Dw7WAF17WNviUcD5V1pUjkk7ZLYPeZlOHUwQgAPKOzEkOU3kDMqxOFBKx2Omt64YWXCyfR80MiZ5XkOWbOL3jVi2dSx9w8+vWpbF7EjUZ2wsgRMNmMkOKSRFVkN1s63zDOTa+vGoxIcMMpjmcdnA7hZIhoGEjXLox1u45dKkGVeUhmZhezMSLgA2J5gG16ApTqdKerGNA7RnNGzI3VSVPyq0bH3zdbLiRnX+sAGceYGjfI+dVi9MVpUJ7Ndw+ISRQ6MGU8GGoNdL1mWwNtPhX5tGfeS/+JejD51o8UyuqupurAEHqDwpENUdb0jQXpwaCTtHwp6aMaU9AzwtxPmacUzDU+dOBTEEKKhpUAEKqu8G9oS8eH1YaGTQqD+EfaPjw868u923iWMELWAuJGHEnmoPIDn6VUlWg0jH0KR2ZizMWZjckm5J8SeNOKa1EKZQ9KlSoENT20pGlQM4tHXEw17rUNqVD5ElsDenFYTRCHT7kgzD4Hiv5eFXbZ/tSh07bCOD1jdWHo2W3rWcWprVDxxZSyNGwx+1jAAf0OKH9iL/APSuWK9sWGA+qwk7H8bRoP8ACWrJLUqPiiHysue3fanj8SpjjCYdGBB7O7SEHQjtG4fAA1SUitRkimzCrUUiG2wkYXW+ouLgGxIuLgG2htUzIMAb2bFR3NjdIZhf4FLjSojB4ftJEQMi5j7ztlUWBOp5cKncNu1OWTL2Ugzgns54X0uOQa/XlQSya2ntJJYsRGuIw+U9nHaSCWHJkuoHcDIx+rJDcio5aVD7Q2W6DEOGjYJFHH3JEY6GJWut8w0VjqOdcpdhYgL9ZDKrSzC90awGupNrWvKfSvDjpLpK39ZiOPgvaEj/ANRPlU0VdkeKc01PViFSFMaEUAItVv3H2pxw7Hq0f+df5+tUy9ejBYkxusi8UII+HEfEXHxpA0azenBrlFIGAYcGAI8iLj86O9IyPVG2lKhiJtSoGeVuJ86cUjxPmaQpiFXk2rjOxhkk+6pt/EdF+ZFes1VN/sXZI4QdXOdvJdAPU3/s0DStlMF+J1PM9SeNPTUiaDU6CnNcYW1I+IruaYMYU9MKRNAhU4FNenoGFTU6mhoEI0jSp6AGBpWpjSBoAWShKCulMKBigUBwegJ+VWXdDCJJiY1kAZcrue+EICgm4PM6DT9Khdl4x4ZC8ZF8tu8quCCfuuCOVTMe3rh+0gw72j/qhGbsVW14iunePpR4Q9smtu7PXAthfo0sxibtJe1EgMbqBmsCti1gijhbU6m+lV2ptGaSCBZZC+rv3rFuIQd46/ZbnzqTbF4Y+9h2TJh+Mcx0Eg4BZA2v1x586idtiIGIRZ7CJdHAuMxLjVePvdBUop9kbSvSNKqAY01O1A1IYJ404NIimFAzRt0sRnwqdULJ6HT5EVMA1Vtwpfq5V6Orf3lt/lq0XpGMtM9EZ0pUMfClQAZUXNOFHSmpUAIqOlUDfgD6V/w1/NqalTLh2QYUdBSyjoKVKgoB1GZdBXpyi3ClSpIb8BCjoKcqOgpqVMTHyjoKdlHQUqVMBKo6CllHQUqVIXoig6CnCjoKVKmN9A5R0FNlHQUqVAMIKOgpFR0FKlQIUajvaDgPzr2YdBlk0HBOX4hT0qQju6i8+n+yh/8AprzbWUdrwH9HF/8AGtKlQM8eUdBSKjoKalTGM6joKHKOlKlSGDkFhoKQUdBSpUDfRatwx3pvJPzareFF6VKgwl2dFFNSpUhH/9k="
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRF9IIwP_vI4I30S2hhzXBtsMQQ5IDg4mUlCJG5YADjyk1iZtzD&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ2lWkUYUjSisqChVAJ2uaMwWIVb2aSP55jx-MTTxL2OIHY1TWX&usqp=CAU"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}];
/* harmony default export */ var consts_constJsonDouble = (constJsonDouble);
// CONCATENATED MODULE: ./website/values/consts/moreExploreConst.js
const moreExploreConst = [{
  title: "Geners"
}, {
  title: "Instruments"
}, {
  title: "Videos"
}, {
  title: "Artists"
}, {
  title: "PlayList"
}, {
  title: "example"
}, {
  title: "example2"
}, {
  title: "example3"
}, {
  title: "example4"
}, {
  title: "example5"
}, {
  title: "example6"
}];
/* harmony default export */ var consts_moreExploreConst = (moreExploreConst);
// CONCATENATED MODULE: ./website/values/consts/instrumentals.js
const instrumentals = [{
  titleFa: "پیانو",
  titleEn: "Piano",
  icon: "far fa-piano"
}, {
  titleFa: "چمپن استیک",
  titleEn: "Chapman Stick",
  icon: "far fa-guitar-electric"
}, {
  titleFa: "گیتار",
  titleEn: "Guitar",
  icon: "fal fa-guitar"
}, {
  titleFa: "ساکسیفون",
  titleEn: "Saxophone",
  icon: "fal fa-saxophone"
}, {
  titleFa: "پیانو",
  titleEn: "Piano",
  icon: "far fa-piano"
}, {
  titleFa: "چمپن استیک",
  titleEn: "Chapman Stick",
  icon: "far fa-guitar-electric"
}, {
  titleFa: "گیتار",
  titleEn: "Guitar",
  icon: "fal fa-guitar"
}, {
  titleFa: "ساکسیفون",
  titleEn: "Saxophone",
  icon: "fal fa-saxophone"
}, {
  titleFa: "پیانو",
  titleEn: "Piano",
  icon: "far fa-piano"
}, {
  titleFa: "چمپن استیک",
  titleEn: "Chapman Stick",
  icon: "far fa-guitar-electric"
}, {
  titleFa: "گیتار",
  titleEn: "Guitar",
  icon: "fal fa-guitar"
}, {
  titleFa: "ساکسیفون",
  titleEn: "Saxophone",
  icon: "fal fa-saxophone"
}];
/* harmony default export */ var consts_instrumentals = (instrumentals);
// CONCATENATED MODULE: ./website/values/consts/index.js





const consts = {
  headerMiddle: consts_headerMiddle,
  constJson: consts_constJson,
  constJsonDouble: consts_constJsonDouble,
  moreExploreConst: consts_moreExploreConst,
  instrumentals: consts_instrumentals
};
/* harmony default export */ var values_consts = (consts);
// CONCATENATED MODULE: ./website/values/index.js



const values = {
  consts: values_consts,
  strings: strings["a" /* default */],
  themeColor: themeColor["a" /* default */]
};
/* harmony default export */ var website_values = (values);
// CONCATENATED MODULE: ./website/utils/convert/sliderCard.js


const sliderCard = (data, direction) => {
  let convertData = []; // console.log({PAPPAAPA:data});

  for (const index in data) {
    let noEntries = website_0.values.strings.NO_ENTRIED;
    let title = data[index].title ? data[index].title : noEntries;
    let parentName = data[index].parentName ? data[index].parentName : noEntries;
    let parentArtist = data[index].parentArtist ? data[index].parentArtist : noEntries;
    convertData.push({
      titleTop: title,
      titleMiddle: parentName,
      titleBottom: parentArtist,
      images: data[index].images,
      location: {
        href: "/slider",
        as: `/slider#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_sliderCard = (sliderCard);
// CONCATENATED MODULE: ./website/utils/chunkArray.js
const chunkArray = (myArray, chunkSize) => {
  let results = [];

  while (myArray.length) {
    results.push(myArray.splice(0, chunkSize));
  }

  return results;
};

/* harmony default export */ var utils_chunkArray = (chunkArray);
// CONCATENATED MODULE: ./website/utils/convert/albumCard.js


const albumCard = (data, chunkNumber, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (let index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleMiddle: data[index].artist,
        titleBottom: [data[index].releaseDate, data[index].genres[0]],
        images: data[index].images,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    if (chunkNumber) convertData = utils_chunkArray(convertData, chunkNumber); // convertData = chunkArray(convertData, Math.ceil(data.length / 2));

    return convertData;
  }
};

/* harmony default export */ var convert_albumCard = (albumCard);
// CONCATENATED MODULE: ./website/utils/convert/topTracksCard.js


const topTracksCard = (data, chunkNumber, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (let index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleBottom: [data[index].artist, ", ", data[index].artist],
        image: data[index].cover,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    if (chunkNumber) convertData = utils_chunkArray(convertData, chunkNumber); // convertData = chunkArray(convertData, Math.ceil(data.length / 2));

    return convertData;
  }
};

/* harmony default export */ var convert_topTracksCard = (topTracksCard);
// CONCATENATED MODULE: ./website/utils/convert/singleCard.js


const singleCard = (data, direction) => {
  if (data) {
    // console.log({ data });
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (const index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleMiddle: data[index].artist,
        titleBottom: [data[index].releaseDate, data[index].genres[0]],
        images: data[index].images,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    convertData = utils_chunkArray(convertData, 2); // convertData = chunkArray(convertData, Math.ceil(data.length / 2));
    // console.log({ convertData });

    return convertData;
  }
};

/* harmony default export */ var convert_singleCard = (singleCard);
// CONCATENATED MODULE: ./website/utils/convert/comingSoonCard.js


const comingSoonCard = (data, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (const index in data) {
      let noEntries = website_0.values.strings.NO_ENTRIED;
      let releaseDate = data[index].releaseDate;
      let year = releaseDate.year + "/" + releaseDate.month + "/" + releaseDate.day;
      let title = data[index].title ? data[index].title : noEntries;
      let artist = data[index].artist ? data[index].artist : noEntries;
      let genres = data[index].genres.length ? data[index].genres[0] : noEntries;
      convertData.push({
        titleTop: title,
        titleMiddle: artist,
        titleBottom: [year, genres],
        images: data[index].images,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    return convertData;
  }
};

/* harmony default export */ var convert_comingSoonCard = (comingSoonCard);
// CONCATENATED MODULE: ./website/utils/convert/moodCard.js
const moodCard = (data, direction) => {
  let convertData = []; // console.log({ mood: data });

  let dir = false;
  if (direction === "rtl") dir = true;

  for (const index in data) {
    convertData.push({
      title: data[index].title,
      images: data[index].images,
      location: {
        href: "/slider",
        as: `/slider#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_moodCard = (moodCard);
// CONCATENATED MODULE: ./website/utils/convert/playlistCard.js


const playlistCard = (data, direction) => {
  let convertData = []; // console.log({PAPPAAPA:data});

  let noEntries = website_0.values.strings.NO_ENTRIED;

  for (const index in data) {
    let title = data[index].title ? data[index].title : noEntries;
    let publisher = data[index].publisher ? data[index].publisher : noEntries;
    let tracksCount = data[index].tracksCount ? data[index].tracksCount : "0";
    let genres = data[index].genres ? data[index].genres : "0";
    convertData.push({
      titleTop: title,
      titleMiddle: publisher,
      titleBottom: [tracksCount + " " + website_0.values.strings.TRACKS, genres[0] + " " + website_0.values.strings.FOLLOWERS],
      images: data[index].images,
      location: {
        href: "/playlist",
        as: `/playlist#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_playlistCard = (playlistCard);
// CONCATENATED MODULE: ./website/utils/convert/videoCard.js


const videoCard = data => {
  let convertData = []; // console.log({ mood: data });

  for (const index in data) {
    let noEntries = website_0.values.strings.NO_ENTRIED;
    let title = data[index].title ? data[index].title : noEntries;
    let artist = data[index].artist ? data[index].artist : noEntries;
    let releaseDate = data[index].releaseDate ? data[index].releaseDate : "0";
    let genres = data[index].genres ? data[index].genres : "0";
    convertData.push({
      titleTop: title,
      titleMiddle: artist,
      titleBottom: [releaseDate, genres[0]],
      images: data[index].images,
      location: {
        href: "/video",
        as: `/video#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_videoCard = (videoCard);
// CONCATENATED MODULE: ./website/utils/convert/PlaylistForAutuminCard.js


const PlaylistForAutuminCard = (data, direction) => {
  let convertData = []; // console.log({PAPPAAPA:data});

  let noEntries = website_0.values.strings.NO_ENTRIED;

  for (const index in data) {
    let title = data[index].title ? data[index].title : noEntries;
    let publisher = data[index].publisher ? data[index].publisher : noEntries;
    let tracksCount = data[index].tracksCount ? data[index].tracksCount : "0";
    let genres = data[index].genres ? data[index].genres : "0";
    convertData.push({
      titleTop: title,
      titleMiddle: publisher,
      titleBottom: [tracksCount + " " + website_0.values.strings.TRACKS, genres[0] + " " + website_0.values.strings.FOLLOWERS],
      images: data[index].images,
      location: {
        href: "/playlist",
        as: `/playlist#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_PlaylistForAutuminCard = (PlaylistForAutuminCard);
// CONCATENATED MODULE: ./website/utils/convert/index.js








 // import moreExploreCard from "./moreExploreCard";

const convert = {
  sliderCard: convert_sliderCard,
  albumCard: convert_albumCard,
  singleCard: convert_singleCard,
  comingSoonCard: convert_comingSoonCard,
  moodCard: convert_moodCard,
  playlistCard: convert_playlistCard,
  videoCard: convert_videoCard,
  PlaylistForAutuminCard: convert_PlaylistForAutuminCard,
  topTrackCard: convert_topTracksCard
};
/* harmony default export */ var utils_convert = (convert);
// CONCATENATED MODULE: ./website/utils/index.js

const utils = {
  convert: utils_convert
};
/* harmony default export */ var website_utils = (utils);
// EXTERNAL MODULE: external "redux"
var external_redux_ = __webpack_require__("rKB8");

// EXTERNAL MODULE: ./website/storeWebsite/actionTypes/redux/index.js
var redux = __webpack_require__("lmmF");

// CONCATENATED MODULE: ./website/storeWebsite/reducer/errorReducer.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const errorInitialState = {
  error: null
};
const addFailure = (state, action) => {
  return _objectSpread({}, state, {
    error: action.error
  });
};
const removeFailure = state => {
  return _objectSpread({}, state, {
    error: null
  });
};

function errorReducer(state = errorInitialState, action) {
  switch (action.type) {
    case redux["a" /* default */].ADD_FAILURE:
      return addFailure(state, action);

    case redux["a" /* default */].REMOVE_FAILURE:
      return removeFailure(state);

    default:
      return state;
  }
}

/* harmony default export */ var reducer_errorReducer = (errorReducer);
// CONCATENATED MODULE: ./website/storeWebsite/reducer/homeReducer.js
function homeReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function homeReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { homeReducer_ownKeys(Object(source), true).forEach(function (key) { homeReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { homeReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function homeReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const homeInitialState = {
  homeData: []
};
const setHomeData = (state, action) => {
  return homeReducer_objectSpread({}, state, {
    homeData: action.data
  });
};

function homeReducer(state = homeInitialState, action) {
  switch (action.type) {
    case redux["a" /* default */].SET_HOME_DATA:
      return setHomeData(state, action);

    default:
      return state;
  }
}

/* harmony default export */ var reducer_homeReducer = (homeReducer);
// CONCATENATED MODULE: ./website/storeWebsite/reducer/themeColorReducer.js
function themeColorReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function themeColorReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { themeColorReducer_ownKeys(Object(source), true).forEach(function (key) { themeColorReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { themeColorReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function themeColorReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const webThemeColorInitialstate = {
  light: {
    primaryColor: "#0070ef",
    accentColor: "#666666",
    mainColor: "#ffff",
    titleColor: "#333333"
  },
  dark: {
    primaryColor: "#0070ef",
    accentColor: "#666666",
    mainColor: "#000",
    titleColor: "#ffff"
  },
  currentTheme: "light"
};
const setThemeColor = (state, action) => {
  return themeColorReducer_objectSpread({}, state, {
    homeData: action.data
  });
};

function themeColorReducer(state = webThemeColorInitialstate, action) {
  //   switch (action.type) {
  //     case atRedux.:
  //       return setThemeColor(state, action);
  //     default:
  //       return state;
  //   }
  return state;
}

/* harmony default export */ var reducer_themeColorReducer = (themeColorReducer);
// CONCATENATED MODULE: ./website/storeWebsite/reducer/filterReducer.js
function filterReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function filterReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { filterReducer_ownKeys(Object(source), true).forEach(function (key) { filterReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { filterReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function filterReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const filterInisialState = {
  filterData: []
};
const setFilter = (state, action) => {
  if (state.filterData.length > 0) {
    const filter = state.filterData.filter(item => {
      if (item.type !== action.data.type) return item;
    });
    return filterReducer_objectSpread({}, state, {
      filterData: [...filter, action.data]
    });
  } else {
    return filterReducer_objectSpread({}, state, {
      filterData: [...state.filterData, action.data]
    });
  }
};
const removeFilter = (state, action) => {
  return filterReducer_objectSpread({}, state, {
    filterData: []
  });
};

function filterReducer(state = filterInisialState, action) {
  switch (action.type) {
    case redux["a" /* default */].SET_FILTER_TYPE:
      return setFilter(state, action);

    case redux["a" /* default */].REMOVE_FILTER_TYPE:
      return removeFilter();

    default:
      return state;
  }
}

/* harmony default export */ var reducer_filterReducer = (filterReducer);
// CONCATENATED MODULE: ./website/storeWebsite/reducer/index.js





const rootReducer = {
  error: reducer_errorReducer,
  home: reducer_homeReducer,
  themeColor: reducer_themeColorReducer,
  filter: reducer_filterReducer
};
/* harmony default export */ var reducer = (rootReducer);
// CONCATENATED MODULE: ./website/index.js



const website = {
  values: website_values,
  utils: website_utils,
  reducer: reducer
};
/* harmony default export */ var website_0 = __webpack_exports__["a"] = (website);

/***/ }),

/***/ "bzos":
/***/ (function(module, exports) {

module.exports = require("url");

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "cDf5":
/***/ (function(module, exports) {

function _typeof2(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

function _typeof(obj) {
  if (typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return _typeof2(obj);
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "cTJO":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__("TqRt");

var _interopRequireWildcard = __webpack_require__("284h");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__("cDcd"));

var _url = __webpack_require__("bzos");

var _utils = __webpack_require__("kYf9");

var _router = _interopRequireDefault(__webpack_require__("nOHt"));

var _router2 = __webpack_require__("elyg");

function isLocal(href) {
  var url = (0, _url.parse)(href, false, true);
  var origin = (0, _url.parse)((0, _utils.getLocationOrigin)(), false, true);
  return !url.host || url.protocol === origin.protocol && url.host === origin.host;
}

function memoizedFormatUrl(formatFunc) {
  var lastHref = null;
  var lastAs = null;
  var lastResult = null;
  return (href, as) => {
    if (lastResult && href === lastHref && as === lastAs) {
      return lastResult;
    }

    var result = formatFunc(href, as);
    lastHref = href;
    lastAs = as;
    lastResult = result;
    return result;
  };
}

function formatUrl(url) {
  return url && typeof url === 'object' ? (0, _utils.formatWithValidation)(url) : url;
}

var observer;
var listeners = new Map();
var IntersectionObserver = false ? undefined : null;
var prefetched = {};

function getObserver() {
  // Return shared instance of IntersectionObserver if already created
  if (observer) {
    return observer;
  } // Only create shared IntersectionObserver if supported in browser


  if (!IntersectionObserver) {
    return undefined;
  }

  return observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (!listeners.has(entry.target)) {
        return;
      }

      var cb = listeners.get(entry.target);

      if (entry.isIntersecting || entry.intersectionRatio > 0) {
        observer.unobserve(entry.target);
        listeners.delete(entry.target);
        cb();
      }
    });
  }, {
    rootMargin: '200px'
  });
}

var listenToIntersections = (el, cb) => {
  var observer = getObserver();

  if (!observer) {
    return () => {};
  }

  observer.observe(el);
  listeners.set(el, cb);
  return () => {
    try {
      observer.unobserve(el);
    } catch (err) {
      console.error(err);
    }

    listeners.delete(el);
  };
};

class Link extends _react.Component {
  constructor(props) {
    super(props);
    this.p = void 0;

    this.cleanUpListeners = () => {};

    this.formatUrls = memoizedFormatUrl((href, asHref) => {
      return {
        href: (0, _router2.addBasePath)(formatUrl(href)),
        as: asHref ? (0, _router2.addBasePath)(formatUrl(asHref)) : asHref
      };
    });

    this.linkClicked = e => {
      var {
        nodeName,
        target
      } = e.currentTarget;

      if (nodeName === 'A' && (target && target !== '_self' || e.metaKey || e.ctrlKey || e.shiftKey || e.nativeEvent && e.nativeEvent.which === 2)) {
        // ignore click for new tab / new window behavior
        return;
      }

      var {
        href,
        as
      } = this.formatUrls(this.props.href, this.props.as);

      if (!isLocal(href)) {
        // ignore click if it's outside our scope (e.g. https://google.com)
        return;
      }

      var {
        pathname
      } = window.location;
      href = (0, _url.resolve)(pathname, href);
      as = as ? (0, _url.resolve)(pathname, as) : href;
      e.preventDefault(); //  avoid scroll for urls with anchor refs

      var {
        scroll
      } = this.props;

      if (scroll == null) {
        scroll = as.indexOf('#') < 0;
      } // replace state instead of push if prop is present


      _router.default[this.props.replace ? 'replace' : 'push'](href, as, {
        shallow: this.props.shallow
      }).then(success => {
        if (!success) return;

        if (scroll) {
          window.scrollTo(0, 0);
          document.body.focus();
        }
      });
    };

    if (false) {}

    this.p = props.prefetch !== false;
  }

  componentWillUnmount() {
    this.cleanUpListeners();
  }

  getPaths() {
    var {
      pathname
    } = window.location;
    var {
      href: parsedHref,
      as: parsedAs
    } = this.formatUrls(this.props.href, this.props.as);
    var resolvedHref = (0, _url.resolve)(pathname, parsedHref);
    return [resolvedHref, parsedAs ? (0, _url.resolve)(pathname, parsedAs) : resolvedHref];
  }

  handleRef(ref) {
    if (this.p && IntersectionObserver && ref && ref.tagName) {
      this.cleanUpListeners();
      var isPrefetched = prefetched[this.getPaths().join( // Join on an invalid URI character
      '%')];

      if (!isPrefetched) {
        this.cleanUpListeners = listenToIntersections(ref, () => {
          this.prefetch();
        });
      }
    }
  } // The function is memoized so that no extra lifecycles are needed
  // as per https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html


  prefetch(options) {
    if (!this.p || true) return; // Prefetch the JSON page if asked (only in the client)

    var paths = this.getPaths(); // We need to handle a prefetch error here since we may be
    // loading with priority which can reject but we don't
    // want to force navigation since this is only a prefetch

    _router.default.prefetch(paths[
    /* href */
    0], paths[
    /* asPath */
    1], options).catch(err => {
      if (false) {}
    });

    prefetched[paths.join( // Join on an invalid URI character
    '%')] = true;
  }

  render() {
    var {
      children
    } = this.props;
    var {
      href,
      as
    } = this.formatUrls(this.props.href, this.props.as); // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

    if (typeof children === 'string') {
      children = _react.default.createElement("a", null, children);
    } // This will return the first child, if multiple are provided it will throw an error


    var child = _react.Children.only(children);

    var props = {
      ref: el => {
        this.handleRef(el);

        if (child && typeof child === 'object' && child.ref) {
          if (typeof child.ref === 'function') child.ref(el);else if (typeof child.ref === 'object') {
            child.ref.current = el;
          }
        }
      },
      onMouseEnter: e => {
        if (child.props && typeof child.props.onMouseEnter === 'function') {
          child.props.onMouseEnter(e);
        }

        this.prefetch({
          priority: true
        });
      },
      onClick: e => {
        if (child.props && typeof child.props.onClick === 'function') {
          child.props.onClick(e);
        }

        if (!e.defaultPrevented) {
          this.linkClicked(e);
        }
      }
    }; // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
    // defined, we specify the current 'href', so that repetition is not needed by the user

    if (this.props.passHref || child.type === 'a' && !('href' in child.props)) {
      props.href = as || href;
    } // Add the ending slash to the paths. So, we can serve the
    // "<page>/index.html" directly.


    if (false) { var rewriteUrlForNextExport; }

    return _react.default.cloneElement(child, props);
  }

}

if (false) { var exact, PropTypes, warn; }

var _default = Link;
exports.default = _default;

/***/ }),

/***/ "dZ6Y":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/*
MIT License

Copyright (c) Jason Miller (https://jasonformat.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

Object.defineProperty(exports, "__esModule", {
  value: true
});

function mitt() {
  const all = Object.create(null);
  return {
    on(type, handler) {
      ;
      (all[type] || (all[type] = [])).push(handler);
    },

    off(type, handler) {
      if (all[type]) {
        // tslint:disable-next-line:no-bitwise
        all[type].splice(all[type].indexOf(handler) >>> 0, 1);
      }
    },

    emit(type, ...evts) {
      // eslint-disable-next-line array-callback-return
      ;
      (all[type] || []).slice().map(handler => {
        handler(...evts);
      });
    }

  };
}

exports.default = mitt;

/***/ }),

/***/ "elyg":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__("bzos");

const mitt_1 = __importDefault(__webpack_require__("dZ6Y"));

const utils_1 = __webpack_require__("g/15");

const is_dynamic_1 = __webpack_require__("/jkW");

const route_matcher_1 = __webpack_require__("gguc");

const route_regex_1 = __webpack_require__("YTqd");

const basePath =  false || '';

function addBasePath(path) {
  return path.indexOf(basePath) !== 0 ? basePath + path : path;
}

exports.addBasePath = addBasePath;

function delBasePath(path) {
  return path.indexOf(basePath) === 0 ? path.substr(basePath.length) || '/' : path;
}

function toRoute(path) {
  return path.replace(/\/$/, '') || '/';
}

const prepareRoute = path => toRoute(!path || path === '/' ? '/index' : path);

function fetchNextData(pathname, query, isServerRender, cb) {
  let attempts = isServerRender ? 3 : 1;

  function getResponse() {
    return fetch(utils_1.formatWithValidation({
      // @ts-ignore __NEXT_DATA__
      pathname: `/_next/data/${__NEXT_DATA__.buildId}${pathname}.json`,
      query
    }), {
      // Cookies are required to be present for Next.js' SSG "Preview Mode".
      // Cookies may also be required for `getServerSideProps`.
      //
      // > `fetch` won’t send cookies, unless you set the credentials init
      // > option.
      // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
      //
      // > For maximum browser compatibility when it comes to sending &
      // > receiving cookies, always supply the `credentials: 'same-origin'`
      // > option instead of relying on the default.
      // https://github.com/github/fetch#caveats
      credentials: 'same-origin'
    }).then(res => {
      if (!res.ok) {
        if (--attempts > 0 && res.status >= 500) {
          return getResponse();
        }

        throw new Error(`Failed to load static props`);
      }

      return res.json();
    });
  }

  return getResponse().then(data => {
    return cb ? cb(data) : data;
  }).catch(err => {
    // We should only trigger a server-side transition if this was caused
    // on a client-side transition. Otherwise, we'd get into an infinite
    // loop.
    if (!isServerRender) {
      ;
      err.code = 'PAGE_LOAD_ERROR';
    }

    throw err;
  });
}

class Router {
  constructor(pathname, query, as, {
    initialProps,
    pageLoader,
    App,
    wrapApp,
    Component,
    err,
    subscription,
    isFallback
  }) {
    // Static Data Cache
    this.sdc = {};

    this.onPopState = e => {
      if (!e.state) {
        // We get state as undefined for two reasons.
        //  1. With older safari (< 8) and older chrome (< 34)
        //  2. When the URL changed with #
        //
        // In the both cases, we don't need to proceed and change the route.
        // (as it's already changed)
        // But we can simply replace the state with the new changes.
        // Actually, for (1) we don't need to nothing. But it's hard to detect that event.
        // So, doing the following for (1) does no harm.
        const {
          pathname,
          query
        } = this;
        this.changeState('replaceState', utils_1.formatWithValidation({
          pathname,
          query
        }), utils_1.getURL());
        return;
      } // Make sure we don't re-render on initial load,
      // can be caused by navigating back from an external site


      if (e.state && this.isSsr && e.state.as === this.asPath && url_1.parse(e.state.url).pathname === this.pathname) {
        return;
      } // If the downstream application returns falsy, return.
      // They will then be responsible for handling the event.


      if (this._bps && !this._bps(e.state)) {
        return;
      }

      const {
        url,
        as,
        options
      } = e.state;

      if (false) {}

      this.replace(url, as, options);
    };

    this._getStaticData = asPath => {
      const pathname = prepareRoute(url_1.parse(asPath).pathname);
      return  true && this.sdc[pathname] ? Promise.resolve(this.sdc[pathname]) : fetchNextData(pathname, null, this.isSsr, data => this.sdc[pathname] = data);
    };

    this._getServerData = asPath => {
      let {
        pathname,
        query
      } = url_1.parse(asPath, true);
      pathname = prepareRoute(pathname);
      return fetchNextData(pathname, query, this.isSsr);
    }; // represents the current component key


    this.route = toRoute(pathname); // set up the component cache (by route keys)

    this.components = {}; // We should not keep the cache, if there's an error
    // Otherwise, this cause issues when when going back and
    // come again to the errored page.

    if (pathname !== '/_error') {
      this.components[this.route] = {
        Component,
        props: initialProps,
        err,
        __N_SSG: initialProps && initialProps.__N_SSG,
        __N_SSP: initialProps && initialProps.__N_SSP
      };
    }

    this.components['/_app'] = {
      Component: App
    }; // Backwards compat for Router.router.events
    // TODO: Should be remove the following major version as it was never documented

    this.events = Router.events;
    this.pageLoader = pageLoader;
    this.pathname = pathname;
    this.query = query; // if auto prerendered and dynamic route wait to update asPath
    // until after mount to prevent hydration mismatch

    this.asPath = // @ts-ignore this is temporarily global (attached to window)
    is_dynamic_1.isDynamicRoute(pathname) && __NEXT_DATA__.autoExport ? pathname : as;
    this.basePath = basePath;
    this.sub = subscription;
    this.clc = null;
    this._wrapApp = wrapApp; // make sure to ignore extra popState in safari on navigating
    // back from external site

    this.isSsr = true;
    this.isFallback = isFallback;

    if (false) {}
  } // @deprecated backwards compatibility even though it's a private method.


  static _rewriteUrlForNextExport(url) {
    if (false) {} else {
      return url;
    }
  }

  update(route, mod) {
    const Component = mod.default || mod;
    const data = this.components[route];

    if (!data) {
      throw new Error(`Cannot update unavailable route: ${route}`);
    }

    const newData = Object.assign(Object.assign({}, data), {
      Component,
      __N_SSG: mod.__N_SSG,
      __N_SSP: mod.__N_SSP
    });
    this.components[route] = newData; // pages/_app.js updated

    if (route === '/_app') {
      this.notify(this.components[this.route]);
      return;
    }

    if (route === this.route) {
      this.notify(newData);
    }
  }

  reload() {
    window.location.reload();
  }
  /**
   * Go back in history
   */


  back() {
    window.history.back();
  }
  /**
   * Performs a `pushState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  push(url, as = url, options = {}) {
    return this.change('pushState', url, as, options);
  }
  /**
   * Performs a `replaceState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  replace(url, as = url, options = {}) {
    return this.change('replaceState', url, as, options);
  }

  change(method, _url, _as, options) {
    return new Promise((resolve, reject) => {
      if (!options._h) {
        this.isSsr = false;
      } // marking route changes as a navigation start entry


      if (utils_1.ST) {
        performance.mark('routeChange');
      } // If url and as provided as an object representation,
      // we'll format them into the string version here.


      let url = typeof _url === 'object' ? utils_1.formatWithValidation(_url) : _url;
      let as = typeof _as === 'object' ? utils_1.formatWithValidation(_as) : _as;
      url = addBasePath(url);
      as = addBasePath(as); // Add the ending slash to the paths. So, we can serve the
      // "<page>/index.html" directly for the SSR page.

      if (false) {}

      this.abortComponentLoad(as); // If the url change is only related to a hash change
      // We should not proceed. We should only change the state.
      // WARNING: `_h` is an internal option for handing Next.js client-side
      // hydration. Your app should _never_ use this property. It may change at
      // any time without notice.

      if (!options._h && this.onlyAHashChange(as)) {
        this.asPath = as;
        Router.events.emit('hashChangeStart', as);
        this.changeState(method, url, as, options);
        this.scrollToHash(as);
        Router.events.emit('hashChangeComplete', as);
        return resolve(true);
      }

      const {
        pathname,
        query,
        protocol
      } = url_1.parse(url, true);

      if (!pathname || protocol) {
        if (false) {}

        return resolve(false);
      } // If asked to change the current URL we should reload the current page
      // (not location.reload() but reload getInitialProps and other Next.js stuffs)
      // We also need to set the method = replaceState always
      // as this should not go into the history (That's how browsers work)
      // We should compare the new asPath to the current asPath, not the url


      if (!this.urlIsNew(as)) {
        method = 'replaceState';
      }

      const route = toRoute(pathname);
      const {
        shallow = false
      } = options;

      if (is_dynamic_1.isDynamicRoute(route)) {
        const {
          pathname: asPathname
        } = url_1.parse(as);
        const routeRegex = route_regex_1.getRouteRegex(route);
        const routeMatch = route_matcher_1.getRouteMatcher(routeRegex)(asPathname);

        if (!routeMatch) {
          const missingParams = Object.keys(routeRegex.groups).filter(param => !query[param]);

          if (missingParams.length > 0) {
            if (false) {}

            return reject(new Error(`The provided \`as\` value (${asPathname}) is incompatible with the \`href\` value (${route}). ` + `Read more: https://err.sh/zeit/next.js/incompatible-href-as`));
          }
        } else {
          // Merge params into `query`, overwriting any specified in search
          Object.assign(query, routeMatch);
        }
      }

      Router.events.emit('routeChangeStart', as); // If shallow is true and the route exists in the router cache we reuse the previous result

      this.getRouteInfo(route, pathname, query, as, shallow).then(routeInfo => {
        const {
          error
        } = routeInfo;

        if (error && error.cancelled) {
          return resolve(false);
        }

        Router.events.emit('beforeHistoryChange', as);
        this.changeState(method, url, as, options);

        if (false) {}

        this.set(route, pathname, query, as, routeInfo);

        if (error) {
          Router.events.emit('routeChangeError', error, as);
          throw error;
        }

        Router.events.emit('routeChangeComplete', as);
        return resolve(true);
      }, reject);
    });
  }

  changeState(method, url, as, options = {}) {
    if (false) {}

    if (method !== 'pushState' || utils_1.getURL() !== as) {
      window.history[method]({
        url,
        as,
        options
      }, // Most browsers currently ignores this parameter, although they may use it in the future.
      // Passing the empty string here should be safe against future changes to the method.
      // https://developer.mozilla.org/en-US/docs/Web/API/History/replaceState
      '', as);
    }
  }

  getRouteInfo(route, pathname, query, as, shallow = false) {
    const cachedRouteInfo = this.components[route]; // If there is a shallow route transition possible
    // If the route is already rendered on the screen.

    if (shallow && cachedRouteInfo && this.route === route) {
      return Promise.resolve(cachedRouteInfo);
    }

    const handleError = (err, loadErrorFail) => {
      return new Promise(resolve => {
        if (err.code === 'PAGE_LOAD_ERROR' || loadErrorFail) {
          // If we can't load the page it could be one of following reasons
          //  1. Page doesn't exists
          //  2. Page does exist in a different zone
          //  3. Internal error while loading the page
          // So, doing a hard reload is the proper way to deal with this.
          window.location.href = as; // Changing the URL doesn't block executing the current code path.
          // So, we need to mark it as a cancelled error and stop the routing logic.

          err.cancelled = true; // @ts-ignore TODO: fix the control flow here

          return resolve({
            error: err
          });
        }

        if (err.cancelled) {
          // @ts-ignore TODO: fix the control flow here
          return resolve({
            error: err
          });
        }

        resolve(this.fetchComponent('/_error').then(res => {
          const {
            page: Component
          } = res;
          const routeInfo = {
            Component,
            err
          };
          return new Promise(resolve => {
            this.getInitialProps(Component, {
              err,
              pathname,
              query
            }).then(props => {
              routeInfo.props = props;
              routeInfo.error = err;
              resolve(routeInfo);
            }, gipErr => {
              console.error('Error in error page `getInitialProps`: ', gipErr);
              routeInfo.error = err;
              routeInfo.props = {};
              resolve(routeInfo);
            });
          });
        }).catch(err => handleError(err, true)));
      });
    };

    return new Promise((resolve, reject) => {
      if (cachedRouteInfo) {
        return resolve(cachedRouteInfo);
      }

      this.fetchComponent(route).then(res => resolve({
        Component: res.page,
        __N_SSG: res.mod.__N_SSG,
        __N_SSP: res.mod.__N_SSP
      }), reject);
    }).then(routeInfo => {
      const {
        Component,
        __N_SSG,
        __N_SSP
      } = routeInfo;

      if (false) {}

      return this._getData(() => __N_SSG ? this._getStaticData(as) : __N_SSP ? this._getServerData(as) : this.getInitialProps(Component, // we provide AppTree later so this needs to be `any`
      {
        pathname,
        query,
        asPath: as
      })).then(props => {
        routeInfo.props = props;
        this.components[route] = routeInfo;
        return routeInfo;
      });
    }).catch(handleError);
  }

  set(route, pathname, query, as, data) {
    this.isFallback = false;
    this.route = route;
    this.pathname = pathname;
    this.query = query;
    this.asPath = as;
    this.notify(data);
  }
  /**
   * Callback to execute before replacing router state
   * @param cb callback to be executed
   */


  beforePopState(cb) {
    this._bps = cb;
  }

  onlyAHashChange(as) {
    if (!this.asPath) return false;
    const [oldUrlNoHash, oldHash] = this.asPath.split('#');
    const [newUrlNoHash, newHash] = as.split('#'); // Makes sure we scroll to the provided hash if the url/hash are the same

    if (newHash && oldUrlNoHash === newUrlNoHash && oldHash === newHash) {
      return true;
    } // If the urls are change, there's more than a hash change


    if (oldUrlNoHash !== newUrlNoHash) {
      return false;
    } // If the hash has changed, then it's a hash only change.
    // This check is necessary to handle both the enter and
    // leave hash === '' cases. The identity case falls through
    // and is treated as a next reload.


    return oldHash !== newHash;
  }

  scrollToHash(as) {
    const [, hash] = as.split('#'); // Scroll to top if the hash is just `#` with no value

    if (hash === '') {
      window.scrollTo(0, 0);
      return;
    } // First we check if the element by id is found


    const idEl = document.getElementById(hash);

    if (idEl) {
      idEl.scrollIntoView();
      return;
    } // If there's no element with the id, we check the `name` property
    // To mirror browsers


    const nameEl = document.getElementsByName(hash)[0];

    if (nameEl) {
      nameEl.scrollIntoView();
    }
  }

  urlIsNew(asPath) {
    return this.asPath !== asPath;
  }
  /**
   * Prefetch page code, you may wait for the data during page rendering.
   * This feature only works in production!
   * @param url the href of prefetched page
   * @param asPath the as path of the prefetched page
   */


  prefetch(url, asPath = url, options = {}) {
    return new Promise((resolve, reject) => {
      const {
        pathname,
        protocol
      } = url_1.parse(url);

      if (!pathname || protocol) {
        if (false) {}

        return;
      } // Prefetch is not supported in development mode because it would trigger on-demand-entries


      if (false) {}

      const route = delBasePath(toRoute(pathname));
      Promise.all([this.pageLoader.prefetchData(url, delBasePath(asPath)), this.pageLoader[options.priority ? 'loadPage' : 'prefetch'](route)]).then(() => resolve(), reject);
    });
  }

  async fetchComponent(route) {
    let cancelled = false;

    const cancel = this.clc = () => {
      cancelled = true;
    };

    route = delBasePath(route);
    const componentResult = await this.pageLoader.loadPage(route);

    if (cancelled) {
      const error = new Error(`Abort fetching component for route: "${route}"`);
      error.cancelled = true;
      throw error;
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    return componentResult;
  }

  _getData(fn) {
    let cancelled = false;

    const cancel = () => {
      cancelled = true;
    };

    this.clc = cancel;
    return fn().then(data => {
      if (cancel === this.clc) {
        this.clc = null;
      }

      if (cancelled) {
        const err = new Error('Loading initial props cancelled');
        err.cancelled = true;
        throw err;
      }

      return data;
    });
  }

  getInitialProps(Component, ctx) {
    const {
      Component: App
    } = this.components['/_app'];

    const AppTree = this._wrapApp(App);

    ctx.AppTree = AppTree;
    return utils_1.loadGetInitialProps(App, {
      AppTree,
      Component,
      router: this,
      ctx
    });
  }

  abortComponentLoad(as) {
    if (this.clc) {
      const e = new Error('Route Cancelled');
      e.cancelled = true;
      Router.events.emit('routeChangeError', e, as);
      this.clc();
      this.clc = null;
    }
  }

  notify(data) {
    this.sub(data, this.components['/_app'].Component);
  }

}

exports.default = Router;
Router.events = mitt_1.default();

/***/ }),

/***/ "faye":
/***/ (function(module, exports) {

module.exports = require("react-dom");

/***/ }),

/***/ "g/15":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__("bzos");
/**
 * Utils
 */


function execOnce(fn) {
  let used = false;
  let result;
  return (...args) => {
    if (!used) {
      used = true;
      result = fn(...args);
    }

    return result;
  };
}

exports.execOnce = execOnce;

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

exports.getLocationOrigin = getLocationOrigin;

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

exports.getURL = getURL;

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

exports.getDisplayName = getDisplayName;

function isResSent(res) {
  return res.finished || res.headersSent;
}

exports.isResSent = isResSent;

async function loadGetInitialProps(App, ctx) {
  var _a;

  if (false) {} // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!App.getInitialProps) {
    if (ctx.ctx && ctx.Component) {
      // @ts-ignore pageProps default
      return {
        pageProps: await loadGetInitialProps(ctx.Component, ctx.ctx)
      };
    }

    return {};
  }

  const props = await App.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(App)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (false) {}

  return props;
}

exports.loadGetInitialProps = loadGetInitialProps;
exports.urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];

function formatWithValidation(url, options) {
  if (false) {}

  return url_1.format(url, options);
}

exports.formatWithValidation = formatWithValidation;
exports.SP = typeof performance !== 'undefined';
exports.ST = exports.SP && typeof performance.mark === 'function' && typeof performance.measure === 'function';

/***/ }),

/***/ "ge5p":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const updateObject = (oldObject, updatedProperties) => {
  return _objectSpread({}, oldObject, {}, updatedProperties);
};

/* harmony default export */ __webpack_exports__["a"] = (updateObject);

/***/ }),

/***/ "gguc":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteMatcher(routeRegex) {
  const {
    re,
    groups
  } = routeRegex;
  return pathname => {
    const routeMatch = re.exec(pathname);

    if (!routeMatch) {
      return false;
    }

    const decode = param => {
      try {
        return decodeURIComponent(param);
      } catch (_) {
        const err = new Error('failed to decode param');
        err.code = 'DECODE_FAILED';
        throw err;
      }
    };

    const params = {};
    Object.keys(groups).forEach(slugName => {
      const g = groups[slugName];
      const m = routeMatch[g.pos];

      if (m !== undefined) {
        params[slugName] = ~m.indexOf('/') ? m.split('/').map(entry => decode(entry)) : g.repeat ? [decode(m)] : decode(m);
      }
    });
    return params;
  };
}

exports.getRouteMatcher = getRouteMatcher;

/***/ }),

/***/ "h74D":
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "kYf9":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/utils.js");

/***/ }),

/***/ "lmmF":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const atRedux = {
  //======================================================== Redux
  SET_HOME_DATA: "SET_HOME_DATA_REDUX",
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX",
  SET_FILTER_TYPE: "SET_FILTER_TYPE_REDUX",
  REMOVE_FILTER_TYPE: "REMOVE_FILTER_TYPE_REDUX"
};
/* harmony default export */ __webpack_exports__["a"] = (atRedux);

/***/ }),

/***/ "nOHt":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__("284h");

var _interopRequireDefault = __webpack_require__("TqRt");

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__("cDcd"));

var _router2 = _interopRequireWildcard(__webpack_require__("elyg"));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__("Osoz");

var _withRouter = _interopRequireDefault(__webpack_require__("0Bsm"));

exports.withRouter = _withRouter.default;
/* global window */

var singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

var urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components', 'isFallback', 'basePath'];
var routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
var coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

Object.defineProperty(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  Object.defineProperty(singletonRouter, field, {
    get() {
      var router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = function () {
    var router = getRouter();
    return router[field](...arguments);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, function () {
      var eventField = "on" + event.charAt(0).toUpperCase() + event.substring(1);
      var _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...arguments);
        } catch (err) {
          // tslint:disable-next-line:no-console
          console.error("Error when running the Router event: " + eventField); // tslint:disable-next-line:no-console

          console.error(err.message + "\n" + err.stack);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    var message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


var createRouter = function createRouter() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  var _router = router;
  var instance = {};

  for (var property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = Object.assign({}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = function () {
      return _router[field](...arguments);
    };
  });
  return instance;
}

/***/ }),

/***/ "oAEb":
/***/ (function(module, exports) {

module.exports = require("react-toastify");

/***/ }),

/***/ "on69":
/***/ (function(module, exports) {



/***/ }),

/***/ "rKB8":
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),

/***/ "rUrl":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const atRedux = {
  //======================================================== Redux
  SET_HOME_DATA: "SET_HOME_DATA_REDUX",
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX" // // ======================================================== UPLOAD
  // SET_UPLOAD_IMAGE: "SET_UPLOAD_IMAGE_REDUX",
  // // ======================================================== GALLERY
  // SET_GALLERY_DATA: "SET_GALLERY_DATA_REDUX",
  // CHANGE_ADD_GALLERY_DATA: "CHANGE_ADD_GALLERY_DATA_REDUX",
  // // ======================================================== ARTIST
  // SET_ARTIST_DATA: "SET_GALLERY_DATA_REDUX",
  // START_ARTIST_DATA: "START_GALLERY_DATA_REDUX",
  // // ======================= SEARCH ARTIST
  // SET_SEARCH_ARTIST_DATA: "SET_SEARCH_ARTIST_DATA_REDUX",
  // START_SEARCH_ARTIST_DATA: "START_SEARCH_ARTIST_DATA_REDUX",
  // // ======================================================== END ARTIST
  // // ======================================================== NAVBAR
  // SET_PAGE_NAME: "SET_PAGE_NAME",

};
/* harmony default export */ __webpack_exports__["a"] = (atRedux);

/***/ }),

/***/ "sHut":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const atSaga = {
  GET_HOME_SCREEN_DATA: "GET_HOME_SCREEN_DATA_SAGA" // POST_UPLOAD_IMAGE: "POST_UPLOAD_IMAGE_SAGA",
  // GET_GALLERY_DATA: "GET_GALLERY_DATA_SAGA",
  // GET_ARTIST_DATA: "GET_ARTIST_DATA_SAGA",
  // GET_SEARCH_ARTIST_DATA: "GET_SEARCH_ARTIST_DATA_SAGA",

};
/* harmony default export */ __webpack_exports__["a"] = (atSaga);

/***/ }),

/***/ "vEqB":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
  mainColor: "#0070ef",
  accentColor: "#666666",
  white: "#ffff",
  black: "#000",
  textColor: "#666666"
});

/***/ }),

/***/ "vfdN":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// CONCATENATED MODULE: ./website/components/Header/HamburgerMenu/index.js
var __jsx = external_react_default.a.createElement;
 // import "./index.scss";
// import { useDispatch, useSelector } from "react-redux";
// import * as actions from "../../../../store/actions";

const HamburgerMenu = () => {
  // const dispatch = useDispatch();
  // const user = useSelector((state) => {
  //   console.log({ state });
  //   return state;
  // });
  // console.log({ user });
  // const getUser = () => dispatch(actions.setUserInfo("tooooooooooooooooooken"));
  return __jsx("div", {
    className: "HamburgerMenu"
  }, __jsx("div", {
    className: "border-hamburger-menu"
  }, __jsx("i", {
    // onClick={getUser}
    className: "fas fa-bars"
  })));
};

/* harmony default export */ var Header_HamburgerMenu = (HamburgerMenu);
// EXTERNAL MODULE: ./public/assets/images/Rimtal.png
var Rimtal = __webpack_require__("2FOw");
var Rimtal_default = /*#__PURE__*/__webpack_require__.n(Rimtal);

// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__("YFqc");
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);

// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__("4Q3z");

// CONCATENATED MODULE: ./website/components/Header/HeaderMiddleElement/index.js
var HeaderMiddleElement_jsx = external_react_default.a.createElement;


 // import "./index.scss";

const HeaderMiddleElement = ({
  icon,
  text,
  color,
  location
}) => {
  const router = Object(router_["useRouter"])();
  let locationActive = false;
  if (router.pathname === location.href) locationActive = true;
  return HeaderMiddleElement_jsx(link_default.a, location, HeaderMiddleElement_jsx("a", {
    className: `${locationActive ? "url-active-in-page" : ""} navbar-link-to-other-page`
  }, HeaderMiddleElement_jsx("div", {
    className: "text-icon-wrapper pointer "
  }, HeaderMiddleElement_jsx("div", null, HeaderMiddleElement_jsx("div", {
    className: "header-icon-middle"
  }, HeaderMiddleElement_jsx("i", {
    style: {
      color
    },
    className: icon
  })), HeaderMiddleElement_jsx("div", {
    className: "header-text-middle "
  }, text)), HeaderMiddleElement_jsx("div", {
    className: "active-page"
  }))));
};

/* harmony default export */ var Header_HeaderMiddleElement = (HeaderMiddleElement);
// EXTERNAL MODULE: ./website/values/strings/index.js + 8 modules
var strings = __webpack_require__("wtyS");

// CONCATENATED MODULE: ./website/components/Header/LoginElement/index.js
var LoginElement_jsx = external_react_default.a.createElement;
 // import "./index.scss";

const LoginElement = ({
  color,
  strings
}) => {
  return LoginElement_jsx("div", {
    className: "login-element "
  }, LoginElement_jsx("div", {
    className: "login-wrapper"
  }, LoginElement_jsx("div", {
    className: "login-element-container pointer",
    style: {
      backgroundColor: color.mainColor,
      color: color.white
    }
  }, LoginElement_jsx("div", null, LoginElement_jsx("i", {
    className: "fas fa-user-alt"
  })), LoginElement_jsx("div", null, LoginElement_jsx("div", {
    className: ""
  }, strings.SIGN_IN))), LoginElement_jsx("div", {
    className: "for-small-display login-element-container pointer "
  }, LoginElement_jsx("i", {
    className: "far fa-user-circle"
  }), LoginElement_jsx("div", {
    className: "chevron-down-icon"
  }, " ", LoginElement_jsx("i", {
    className: "fal fa-chevron-down"
  })))));
};

/* harmony default export */ var Header_LoginElement = (LoginElement);
// EXTERNAL MODULE: ./website/values/theme/themeColor.js
var themeColor = __webpack_require__("vEqB");

// EXTERNAL MODULE: ./website/index.js + 24 modules
var website = __webpack_require__("akFz");

// EXTERNAL MODULE: ./components/AwesomeScroll/index.js
var AwesomeScroll = __webpack_require__("K3nj");

// CONCATENATED MODULE: ./website/components/Header/index/index.js
var index_jsx = external_react_default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }




 // import consts from "../../../../public/values/consts";


 // import "./index.scss";






const Header = () => {
  return index_jsx("header", {
    style: {
      color: themeColor["a" /* default */].textColor
    },
    className: "header-wrapper "
  }, index_jsx("div", {
    className: "header-container  row-main-wrapper",
    onDragStart: e => e.preventDefault()
  }, index_jsx("div", {
    className: "header-side-elements"
  }, index_jsx(Header_HamburgerMenu, null), index_jsx("div", {
    className: "header-search-icon centerAll pointer for-small-display"
  }, index_jsx("i", {
    className: "fal fa-search"
  })), index_jsx(link_default.a, {
    href: "/"
  }, index_jsx("a", {
    className: "logo-website"
  }, index_jsx("img", {
    src: Rimtal_default.a,
    alt: "logo"
  })))), index_jsx("div", {
    className: "logo-website for-small-display"
  }, index_jsx(link_default.a, {
    href: "/"
  }, index_jsx("a", null, index_jsx("img", {
    src: Rimtal_default.a,
    alt: "logo"
  })))), index_jsx("div", {
    className: "header-middle-elements "
  }, index_jsx(AwesomeScroll["a" /* default */], null, index_jsx("div", {
    onDragStart: e => e.preventDefault(),
    className: "header-middle-into-elemen"
  }, website["a" /* default */].values.consts.headerMiddle.map((middle, index) => {
    return index_jsx(external_react_["Fragment"], null, index_jsx(Header_HeaderMiddleElement, _extends({}, middle, {
      key: "header-" + index
    })));
  })))), index_jsx("div", {
    className: "header-side-elements flex-end-for-small"
  }, index_jsx("div", {
    className: "header-search-icon centerAll pointer"
  }, index_jsx("i", {
    className: "fal fa-search"
  })), index_jsx("div", {
    className: "header-bell-icon pointer"
  }, index_jsx("i", {
    className: "far fa-bell"
  })), index_jsx(Header_LoginElement, {
    strings: strings["a" /* default */],
    color: themeColor["a" /* default */]
  }))));
};

/* harmony default export */ var Header_index = __webpack_exports__["a"] = (Header);

/***/ }),

/***/ "wtyS":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./website/values/strings/en/navbar.js
const APP_NAME = "Rimtal";
const EXPLORE = "Explore";
const TRACKS = "Tracks";
const PLAYLISTS = "Playlists";
const ALBUMS = "Albums";
const ARTISTS = "Artists";
const VIDEOS = "Videos";
const SIGN_IN = "Sign In";
const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN
};
/* harmony default export */ var en_navbar = (navbar);
// CONCATENATED MODULE: ./website/values/strings/en/homePage.js
const VIEW_ALL = "View All";
const SINGLES = "Singles";
const COMING_SOON = "Coming Soon";
const PLAY_LISTS = "Playlists";
const homePage_VIDEOS = "Videos";
const PLAYLIST_FOR_AUTUMN = "Playlist For Autumn";
const MOOD = "Mood";
const COMMING_SOON = "Comming Soon";
const homePage = {
  MOOD,
  SINGLES,
  VIEW_ALL,
  COMING_SOON,
  PLAY_LISTS,
  VIDEOS: homePage_VIDEOS,
  MOOD,
  PLAYLIST_FOR_AUTUMN,
  COMMING_SOON
};
/* harmony default export */ var en_homePage = (homePage);
// CONCATENATED MODULE: ./website/values/strings/en/global.js
const global_TRACKS = "Tracks";
const NO_ENTRIED = "no entried";
const FOLLOWERS = "followers";
const global = {
  TRACKS: global_TRACKS,
  NO_ENTRIED,
  FOLLOWERS
};
/* harmony default export */ var en_global = (global);
// CONCATENATED MODULE: ./website/values/strings/en/index.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const en = _objectSpread({}, en_navbar, {}, en_homePage, {}, en_global);

/* harmony default export */ var strings_en = (en);
// CONCATENATED MODULE: ./website/values/strings/fa/navbar.js
const navbar_APP_NAME = "ریمتال";
const navbar_EXPLORE = "کاوش کردن";
const navbar_TRACKS = "آهنگ ها";
const navbar_PLAYLISTS = "لیست های پخش";
const navbar_ALBUMS = "آلبوم ها";
const navbar_ARTISTS = "هنرمندان";
const navbar_VIDEOS = "ویدیو ها";
const navbar_SIGN_IN = "ورود";
const navbar_navbar = {
  APP_NAME: navbar_APP_NAME,
  EXPLORE: navbar_EXPLORE,
  TRACKS: navbar_TRACKS,
  PLAYLISTS: navbar_PLAYLISTS,
  ALBUMS: navbar_ALBUMS,
  ARTISTS: navbar_ARTISTS,
  VIDEOS: navbar_VIDEOS,
  SIGN_IN: navbar_SIGN_IN
};
/* harmony default export */ var fa_navbar = (navbar_navbar);
// CONCATENATED MODULE: ./website/values/strings/fa/homePage.js
const homePage_VIEW_ALL = "مشاهده همه";
const homePage_SINGLES = "تک آهنگ ها";
const homePage_COMING_SOON = "بزودی";
const homePage_PLAY_LISTS = "لیست پخش";
const fa_homePage_VIDEOS = "ویدیو ها";
const homePage_PLAYLIST_FOR_AUTUMN = "لیست پخش پیشنهادی ";
const homePage_MOOD = "حالت";
const homePage_COMMING_SOON = "بزودی";
const homePage_homePage = {
  MOOD: homePage_MOOD,
  SINGLES: homePage_SINGLES,
  VIEW_ALL: homePage_VIEW_ALL,
  SINGLES: homePage_SINGLES,
  COMING_SOON: homePage_COMING_SOON,
  PLAY_LISTS: homePage_PLAY_LISTS,
  VIDEOS: fa_homePage_VIDEOS,
  PLAYLIST_FOR_AUTUMN: homePage_PLAYLIST_FOR_AUTUMN,
  COMMING_SOON: homePage_COMMING_SOON
};
/* harmony default export */ var fa_homePage = (homePage_homePage);
// CONCATENATED MODULE: ./website/values/strings/fa/global.js
const fa_global_TRACKS = "آهنگ ها ";
const global_NO_ENTRIED = "وارد نشده";
const global_FOLLOWERS = "دنبال کنندگان";
const global_global = {
  TRACKS: fa_global_TRACKS,
  NO_ENTRIED: global_NO_ENTRIED,
  FOLLOWERS: global_FOLLOWERS
};
/* harmony default export */ var fa_global = (global_global);
// CONCATENATED MODULE: ./website/values/strings/fa/index.js
function fa_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function fa_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { fa_ownKeys(Object(source), true).forEach(function (key) { fa_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { fa_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function fa_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const fa = fa_objectSpread({}, fa_navbar, {}, fa_homePage, {}, fa_global);

/* harmony default export */ var strings_fa = (fa);
// CONCATENATED MODULE: ./website/values/strings/index.js


const Lang = "ltr";
let string;
if (Lang === "rtl") string = strings_fa;else string = strings_en;
/* harmony default export */ var strings = __webpack_exports__["a"] = (string);

/***/ }),

/***/ "xnum":
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "zr5I":
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ })

/******/ });