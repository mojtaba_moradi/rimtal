module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "/jkW":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
}); // Identify /[param]/ in route string

const TEST_ROUTE = /\/\[[^/]+?\](?=\/|$)/;

function isDynamicRoute(route) {
  return TEST_ROUTE.test(route);
}

exports.isDynamicRoute = isDynamicRoute;

/***/ }),

/***/ "0Bsm":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__("TqRt");

exports.__esModule = true;
exports.default = withRouter;

var _react = _interopRequireDefault(__webpack_require__("cDcd"));

var _router = __webpack_require__("nOHt");

function withRouter(ComposedComponent) {
  function WithRouterWrapper(props) {
    return _react.default.createElement(ComposedComponent, Object.assign({
      router: (0, _router.useRouter)()
    }, props));
  }

  WithRouterWrapper.getInitialProps = ComposedComponent.getInitialProps // This is needed to allow checking for custom getInitialProps in _app
  ;
  WithRouterWrapper.origGetInitialProps = ComposedComponent.origGetInitialProps;

  if (false) { var name; }

  return WithRouterWrapper;
}

/***/ }),

/***/ "284h":
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__("cDf5");

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ "2FOw":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAXCAYAAADHqJcNAAAGwUlEQVRogb2aeahVRRzHP/pepZlttlFR2WpiWUm9tL2o1GwjbKesBNMwzIoMaTda7I8kWohKIgiL9sUWyHYqLZdS20XL1MQ2l3pu78UPvhM/pjnnnnPufX7gcu/MnTNnzpzf/LaZToz5g4i9gBOBXsCW8Z9iPbAY+Aj4LKON50zgGuAd4C5gQ0a7PsBoYD4wqUC/RdkKuAPYBrhZYw/YMw4DdmrQvX4CntIcpWgGTgUGAUcAPYF5wHTgFc1pWYZpfp8B7gbaynbgBWEX4FbgAmDrEn28C9wOvJfxv03+t8DOKh8LfJho1wR8Ahyu8nnAsyXGkcd9wLX6f4qeMfAQMLJB9wnYfNySqB+s+iNyrn0TuAH4suC9TIAXAN1UPgSYU3bAnfW9LzANGFFSCIwTgLeASzP+7+4GaWyf0a5ZwhgwoexSciwprM/hrn6PqM2BDbhHzAGJuiuBV2sIgTFQi+r4gvey97WFK/eoMuBmvaiXExNiq3hl4pp2YHNgbyc0Vn4cWJTQDNZ+oytnqa32yGTYeIZKzdbDSGmlQGyWbPXdnzOB7RLeHVU2lb9Q9Sl+kUbwmBl4OKoz8/QpsALYDTga2E7/2ffzQH/guxrP3qb5bXbl0tjFY4He7kLTDOOBmcC6jA47aWWZlI9Tnan2B6Xa/64ymATjNSFV+zO1OSoxdo/Z5gHRqvLYC78amKi6pUALsCbRl7E2Km+reQm0yTyYSfrd1e8J3ARcobIJ3z3A2TWeMRbI1Jhq0lkvM2CO3xBJapYQhJvb6r8RuM3V95apqEr8UKZiL6+jP3OidijYdm3GZ11iLlpVl7om5lw5hAFbOBMiIUDzaSbsSVd3hmx+LbK0U2E6R3bZXuo/Jfu4E/jBlU+uYzypBxob+RhF2UYruRHEq6ypRJ+D3O9ZclzzGOdMcmc5zWWorBECFj7MqNDH+iiE7JnTtgo9I61VlBGyvTFVJsoLaJnrzdnt68ovFFi9y4D3XXlIDcGrWxsQCUJrTnxfC69FNq/YR94DjY0cvlp064CQsAr7RFHKzIJ9POd+9yoQ2VQV1P/wgtCpaifRdQ2RUOADl5TZVSu8KJcoMYaEe0nGWItS1SE71K1m8ym+L3jd2848mEN/Wk7b9kYLQj3kOZZl8A80RZnIwPUuKZVHF5c8Qkmq1xo0vrL4nMFCfYqwTA574OycF9yQqKG5QJt9ldzYXdKdWvH9q9w8ge+7Vd71QJXN+79KKeI8hkolB+5RNjOwKX2Eg9zv2Tlp5xQmvKeovp9MxNflhl2cPEHoosTIqJJeexmPOg9LdH0srXCS2pndfyRS9Z4tXF4DTdzrkede1fyVxfY39nPXTC95vY37Xr2HZoXlWYLQYabBOntM6rhs6JbKRlYhjG2Cu9a0wqNA14z+JkbJsbBx1SgTGCgy2fvLtwmUjcgWyKwFshJLsY9QiSyNYLHrRa7cpoGtrzEJy7XL10gsZf0ScJb6PE2bVvcqbN0gFTxGu3qBOS45U/eKiSjSx2Gu3coCqeIUb7gE3VHKPi6K2sWCUMTc/4/4otChT8sulWDMiPYMUpSxgTF53u9oZdhCJNBPW65rJKTdo75WKUvXmrjPpvIRWtzvhVokZXlL2/ZN0oKDE3sWfwJ/uTkwjTi17I1ilblBpsDHrZO0Alv1ovM+9ZI14YulCeLt1W4JIVgsNfq5q2tUSFuUTtIIgdkVN4Pm6trA6Yk2qyNtc3EVPy0WhHY5XF5T/Fy20w7CDqsco42ZHxO3WKJdxJYo7IzZFBphV/kIgSKHd1K0ySwGjnS7oB6fgOpbILL6H7EgNEnNrHB1w3N25jqS1ISvkvN4sFbcEG3MtOh00zUZEUW9GqFsrH6AoobAF3Xc+xVnkrfL2MuxrfpvXNkE4YmMcxFJYh+hSTc1KbxOdSco923x+It1PFAj+VsbOLMK9rmpTUMf9/tXne2oynyp/mCuzwGejvparRzLVLdoLwPOl4CsdsIbf5spHZ/lYU7UDcMGUos2TN7QyaGyMXFRGu3ddwRFxuUzivPk0FVlg3IKQRCOk2aID5tO0xG8yW5fpqvS3LVY5U2DF4rlOnA6N+pgkOzd5Ch7l4dN3Gbu/7yJ9GNoVGKKyARultMuCz/mWuFZcyQIpc8PJvCauIeEIavdAGn0MhuIy5vdAYkV0Yr8Ssenhiuc3Fb19oIulOPygGxRKkwLbJRghWNtqcMb6N4r3JnGRp1yQnF8eM74QEgRbHc1XPdbjQssFW/RTGjfCO1pEZD1Y+l+w06ZeyfSY6bEoibzo+wdWe7BNIMPz/33EuCJfwFWG3TGQDr4PQAAAABJRU5ErkJggg=="

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("RNiq");


/***/ }),

/***/ "3OaI":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAhwAAAE2CAMAAAAd/7uDAAAAAXNSR0IB2cksfwAAAGlQTFRFAAAA5+fn5+fn5+fn5+fn5ubm5+fn4uLi2tra09PTzs7OzMzM3d3d5ubm39/f19fXz8/P0NDQ4+Pj5OTk3Nzc1NTUzc3N4eHh2dnZ0dHR5eXl3t7e1tbW29vb4ODg2NjY1dXV0tLS5+fnMnVfxgAAACN0Uk5TAIDn/32F6P///////////////////////////////////3bDR9C7AAAGs0lEQVR4nO3djVLbaBKGUW+YJMb8h78JITOw93+RC7vZ2izwIsm0LPnTORfQ1ap6CmvoMF6t/vHpAF759Mdq9XnqJZirz6svU6/AXH1ZTb0B8yUOInEQiYNIHETiIBIHkTiIxEEkDiJxEImDSBxE4iASB5E4iMRBJA4icRCJg0gcROIgEgeROIjEQSQOInEQiYNIHETiIBIHkTiIxEEkDiJxEImDSBxE4iASB5E4iMRBJA4icRCJg0gcROIgEgeROIjEQSQOInEQiYNIHETiIBIHkTiIxEEkDiJxEImDSBxE4iASB5E4iMRBJA4icRCJg0gcROIgEgeROIjEQSQOInEQiYNIHETiIBIHkTiIxEEkDiJxEImDSBxE4iASB5E4iMSxpa/rw83R5vB46j3GJI4tnJyenR/9cvFt6m3GI46hLq+ub45+c9NuHeIY5PbP70cvXUy91GjE0dvdj/vNqzKeNfveIY5+vv08fDOMZ4dTLzcWcXQ7+evvi1jGk83UC45FHB3ujh9u3ivj2dQ7jkUc7/m6fuwKQxxLdHJ6f97dhTiW5+n9s/PDRBxL1PH+KY6lenr/fPuXGeJYuJ7vn+JYmt+PaeL4D3E8e3lME8e/iePNY5o4ni08jnhME8fBsuN475gmjoPlxtF1TBPHwULjuDz+0PunOJr18fdPcbTptuL9UxwtOnkYoQxxNOGk/PNEHM24H6kNcey/27HaEMf+G+mFQxwt2ProKo72jdaGOPafOIYShzgicWzh5clu6icbizgGurn++e3lqKmfbCziGOL87PTkjVFTP9lYxNHb4/prGDXlU41JHL1sHo7v8qjJHmlk4uj2/c/b90dN8jg7II733VxfXXaO2v2z7IY43vHf98+uUTt+kJ0RR3L4M/9vAsXRmgFhbO5/3A0Ytasn2DVxvPL6/bNr1C62n4I4/s/Nw/Eb759do0ZffSLi+J+Lv/96+/2za9S4e09HHL9+ZLz3/tk1arydpyWOJ+f3P3r+yHh71EgLT04cv51Mth01wrKzsOw4XpxMth1VvelcLDiOp/fPolGVW87JcuMY/mESR5WtODPLjWOrz5O3R5WtODPiKBhVtuLMiKNgVNmKMyOOglFlK86MOApGla04M+IoGFW24syIo2BU2YozI46CUWUrzow4CkaVrTgz4igYVbbizIijYFTZijMjjoJRZSvOjDgKRpWtODPiKBhVtuLMiKNgVNmKMyOOglFlK86MOApGla04M+IoGFW24syIo2BU2YozI46CUWUrzow4CkaVrTgz4igYVbbizIijYFTZijMjjoJRZSvOjDj6uDtdPzyeP3/d6Obi+uzq5df6jLbyxMTR6dv68eh9Y249JXF0OL3uKEMcLdgmjtuuHxriaMPwOE7O+qQhjgYMjuOy7/fQ7mD5SYgj+tr7GwN3sf0UxJFc9v82yZ2sPwFxBCe9XkXF0Yhhcaz7tyGO/TcojpONOMQRXA1oQxz7b1AcPX4vKo6GDIpjyKeKOPbfoDiGtCGO/fP7nf3w9Z1dHJ1ajaP7zl73sbLZ0TPtXJtx9Lmz172QXu/oqXauxTj63dnr/lP2akfPtXPtxdH3zl72S7DNsO9q2SPNxdH7zl726/P1bh5sAq3F0f/OXnV4e2z2B0drcQy4sxed7M/7fJvknmorjiF39pp/7HP+ka9tmbu24hhyZy/5Z4LfB3yl5P5pKo5Bd/aKf2B81u77xrOm4hh0Z//4nyY8dn6t+Z5rKo5Bd/aP/lHT9em4DzMDTcUx6M7+oT+HfFw3/bLxS1NxDGljyz+kvjl/fFiffuQv9PfIcuOYetk90FQc7uy1morDnb1WU3G4s9dqKg539lpNxeHOXqutONzZS7UVhzt7qcbicGev1Focve7sfm7001wci7+zF2ovjqXf2Qu1GMey7+yF2oxjyXf2Qq3GcbDYO3uhhuPgo8RBJA4icRCJg0gcROIgEgeROIjEQSQOInEQiYNIHETiIBIHkTiIxEEkDiJxEImDSBxE4iASB5E4iMRBJA4icRCJg0gcROIgEgeROIjEQSQOInEQiYNIHETiIBIHkTiIxEEkDiJxEImDSBxE4iASB5E4iMRBJA4icRCJg0gcROIgEgeROIjEQSQOInEQiYNIHETiIBIHkTiIxEEkDiJxEImDSBxE4iASB5E4iMRBJA4icRCJg0gcROIgEgeROIjEQSQOInEQiYNIHETiIBIHkTiIxEEkDiJxEImDSBxE4iASB9Hqy9QbMFdfVp+nXoG5+rxa/fFp6iWYo0//XP0LPwaCpL2UchAAAAAASUVORK5CYII="

/***/ }),

/***/ "4Q3z":
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "5EiB":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWgAAAC0CAYAAAC5brY1AAAG+0lEQVR4nO3d6U4bZxSA4RNkigiLAGW5ufYW+q/91xtob6H/enEogRoCYV9aHRpLSYPHM17wked5JITEIqQZeP35mzPm1eHh4Q8R8UdE/BgRBwHAMp1ExF8R8csgIn6PiJ+dDoAS9r80+SpX0H9bOQOUM1wTZ4CSDtacF4CaBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgqIET0y/39/dxeXkZ19fXcXd31/fDEYPBIDY2NmJzc/PpPVQi0D3x+PgYnz9/jvPz874fim/kA1a+XVxcPAV6b2/vKdpQgS2OHsiV8vHxsThPcHNzE0dHR3F1ddX8hfBCBHrF5cr55OTEdkZLebxOT08dL0oQ6BU3HA7FpqOMdD7jyPewTAK9wvIpe77RXcY596VhmVwNWWGTApMXxfItL4qtrfXrsfr29jYeHh4aj1FeVN3Z2fnu4/OQD5y5150XKEfyXGxtbfXuXDCeQK+wptVzTitkDPpqNFKXx2DcdkZ+LLeH1tfXv/vctDLIucf93LnJj+WDQp6bHPsDD9Ur7LnoxJco9TnOX8v47u7ufvfxkXHHcBo5RZNTIs/F+eufl9cNTJIQVtD9NM8V4SpY9A0quQrvOklzdnZmFY1A95EbMb61qOMxutCYse0qt0JyFS3S/eYvFRYgtzFyr/nri4BdzfK9rAaBhjnKVXOumJumQ/4vpzbMXPMcgYY5yS2JXDW3jW1eC9jf349Pnz41XjikvwQaZtQ0OvecXDFvb28vbMaa1SHQMIMcncvZ5barZq+YRxd+S2AKXUfnctWc89bmz+lCoKGDaUbnclQuV81u4aYrgYaWuo7O5TZGhtl/amFaAg0TTDM6lxcA80KgVTOzEGhokKvmfG2MrqNzbqdnHgQaGrT9N2FG51gEgYYZGZ1jUfxGwZSMzrFoAg1TMDrHSxBo6MDoHC9JoKElo3O8NIGGCYzOsSwCDQ1y1dz0PwthkTxXgwb2mlkmgQYoSqABihJogKIEGqAogQYoSqABihJogKIEGqAogQYoSqABihJogKIEGqAogQYoSqABihJogKIEGqAogYYGNzc34z8JCybQ0OD8/Dw+fvwo1CyFQMMEd3d3cXx8HGdnZ/H4+Nj8xTBHAg0t5Wr66Ogorq6uHDJehEBDB/f39zEcDp/erKZZtIEjDN3lKjr3pXd3d2Nra8sRZCGsoGFKuYI+PT192p/OlTXMm0DDjHIlnXvTuUcN8yTQ0GBnZyfW1ib/meRqOqc8ciQvpz5gHgQaGmxsbMT79+9b7zNnnDPSRvKYB4GGCXIFvbe3F2/evInBoN119dFInhtcmIVAQ0u5mn779u3T5EYbeeEwLyAayWNaAg0d5Go696XfvXsX6+vrrb4xR/I+fPgQFxcX330Omgg0TCHjnJHO1XTbi4hG8uhKoGEGuZrObY/c/mjDSB5dCDTMKC8c5gXEg4MDI3nMlVu9YU42NzefVtK5ldHmBZVGI3ltok4/+c2AOcrY5kq6y0ieCQ/GEWhYgNFIXu5RT6tt4FldAt1Dpgi+tajjkavpnPLoMpI3knHOLRP6TaB7yIWpby36br+uI3kp71wEgV5h42KQN0y4aeI/+WCVExXjjDuG02gzkjfaw276GvrDJtcKyz/ycdMEo0mD/Jp8Ot23SYLb29t4eHhofKDKY9J1a2KS0UhertrzZ399gTDPRb4ok6kORgR6heUf+7hAx5en9l7MZ7zt7e2xn5tVxtgqmUk8VK8wEZhermL9KyuWTaBXXO5nzvtp+qrLOOc2hK0Gls1v4IrLyOzv74t0S6PXfna8qECgeyBjkyvCWW6a6IPRzSXmj6nCRcKeGN008fr167i8vIzr62vz0F+mKjLMo9fRgEpeHR4e/uOMANRjiwOgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYoSaICiBBqgKIEGKEqgAYrKQA+dHIByhhnov5wXgHL+HETErxHxKiJ+ioh95whgqU6eFs4Rv/0LFpk56pxIEY0AAAAASUVORK5CYII="

/***/ }),

/***/ "9iSo":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAYAAAA9zQYyAAAGPklEQVR4nO3d21IiVwCF4T0eUUdFy/LpklfIXXKXF0heIXfzcFxIlOIgipSmVs/sKQJ2A9qnvfi/Koopk4EZ+Ltn9+7Tl16vdxRC+DuE8EsI4ToA6XkIIXwLIfx+EEL4K4TwG18iEnb1o+Gp1tD/smaGifs9YoaR6z2+TTghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFghaFg54OtMx/Pzc5jNZj//vHt7e+Hk5CR7xncE3WIxYD3r8Z7hcBi+fv0azs/Pd/7zCgTdLpsEvOz19TWL+u3tLVxcXKz8911D0A1RiIr25eUlPD09Zc+fMRqNwvHxcfbYZQRdk7IDfs9kMiHolZ+gFHUEvGw6na78bNcQdEmaCBirCPqTFPJ4PM7GsFXRtJyGEoeHh6HT6WTv2e/30/3QKkTQnxDDKnttvBywnhdtOgOyiwj6EwaDQSkxrwsYmyPoD5rP5x/eCCPg6hD0B20TMwHXh6ArQMDNIeiSKeSbmxurv1NKOEwLVggaVggaVggaVggaVggaVggaVggaVggaVggaVggaVjiWIxGLp3ZxgH8+gm6p5YB1dsw6XEGJoFvjIwEv02XBdh1BN6SMgJfpkmC7jqBrUkXAi7rdbjg44OvkE6hI1QFHiljXtGO48R1Bl0wB93q9ygKWxVO7CPn/CLpkVYSstbDCVcA6xYvZjHwE3UIKOJ5kq2fGxpvjk2oBAi4Pn1wD4mUO4oOAy8MnWQOu01Efgq4AATeHoEvGhWaaxfwPrBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rNhfTle3VJvNZj9/nUfXdNZ1nPV8dHTENZ0TZRe07kI1nU6zePW8jcX/P973RA9unZYOm6B1f8DJZJI9yjCfz7OHXk9rbd12+OzsjFuqtVzyQSu68XhcWsjv0Vp/OBxm7xPDRjslHfRoNMoiq/KurYti2I+Pj4yxWyrJoBXWYDDYeoxcljgcQfskF7Ri7vf72Zh5W9rQ29/ff/d36fXqWtOjOkkFvW3M8R7ZcbZik9fX7EicISHw9CQVtMbLm8SsjTY9th3nagZDC4Ae3W4329CMU4BIQzJBKyxtBBZRwFdXV6VtsMUFQ++tMTtr7PZLJmjNLhRReFqrViEOWz46dkd9kthLoDVk0azCxcVFZTFHGo7c3t6y17Dlkgi6aAyrwM7Pz1d+XhUtONrYRDslHbTWmlWvmdvwnthcEkHnDTe0dm7i2AqNp1lLt1PSR9rk7SRxf2/k49AxWEk66Can0Ji+a6ckgs7bUaLpvCbC0g6evJ0sOtsFzUki6E6ns/Kz6OHhITeuKmgB0i74PHkLH+qRRNBFOzMUmPYi1hG1ZluKFqB4LAiak8yQo+hoOR1EpN3SedN7ZdBc+N3dXeEQR2ezoFnJbBReXl4WzjkrNAW37gCmbcWTCbTA5K2Zw4+Frs49lnhfMnsHFIyO2VBceRZPkTo9Pc3++f/oDpB40u0mx0VrQdNRfmheUru7dERdDK2Ihh4KWw8tCApbsw/aGZIXuF5Xv0/P6w6GWqYFjY3Bdnj/222xeBzFpmd5K9Cice9n6c/DWeDtkeQBCYpIa9t1x0hXScOM6+vrwo1V1C/ZI2y0Aab5aU2j1b1zJZ6iVbSRimZ86fV6b6l/9hp+1DEXrXGyZltYK7eXxTGQ8dy/eCmwstfYWiPr9Qm5/awO6o1ha4Yinq2dd3JAEQ0lFi/UyNAiHRZDjnXiRWTiZXXzrJvaQ/vtxDcX54gZMvjj31JYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYUdD3fKUwca+gv/FtwsQ/uq3bH7pfYQjh1xDCFd8sEvSQrZhD+PM/cGeXEE/ovWwAAAAASUVORK5CYII="

/***/ }),

/***/ "BpoA":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getHomeData", function() { return getHomeData; });
/* harmony import */ var _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("sHut");

function getHomeData() {
  console.log("getHomeData");
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].GET_HOME_SCREEN_DATA
  };
} // export function uploadImageData(data) {
//   return { type: atSaga.POST_UPLOAD_IMAGE, data: data };
// }
// export function getGalleryData(data) {
//   return { type: atSaga.GET_GALLERY_DATA, data: data };
// }
// export function getArtistData({ page }) {
//   return { type: atSaga.GET_ARTIST_DATA, page };
// }
// export function getSearchArtistData({ page }) {
//   return { type: atSaga.GET_SEARCH_ARTIST_DATA, page };
// }

/***/ }),

/***/ "HJQg":
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ }),

/***/ "K3nj":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("RfFk");
/* harmony import */ var react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_useragent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("W0sL");
/* harmony import */ var next_useragent__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_useragent__WEBPACK_IMPORTED_MODULE_2__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const AwesomeScroll = props => {
  const {
    data,
    Row,
    ua,
    scrollBar
  } = props; // console.log({ data, Row });
  // console.log({ ua });

  const scrollRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null); // console.log({ AwesomeScroll: data });

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    if (scrollRef.current) {
      const slider = scrollRef.current.children[0];
      let isDown = false;
      let startX;
      let scrollLeft;
      slider.addEventListener("mousedown", e => {
        isDown = true; // slider.classList.add("active");

        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
      });
      slider.addEventListener("mouseleave", () => {
        isDown = false; // slider.classList.remove("active");
      });
      slider.addEventListener("mouseup", () => {
        isDown = false; // slider.classList.remove("active");
      });
      slider.addEventListener("mousemove", e => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = x - startX; //scroll-fast
        // console.log({ x, walk }, (slider.scrollLeft = scrollLeft - walk));

        slider.scrollLeft = scrollLeft - walk;
      });
    }
  }, []);
  return __jsx("div", null, !scrollBar ? __jsx("div", {
    ref: scrollRef,
    className: "awesome-scroll-container"
  }, props.children) : ua && ua.isMobile ? __jsx("div", {
    className: "awesome-scroll-container"
  }, props.children) : __jsx("div", {
    ref: scrollRef,
    className: "awesome-scroll-container"
  }, __jsx(react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1___default.a, null, props.children)));
};

AwesomeScroll.getInitialProps = async ctx => {
  return {
    useragent: ctx.ua.source
  };
};

/* harmony default export */ __webpack_exports__["a"] = (Object(next_useragent__WEBPACK_IMPORTED_MODULE_2__["withUserAgent"])(AwesomeScroll));

/***/ }),

/***/ "Lhaj":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAYAAAA9zQYyAAAId0lEQVR4nO3dW3ITRxjF8bYxBsxVLmB1yRbylrxlA8kW8sbiqOJiMDdzJ3WEuhBo+puRNSOpT/9/VS6qDInHmjPf9HT3dB88fvz4OKX0b0rpt5TSaQLq8yKl9Cil9OdRSumflNIfnERUbLbI8IUq9HMqM0ycHRJmGDk95GzCCYGGFQINKwQaVgg0rBBoWCHQsEKgYYVAwwqBhhUCDSsEGlYINKwQaFgh0LBCoGGFQMMKgYYVAg0rBBpWCDSsEGhYIdCwQqBhhUDDCoGGFQINKwQaVgg0rBBoWCHQsEKgYYVAwwqBhhUCDSsEGlYINKwQaFgh0LBCoGGFQMPKUY2/zOfPn9OXL19Wvo9+165ds/6Uqgr0p0+f0vn5efrw4cPK32GYw8PDdOvWrXT79u2Vv3NQTaDfvn2bXr58ufJ9rOfr16/p1atX6eLiIt2/f38ecCdV/DZqYugkYDy6271588buE60i0PrgVVkwrtevX8+LhZMqAq3bI/hsh6gi0FRnDFVlt12mhxr3bqixPHv2rIneIQZWYIVAwwqBhhUCDSsEGlaq7uXYJfUYfPz4MX379m3+Z4mGlq9evTr/Uz0yR0d85FPi0x1II2oahFCQ1+3+Wh68UKAVbIX85s2bK/8WmyHQAQ3oKIyaGKW5D2PQhZGHmzU/5caNG/NgK+DYHIHuoCArxFPPIck/R1+q2prSyUDRZgj0L1SRNU1128PtuSmjan3nzh27aZ3bQqAX1AxQkHc9PKxqrYvKeRL+lAj0CFVZD3pXrlxZ+X5aVN515Un4arffu3ePar2G5gOtOcHrvjygdq6+jo+PB7V5Vf0VbAVUfw6dg6wLTf92Npvx0DhQ04FWVdYtfghV4ZOTk3mvxLp9yfr3y/9N7v4b8rN1EWimnGYWEup+zQZ6aJgVRLVnx+wz1kWhL/1/1ZPSdxxqghDqYZpsnA0Ns3obHjx4MNkAiC4WtZGHBDWHeqz+cFfNBVpt5r4wK2gPHz6c9zJs44FM7fD88yIK9dnZGW/wBJoKtNqtfQ+AqpSqyru4teuOoIod0UOiQo1uzQQ6V7eImha7XqtCx6BqHR2DLkzdabCq/KmZUWWObtV6SNuXPl/dHfouLP0+bksQjKH8iRnp6yJTgPpu9dumY1ITJMJKUquaCHR04lUF93VJrDyvo0QXKmuW/Mw+0KrM0a359PR0r4eW+2bgsUTaz+wDHa3fpgoYhWVfRM0hXaxRc6o11oGO5k2oKke3832ifvHoWGl2/GAd6OhEa9i5pllsupuUjje6cFvT/QkZyK9PdVEwanufLy9UXlL6XVtjG2hVrVK/s/qcS9Vun+m4Swj0d7aBjibxRMHYZ/mN8S76fUsXcEtsA/3+/fuV76XFrbsUihpEx87eM6aBVqUqVegoEDW4fv168ShLv3NLLCf4Ryd2yll0y9vNTXXh5FWYupoX0QpOrbAMdNSFpfcAx9a13dyU26cp1F3NC/ZuNG5ylIxdoXXxdK2On9/cnmJounRRRhdyK5p7Y2Xs7rq+1ZWm2Gnq4OBg5Xv4jgUfNjSk/5c+4u0h0BuKqjO2j0BviFWN9gtnY0NDRh1rHZmsUXOBHruJ0DdrT912Y6/ar10D0K18JioWBSwadLkMhbVrA9A83zqax3xZpQEUtrswHViJTqzCMPYoXn5LexsjhSm4KEsroLakfOYrFg2elMIwhl8XZZxCNKuuNODSkvK9uWJ556kuXUPGNSnNIkw9F3IrbB8KS7PSVN1qDnV07OzPYhzoqFrVOnKXF07vkmfhtc72E1C1Kp1gBbrUDt1n0YVIX/d33WfcgMJcOsl5O7Wa6JijNUZKv2trrO9R0Umeeg/CsekCLB0vWy7/YB3o6ETn+co1UNs5Ola2WP7B/ikiWstCVa/0kLVPosUmdcFGd6LW2Ae6bx/tfd/iQS8IRBddLcuZbUsT/Tx3795d+V6WN+PZx1DrDhI1NdSkojr/rIlA68RH7UwNJ0fB2YUhxxStStqqZnri+zaEVzWM2qrblDfbjO4a+n1KD7wtK59hMwqzFjePKNRPnjwJgzS1IcegZgYb23draqxUTY++hyhVx6dPn+5kFSI1MfruEnmzTnRrbvBflS1qT6dFv6+qpHoYoko5FvVi5J8XyXeZqOnUuiYbYbnC9Q1/q2K+e/du9L2+M104Q/b6TkubG0VdkGh48/qhoVbo1AxQ8E5OTubt100fxjTJqG+ruWWEebimH5MVar22NKTLLg8/60ttcX3pDZEhc5DztE+1y9fdPkIhns1mhHmg5vt98lvZqsJD28sK5fLonf770vt80Shfn7xPIW3m4ZoPdFp0g6nSahj8MgFUxR1z/br8xjiTjtZHoBdyO1Xt213uo01V3gyB/kWu1npg2+acaf3Mvl1j0Y9Ad1B1zP3VqtgK9xQDLfmtGnULMow9Dj7FQN7PUF9qguTutk0e9PJOVuq1oI08PgI9kIKoqp3nUCjUWoVJ68yVluZKS2uE5N23qMTT4tO9pNwXjf3CozSsEGhYIdCwQqBhhUDDStW9HHrvDlhWRYVmXgOGqiIprD0xHbfPtopA9+00hcuZYoeuXasiJfrQWfJqXBqOj9b9q1U1l6cm8uiVp/Pz8+r3Sdkl3ekUZNd1Paq633Rtn4b1uM8/qbIBtY3t01AnnrRghUDDCoGGFQINKwQaVgg0rBBoWCHQsEKgYYVAwwqBhhUCDSsEGlYINKwQaFgh0LBCoGGFQMMKgYYVAg0rBBpWCDSsEGhYIdCwQqBhhUDDCoGGFQINKwQaVgg0rBBoWCHQsEKgYYVAwwqBhhUCDSsEGlYINKwQaFgh0LBCoGGFQMOKAn3GKYWJMwX6EWcTJv7TDvB/pZQOUkq/p5RmnFlU6MW8MKf09/8mzn7LxDoZywAAAABJRU5ErkJggg=="

/***/ }),

/***/ "Osoz":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/router-context.js");

/***/ }),

/***/ "RNiq":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: ./store/actions/saga/index.js
var saga = __webpack_require__("BpoA");

// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__("h74D");

// CONCATENATED MODULE: ./utils/Loading/index.js
var __jsx = external_react_default.a.createElement;

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // import { CoffeeLoading } from "react-loadingg";

function Loading(Component) {
  return function LoadingComponent(_ref) {
    let {
      isLoading
    } = _ref,
        props = _objectWithoutProperties(_ref, ["isLoading"]);

    if (!isLoading) return __jsx(Component, props);else return __jsx("div", {
      style: {
        display: "flex",
        width: "100vw",
        height: "100vh",
        justifyContent: "center",
        alignItem: "center"
      }
    });
  };
}

/* harmony default export */ var utils_Loading = (Loading);
// EXTERNAL MODULE: ./website/components/Header/index/index.js + 3 modules
var Header_index = __webpack_require__("vfdN");

// EXTERNAL MODULE: ./components/AwesomeScroll/index.js
var AwesomeScroll = __webpack_require__("K3nj");

// EXTERNAL MODULE: ./public/assets/images/placeholder/sliderplaceholder.png
var sliderplaceholder = __webpack_require__("3OaI");
var sliderplaceholder_default = /*#__PURE__*/__webpack_require__.n(sliderplaceholder);

// EXTERNAL MODULE: ./website/index.js + 24 modules
var website = __webpack_require__("akFz");

// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__("YFqc");
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);

// EXTERNAL MODULE: ./website/components/Ui/Icons/PlayIcon/index.js
var PlayIcon = __webpack_require__("gfwP");

// EXTERNAL MODULE: ./components/LazyImage/index.js
var LazyImage = __webpack_require__("q7pC");

// CONCATENATED MODULE: ./website/components/cards/cardElements/SliderCard/index.js
var SliderCard_jsx = external_react_default.a.createElement;





const SliderCard = props => {
  const {
    titleTop,
    titleMiddle,
    titleBottom,
    images
  } = props.data;
  const optionRef = Object(external_react_["useRef"])(null);

  const imageOnload = () => {
    optionRef.current.style.display = "flex";
  }; // console.log({SliderCardProps:props});
  // console.log({ SliderCardProps: props });
  // useEffect(() => {
  //   console.log({ liRef: liRef.current });
  // }, []);


  return SliderCard_jsx("li", {
    className: "slider-card-container col-lg-5 col-md-7 col-9 px-0"
  }, SliderCard_jsx("div", {
    className: "slider-card-wrapper"
  }, SliderCard_jsx("div", {
    className: "slider-card-title"
  }, SliderCard_jsx("h4", null, titleTop), SliderCard_jsx("span", null, titleMiddle), SliderCard_jsx("h3", null, titleBottom)), SliderCard_jsx("div", {
    className: "slider-card-image"
  }, SliderCard_jsx("picture", null, SliderCard_jsx("source", {
    media: "(max-width: 375px)",
    srcSet: images.phone
  }), SliderCard_jsx(LazyImage["a" /* default */], {
    imageOnload: imageOnload,
    id: "myImage",
    className: "noSelect noEvent",
    defaultImage: props.placeholder,
    src: images.phone,
    alt: titleTop
  })), SliderCard_jsx("div", {
    ref: optionRef,
    className: "card-options"
  }, SliderCard_jsx("a", null, SliderCard_jsx(PlayIcon["a" /* default */] // className={classNameNew}
  , {
    style: {
      fontSize: "1em",
      margin: "0.7em"
    }
  }))))));
};

/* harmony default export */ var cardElements_SliderCard = (SliderCard);
// CONCATENATED MODULE: ./website/components/containers/SliderCardContainer/index.js
var SliderCardContainer_jsx = external_react_default.a.createElement;






const SliderCardContainer = props => {
  const {
    sliders
  } = props; // console.log({ sliders });

  const newData = website["a" /* default */].utils.convert.sliderCard(sliders);
  return SliderCardContainer_jsx("section", {
    className: "row-container row-main-wrapper"
  }, " ", SliderCardContainer_jsx(AwesomeScroll["a" /* default */], {
    scrollBar: true
  }, SliderCardContainer_jsx("ul", {
    onDragStart: e => e.preventDefault(),
    className: "awesome-scroll-wrapper row mx-0"
  }, newData.map((data, index) => {
    return SliderCardContainer_jsx(cardElements_SliderCard, {
      key: "single-" + index,
      data: data,
      placeholder: sliderplaceholder_default.a,
      parentClass: " col-lg-2 col-md-3 col-5  px-0"
    });
  }))));
};

/* harmony default export */ var containers_SliderCardContainer = (SliderCardContainer);
// EXTERNAL MODULE: ./website/values/theme/themeColor.js
var theme_themeColor = __webpack_require__("vEqB");

// EXTERNAL MODULE: ./public/assets/images/placeholder/singleplaceholder.png
var singleplaceholder = __webpack_require__("9iSo");
var singleplaceholder_default = /*#__PURE__*/__webpack_require__.n(singleplaceholder);

// EXTERNAL MODULE: ./website/components/Ui/Icons/HeartIcon/index.js
var HeartIcon = __webpack_require__("oviB");

// EXTERNAL MODULE: ./website/components/Ui/Icons/DotIcon/index.js
var DotIcon = __webpack_require__("rYez");

// CONCATENATED MODULE: ./website/components/cards/cardElements/MusicCardDouble/index.js
var MusicCardDouble_jsx = external_react_default.a.createElement;






const MusicCardDouble = props => {
  const {
    data,
    themeColor,
    directionWeb,
    placeholder,
    dotIconClick,
    dotModalInfo
  } = props; // console.log({ MusicCardDoubledata: data });

  const preventDragHandler = e => e.preventDefault();

  const optionRef = Object(external_react_["useRef"])(null);

  const imageOnload = () => {
    optionRef.current.style.display = "flex";
  };

  return MusicCardDouble_jsx("li", {
    onDragStart: preventDragHandler,
    className: "disColumn doubleColum-wrapper  col-lg-2 col-md-3 col-5 px-0 "
  }, data && data.map((info, index) => {
    // console.log({ info });
    return MusicCardDouble_jsx("div", {
      key: "musicCard" + index,
      className: "play-Card-container  play-Card-wrapper"
    }, MusicCardDouble_jsx("div", {
      className: "imageCard-top"
    }, MusicCardDouble_jsx("picture", null, MusicCardDouble_jsx("source", {
      media: "(max-width: 375px)",
      srcSet: info.images.phone
    }), MusicCardDouble_jsx(LazyImage["a" /* default */], {
      imageOnload: imageOnload,
      src: info.images.web,
      defaultImage: placeholder,
      alt: "placeholder"
    })), MusicCardDouble_jsx("div", {
      ref: optionRef,
      className: "card-options"
    }, MusicCardDouble_jsx("a", null, MusicCardDouble_jsx(PlayIcon["a" /* default */] // className={classNameNew}
    , {
      style: {
        fontSize: "0.6em"
      }
    })), MusicCardDouble_jsx(HeartIcon["a" /* default */], {
      style: {
        fontSize: "0.6em"
      }
    }), MusicCardDouble_jsx(DotIcon["a" /* default */], {
      click: dotIconClick,
      modalInfo: dotModalInfo
    }))), MusicCardDouble_jsx("div", {
      className: "descriptionCard-bottom noSelect"
    }, MusicCardDouble_jsx("h4", {
      className: "play-Card-title"
    }, info.titleTop), MusicCardDouble_jsx("span", {
      className: "play-Card-subTitle" // style={colorStyle}

    }, info.titleMiddle), MusicCardDouble_jsx("h3", {
      className: "play-Card-text" //  style={colorStyle}

    }, info.titleBottom[0], MusicCardDouble_jsx("i", {
      className: "fas fa-circle"
    }), info.titleBottom[1])));
  }));
};

/* harmony default export */ var cardElements_MusicCardDouble = (MusicCardDouble); // return <MusicCard data={info} directionWeb={directionWeb} themeColor={themeColor} key={index + "asdasd"} />;
//         })}
//     </div>
//   );
// };
// export default MusicCardDouble;
// CONCATENATED MODULE: ./website/components/containers/SingleContainer/index.js
var SingleContainer_jsx = external_react_default.a.createElement;








const SingleContainer = props => {
  const {
    0: state,
    1: setState
  } = Object(external_react_["useState"])({
    dotModal: false
  });
  const {
    singles
  } = props; // console.log({ singles });

  const newData = website["a" /* default */].utils.convert.singleCard(singles); // console.log({ SingleContainer: newData });

  const onShowNodal = () => {};

  return SingleContainer_jsx("section", {
    className: "row-container row-main-wrapper"
  }, SingleContainer_jsx("div", {
    className: "top-card-head"
  }, " ", SingleContainer_jsx("div", {
    className: "cardHead-headline"
  }, SingleContainer_jsx("h3", null, website["a" /* default */].values.strings.SINGLES)), SingleContainer_jsx("div", {
    className: "cardHead-change-Location "
  }, SingleContainer_jsx(link_default.a, {
    href: "#",
    as: "#"
  }, SingleContainer_jsx("a", {
    className: "pointer"
  }, SingleContainer_jsx("span", {
    style: {
      color: theme_themeColor["a" /* default */].mainColor
    }
  }, website["a" /* default */].values.strings.VIEW_ALL))))), SingleContainer_jsx(AwesomeScroll["a" /* default */], {
    scrollBar: true
  }, SingleContainer_jsx("ul", {
    onDragStart: e => e.preventDefault(),
    className: "awesome-scroll-wrapper row mx-0"
  }, newData.map((data, index) => {
    return SingleContainer_jsx(cardElements_MusicCardDouble, {
      key: "single-" + index,
      data: data,
      click: onShowNodal,
      state: state,
      placeholder: singleplaceholder_default.a
    });
  }))));
};

/* harmony default export */ var containers_SingleContainer = (SingleContainer);
// EXTERNAL MODULE: ./public/assets/images/placeholder/albumPlaceholder.png
var albumPlaceholder = __webpack_require__("Lhaj");
var albumPlaceholder_default = /*#__PURE__*/__webpack_require__.n(albumPlaceholder);

// CONCATENATED MODULE: ./website/components/containers/AlbumContainer/index.js
var AlbumContainer_jsx = external_react_default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








const modalData = [{
  icon: "far fa-stream",
  title: "Add To Quque"
}, {
  icon: "far fa-album",
  title: "Go To Album"
}, {
  icon: "fal fa-plus",
  title: "Add To Playlist",
  child: []
}, {
  icon: "fas fa-share",
  title: "Share",
  child: []
}];

const AlbumContainer = props => {
  const {
    albums
  } = props;
  const {
    0: state,
    1: setState
  } = Object(external_react_["useState"])({
    modal: {
      show: false,
      data: modalData
    }
  });

  const toggleDotIconClick = () => {
    // console.log("omad");
    setState(prev => _objectSpread({}, prev, {
      modal: {
        show: !prev.modal.show
      }
    }));
  }; // console.log({ albums });


  const newData = website["a" /* default */].utils.convert.albumCard(albums, 2); // console.log({ newData });

  return AlbumContainer_jsx("section", {
    className: "row-container row-main-wrapper"
  }, AlbumContainer_jsx("div", {
    className: "top-card-head"
  }, " ", AlbumContainer_jsx("div", {
    className: "cardHead-headline"
  }, AlbumContainer_jsx("h3", null, website["a" /* default */].values.strings.ALBUMS)), AlbumContainer_jsx("div", {
    className: "cardHead-change-Location "
  }, AlbumContainer_jsx(link_default.a, {
    href: "#",
    as: "#"
  }, AlbumContainer_jsx("a", {
    className: "pointer"
  }, AlbumContainer_jsx("span", {
    style: {
      color: theme_themeColor["a" /* default */].mainColor
    }
  }, website["a" /* default */].values.strings.VIEW_ALL))))), AlbumContainer_jsx(AwesomeScroll["a" /* default */], {
    scrollBar: true
  }, AlbumContainer_jsx("ul", {
    onDragStart: e => e.preventDefault(),
    className: "awesome-scroll-wrapper row mx-0"
  }, newData.map((data, index) => {
    return AlbumContainer_jsx(cardElements_MusicCardDouble, {
      key: "AlbumCard-" + index,
      data: data,
      placeholder: albumPlaceholder_default.a,
      dotIconClick: toggleDotIconClick,
      dotModalInfo: state.modal,
      parentClass: " col-lg-2 col-md-3 col-5  px-0"
    });
  }))));
};

/* harmony default export */ var containers_AlbumContainer = (AlbumContainer);
// EXTERNAL MODULE: ./public/assets/images/placeholder/videoplaceholder.png
var videoplaceholder = __webpack_require__("5EiB");
var videoplaceholder_default = /*#__PURE__*/__webpack_require__.n(videoplaceholder);

// CONCATENATED MODULE: ./website/components/cards/cardElements/MoodCard/index.js
var MoodCard_jsx = external_react_default.a.createElement;




const MoodCard = props => {
  const {
    title,
    images,
    alt
  } = props.data;

  const imageOnload = () => {// optionRef.current.style.display = "flex";
  };

  return MoodCard_jsx("li", {
    className: "card-Mood-container noSelect  col-lg-2 col-md-3 col-5 px-0"
  }, MoodCard_jsx("div", {
    className: " card-Mood-wrapper"
  }, MoodCard_jsx("div", {
    className: "imageCard noSelect noEvent"
  }, MoodCard_jsx("div", {
    className: "descriptionCard-middle"
  }, MoodCard_jsx("span", {
    className: "card-mood-title"
  }, title)), MoodCard_jsx("picture", null, MoodCard_jsx("source", {
    media: "(max-width: 375px)",
    srcSet: images.phone
  }), MoodCard_jsx(LazyImage["a" /* default */], {
    imageOnload: imageOnload,
    defaultImage: videoplaceholder_default.a,
    src: images.web,
    alt: "placeholder"
  })))));
};

/* harmony default export */ var cardElements_MoodCard = (MoodCard);
// CONCATENATED MODULE: ./website/components/containers/MoodCardContainer/index.js
var MoodCardContainer_jsx = external_react_default.a.createElement;







const MoodCardContainer = props => {
  const {
    moods
  } = props;
  const newData = website["a" /* default */].utils.convert.moodCard(moods); // console.log({ newData });

  return MoodCardContainer_jsx("section", {
    className: "row-container row-main-wrapper"
  }, MoodCardContainer_jsx("div", {
    className: "top-card-head"
  }, " ", MoodCardContainer_jsx("div", {
    className: "cardHead-headline"
  }, MoodCardContainer_jsx("h3", null, website["a" /* default */].values.strings.MOOD)), MoodCardContainer_jsx("div", {
    className: "cardHead-change-Location "
  }, MoodCardContainer_jsx(link_default.a, {
    href: "#",
    as: "#"
  }, MoodCardContainer_jsx("a", {
    className: "pointer"
  }, MoodCardContainer_jsx("span", {
    style: {
      color: theme_themeColor["a" /* default */].mainColor
    }
  }, website["a" /* default */].values.strings.VIEW_ALL))))), MoodCardContainer_jsx(AwesomeScroll["a" /* default */], {
    scrollBar: true
  }, MoodCardContainer_jsx("ul", {
    onDragStart: e => e.preventDefault(),
    className: "awesome-scroll-wrapper row mx-0"
  }, newData.map((data, index) => {
    return MoodCardContainer_jsx(cardElements_MoodCard, {
      key: "MoodCard-" + index,
      data: data
    });
  }))));
};

/* harmony default export */ var containers_MoodCardContainer = (MoodCardContainer);
// EXTERNAL MODULE: ./public/assets/images/placeholder/playlistplaceholder.png
var playlistplaceholder = __webpack_require__("TYFc");
var playlistplaceholder_default = /*#__PURE__*/__webpack_require__.n(playlistplaceholder);

// EXTERNAL MODULE: ./website/components/cards/cardElements/MusicCard/index.js
var MusicCard = __webpack_require__("VkSw");

// CONCATENATED MODULE: ./website/components/containers/PlaylistContainer/index.js
var PlaylistContainer_jsx = external_react_default.a.createElement;








const PlaylistContainer = props => {
  const {
    playlists
  } = props; // console.log({ singles });

  const newData = website["a" /* default */].utils.convert.playlistCard(playlists);
  return PlaylistContainer_jsx("section", {
    className: "row-container row-main-wrapper"
  }, PlaylistContainer_jsx("div", {
    className: "top-card-head"
  }, " ", PlaylistContainer_jsx("div", {
    className: "cardHead-headline"
  }, PlaylistContainer_jsx("h3", null, website["a" /* default */].values.strings.PLAY_LISTS)), PlaylistContainer_jsx("div", {
    className: "cardHead-change-Location "
  }, PlaylistContainer_jsx(link_default.a, {
    href: "#",
    as: "#"
  }, PlaylistContainer_jsx("a", {
    className: "pointer"
  }, PlaylistContainer_jsx("span", {
    style: {
      color: theme_themeColor["a" /* default */].mainColor
    }
  }, website["a" /* default */].values.strings.VIEW_ALL))))), PlaylistContainer_jsx(AwesomeScroll["a" /* default */], {
    scrollBar: true
  }, PlaylistContainer_jsx("ul", {
    onDragStart: e => e.preventDefault(),
    className: "awesome-scroll-wrapper row mx-0"
  }, newData.map((data, index) => {
    return PlaylistContainer_jsx(MusicCard["a" /* default */], {
      key: "PlaylistCard-" + index,
      data: data,
      placeholder: playlistplaceholder_default.a,
      parentClass: " col-lg-2 col-md-3 col-5  px-0"
    });
  }))));
};

/* harmony default export */ var containers_PlaylistContainer = (PlaylistContainer);
// CONCATENATED MODULE: ./website/components/cards/cardElements/VideoCard/index.js
var VideoCard_jsx = external_react_default.a.createElement;

 // import "./index.scss";





const VideoCard = props => {
  // console.log({ VideoCardprops: props });
  const {
    titleTop,
    titleMiddle,
    titleBottom,
    images
  } = props.data;
  const optionRef = Object(external_react_["useRef"])(null);

  const imageOnload = () => {
    optionRef.current.style.display = "flex";
  }; // const imageRef = useRef(null);
  // let style = {
  //   marginRight: "1.5em",
  // };
  // let classNameNew = "";
  // let titleStyle = {
  //   marginLeft: "0",
  // };
  // if (directionWeb === "rtl") {
  //   style = { marginLeft: "1.5em" };
  //   titleStyle = { marginRight: "0" };
  //   classNameNew = "Rotate180";
  // }
  // useEffect(() => {
  //   imageRef.current.draggable = false;
  // }, []);


  const preventDragHandler = e => e.preventDefault();

  return VideoCard_jsx("li", {
    onDragStart: preventDragHandler,
    className: " play-Card-container videoCard col-lg-3 col-md-5 col-8  px-0 "
  }, VideoCard_jsx("div", {
    className: " play-Card-wrapper "
  }, VideoCard_jsx("div", {
    className: "imageCard-top"
  }, VideoCard_jsx("picture", null, VideoCard_jsx("source", {
    media: "(max-width: 375px)",
    srcSet: images ? images.phone : ""
  }), VideoCard_jsx(LazyImage["a" /* default */], {
    imageOnload: imageOnload,
    src: images.web,
    defaultImage: videoplaceholder_default.a,
    alt: "placeholder"
  })), VideoCard_jsx("div", {
    ref: optionRef,
    className: "video-card-options centerAll"
  }, VideoCard_jsx(PlayIcon["a" /* default */] //  className={classNameNew}
  , {
    style: {
      fontSize: "1em"
    }
  }))), VideoCard_jsx("div", {
    // style={titleStyle}
    className: "descriptionCard-bottom noSelect"
  }, VideoCard_jsx("h4", {
    className: "play-Card-title"
  }, titleTop), VideoCard_jsx("span", {
    className: "play-Card-subTitle" // style={{ color: themeColor ? themeColor.accentColor : "" }}

  }, titleMiddle), VideoCard_jsx("h3", {
    className: "play-Card-text" //  style={{ color: themeColor ? themeColor.accentColor : "" }}

  }, titleBottom[0], VideoCard_jsx("i", {
    className: "fas fa-circle"
  }), titleBottom[1]))));
};

/* harmony default export */ var cardElements_VideoCard = (VideoCard);
// CONCATENATED MODULE: ./website/components/containers/VideoCardContainer/index.js
var VideoCardContainer_jsx = external_react_default.a.createElement;







const VideoCardContainer = props => {
  const {
    musicVideos
  } = props; // console.log({ musicVideos });

  const newData = website["a" /* default */].utils.convert.videoCard(musicVideos); // console.log({ newData });

  return VideoCardContainer_jsx("section", {
    className: "row-container row-main-wrapper"
  }, VideoCardContainer_jsx("div", {
    className: "top-card-head"
  }, " ", VideoCardContainer_jsx("div", {
    className: "cardHead-headline"
  }, VideoCardContainer_jsx("h3", null, website["a" /* default */].values.strings.VIDEOS)), VideoCardContainer_jsx("div", {
    className: "cardHead-change-Location "
  }, VideoCardContainer_jsx(link_default.a, {
    href: "#",
    as: "#"
  }, VideoCardContainer_jsx("a", {
    className: "pointer"
  }, VideoCardContainer_jsx("span", {
    style: {
      color: theme_themeColor["a" /* default */].mainColor
    }
  }, website["a" /* default */].values.strings.VIEW_ALL))))), VideoCardContainer_jsx(AwesomeScroll["a" /* default */], {
    scrollBar: true
  }, VideoCardContainer_jsx("ul", {
    onDragStart: e => e.preventDefault(),
    className: "awesome-scroll-wrapper row mx-0"
  }, newData.map((data, index) => {
    return VideoCardContainer_jsx(cardElements_VideoCard, {
      key: "VideoCard-" + index,
      data: data
    });
  }))));
};

/* harmony default export */ var containers_VideoCardContainer = (VideoCardContainer);
// CONCATENATED MODULE: ./website/components/containers/PlaylistForAutuminContainer/index.js
var PlaylistForAutuminContainer_jsx = external_react_default.a.createElement;

function PlaylistForAutuminContainer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function PlaylistForAutuminContainer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { PlaylistForAutuminContainer_ownKeys(Object(source), true).forEach(function (key) { PlaylistForAutuminContainer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { PlaylistForAutuminContainer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function PlaylistForAutuminContainer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








const PlaylistForAutuminContainer_modalData = [{
  icon: "far fa-stream",
  title: "Add To Quque"
}, {
  icon: "far fa-album",
  title: "Go To Album"
}, {
  icon: "fal fa-plus",
  title: "Add To Playlist",
  child: []
}, {
  icon: "fas fa-share",
  title: "Share",
  child: []
}];

const PlaylistForAutuminContainer = props => {
  const {
    PlaylistForAutumins
  } = props;
  const {
    0: state,
    1: setState
  } = Object(external_react_["useState"])({
    modal: {
      show: false,
      data: PlaylistForAutuminContainer_modalData
    }
  });

  const toggleDotIconClick = () => {
    // console.log("omad");
    setState(prev => PlaylistForAutuminContainer_objectSpread({}, prev, {
      modal: {
        show: !prev.modal.show
      }
    }));
  }; // console.log({ singles });
  // console.log({ state });


  const newData = website["a" /* default */].utils.convert.PlaylistForAutuminCard(PlaylistForAutumins);
  return PlaylistForAutuminContainer_jsx("section", {
    className: "row-container row-main-wrapper"
  }, PlaylistForAutuminContainer_jsx("div", {
    className: "top-card-head"
  }, " ", PlaylistForAutuminContainer_jsx("div", {
    className: "cardHead-headline"
  }, PlaylistForAutuminContainer_jsx("h3", null, website["a" /* default */].values.strings.PLAYLIST_FOR_AUTUMN)), PlaylistForAutuminContainer_jsx("div", {
    className: "cardHead-change-Location"
  }, PlaylistForAutuminContainer_jsx(link_default.a, {
    href: "#",
    as: "#"
  }, PlaylistForAutuminContainer_jsx("a", {
    className: "pointer"
  }, PlaylistForAutuminContainer_jsx("span", {
    style: {
      color: theme_themeColor["a" /* default */].mainColor
    }
  }, website["a" /* default */].values.strings.VIEW_ALL))))), PlaylistForAutuminContainer_jsx(AwesomeScroll["a" /* default */], {
    scrollBar: true
  }, PlaylistForAutuminContainer_jsx("ul", {
    onDragStart: e => e.preventDefault(),
    className: "awesome-scroll-wrapper row mx-0"
  }, newData.map((data, index) => {
    return PlaylistForAutuminContainer_jsx(MusicCard["a" /* default */], {
      key: "PlaylistCard-" + index,
      data: data,
      placeholder: playlistplaceholder_default.a,
      dotIconClick: toggleDotIconClick,
      dotModalInfo: state.modal,
      parentClass: " col-lg-2 col-md-3 col-5  px-0"
    });
  }))));
};

/* harmony default export */ var containers_PlaylistForAutuminContainer = (PlaylistForAutuminContainer);
// CONCATENATED MODULE: ./website/components/containers/ComingSoonContainer/index.js
var ComingSoonContainer_jsx = external_react_default.a.createElement;








const ComingSoonContainer = props => {
  const {
    comingSoon
  } = props; // console.log({ comingSoonContainer:comingSoon });

  const newData = website["a" /* default */].utils.convert.comingSoonCard(comingSoon); // console.log({ newDataContainer: newData });

  return ComingSoonContainer_jsx("section", {
    className: "row-container row-main-wrapper"
  }, ComingSoonContainer_jsx("div", {
    className: "top-card-head"
  }, ComingSoonContainer_jsx("div", {
    className: "cardHead-headline"
  }, ComingSoonContainer_jsx("h3", null, website["a" /* default */].values.strings.COMMING_SOON)), ComingSoonContainer_jsx("div", {
    className: "cardHead-change-Location"
  }, ComingSoonContainer_jsx(link_default.a, {
    href: "#",
    as: "#"
  }, ComingSoonContainer_jsx("a", {
    className: "pointer"
  }, ComingSoonContainer_jsx("span", {
    style: {
      color: theme_themeColor["a" /* default */].mainColor
    }
  }, website["a" /* default */].values.strings.VIEW_ALL))))), ComingSoonContainer_jsx(AwesomeScroll["a" /* default */], {
    scrollBar: true
  }, ComingSoonContainer_jsx("ul", {
    onDragStart: e => e.preventDefault(),
    className: "awesome-scroll-wrapper row mx-0"
  }, newData.map((data, index) => {
    return ComingSoonContainer_jsx(MusicCard["a" /* default */], {
      key: "MusicCard-" + index,
      data: data,
      placeholder: playlistplaceholder_default.a,
      parentClass: " col-lg-2 col-md-3 col-5  px-0"
    });
  }))));
};

/* harmony default export */ var containers_ComingSoonContainer = (ComingSoonContainer);
// CONCATENATED MODULE: ./website/screen/HomeScreen/index.js
var HomeScreen_jsx = external_react_default.a.createElement;













 // import dynamic from "next/dynamic";
// const DesktopContent = dynamic(() => import("./desktop-content"));
// const MobileContent = dynamic(() => import("./mobile-content"));

const HomeScreen = props => {
  const {
    0: isLoading,
    1: setLoading
  } = Object(external_react_["useState"])(true); //Get Home Data

  const {
    0: direction,
    1: setDirection
  } = Object(external_react_["useState"])("ltr"); // Connect to Store

  const home = Object(external_react_redux_["useSelector"])(state => {
    return state.home.homeData;
  });
  Object(external_react_["useEffect"])(() => {
    console.log({
      home
    });
    if (home.sliders) setLoading(false);
  }, [home]);

  const main = () => HomeScreen_jsx("main", {
    className: "main-screen-wrapper padding-top"
  }, HomeScreen_jsx(containers_SliderCardContainer, {
    sliders: home.sliders
  }), HomeScreen_jsx(containers_MoodCardContainer, {
    moods: home.moods
  }), HomeScreen_jsx(containers_AlbumContainer, {
    albums: home.albums
  }), HomeScreen_jsx(containers_SingleContainer, {
    singles: home.singles
  }), HomeScreen_jsx(containers_PlaylistContainer, {
    playlists: home.playlists
  }), HomeScreen_jsx(containers_VideoCardContainer, {
    musicVideos: home.musicVideos
  }), HomeScreen_jsx(containers_PlaylistForAutuminContainer, {
    PlaylistForAutumins: home.suggestedPlaylists
  }), HomeScreen_jsx(containers_ComingSoonContainer, {
    comingSoon: home.comingSoon
  }));

  return isLoading ? "" : main();
};

/* harmony default export */ var screen_HomeScreen = (HomeScreen);
// CONCATENATED MODULE: ./pages/index.js
var pages_jsx = external_react_default.a.createElement;


 // import { useSelector, useDispatch } from "react-redux";



const Home = props => {
  // const dispatch = useDispatch();
  // const getHomeData = () => dispatch(sagaActions.getHomeData());
  Object(external_react_["useEffect"])(() => {
    props.dispatch(saga["getHomeData"]());
    localStorage.setItem("DIR", "ltr");
  }, []);
  return pages_jsx(screen_HomeScreen, props);
};

Home.getInitialProps = async props => {
  const {
    store,
    isServer
  } = props.ctx; // store.dispatch(sagaActions.getHomeData());

  return {
    isServer
  };
};

/* harmony default export */ var pages = __webpack_exports__["default"] = (Object(external_react_redux_["connect"])()(Home));

/***/ }),

/***/ "RfFk":
/***/ (function(module, exports) {

module.exports = require("react-perfect-scrollbar");

/***/ }),

/***/ "TYFc":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAYAAAA9zQYyAAAGaklEQVR4nO3d3VIiRwCG4V7/LYVirVLL8taSW8hZcpYbSG4hZ3txHEgQ//BfUx/JbMZFZoahgelv3qfK0sJEXHhph6an+dLv93dCCH+GEH4KIRwFID2XIYRvIYRft0IIf4QQfuFORMK+/tfwvUbovxmZYWK4QcwwcrTBvQknBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rBA0rW9ydyHt+fg4PDw/fL9nY2Aj7+/uTzykgaIT7+/vw+Pg4+fz29jZ1g1xfX4fDw8PQ6XSmvtc0BN1SZRHn6fuK+v39PXS73anvNwlBt8g8EX/m5uYm7O7uTj6aiqDNLRrxj+7u7ggaqxU74jz9zCYjaBPLjDglBJ04Hdfe3t5GiTibotMhhb4eDAZT/03TEXTCRqPR5Jh2EfmI9Tmj0T5FBJ0ojcx1Y54VsQOCTtR4PJ7rF3eOOI+gE6SXp19eXkp/8bZEnEfQCSp6AtjGiPOSCHo4HDZ+/rOura2tcHx8HG3xz9nZ2dRlbZLEEirXmEWHDjqEQBysh4YVgoYVgoaVJILWEydnm5ub1v++VUqiFM0CuD5xUszuD9hVSuKW1JRWk9fgojk4hoYVgoYVgoYVgoYVnl7jg/ypXCkiaMx1PmLTd1Ai6Jaqe1Jt05ekEnSLxDgzXFuCNRlBm4u5vUGv12v8q5oEbSj2Hh2KWHvapXAGDEGb6ff70TaayU7lOjg4mPpeUxG0mUVjzp+PmMqe0HkEjeQjziPolnKKOI+gW8Q14jyCNteGiPMI2lDbIs5jo5k1i73RzPn5+dRlbcJGM2vGRjNxsR4aVggaVggaVthopgHYaCYeNppZMzaaiYuNZmCFY2hYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYIWhYYaOZArE3gcHysdFMATaBSQ9DD6wQNKwQNKyw0UwJNoFJCxvNFFh0Exj9zk9PT5M38tHXRW/ok+07srOzwx4kC2Cjmcjqvkeg/p9M9u9t66bli2APqgg0vaeAb29vo7xHoH6Gfp4+RqPR5H0C9ZbEbBlWjltoAQrv7u4uWsiz6Dr0kYWN2Qi6Jh0iaPTU6LwqilqjdgpvUbwuBF3Dzc1NuL6+Xst1Z38V8DmCnpNG5TpB6Ynd9vb21OXy+vq60pHeGUHPYZ6YFXD29mr6qDJTocOYbIaEwOsh6IoUWZWYFa+evNU5zs3i73a738Pm8GI+BF2BRkuNzkU0Avd6vWhP2LK49eC4vLxk1V9FzNhXUDYtp2Pj09PTpcw+6GefnJxMwkY5RugSZbMKivjo6Gjq8tg0+uul+HXNrqSCEbpE0ckFeuVOoa1Kp9NhDroEQZcoOnZVzKteZ7GO60wJt0yJWdNnGp3XsWAqmw7E5wi6pnWuk2aN9mwEDSsEXVPRsbXzdTcdQZfQGSSf0XSeFimtmmKeNfMya61ImxB0iaJI9ILLKkdLPYj0quEse3t7M77THgRdougUqCywWTMhMem69KJK0QOI2Q+CrqToLBEFdnFx8eGcwNj0gBkMBoWvWGoKseivSVsQdAVaR1EUi0ZPBacRtGjNRx06TtcDpmhkzhZGIYQv/X7/nduhnIJStGXBZi98lD0IimQn3Y7H40qHM4qZxUv/Iug56E9+2TLSPL2aqLgVtr6eFbii1Vkr2sNDIReNxj9SyIzO/2O13RyyUbBq1Ap1mVN7xDyNoOekiDTaas/qZW5dUEZntWj1HT7iSWENmlFY1oL+MtmCf2L+HCN0TXryp4X9mq67urpa+gssuj6Nyjz5K8aTwkgUdrYRTEwakRUxIVfDCB1JdlJrti9dtiVBnePs/EaN7Gc3H26tyHRokB9R81NyRbQIqmgzGlRD0EumEXZdZ7e0EbMcsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsELQsKKgh9ylMDFU0N+4N2HiL72t2296R9kQws8hhK/cs0jQ5WRgDuH3fwBA9JyaNfJs+QAAAABJRU5ErkJggg=="

/***/ }),

/***/ "TqRt":
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "VkSw":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Ui_Icons_PlayIcon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("gfwP");
/* harmony import */ var _Ui_Icons_HeartIcon__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("oviB");
/* harmony import */ var _Ui_Icons_DotIcon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("rYez");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("YFqc");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_LazyImage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("q7pC");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
 // import "./index.scss";







const MusicCard = props => {
  // console.log({ MusicCard: props });
  const {
    dotIconClick,
    dotModalInfo,
    data,
    placeholder,
    parentClass
  } = props;
  const {
    titleTop,
    titleMiddle,
    titleBottom,
    images
  } = data;
  const optionRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  const liRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  const imageOnload = () => {
    optionRef.current.style.display = "flex";
  };

  const preventDragHandler = e => e.preventDefault();

  return __jsx("li", {
    ref: liRef,
    onDragStart: preventDragHandler,
    className: `${parentClass ? parentClass : "card-row  col-lg-2 col-md-3 col-5  px-0"}`
  }, __jsx("div", {
    className: "play-Card-container  play-Card-wrapper"
  }, __jsx("div", {
    className: "imageCard-top"
  }, __jsx("picture", null, __jsx("source", {
    media: "(max-width: 375px)",
    srcSet: images.phone
  }), __jsx(_components_LazyImage__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"], {
    imageOnload: imageOnload,
    src: images.web,
    defaultImage: placeholder,
    alt: titleTop
  })), __jsx("div", {
    ref: optionRef,
    className: "card-options"
  }, __jsx("a", null, __jsx(_Ui_Icons_PlayIcon__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"] // className={classNameNew}
  , {
    style: {
      fontSize: "0.6em"
    }
  })), __jsx(_Ui_Icons_HeartIcon__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], {
    style: {
      fontSize: "0.6em"
    }
  }), __jsx(_Ui_Icons_DotIcon__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
    click: dotIconClick
  }))), __jsx("div", {
    className: "descriptionCard-bottom noSelect"
  }, __jsx("h4", {
    className: "play-Card-title"
  }, titleTop), __jsx("span", {
    className: "play-Card-subTitle" // style={colorStyle}

  }, titleMiddle), __jsx("h3", {
    className: "play-Card-text" //  style={colorStyle}

  }, titleBottom[0], __jsx("i", {
    className: "fas fa-circle"
  }), titleBottom[1]))));
};

/* harmony default export */ __webpack_exports__["a"] = (MusicCard);

/***/ }),

/***/ "W0sL":
/***/ (function(module, exports) {

module.exports = require("next-useragent");

/***/ }),

/***/ "YFqc":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("cTJO")


/***/ }),

/***/ "YTqd":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteRegex(normalizedRoute) {
  // Escape all characters that could be considered RegEx
  const escapedRoute = (normalizedRoute.replace(/\/$/, '') || '/').replace(/[|\\{}()[\]^$+*?.-]/g, '\\$&');
  const groups = {};
  let groupIndex = 1;
  const parameterizedRoute = escapedRoute.replace(/\/\\\[([^/]+?)\\\](?=\/|$)/g, (_, $1) => {
    const isCatchAll = /^(\\\.){3}/.test($1);
    groups[$1 // Un-escape key
    .replace(/\\([|\\{}()[\]^$+*?.-])/g, '$1').replace(/^\.{3}/, '') // eslint-disable-next-line no-sequences
    ] = {
      pos: groupIndex++,
      repeat: isCatchAll
    };
    return isCatchAll ? '/(.+?)' : '/([^/]+?)';
  });
  return {
    re: new RegExp('^' + parameterizedRoute + '(?:/)?$', 'i'),
    groups
  };
}

exports.getRouteRegex = getRouteRegex;

/***/ }),

/***/ "akFz":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./website/values/strings/index.js + 8 modules
var strings = __webpack_require__("wtyS");

// EXTERNAL MODULE: ./website/values/theme/themeColor.js
var themeColor = __webpack_require__("vEqB");

// CONCATENATED MODULE: ./website/values/consts/headerMiddle.js


let color = themeColor["a" /* default */].mainColor;
const headerMiddle = [{
  text: strings["a" /* default */].EXPLORE,
  icon: "far fa-compass",
  location: {
    href: "#"
  },
  color
}, {
  text: strings["a" /* default */].TRACKS,
  icon: "far fa-music",
  location: {
    href: "#"
  },
  color
}, {
  text: strings["a" /* default */].PLAYLISTS,
  icon: "far fa-list-music",
  location: {
    href: "#"
  },
  color
}, {
  text: strings["a" /* default */].ALBUMS,
  icon: "far fa-album",
  location: {
    href: "/albums"
  },
  color
}, {
  text: strings["a" /* default */].ARTISTS,
  icon: "far fa-user-music",
  location: {
    href: "#"
  },
  color
}, {
  text: strings["a" /* default */].VIDEOS,
  icon: "far fa-video",
  location: {
    href: "#"
  },
  color
}];
/* harmony default export */ var consts_headerMiddle = (headerMiddle);
// CONCATENATED MODULE: ./website/values/consts/constJson.js
const constJson = [{
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI8dfxNQMO5mcaq3AC4IxG5N-u2GQtfT-gS7Va9HT9IICnHwEhTA&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}];
/* harmony default export */ var consts_constJson = (constJson);
// CONCATENATED MODULE: ./website/values/consts/constJsonDouble.js
const constJsonDouble = [{
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUTEhMVFhUVFxcXFxUXFRYVFxcXFxcXFxUXFxUYHSggGBolHRUVITEhJSkrLi4uFx81ODMtNygtLisBCgoKDg0OFxAQGi0dICUrLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKy0tLS0tLS0tLS0tLS0tLf/AABEIAMIBAwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAAEFBgcDBAj/xABJEAACAQIDBQUECAIHBQkBAAABAgMAEQQSIQUGMUFREyJhcZEHMoGhFCNCUmKx0fBywTNTgpKiwuFDY4OT8RYXJHOjsrPD0xX/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQIDBAX/xAAkEQACAgIDAAEEAwAAAAAAAAAAAQIRAyESMUETBBQyUSJhcf/aAAwDAQACEQMRAD8AgHY3Op4nr1pBz1PrTNxPmacCug4Qgx6n1ow56n1oFowKACDHqfU0QY9T60IFGBSGOCep9aIMep9aYCjAoAQY9TRBj1PrSAogKAECep9ae56n1pAUWWkMFXvpfXzornqfWozbmJVFAHvnVTqLfiNtba/I1ExbwyAd4i4uNLcR1Fjp46fCk2UoNlqBPU+tcMfKVika57qMePgag4N7kuBJGydSO8Pkb/KvVtfakb4WXIwuy5RYk8SBzA8eNFj4tFp9kmOzQhb+9Gh484SYW+QiPxrQVNYR7NttjDSBXvbPcWv7rrlkBJ05Iw/hrXo95MOzBFlRmJAABHPr00BPp1rzM0amz0ce4k5mNPc9a5g3ojWVlni23h5JMNNHEQJHidELEgBmUqCSL2te9ZG/s4xcSZey7T8SyKWv4Xtp4VtBa1NmrXHmePoyyY1M+bNpbr46FrNh58v3gjsPVb145FtoxytaxDM6nl1UWr6gU1zmgV1IdVYE8GUMONudbL6v9oh4H+z5pTBkoWDA5GUACUG+bMxvfj7tdYYZ42AjlljLR9oWV2AACNIR3GF9ANfGvoDFbp4CUkPhIbaE2QLrrY922vH1qHm9nmAJDKkiM6mMlJG0TJlsA1wO6AK0+4i+0T8UkZJht9Npworri5CMzJlktJ7qoSe8Dp3xzq07O9sbBVE+GzGwu8clrnrkZdPK9TWO9lMEi9mmJlURs1iyo984Um9svCwHwqG/7oZUHdxMbnlmRl/mam8M+yryRRPbJ9qWDnkSLLOjuyqoZQRmYgDVWPM9Ku5Y/s1k+xvZzi4cdh5nVGjSRWdlccFuQbHXjataK1zZoQi/4muOTa2AxNC1+tdGFMwrGiwE4U1dEXSlQFmQMNT5n86cCnYanzP509q908YdRRBaQFdAKQDAUQFOBRqKBjAUYFJRRgUDGAowKa1FSAYCuWJkygkECwvc6ADmTXPG4+OEZpDYdbacbcaou3d5ZJrqvdj1sBoxHK55UmzSMWzybTxZZ7qzMdSSbnTy5DjpXlGfmH425gX8+tcIiR3gSPEafCpbC7WZkETG1rhWBI4/ZYDRh000rNtnQkiOkUi+oPiDfx+FdYnciwN78r3ufLiDSxKsrEHW3O1cTGdDlPna3zpAEC3Dn0vU5u9jUjkHbRqy3Gjq5UEkC4KHSwubm/Co6ePMobQW0J5npm/K9FH7ttT+E/5T+/jSltDWnZ9AbsYvDvf6LPnjFs0RYsY25ZSdQOII1FxpU/evn7dfbsmEYN2QZAbZrWy66DPlNutvGtW2BvimKB7MAFdCruFbwIHTx0rhnBx/w6IyUiyy3oVNNHJm/Pr6HnXSMa1lYw1olHu+f6mgJ0rsF1UdAf5CihjH7R/fD/Why6qOg/QUZ90+Jt6m1I+8x6D9TT8A4wjQnqzH52FPaniFlHkPnrXS1UloTZxIoCK7EUDCk0FgWpmWuhFMakLEi6UqQlUaFgD0JApqoVmQMNT5mnApNxPnRAV7J5AgK6AUwFGBQMQWjUUlFGBQAgKILTgUVIoYm1Q+19oWjYrowNiLXIKkMNByIv8ACpY4hLEE8tb6fnVF3jxvfKi+ZdA9+KG5Ct1tfQ0rLjEiNoYt5DqW46LckD4VwfBNoPtHl++dS6bNKZHkuCfs81FtP7R105DjTr3pwwFltlAHTl8f1rFzOlQo8EWBJUgDgLk+P7/KvIcMeJrTN3NjRlGJVXYnS/uKNNT94+FebamwCSQBwBNx1J5j48P0rD7hWdH27qygWbTXU6cflXrIKizXuDbXl4a8ONe3aGzDGx8OP76muCxtLlQk2HA6m3QE1ryTVmXBp0c8N3ja3/Tp4cqGeNraFsvi3ytfSrCuz8sZIFgBlv1IvcanSoWddMvIcel/PlxtURyKT0aTxOK2eM4xiDe5Btm10vwzW5Gu2zSqyrnJCn7Wvyym/EAUo0yhlYWOh8COh+Vc8ehFuNrX05edaaejCq2a1udtOWPEjDs/aRSAlDnD5CADbNp1tYi/ia0VUr502FtN1kja7WHA3UZSRYHMRwva4Nb1sbHAxCR5FK5ST7wyZQC2YubjjfXhbpXHOHFmylaJIiuwHePgB8yf0rGN8Pa27O0eAsiD/bst2bxRDoo8Tc+VUmLeDGTzR9pi8Q2d1B+te1i1j3QbDS9VHBJ7YfIkfTltF8Tf5XoJuD+n8q+cdm74Y+GOSWPFS/0iBQ7GRRmEjEZXuOCAfGtO3e9ogkkOFxahHVY2aYECO5EZcMD7nfew5eVEsLSGppmgk8qe1CK6EVAAGhIroRQGkMC1RO8G2kwyHgXylgCbKqjjJIfsoPnwFcN5N448Mr95cyi7u3uRA8M1tWY8kGprBt7N6XxTMqlhGWzHMe/Kw4NIR05INF5da0x4uWyJTromsbvsrSMRAs1yfrZHdWfqco0UdByFhSqlwjQUq6uETGzUmGp86ICkRqfP+dEBXScQ6ijAplFdAKB0OoolFICjFIBAUQFOBTgUFUQO8yIy2JyuPcP536iqVGbydQuvW4HAk8emtXzei4gJyhrEEg6gqD3h6XPwqg7MhLElb2sT45V1P5AVnM2xkx2jSsAx4XJ1vqTYADmbDX4U8EYDDlpqSfy8fGoqDEtwvwJ4G2vnzqY2UoLasVA5gXPpXNNNI7MbTZYtn4goFHDoP1FTcb5xwGoOvH46XHrUDIq5hYlh5EC19AL61O4F0tbmOQvl8unKuGR6MYkRjNmPKRHrkFr2AvwIHer1bN3VsLgfZ4+J/KpGWYrfLqTbT5D871O7IwgdFDAmwuVJbLc8AVva3h4U+UnoXBJ8im7dwGWOKCLUk2aTldtDlbnz1rybxbvDCwRrmHaSnNlA91Rc8eXL1rRsbswGRXfgvK3T8gKpm0pVnxLSufqoxZb89baDncggdbGhNrQOKlsoeKwJjytJYZuvK518tOVefa+HKItwAyqnC/Frn8hUztnaInm1UkKwPIC+oy/sVz2WwxWLKy+4SRYdFBCgetdcZOrZwzgrqJ5tywjv2UiB1DBrcTZtNANTY8vKrDvKpw+CxEUU1w7Il9RZNZMgJNwTlKm/3QKqJQ4fFfVkjKRbip68v2bVeJNmDaOAm7ASGWPvXJHeyWJUgm5bKTbjrSn+al4RH8WvTIq9+xzaUMR7qu392NiPnarBs3dQte4J0HIjU/CtR3c3HiGFivGCXQ3JHEMT/KtnlXSI+KVWzEYo2aKKMf7WY+oEar/72r34yUE4+S9wziMeTTdoPlBVkxm74TEQkKQsbvJblYSyPb+6i1UZ4CmFsR3nnNz/AOXGP5zmqjJS6JcXHs2b2M7wticK0MjZnw5CgniY2ByX8rMPICtEFZH7AcC4TFTEWRjHGviVzM3pmX1rXAK5siqTo1j0Mar+923VwsRObKxUsW49mg0L25kkhVHNiKsJrE/bRj2zMlz35Qp/hhjQhfi0pbzApQjykKTpFD3k3gfFPzWJSSkd76ni7n7Uh5sfLhUPEhYhQLkkADqToBTVfdyN3SlsRKLMR9Wp5A/bPj0/6V2peGEnSJ/Y278MUKI6Kzgd4/iJJPzNqVSycKVaUjn5SIsjU+ZowKTDU+ZohVGY6iui0KiugFIpDgUQFIUQoGPaipKKcCgCsb9YgpCACO8bEWJJ9OH+tVfCYvIl107QGPrZbC48+fxq078QRmMF1ObgrcuttOB/Ws/RsrZPHS3U25VnNWbY3SPZhAOut+Hh1qf2ZAxYBbj98yah9mwlpcqqTcC1vHn5aVqGw9hroZHVV01JA/u9T41z5X4dmBeg7H2E0pHdY/eYkKunlxq14rAxxRhbagacNfLp5n516cLtCJAI4ihA07p187866rhDKbyHh8/0rk+M7vk98IPZOzu0ckjQai3C/nVuwuC7NeH75UEcKrqLAAcKafbUIsC4q4xUeyJzc+jx7Zw6MjdwMSNdSF+J4W9apaYcODlUE8SxFtRwCg6KANB/rV6faMDK13vcHj5cq8GJSIxdwroPDlzPQ1Eop7TKjKlTRjG8adiWBVhrcdB4i2h8zUHsbEkShr8xr5G4qzb+2DXGvhfTlVQwisxLHhbXkPAACuvErhs4szrJok8Ue1kzA6s2nrx/OtL9k2EX6VPIAxyrlDDhZjcg+N+XnwrKsIxuABcXGnh+9a2z2RwFIZWJuXcEnxUWNx6fvjGVUkhRd2zntXa+y1lkCYmKKQMQ6OHRS6Eq2ViuUajlpVt2XtvCNGoixMD5EA7sqNYheGh46GsTwG0ZMRKUg2pKGdmYRyQM0a5mJsGzOAovbgOWmtqlNtbLxMKWeXZs7ZyG7aKGLXKuQBsi66vrm10tWiwRjtekPPJqn4WyWDML5b9y3Ak6rbTT8VQg9nsuKVEb6qNXdndh3+8w0VepCrqeFxx4VDZp4+3ePCuO8FVsFipCWGe+aySSKpCoOCjjatG9neMlljlMrTnKYltOQXDGJZH72UEnvqDccVrH4ZY1aZrLMsmqJ/Y+yosLCkEC5Y0FgOZ5kk8yTqTXsAogKcCpJOZFZD7bNlkqZAODJJ8GHZP/AIhF61sVqgN7cIGjV7XyMA2l+6/dvbwbIfhVY3UiZ9GKbpbo2tNiV14pEfkzj/L61c2NE1+B4jSgNegkcEpNnRDpSpR8KVAiPPE+ZogKTDU+Z/OiUUxBCjAoVowKBhAUYFCKMUDCFPakKIUDPFtXCpLGUdSw/D7w8RzvWV4/DBZHtfT3c9wza2uP0rYSL1RN89jpGhdc2p93iAeJJvyqWVB7KthsY5AsbZb6g2NvPnUlFNKwKx52spYm7BdBc89TVg9lexUnL51vY21HxrUMHusYTeEgKfeUgEG3A6jjqawc0nVHZHG2rujEcNi8VFdwr2QZmIzd0XsCb308a1L2abytjPqmPeUXv1FXHDbGRFKhI1U6kKigE/AV4th7PhjxbGMDNlNzp9or0/hFRNxfhrjjOPbs9m8WHdI+6SSeQ51nEuwMbLKq2KZtc7BmsPLh8PDlWw4i2YXp3w+YaEjyNqlR2W5vjRiEewNpLP2KnXORcppkv7x0tbLrfNbW3GrJs7ZGPRrOiMo0LIwsPNT+YrRU2a3ORiOhtXqSAKLD51U6fSoiDce3Zi2826plkFhxuWFuGnG3wqlbX2aMNLlvYjRtdL+FfRe0411OUE29axnezAGfGEJzsbHlew/SsotxdPo0nFTVpbKsQEVW6sNL20A1s1ud63bdyIwbOZ2Iv2TyGx7ijISoQW0WwB0HMnnWP7R2QXxS4VLERWVyTZSzHveNgMvxJrXtro2H2TMtu0KQZAtj3r2RUsupGoGmtObtxRjVJlL3Vw+KTvzT4SaMAIRaINYAllLSRKQdF0J5V7toxSlosPJg45o2YPJLHmsgVsyqFje5IsLmwBPLjaP2VjoJUlk+jWdMskqZnGZjrlCSI1gSGU25W11qxbZeOSIstwVjMkVo0ktmUhSLlWOgawuL2tXWzjTKJvVsXC4VEVosQhmlveORW76L3bdooNj2raX0txrWNwUH0VmDMweeaxYWNkfsgALnT6vTrx0rMtl7OxROESHGsVt2xVzLGXiMi3sCCraWFr/a8a1rc6JlwOGzkFzErMRaxZxnYi2mpY1lmejXES9qe1PalXObAkV5dpQdpDIn3kYfG2nztXrtQyaAnoDQIy3EHW/3grfFlBPzJrga9E49z+BfnqPkRXEivQj0jz5dhR8KVEi6UqoR4SNT5miApm4nzohQIMUYFCtGoplDiugoBRikAQFEKaiFAx7VwxuEWWNo2GjAjxB6ivQKIUDKxuNIcJjJYGOpysDwDAj9+lbHg5swF7Vhu+kjQ4qGdemXlyN/1rQd2d5RIgPUfsVyZlTs9D6eSlGvS64h7KT0FQG6OHYs0jixJtboOOvjrTbc2p2eGllH2UYjzsbVQNzvapHGHXEqQSxKsovcHkehHzrNJvaNm1HTNixg9afBYjiDxFZhtT2sKZQIoHkTS7AEG/S1qv8AsctJCsrAo0gzFTxW/AHxtahppiVOJN9pXCaSo44ll0avFi8ceA/etHIFAPaWK0NqyHb+1Po2LaW2ZhcKp0F+APkLVo+NxJtpwFibdTyrIN8ZzJiyoBIGugv8bDxvSgrkGSXGOjvurhsViMSHiuZM+dnI7gN7jOeFvDjpWte0V2XZjDtFiZ3hXtLuqqc4Y2ZQWHukDSvB7NcIohXKVJ4mxBN/EcRXT2xPGMLBHIJCsk40iy5+6jkWzAg620oi+WTowlqBAbn4PHRNIJsUsquoyg4nMQRmawEwBViFta1tdfCzAYgSMWQNGRdcsSO4Ol1LAWCj3ha5tw4VRH2/hJcsMwxCHgqtCq2a9kbuOCALE8Na6bJx8ky5UxOHlZjljJSWFgQrsScyFb3KHoBeuuSZyo9u0N4UV58N9HAJw5kQqMrCRzl0LZgmjKLganiNNNfw0IjRUHBFVR5KAB+VZBhosW+0GTtu0gMuHj0kSUEdpG0pKXJTRHHAca2Q1hlXRti6GNI0qasjQVRm8WK7OBre8/cW/VtGJ6ALc17cXikiUvIwVRz/ACAHM+ArMd5d5PpGLXDLo1iXXj2MI1Ksf62Q5Qw+yptxOlQjyZMpUh52BYkcNAP4QAF+QFcSK7OKAiu5I4HsJBpTUUY0pU6CzwnifOiApm4nzNEKBBLRChFGopjQQoxQiiFIYQo1oKKgYQp70IpwaAK9v7hO0wjMOMZD/AcfkTVf3G2iyyBb6HUDl5VoLqCCDqCCCPOsq2phHwM+XXJmzI1jqOg8eVZzjaNsUuLL1tLfaMg4fEIyKwIvbunl6VTX2LgHl7mMCIeN1Jt5HTSrvh8Rhto4fLKAWHA6Ai/Cx5VHYbYuPwr/APh4Enj4r3QGHhrXNCl/TO+uTXLaLDu2dm4VcsUxnPG0UbSnxuFHGp6PeeIkLFFimbhY4eVRwtqWAA9ar2Bn23IcownZA8ywC+gNW3ZuyJYgHxEhdrXIuAo62AFZySWzf+NVFjbNfFO318QjGpALKxtyvbgfia6YjDAtc8tL17J8aNL+YPDS3yqs7e3hWEEn4G/HXlUqn0Zu/Tz7w7TTDwk8zwB5nUWrENq4h+3Y3IZTa40II8fWrnHjGx+MQH3UOY+QuRf41G787tSxH6SoLRSM1yBfs31zA2+zoSDXRjSXZz5ZcuujlszfnGRWuyyAcpUDN/f9751Z/wDt3LiYpZGgQth0FgWd0BldVLAFwyWCkXU373KsvU/sVKbF2xJhw/Zsoz2DBkjcEDwdTprWsYqznk9FzwO98EzAYvCKcisVkRmJjAUsbBybcOR4mpTAbc2ZE8H0aKSNXZiBlzKTYRsHJdWyki9vw1UcLtwFZC+FwjWTiIezY5mRLExMunePpXtjxOEzRn6MylMO8l452soZZGtllV7nvjW/EjpWjRBN+z6KH6Yk4xCylppJTZGia6QyCwR9NDiVOhPEVsK7YjP2Zf8AlOfyFfMu3xAsUAhMhB7R8siqD3mCe8psdYugqHjmI4Fh5EisZw5M1jKkfWMm2EH2JT/wmHzawqu7a3/w8AN5IkI5PIJH/wCVCWPqRXzc0pI1JPmSfzrmTUrEh8maDvX7TJJiRh86nUds9g4H+6jHdi/i1bxFF7M8H3Jp2uS7ZATxIHeY353JHpWdohYgAXJIAHUnQCtq2Fs/6Ph44uar3v4ySzfMmtYJGWR6PawoGFdDQGtTnY6cKVFGNKVAjwHifM060zcT50QpjCFGKFaMUDCFPTU4pAEDT0N6V6Bh3pXoL0E0qqpZiFUcWJAA+JoA6568G24IJYiuIKhPvMQuU9QTzqs7a35RLrh1zn77XCDyHFvlVI2ptWbENeVy1uA4Kvko0FS2i4wZK7A2uMNMQGzx5iAeFxfRrHhy9a2Ddze9HsMwsLD1H/X0rAEr0Q4l11ViPIkcOFc88fLZ148vHT2j6Wk3ijDFS3AXseNeXEbwLfvN3e9x6af618/Da8/324W48qCTGSMLMxI6XrN4JP02X1K/RfN5t9C75YfcUnXraqljtoy4h7XJJ0AF7V59n7PeVgqj9K03c/c5Y7O4u3lw8q0UY40YucsjC3I3eMEWZh3m1P6Vc4MMPohuNRLGR59tHb87fGvSYAq2r0dn3I0+9NF8cjiU/KM1Ftuy+lRgu/eyUw2PxECKBGGDIOiuqvYdACxA8qjdlbXmwylYpmRSxJW+hPDVToeHSrZ7YkttR/GKE/Ij+VVjZ23MTEoSKd1XU5A111OvcNx8q7Y7SOKXbPcN45OykMkeHkIZAM+Hi1Bzk3KgE+4vOvVPtCC85fCIMkaRns5JIybmNCLMWUWseA4CrFuzh/puFvI0Xbdsf6XCRMjxKApKdkFYsC471+ZHjVbx+0MOwnLYVTmnCho3liLW7Rr5XzAH3dLfa8KQFe3meLPGIw6hYkBDsGILDP7wAv7/AEFQ5NTG8UatiZclwFbJYkMe53OIA07ulPs7Y8MtgcVHGekiMP8AF7p9ahxZaaSIS9FGhYgKCSdAALknoBzrQMB7P4Tq2JMg/wB2FA/vXarPsvYmHw39FGA33z3nP9o6+lHETmkV7czdMwkTzj6z7Cfc/E34vDl58LjTGmq0qMW2xzTUjTGmI6oNKemQ6UqQjwEanzNOKZuJ86cVQghRigFEDQUHSoQae9ABUqjtq7Zhww+sbvHgi6sfG3IeJsKpu1d7p5brH9UngbuR4vy+HrQNKy2ba3hhw1wTnk5Rrxv+I/ZHz8KoG2drTYk3kOn2UGir8OZ8TXjA/f8AOnAootaPI8dcJI7VIOtcpI6mUTRM8aGuq1zkS1HBqQOFRRQYqZ2Fsd8QwsO7zNS+xN0DIAx4Vf8AY2yxEAAo9BUSnXRUY8g93N2VjUWX41dMNhgo4cK82BQgCveVJFY9m/R5pBmYV0U5sUicoYzI38UpMcf+FZvUV6cLh+Zqgbwb/R4SGVoiHxeKcsi3zCGIARwM/Q5FDhOrm+lXGNmcpFK9rGMWXac2U3EapGT4ot29CxHwqFw23p0RUzIyKNFeGGQaDh3kJ+dRTMTdmJJa5JJuSTqSTzJNTH/9ePg+Dw7WAF17WNviUcD5V1pUjkk7ZLYPeZlOHUwQgAPKOzEkOU3kDMqxOFBKx2Omt64YWXCyfR80MiZ5XkOWbOL3jVi2dSx9w8+vWpbF7EjUZ2wsgRMNmMkOKSRFVkN1s63zDOTa+vGoxIcMMpjmcdnA7hZIhoGEjXLox1u45dKkGVeUhmZhezMSLgA2J5gG16ApTqdKerGNA7RnNGzI3VSVPyq0bH3zdbLiRnX+sAGceYGjfI+dVi9MVpUJ7Ndw+ISRQ6MGU8GGoNdL1mWwNtPhX5tGfeS/+JejD51o8UyuqupurAEHqDwpENUdb0jQXpwaCTtHwp6aMaU9AzwtxPmacUzDU+dOBTEEKKhpUAEKqu8G9oS8eH1YaGTQqD+EfaPjw868u923iWMELWAuJGHEnmoPIDn6VUlWg0jH0KR2ZizMWZjckm5J8SeNOKa1EKZQ9KlSoENT20pGlQM4tHXEw17rUNqVD5ElsDenFYTRCHT7kgzD4Hiv5eFXbZ/tSh07bCOD1jdWHo2W3rWcWprVDxxZSyNGwx+1jAAf0OKH9iL/APSuWK9sWGA+qwk7H8bRoP8ACWrJLUqPiiHysue3fanj8SpjjCYdGBB7O7SEHQjtG4fAA1SUitRkimzCrUUiG2wkYXW+ouLgGxIuLgG2htUzIMAb2bFR3NjdIZhf4FLjSojB4ftJEQMi5j7ztlUWBOp5cKncNu1OWTL2Ugzgns54X0uOQa/XlQSya2ntJJYsRGuIw+U9nHaSCWHJkuoHcDIx+rJDcio5aVD7Q2W6DEOGjYJFHH3JEY6GJWut8w0VjqOdcpdhYgL9ZDKrSzC90awGupNrWvKfSvDjpLpK39ZiOPgvaEj/ANRPlU0VdkeKc01PViFSFMaEUAItVv3H2pxw7Hq0f+df5+tUy9ejBYkxusi8UII+HEfEXHxpA0azenBrlFIGAYcGAI8iLj86O9IyPVG2lKhiJtSoGeVuJ86cUjxPmaQpiFXk2rjOxhkk+6pt/EdF+ZFes1VN/sXZI4QdXOdvJdAPU3/s0DStlMF+J1PM9SeNPTUiaDU6CnNcYW1I+IruaYMYU9MKRNAhU4FNenoGFTU6mhoEI0jSp6AGBpWpjSBoAWShKCulMKBigUBwegJ+VWXdDCJJiY1kAZcrue+EICgm4PM6DT9Khdl4x4ZC8ZF8tu8quCCfuuCOVTMe3rh+0gw72j/qhGbsVW14iunePpR4Q9smtu7PXAthfo0sxibtJe1EgMbqBmsCti1gijhbU6m+lV2ptGaSCBZZC+rv3rFuIQd46/ZbnzqTbF4Y+9h2TJh+Mcx0Eg4BZA2v1x586idtiIGIRZ7CJdHAuMxLjVePvdBUop9kbSvSNKqAY01O1A1IYJ404NIimFAzRt0sRnwqdULJ6HT5EVMA1Vtwpfq5V6Orf3lt/lq0XpGMtM9EZ0pUMfClQAZUXNOFHSmpUAIqOlUDfgD6V/w1/NqalTLh2QYUdBSyjoKVKgoB1GZdBXpyi3ClSpIb8BCjoKcqOgpqVMTHyjoKdlHQUqVMBKo6CllHQUqVIXoig6CnCjoKVKmN9A5R0FNlHQUqVAMIKOgpFR0FKlQIUajvaDgPzr2YdBlk0HBOX4hT0qQju6i8+n+yh/8AprzbWUdrwH9HF/8AGtKlQM8eUdBSKjoKalTGM6joKHKOlKlSGDkFhoKQUdBSpUDfRatwx3pvJPzareFF6VKgwl2dFFNSpUhH/9k="
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRF9IIwP_vI4I30S2hhzXBtsMQQ5IDg4mUlCJG5YADjyk1iZtzD&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ2lWkUYUjSisqChVAJ2uaMwWIVb2aSP55jx-MTTxL2OIHY1TWX&usqp=CAU"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}];
/* harmony default export */ var consts_constJsonDouble = (constJsonDouble);
// CONCATENATED MODULE: ./website/values/consts/moreExploreConst.js
const moreExploreConst = [{
  title: "Geners"
}, {
  title: "Instruments"
}, {
  title: "Videos"
}, {
  title: "Artists"
}, {
  title: "PlayList"
}, {
  title: "example"
}, {
  title: "example2"
}, {
  title: "example3"
}, {
  title: "example4"
}, {
  title: "example5"
}, {
  title: "example6"
}];
/* harmony default export */ var consts_moreExploreConst = (moreExploreConst);
// CONCATENATED MODULE: ./website/values/consts/instrumentals.js
const instrumentals = [{
  titleFa: "پیانو",
  titleEn: "Piano",
  icon: "far fa-piano"
}, {
  titleFa: "چمپن استیک",
  titleEn: "Chapman Stick",
  icon: "far fa-guitar-electric"
}, {
  titleFa: "گیتار",
  titleEn: "Guitar",
  icon: "fal fa-guitar"
}, {
  titleFa: "ساکسیفون",
  titleEn: "Saxophone",
  icon: "fal fa-saxophone"
}, {
  titleFa: "پیانو",
  titleEn: "Piano",
  icon: "far fa-piano"
}, {
  titleFa: "چمپن استیک",
  titleEn: "Chapman Stick",
  icon: "far fa-guitar-electric"
}, {
  titleFa: "گیتار",
  titleEn: "Guitar",
  icon: "fal fa-guitar"
}, {
  titleFa: "ساکسیفون",
  titleEn: "Saxophone",
  icon: "fal fa-saxophone"
}, {
  titleFa: "پیانو",
  titleEn: "Piano",
  icon: "far fa-piano"
}, {
  titleFa: "چمپن استیک",
  titleEn: "Chapman Stick",
  icon: "far fa-guitar-electric"
}, {
  titleFa: "گیتار",
  titleEn: "Guitar",
  icon: "fal fa-guitar"
}, {
  titleFa: "ساکسیفون",
  titleEn: "Saxophone",
  icon: "fal fa-saxophone"
}];
/* harmony default export */ var consts_instrumentals = (instrumentals);
// CONCATENATED MODULE: ./website/values/consts/index.js





const consts = {
  headerMiddle: consts_headerMiddle,
  constJson: consts_constJson,
  constJsonDouble: consts_constJsonDouble,
  moreExploreConst: consts_moreExploreConst,
  instrumentals: consts_instrumentals
};
/* harmony default export */ var values_consts = (consts);
// CONCATENATED MODULE: ./website/values/index.js



const values = {
  consts: values_consts,
  strings: strings["a" /* default */],
  themeColor: themeColor["a" /* default */]
};
/* harmony default export */ var website_values = (values);
// CONCATENATED MODULE: ./website/utils/convert/sliderCard.js


const sliderCard = (data, direction) => {
  let convertData = []; // console.log({PAPPAAPA:data});

  for (const index in data) {
    let noEntries = website_0.values.strings.NO_ENTRIED;
    let title = data[index].title ? data[index].title : noEntries;
    let parentName = data[index].parentName ? data[index].parentName : noEntries;
    let parentArtist = data[index].parentArtist ? data[index].parentArtist : noEntries;
    convertData.push({
      titleTop: title,
      titleMiddle: parentName,
      titleBottom: parentArtist,
      images: data[index].images,
      location: {
        href: "/slider",
        as: `/slider#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_sliderCard = (sliderCard);
// CONCATENATED MODULE: ./website/utils/chunkArray.js
const chunkArray = (myArray, chunkSize) => {
  let results = [];

  while (myArray.length) {
    results.push(myArray.splice(0, chunkSize));
  }

  return results;
};

/* harmony default export */ var utils_chunkArray = (chunkArray);
// CONCATENATED MODULE: ./website/utils/convert/albumCard.js


const albumCard = (data, chunkNumber, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (let index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleMiddle: data[index].artist,
        titleBottom: [data[index].releaseDate, data[index].genres[0]],
        images: data[index].images,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    if (chunkNumber) convertData = utils_chunkArray(convertData, chunkNumber); // convertData = chunkArray(convertData, Math.ceil(data.length / 2));

    return convertData;
  }
};

/* harmony default export */ var convert_albumCard = (albumCard);
// CONCATENATED MODULE: ./website/utils/convert/topTracksCard.js


const topTracksCard = (data, chunkNumber, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (let index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleBottom: [data[index].artist, ", ", data[index].artist],
        image: data[index].cover,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    if (chunkNumber) convertData = utils_chunkArray(convertData, chunkNumber); // convertData = chunkArray(convertData, Math.ceil(data.length / 2));

    return convertData;
  }
};

/* harmony default export */ var convert_topTracksCard = (topTracksCard);
// CONCATENATED MODULE: ./website/utils/convert/singleCard.js


const singleCard = (data, direction) => {
  if (data) {
    // console.log({ data });
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (const index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleMiddle: data[index].artist,
        titleBottom: [data[index].releaseDate, data[index].genres[0]],
        images: data[index].images,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    convertData = utils_chunkArray(convertData, 2); // convertData = chunkArray(convertData, Math.ceil(data.length / 2));
    // console.log({ convertData });

    return convertData;
  }
};

/* harmony default export */ var convert_singleCard = (singleCard);
// CONCATENATED MODULE: ./website/utils/convert/comingSoonCard.js


const comingSoonCard = (data, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (const index in data) {
      let noEntries = website_0.values.strings.NO_ENTRIED;
      let releaseDate = data[index].releaseDate;
      let year = releaseDate.year + "/" + releaseDate.month + "/" + releaseDate.day;
      let title = data[index].title ? data[index].title : noEntries;
      let artist = data[index].artist ? data[index].artist : noEntries;
      let genres = data[index].genres.length ? data[index].genres[0] : noEntries;
      convertData.push({
        titleTop: title,
        titleMiddle: artist,
        titleBottom: [year, genres],
        images: data[index].images,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    return convertData;
  }
};

/* harmony default export */ var convert_comingSoonCard = (comingSoonCard);
// CONCATENATED MODULE: ./website/utils/convert/moodCard.js
const moodCard = (data, direction) => {
  let convertData = []; // console.log({ mood: data });

  let dir = false;
  if (direction === "rtl") dir = true;

  for (const index in data) {
    convertData.push({
      title: data[index].title,
      images: data[index].images,
      location: {
        href: "/slider",
        as: `/slider#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_moodCard = (moodCard);
// CONCATENATED MODULE: ./website/utils/convert/playlistCard.js


const playlistCard = (data, direction) => {
  let convertData = []; // console.log({PAPPAAPA:data});

  let noEntries = website_0.values.strings.NO_ENTRIED;

  for (const index in data) {
    let title = data[index].title ? data[index].title : noEntries;
    let publisher = data[index].publisher ? data[index].publisher : noEntries;
    let tracksCount = data[index].tracksCount ? data[index].tracksCount : "0";
    let genres = data[index].genres ? data[index].genres : "0";
    convertData.push({
      titleTop: title,
      titleMiddle: publisher,
      titleBottom: [tracksCount + " " + website_0.values.strings.TRACKS, genres[0] + " " + website_0.values.strings.FOLLOWERS],
      images: data[index].images,
      location: {
        href: "/playlist",
        as: `/playlist#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_playlistCard = (playlistCard);
// CONCATENATED MODULE: ./website/utils/convert/videoCard.js


const videoCard = data => {
  let convertData = []; // console.log({ mood: data });

  for (const index in data) {
    let noEntries = website_0.values.strings.NO_ENTRIED;
    let title = data[index].title ? data[index].title : noEntries;
    let artist = data[index].artist ? data[index].artist : noEntries;
    let releaseDate = data[index].releaseDate ? data[index].releaseDate : "0";
    let genres = data[index].genres ? data[index].genres : "0";
    convertData.push({
      titleTop: title,
      titleMiddle: artist,
      titleBottom: [releaseDate, genres[0]],
      images: data[index].images,
      location: {
        href: "/video",
        as: `/video#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_videoCard = (videoCard);
// CONCATENATED MODULE: ./website/utils/convert/PlaylistForAutuminCard.js


const PlaylistForAutuminCard = (data, direction) => {
  let convertData = []; // console.log({PAPPAAPA:data});

  let noEntries = website_0.values.strings.NO_ENTRIED;

  for (const index in data) {
    let title = data[index].title ? data[index].title : noEntries;
    let publisher = data[index].publisher ? data[index].publisher : noEntries;
    let tracksCount = data[index].tracksCount ? data[index].tracksCount : "0";
    let genres = data[index].genres ? data[index].genres : "0";
    convertData.push({
      titleTop: title,
      titleMiddle: publisher,
      titleBottom: [tracksCount + " " + website_0.values.strings.TRACKS, genres[0] + " " + website_0.values.strings.FOLLOWERS],
      images: data[index].images,
      location: {
        href: "/playlist",
        as: `/playlist#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ var convert_PlaylistForAutuminCard = (PlaylistForAutuminCard);
// CONCATENATED MODULE: ./website/utils/convert/index.js








 // import moreExploreCard from "./moreExploreCard";

const convert = {
  sliderCard: convert_sliderCard,
  albumCard: convert_albumCard,
  singleCard: convert_singleCard,
  comingSoonCard: convert_comingSoonCard,
  moodCard: convert_moodCard,
  playlistCard: convert_playlistCard,
  videoCard: convert_videoCard,
  PlaylistForAutuminCard: convert_PlaylistForAutuminCard,
  topTrackCard: convert_topTracksCard
};
/* harmony default export */ var utils_convert = (convert);
// CONCATENATED MODULE: ./website/utils/index.js

const utils = {
  convert: utils_convert
};
/* harmony default export */ var website_utils = (utils);
// EXTERNAL MODULE: external "redux"
var external_redux_ = __webpack_require__("rKB8");

// EXTERNAL MODULE: ./website/storeWebsite/actionTypes/redux/index.js
var redux = __webpack_require__("lmmF");

// CONCATENATED MODULE: ./website/storeWebsite/reducer/errorReducer.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const errorInitialState = {
  error: null
};
const addFailure = (state, action) => {
  return _objectSpread({}, state, {
    error: action.error
  });
};
const removeFailure = state => {
  return _objectSpread({}, state, {
    error: null
  });
};

function errorReducer(state = errorInitialState, action) {
  switch (action.type) {
    case redux["a" /* default */].ADD_FAILURE:
      return addFailure(state, action);

    case redux["a" /* default */].REMOVE_FAILURE:
      return removeFailure(state);

    default:
      return state;
  }
}

/* harmony default export */ var reducer_errorReducer = (errorReducer);
// CONCATENATED MODULE: ./website/storeWebsite/reducer/homeReducer.js
function homeReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function homeReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { homeReducer_ownKeys(Object(source), true).forEach(function (key) { homeReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { homeReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function homeReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const homeInitialState = {
  homeData: []
};
const setHomeData = (state, action) => {
  return homeReducer_objectSpread({}, state, {
    homeData: action.data
  });
};

function homeReducer(state = homeInitialState, action) {
  switch (action.type) {
    case redux["a" /* default */].SET_HOME_DATA:
      return setHomeData(state, action);

    default:
      return state;
  }
}

/* harmony default export */ var reducer_homeReducer = (homeReducer);
// CONCATENATED MODULE: ./website/storeWebsite/reducer/themeColorReducer.js
function themeColorReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function themeColorReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { themeColorReducer_ownKeys(Object(source), true).forEach(function (key) { themeColorReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { themeColorReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function themeColorReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const webThemeColorInitialstate = {
  light: {
    primaryColor: "#0070ef",
    accentColor: "#666666",
    mainColor: "#ffff",
    titleColor: "#333333"
  },
  dark: {
    primaryColor: "#0070ef",
    accentColor: "#666666",
    mainColor: "#000",
    titleColor: "#ffff"
  },
  currentTheme: "light"
};
const setThemeColor = (state, action) => {
  return themeColorReducer_objectSpread({}, state, {
    homeData: action.data
  });
};

function themeColorReducer(state = webThemeColorInitialstate, action) {
  //   switch (action.type) {
  //     case atRedux.:
  //       return setThemeColor(state, action);
  //     default:
  //       return state;
  //   }
  return state;
}

/* harmony default export */ var reducer_themeColorReducer = (themeColorReducer);
// CONCATENATED MODULE: ./website/storeWebsite/reducer/filterReducer.js
function filterReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function filterReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { filterReducer_ownKeys(Object(source), true).forEach(function (key) { filterReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { filterReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function filterReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const filterInisialState = {
  filterData: []
};
const setFilter = (state, action) => {
  if (state.filterData.length > 0) {
    const filter = state.filterData.filter(item => {
      if (item.type !== action.data.type) return item;
    });
    return filterReducer_objectSpread({}, state, {
      filterData: [...filter, action.data]
    });
  } else {
    return filterReducer_objectSpread({}, state, {
      filterData: [...state.filterData, action.data]
    });
  }
};
const removeFilter = (state, action) => {
  return filterReducer_objectSpread({}, state, {
    filterData: []
  });
};

function filterReducer(state = filterInisialState, action) {
  switch (action.type) {
    case redux["a" /* default */].SET_FILTER_TYPE:
      return setFilter(state, action);

    case redux["a" /* default */].REMOVE_FILTER_TYPE:
      return removeFilter();

    default:
      return state;
  }
}

/* harmony default export */ var reducer_filterReducer = (filterReducer);
// CONCATENATED MODULE: ./website/storeWebsite/reducer/index.js





const rootReducer = {
  error: reducer_errorReducer,
  home: reducer_homeReducer,
  themeColor: reducer_themeColorReducer,
  filter: reducer_filterReducer
};
/* harmony default export */ var reducer = (rootReducer);
// CONCATENATED MODULE: ./website/index.js



const website = {
  values: website_values,
  utils: website_utils,
  reducer: reducer
};
/* harmony default export */ var website_0 = __webpack_exports__["a"] = (website);

/***/ }),

/***/ "bzos":
/***/ (function(module, exports) {

module.exports = require("url");

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "cDf5":
/***/ (function(module, exports) {

function _typeof2(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

function _typeof(obj) {
  if (typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return _typeof2(obj);
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "cTJO":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__("TqRt");

var _interopRequireWildcard = __webpack_require__("284h");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__("cDcd"));

var _url = __webpack_require__("bzos");

var _utils = __webpack_require__("kYf9");

var _router = _interopRequireDefault(__webpack_require__("nOHt"));

var _router2 = __webpack_require__("elyg");

function isLocal(href) {
  var url = (0, _url.parse)(href, false, true);
  var origin = (0, _url.parse)((0, _utils.getLocationOrigin)(), false, true);
  return !url.host || url.protocol === origin.protocol && url.host === origin.host;
}

function memoizedFormatUrl(formatFunc) {
  var lastHref = null;
  var lastAs = null;
  var lastResult = null;
  return (href, as) => {
    if (lastResult && href === lastHref && as === lastAs) {
      return lastResult;
    }

    var result = formatFunc(href, as);
    lastHref = href;
    lastAs = as;
    lastResult = result;
    return result;
  };
}

function formatUrl(url) {
  return url && typeof url === 'object' ? (0, _utils.formatWithValidation)(url) : url;
}

var observer;
var listeners = new Map();
var IntersectionObserver = false ? undefined : null;
var prefetched = {};

function getObserver() {
  // Return shared instance of IntersectionObserver if already created
  if (observer) {
    return observer;
  } // Only create shared IntersectionObserver if supported in browser


  if (!IntersectionObserver) {
    return undefined;
  }

  return observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (!listeners.has(entry.target)) {
        return;
      }

      var cb = listeners.get(entry.target);

      if (entry.isIntersecting || entry.intersectionRatio > 0) {
        observer.unobserve(entry.target);
        listeners.delete(entry.target);
        cb();
      }
    });
  }, {
    rootMargin: '200px'
  });
}

var listenToIntersections = (el, cb) => {
  var observer = getObserver();

  if (!observer) {
    return () => {};
  }

  observer.observe(el);
  listeners.set(el, cb);
  return () => {
    try {
      observer.unobserve(el);
    } catch (err) {
      console.error(err);
    }

    listeners.delete(el);
  };
};

class Link extends _react.Component {
  constructor(props) {
    super(props);
    this.p = void 0;

    this.cleanUpListeners = () => {};

    this.formatUrls = memoizedFormatUrl((href, asHref) => {
      return {
        href: (0, _router2.addBasePath)(formatUrl(href)),
        as: asHref ? (0, _router2.addBasePath)(formatUrl(asHref)) : asHref
      };
    });

    this.linkClicked = e => {
      var {
        nodeName,
        target
      } = e.currentTarget;

      if (nodeName === 'A' && (target && target !== '_self' || e.metaKey || e.ctrlKey || e.shiftKey || e.nativeEvent && e.nativeEvent.which === 2)) {
        // ignore click for new tab / new window behavior
        return;
      }

      var {
        href,
        as
      } = this.formatUrls(this.props.href, this.props.as);

      if (!isLocal(href)) {
        // ignore click if it's outside our scope (e.g. https://google.com)
        return;
      }

      var {
        pathname
      } = window.location;
      href = (0, _url.resolve)(pathname, href);
      as = as ? (0, _url.resolve)(pathname, as) : href;
      e.preventDefault(); //  avoid scroll for urls with anchor refs

      var {
        scroll
      } = this.props;

      if (scroll == null) {
        scroll = as.indexOf('#') < 0;
      } // replace state instead of push if prop is present


      _router.default[this.props.replace ? 'replace' : 'push'](href, as, {
        shallow: this.props.shallow
      }).then(success => {
        if (!success) return;

        if (scroll) {
          window.scrollTo(0, 0);
          document.body.focus();
        }
      });
    };

    if (false) {}

    this.p = props.prefetch !== false;
  }

  componentWillUnmount() {
    this.cleanUpListeners();
  }

  getPaths() {
    var {
      pathname
    } = window.location;
    var {
      href: parsedHref,
      as: parsedAs
    } = this.formatUrls(this.props.href, this.props.as);
    var resolvedHref = (0, _url.resolve)(pathname, parsedHref);
    return [resolvedHref, parsedAs ? (0, _url.resolve)(pathname, parsedAs) : resolvedHref];
  }

  handleRef(ref) {
    if (this.p && IntersectionObserver && ref && ref.tagName) {
      this.cleanUpListeners();
      var isPrefetched = prefetched[this.getPaths().join( // Join on an invalid URI character
      '%')];

      if (!isPrefetched) {
        this.cleanUpListeners = listenToIntersections(ref, () => {
          this.prefetch();
        });
      }
    }
  } // The function is memoized so that no extra lifecycles are needed
  // as per https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html


  prefetch(options) {
    if (!this.p || true) return; // Prefetch the JSON page if asked (only in the client)

    var paths = this.getPaths(); // We need to handle a prefetch error here since we may be
    // loading with priority which can reject but we don't
    // want to force navigation since this is only a prefetch

    _router.default.prefetch(paths[
    /* href */
    0], paths[
    /* asPath */
    1], options).catch(err => {
      if (false) {}
    });

    prefetched[paths.join( // Join on an invalid URI character
    '%')] = true;
  }

  render() {
    var {
      children
    } = this.props;
    var {
      href,
      as
    } = this.formatUrls(this.props.href, this.props.as); // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

    if (typeof children === 'string') {
      children = _react.default.createElement("a", null, children);
    } // This will return the first child, if multiple are provided it will throw an error


    var child = _react.Children.only(children);

    var props = {
      ref: el => {
        this.handleRef(el);

        if (child && typeof child === 'object' && child.ref) {
          if (typeof child.ref === 'function') child.ref(el);else if (typeof child.ref === 'object') {
            child.ref.current = el;
          }
        }
      },
      onMouseEnter: e => {
        if (child.props && typeof child.props.onMouseEnter === 'function') {
          child.props.onMouseEnter(e);
        }

        this.prefetch({
          priority: true
        });
      },
      onClick: e => {
        if (child.props && typeof child.props.onClick === 'function') {
          child.props.onClick(e);
        }

        if (!e.defaultPrevented) {
          this.linkClicked(e);
        }
      }
    }; // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
    // defined, we specify the current 'href', so that repetition is not needed by the user

    if (this.props.passHref || child.type === 'a' && !('href' in child.props)) {
      props.href = as || href;
    } // Add the ending slash to the paths. So, we can serve the
    // "<page>/index.html" directly.


    if (false) { var rewriteUrlForNextExport; }

    return _react.default.cloneElement(child, props);
  }

}

if (false) { var exact, PropTypes, warn; }

var _default = Link;
exports.default = _default;

/***/ }),

/***/ "dZ6Y":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/*
MIT License

Copyright (c) Jason Miller (https://jasonformat.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

Object.defineProperty(exports, "__esModule", {
  value: true
});

function mitt() {
  const all = Object.create(null);
  return {
    on(type, handler) {
      ;
      (all[type] || (all[type] = [])).push(handler);
    },

    off(type, handler) {
      if (all[type]) {
        // tslint:disable-next-line:no-bitwise
        all[type].splice(all[type].indexOf(handler) >>> 0, 1);
      }
    },

    emit(type, ...evts) {
      // eslint-disable-next-line array-callback-return
      ;
      (all[type] || []).slice().map(handler => {
        handler(...evts);
      });
    }

  };
}

exports.default = mitt;

/***/ }),

/***/ "elyg":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__("bzos");

const mitt_1 = __importDefault(__webpack_require__("dZ6Y"));

const utils_1 = __webpack_require__("g/15");

const is_dynamic_1 = __webpack_require__("/jkW");

const route_matcher_1 = __webpack_require__("gguc");

const route_regex_1 = __webpack_require__("YTqd");

const basePath =  false || '';

function addBasePath(path) {
  return path.indexOf(basePath) !== 0 ? basePath + path : path;
}

exports.addBasePath = addBasePath;

function delBasePath(path) {
  return path.indexOf(basePath) === 0 ? path.substr(basePath.length) || '/' : path;
}

function toRoute(path) {
  return path.replace(/\/$/, '') || '/';
}

const prepareRoute = path => toRoute(!path || path === '/' ? '/index' : path);

function fetchNextData(pathname, query, isServerRender, cb) {
  let attempts = isServerRender ? 3 : 1;

  function getResponse() {
    return fetch(utils_1.formatWithValidation({
      // @ts-ignore __NEXT_DATA__
      pathname: `/_next/data/${__NEXT_DATA__.buildId}${pathname}.json`,
      query
    }), {
      // Cookies are required to be present for Next.js' SSG "Preview Mode".
      // Cookies may also be required for `getServerSideProps`.
      //
      // > `fetch` won’t send cookies, unless you set the credentials init
      // > option.
      // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
      //
      // > For maximum browser compatibility when it comes to sending &
      // > receiving cookies, always supply the `credentials: 'same-origin'`
      // > option instead of relying on the default.
      // https://github.com/github/fetch#caveats
      credentials: 'same-origin'
    }).then(res => {
      if (!res.ok) {
        if (--attempts > 0 && res.status >= 500) {
          return getResponse();
        }

        throw new Error(`Failed to load static props`);
      }

      return res.json();
    });
  }

  return getResponse().then(data => {
    return cb ? cb(data) : data;
  }).catch(err => {
    // We should only trigger a server-side transition if this was caused
    // on a client-side transition. Otherwise, we'd get into an infinite
    // loop.
    if (!isServerRender) {
      ;
      err.code = 'PAGE_LOAD_ERROR';
    }

    throw err;
  });
}

class Router {
  constructor(pathname, query, as, {
    initialProps,
    pageLoader,
    App,
    wrapApp,
    Component,
    err,
    subscription,
    isFallback
  }) {
    // Static Data Cache
    this.sdc = {};

    this.onPopState = e => {
      if (!e.state) {
        // We get state as undefined for two reasons.
        //  1. With older safari (< 8) and older chrome (< 34)
        //  2. When the URL changed with #
        //
        // In the both cases, we don't need to proceed and change the route.
        // (as it's already changed)
        // But we can simply replace the state with the new changes.
        // Actually, for (1) we don't need to nothing. But it's hard to detect that event.
        // So, doing the following for (1) does no harm.
        const {
          pathname,
          query
        } = this;
        this.changeState('replaceState', utils_1.formatWithValidation({
          pathname,
          query
        }), utils_1.getURL());
        return;
      } // Make sure we don't re-render on initial load,
      // can be caused by navigating back from an external site


      if (e.state && this.isSsr && e.state.as === this.asPath && url_1.parse(e.state.url).pathname === this.pathname) {
        return;
      } // If the downstream application returns falsy, return.
      // They will then be responsible for handling the event.


      if (this._bps && !this._bps(e.state)) {
        return;
      }

      const {
        url,
        as,
        options
      } = e.state;

      if (false) {}

      this.replace(url, as, options);
    };

    this._getStaticData = asPath => {
      const pathname = prepareRoute(url_1.parse(asPath).pathname);
      return  true && this.sdc[pathname] ? Promise.resolve(this.sdc[pathname]) : fetchNextData(pathname, null, this.isSsr, data => this.sdc[pathname] = data);
    };

    this._getServerData = asPath => {
      let {
        pathname,
        query
      } = url_1.parse(asPath, true);
      pathname = prepareRoute(pathname);
      return fetchNextData(pathname, query, this.isSsr);
    }; // represents the current component key


    this.route = toRoute(pathname); // set up the component cache (by route keys)

    this.components = {}; // We should not keep the cache, if there's an error
    // Otherwise, this cause issues when when going back and
    // come again to the errored page.

    if (pathname !== '/_error') {
      this.components[this.route] = {
        Component,
        props: initialProps,
        err,
        __N_SSG: initialProps && initialProps.__N_SSG,
        __N_SSP: initialProps && initialProps.__N_SSP
      };
    }

    this.components['/_app'] = {
      Component: App
    }; // Backwards compat for Router.router.events
    // TODO: Should be remove the following major version as it was never documented

    this.events = Router.events;
    this.pageLoader = pageLoader;
    this.pathname = pathname;
    this.query = query; // if auto prerendered and dynamic route wait to update asPath
    // until after mount to prevent hydration mismatch

    this.asPath = // @ts-ignore this is temporarily global (attached to window)
    is_dynamic_1.isDynamicRoute(pathname) && __NEXT_DATA__.autoExport ? pathname : as;
    this.basePath = basePath;
    this.sub = subscription;
    this.clc = null;
    this._wrapApp = wrapApp; // make sure to ignore extra popState in safari on navigating
    // back from external site

    this.isSsr = true;
    this.isFallback = isFallback;

    if (false) {}
  } // @deprecated backwards compatibility even though it's a private method.


  static _rewriteUrlForNextExport(url) {
    if (false) {} else {
      return url;
    }
  }

  update(route, mod) {
    const Component = mod.default || mod;
    const data = this.components[route];

    if (!data) {
      throw new Error(`Cannot update unavailable route: ${route}`);
    }

    const newData = Object.assign(Object.assign({}, data), {
      Component,
      __N_SSG: mod.__N_SSG,
      __N_SSP: mod.__N_SSP
    });
    this.components[route] = newData; // pages/_app.js updated

    if (route === '/_app') {
      this.notify(this.components[this.route]);
      return;
    }

    if (route === this.route) {
      this.notify(newData);
    }
  }

  reload() {
    window.location.reload();
  }
  /**
   * Go back in history
   */


  back() {
    window.history.back();
  }
  /**
   * Performs a `pushState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  push(url, as = url, options = {}) {
    return this.change('pushState', url, as, options);
  }
  /**
   * Performs a `replaceState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  replace(url, as = url, options = {}) {
    return this.change('replaceState', url, as, options);
  }

  change(method, _url, _as, options) {
    return new Promise((resolve, reject) => {
      if (!options._h) {
        this.isSsr = false;
      } // marking route changes as a navigation start entry


      if (utils_1.ST) {
        performance.mark('routeChange');
      } // If url and as provided as an object representation,
      // we'll format them into the string version here.


      let url = typeof _url === 'object' ? utils_1.formatWithValidation(_url) : _url;
      let as = typeof _as === 'object' ? utils_1.formatWithValidation(_as) : _as;
      url = addBasePath(url);
      as = addBasePath(as); // Add the ending slash to the paths. So, we can serve the
      // "<page>/index.html" directly for the SSR page.

      if (false) {}

      this.abortComponentLoad(as); // If the url change is only related to a hash change
      // We should not proceed. We should only change the state.
      // WARNING: `_h` is an internal option for handing Next.js client-side
      // hydration. Your app should _never_ use this property. It may change at
      // any time without notice.

      if (!options._h && this.onlyAHashChange(as)) {
        this.asPath = as;
        Router.events.emit('hashChangeStart', as);
        this.changeState(method, url, as, options);
        this.scrollToHash(as);
        Router.events.emit('hashChangeComplete', as);
        return resolve(true);
      }

      const {
        pathname,
        query,
        protocol
      } = url_1.parse(url, true);

      if (!pathname || protocol) {
        if (false) {}

        return resolve(false);
      } // If asked to change the current URL we should reload the current page
      // (not location.reload() but reload getInitialProps and other Next.js stuffs)
      // We also need to set the method = replaceState always
      // as this should not go into the history (That's how browsers work)
      // We should compare the new asPath to the current asPath, not the url


      if (!this.urlIsNew(as)) {
        method = 'replaceState';
      }

      const route = toRoute(pathname);
      const {
        shallow = false
      } = options;

      if (is_dynamic_1.isDynamicRoute(route)) {
        const {
          pathname: asPathname
        } = url_1.parse(as);
        const routeRegex = route_regex_1.getRouteRegex(route);
        const routeMatch = route_matcher_1.getRouteMatcher(routeRegex)(asPathname);

        if (!routeMatch) {
          const missingParams = Object.keys(routeRegex.groups).filter(param => !query[param]);

          if (missingParams.length > 0) {
            if (false) {}

            return reject(new Error(`The provided \`as\` value (${asPathname}) is incompatible with the \`href\` value (${route}). ` + `Read more: https://err.sh/zeit/next.js/incompatible-href-as`));
          }
        } else {
          // Merge params into `query`, overwriting any specified in search
          Object.assign(query, routeMatch);
        }
      }

      Router.events.emit('routeChangeStart', as); // If shallow is true and the route exists in the router cache we reuse the previous result

      this.getRouteInfo(route, pathname, query, as, shallow).then(routeInfo => {
        const {
          error
        } = routeInfo;

        if (error && error.cancelled) {
          return resolve(false);
        }

        Router.events.emit('beforeHistoryChange', as);
        this.changeState(method, url, as, options);

        if (false) {}

        this.set(route, pathname, query, as, routeInfo);

        if (error) {
          Router.events.emit('routeChangeError', error, as);
          throw error;
        }

        Router.events.emit('routeChangeComplete', as);
        return resolve(true);
      }, reject);
    });
  }

  changeState(method, url, as, options = {}) {
    if (false) {}

    if (method !== 'pushState' || utils_1.getURL() !== as) {
      window.history[method]({
        url,
        as,
        options
      }, // Most browsers currently ignores this parameter, although they may use it in the future.
      // Passing the empty string here should be safe against future changes to the method.
      // https://developer.mozilla.org/en-US/docs/Web/API/History/replaceState
      '', as);
    }
  }

  getRouteInfo(route, pathname, query, as, shallow = false) {
    const cachedRouteInfo = this.components[route]; // If there is a shallow route transition possible
    // If the route is already rendered on the screen.

    if (shallow && cachedRouteInfo && this.route === route) {
      return Promise.resolve(cachedRouteInfo);
    }

    const handleError = (err, loadErrorFail) => {
      return new Promise(resolve => {
        if (err.code === 'PAGE_LOAD_ERROR' || loadErrorFail) {
          // If we can't load the page it could be one of following reasons
          //  1. Page doesn't exists
          //  2. Page does exist in a different zone
          //  3. Internal error while loading the page
          // So, doing a hard reload is the proper way to deal with this.
          window.location.href = as; // Changing the URL doesn't block executing the current code path.
          // So, we need to mark it as a cancelled error and stop the routing logic.

          err.cancelled = true; // @ts-ignore TODO: fix the control flow here

          return resolve({
            error: err
          });
        }

        if (err.cancelled) {
          // @ts-ignore TODO: fix the control flow here
          return resolve({
            error: err
          });
        }

        resolve(this.fetchComponent('/_error').then(res => {
          const {
            page: Component
          } = res;
          const routeInfo = {
            Component,
            err
          };
          return new Promise(resolve => {
            this.getInitialProps(Component, {
              err,
              pathname,
              query
            }).then(props => {
              routeInfo.props = props;
              routeInfo.error = err;
              resolve(routeInfo);
            }, gipErr => {
              console.error('Error in error page `getInitialProps`: ', gipErr);
              routeInfo.error = err;
              routeInfo.props = {};
              resolve(routeInfo);
            });
          });
        }).catch(err => handleError(err, true)));
      });
    };

    return new Promise((resolve, reject) => {
      if (cachedRouteInfo) {
        return resolve(cachedRouteInfo);
      }

      this.fetchComponent(route).then(res => resolve({
        Component: res.page,
        __N_SSG: res.mod.__N_SSG,
        __N_SSP: res.mod.__N_SSP
      }), reject);
    }).then(routeInfo => {
      const {
        Component,
        __N_SSG,
        __N_SSP
      } = routeInfo;

      if (false) {}

      return this._getData(() => __N_SSG ? this._getStaticData(as) : __N_SSP ? this._getServerData(as) : this.getInitialProps(Component, // we provide AppTree later so this needs to be `any`
      {
        pathname,
        query,
        asPath: as
      })).then(props => {
        routeInfo.props = props;
        this.components[route] = routeInfo;
        return routeInfo;
      });
    }).catch(handleError);
  }

  set(route, pathname, query, as, data) {
    this.isFallback = false;
    this.route = route;
    this.pathname = pathname;
    this.query = query;
    this.asPath = as;
    this.notify(data);
  }
  /**
   * Callback to execute before replacing router state
   * @param cb callback to be executed
   */


  beforePopState(cb) {
    this._bps = cb;
  }

  onlyAHashChange(as) {
    if (!this.asPath) return false;
    const [oldUrlNoHash, oldHash] = this.asPath.split('#');
    const [newUrlNoHash, newHash] = as.split('#'); // Makes sure we scroll to the provided hash if the url/hash are the same

    if (newHash && oldUrlNoHash === newUrlNoHash && oldHash === newHash) {
      return true;
    } // If the urls are change, there's more than a hash change


    if (oldUrlNoHash !== newUrlNoHash) {
      return false;
    } // If the hash has changed, then it's a hash only change.
    // This check is necessary to handle both the enter and
    // leave hash === '' cases. The identity case falls through
    // and is treated as a next reload.


    return oldHash !== newHash;
  }

  scrollToHash(as) {
    const [, hash] = as.split('#'); // Scroll to top if the hash is just `#` with no value

    if (hash === '') {
      window.scrollTo(0, 0);
      return;
    } // First we check if the element by id is found


    const idEl = document.getElementById(hash);

    if (idEl) {
      idEl.scrollIntoView();
      return;
    } // If there's no element with the id, we check the `name` property
    // To mirror browsers


    const nameEl = document.getElementsByName(hash)[0];

    if (nameEl) {
      nameEl.scrollIntoView();
    }
  }

  urlIsNew(asPath) {
    return this.asPath !== asPath;
  }
  /**
   * Prefetch page code, you may wait for the data during page rendering.
   * This feature only works in production!
   * @param url the href of prefetched page
   * @param asPath the as path of the prefetched page
   */


  prefetch(url, asPath = url, options = {}) {
    return new Promise((resolve, reject) => {
      const {
        pathname,
        protocol
      } = url_1.parse(url);

      if (!pathname || protocol) {
        if (false) {}

        return;
      } // Prefetch is not supported in development mode because it would trigger on-demand-entries


      if (false) {}

      const route = delBasePath(toRoute(pathname));
      Promise.all([this.pageLoader.prefetchData(url, delBasePath(asPath)), this.pageLoader[options.priority ? 'loadPage' : 'prefetch'](route)]).then(() => resolve(), reject);
    });
  }

  async fetchComponent(route) {
    let cancelled = false;

    const cancel = this.clc = () => {
      cancelled = true;
    };

    route = delBasePath(route);
    const componentResult = await this.pageLoader.loadPage(route);

    if (cancelled) {
      const error = new Error(`Abort fetching component for route: "${route}"`);
      error.cancelled = true;
      throw error;
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    return componentResult;
  }

  _getData(fn) {
    let cancelled = false;

    const cancel = () => {
      cancelled = true;
    };

    this.clc = cancel;
    return fn().then(data => {
      if (cancel === this.clc) {
        this.clc = null;
      }

      if (cancelled) {
        const err = new Error('Loading initial props cancelled');
        err.cancelled = true;
        throw err;
      }

      return data;
    });
  }

  getInitialProps(Component, ctx) {
    const {
      Component: App
    } = this.components['/_app'];

    const AppTree = this._wrapApp(App);

    ctx.AppTree = AppTree;
    return utils_1.loadGetInitialProps(App, {
      AppTree,
      Component,
      router: this,
      ctx
    });
  }

  abortComponentLoad(as) {
    if (this.clc) {
      const e = new Error('Route Cancelled');
      e.cancelled = true;
      Router.events.emit('routeChangeError', e, as);
      this.clc();
      this.clc = null;
    }
  }

  notify(data) {
    this.sub(data, this.components['/_app'].Component);
  }

}

exports.default = Router;
Router.events = mitt_1.default();

/***/ }),

/***/ "g/15":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__("bzos");
/**
 * Utils
 */


function execOnce(fn) {
  let used = false;
  let result;
  return (...args) => {
    if (!used) {
      used = true;
      result = fn(...args);
    }

    return result;
  };
}

exports.execOnce = execOnce;

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

exports.getLocationOrigin = getLocationOrigin;

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

exports.getURL = getURL;

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

exports.getDisplayName = getDisplayName;

function isResSent(res) {
  return res.finished || res.headersSent;
}

exports.isResSent = isResSent;

async function loadGetInitialProps(App, ctx) {
  var _a;

  if (false) {} // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!App.getInitialProps) {
    if (ctx.ctx && ctx.Component) {
      // @ts-ignore pageProps default
      return {
        pageProps: await loadGetInitialProps(ctx.Component, ctx.ctx)
      };
    }

    return {};
  }

  const props = await App.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(App)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (false) {}

  return props;
}

exports.loadGetInitialProps = loadGetInitialProps;
exports.urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];

function formatWithValidation(url, options) {
  if (false) {}

  return url_1.format(url, options);
}

exports.formatWithValidation = formatWithValidation;
exports.SP = typeof performance !== 'undefined';
exports.ST = exports.SP && typeof performance.mark === 'function' && typeof performance.measure === 'function';

/***/ }),

/***/ "gfwP":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
 // import "./index.scss";

const PlayIcon = ({
  style,
  className
}) => {
  return __jsx("div", {
    className: `icon-wrapper ${className}`
  }, __jsx("i", {
    className: "fas fa-play"
  }));
};

/* harmony default export */ __webpack_exports__["a"] = (PlayIcon);

/***/ }),

/***/ "gguc":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteMatcher(routeRegex) {
  const {
    re,
    groups
  } = routeRegex;
  return pathname => {
    const routeMatch = re.exec(pathname);

    if (!routeMatch) {
      return false;
    }

    const decode = param => {
      try {
        return decodeURIComponent(param);
      } catch (_) {
        const err = new Error('failed to decode param');
        err.code = 'DECODE_FAILED';
        throw err;
      }
    };

    const params = {};
    Object.keys(groups).forEach(slugName => {
      const g = groups[slugName];
      const m = routeMatch[g.pos];

      if (m !== undefined) {
        params[slugName] = ~m.indexOf('/') ? m.split('/').map(entry => decode(entry)) : g.repeat ? [decode(m)] : decode(m);
      }
    });
    return params;
  };
}

exports.getRouteMatcher = getRouteMatcher;

/***/ }),

/***/ "h74D":
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "kYf9":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/utils.js");

/***/ }),

/***/ "lmmF":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const atRedux = {
  //======================================================== Redux
  SET_HOME_DATA: "SET_HOME_DATA_REDUX",
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX",
  SET_FILTER_TYPE: "SET_FILTER_TYPE_REDUX",
  REMOVE_FILTER_TYPE: "REMOVE_FILTER_TYPE_REDUX"
};
/* harmony default export */ __webpack_exports__["a"] = (atRedux);

/***/ }),

/***/ "nOHt":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__("284h");

var _interopRequireDefault = __webpack_require__("TqRt");

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__("cDcd"));

var _router2 = _interopRequireWildcard(__webpack_require__("elyg"));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__("Osoz");

var _withRouter = _interopRequireDefault(__webpack_require__("0Bsm"));

exports.withRouter = _withRouter.default;
/* global window */

var singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

var urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components', 'isFallback', 'basePath'];
var routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
var coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

Object.defineProperty(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  Object.defineProperty(singletonRouter, field, {
    get() {
      var router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = function () {
    var router = getRouter();
    return router[field](...arguments);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, function () {
      var eventField = "on" + event.charAt(0).toUpperCase() + event.substring(1);
      var _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...arguments);
        } catch (err) {
          // tslint:disable-next-line:no-console
          console.error("Error when running the Router event: " + eventField); // tslint:disable-next-line:no-console

          console.error(err.message + "\n" + err.stack);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    var message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


var createRouter = function createRouter() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  var _router = router;
  var instance = {};

  for (var property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = Object.assign({}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = function () {
      return _router[field](...arguments);
    };
  });
  return instance;
}

/***/ }),

/***/ "oviB":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const HeartIcon = ({
  style
}) => {
  const {
    0: actived,
    1: setActived
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);

  const _handelClick = () => {
    setActived(!actived);
  };

  return __jsx("div", {
    onClick: _handelClick,
    style: style,
    className: "icon-wrapper"
  }, actived ? __jsx("i", {
    style: {
      color: "red"
    },
    className: "fas fa-heart"
  }) : __jsx("i", {
    className: "far fa-heart"
  }));
};

/* harmony default export */ __webpack_exports__["a"] = (HeartIcon);

/***/ }),

/***/ "q7pC":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export LazyImage */
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("HJQg");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;

const LazyImage = ({
  src,
  alt,
  defaultImage,
  imageOnload
}) => {
  let placeHolder = defaultImage ? defaultImage : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSP-Ze14CTqFzlYouUOJ7OIGAN14YRJrmT1EwYEhsJTBrC92fhtTQ&s";
  const {
    0: imageSrc,
    1: setImageSrc
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(placeHolder); //   const [imageRef, setImageRef] = useState();
  //   console.log({ imageSrc });

  const imageRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  const onLoad = event => {
    // console.log({ onLoad: event });
    event.target.classList.add("loaded"); // imageOnload();
  };

  const onError = event => {
    // console.log({ error: event });
    event.target.classList.add("has-error");
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    let observer;
    let didCancel = false;

    if (imageRef.current && imageSrc !== src) {
      if (IntersectionObserver) {
        observer = new IntersectionObserver(entries => {
          // console.log({ observer, entries });
          entries.forEach(entry => {
            if (!didCancel && (entry.intersectionRatio > 0 || entry.isIntersecting)) {
              setImageSrc(src);
              observer.unobserve(imageRef.current) && observer.unobserve(imageRef.current);
            }
          });
        }, {
          threshold: 0.01,
          rootMargin: "75%"
        });
        observer.observe(imageRef.current);
      } else {
        // Old browsers fallback
        setImageSrc(src);
      }
    }

    return () => {
      didCancel = true; // on component cleanup, we remove the listner

      if (observer && observer.unobserve) {
        observer.unobserve(imageRef.current);
      }
    };
  }, [src, imageSrc, imageRef.current]); //   return <Image ref={setImageRef} src={imageSrc} alt={alt} onLoad={onLoad} onError={onError} />;

  return __jsx(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, __jsx("img", {
    ref: imageRef,
    src: imageSrc ? imageSrc : placeHolder,
    alt: alt,
    onLoadCapture: onLoad,
    onError: onError,
    className: styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a.dynamic([["3389864842", [placeHolder]]]) + " " + "imageComponent"
  }), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    id: "3389864842",
    dynamic: [placeHolder]
  }, [".imageComponent.__jsx-style-dynamic-selector{display:block;}", ".imageComponent.loaded.__jsx-style-dynamic-selector{-webkit-animation:loaded-__jsx-style-dynamic-selector 0.35s ease-in-out;animation:loaded-__jsx-style-dynamic-selector 0.35s ease-in-out;}", `.imageComponent.has-error.__jsx-style-dynamic-selector{content:url(${placeHolder});width:100%;height:100%;}`, ".image-lazyloading-container.__jsx-style-dynamic-selector{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;width:100%;height:100%;background-color:#ccc;}", "@-webkit-keyframes loaded-__jsx-style-dynamic-selector{0%{opacity:0.7;}100%{opacity:1;}}", "@keyframes loaded-__jsx-style-dynamic-selector{0%{opacity:0.7;}100%{opacity:1;}}"]));
};
/* harmony default export */ __webpack_exports__["a"] = (LazyImage);

/***/ }),

/***/ "rKB8":
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),

/***/ "rYez":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const DotIcon = ({
  click,
  overflowUnset
}) => {
  const wrapperRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  const modalData = [{
    icon: "far fa-stream",
    title: "Add To Quque"
  }, {
    icon: "far fa-album",
    title: "Go To Album"
  }, {
    icon: "fal fa-plus",
    title: "Add To Playlist",
    child: []
  }, {
    icon: "fas fa-share",
    title: "Share",
    child: []
  }];
  const {
    0: modal,
    1: setModal
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    show: false,
    data: modalData
  });

  const toggleDotIconClick = () => {
    console.log("toggleDotIconClick"); // console.log({ wrapperRef: wrapperRef.current });

    setModal(prev => _objectSpread({}, prev, {
      show: !prev.show
    }));
  };

  const handleClickOutside = event => {
    if (modal.show) if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      toggleDotIconClick();
    } //  else if (wrapperRef.current && wrapperRef.current.contains(event.target)) {
    //   console.log({ handleClickOutside: event, screenX: event.screenX, screenY: event.screenY });
    // }
  };

  const showModal = event => {
    console.log({
      showModal: event
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    document.addEventListener("click", handleClickOutside);
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  });

  const modalElement = __jsx("div", {
    className: `small-modal-card-container ${modal && modal.show ? "showModalElement" : ""}`
  }, __jsx("div", {
    className: "small-modal-card-wrapper"
  }, modal && modal.data.map((data, index) => {
    return __jsx("div", {
      key: "modal-" + index,
      className: "modal-element-details"
    }, __jsx("i", {
      className: data.icon
    }), __jsx("div", {
      className: "title-arrow"
    }, __jsx("div", {
      className: "small-modal-card-title"
    }, data.title), data.child ? __jsx("div", {
      className: "chevron-arrow-dropDown"
    }, __jsx("i", {
      className: "far fa-chevron-right"
    })) : ""));
  })));

  const moseleave = () => {
    if (modal.show) toggleDotIconClick();
  };

  return __jsx("div", {
    onMouseLeave: moseleave,
    ref: wrapperRef,
    className: " icon-ellipsis relative centerAll pointer",
    style: {
      color: "white"
    }
  }, __jsx("div", {
    onClick: toggleDotIconClick,
    className: ""
  }, __jsx("i", {
    className: "far fa-ellipsis-h"
  })), modalElement);
};

/* harmony default export */ __webpack_exports__["a"] = (DotIcon);

/***/ }),

/***/ "sHut":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const atSaga = {
  GET_HOME_SCREEN_DATA: "GET_HOME_SCREEN_DATA_SAGA" // POST_UPLOAD_IMAGE: "POST_UPLOAD_IMAGE_SAGA",
  // GET_GALLERY_DATA: "GET_GALLERY_DATA_SAGA",
  // GET_ARTIST_DATA: "GET_ARTIST_DATA_SAGA",
  // GET_SEARCH_ARTIST_DATA: "GET_SEARCH_ARTIST_DATA_SAGA",

};
/* harmony default export */ __webpack_exports__["a"] = (atSaga);

/***/ }),

/***/ "vEqB":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
  mainColor: "#0070ef",
  accentColor: "#666666",
  white: "#ffff",
  black: "#000",
  textColor: "#666666"
});

/***/ }),

/***/ "vfdN":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// CONCATENATED MODULE: ./website/components/Header/HamburgerMenu/index.js
var __jsx = external_react_default.a.createElement;
 // import "./index.scss";
// import { useDispatch, useSelector } from "react-redux";
// import * as actions from "../../../../store/actions";

const HamburgerMenu = () => {
  // const dispatch = useDispatch();
  // const user = useSelector((state) => {
  //   console.log({ state });
  //   return state;
  // });
  // console.log({ user });
  // const getUser = () => dispatch(actions.setUserInfo("tooooooooooooooooooken"));
  return __jsx("div", {
    className: "HamburgerMenu"
  }, __jsx("div", {
    className: "border-hamburger-menu"
  }, __jsx("i", {
    // onClick={getUser}
    className: "fas fa-bars"
  })));
};

/* harmony default export */ var Header_HamburgerMenu = (HamburgerMenu);
// EXTERNAL MODULE: ./public/assets/images/Rimtal.png
var Rimtal = __webpack_require__("2FOw");
var Rimtal_default = /*#__PURE__*/__webpack_require__.n(Rimtal);

// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__("YFqc");
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);

// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__("4Q3z");

// CONCATENATED MODULE: ./website/components/Header/HeaderMiddleElement/index.js
var HeaderMiddleElement_jsx = external_react_default.a.createElement;


 // import "./index.scss";

const HeaderMiddleElement = ({
  icon,
  text,
  color,
  location
}) => {
  const router = Object(router_["useRouter"])();
  let locationActive = false;
  if (router.pathname === location.href) locationActive = true;
  return HeaderMiddleElement_jsx(link_default.a, location, HeaderMiddleElement_jsx("a", {
    className: `${locationActive ? "url-active-in-page" : ""} navbar-link-to-other-page`
  }, HeaderMiddleElement_jsx("div", {
    className: "text-icon-wrapper pointer "
  }, HeaderMiddleElement_jsx("div", null, HeaderMiddleElement_jsx("div", {
    className: "header-icon-middle"
  }, HeaderMiddleElement_jsx("i", {
    style: {
      color
    },
    className: icon
  })), HeaderMiddleElement_jsx("div", {
    className: "header-text-middle "
  }, text)), HeaderMiddleElement_jsx("div", {
    className: "active-page"
  }))));
};

/* harmony default export */ var Header_HeaderMiddleElement = (HeaderMiddleElement);
// EXTERNAL MODULE: ./website/values/strings/index.js + 8 modules
var strings = __webpack_require__("wtyS");

// CONCATENATED MODULE: ./website/components/Header/LoginElement/index.js
var LoginElement_jsx = external_react_default.a.createElement;
 // import "./index.scss";

const LoginElement = ({
  color,
  strings
}) => {
  return LoginElement_jsx("div", {
    className: "login-element "
  }, LoginElement_jsx("div", {
    className: "login-wrapper"
  }, LoginElement_jsx("div", {
    className: "login-element-container pointer",
    style: {
      backgroundColor: color.mainColor,
      color: color.white
    }
  }, LoginElement_jsx("div", null, LoginElement_jsx("i", {
    className: "fas fa-user-alt"
  })), LoginElement_jsx("div", null, LoginElement_jsx("div", {
    className: ""
  }, strings.SIGN_IN))), LoginElement_jsx("div", {
    className: "for-small-display login-element-container pointer "
  }, LoginElement_jsx("i", {
    className: "far fa-user-circle"
  }), LoginElement_jsx("div", {
    className: "chevron-down-icon"
  }, " ", LoginElement_jsx("i", {
    className: "fal fa-chevron-down"
  })))));
};

/* harmony default export */ var Header_LoginElement = (LoginElement);
// EXTERNAL MODULE: ./website/values/theme/themeColor.js
var themeColor = __webpack_require__("vEqB");

// EXTERNAL MODULE: ./website/index.js + 24 modules
var website = __webpack_require__("akFz");

// EXTERNAL MODULE: ./components/AwesomeScroll/index.js
var AwesomeScroll = __webpack_require__("K3nj");

// CONCATENATED MODULE: ./website/components/Header/index/index.js
var index_jsx = external_react_default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }




 // import consts from "../../../../public/values/consts";


 // import "./index.scss";






const Header = () => {
  return index_jsx("header", {
    style: {
      color: themeColor["a" /* default */].textColor
    },
    className: "header-wrapper "
  }, index_jsx("div", {
    className: "header-container  row-main-wrapper",
    onDragStart: e => e.preventDefault()
  }, index_jsx("div", {
    className: "header-side-elements"
  }, index_jsx(Header_HamburgerMenu, null), index_jsx("div", {
    className: "header-search-icon centerAll pointer for-small-display"
  }, index_jsx("i", {
    className: "fal fa-search"
  })), index_jsx(link_default.a, {
    href: "/"
  }, index_jsx("a", {
    className: "logo-website"
  }, index_jsx("img", {
    src: Rimtal_default.a,
    alt: "logo"
  })))), index_jsx("div", {
    className: "logo-website for-small-display"
  }, index_jsx(link_default.a, {
    href: "/"
  }, index_jsx("a", null, index_jsx("img", {
    src: Rimtal_default.a,
    alt: "logo"
  })))), index_jsx("div", {
    className: "header-middle-elements "
  }, index_jsx(AwesomeScroll["a" /* default */], null, index_jsx("div", {
    onDragStart: e => e.preventDefault(),
    className: "header-middle-into-elemen"
  }, website["a" /* default */].values.consts.headerMiddle.map((middle, index) => {
    return index_jsx(external_react_["Fragment"], null, index_jsx(Header_HeaderMiddleElement, _extends({}, middle, {
      key: "header-" + index
    })));
  })))), index_jsx("div", {
    className: "header-side-elements flex-end-for-small"
  }, index_jsx("div", {
    className: "header-search-icon centerAll pointer"
  }, index_jsx("i", {
    className: "fal fa-search"
  })), index_jsx("div", {
    className: "header-bell-icon pointer"
  }, index_jsx("i", {
    className: "far fa-bell"
  })), index_jsx(Header_LoginElement, {
    strings: strings["a" /* default */],
    color: themeColor["a" /* default */]
  }))));
};

/* harmony default export */ var Header_index = __webpack_exports__["a"] = (Header);

/***/ }),

/***/ "wtyS":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./website/values/strings/en/navbar.js
const APP_NAME = "Rimtal";
const EXPLORE = "Explore";
const TRACKS = "Tracks";
const PLAYLISTS = "Playlists";
const ALBUMS = "Albums";
const ARTISTS = "Artists";
const VIDEOS = "Videos";
const SIGN_IN = "Sign In";
const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN
};
/* harmony default export */ var en_navbar = (navbar);
// CONCATENATED MODULE: ./website/values/strings/en/homePage.js
const VIEW_ALL = "View All";
const SINGLES = "Singles";
const COMING_SOON = "Coming Soon";
const PLAY_LISTS = "Playlists";
const homePage_VIDEOS = "Videos";
const PLAYLIST_FOR_AUTUMN = "Playlist For Autumn";
const MOOD = "Mood";
const COMMING_SOON = "Comming Soon";
const homePage = {
  MOOD,
  SINGLES,
  VIEW_ALL,
  COMING_SOON,
  PLAY_LISTS,
  VIDEOS: homePage_VIDEOS,
  MOOD,
  PLAYLIST_FOR_AUTUMN,
  COMMING_SOON
};
/* harmony default export */ var en_homePage = (homePage);
// CONCATENATED MODULE: ./website/values/strings/en/global.js
const global_TRACKS = "Tracks";
const NO_ENTRIED = "no entried";
const FOLLOWERS = "followers";
const global = {
  TRACKS: global_TRACKS,
  NO_ENTRIED,
  FOLLOWERS
};
/* harmony default export */ var en_global = (global);
// CONCATENATED MODULE: ./website/values/strings/en/index.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const en = _objectSpread({}, en_navbar, {}, en_homePage, {}, en_global);

/* harmony default export */ var strings_en = (en);
// CONCATENATED MODULE: ./website/values/strings/fa/navbar.js
const navbar_APP_NAME = "ریمتال";
const navbar_EXPLORE = "کاوش کردن";
const navbar_TRACKS = "آهنگ ها";
const navbar_PLAYLISTS = "لیست های پخش";
const navbar_ALBUMS = "آلبوم ها";
const navbar_ARTISTS = "هنرمندان";
const navbar_VIDEOS = "ویدیو ها";
const navbar_SIGN_IN = "ورود";
const navbar_navbar = {
  APP_NAME: navbar_APP_NAME,
  EXPLORE: navbar_EXPLORE,
  TRACKS: navbar_TRACKS,
  PLAYLISTS: navbar_PLAYLISTS,
  ALBUMS: navbar_ALBUMS,
  ARTISTS: navbar_ARTISTS,
  VIDEOS: navbar_VIDEOS,
  SIGN_IN: navbar_SIGN_IN
};
/* harmony default export */ var fa_navbar = (navbar_navbar);
// CONCATENATED MODULE: ./website/values/strings/fa/homePage.js
const homePage_VIEW_ALL = "مشاهده همه";
const homePage_SINGLES = "تک آهنگ ها";
const homePage_COMING_SOON = "بزودی";
const homePage_PLAY_LISTS = "لیست پخش";
const fa_homePage_VIDEOS = "ویدیو ها";
const homePage_PLAYLIST_FOR_AUTUMN = "لیست پخش پیشنهادی ";
const homePage_MOOD = "حالت";
const homePage_COMMING_SOON = "بزودی";
const homePage_homePage = {
  MOOD: homePage_MOOD,
  SINGLES: homePage_SINGLES,
  VIEW_ALL: homePage_VIEW_ALL,
  SINGLES: homePage_SINGLES,
  COMING_SOON: homePage_COMING_SOON,
  PLAY_LISTS: homePage_PLAY_LISTS,
  VIDEOS: fa_homePage_VIDEOS,
  PLAYLIST_FOR_AUTUMN: homePage_PLAYLIST_FOR_AUTUMN,
  COMMING_SOON: homePage_COMMING_SOON
};
/* harmony default export */ var fa_homePage = (homePage_homePage);
// CONCATENATED MODULE: ./website/values/strings/fa/global.js
const fa_global_TRACKS = "آهنگ ها ";
const global_NO_ENTRIED = "وارد نشده";
const global_FOLLOWERS = "دنبال کنندگان";
const global_global = {
  TRACKS: fa_global_TRACKS,
  NO_ENTRIED: global_NO_ENTRIED,
  FOLLOWERS: global_FOLLOWERS
};
/* harmony default export */ var fa_global = (global_global);
// CONCATENATED MODULE: ./website/values/strings/fa/index.js
function fa_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function fa_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { fa_ownKeys(Object(source), true).forEach(function (key) { fa_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { fa_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function fa_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const fa = fa_objectSpread({}, fa_navbar, {}, fa_homePage, {}, fa_global);

/* harmony default export */ var strings_fa = (fa);
// CONCATENATED MODULE: ./website/values/strings/index.js


const Lang = "ltr";
let string;
if (Lang === "rtl") string = strings_fa;else string = strings_en;
/* harmony default export */ var strings = __webpack_exports__["a"] = (string);

/***/ })

/******/ });