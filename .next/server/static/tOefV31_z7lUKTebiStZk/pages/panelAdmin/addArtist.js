module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "1Awo":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const SpinnerRotate = () => {
  return __jsx("div", {
    className: "spinner"
  }, __jsx("div", {
    className: "cube1"
  }), __jsx("div", {
    className: "cube2"
  }));
};

/* harmony default export */ __webpack_exports__["a"] = (SpinnerRotate);

/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("wgdk");


/***/ }),

/***/ "7NiY":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const BackgrandCover = props => {
  return __jsx("div", {
    id: "coverContainer",
    onClick: props.onClick,
    className: props.fadeIn ? " fadeIn" : " fadeOut"
  });
};

/* harmony default export */ __webpack_exports__["a"] = (BackgrandCover);

/***/ }),

/***/ "9Pu4":
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/styles");

/***/ }),

/***/ "BpoA":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getHomeData", function() { return getHomeData; });
/* harmony import */ var _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("sHut");

function getHomeData() {
  console.log("getHomeData");
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].GET_HOME_SCREEN_DATA
  };
} // export function uploadImageData(data) {
//   return { type: atSaga.POST_UPLOAD_IMAGE, data: data };
// }
// export function getGalleryData(data) {
//   return { type: atSaga.GET_GALLERY_DATA, data: data };
// }
// export function getArtistData({ page }) {
//   return { type: atSaga.GET_ARTIST_DATA, page };
// }
// export function getSearchArtistData({ page }) {
//   return { type: atSaga.GET_SEARCH_ARTIST_DATA, page };
// }

/***/ }),

/***/ "D8kl":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "increment", function() { return increment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setHomeData", function() { return setHomeData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setFailure", function() { return setFailure; });
/* harmony import */ var _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("rUrl");

function increment() {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].INCREMENT
  };
} // // =================================================== NAVBAR
// export function setPageName(data) {
//   return { type: atRedux.SET_PAGE_NAME, data };
// }
// =================================================== HOME

function setHomeData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].SET_HOME_DATA,
    data
  };
} // =================================================== ERROR

function setFailure(error) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].ADD_FAILURE,
    error
  };
} // // =================================================== UPLOAD
// export function setUploadImage(data) {
//   return { type: atRedux.SET_UPLOAD_IMAGE, data };
// }
// // =================================================== GALLERY
// export function setGalleryData(data) {
//   return { type: atRedux.SET_GALLERY_DATA, data };
// }
// export function changeAddGalleryData(data) {
//   return { type: atRedux.CHANGE_ADD_GALLERY_DATA, data };
// }
// // =================================================== END GALLERY
// // =================================================== ARTIST
// export function setArtistData(data) {
//   return { type: atRedux.SET_ARTIST_DATA, data };
// }
// export function setSearchArtistData(data) {
//   return { type: atRedux.SET_SEARCH_ARTIST_DATA, data };
// }
// export function startGetArtist(data) {
//   return { type: atRedux.START_ARTIST_DATA, data };
// }
// export function startSearchArtist(data) {
//   return { type: atRedux.START_SEARCH_ARTIST_DATA, data };
// }
// // ================================================= END ARTIST

/***/ }),

/***/ "FRaV":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "get", function() { return /* reexport */ Get; });
__webpack_require__.d(__webpack_exports__, "post", function() { return /* reexport */ Post; });
__webpack_require__.d(__webpack_exports__, "put", function() { return /* reexport */ Put; });
__webpack_require__.d(__webpack_exports__, "patch", function() { return /* reexport */ api_Patch; });
__webpack_require__.d(__webpack_exports__, "deletes", function() { return /* reexport */ api_Delete; });

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__("zr5I");
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// CONCATENATED MODULE: ./panelAdmin/api/axios-orders.js
 // import Cookie from "js-cookie";

const instance = external_axios_default.a.create({
  baseURL: "https://rimtal.com/api/v1"
});
instance.defaults.headers.common["Authorization"] = "Bearer ";
/* harmony default export */ var axios_orders = (instance);
// EXTERNAL MODULE: ./panelAdmin/index.js + 104 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/api/Get/genres.js



const genres = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  const axiosData = page ? strings.GENRES + "/" + page : strings.GENRES;
  return axios_orders.get(axiosData); // .then((genres) => {
  //   console.log({ genres });
  //   returnData(genres.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_genres = (genres);
// CONCATENATED MODULE: ./panelAdmin/api/Get/genreSearch.js



const genreSearch = async (param, page, returnData) => {
  // console.log({ param, page, returnData });
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  return axios_orders.get(strings.GENRES + "/" + param + "/" + page).then(genreSearch => {
    console.log({
      genreSearch
    });
    returnData(genreSearch.data); // loading(false);
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ var Get_genreSearch = (genreSearch);
// CONCATENATED MODULE: ./panelAdmin/api/Get/artistSearch.js



const artistSearch = async ({
  title,
  page
}) => {
  console.log({
    cattitle: title
  });
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  return axios_orders.get(strings.ARTIST_SEARCH + "/" + title + "/" + page); // .then((artistSearch) => {
  //   // console.log({ artistSearch });
  //   returnData(artistSearch.data);
  //   // loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   // else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_artistSearch = (artistSearch);
// CONCATENATED MODULE: ./panelAdmin/api/Get/country.js



const country = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.COUNTRY;
  const axiosData = page ? strings + "/" + page : strings;
  return axios_orders.get(axiosData); // .then((country) => {
  //   console.log({ country });
  //   returnData(country.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_country = (country);
// CONCATENATED MODULE: ./panelAdmin/api/Get/countrySearch.js



const countrySearch = async (title, page, returnData) => {
  console.log({
    title,
    page
  });
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.COUNTRY;
  return axios_orders.get(strings + "/" + title + "/" + page).then(countrySearch => {
    // console.log({ countrySearch });
    returnData(countrySearch.data); // loading(false);
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ var Get_countrySearch = (countrySearch);
// CONCATENATED MODULE: ./panelAdmin/api/Get/instrument.js



const instrument = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.INSTRUMENT;
  const axiosData = page ? strings + "/" + page : strings;
  return axios_orders.get(axiosData); // .then((instrument) => {
  //   console.log({ instrument });
  //   returnData(instrument.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_instrument = (instrument);
// CONCATENATED MODULE: ./panelAdmin/api/Get/instrumentSearch.js



const instrumentSearch = async (param, page, returnData) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.INSTRUMENT;
  return axios_orders.get(strings + "/" + param + "/" + page).then(instrumentSearch => {
    // console.log({ instrumentSearch });
    returnData(instrumentSearch.data); // loading(false);
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ var Get_instrumentSearch = (instrumentSearch);
// CONCATENATED MODULE: ./panelAdmin/api/Get/artists.js



const artists = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  return axios_orders.get(strings.ARTIST + "/" + page); // .then((artists) => {
  //   console.log({ artists });
  //   // return artists;
  //   // returnData(artists.data);
  //   // loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_artists = (artists);
// CONCATENATED MODULE: ./panelAdmin/api/Get/mood.js



const mood = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.MOOD;
  return axios_orders.get(strings + "/" + page); // .then((mood) => {
  //   console.log({ mood });
  //   returnData(mood.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_mood = (mood);
// CONCATENATED MODULE: ./panelAdmin/api/Get/hashtag.js



const hashtag = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.HASHTAG;
  return axios_orders.get(strings + "/" + page); // .then((hashtag) => {
  //   console.log({ hashtag });
  //   returnData(hashtag.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_hashtag = (hashtag);
// CONCATENATED MODULE: ./panelAdmin/api/Get/albums.js



const albums = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  return axios_orders.get(strings.ALBUM + "/" + page);
};

/* harmony default export */ var Get_albums = (albums);
// CONCATENATED MODULE: ./panelAdmin/api/Get/index.js
// import categoreis from "./categoreis";
// import sliders from "./sliders";
// import Owners from "./Owners";
// import discounts from "./discounts";
// import discount from "./discount";
// import categoreisSearch from "./categoreisSearch";
// import ownersSearch from "./ownersSearch";
// import discountSearch from "./discountSearch";
 // import club from "./club";

 // import category from "./category";
// import members from "./members";
// import banners from "./banners";
// import scenarios from "./scenarios";





 // import gallery from "./gallery";
// import song from "./song";





const get = {
  instrument: Get_instrument,
  instrumentSearch: Get_instrumentSearch,
  // categoreis,
  // sliders,
  // Owners,
  // discounts,
  // categoreisSearch,
  // category,
  // ownersSearch,
  // discountSearch,
  genres: Get_genres,
  genreSearch: Get_genreSearch,
  // discount,
  // club,
  // members,
  // banners,
  // scenarios,
  artistSearch: Get_artistSearch,
  country: Get_country,
  countrySearch: Get_countrySearch,
  // gallery,
  // song,
  artists: Get_artists,
  mood: Get_mood,
  hashtag: Get_hashtag,
  albums: Get_albums
};
/* harmony default export */ var Get = (get);
// CONCATENATED MODULE: ./panelAdmin/api/Put/index.js
// import editSection from "./editSection";
// import scenario from "./scenario";
const put = {};
/* harmony default export */ var Put = (put);
// CONCATENATED MODULE: ./panelAdmin/api/Post/imageUpload.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const imageUpload = async (files, setLoading, type, setState, imageName) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  console.log({
    files,
    setLoading,
    type,
    setState,
    imageName
  });
  setLoading(true); // ============================================= const

  const CancelToken = external_axios_default.a.CancelToken;
  const source = CancelToken.source();
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total);
      setState(prev => _objectSpread({}, prev, {
        progressPercentImage: percentCompleted
      }));
    },
    cancelToken: source.token
  };
  const URL = strings.IMAGE_UPLOAD;
  const formData = new FormData();
  formData.append("imageName", imageName);
  formData.append("imageType", type);
  formData.append("image", files); //=============================================== axios

  return axios_orders.post(URL, formData, settings).then(Response => {
    console.log({
      Response
    });
    setLoading(false);
    return Response.data;
  }).catch(error => {
    console.log({
      error
    });
    setLoading(false);
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_imageUpload = (imageUpload);
// CONCATENATED MODULE: ./panelAdmin/api/Post/artist.js



const artist = async (param, setLoading) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.ARTIST;
  return axios_orders.post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_artist = (artist);
// CONCATENATED MODULE: ./panelAdmin/api/Post/voiceUpload.js
function voiceUpload_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function voiceUpload_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { voiceUpload_ownKeys(Object(source), true).forEach(function (key) { voiceUpload_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { voiceUpload_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function voiceUpload_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const voiceUpload = async (files, setLoading, type, setState, songName) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.strings;
  setLoading(true);
  console.log({
    files,
    setLoading,
    type,
    setState,
    songName
  }); // ============================================= const

  const CancelToken = external_axios_default.a.CancelToken;
  const source = CancelToken.source();
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total);
      setState(prev => voiceUpload_objectSpread({}, prev, {
        progressPercentSongs: percentCompleted
      }));
    },
    cancelToken: source.token
  };
  const formData = new FormData();
  formData.append("songName", songName);
  formData.append("song", files);
  const URL = strings.ApiString.SONG_UPLOAD;

  for (var pair of formData.entries()) {
    console.log(pair[0] + ", " + pair[1]);
  } // ============================================== End const ==============================================
  // ============================================== log
  //console.log("Sending ...");
  //console.log({ AxiosCancelUpload: cancelUpload });


  console.log({
    formData
  }); //console.log({ update });
  // ============================================== End log ==============================================
  // if (cancelUpload) {
  //   source.cancel();
  // }
  //=============================================== axios

  return axios_orders.post(URL, formData, settings).then(Response => {
    console.log({
      Response
    });
    setLoading(false); // update[name] = Response.data.voiceUrl;
    // return Response.data.songUrl;

    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_voiceUpload = (voiceUpload);
// CONCATENATED MODULE: ./panelAdmin/api/Post/videoUpload.js
function videoUpload_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function videoUpload_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { videoUpload_ownKeys(Object(source), true).forEach(function (key) { videoUpload_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { videoUpload_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function videoUpload_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const videoUpload = async (files, setLoading, type, setState, cancelUpload) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  let Strings = panelAdmin["a" /* default */].values.ApiString; // //console.log({ update });

  setLoading(true); // ============================================= const

  const CancelToken = external_axios_default.a.CancelToken;
  const source = CancelToken.source();
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total);
      setState(prev => videoUpload_objectSpread({}, prev, {
        progressPercentVideo: percentCompleted
      }));
    },
    cancelToken: source.token
  };
  const formData = new FormData();
  const URL = Strings.ApiString.VIDEO_UPLOAD;
  formData.append("video", files); // ============================================== End const ==============================================
  // ============================================== log
  //console.log("Sending ...");
  //console.log({ AxiosCancelUpload: cancelUpload });
  //console.log({ formData });
  // ============================================== End log ==============================================
  // if (cancelUpload) {
  //   source.cancel();
  // }
  //=============================================== axios

  return axios_orders.post(URL, formData, settings).then(Response => {
    console.log({
      Response
    });
    setLoading(false);
    return Response.data.videoUrl; // update[name] = Response.data.videoUrl;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_videoUpload = (videoUpload);
// CONCATENATED MODULE: ./panelAdmin/api/Post/genres.js



const genres_genres = async param => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.GENRES;
  return axios_orders.post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_genres = (genres_genres);
// CONCATENATED MODULE: ./panelAdmin/api/Post/country.js



const country_country = async param => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.COUNTRY;
  return axios_orders.post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_country = (country_country);
// CONCATENATED MODULE: ./panelAdmin/api/Post/instrument.js



const instrument_instrument = async (param, setLoading) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.INSTRUMENT;
  return axios_orders.post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_instrument = (instrument_instrument);
// CONCATENATED MODULE: ./panelAdmin/api/Post/mood.js



const mood_mood = async param => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.MOOD;
  return axios_orders.post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_mood = (mood_mood);
// CONCATENATED MODULE: ./panelAdmin/api/Post/hashtag.js



const hashtag_hashtag = async param => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.HASHTAG;
  return axios_orders.post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_hashtag = (hashtag_hashtag);
// CONCATENATED MODULE: ./panelAdmin/api/Post/index.js
 // import owner from "./owner";
// import category from "./category";
// import slider from "./slider";
// import login from "./login";
// import album from "./album";
// import club from "./club";
// import banner from "./banner";








 // import gallery from "./gallery";

const post = {
  imageUpload: Post_imageUpload,
  // owner,
  // category,
  // slider,
  // login,
  // album,
  // club,
  // banner,
  artist: Post_artist,
  voiceUpload: Post_voiceUpload,
  videoUpload: Post_videoUpload,
  genres: Post_genres,
  country: Post_country,
  instrument: Post_instrument,
  // gallery,
  mood: Post_mood,
  hashtag: Post_hashtag
};
/* harmony default export */ var Post = (post);
// CONCATENATED MODULE: ./panelAdmin/api/Patch/index.js
// import category from "./category";
// import editDiscount from "./discount";
// import club from "./club";
// import owner from "./owner";
// import slider from "./slider";
// import banner from "./banner";
const Patch = {// category,
  // editDiscount,
  // club,
  // owner,
  // slider,
  // banner,
};
/* harmony default export */ var api_Patch = (Patch);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/index.js
// import owner from "./owner";
// import discount from "./discount";
// import club from "./club";
// import slider from "./slider";
// import banner from "./banner";
// import category from "./category";
const Delete = {};
/* harmony default export */ var api_Delete = (Delete);
// CONCATENATED MODULE: ./panelAdmin/api/index.js







/***/ }),

/***/ "HJQg":
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ }),

/***/ "IZS3":
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ }),

/***/ "Ol4B":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _BackgrandCover__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("7NiY");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const ModalBox = props => {
  const {
    showModal,
    onHideModal
  } = props;
  const {
    0: modalHide,
    1: setModalHide
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(true);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    if (showModal) {
      setModalHide(false);
    }
  }, [showModal]);

  const endAnimation = () => {
    if (!showModal) {
      onHideModal();
      setModalHide(true);
    }
  };

  return __jsx("div", {
    className: "modal_container-box",
    style: {
      display: modalHide ? "none" : ""
    }
  }, __jsx("div", {
    onAnimationEnd: endAnimation,
    id: "subjectModal",
    className: showModal ? "fadeIn" : "fadeOut"
  }, __jsx("div", {
    className: "subjectModal-box"
  }, props.children)), __jsx(_BackgrandCover__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], {
    onClick: onHideModal,
    fadeIn: !modalHide
  }));
};

/* harmony default export */ __webpack_exports__["a"] = (ModalBox);

/***/ }),

/***/ "RfFk":
/***/ (function(module, exports) {

module.exports = require("react-perfect-scrollbar");

/***/ }),

/***/ "RmXt":
/***/ (function(module, exports) {

module.exports = require("redux-saga/effects");

/***/ }),

/***/ "TI7Q":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("IZS3");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const DropdownBoot = props => {
  const {
    accepted,
    dropDownData,
    value,
    className,
    checkSubmited,
    disabled
  } = props;
  let index = dropDownData && dropDownData.findIndex(d => d.value === value);
  let propsVal;
  if (index >= 0) propsVal = dropDownData[index].title;
  const {
    0: Title,
    1: setTitle
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");

  const clickedElement = (value, title) => {
    if (value) accepted(value, title);
    if (title) setTitle(title);
  };

  return __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"], null, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Toggle, {
    variant: "success",
    id: "dropdown-basic"
  }, Title ? Title : propsVal ? propsVal : value ? value : "انتخاب نمایید  "), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Menu, null, dropDownData && dropDownData.map((info, index) => {
    // console.log({ value, infoval: info });
    return __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Item, {
      key: index + "boot",
      active: value === info.title,
      onClick: () => clickedElement(info.value, info.title)
    }, info.title);
  })));
};

/* harmony default export */ __webpack_exports__["a"] = (DropdownBoot);

/***/ }),

/***/ "VGcP":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// NAMESPACE OBJECT: ./panelAdmin/storeAdmin/actions/redux/index.js
var actions_redux_namespaceObject = {};
__webpack_require__.r(actions_redux_namespaceObject);
__webpack_require__.d(actions_redux_namespaceObject, "setPageName", function() { return setPageName; });
__webpack_require__.d(actions_redux_namespaceObject, "setUploadImage", function() { return setUploadImage; });
__webpack_require__.d(actions_redux_namespaceObject, "setGalleryData", function() { return redux_setGalleryData; });
__webpack_require__.d(actions_redux_namespaceObject, "changeAddGalleryData", function() { return changeAddGalleryData; });
__webpack_require__.d(actions_redux_namespaceObject, "setArtistData", function() { return setArtistData; });
__webpack_require__.d(actions_redux_namespaceObject, "setSearchArtistData", function() { return setSearchArtistData; });
__webpack_require__.d(actions_redux_namespaceObject, "startGetArtist", function() { return startGetArtist; });
__webpack_require__.d(actions_redux_namespaceObject, "startSearchArtist", function() { return startSearchArtist; });
__webpack_require__.d(actions_redux_namespaceObject, "setInstrumentData", function() { return setInstrumentData; });
__webpack_require__.d(actions_redux_namespaceObject, "setSearchInstrumentData", function() { return setSearchInstrumentData; });
__webpack_require__.d(actions_redux_namespaceObject, "startGetInstrument", function() { return startGetInstrument; });
__webpack_require__.d(actions_redux_namespaceObject, "startSearchInstrument", function() { return startSearchInstrument; });
__webpack_require__.d(actions_redux_namespaceObject, "setCountryData", function() { return setCountryData; });
__webpack_require__.d(actions_redux_namespaceObject, "setSearchCountryData", function() { return setSearchCountryData; });
__webpack_require__.d(actions_redux_namespaceObject, "startGetCountry", function() { return startGetCountry; });
__webpack_require__.d(actions_redux_namespaceObject, "startSearchCountry", function() { return startSearchCountry; });
__webpack_require__.d(actions_redux_namespaceObject, "setMoodData", function() { return setMoodData; });
__webpack_require__.d(actions_redux_namespaceObject, "setSearchMoodData", function() { return setSearchMoodData; });
__webpack_require__.d(actions_redux_namespaceObject, "startGetMood", function() { return startGetMood; });
__webpack_require__.d(actions_redux_namespaceObject, "startSearchMood", function() { return startSearchMood; });
__webpack_require__.d(actions_redux_namespaceObject, "setHashtagData", function() { return setHashtagData; });
__webpack_require__.d(actions_redux_namespaceObject, "setSearchHashtagData", function() { return setSearchHashtagData; });
__webpack_require__.d(actions_redux_namespaceObject, "startGetHashtag", function() { return startGetHashtag; });
__webpack_require__.d(actions_redux_namespaceObject, "startSearchHashtag", function() { return startSearchHashtag; });
__webpack_require__.d(actions_redux_namespaceObject, "setGenreData", function() { return setGenreData; });
__webpack_require__.d(actions_redux_namespaceObject, "setSearchGenreData", function() { return setSearchGenreData; });
__webpack_require__.d(actions_redux_namespaceObject, "startGetGenre", function() { return startGetGenre; });
__webpack_require__.d(actions_redux_namespaceObject, "startSearchGenre", function() { return startSearchGenre; });

// NAMESPACE OBJECT: ./panelAdmin/storeAdmin/actions/saga/index.js
var actions_saga_namespaceObject = {};
__webpack_require__.r(actions_saga_namespaceObject);
__webpack_require__.d(actions_saga_namespaceObject, "uploadImageData", function() { return uploadImageData; });
__webpack_require__.d(actions_saga_namespaceObject, "getGalleryData", function() { return getGalleryData; });
__webpack_require__.d(actions_saga_namespaceObject, "getArtistData", function() { return getArtistData; });
__webpack_require__.d(actions_saga_namespaceObject, "getSearchArtistData", function() { return getSearchArtistData; });
__webpack_require__.d(actions_saga_namespaceObject, "getInstrumentData", function() { return getInstrumentData; });
__webpack_require__.d(actions_saga_namespaceObject, "getSearchInstrumentData", function() { return getSearchInstrumentData; });
__webpack_require__.d(actions_saga_namespaceObject, "getCountryData", function() { return getCountryData; });
__webpack_require__.d(actions_saga_namespaceObject, "getSearchCountryData", function() { return getSearchCountryData; });
__webpack_require__.d(actions_saga_namespaceObject, "getMoodData", function() { return getMoodData; });
__webpack_require__.d(actions_saga_namespaceObject, "getSearchMoodData", function() { return getSearchMoodData; });
__webpack_require__.d(actions_saga_namespaceObject, "getAlbumData", function() { return getAlbumData; });
__webpack_require__.d(actions_saga_namespaceObject, "getSearchAlbumData", function() { return getSearchAlbumData; });
__webpack_require__.d(actions_saga_namespaceObject, "getHashtagData", function() { return getHashtagData; });
__webpack_require__.d(actions_saga_namespaceObject, "getSearchHashtagData", function() { return getSearchHashtagData; });
__webpack_require__.d(actions_saga_namespaceObject, "getGenreData", function() { return getGenreData; });
__webpack_require__.d(actions_saga_namespaceObject, "getSearchGenreData", function() { return getSearchGenreData; });

// NAMESPACE OBJECT: ./panelAdmin/storeAdmin/saga/index.js
var storeAdmin_saga_namespaceObject = {};
__webpack_require__.r(storeAdmin_saga_namespaceObject);
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchUploadImage", function() { return watchUploadImage; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGallery", function() { return watchGallery; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGetArtistData", function() { return watchGetArtistData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchSearchArtistData", function() { return watchSearchArtistData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGetInstrumentalData", function() { return watchGetInstrumentalData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchSearchInstrumentalData", function() { return watchSearchInstrumentalData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGetCountryData", function() { return watchGetCountryData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchSearchCountryData", function() { return watchSearchCountryData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGetMoodData", function() { return watchGetMoodData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchSearchMoodData", function() { return watchSearchMoodData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGetHashtagData", function() { return watchGetHashtagData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchSearchHashtagData", function() { return watchSearchHashtagData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGetGenreData", function() { return watchGetGenreData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchSearchGenreData", function() { return watchSearchGenreData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchGetAlbumData", function() { return watchGetAlbumData; });
__webpack_require__.d(storeAdmin_saga_namespaceObject, "watchSearchAlbumData", function() { return watchSearchAlbumData; });

// CONCATENATED MODULE: ./panelAdmin/values/strings/en/navbar.js
const APP_NAME = "Rimtal";
const EXPLORE = "Explore";
const TRACKS = "Tracks";
const PLAYLISTS = "Playlists";
const ALBUMS = "Albums";
const ARTISTS = "Artists";
const VIDEOS = "Videos";
const SIGN_IN = "Sign In";
const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN
};
/* harmony default export */ var en_navbar = (navbar);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/sideMenu.js
const DASHBOARD = "داشبورد";
const SIDEBAR_ONE_TITLE = "عمومی";
const SIDEBAR_TWO_TITLE = "کاربردی";
const SETTING_WEB = "تنظیمات سایت";
const CATEGORIES = "دسته بندی ها";
const SEE_ARTIST = "مشاهده هنرمندان";
const ARTIST = "هنرمند";
const ADD_ARTIST = "افزودن هنرمند";
const SEE_ALBUM = "مشاهده آلبوم ها";
const ALBUM = "آلبوم";
const ADD_ALBUM = "افزودن آلبوم";
const SEE_GENRES = "مشاهده ژانر ها";
const GENRES = "ژانر";
const ADD_GENRE = "افزودن ژانر";
const SEE_COUNTRY = "مشاهده کشور ها";
const COUNTRY = "کشور";
const ADD_COUNTRY = "افزودن کشور";
const SEE_INSTRUMENT = "مشاهده ساز ها";
const INSTRUMENT = "ساز";
const ADD_INSTRUMENT = "افزودن ساز";
const SEE_GALLERY = "مشاهده گالری ها";
const GALLERY = "گالری";
const SEE_SONG = "مشاهده آهنگ ها";
const SONG = "آهنگ";
const SEE_MOOD = "مشاهده حالت ها";
const MOOD = "حالت";
const sideMenu = {
  SEE_GALLERY,
  GALLERY,
  DASHBOARD,
  SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE,
  SETTING_WEB,
  CATEGORIES,
  SEE_ARTIST,
  ARTIST,
  ADD_ARTIST,
  ALBUM,
  SEE_ALBUM,
  ADD_ALBUM,
  SEE_GENRES,
  GENRES,
  ADD_GENRE,
  SEE_COUNTRY,
  COUNTRY,
  ADD_COUNTRY,
  SEE_INSTRUMENT,
  INSTRUMENT,
  ADD_INSTRUMENT,
  SEE_SONG,
  SONG,
  SEE_MOOD,
  MOOD
};
/* harmony default export */ var en_sideMenu = (sideMenu);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/global.js
const global_TRACKS = "Tracks";
const NO_ENTRIES = "no entries";
const FOLLOWERS = "followers";
const global = {
  TRACKS: global_TRACKS,
  NO_ENTRIES: NO_ENTRIES,
  FOLLOWERS
};
/* harmony default export */ var en_global = (global);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/constants.js
const ALBUM_CONSTANTS = "album";
const PLAY_LIST_CONSTANTS = "playlist";
const SONG_CONSTANTS = "song";
const FLAG_CONSTANTS = "flag";
const ARTIST_CONSTANTS = "artist";
const INSTRUMENT_CONSTANTS = "instrument";
const MUSIC_VIDEO_CONSTANTS = "musicvideo";
const MOOD_CONSTANTS = "mood";
const constants = {
  ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS,
  FLAG_CONSTANTS,
  ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS
};
/* harmony default export */ var en_constants = (constants);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/index.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const en = _objectSpread({}, en_navbar, {}, en_sideMenu, {}, en_global, {}, en_constants);

/* harmony default export */ var strings_en = (en);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/navbar.js
const navbar_APP_NAME = "ریمتال";
const navbar_EXPLORE = "کاوش کردن";
const navbar_TRACKS = "آهنگ ها";
const navbar_PLAYLISTS = "لیست های پخش";
const navbar_ALBUMS = "آلبوم ها";
const navbar_ARTISTS = "هنرمندان";
const navbar_VIDEOS = "ویدیو ها";
const navbar_SIGN_IN = "ورود";
const navbar_navbar = {
  APP_NAME: navbar_APP_NAME,
  EXPLORE: navbar_EXPLORE,
  TRACKS: navbar_TRACKS,
  PLAYLISTS: navbar_PLAYLISTS,
  ALBUMS: navbar_ALBUMS,
  ARTISTS: navbar_ARTISTS,
  VIDEOS: navbar_VIDEOS,
  SIGN_IN: navbar_SIGN_IN
};
/* harmony default export */ var fa_navbar = (navbar_navbar);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/sideMenu.js
const sideMenu_DASHBOARD = "داشبورد";
const sideMenu_SIDEBAR_ONE_TITLE = "عمومی";
const sideMenu_SIDEBAR_TWO_TITLE = "کاربردی";
const sideMenu_SETTING_WEB = "تنظیمات سایت";
const sideMenu_CATEGORIES = "دسته بندی ها";
const sideMenu_SEE_ARTIST = "مشاهده هنرمندان";
const sideMenu_ARTIST = "هنرمند";
const sideMenu_ADD_ARTIST = "افزودن هنرمند";
const sideMenu_SEE_ALBUM = "مشاهده آلبوم ها";
const sideMenu_ALBUM = "آلبوم";
const sideMenu_ADD_ALBUM = "افزودن آلبوم";
const sideMenu_SEE_GENRES = "مشاهده ژانر ها";
const sideMenu_GENRES = "ژانر";
const sideMenu_ADD_GENRE = "افزودن ژانر";
const sideMenu_SEE_COUNTRY = "مشاهده کشور ها";
const sideMenu_COUNTRY = "کشور";
const sideMenu_ADD_COUNTRY = "افزودن کشور";
const sideMenu_SEE_INSTRUMENT = "مشاهده ساز ها";
const sideMenu_INSTRUMENT = "ساز";
const sideMenu_ADD_INSTRUMENT = "افزودن ساز";
const SEE_GALLERIES = "مشاهده گالری ها";
const sideMenu_GALLERY = "گالری";
const sideMenu_SEE_SONG = "مشاهده آهنگ ها";
const sideMenu_SONG = "آهنگ";
const sideMenu_SEE_MOOD = "مشاهده حالت ها";
const sideMenu_MOOD = "حالت";
const ADD_MOOD = "افزودن حالت";
const SEE_HASHTAG = "مشاهده هشتگ ها";
const HASHTAG = "هشتگ";
const ADD_HASHTAG = "افزودن هشتگ";
const sideMenu_sideMenu = {
  SEE_GALLERIES,
  GALLERY: sideMenu_GALLERY,
  DASHBOARD: sideMenu_DASHBOARD,
  SIDEBAR_ONE_TITLE: sideMenu_SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE: sideMenu_SIDEBAR_TWO_TITLE,
  SETTING_WEB: sideMenu_SETTING_WEB,
  CATEGORIES: sideMenu_CATEGORIES,
  SEE_ARTIST: sideMenu_SEE_ARTIST,
  ARTIST: sideMenu_ARTIST,
  ADD_ARTIST: sideMenu_ADD_ARTIST,
  ALBUM: sideMenu_ALBUM,
  SEE_ALBUM: sideMenu_SEE_ALBUM,
  ADD_ALBUM: sideMenu_ADD_ALBUM,
  SEE_GENRES: sideMenu_SEE_GENRES,
  GENRES: sideMenu_GENRES,
  ADD_GENRE: sideMenu_ADD_GENRE,
  SEE_COUNTRY: sideMenu_SEE_COUNTRY,
  COUNTRY: sideMenu_COUNTRY,
  ADD_COUNTRY: sideMenu_ADD_COUNTRY,
  SEE_INSTRUMENT: sideMenu_SEE_INSTRUMENT,
  INSTRUMENT: sideMenu_INSTRUMENT,
  ADD_INSTRUMENT: sideMenu_ADD_INSTRUMENT,
  SEE_SONG: sideMenu_SEE_SONG,
  SONG: sideMenu_SONG,
  SEE_MOOD: sideMenu_SEE_MOOD,
  MOOD: sideMenu_MOOD,
  ADD_MOOD,
  SEE_HASHTAG,
  HASHTAG,
  ADD_HASHTAG
};
/* harmony default export */ var fa_sideMenu = (sideMenu_sideMenu);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/global.js
const fa_global_TRACKS = "آهنگ ها ";
const global_NO_ENTRIES = "وارد نشده";
const global_FOLLOWERS = "دنبال کنندگان";
const global_global = {
  TRACKS: fa_global_TRACKS,
  NO_ENTRIES: global_NO_ENTRIES,
  FOLLOWERS: global_FOLLOWERS
};
/* harmony default export */ var fa_global = (global_global);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/constants.js
const constants_ALBUM_CONSTANTS = "آلبوم";
const constants_PLAY_LIST_CONSTANTS = "لیست پخش";
const constants_SONG_CONSTANTS = "موزیک";
const constants_FLAG_CONSTANTS = "پرچم";
const constants_ARTIST_CONSTANTS = "هنرمند";
const constants_INSTRUMENT_CONSTANTS = "ساز";
const constants_MUSIC_VIDEO_CONSTANTS = "موزیک ویدئو";
const constants_MOOD_CONSTANTS = "حالت";
const constants_constants = {
  ALBUM_CONSTANTS: constants_ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS: constants_PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS: constants_SONG_CONSTANTS,
  FLAG_CONSTANTS: constants_FLAG_CONSTANTS,
  ARTIST_CONSTANTS: constants_ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS: constants_INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS: constants_MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS: constants_MOOD_CONSTANTS
};
/* harmony default export */ var fa_constants = (constants_constants);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/index.js
function fa_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function fa_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { fa_ownKeys(Object(source), true).forEach(function (key) { fa_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { fa_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function fa_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const fa = fa_objectSpread({}, fa_navbar, {}, fa_sideMenu, {}, fa_global, {}, fa_constants);

/* harmony default export */ var strings_fa = (fa);
// CONCATENATED MODULE: ./panelAdmin/values/strings/index.js


const Lang = "rtl";
let strings_strings;
if (Lang === "rtl") strings_strings = strings_fa;else strings_strings = strings_en;
/* harmony default export */ var values_strings = (strings_strings);
// CONCATENATED MODULE: ./panelAdmin/values/routes/index.js
const GS_PANEL_ADMIN_TITLE = "/panelAdmin";
const GS_ADMIN_DASHBOARD = GS_PANEL_ADMIN_TITLE + "/dashboard";
const GS_ADMIN_ARTIST = GS_PANEL_ADMIN_TITLE + "/artist";
const GS_ADMIN_ADD_ARTIST = GS_PANEL_ADMIN_TITLE + "/addArtist";
const GS_ADMIN_ALBUM = GS_PANEL_ADMIN_TITLE + "/album";
const GS_ADMIN_ADD_ALBUM = GS_PANEL_ADMIN_TITLE + "/addAlbum";
const GS_ADMIN_GENRE = GS_PANEL_ADMIN_TITLE + "/genre";
const GS_ADMIN_ADD_GENRE = GS_PANEL_ADMIN_TITLE + "/addGenre";
const GS_ADMIN_COUNTRY = GS_PANEL_ADMIN_TITLE + "/country";
const GS_ADMIN_ADD_COUNTRY = GS_PANEL_ADMIN_TITLE + "/addCountry";
const GS_ADMIN_INSTRUMENT = GS_PANEL_ADMIN_TITLE + "/instrument";
const GS_ADMIN_ADD_INSTRUMENT = GS_PANEL_ADMIN_TITLE + "/addInstrument";
const GS_ADMIN_GALLERY = GS_PANEL_ADMIN_TITLE + "/gallery";
const GS_ADMIN_SONG = GS_PANEL_ADMIN_TITLE + "/song";
const GS_ADMIN_MOOD = GS_PANEL_ADMIN_TITLE + "/mood";
const GS_ADMIN_ADD_MOOD = GS_PANEL_ADMIN_TITLE + "/addMood";
const GS_ADMIN_HASHTAG = GS_PANEL_ADMIN_TITLE + "/hashtag";
const GS_ADMIN_ADD_HASHTAG = GS_PANEL_ADMIN_TITLE + "/addHashtag"; // const GS_ADMIN_GALLERY = GS_PANEL_ADMIN_TITLE + "/gallery";
// const GS_ADMIN_ADD_GALLERY = GS_PANEL_ADMIN_TITLE + "/addGallery";

const routes = {
  GS_ADMIN_DASHBOARD,
  GS_PANEL_ADMIN_TITLE,
  GS_ADMIN_ARTIST,
  GS_ADMIN_ADD_ARTIST,
  GS_ADMIN_ALBUM,
  GS_ADMIN_ADD_ALBUM,
  GS_ADMIN_GENRE,
  GS_ADMIN_ADD_GENRE,
  GS_ADMIN_COUNTRY,
  GS_ADMIN_ADD_COUNTRY,
  GS_ADMIN_INSTRUMENT,
  GS_ADMIN_ADD_INSTRUMENT,
  GS_ADMIN_GALLERY,
  GS_ADMIN_SONG,
  GS_ADMIN_MOOD,
  GS_ADMIN_ADD_MOOD,
  GS_ADMIN_HASHTAG,
  GS_ADMIN_ADD_HASHTAG
};
/* harmony default export */ var values_routes = (routes);
// CONCATENATED MODULE: ./panelAdmin/values/apiString.js
const ADMIN = "/admin";
const SONG_UPLOAD = ADMIN + "/songupload";
const UPLOAD = ADMIN + "/upload";
const apiString_ALBUM = ADMIN + "/album";
const LOGIN = ADMIN + "/login";
const IMAGE_UPLOAD = ADMIN + "/upload/image";
const apiString_GENRES = ADMIN + "/genre";
const apiString_ARTIST = ADMIN + "/artist";
const ARTIST_SEARCH = apiString_ARTIST + "/search";
const apiString_COUNTRY = ADMIN + "/country";
const apiString_INSTRUMENT = ADMIN + "/instrument";
const apiString_GALLERY = ADMIN + "/gallery";
const apiString_SONG = ADMIN + "/songlibrary";
const apiString_MOOD = ADMIN + "/mood";
const apiString_HASHTAG = ADMIN + "/hashtag";
const apiString = {
  SONG_UPLOAD,
  ALBUM: apiString_ALBUM,
  LOGIN,
  UPLOAD,
  IMAGE_UPLOAD,
  GENRES: apiString_GENRES,
  ARTIST_SEARCH,
  COUNTRY: apiString_COUNTRY,
  INSTRUMENT: apiString_INSTRUMENT,
  GALLERY: apiString_GALLERY,
  SONG: apiString_SONG,
  ARTIST: apiString_ARTIST,
  MOOD: apiString_MOOD,
  HASHTAG: apiString_HASHTAG
};
/* harmony default export */ var values_apiString = (apiString);
// CONCATENATED MODULE: ./panelAdmin/values/strings/constants.js
const strings_constants_ALBUM_CONSTANTS = "album";
const strings_constants_PLAY_LIST_CONSTANTS = "playlist";
const strings_constants_SONG_CONSTANTS = "song";
const strings_constants_FLAG_CONSTANTS = "flag";
const strings_constants_ARTIST_CONSTANTS = "artist";
const strings_constants_INSTRUMENT_CONSTANTS = "instrument";
const strings_constants_MUSIC_VIDEO_CONSTANTS = "musicvideo";
const strings_constants_MOOD_CONSTANTS = "mood";
const strings_constants_constants = {
  ALBUM_CONSTANTS: strings_constants_ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS: strings_constants_PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS: strings_constants_SONG_CONSTANTS,
  FLAG_CONSTANTS: strings_constants_FLAG_CONSTANTS,
  ARTIST_CONSTANTS: strings_constants_ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS: strings_constants_INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS: strings_constants_MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS: strings_constants_MOOD_CONSTANTS
};
/* harmony default export */ var strings_constants = (strings_constants_constants);
// CONCATENATED MODULE: ./panelAdmin/values/index.js






const values = {
  routes: values_routes,
  strings: values_strings,
  apiString: values_apiString,
  constants: strings_constants,
  fa: strings_fa,
  en: strings_en
};
/* harmony default export */ var panelAdmin_values = (values);
// CONCATENATED MODULE: ./panelAdmin/utils/menuFormat.js


const menuFormat = [{
  title: "عمومی",
  menus: [{
    route: panelAdmin_values.routes.GS_ADMIN_DASHBOARD,
    menuTitle: panelAdmin_values.strings.DASHBOARD,
    menuIconImg: false,
    menuIconClass: "fas fa-tachometer-slowest",
    subMenu: [// { title: "SubDashboard", route: "/dashboard1" },
      // { title: "SubDashboard", route: "/dashboard2" }
    ]
  }]
}, {
  title: "کاربردی",
  menus: [{
    route: false,
    menuTitle: panelAdmin_values.strings.ARTIST,
    menuIconImg: false,
    menuIconClass: "fas fa-user-music",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_ARTIST,
      route: panelAdmin_values.routes.GS_ADMIN_ARTIST
    }, {
      title: panelAdmin_values.strings.ADD_ARTIST,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_ARTIST
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.ALBUM,
    menuIconImg: false,
    menuIconClass: "fas fa-album",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_ALBUM,
      route: panelAdmin_values.routes.GS_ADMIN_ALBUM
    }, {
      title: panelAdmin_values.strings.ADD_ALBUM,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_ALBUM
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.GENRES,
    menuIconImg: false,
    menuIconClass: "fas fa-album",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_GENRES,
      route: panelAdmin_values.routes.GS_ADMIN_GENRE
    }, {
      title: panelAdmin_values.strings.ADD_GENRE,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_GENRE
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.COUNTRY,
    menuIconImg: false,
    menuIconClass: "fas fa-flag",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_COUNTRY,
      route: panelAdmin_values.routes.GS_ADMIN_COUNTRY
    }, {
      title: panelAdmin_values.strings.ADD_COUNTRY,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_COUNTRY
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.INSTRUMENT,
    menuIconImg: false,
    menuIconClass: "fas fa-saxophone",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_INSTRUMENT,
      route: panelAdmin_values.routes.GS_ADMIN_INSTRUMENT
    }, {
      title: panelAdmin_values.strings.ADD_INSTRUMENT,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_INSTRUMENT
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.GALLERY,
    menuIconImg: false,
    menuIconClass: "fad fa-images",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_GALLERIES,
      route: panelAdmin_values.routes.GS_ADMIN_GALLERY
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.SONG,
    menuIconImg: false,
    menuIconClass: "fad fa-comment-music",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_SONG,
      route: panelAdmin_values.routes.GS_ADMIN_SONG
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.MOOD,
    menuIconImg: false,
    menuIconClass: "fas fa-album",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_MOOD,
      route: panelAdmin_values.routes.GS_ADMIN_MOOD
    }, {
      title: panelAdmin_values.strings.ADD_MOOD,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_MOOD
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.HASHTAG,
    menuIconImg: false,
    menuIconClass: "fas fa-album",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_HASHTAG,
      route: panelAdmin_values.routes.GS_ADMIN_HASHTAG
    }, {
      title: panelAdmin_values.strings.ADD_HASHTAG,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_HASHTAG
    }]
  }]
}];
/* harmony default export */ var utils_menuFormat = (menuFormat);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/genres.js


const genres = data => {
  const thead = ["#", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_genres = (genres);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowScenario.js
var __jsx = external_react_default.a.createElement;


const ShowScenario = scenario => {
  const thead = ["#", "نام", "تاریخ شروع ", " تاریخ پایان", "تعداد برندگان ", "شركت كنندگان", "برندگان", "جوایز", "پوچ ها", "زمان چرخش مجدد (روز)", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < scenario.length; index++) {
    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    tbody.push({
      data: [scenario[index].name, scenario[index].startDate, scenario[index].endDate, scenario[index].winnersCount, {
        option: {
          eye: true,
          name: "participants"
        }
      }, {
        option: {
          eye: true,
          name: "winners"
        }
      }, {
        option: {
          eye: true,
          name: "gifts"
        }
      }, {
        option: {
          eye: true,
          name: "empty"
        }
      }, scenario[index].spinRepeatTime, scenario[index].isActive ? active : deActive, {
        option: {
          edit: true
        }
      }],
      style: {
        background: scenario[index].isActive ? "rgb(100, 221, 23,0.3)" : ""
      }
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowScenario = (ShowScenario);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowScenarioDataInModal.js


const ShowScenarioDataInModal = (data, name) => {
  const NotEntered = "وارد نشده";
  let thead = null;
  const headGift = ["شماره", "نام", "شماره همراه", "جایزه", " تاریخ "];
  const headNotGift = ["شماره", "نام", "شماره همراه", " تاریخ "];
  name === "participants" ? thead = headNotGift : thead = headGift;
  let tbody = [];
  let body = null;

  for (let index = 0; index < data.length; index++) {
    let fullName = data[index].fullName ? data[index].fullName : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let gift = data[index].gift ? data[index].gift : NotEntered;
    let date = data[index].date ? data[index].date : NotEntered;
    const bodyGift = [fullName, phoneNumber, gift, date];
    const bodyNotGift = [fullName, phoneNumber, date];
    name === "participants" ? body = bodyNotGift : body = bodyGift;
    tbody.push({
      data: body,
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowScenarioDataInModal = (ShowScenarioDataInModal);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowScenarioDataInModalTwo.js


const ShowScenarioDataInModalTwo = (data, name) => {
  const NotEntered = "وارد نشده";
  const thead = ["شماره", name];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    tbody.push({
      data: [data[index] ? data[index] : NotEntered],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowScenarioDataInModalTwo = (ShowScenarioDataInModalTwo);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowOwner.js
var ShowOwner_jsx = external_react_default.a.createElement;


const ShowOwner = scenario => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "محدوده  ", " شماره همراه", "موجودی", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < scenario.length; index++) {
    let NotEntered = "وارد نشده";

    let active = ShowOwner_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = ShowOwner_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let thumbnail = scenario[index].thumbnail ? scenario[index].thumbnail : NotEntered;
    let title = scenario[index].title ? scenario[index].title : NotEntered;
    let rating = scenario[index].rating ? scenario[index].rating : NotEntered;
    let district = scenario[index].district ? scenario[index].district : NotEntered;
    let phoneNumber = scenario[index].phoneNumber ? scenario[index].phoneNumber : NotEntered;
    let balance = scenario[index].balance ? scenario[index].balance : "0";
    let isActive = scenario[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, district, phoneNumber, balance, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowOwner = (ShowOwner);
// CONCATENATED MODULE: ./panelAdmin/utils/formatMoney.js
const formatMoney = number => {
  return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

/* harmony default export */ var utils_formatMoney = (formatMoney);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowDiscount.js
var ShowDiscount_jsx = external_react_default.a.createElement;



const ShowDiscount = data => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "دسته بندی  ", "قیمت اصلی", "قیمت جدید", " تخفیف", "فروش", "بازدید ", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = ShowDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = ShowDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let realPrice = data[index].realPrice ? utils_formatMoney(data[index].realPrice) : NotEntered;
    let newPrice = data[index].newPrice ? utils_formatMoney(data[index].newPrice) : NotEntered;
    let rating = data[index].rating ? data[index].rating : 0;
    let percent = data[index].percent ? "%" + data[index].percent : NotEntered;
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : NotEntered;
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, categoryTitleFa, realPrice, newPrice, percent, boughtCount, viewCount, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowDiscount = (ShowDiscount);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowClub.js
var ShowClub_jsx = external_react_default.a.createElement;


const ShowClub = data => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "عضویت ", "دسته بندی ", " تخفیف", "فروش", "بازدید ", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let cardElement = ShowClub_jsx("i", {
      style: {
        fontSize: "1.2em"
      },
      className: "fal fa-credit-card"
    });

    let applicationElement = ShowClub_jsx("i", {
      style: {
        fontSize: "1.2em"
      },
      className: "fal fa-mobile-android"
    });

    let active = ShowClub_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = ShowClub_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let NotEntered = "وارد نشده";
    let thumbnail = data[index].slides[0] ? data[index].slides[0] : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : 0;
    let percent = data[index].percent ? data[index].percent + " %" : NotEntered;
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : "";
    let membership = data[index].membership.length ? data[index].membership : NotEntered;
    let membershipData = membership.includes("CARD") && membership.includes("APPLICATION") ? ShowClub_jsx("div", {
      style: {
        fontSize: "1.2em",
        fontWeight: "900",
        display: "flex",
        justifyContent: "space-around"
      }
    }, " ", ShowClub_jsx("div", null, cardElement, " "), ShowClub_jsx("div", null, " ", applicationElement)) : membership.includes("APPLICATION") ? applicationElement : membership.includes("CARD") ? cardElement : "";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, membershipData, categoryTitleFa, percent, boughtCount, viewCount, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowClub = (ShowClub);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowTransactions.js
var ShowTransactions_jsx = external_react_default.a.createElement;



const ShowTransactions = data => {
  const thead = ["#", "نام", " شماره همراه ", " تاریخ (زمان) ", " کل مبلغ", "تخفیف", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let NotEntered = "وارد نشده";
    let user = data[index].user ? data[index].user : NotEntered;
    let userName = user ? user.name : NotEntered;
    let userPhoneNumber = user ? user.phoneNumber : NotEntered;
    let date = dateSplit ? ShowTransactions_jsx("span", null, dateSplit[1] + " - " + dateSplit[0]) : NotEntered;
    let totalPrice = data[index].totalPrice ? utils_formatMoney(data[index].totalPrice) : "0";
    let showDiscount = {
      option: {
        eye: true,
        name: "discounts"
      }
    };
    let paymentStatus = data[index].paymentStatus ? ShowTransactions_jsx("span", {
      style: {
        color: "green"
      }
    }, "پرداخت شده") : ShowTransactions_jsx("span", {
      style: {
        color: "red"
      }
    }, " ", "پرداخت نشده");
    tbody.push({
      data: [userName, userPhoneNumber, date, totalPrice, showDiscount, paymentStatus],
      style: {
        background: data[index].isActive ? "rgb(100, 221, 23,0.3)" : ""
      }
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowTransactions = (ShowTransactions);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/transactionDiscount.js
var transactionDiscount_jsx = external_react_default.a.createElement;



const transactionDiscount = data => {
  // let data =datas.discount
  const thead = ["#", "عکس", "نام ", " امتیاز ", "دسته بندی  ", "قیمت اصلی", "قیمت جدید", " تخفیف", "فروش", "بازدید ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = transactionDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = transactionDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let dataIndex = data[index].discount;
    let thumbnail = dataIndex.thumbnail ? dataIndex.thumbnail : NotEntered;
    let title = dataIndex.title ? dataIndex.title : NotEntered;
    let realPrice = dataIndex.realPrice ? utils_formatMoney(dataIndex.realPrice) : NotEntered;
    let newPrice = dataIndex.newPrice ? utils_formatMoney(dataIndex.newPrice) : NotEntered;
    let rating = dataIndex.rating ? dataIndex.rating : 0;
    let percent = dataIndex.percent ? "%" + dataIndex.percent : NotEntered;
    let boughtCount = dataIndex.boughtCount ? dataIndex.boughtCount : "0";
    let category = dataIndex.category ? dataIndex.category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : NotEntered;
    let viewCount = dataIndex.viewCount ? dataIndex.viewCount : "0";
    let isActive = dataIndex.isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, categoryTitleFa, realPrice, newPrice, percent, boughtCount, viewCount, isActive],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_transactionDiscount = (transactionDiscount);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/members.js
var members_jsx = external_react_default.a.createElement;


const members = data => {
  const thead = ["#", "نام", "شماره همراه", "تراکنش ها", "تخفیف ها", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = members_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = members_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    }); // let avatar = data[index].avatar ? data[index].avatar : NotEntered;


    let name = data[index].name ? data[index].name : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let showTransaction = {
      option: {
        eye: true,
        name: "transactions"
      }
    };
    let showDiscount = {
      option: {
        eye: true,
        name: "discounts"
      }
    };
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [name, phoneNumber, showTransaction, showDiscount, isActive],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_members = (members);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/memberTransaction.js
var memberTransaction_jsx = external_react_default.a.createElement;



const memberTransaction = data => {
  console.log({
    data
  }); // let data =datas.discount

  const thead = ["#", "تاریخ", "قیمت ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dataIndex = data[index];
    let paymentStatus = data[index].paymentStatus ? memberTransaction_jsx("span", {
      style: {
        color: "green"
      }
    }, "پرداخت شده") : memberTransaction_jsx("span", {
      style: {
        color: "red"
      }
    }, " ", "پرداخت نشده");
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let date = dateSplit ? memberTransaction_jsx("span", null, dateSplit[1] + " - " + dateSplit[0]) : NotEntered;
    let totalPrice = dataIndex.totalPrice ? utils_formatMoney(dataIndex.totalPrice) : "0";
    tbody.push({
      data: [date, totalPrice, paymentStatus],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_memberTransaction = (memberTransaction);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/memberDiscount.js
var memberDiscount_jsx = external_react_default.a.createElement;



const memberDiscount = data => {
  console.log({
    data
  }); // let data =datas.discount

  const thead = ["#", "نام", "قیمت ", "تاریخ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dataIndex = data[index];
    let discount = data[index].discount;
    let discountName = discount.title;
    let dateSplit = data[index].usedDate ? data[index].usedDate.split(" ") : "";
    let date = dateSplit ? memberDiscount_jsx("span", null, dateSplit[1] + " - " + dateSplit[0]) : "استفاده نشده";
    let price = dataIndex.price ? utils_formatMoney(dataIndex.price) : "0";
    let isUsed = dataIndex.isUsed ? "استفاده شده" : "استفاده نشده";
    tbody.push({
      data: [discountName, price, date, isUsed],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_memberDiscount = (memberDiscount);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/country.js


const country = data => {
  const thead = ["#", "عکس", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let flag = data[index].flag ? data[index].flag : NotEntered;
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [flag, titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_country = (country);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/instrument.js
const instrument = data => {
  const thead = ["#", "عکس", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [thumbnail, titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_instrument = (instrument);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/song.js


const song = data => {
  const thead = ["#", "نام ", "پخش "];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let title = data[index].title ? data[index].title : NotEntered;
    tbody.push({
      data: [title, {
        option: {
          play: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_song = (song);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/artist.js


const artist = data => {
  console.log({
    data
  });
  const thead = ["#", "نام فارسی", "نام انگلیسی", "توضیحات فارسی ", "توضیحات انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", " لایک", " شنوندگان", " ساز", " بازدید", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let nameFa = data[index].nameFa ? data[index].nameFa : NotEntered;
    let nameEn = data[index].nameEn ? data[index].nameEn : NotEntered;
    let descriptionFa = data[index].descriptionFa ? data[index].descriptionFa : NotEntered;
    let descriptionEn = data[index].descriptionEn ? data[index].descriptionEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albums && data[index].albums.length ? data[index].albums.length : "0";
    let singlesCount = data[index].singles && data[index].singles.length ? data[index].singles.length : "0";
    let musicVideoCounts = data[index].musicVideos && data[index].musicVideos.length ? data[index].musicVideos.length : "0";
    let instrumentCounts = data[index].instruments && data[index].instruments.length ? data[index].instruments.length : "0";
    let likeCount = data[index].likeCount ? data[index].likeCount : "0";
    let listenCount = data[index].listenCount ? data[index].listenCount : "0";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    tbody.push({
      data: [nameFa, nameEn, descriptionFa, descriptionEn, followersCount, albumsCount, singlesCount, likeCount, listenCount, instrumentCounts, viewCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_artist = (artist);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/index.js
















const table = {
  instrument: table_instrument,
  country: table_country,
  memberDiscount: table_memberDiscount,
  memberTransaction: table_memberTransaction,
  members: table_members,
  transactionDiscount: table_transactionDiscount,
  showTransaction: table_ShowTransactions,
  ShowClub: table_ShowClub,
  ShowDiscount: table_ShowDiscount,
  showOwner: table_ShowOwner,
  showScenario: table_ShowScenario,
  genres: table_genres,
  ShowScenarioDataInModal: table_ShowScenarioDataInModal,
  ShowScenarioDataInModalTwo: table_ShowScenarioDataInModalTwo,
  song: table_song,
  artist: table_artist
};
/* harmony default export */ var consts_table = (table);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addGenres.js
const addGenres = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addGenres = (addGenres);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addCategory.js
const owner = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    weight: {
      label: "وزن :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "وزن"
      },
      value: "",
      validation: {
        minLength: 1,
        isNumeric: true,
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var addCategory = (owner);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addArtist.js
const addArtist = {
  Form: {
    nameFa: {
      label: "نام فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام فارسی"
      },
      value: "",
      validation: {
        required: false,
        isFa: false
      },
      valid: true,
      touched: false
    },
    nameEn: {
      label: "نام انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " نام انگلیسی"
      },
      value: "",
      validation: {
        required: false,
        isEn: true
      },
      valid: true,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: false,
        isFa: false
      },
      valid: true,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: false,
        isEn: true
      },
      valid: true,
      touched: false
    },
    defaultGenre: {
      label: "ژانر اصلی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "ژانر اصلی"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    genre: {
      label: "ژانر های دیگر :",
      elementType: "inputDropDownSearchArray",
      elementConfig: {
        type: "text",
        placeholder: "ژانر های دیگر"
      },
      value: [],
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    birthPlaceCountry: {
      label: "متولد در کشور  :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "متولد در کشور "
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    birthPlaceCity: {
      label: "متولد در شهر  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "متولد در شهر "
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    birthday: {
      label: "تاریخ تولد :",
      elementType: "dateInput",
      elementConfig: {
        type: "text",
        placeholder: "بر روی تقویم کلیک نمایید "
      },
      value: {
        month: "",
        day: "",
        year: ""
      },
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    instruments: {
      label: "ساز ها :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "ساز ها"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    website: {
      label: "وب سایت :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "وب سایت"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    instagram: {
      label: "اینستاگرام :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "اینستاگرام"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    youTube: {
      label: "یوتیوب :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "یوتیوب"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    twitter: {
      label: "توییتر :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توییتر"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    avatar: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addArtist = (addArtist);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addSlider.js
const addSlider = {
  Form: {
    parentType: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد"
      },
      childValue: [{
        name: "باشگاه",
        value: "CLUB"
      }, {
        name: "تخفیف",
        value: "DISCOUNT"
      }],
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    type: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    discount: {
      label: "تخفیف :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "تخفیف"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    club: {
      label: "باشگاه :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "باشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addSlider = (addSlider);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addClub.js
const addClub = {
  Form: {
    membership: {
      label: "عضویت :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عضویت"
      },
      childValue: [{
        value: "APPLICATION"
      }, {
        value: "CARD"
      }],
      value: [],
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    owner: {
      label: "فروشنده :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشنده"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    aboutClub: {
      label: " درباره باشگاه :",
      elementType: "textarea",
      elementConfig: {
        type: "text",
        placeholder: " درباره باشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    percent: {
      label: "درصد :",
      elementType: "input",
      value: "",
      validation: {
        minLength: 1,
        maxLength: 3,
        isNumeric: true,
        required: true
      },
      valid: false,
      touched: false
    },
    slides: {
      label: "اسلایدس :",
      elementType: "InputFileArray",
      kindOf: "image",
      value: [],
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addClub = (addClub);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/upload.js
const upload = {
  Form: {
    file: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_upload = (upload);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addBanner.js
const addBanner = {
  Form: {
    parentType: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد"
      },
      childValue: [{
        name: "باشگاه",
        value: "CLUB"
      }, {
        name: "تخفیف",
        value: "DISCOUNT"
      }],
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    discount: {
      label: "تخفیف :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "تخفیف"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    club: {
      label: "باشگاه :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "باشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addBanner = (addBanner);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addScenario.js
const addScenario = {
  Form: {
    name: {
      label: "نام :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    spinRepeatTime: {
      label: "زمان چرخش مجدد (روز):",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "روز"
      },
      value: "",
      validation: {
        required: true,
        minLength: 1,
        maxLength: 1,
        isNumeric: true
      },
      valid: false,
      touched: false
    },
    startDate: {
      label: "تاریخ شروع :",
      elementType: "date",
      elementConfig: {
        type: "text",
        placeholder: "بر روی تقویم کلیک نمایید "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    endDate: {
      label: "تاریخ پایان :",
      elementType: "date",
      elementConfig: {
        type: "text",
        placeholder: "بر روی تقویم کلیک نمایید "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    empty: {
      label: "پوچ ها :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "پوچ ها"
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true
      },
      value: [],
      valid: false,
      touched: false
    },
    gifts: {
      label: "جوایز :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "جوایز"
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true
      },
      value: [],
      valid: false,
      touched: false
    },
    winnersCount: {
      label: "تعداد برندگان :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "تعداد برندگان"
      },
      value: "",
      validation: {
        required: true,
        minLength: 1,
        isNumeric: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addScenario = (addScenario);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addAlbum.js
const addAlbum = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    artist: {
      label: "هنرمند :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "هنرمند"
      },
      value: "",
      validation: {
        minLength: 1,
        required: true
      },
      valid: false,
      touched: false
    },
    genres: {
      label: "ژانر :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "ژانر"
      },
      value: "",
      validation: {
        minLength: 1,
        required: true
      },
      valid: false,
      touched: false
    },
    songs: {
      label: "آهنگ ها :",
      elementType: "InputFileArray",
      kindOf: "voice",
      value: [],
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: " عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addAlbum = (addAlbum);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addCountry.js
const addCountry = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    flag: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addCountry = (addCountry);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/instruments.js
const addInstruments = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    thumbnail: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var instruments = (addInstruments);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addGallery.js
const addGallery = {
  Form: {
    imageName: {
      label: "نام عکس :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام عکس"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    imageType: {
      label: "دسته عکس :",
      elementType: "inputDropDown",
      elementConfig: {
        type: "text",
        placeholder: " دسته عکس"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: " عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addGallery = (addGallery);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addSong.js
const addSong = {
  Form: {
    songName: {
      label: "نام آهنگ :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام آهنگ"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    song: {
      label: " آهنگ :",
      elementType: "inputFile",
      kindOf: "voice",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addSong = (addSong);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addMood.js
const addMood = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    images: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addMood = (addMood);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addHashtag.js
const addHashtag = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addHashtag = (addHashtag);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/index.js















const states = {
  addScenario: states_addScenario,
  addGenres: states_addGenres,
  addCategory: addCategory,
  addArtist: states_addArtist,
  addSlider: states_addSlider,
  addClub: states_addClub,
  upload: states_upload,
  addBanner: states_addBanner,
  addAlbum: states_addAlbum,
  addCountry: states_addCountry,
  instrument: instruments,
  addGallery: states_addGallery,
  addSong: states_addSong,
  addMood: states_addMood,
  addHashtag: states_addHashtag
};
/* harmony default export */ var consts_states = (states);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/galleryConstants.js



const galleryConstants = () => {
  const ConstantsEn = panelAdmin_0.values.en;
  const string = panelAdmin_0.values.strings;
  return [{
    value: ConstantsEn.ALBUM_CONSTANTS,
    title: string.ALBUM_CONSTANTS
  }, {
    value: ConstantsEn.PLAY_LIST_CONSTANTS,
    title: string.PLAY_LIST_CONSTANTS
  }, {
    value: ConstantsEn.FLAG_CONSTANTS,
    title: string.FLAG_CONSTANTS
  }, {
    value: ConstantsEn.SONG_CONSTANTS,
    title: string.SONG_CONSTANTS
  }, {
    value: ConstantsEn.ARTIST_CONSTANTS,
    title: string.ARTIST_CONSTANTS
  }, {
    value: ConstantsEn.INSTRUMENT_CONSTANTS,
    title: string.INSTRUMENT_CONSTANTS
  }, {
    value: ConstantsEn.MUSIC_VIDEO_CONSTANTS,
    title: string.MUSIC_VIDEO_CONSTANTS
  }, {
    value: ConstantsEn.MOOD_CONSTANTS,
    title: string.MOOD_CONSTANTS
  }];
};

/* harmony default export */ var consts_galleryConstants = (galleryConstants);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/instrument.js
const instrument_instrument = data => {
  const cardFormat = [];

  for (let index in data) {
    let dataIndex = data[index];
    let description = dataIndex.descriptionFa ? dataIndex.descriptionFa : "";
    let price = dataIndex.price ? dataIndex.price : "";
    let title = dataIndex.titleFa ? dataIndex.titleFa : "";
    let images = data[index].thumbnail ? data[index].thumbnail : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }, {
        left: [{
          elementType: "price",
          value: price,
          direction: "ltr"
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_instrument = (instrument_instrument);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/gallery.js
const gallery_instrument = (data, acceptedCard) => {
  const cardFormat = [];
  console.log({
    insss: data
  });

  for (let index in data) {
    let dataIndex = data[index];
    let web = dataIndex.web ? dataIndex.web : "";
    let phone = dataIndex.phone ? dataIndex.phone : "";
    let title = dataIndex.title ? dataIndex.title : ""; // let images = { web, phone };
    // console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? acceptedCard.web === web || acceptedCard.phone === phone ? "activeImage" : "" : "",
      image: {
        value: {
          web,
          phone
        }
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var gallery = (gallery_instrument);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/country.js


const country_country = data => {
  const cardFormat = [];
  console.log({
    data
  });

  for (let index in data) {
    let dataIndex = data[index];
    let description = dataIndex.titleFa ? dataIndex.titleFa : "";
    let price = dataIndex.price ? dataIndex.price : "";
    let title = dataIndex.titleFa ? dataIndex.titleFa : "";
    let images = data[index].flag ? data[index].flag : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }, {
        left: [{
          elementType: "price",
          value: price,
          direction: "ltr"
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_country = (country_country);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/mood.js


const mood = data => {
  const cardFormat = [];
  console.log({
    data
  });

  for (let index in data) {
    let dataIndex = data[index];
    let description = dataIndex.titleFa ? dataIndex.titleFa : "";
    let price = dataIndex.price ? dataIndex.price : "";
    let title = dataIndex.titleFa ? dataIndex.titleFa : "";
    let images = dataIndex.images ? dataIndex.images : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }, {
        left: [{
          elementType: "price",
          value: price,
          direction: "ltr"
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_mood = (mood);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/hashtag.js



const hashtag = data => {
  const cardFormat = [];
  console.log({
    data
  });

  for (let index in data) {
    let noEntries = panelAdmin_0.values.strings.NO_ENTRIES;
    let dataIndex = data[index];
    let description = dataIndex.titleFa ? dataIndex.titleFa : noEntries;
    let title = dataIndex.titleFa ? dataIndex.titleFa : noEntries; // let images = dataIndex.images ? dataIndex.images : "";

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      // image: { value: images },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_hashtag = (hashtag);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/genre.js



const genre = data => {
  const cardFormat = [];
  console.log({
    data
  });

  for (let index in data) {
    let noEntries = panelAdmin_0.values.strings.NO_ENTRIES;
    let dataIndex = data[index];
    let description = dataIndex.titleFa ? dataIndex.titleFa : noEntries;
    let title = dataIndex.titleFa ? dataIndex.titleFa : noEntries; // let images = dataIndex.images ? dataIndex.images : "";

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      // image: { value: images },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_genre = (genre);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/artist.js
const artist_artist = (data, acceptedCard) => {
  const cardFormat = [];
  console.log({
    insss: data
  });

  for (let index in data) {
    let dataIndex = data[index];
    let web = dataIndex.web ? dataIndex.web : "";
    let phone = dataIndex.phone ? dataIndex.phone : "";
    let title = dataIndex.nameFa ? dataIndex.nameFa : "";
    let description = dataIndex.descriptionFa ? dataIndex.descriptionFa : "";
    let images = dataIndex.avatar ? dataIndex.avatar : ""; // console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      isActive: data[index].isActive,
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          style: {
            color: "#8c8181",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_artist = (artist_artist);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/index.js







const card = {
  instrument: card_instrument,
  gallery: gallery,
  country: card_country,
  mood: card_mood,
  hashtag: card_hashtag,
  artist: card_artist,
  genre: card_genre
};
/* harmony default export */ var consts_card = (card);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/index.js




const consts = {
  table: consts_table,
  states: consts_states,
  galleryConstants: consts_galleryConstants,
  card: consts_card
};
/* harmony default export */ var utils_consts = (consts);
// EXTERNAL MODULE: external "react-toastify"
var external_react_toastify_ = __webpack_require__("oAEb");

// CONCATENATED MODULE: ./panelAdmin/utils/toastify.js



const toastify_toastify = async (text, type) => {
  external_react_toastify_["toast"].configure();

  if (type === "error") {
    // alert("error");
    await external_react_toastify_["toast"].error(text, {
      position: "top-right",
      autoClose: 5000
    });
  } else if (type === "success") {
    // alert("success");
    await external_react_toastify_["toast"].success(text, {
      position: "top-right",
      autoClose: 5000
    });
  }
};

/* harmony default export */ var utils_toastify = (toastify_toastify);
// CONCATENATED MODULE: ./panelAdmin/utils/CelanderConvert/convertToCelander.js
const convertToCelander = value => {
  let valDate,
      day,
      month,
      year,
      selectedDay = null;

  if (value) {
    valDate = value.split("/");
    day = Number(valDate[2]);
    month = Number(valDate[1]);
    year = Number(valDate[0]);
    selectedDay = {
      day,
      month,
      year
    };
  }

  return selectedDay;
};

/* harmony default export */ var CelanderConvert_convertToCelander = (convertToCelander);
// CONCATENATED MODULE: ./panelAdmin/utils/CelanderConvert/convertToDate.js
const convertToDate = selectedDay => {
  let day = selectedDay.day;
  let month = selectedDay.month;
  let year = selectedDay.year;
  let date = year + "/" + month + "/" + day;
  return date;
};

/* harmony default export */ var CelanderConvert_convertToDate = (convertToDate);
// CONCATENATED MODULE: ./panelAdmin/utils/CelanderConvert/index.js


const CelanderConvert = {
  convertToCelander: CelanderConvert_convertToCelander,
  convertToDate: CelanderConvert_convertToDate
};
/* harmony default export */ var utils_CelanderConvert = (CelanderConvert);
// CONCATENATED MODULE: ./panelAdmin/utils/checkValidity.js
const checkValidity = (value, rules, array) => {
  let isValid = true;
  console.log({
    value,
    rules,
    array
  });
  console.log({
    mehrad: typeof value
  });

  if (!rules) {
    return true;
  }

  if (array) {
    return isValid = value.length > 0;
  }

  if (rules.required) {
    if (typeof value === "object") {
      isValid = true;
    } else {
      isValid = value.trim() !== "" && isValid;
    }
  }

  if (rules.minLength) {
    isValid = value.length >= rules.minLength && isValid;
  }

  if (rules.maxLength) {
    isValid = value.length <= rules.maxLength && isValid;
  }

  if (rules.isEmail) {
    const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isNumeric) {
    const pattern = /^\d+$/;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isPhone) {
    const pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isMobile) {
    const pattern = /[0,9]{2}\d{9}/g;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isEn) {
    const pattern = /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isFa) {
    const pattern = /^[\u0600-\u06FF\s]+$/;
    isValid = pattern.test(value) && isValid;
  }

  return isValid;
};
// EXTERNAL MODULE: ./panelAdmin/utils/updateObject.js
var updateObject = __webpack_require__("ge5p");

// EXTERNAL MODULE: ./panelAdmin/api/index.js + 26 modules
var api = __webpack_require__("FRaV");

// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/validUpload/voicevalid.js
let formats = ["mp3", "voice", "audio"];

const voiceValid = type => {
  let valid = formats.map(format => {
    if (type.includes(format)) return true;else return false;
  });
  let finalValid = valid.includes(true) ? true : false;
  return finalValid;
};

/* harmony default export */ var voicevalid = (voiceValid);
// EXTERNAL MODULE: ./store/actions/saga/index.js
var saga = __webpack_require__("BpoA");

// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/uploadChange.js
function uploadChange_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function uploadChange_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { uploadChange_ownKeys(Object(source), true).forEach(function (key) { uploadChange_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { uploadChange_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function uploadChange_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







const uploadChange = async props => {
  const {
    event,
    setLoading,
    imageType,
    setState,
    valid,
    fileName,
    dispatch
  } = props;
  console.log({
    eventUpload: event
  });
  let files = event.files[0];
  console.log({
    imageType,
    fileName
  });
  let returnData = false;

  if (files) {
    switch (valid) {
      case "image":
        if (files.type.includes("image")) {
          if (imageType && fileName) {
            if ( // dispatch(sagaActions.uploadImageData({ data: files, imageType, fileName }))
            await api["post"].imageUpload(files, setLoading, imageType, setState, fileName)) returnData = fileName;
          } else {
            utils_toastify("لطفا اطلاعات قبلی را پر کنید", "error");
          }
        }

        break;

      case "video":
        if (files.type.includes("video")) returnData = await api["post"].videoUpload(files, setLoading, imageType, setState);
        break;

      case "voice":
        if (voicevalid(files.type)) {
          if (fileName) {
            if (await api["post"].voiceUpload(files, setLoading, imageType, setState, fileName)) returnData = fileName;
          } else {
            utils_toastify("لطفا اطلاعات قبلی را پر کنید", "error");
          }
        }

        break;

      default:
        utils_toastify("فایل شما نباید " + files.type + " باشد", "error");
        break;
    }
  }

  console.log({
    files: files,
    returnData,
    fileName
  });
  if (!returnData && files) utils_toastify("فایل شما نباید " + files.type + " باشد", "error");
  setState(prev => uploadChange_objectSpread({}, prev, {
    progressPercentImage: null,
    progressPercentVideo: null,
    progressPercentSongs: null
  }));
  return returnData;
};

/* harmony default export */ var onChanges_uploadChange = (uploadChange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/handelOnchange.js
function handelOnchange_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function handelOnchange_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { handelOnchange_ownKeys(Object(source), true).forEach(function (key) { handelOnchange_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { handelOnchange_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function handelOnchange_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const handelOnchange = async ({
  event,
  data,
  setData,
  setState,
  setLoading,
  imageType,
  validName,
  checkValidity,
  updateObject,
  uploadChange,
  fileName
}) => {
  let changeValid,
      updatedForm,
      updatedFormElement = {};
  let formIsValid = true;
  let value = data.Form[event.name].value;

  let update = handelOnchange_objectSpread({}, value);

  let eventValue = event.value;
  let typeofData = typeof value;
  let isArray, isObject, isString;
  console.log({
    length: value.length
  });
  typeofData === "object" && value != undefined ? value && value.length >= 0 ? isArray = true : isObject = true : isString = true;
  console.log({
    typeofData,
    value,
    isArray,
    isObject,
    isString
  });

  const remove = index => value.splice(index, 1)[0];

  const push = (val, newVal) => newVal != undefined ? val.push(newVal) : "";

  console.log({
    eventValue,
    value
  });

  if (event.type === "file") {
    const uploadFile = await uploadChange({
      event,
      setLoading,
      imageType,
      setState,
      valid: data.Form[event.name].kindOf,
      fileName
    });

    if (uploadFile) {
      if (isArray) value.includes(uploadFile) ? remove(value.findIndex(d => d === uploadFile)) : push(value, uploadFile);else value = uploadFile;
    } else return;
  } else if (isArray) value.includes(eventValue) ? remove(value.findIndex(d => d === eventValue)) : push(value, eventValue);else if (isObject) {
    // value = eventValue;
    console.log({
      event
    });

    if (event.child) {
      console.log({
        eventValue,
        value
      });
      value[event.child] = eventValue;
    } else {
      value = eventValue;
    }
  } else if (isString) value = eventValue;

  let checkValidValue;
  if (typeofData === "object") checkValidValue = eventValue;else checkValidValue = value;
  updatedFormElement[event.name] = updateObject(data.Form[event.name], {
    value: value,
    valid: typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray),
    touched: true
  });

  if (validName) {
    changeValid = updateObject(data.Form[validName], {
      valid: true
    });
    updatedForm = updateObject(data.Form, handelOnchange_objectSpread({}, updatedFormElement, {
      [validName]: changeValid
    }));
  } else updatedForm = updateObject(data.Form, handelOnchange_objectSpread({}, updatedFormElement));

  console.log({
    updatedFormElement,
    updatedForm
  });

  for (let name in updatedForm) formIsValid = updatedForm[name].valid && formIsValid;

  return setData({
    Form: updatedForm,
    formIsValid: formIsValid
  });
};

/* harmony default export */ var onChanges_handelOnchange = (handelOnchange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/arrayOnchange.js
function arrayOnchange_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function arrayOnchange_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { arrayOnchange_ownKeys(Object(source), true).forEach(function (key) { arrayOnchange_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { arrayOnchange_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function arrayOnchange_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const arrayOnchange = async props => {
  const {
    event: e,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity,
    updateObject,
    uploadChange
  } = props;
  console.log({
    validName
  });
  let changeValid,
      updatedForm,
      updatedFormElement = {};
  e.map(async event => {
    let value = data.Form[event.name].value;

    let update = arrayOnchange_objectSpread({}, value);

    let eventValue = event.value;
    let typeofData = typeof value;
    let isArray, isObject, isString;
    typeofData === "object" && value != null ? value.length >= 0 ? isArray = true : isObject = true : isString = true;

    const remove = index => value.splice(index, 1)[0];

    const push = (val, newVal) => newVal != undefined ? val.push(newVal) : ""; // console.log({ eventValue, value });


    if (event.type === "file") {
      const uploadFile = await uploadChange(event, setLoading, imageType, setState);

      if (uploadFile) {
        if (isArray) value.includes(uploadFile) ? remove(value.findIndex(d => d === uploadFile)) : push(value, uploadFile);else value = uploadFile;
      } else return;
    } else if (isArray) value.includes(eventValue) ? remove(value.findIndex(d => d === eventValue)) : push(value, eventValue);else if (isObject) {
      value = eventValue;
      if (event.child) value = update[event.child] = eventValue;
    } else if (isString) value = eventValue;

    let checkValidValue;
    if (typeofData === "object") checkValidValue = eventValue;else checkValidValue = value;
    updatedFormElement[event.name] = updateObject(data.Form[event.name], {
      value: value,
      valid: typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray),
      touched: true
    });

    if (validName) {
      changeValid = updateObject(data.Form[validName], {
        valid: true
      });
      updatedForm = updateObject(data.Form, arrayOnchange_objectSpread({}, updatedFormElement, {
        [validName]: changeValid
      }));
    } else updatedForm = updateObject(data.Form, arrayOnchange_objectSpread({}, updatedFormElement));

    console.log({
      updatedFormElement,
      updatedForm
    });
    let formIsValid = true;

    for (let name in updatedForm) formIsValid = updatedForm[name].valid && formIsValid;

    return setData({
      Form: updatedForm,
      formIsValid: formIsValid
    });
  });
};

/* harmony default export */ var onChanges_arrayOnchange = (arrayOnchange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/globalChange.js






const globalChange = async props => {
  const {
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    fileName,
    dispatch
  } = props;
  let typeCheck = typeof event; // console.log(typeCheck === "object" && event.length > 0);

  if (typeCheck === "object" && event.length > 0) return onChanges_arrayOnchange({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity: checkValidity,
    updateObject: updateObject["a" /* default */],
    uploadChange: onChanges_uploadChange,
    fileName,
    dispatch
  });else if (typeCheck === "object") return onChanges_handelOnchange({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity: checkValidity,
    updateObject: updateObject["a" /* default */],
    uploadChange: onChanges_uploadChange,
    fileName,
    dispatch
  });
};

/* harmony default export */ var onChanges_globalChange = (globalChange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/index.js


const onChanges = {
  globalChange: onChanges_globalChange,
  arrayOnchange: onChanges_arrayOnchange
};
/* harmony default export */ var utils_onChanges = (onChanges);
// CONCATENATED MODULE: ./panelAdmin/utils/handleKey.js
const handleKey = event => {
  const form = event.target.form;
  const index = Array.prototype.indexOf.call(form, event.target);
  let keyCode = event.keyCode;
  let numberAccepted = [8, 13];

  if (numberAccepted.includes(keyCode)) {
    if (keyCode === 13) {
      if (form.elements[index + 1]) return form.elements[index + 1].focus();
    } else if (keyCode === 8) if (form.elements[index].value) return form.elements[index].value.length - 1;else if (form.elements[index - 1]) form.elements[index - 1].focus();

    event.preventDefault();
  }
};

/* harmony default export */ var utils_handleKey = (handleKey);
// CONCATENATED MODULE: ./panelAdmin/utils/index.js






const utils = {
  consts: utils_consts,
  toastify: utils_toastify,
  CelanderConvert: utils_CelanderConvert,
  onChanges: utils_onChanges,
  handleKey: utils_handleKey,
  formatMoney: utils_formatMoney
};
/* harmony default export */ var panelAdmin_utils = (utils);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/uploadReducer.js
function uploadReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function uploadReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { uploadReducer_ownKeys(Object(source), true).forEach(function (key) { uploadReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { uploadReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function uploadReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const uploadInitialState = {
  uploadImageData: ""
};
const setUploadImageData = (state, action) => {
  return uploadReducer_objectSpread({}, state, {
    uploadData: action.data
  });
};

function uploadReducer(state = uploadInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_UPLOAD_IMAGE:
      return setUploadImageData(state, action);

    default:
      return state;
  }
}

/* harmony default export */ var reducer_uploadReducer = (uploadReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/galleryReducer.js
function galleryReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function galleryReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { galleryReducer_ownKeys(Object(source), true).forEach(function (key) { galleryReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { galleryReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function galleryReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const galleryInitialState = {
  galleryData: []
};
const setGalleryData = (state, action) => {
  return galleryReducer_objectSpread({}, state, {
    galleryData: action.data
  });
};

function galleryReducer(state = galleryInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_GALLERY_DATA:
      return setGalleryData(state, action);

    default:
      return state;
  }
}

/* harmony default export */ var reducer_galleryReducer = (galleryReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/artistReducer.js
function artistReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function artistReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { artistReducer_ownKeys(Object(source), true).forEach(function (key) { artistReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { artistReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function artistReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const artistInitialState = {
  artistData: null,
  searchArtistData: null,
  loading: false,
  searchLoading: false
};

function artistReducer(state = artistInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_ARTIST_DATA:
      return artistReducer_objectSpread({}, state, {}, {
        artistData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_ARTIST_DATA:
      return artistReducer_objectSpread({}, state, {}, {
        searchArtistData: action.data,
        searchLoading: false
      });

    case atRedux.START_ARTIST_DATA:
      console.log("START_ARTIST_DATA");
      return artistReducer_objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_ARTIST_DATA:
      console.log("START_SEARCH_ARTIST_DATA");
      return artistReducer_objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_artistReducer = (artistReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/panelNavbarReducer.js
function panelNavbarReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function panelNavbarReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { panelNavbarReducer_ownKeys(Object(source), true).forEach(function (key) { panelNavbarReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { panelNavbarReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function panelNavbarReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// import atRedux from "../../../store/actionTypes/redux";

const panelNavbarReducer_artistInitialState = {
  pageName: null
};

function panelNavbarReducer(state = panelNavbarReducer_artistInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_PAGE_NAME:
      return panelNavbarReducer_objectSpread({}, state, {}, {
        pageName: action.data
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_panelNavbarReducer = (panelNavbarReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/instrumentReducer.js
function instrumentReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function instrumentReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { instrumentReducer_ownKeys(Object(source), true).forEach(function (key) { instrumentReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { instrumentReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function instrumentReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const instrumentInitialState = {
  instrumentData: null,
  searchInstrumentData: null,
  loading: false,
  searchLoading: false
};

function instrumentReducer(state = instrumentInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_INSTRUMENT_DATA:
      return instrumentReducer_objectSpread({}, state, {}, {
        instrumentData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_INSTRUMENT_DATA:
      return instrumentReducer_objectSpread({}, state, {}, {
        searchInstrumentData: action.data,
        searchLoading: false
      });

    case atRedux.START_INSTRUMENT_DATA:
      return instrumentReducer_objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_INSTRUMENT_DATA:
      return instrumentReducer_objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_instrumentReducer = (instrumentReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/countryReducer.js
function countryReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function countryReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { countryReducer_ownKeys(Object(source), true).forEach(function (key) { countryReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { countryReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function countryReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const countryInitialState = {
  countryData: null,
  searchCountryData: null,
  loading: false,
  searchLoading: false
};

function countryReducer(state = countryInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_COUNTRY_DATA:
      return countryReducer_objectSpread({}, state, {}, {
        countryData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_COUNTRY_DATA:
      return countryReducer_objectSpread({}, state, {}, {
        searchCountryData: action.data,
        searchLoading: false
      });

    case atRedux.START_COUNTRY_DATA:
      return countryReducer_objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_COUNTRY_DATA:
      return countryReducer_objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_countryReducer = (countryReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/moodReducer.js
function moodReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function moodReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { moodReducer_ownKeys(Object(source), true).forEach(function (key) { moodReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { moodReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function moodReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const moodInitialState = {
  moodData: null,
  searchMoodData: null,
  loading: false,
  searchLoading: false
};

function moodReducer(state = moodInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_MOOD_DATA:
      return moodReducer_objectSpread({}, state, {}, {
        moodData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_MOOD_DATA:
      return moodReducer_objectSpread({}, state, {}, {
        searchMoodData: action.data,
        searchLoading: false
      });

    case atRedux.START_MOOD_DATA:
      return moodReducer_objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_MOOD_DATA:
      return moodReducer_objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_moodReducer = (moodReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/hashtagReducer.js
function hashtagReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function hashtagReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { hashtagReducer_ownKeys(Object(source), true).forEach(function (key) { hashtagReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { hashtagReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function hashtagReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const hashtagInitialState = {
  hashtagData: null,
  searchMoodData: null,
  loading: false,
  searchLoading: false
};

function hashtagReducer(state = hashtagInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_HASHTAG_DATA:
      return hashtagReducer_objectSpread({}, state, {}, {
        hashtagData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_HASHTAG_DATA:
      return hashtagReducer_objectSpread({}, state, {}, {
        searchMoodData: action.data,
        searchLoading: false
      });

    case atRedux.START_HASHTAG_DATA:
      return hashtagReducer_objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_HASHTAG_DATA:
      return hashtagReducer_objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_hashtagReducer = (hashtagReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/genreReducer.js
function genreReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function genreReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { genreReducer_ownKeys(Object(source), true).forEach(function (key) { genreReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { genreReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function genreReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const genreInitialState = {
  genreData: null,
  searchGenreData: null,
  loading: false,
  searchLoading: false
};

function genreReducer(state = genreInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;
  console.log({
    action
  });

  switch (action.type) {
    case atRedux.SET_GENRE_DATA:
      return genreReducer_objectSpread({}, state, {}, {
        genreData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_GENRE_DATA:
      return genreReducer_objectSpread({}, state, {}, {
        searchGenreData: action.data,
        searchLoading: false
      });

    case atRedux.START_GENRE_DATA:
      return genreReducer_objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_GENRE_DATA:
      return genreReducer_objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_genreReducer = (genreReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/albumReducer.js
function albumReducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function albumReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { albumReducer_ownKeys(Object(source), true).forEach(function (key) { albumReducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { albumReducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function albumReducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const albumInitialState = {
  albumData: null,
  searchAlbumData: null,
  loading: false,
  searchLoading: false
};

function albumReducer(state = albumInitialState, action) {
  const atRedux = panelAdmin_0.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_ALBUM_DATA:
      return albumReducer_objectSpread({}, state, {}, {
        albumData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_ALBUM_DATA:
      return albumReducer_objectSpread({}, state, {}, {
        searchAlbumData: action.data,
        searchLoading: false
      });

    case atRedux.START_ALBUM_DATA:
      console.log("START_ALBUM_DATA");
      return albumReducer_objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_ALBUM_DATA:
      console.log("START_SEARCH_ALBUM_DATA");
      return albumReducer_objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ var reducer_albumReducer = (albumReducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/reducer/index.js
// import galleryReducer from "./galleryReducer";










const reducer = {
  upload: reducer_uploadReducer,
  gallery: reducer_galleryReducer,
  artist: reducer_artistReducer,
  panelNavbar: reducer_panelNavbarReducer,
  instrument: reducer_instrumentReducer,
  country: reducer_countryReducer,
  mood: reducer_moodReducer,
  hashtag: reducer_hashtagReducer,
  genre: reducer_genreReducer,
  album: reducer_albumReducer
};
/* harmony default export */ var storeAdmin_reducer = (reducer);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/actionTypes/redux/index.js
const redux_atRedux = {
  //======================================================== Redux
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX",
  // ======================================================== NAVBAR
  SET_PAGE_NAME: "SET_PAGE_NAME",
  // ======================================================== UPLOAD
  SET_UPLOAD_IMAGE: "SET_UPLOAD_IMAGE_REDUX",
  // ======================================================== GALLERY
  SET_GALLERY_DATA: "SET_GALLERY_DATA_REDUX",
  CHANGE_ADD_GALLERY_DATA: "CHANGE_ADD_GALLERY_DATA_REDUX",
  // ======================================================== ARTIST
  SET_ARTIST_DATA: "SET_GALLERY_DATA_REDUX",
  START_ARTIST_DATA: "START_GALLERY_DATA_REDUX",
  // ======================= SEARCH ARTIST
  SET_SEARCH_ARTIST_DATA: "SET_SEARCH_ARTIST_DATA_REDUX",
  START_SEARCH_ARTIST_DATA: "START_SEARCH_ARTIST_DATA_REDUX",
  // ======================================================== END ARTIST
  // ======================================================== INSTRUMENT
  SET_INSTRUMENT_DATA: "SET_GALLERY_DATA_REDUX",
  START_INSTRUMENT_DATA: "START_INSTRUMENT_DATA_REDUX",
  // ======================= SEARCH INSTRUMENT
  SET_SEARCH_INSTRUMENT_DATA: "SET_SEARCH_INSTRUMENT_DATA_REDUX",
  START_SEARCH_INSTRUMENT_DATA: "START_SEARCH_INSTRUMENT_DATA_REDUX",
  // ======================================================== END INSTRUMENT
  // ======================================================== COUNTRY
  SET_COUNTRY_DATA: "SET_GALLERY_DATA_REDUX",
  START_COUNTRY_DATA: "START_COUNTRY_DATA_REDUX",
  // ======================= SEARCH COUNTRY
  SET_SEARCH_COUNTRY_DATA: "SET_SEARCH_COUNTRY_DATA_REDUX",
  START_SEARCH_COUNTRY_DATA: "START_SEARCH_COUNTRY_DATA_REDUX",
  // ======================================================== END COUNTRY
  // ======================================================== MOOD
  SET_MOOD_DATA: "SET_GALLERY_DATA_REDUX",
  START_MOOD_DATA: "START_MOOD_DATA_REDUX",
  // ======================= SEARCH MOOD
  SET_SEARCH_MOOD_DATA: "SET_SEARCH_MOOD_DATA_REDUX",
  START_SEARCH_MOOD_DATA: "START_SEARCH_MOOD_DATA_REDUX",
  // ======================================================== END MOOD
  // ======================================================== HASHTAG
  SET_HASHTAG_DATA: "SET_GALLERY_DATA_REDUX",
  START_HASHTAG_DATA: "START_HASHTAG_DATA_REDUX",
  // ======================= SEARCH HASHTAG
  SET_SEARCH_HASHTAG_DATA: "SET_SEARCH_HASHTAG_DATA_REDUX",
  START_SEARCH_HASHTAG_DATA: "START_SEARCH_HASHTAG_DATA_REDUX",
  // ======================================================== END HASHTAG
  // ======================================================== GENRE
  SET_GENRE_DATA: "SET_GALLERY_DATA_REDUX",
  START_GENRE_DATA: "START_GENRE_DATA_REDUX",
  // ======================= SEARCH GENRE
  SET_SEARCH_GENRE_DATA: "SET_SEARCH_GENRE_DATA_REDUX",
  START_SEARCH_GENRE_DATA: "START_SEARCH_GENRE_DATA_REDUX" // ======================================================== END GENRE

};
/* harmony default export */ var redux = (redux_atRedux);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/actions/redux/index.js
 // =================================================== NAVBAR

function setPageName(data) {
  return {
    type: redux.SET_PAGE_NAME,
    data
  };
} // =================================================== UPLOAD

function setUploadImage(data) {
  return {
    type: redux.SET_UPLOAD_IMAGE,
    data
  };
} // =================================================== GALLERY

function redux_setGalleryData(data) {
  return {
    type: redux.SET_GALLERY_DATA,
    data
  };
}
function changeAddGalleryData(data) {
  return {
    type: redux.CHANGE_ADD_GALLERY_DATA,
    data
  };
} // =================================================== ARTIST

function setArtistData(data) {
  return {
    type: redux.SET_ARTIST_DATA,
    data
  };
}
function setSearchArtistData(data) {
  return {
    type: redux.SET_SEARCH_ARTIST_DATA,
    data
  };
}
function startGetArtist(data) {
  return {
    type: redux.START_ARTIST_DATA,
    data
  };
}
function startSearchArtist(data) {
  return {
    type: redux.START_SEARCH_ARTIST_DATA,
    data
  };
} // =================================================== INSTRUMENT

function setInstrumentData(data) {
  return {
    type: redux.SET_INSTRUMENT_DATA,
    data
  };
}
function setSearchInstrumentData(data) {
  return {
    type: redux.SET_SEARCH_INSTRUMENT_DATA,
    data
  };
}
function startGetInstrument(data) {
  return {
    type: redux.START_INSTRUMENT_DATA,
    data
  };
}
function startSearchInstrument(data) {
  return {
    type: redux.START_SEARCH_INSTRUMENT_DATA,
    data
  };
} // =================================================== COUNTRY

function setCountryData(data) {
  return {
    type: redux.SET_COUNTRY_DATA,
    data
  };
}
function setSearchCountryData(data) {
  return {
    type: redux.SET_SEARCH_COUNTRY_DATA,
    data
  };
}
function startGetCountry(data) {
  return {
    type: redux.START_COUNTRY_DATA,
    data
  };
}
function startSearchCountry(data) {
  return {
    type: redux.START_SEARCH_COUNTRY_DATA,
    data
  };
} // =================================================== MOOD

function setMoodData(data) {
  return {
    type: redux.SET_MOOD_DATA,
    data
  };
}
function setSearchMoodData(data) {
  return {
    type: redux.SET_SEARCH_MOOD_DATA,
    data
  };
}
function startGetMood(data) {
  return {
    type: redux.START_MOOD_DATA,
    data
  };
}
function startSearchMood(data) {
  return {
    type: redux.START_SEARCH_MOOD_DATA,
    data
  };
} // =================================================== HASHTAG

function setHashtagData(data) {
  return {
    type: redux.SET_HASHTAG_DATA,
    data
  };
}
function setSearchHashtagData(data) {
  return {
    type: redux.SET_SEARCH_HASHTAG_DATA,
    data
  };
}
function startGetHashtag(data) {
  return {
    type: redux.START_HASHTAG_DATA,
    data
  };
}
function startSearchHashtag(data) {
  return {
    type: redux.START_SEARCH_HASHTAG_DATA,
    data
  };
} // =================================================== GENRE

function setGenreData(data) {
  return {
    type: redux.SET_GENRE_DATA,
    data
  };
}
function setSearchGenreData(data) {
  return {
    type: redux.SET_SEARCH_GENRE_DATA,
    data
  };
}
function startGetGenre(data) {
  return {
    type: redux.START_GENRE_DATA,
    data
  };
}
function startSearchGenre(data) {
  return {
    type: redux.START_SEARCH_GENRE_DATA,
    data
  };
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/actionTypes/saga/index.js
const atSaga = {
  POST_UPLOAD_IMAGE: "POST_UPLOAD_IMAGE_SAGA",
  GET_GALLERY_DATA: "GET_GALLERY_DATA_SAGA",
  //============================================================= ARTIST
  GET_ARTIST_DATA: "GET_ARTIST_DATA_SAGA",
  GET_SEARCH_ARTIST_DATA: "GET_SEARCH_ARTIST_DATA_SAGA",
  //============================================================= INSTRUMENT
  GET_INSTRUMENT_DATA: "GET_INSTRUMENT_DATA_SAGA",
  GET_SEARCH_INSTRUMENT_DATA: "GET_SEARCH_INSTRUMENT_DATA_SAGA",
  //============================================================= COUNTRY
  GET_COUNTRY_DATA: "GET_COUNTRY_DATA_SAGA",
  GET_SEARCH_COUNTRY_DATA: "GET_SEARCH_COUNTRY_DATA_SAGA",
  //============================================================= ALBUM
  GET_ALBUM_DATA: "GET_ALBUM_DATA_SAGA",
  GET_SEARCH_ALBUM_DATA: "GET_SEARCH_ALBUM_DATA_SAGA",
  //============================================================= MOOD
  GET_MOOD_DATA: "GET_MOOD_DATA_SAGA",
  GET_SEARCH_MOOD_DATA: "GET_SEARCH_MOOD_DATA_SAGA",
  //============================================================= HASHTAG
  GET_HASHTAG_DATA: "GET_HASHTAG_DATA_SAGA",
  GET_SEARCH_HASHTAG_DATA: "GET_SEARCH_HASHTAG_DATA_SAGA",
  //============================================================= GENRE
  GET_GENRE_DATA: "GET_GENRE_DATA_SAGA",
  GET_SEARCH_GENRE_DATA: "GET_SEARCH_GENRE_DATA_SAGA"
};
/* harmony default export */ var actionTypes_saga = (atSaga);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/actions/saga/index.js

function uploadImageData(data) {
  return {
    type: actionTypes_saga.POST_UPLOAD_IMAGE,
    data: data
  };
}
function getGalleryData(data) {
  return {
    type: actionTypes_saga.GET_GALLERY_DATA,
    data: data
  };
} // ========================================================= ARTIST

function getArtistData({
  page
}) {
  console.log({
    mojtaba1: page
  });
  return {
    type: actionTypes_saga.GET_ARTIST_DATA,
    page
  };
}
function getSearchArtistData({
  title,
  page
}) {
  return {
    type: actionTypes_saga.GET_SEARCH_ARTIST_DATA,
    page,
    title
  };
} // ========================================================= INSTRUMENT

function getInstrumentData({
  page
}) {
  return {
    type: actionTypes_saga.GET_INSTRUMENT_DATA,
    page
  };
}
function getSearchInstrumentData({
  title,
  page
}) {
  return {
    type: actionTypes_saga.GET_SEARCH_INSTRUMENT_DATA,
    page,
    title
  };
} // ========================================================= COUNTRY

function getCountryData({
  page
}) {
  return {
    type: actionTypes_saga.GET_COUNTRY_DATA,
    page
  };
}
function getSearchCountryData({
  title,
  page
}) {
  return {
    type: actionTypes_saga.GET_SEARCH_COUNTRY_DATA,
    page,
    title
  };
} // ========================================================= MOOD

function getMoodData({
  page
}) {
  return {
    type: actionTypes_saga.GET_MOOD_DATA,
    page
  };
}
function getSearchMoodData({
  title,
  page
}) {
  return {
    type: actionTypes_saga.GET_SEARCH_MOOD_DATA,
    page,
    title
  };
} // ========================================================= ALBUM

function getAlbumData({
  page
}) {
  return {
    type: actionTypes_saga.GET_ALBUM_DATA,
    page
  };
}
function getSearchAlbumData({
  title,
  page
}) {
  return {
    type: actionTypes_saga.GET_SEARCH_ALBUM_DATA,
    page,
    title
  };
} // ========================================================= HASHTAG

function getHashtagData({
  page
}) {
  return {
    type: actionTypes_saga.GET_HASHTAG_DATA,
    page
  };
}
function getSearchHashtagData({
  title,
  page
}) {
  return {
    type: actionTypes_saga.GET_SEARCH_HASHTAG_DATA,
    page,
    title
  };
} // ========================================================= GENRE

function getGenreData({
  page
}) {
  return {
    type: actionTypes_saga.GET_GENRE_DATA,
    page
  };
}
function getSearchGenreData({
  title,
  page
}) {
  return {
    type: actionTypes_saga.GET_SEARCH_GENRE_DATA,
    page,
    title
  };
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/actions/index.js


const actions = {
  reduxActions: actions_redux_namespaceObject,
  sagaActions: actions_saga_namespaceObject
};
/* harmony default export */ var storeAdmin_actions = (actions);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/actionTypes/index.js


const actionTypes = {
  atRedux: redux,
  atSaga: actionTypes_saga
};
/* harmony default export */ var storeAdmin_actionTypes = (actionTypes);
// EXTERNAL MODULE: external "redux-saga/effects"
var effects_ = __webpack_require__("RmXt");

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__("zr5I");
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/homeData.js



function* homeData() {
  try {
    const res = yield external_axios_default.a.get("https://rimtal.com/api/v1/home");
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setHomeData(res.data)); //********** Level 5 **********//
  } catch (err) {
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE)); //********** Level 6 **********//
  }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/galleryApi.js



function* galleryData({
  data
}) {
  try {
    const res = yield external_axios_default.a.get(`https://rimtal.com/api/v1/admin/gallery/${data.type}/${data.page} `);
    console.log({
      res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setGalleryData(res.data)); //********** Level 5 **********//
  } catch (err) {
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE)); //********** Level 6 **********//
  }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/artists.js




function* artistData({
  page
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startGetArtist());
  console.log({
    mojtaba3: page
  });

  try {
    const res = yield panelAdmin_0.api.get.artists({
      page
    });
    console.log({
      resArtistData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setArtistData(res.data));
  } catch (err) {
    console.log({
      errArtistData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* artistSearchData({
  page,
  title
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startSearchArtist());

  try {
    const res = yield panelAdmin_0.api.get.artistSearch({
      page,
      title
    });
    console.log({
      resArtistSearchData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setSearchArtistData(res.data));
  } catch (err) {
    console.log({
      errArtistSearchData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/instrumental.js



function* instrumentData({
  page
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startGetInstrument());
  console.log({
    mojtaba3: page
  });

  try {
    const res = yield panelAdmin_0.api.get.instrument({
      page
    });
    console.log({
      resArtistData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setArtistData(res.data));
  } catch (err) {
    console.log({
      errArtistData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* instrumentSearchData({
  page,
  title
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startSearchInstrument()); //   try {
  //     const res = yield panelAdmin.api.get.artistSearch({ page, title });
  //     console.log({ resArtistSearchData: res });
  //     yield put(actions.reduxActions.setSearchArtistData(res.data));
  //   } catch (err) {
  //     console.log({ errArtistSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/country.js



function* countryData({
  page
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startGetCountry());
  console.log({
    mojtaba3: page
  });

  try {
    const res = yield panelAdmin_0.api.get.country({
      page
    });
    console.log({
      resCountryData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setCountryData(res.data));
  } catch (err) {
    console.log({
      errCountryData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* countrySearchData({
  page,
  title
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startSearchCountry()); //   try {
  //     const res = yield panelAdmin.api.get.CountrySearch({ page, title });
  //     console.log({ resCountrySearchData: res });
  //     yield put(actions.reduxActions.setSearchCountryData(res.data));
  //   } catch (err) {
  //     console.log({ errCountrySearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/mood.js



function* moodData({
  page
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startGetMood());
  console.log({
    mojtaba3: page
  });

  try {
    const res = yield panelAdmin_0.api.get.mood({
      page
    });
    console.log({
      resArtistData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setMoodData(res.data));
  } catch (err) {
    console.log({
      errArtistData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* moodSearchData({
  page,
  title
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startSearchMood()); //   try {
  //     const res = yield panelAdmin.api.get.artistSearch({ page, title });
  //     console.log({ resArtistSearchData: res });
  //     yield put(actions.reduxActions.setSearchArtistData(res.data));
  //   } catch (err) {
  //     console.log({ errArtistSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/hashtag.js



function* hashtagData({
  page
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startGetHashtag());

  try {
    const res = yield panelAdmin_0.api.get.hashtag({
      page
    });
    console.log({
      resArtistData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setHashtagData(res.data));
  } catch (err) {
    console.log({
      errArtistData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* hashtagSearchData({
  page,
  title
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startSearchHashtag()); //   try {
  //     const res = yield panelAdmin.api.get.artistSearch({ page, title });
  //     console.log({ resArtistSearchData: res });
  //     yield put(actions.reduxActions.setSearchArtistData(res.data));
  //   } catch (err) {
  //     console.log({ errArtistSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/genre.js



function* genreData({
  page
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startGetGenre());

  try {
    const res = yield panelAdmin_0.api.get.genres({
      page
    });
    console.log({
      resGenreData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setGenreData(res.data));
  } catch (err) {
    console.log({
      errGenreData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* genreSearchData({
  page,
  title
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startSearchGenre()); //   try {
  //     const res = yield panelAdmin.api.get.GenreSearch({ page, title });
  //     console.log({ resGenreSearchData: res });
  //     yield put(actions.reduxActions.setSearchGenreData(res.data));
  //   } catch (err) {
  //     console.log({ errGenreSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/albums.js



function* albumsData({
  page
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startGetAlbum());

  try {
    const res = yield panelAdmin_0.api.get.albums({
      page
    });
    console.log({
      resalbumData: res
    });
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setAlbumData(res.data));
  } catch (err) {
    console.log({
      erralbumData: err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090));
    yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
function* albumSearchData({
  page,
  title
}) {
  yield Object(effects_["put"])(storeAdmin_actions.reduxActions.startSearchAlbum()); //   try {
  //     const res = yield panelAdmin.api.get.albumSearch({ page, title });
  //     console.log({ resAlbumSearchData: res });
  //     yield put(actions.reduxActions.setSearchAlbumData(res.data));
  //   } catch (err) {
  //     console.log({ errAlbumSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/GET/index.js









const GET = {
  homeData: homeData,
  // ================================= GALLERY
  galleryData: galleryData,
  // ================================= ARTIST
  artistData: artistData,
  artistSearchData: artistSearchData,
  // ================================= INSTRUMENT
  instrumentData: instrumentData,
  instrumentSearchData: instrumentSearchData,
  // ================================= COUNTRY
  countryData: countryData,
  countrySearchData: countrySearchData,
  // ================================= MOOD
  moodData: moodData,
  moodSearchData: moodSearchData,
  // ================================= HASHTAG
  hashtagData: hashtagData,
  hashtagSearchData: hashtagSearchData,
  // ================================= GENRE
  genreData: genreData,
  genreSearchData: genreSearchData,
  // ================================= ALBUM
  albumsData: albumsData,
  albumSearchData: albumSearchData
};
/* harmony default export */ var webServices_GET = (GET);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/POST/uploadImage.js




function* uploadImage(props) {
  console.log("uploaaaaaaaaad omad");
  const {
    imageName,
    type,
    files
  } = props;
  console.log({
    uploadUploadImage: props
  });
  const toastify = panelAdmin_0.utils.toastify;
  const strings = panelAdmin_0.values.strings;
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total); // setState((prev) => ({ ...prev, progressPercentImage: percentCompleted }));
      // yield put(actions.reduxActions.uploadImageProcess());

      console.log(percentCompleted);
    },
    cancelToken: source.token
  };
  const URL = strings.ApiString.IMAGE_UPLOAD;
  const formData = new FormData();
  formData.append("imageName", imageName);
  formData.append("imageType", type);
  formData.append("image", files);

  try {
    const res = yield external_axios_default.a.post(URL, formData, settings);
    return res; // yield put(actions.reduxActions.setUploadImage(res.data));
  } catch (err) {
    console.log({
      err
    });
    if (!err.response) yield Object(effects_["put"])(storeAdmin_actions.reduxActions.setFailure(1090)); // yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/POST/index.js

const POST = {
  uploadImage: uploadImage
};
/* harmony default export */ var webServices_POST = (POST);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/webServices/index.js


const webServices = {
  GET: webServices_GET,
  POST: webServices_POST
};
/* harmony default export */ var saga_webServices = (webServices);
// CONCATENATED MODULE: ./panelAdmin/storeAdmin/saga/index.js


 // ======================================================================================= UPLOAD

function* watchUploadImage() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.UPLOAD_IMAGE_DATA, saga_webServices.POST.uploadImage);
} // ======================================================================================= GALLERY

function* watchGallery() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_GALLERY_DATA, saga_webServices.GET.galleryData);
} // ======================================================================================= ARTIST
// export function* watchArtist() {
//   yield all([
//     takeEvery(atSaga.GET_ARTIST_DATA, webServices.GET.artistData),
//     takeEvery(atSaga.GET_SEARCH_ARTIST_DATA, webServices.GET.artistSearchData),
//   ]);
// }

function* watchGetArtistData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_ARTIST_DATA, saga_webServices.GET.artistData);
}
function* watchSearchArtistData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_SEARCH_ARTIST_DATA, saga_webServices.GET.artistSearchData);
} // ======================================================================================= INSTRUMENTAL

function* watchGetInstrumentalData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_INSTRUMENT_DATA, saga_webServices.GET.instrumentData);
}
function* watchSearchInstrumentalData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_SEARCH_INSTRUMENT_DATA, saga_webServices.GET.instrumentSearchData);
} // ======================================================================================= COUNTRY

function* watchGetCountryData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_COUNTRY_DATA, saga_webServices.GET.countryData);
}
function* watchSearchCountryData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_SEARCH_COUNTRY_DATA, saga_webServices.GET.countrySearchData);
} // ======================================================================================= MOOD

function* watchGetMoodData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_MOOD_DATA, saga_webServices.GET.moodData);
}
function* watchSearchMoodData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_SEARCH_MOOD_DATA, saga_webServices.GET.moodSearchData);
} // ======================================================================================= HASHTAG

function* watchGetHashtagData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_HASHTAG_DATA, saga_webServices.GET.hashtagData);
}
function* watchSearchHashtagData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_SEARCH_HASHTAG_DATA, saga_webServices.GET.hashtagSearchData);
} // ======================================================================================= GENRE

function* watchGetGenreData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_GENRE_DATA, saga_webServices.GET.genreData);
}
function* watchSearchGenreData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_SEARCH_GENRE_DATA, saga_webServices.GET.genreSearchData);
} // ======================================================================================= ALBUM

function* watchGetAlbumData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_ALBUM_DATA, saga_webServices.GET.albumsData);
}
function* watchSearchAlbumData() {
  yield Object(effects_["takeEvery"])(actionTypes_saga.GET_SEARCH_ALBUM_DATA, saga_webServices.GET.albumSearchData);
}
// CONCATENATED MODULE: ./panelAdmin/index.js








const panelAdmin = {
  menuFormat: utils_menuFormat,
  values: panelAdmin_values,
  utils: panelAdmin_utils,
  api: api,
  reducer: storeAdmin_reducer,
  actions: storeAdmin_actions,
  actionTypes: storeAdmin_actionTypes,
  saga: storeAdmin_saga_namespaceObject
};
/* harmony default export */ var panelAdmin_0 = __webpack_exports__["a"] = (panelAdmin);

/***/ }),

/***/ "VnI6":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const ModalTrueFalse = props => {
  return __jsx("div", {
    className: "modal-box"
  }, __jsx("div", {
    className: "m-b-question-title"
  }, __jsx("span", null, props.modalHeadline), __jsx("span", null, props.modalQuestion)), __jsx("div", {
    className: "btns-container"
  }, __jsx("div", {
    onClick: () => props.modalAccept(true),
    className: "submited-box"
  }, __jsx("a", {
    className: "btns btns-primary",
    type: "submit"
  }, props.modalAcceptTitle, " ")), __jsx("div", {
    onClick: () => props.modalAccept(false),
    className: "submited-box"
  }, __jsx("a", {
    className: "btns btns-warning",
    type: "submit"
  }, " ", props.modalCanselTitle))));
};

/* harmony default export */ __webpack_exports__["a"] = (ModalTrueFalse);

/***/ }),

/***/ "YLtl":
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ "YPTf":
/***/ (function(module, exports) {

module.exports = require("react-scrollbars-custom");

/***/ }),

/***/ "b51O":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _panelAdmin_screen_Gallery_GalleryScreen__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("sjK2");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("h74D");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _panelAdmin__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("VGcP");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






const gallery = props => {
  const {
    acceptedCardInfo,
    parentTrue
  } = props;
  const sagaActions = _panelAdmin__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"].actions.sagaActions;
  const reduxActions = _panelAdmin__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"].actions.reduxActions;
  const filters = _panelAdmin__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"].utils.consts.galleryConstants();
  console.log({
    filters
  });
  const store = Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["useSelector"])(state => {
    return state;
  });
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    props.dispatch(sagaActions.getGalleryData({
      type: filters[0].value,
      page: 1
    }));
    if (!parentTrue) props.dispatch(reduxActions.setPageName(_panelAdmin__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"].values.strings.GALLERY));
  }, []);

  const onDataChange = (filterName, page = 1) => {
    props.dispatch(sagaActions.getGalleryData({
      type: filterName,
      page
    }));
  };

  return __jsx(_panelAdmin_screen_Gallery_GalleryScreen__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], {
    acceptedCardInfo: acceptedCardInfo,
    filters: filters,
    onDataChange: onDataChange
  });
};

gallery.getInitialProps = async props => {
  const {
    store,
    isServer
  } = props.ctx; // store.dispatch(sagaActions.getHomeData());

  return {
    isServer
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])()(gallery));

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "ge5p":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const updateObject = (oldObject, updatedProperties) => {
  return _objectSpread({}, oldObject, {}, updatedProperties);
};

/* harmony default export */ __webpack_exports__["a"] = (updateObject);

/***/ }),

/***/ "h74D":
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "n9sB":
/***/ (function(module, exports) {

module.exports = require("@material-ui/lab/Rating");

/***/ }),

/***/ "oAEb":
/***/ (function(module, exports) {

module.exports = require("react-toastify");

/***/ }),

/***/ "q7pC":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export LazyImage */
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("HJQg");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;

const LazyImage = ({
  src,
  alt,
  defaultImage,
  imageOnload
}) => {
  let placeHolder = defaultImage ? defaultImage : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSP-Ze14CTqFzlYouUOJ7OIGAN14YRJrmT1EwYEhsJTBrC92fhtTQ&s";
  const {
    0: imageSrc,
    1: setImageSrc
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(placeHolder); //   const [imageRef, setImageRef] = useState();
  //   console.log({ imageSrc });

  const imageRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  const onLoad = event => {
    // console.log({ onLoad: event });
    event.target.classList.add("loaded"); // imageOnload();
  };

  const onError = event => {
    // console.log({ error: event });
    event.target.classList.add("has-error");
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    let observer;
    let didCancel = false;

    if (imageRef.current && imageSrc !== src) {
      if (IntersectionObserver) {
        observer = new IntersectionObserver(entries => {
          // console.log({ observer, entries });
          entries.forEach(entry => {
            if (!didCancel && (entry.intersectionRatio > 0 || entry.isIntersecting)) {
              setImageSrc(src);
              observer.unobserve(imageRef.current) && observer.unobserve(imageRef.current);
            }
          });
        }, {
          threshold: 0.01,
          rootMargin: "75%"
        });
        observer.observe(imageRef.current);
      } else {
        // Old browsers fallback
        setImageSrc(src);
      }
    }

    return () => {
      didCancel = true; // on component cleanup, we remove the listner

      if (observer && observer.unobserve) {
        observer.unobserve(imageRef.current);
      }
    };
  }, [src, imageSrc, imageRef.current]); //   return <Image ref={setImageRef} src={imageSrc} alt={alt} onLoad={onLoad} onError={onError} />;

  return __jsx(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, __jsx("img", {
    ref: imageRef,
    src: imageSrc ? imageSrc : placeHolder,
    alt: alt,
    onLoadCapture: onLoad,
    onError: onError,
    className: styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a.dynamic([["3389864842", [placeHolder]]]) + " " + "imageComponent"
  }), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    id: "3389864842",
    dynamic: [placeHolder]
  }, [".imageComponent.__jsx-style-dynamic-selector{display:block;}", ".imageComponent.loaded.__jsx-style-dynamic-selector{-webkit-animation:loaded-__jsx-style-dynamic-selector 0.35s ease-in-out;animation:loaded-__jsx-style-dynamic-selector 0.35s ease-in-out;}", `.imageComponent.has-error.__jsx-style-dynamic-selector{content:url(${placeHolder});width:100%;height:100%;}`, ".image-lazyloading-container.__jsx-style-dynamic-selector{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;width:100%;height:100%;background-color:#ccc;}", "@-webkit-keyframes loaded-__jsx-style-dynamic-selector{0%{opacity:0.7;}100%{opacity:1;}}", "@keyframes loaded-__jsx-style-dynamic-selector{0%{opacity:0.7;}100%{opacity:1;}}"]));
};
/* harmony default export */ __webpack_exports__["a"] = (LazyImage);

/***/ }),

/***/ "rUrl":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const atRedux = {
  //======================================================== Redux
  SET_HOME_DATA: "SET_HOME_DATA_REDUX",
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX" // // ======================================================== UPLOAD
  // SET_UPLOAD_IMAGE: "SET_UPLOAD_IMAGE_REDUX",
  // // ======================================================== GALLERY
  // SET_GALLERY_DATA: "SET_GALLERY_DATA_REDUX",
  // CHANGE_ADD_GALLERY_DATA: "CHANGE_ADD_GALLERY_DATA_REDUX",
  // // ======================================================== ARTIST
  // SET_ARTIST_DATA: "SET_GALLERY_DATA_REDUX",
  // START_ARTIST_DATA: "START_GALLERY_DATA_REDUX",
  // // ======================= SEARCH ARTIST
  // SET_SEARCH_ARTIST_DATA: "SET_SEARCH_ARTIST_DATA_REDUX",
  // START_SEARCH_ARTIST_DATA: "START_SEARCH_ARTIST_DATA_REDUX",
  // // ======================================================== END ARTIST
  // // ======================================================== NAVBAR
  // SET_PAGE_NAME: "SET_PAGE_NAME",

};
/* harmony default export */ __webpack_exports__["a"] = (atRedux);

/***/ }),

/***/ "sHut":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const atSaga = {
  GET_HOME_SCREEN_DATA: "GET_HOME_SCREEN_DATA_SAGA" // POST_UPLOAD_IMAGE: "POST_UPLOAD_IMAGE_SAGA",
  // GET_GALLERY_DATA: "GET_GALLERY_DATA_SAGA",
  // GET_ARTIST_DATA: "GET_ARTIST_DATA_SAGA",
  // GET_SEARCH_ARTIST_DATA: "GET_SEARCH_ARTIST_DATA_SAGA",

};
/* harmony default export */ __webpack_exports__["a"] = (atSaga);

/***/ }),

/***/ "sjK2":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: ./panelAdmin/component/UI/Modals/ModalBox/index.js
var ModalBox = __webpack_require__("Ol4B");

// EXTERNAL MODULE: ./panelAdmin/component/UI/Modals/ModalTrueFalse/index.js
var ModalTrueFalse = __webpack_require__("VnI6");

// EXTERNAL MODULE: ./panelAdmin/component/UI/Inputs/Input.js + 10 modules
var Input = __webpack_require__("xYbV");

// EXTERNAL MODULE: ./panelAdmin/index.js + 104 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/screen/Gallery/AddGallery/FormInputGallery/index.js
var __jsx = external_react_default.a.createElement;




const FormInputGallery = props => {
  const {
    stateArray,
    removeHandel,
    state,
    _onSubmited,
    inputChangedHandler,
    checkSubmited
  } = props;
  const dropDown = panelAdmin["a" /* default */].utils.consts.galleryConstants();
  let dropDownData = [];

  for (const index in dropDown) dropDownData.push({
    value: dropDown[index].value,
    title: dropDown[index].title
  });

  return __jsx("form", {
    onSubmit: _onSubmited
  }, stateArray.map(formElement => {
    const invalid = !formElement.config.valid;
    const shouldValidate = formElement.config.validation;
    const touched = formElement.config.touched;
    let changed, accepted, progress;
    const inputClasses = ["InputElement"];
    if (invalid && shouldValidate && touched) inputClasses.push("Invalid");else if (formElement.id === "image") {
      progress = state.progressPercentImage;
    }

    changed = e => inputChangedHandler({
      value: e.currentTarget.value,
      name: formElement.id,
      type: e.currentTarget.type,
      files: e.currentTarget.files
    });

    accepted = value => inputChangedHandler({
      value: value,
      name: formElement.id
    });

    let form = __jsx(Input["a" /* default */], {
      key: formElement.id,
      elementType: formElement.config.elementType,
      elementConfig: formElement.config.elementConfig,
      value: formElement.config.value,
      invalid: invalid,
      shouldValidate: shouldValidate,
      touched: touched,
      changed: changed,
      accepted: accepted,
      removeHandel: index => removeHandel(index, formElement.id),
      label: formElement.config.label,
      progress: progress,
      checkSubmited: checkSubmited,
      dropDownData: dropDownData
    });

    return form;
  }));
};

/* harmony default export */ var AddGallery_FormInputGallery = (FormInputGallery);
// EXTERNAL MODULE: ./panelAdmin/api/index.js + 26 modules
var api = __webpack_require__("FRaV");

// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__("h74D");

// EXTERNAL MODULE: ./store/actions/redux/index.js
var redux = __webpack_require__("D8kl");

// CONCATENATED MODULE: ./panelAdmin/screen/Gallery/AddGallery/index.js
var AddGallery_jsx = external_react_default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }










const AddGallery = ({
  onHideModal,
  getApi,
  data,
  setData
}) => {
  const dispatch = Object(external_react_redux_["useDispatch"])();
  const states = panelAdmin["a" /* default */].utils.consts.states;
  const onChanges = panelAdmin["a" /* default */].utils.onChanges; // const [data, setData] = useState({ ...states.addGallery });

  const {
    0: state,
    1: setState
  } = Object(external_react_["useState"])({
    progressPercentImage: null,
    progressPercentSongs: null,
    remove: {
      value: "",
      name: ""
    }
  });
  const {
    0: Loading,
    1: setLoading
  } = Object(external_react_["useState"])(false);
  const {
    0: Modal,
    1: setModal
  } = Object(external_react_["useState"])({
    show: false
  });
  const {
    0: checkSubmited,
    1: setCheckSubmited
  } = Object(external_react_["useState"])(false); // ======================================== modal
  // ========================= End modal =================
  // ============================= submited
  // const _onSubmited = async (e) => {
  //   setCheckSubmited(!checkSubmited);
  //   e.preventDefault();
  //   const formData = {};
  //   for (let formElementIdentifier in data.Form)
  //     formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
  //   // const initialStateTermsOfUse = updateObject(states.addGallery.Form["termsOfUse"], { value: [] });
  //   // const initialStateFeatures = updateObject(states.addGallery.Form["features"], { value: [] });
  //   // const updatedForm = updateObject(states.addGallery.Form, { ["TermsOfUse"]: initialStateTermsOfUse, ["features"]: initialStateFeatures });
  //   // if (await post.gallery(formData))
  //   onHideModal();
  //   setData({ ...states.addGallery });
  // };
  // ========================= End submited =================
  // ============================= remove

  const __returnPrevstep = value => {
    // onHideModal();
    setState(_objectSpread({}, state, {
      remove: {
        value: "",
        name: ""
      }
    }));
    if (value) inputChangedHandler({
      name: state.remove.name,
      value: state.remove.value
    });
  };

  const removeHandel = (value, name) => {
    // onShowlModal();
    setState(_objectSpread({}, state, {
      remove: {
        value,
        name
      }
    }));
  }; // =========================== End remove  ====================


  const inputChangedHandler = async event => {
    await onChanges.globalChange({
      event,
      data,
      setData,
      setState,
      setLoading,
      imageType: data.Form.imageType.value,
      fileName: data.Form.imageName.value,
      dispatch
    });
  };

  const stateArray = [];

  for (let key in data.Form) stateArray.push({
    id: key,
    config: data.Form[key]
  });

  let form = AddGallery_jsx(AddGallery_FormInputGallery, {
    removeHandel: removeHandel,
    stateArray: stateArray,
    data: data,
    state: state,
    setData: setData,
    Loading: Loading,
    setLoading: setLoading,
    inputChangedHandler: inputChangedHandler,
    checkSubmited: checkSubmited
  });

  return AddGallery_jsx("div", {
    className: " centerAll "
  }, AddGallery_jsx(ModalBox["a" /* default */], {
    onHideModal: onHideModal,
    showModal: Modal.show
  }, AddGallery_jsx(ModalTrueFalse["a" /* default */], {
    modalHeadline: "آیا مطمئن به حذف آن هستید !",
    modalAcceptTitle: "بله",
    modalCanselTitle: "خیر",
    modalAccept: __returnPrevstep
  })), AddGallery_jsx("div", {
    className: "form-countainer shadowUnset"
  }, AddGallery_jsx("div", {
    className: "row-give-information"
  }, form)));
};

/* harmony default export */ var Gallery_AddGallery = (AddGallery);
// EXTERNAL MODULE: external "react-bootstrap"
var external_react_bootstrap_ = __webpack_require__("IZS3");

// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__("YLtl");
var external_lodash_default = /*#__PURE__*/__webpack_require__.n(external_lodash_);

// CONCATENATED MODULE: ./panelAdmin/component/UI/Modals/MyVerticallyCenteredModal/index.js
var MyVerticallyCenteredModal_jsx = external_react_default.a.createElement;



const MyVerticallyCenteredModal = props => {
  const {
    status,
    setStatus,
    onChange,
    title,
    onSubmit,
    acceptedBtn,
    cancelledBtn,
    size,
    acceptedDisabled
  } = props;

  const submitModal = e => {
    setStatus(false);
    onSubmit(e);
  };

  return MyVerticallyCenteredModal_jsx(external_react_bootstrap_["Modal"] // {...props}
  , {
    size: size,
    style: {
      direction: "rtl"
    },
    "aria-labelledby": "contained-modal-title-vcenter",
    centered: true,
    show: status,
    onHide: submitModal,
    animation: true
  }, MyVerticallyCenteredModal_jsx(external_react_bootstrap_["Modal"].Header, {
    closeButton: true
  }, MyVerticallyCenteredModal_jsx(external_react_bootstrap_["Modal"].Title, null, title)), MyVerticallyCenteredModal_jsx(external_react_bootstrap_["Modal"].Body, null, props.children), MyVerticallyCenteredModal_jsx(external_react_bootstrap_["Modal"].Footer, null, cancelledBtn ? MyVerticallyCenteredModal_jsx(external_react_bootstrap_["Button"], {
    variant: "secondary",
    onClick: () => setStatus(false)
  }, cancelledBtn) : "", acceptedBtn ? MyVerticallyCenteredModal_jsx(external_react_bootstrap_["Button"], {
    disabled: acceptedDisabled,
    variant: "primary",
    onClick: submitModal
  }, acceptedBtn) : ""));
};

/* harmony default export */ var Modals_MyVerticallyCenteredModal = (MyVerticallyCenteredModal);
// EXTERNAL MODULE: ./components/LazyImage/index.js
var LazyImage = __webpack_require__("q7pC");

// CONCATENATED MODULE: ./panelAdmin/component/cards/GalleryImageCard/index.js
var GalleryImageCard_jsx = external_react_default.a.createElement;



const GalleryImageCard = ({
  image,
  title,
  handelAcceptedImage,
  imageAccepted
}) => {
  let imageActive = null;
  if (imageAccepted) if (imageAccepted.web || imageAccepted.phone) imageActive = imageAccepted.web === image.web || imageAccepted.phone === image.phone ? "activeImage" : "";
  return GalleryImageCard_jsx("li", {
    onClick: () => imageAccepted ? handelAcceptedImage(image) : null,
    className: "gallery-image-card  px-1 col-6 col-md-5 col-xl-2 col-lg-2"
  }, GalleryImageCard_jsx("div", {
    className: `gallery-image-card-container ${imageActive}`
  }, GalleryImageCard_jsx("picture", null, GalleryImageCard_jsx("source", {
    media: "(max-width: 375px)",
    srcSet: image.phone
  }), GalleryImageCard_jsx(LazyImage["a" /* default */], {
    src: image.web,
    defaultImage: false,
    alt: title
  })), GalleryImageCard_jsx("div", {
    className: "card-image-text-data"
  }, GalleryImageCard_jsx("p", null, title))));
};

/* harmony default export */ var cards_GalleryImageCard = (GalleryImageCard);
// CONCATENATED MODULE: ./panelAdmin/screen/Gallery/GalleryScreen/GalleryImage/index.js
var GalleryImage_jsx = external_react_default.a.createElement;



const GalleryImage = ({
  galleryData,
  handelAcceptedImage,
  imageAccepted
}) => {
  return GalleryImage_jsx("div", {
    className: "panel-main-container  "
  }, galleryData.docs && galleryData.docs.map(info => GalleryImage_jsx(cards_GalleryImageCard, {
    handelAcceptedImage: handelAcceptedImage,
    key: info._id,
    image: {
      web: info.web,
      phone: info.phone
    },
    title: info.title,
    imageAccepted: imageAccepted
  })));
};

/* harmony default export */ var GalleryScreen_GalleryImage = (GalleryImage);
// EXTERNAL MODULE: ./panelAdmin/component/UI/Inputs/DropdownBoot/index.js
var DropdownBoot = __webpack_require__("TI7Q");

// CONCATENATED MODULE: ./panelAdmin/screen/Gallery/GalleryScreen/GalleryFilter/index.js
var GalleryFilter_jsx = external_react_default.a.createElement;




const GalleryFilter = ({
  filters,
  onFilterClick,
  currentFilter,
  dropDownData
}) => {
  return GalleryFilter_jsx("ul", {
    className: "gallery-filter"
  }, GalleryFilter_jsx("div", {
    className: "btns-container"
  }, GalleryFilter_jsx(DropdownBoot["a" /* default */], {
    dropDownData: dropDownData,
    accepted: onFilterClick,
    value: currentFilter
  })), filters && filters.map((filter, index) => {
    return GalleryFilter_jsx("li", {
      key: "gallery-filter-" + index,
      onClick: () => onFilterClick(filter.value),
      className: currentFilter === filter.value ? "active" : ""
    }, external_lodash_default.a.upperFirst(filter.title));
  }));
};

/* harmony default export */ var GalleryScreen_GalleryFilter = (GalleryFilter);
// EXTERNAL MODULE: ./panelAdmin/component/cards/ShowCardInformation/index.js + 3 modules
var ShowCardInformation = __webpack_require__("vM4n");

// EXTERNAL MODULE: ./panelAdmin/component/UI/Loadings/SpinnerRotate/index.js
var SpinnerRotate = __webpack_require__("1Awo");

// CONCATENATED MODULE: ./panelAdmin/screen/Gallery/GalleryScreen/index.js
var GalleryScreen_jsx = external_react_default.a.createElement;

function GalleryScreen_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function GalleryScreen_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { GalleryScreen_ownKeys(Object(source), true).forEach(function (key) { GalleryScreen_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { GalleryScreen_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function GalleryScreen_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }














const GalleryScreen = props => {
  const {
    onDataChange,
    filters,
    acceptedCardInfo
  } = props;
  const states = panelAdmin["a" /* default */].utils.consts.states;
  const {
    0: showUploadModal,
    1: setUploadModal
  } = Object(external_react_["useState"])(false);
  const {
    0: currentFilter,
    1: setCurrentFilter
  } = Object(external_react_["useState"])("album");
  const {
    0: currentPage,
    1: setCurrentPage
  } = Object(external_react_["useState"])(1);
  const {
    0: data,
    1: setData
  } = Object(external_react_["useState"])(GalleryScreen_objectSpread({}, states.addGallery));

  const _onSubmit = async e => {
    // setCheckSubmited(!checkSubmited);
    // e.preventDefault();
    const formData = {};

    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;

    onDataChange(currentFilter, currentPage);
    setData(GalleryScreen_objectSpread({}, states.addGallery));
  };

  const store = Object(external_react_redux_["useSelector"])(state => {
    return state.gallery;
  });
  const card = panelAdmin["a" /* default */].utils.consts.card;
  const loading = store.loading;
  const searchLoading = store.searchLoading;
  const galleryData = store.galleryData;
  const searchGalleryData = store.searchGalleryData;
  const GallerySearch = searchGalleryData ? searchGalleryData.docs.length ? true : false : false;
  let dropDownData = [];

  for (const index in filters) dropDownData.push({
    value: filters[index].value,
    title: filters[index].title
  });

  console.log({
    storeGalleryScreen: store
  });
  const modalData = {
    size: "lg",
    status: showUploadModal,
    setStatus: setUploadModal,
    title: "آپلود عکس",
    acceptedBtn: "ثبت",
    acceptedDisabled: !data.formIsValid,
    onSubmit: _onSubmit
  };

  const onFilterClick = (filterName, currentPage) => {
    setCurrentFilter(filterName);
    onDataChange(filterName, currentPage);
  };

  const AddImage = () => {
    return GalleryScreen_jsx(external_react_bootstrap_["Button"], {
      onClick: () => setUploadModal(true),
      className: ""
    }, "افزودن عکس");
  };

  console.log({
    galleryData
  });
  const showDataElement = galleryData.docs && galleryData.docs.length ? GalleryScreen_jsx(ShowCardInformation["a" /* default */], {
    data: card.gallery(galleryData.docs, acceptedCardInfo ? acceptedCardInfo.acceptedCard : false),
    onClick: null,
    optionClick: null,
    acceptedCardInfo: acceptedCardInfo
  }) : ""; //==============================================================================

  return GalleryScreen_jsx("div", {
    className: "gallery"
  }, GalleryScreen_jsx(Modals_MyVerticallyCenteredModal, modalData, GalleryScreen_jsx(Gallery_AddGallery, {
    data: data,
    setData: setData
  })), GalleryScreen_jsx("div", {
    className: "gallery-header-wrapper"
  }, GalleryScreen_jsx("div", {
    className: "gallery-header"
  }, GalleryScreen_jsx(GalleryScreen_GalleryFilter, {
    dropDownData: dropDownData,
    currentFilter: currentFilter,
    filters: filters,
    onFilterClick: onFilterClick
  }), GalleryScreen_jsx(AddImage, null)), showDataElement), loading && GalleryScreen_jsx("div", {
    className: "staticStyle bgWhite"
  }, GalleryScreen_jsx(SpinnerRotate["a" /* default */], null)));
};

/* harmony default export */ var Gallery_GalleryScreen = __webpack_exports__["a"] = (GalleryScreen);

/***/ }),

/***/ "vM4n":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "@material-ui/lab/Rating"
var Rating_ = __webpack_require__("n9sB");
var Rating_default = /*#__PURE__*/__webpack_require__.n(Rating_);

// EXTERNAL MODULE: external "@material-ui/core/styles"
var styles_ = __webpack_require__("9Pu4");

// CONCATENATED MODULE: ./panelAdmin/component/UI/Rating/Star/index.js
var __jsx = external_react_default.a.createElement;




const Star = props => {
  const {
    rating,
    fixed,
    size
  } = props;
  const {
    0: Value,
    1: setValue
  } = Object(external_react_["useState"])();
  const {
    0: Hover,
    1: setHover
  } = Object(external_react_["useState"])();
  const useStyles = Object(styles_["makeStyles"])(theme => ({
    root: {
      display: "flex",
      flexDirection: "column",
      "& > * + *": {
        marginTop: theme.spacing(1)
      }
    }
  }));
  const classes = useStyles();

  let element = __jsx(Rating_default.a, {
    name: "half-rating",
    defaultValue: Value ? Value : 2.5,
    precision: 0.5,
    onChange: (event, newValue) => {
      setValue(newValue);
    },
    onChangeActive: (event, newHover) => {
      setHover(newHover);
    }
  });

  if (fixed) {
    element = __jsx(Rating_default.a, {
      name: "half-rating-read",
      defaultValue: rating,
      precision: 0.5,
      readOnly: true,
      size: size
    });
  }

  return __jsx("div", {
    style: {
      direction: "ltr",
      marginRight: "auto",
      textAlign: "center",
      justifyContent: "center",
      alignItems: "center"
    },
    className: classes.root,
    size: size
  }, element);
};

/* harmony default export */ var Rating_Star = (Star);
// EXTERNAL MODULE: ./panelAdmin/index.js + 104 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/component/cards/CardElement/CardRow/index.js
var CardRow_jsx = external_react_default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




const formatMoney = panelAdmin["a" /* default */].utils.formatMoney;

const CardRow = props => {
  const {
    elementType,
    value,
    className,
    style,
    iconStyle,
    title
  } = props;
  let element;

  switch (elementType) {
    case "text":
      element = CardRow_jsx("span", {
        title: title,
        style: _objectSpread({}, style)
      }, value);
      break;

    case "star":
      element = CardRow_jsx(Rating_Star, {
        rating: value,
        fixed: true,
        size: "small"
      });
      break;

    case "price":
      element = CardRow_jsx("span", {
        style: _objectSpread({}, style),
        className: "s-c-card-price"
      }, formatMoney(value));
      break;

    case "icon":
      element = CardRow_jsx("div", {
        className: "cardPhonenumber-wrapper"
      }, " ", CardRow_jsx("span", {
        style: _objectSpread({}, style)
      }, value), CardRow_jsx("i", {
        style: _objectSpread({}, iconStyle),
        className: className
      }));
      break;

    case "district":
      element = CardRow_jsx("div", {
        className: "cardDistric-wrapper"
      }, " ", CardRow_jsx("i", {
        className: " icon-location-1"
      }), CardRow_jsx("span", null, value));
      break;

    default:
      break;
  }

  return element;
};

/* harmony default export */ var CardElement_CardRow = (CardRow);
// EXTERNAL MODULE: ./components/LazyImage/index.js
var LazyImage = __webpack_require__("q7pC");

// CONCATENATED MODULE: ./panelAdmin/component/cards/CardElement/index.js
var CardElement_jsx = external_react_["createElement"];



const CardElement = external_react_["memo"](props => {
  const {
    data,
    index,
    onClick,
    submitedTitle,
    optionClick,
    options,
    acceptedCardInfo
  } = props; // console.log({ data });

  return CardElement_jsx("div", {
    style: {
      animationDelay: index * 150 + "ms"
    },
    key: index ? index : "",
    className: `show-Card-Information-row px-1  col-xl-2 col-lg-2 col-6 col-sm-6 col-md-6`,
    onClick: acceptedCardInfo ? () => acceptedCardInfo.handelAcceptedImage({
      index,
      data
    }) : null
  }, CardElement_jsx("div", {
    //  style={{ boxShadow: data.isActive ? "" : "0 0 6px 3px #ff00008a" }}
    className: `card-info transition0-2 ${data.isAccept}`
  }, CardElement_jsx("div", {
    className: "s-c--card-images transition0-2"
  }, CardElement_jsx("div", {
    className: "options-card transition0-2"
  }, options ? CardElement_jsx(Fragment, null, " ", options.remove && CardElement_jsx("span", {
    className: "options-card-cancel",
    onClick: () => optionClick({
      _id: data._id,
      mission: "remove"
    })
  }, CardElement_jsx("i", {
    className: " icon-cancel"
  })), options.edit && CardElement_jsx("span", {
    className: "options-card-edit",
    onClick: () => optionClick({
      _id: data._id,
      mission: "edit"
    })
  }, CardElement_jsx("i", {
    className: " icon-pencil-2"
  })), " ", options.block && CardElement_jsx("span", {
    className: "options-card-block",
    onClick: () => optionClick({
      _id: data._id,
      mission: "block",
      value: !data.isActive
    })
  }, CardElement_jsx("i", {
    className: "icon-lock-1"
  }))) : ""), data.image ? CardElement_jsx("picture", null, CardElement_jsx("source", {
    media: "(max-width: 375px)",
    srcSet: data.image && data.image.value.phone
  }), CardElement_jsx(LazyImage["a" /* default */], {
    src: data.image && data.image.value.web,
    defaultImage: false,
    alt: "cardImage"
  })) : ""), CardElement_jsx("div", {
    className: "s-c-card-body"
  }, CardElement_jsx("div", {
    className: "s-c-body-wrapper"
  }, CardElement_jsx("div", {
    className: "s-c-card-title"
  }, data.body && data.body.length > 0 && data.body.map((info, index) => {
    return CardElement_jsx("div", {
      key: index + "mmj"
    }, CardElement_jsx("div", null, info.right && info.right.map(right => CardElement_CardRow(right))), CardElement_jsx("div", null, info.left && info.left.map(left => CardElement_CardRow(left))));
  }))), submitedTitle ? CardElement_jsx("div", {
    className: "btns-container"
  }, CardElement_jsx("button", {
    onClick: () => onClick(index),
    className: "btns btns-primary"
  }, submitedTitle)) : "")));
});
/* harmony default export */ var cards_CardElement = (CardElement);
// EXTERNAL MODULE: external "react-scrollbars-custom"
var external_react_scrollbars_custom_ = __webpack_require__("YPTf");

// EXTERNAL MODULE: external "react-perfect-scrollbar"
var external_react_perfect_scrollbar_ = __webpack_require__("RfFk");

// CONCATENATED MODULE: ./panelAdmin/component/cards/ShowCardInformation/index.js
var ShowCardInformation_jsx = external_react_default.a.createElement;





const ShowCardInformation = ({
  data,
  onClick,
  submitedTitle,
  optionClick,
  options,
  acceptedCardInfo
}) => {
  const showDataAll = data.map((information, index) => {
    // //console.log({ information });
    return ShowCardInformation_jsx(cards_CardElement, {
      key: index + "mmm",
      data: information,
      options: options,
      optionClick: optionClick,
      index: index,
      onClick: onClick,
      submitedTitle: submitedTitle,
      acceptedCardInfo: acceptedCardInfo
    });
  });

  const ShowData = ShowCardInformation_jsx(cards_CardElement, {
    data: data,
    onClick: onClick,
    options: options,
    optionClick: optionClick,
    submitedTitle: submitedTitle,
    acceptedCardInfo: acceptedCardInfo
  });

  return ShowCardInformation_jsx("div", {
    className: "show-card-elements row m-0"
  }, data.length > 0 ? showDataAll : ShowData);
};

/* harmony default export */ var cards_ShowCardInformation = __webpack_exports__["a"] = (ShowCardInformation);

/***/ }),

/***/ "wgdk":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: ./panelAdmin/api/index.js + 26 modules
var api = __webpack_require__("FRaV");

// EXTERNAL MODULE: ./panelAdmin/component/UI/Inputs/Input.js + 10 modules
var Input = __webpack_require__("xYbV");

// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__("h74D");

// CONCATENATED MODULE: ./panelAdmin/screen/Artist/AddArtist/FormInputArtist/index.js
var __jsx = external_react_default.a.createElement;





const FormInputArtist = props => {
  const {
    stateArray,
    removeHandel,
    state,
    _onSubmited,
    inputChangedHandler,
    checkSubmited,
    showModal
  } = props;
  const {
    0: Genres,
    1: setGenres
  } = Object(external_react_["useState"])();
  const {
    0: BirthPlaceCountry,
    1: setBirthPlaceCountry
  } = Object(external_react_["useState"])();
  const {
    0: Instrument,
    1: setInstrument
  } = Object(external_react_["useState"])();

  const accept = event => inputChangedHandler(event);

  Object(external_react_["useEffect"])(() => {
    api["get"].genres({
      page: false
    }).then(res => {
      setGenres(res.data);
    });
    api["get"].country({
      page: false
    }).then(res => {
      setBirthPlaceCountry(res.data);
    });
    api["get"].instrument({
      page: false
    }).then(res => {
      setInstrument(res.data);
    });
  }, []); // =========================================================================== SEARCH STRUCTURE FOR DROP DOWN
  // =========================== FOR DROP DOWN GENRE ===========================

  let GenreData = [];
  if (Genres) for (const index in Genres) GenreData.push({
    value: Genres[index]._id,
    title: Genres[index].titleFa,
    description: Genres[index].titleEn
  }); // =========================== FOR BIRTH PLACE COUNTRY ===========================

  let BirthPlaceCountryData = [];
  if (BirthPlaceCountry) for (const index in BirthPlaceCountry) BirthPlaceCountryData.push({
    value: BirthPlaceCountry[index]._id,
    title: BirthPlaceCountry[index].titleFa,
    description: BirthPlaceCountry[index].titleEn
  }); // =========================== FOR INSTRUMENT PLACE COUNTRY ===========================

  let InstrumentData = [];
  if (Instrument) for (const index in Instrument) InstrumentData.push({
    value: Instrument[index]._id,
    title: Instrument[index].titleFa,
    description: Instrument[index].titleEn
  }); // =========================================================================== FORM

  return __jsx("form", {
    onSubmit: _onSubmited
  }, stateArray.map((formElement, index) => {
    // ================================= VARIABLE VALUES
    const invalid = !formElement.config.valid;
    const shouldValidate = formElement.config.validation;
    const touched = formElement.config.touched;
    let changed, accepted, dropDownData, disabled;
    disabled = false;
    let value = formElement.config.value;
    const inputClasses = ["InputElement"]; // ================================= CHANGE ELEMENT AMOUNTS

    if (invalid && shouldValidate && touched) inputClasses.push("Invalid");

    if (formElement.id === "avatar") {
      value = formElement.config.value.web ? formElement.config.value.web : "";
      disabled = true;

      accepted = () => showModal({
        kindOf: "showGallery",
        name: formElement.id
      });
    } else if (formElement.id === "birthday") {
      changed = (e, child) => inputChangedHandler({
        value: e.currentTarget.value,
        name: formElement.id,
        child
      });
    } else if (formElement.id === "genre") {
      dropDownData = GenreData;

      accepted = value => accept({
        value,
        name: formElement.id
      });
    } else if (formElement.id === "defaultGenre") {
      dropDownData = GenreData;

      accepted = value => accept({
        value,
        name: formElement.id
      });
    } else if (formElement.id === "birthPlaceCountry") {
      // changed = BirthPlaceCountry;
      dropDownData = BirthPlaceCountryData;

      accepted = value => accept({
        value,
        name: formElement.id
      });
    } else if (formElement.id === "instruments") {
      dropDownData = InstrumentData;

      accepted = value => accept({
        value,
        name: formElement.id
      });
    } else {
      changed = e => inputChangedHandler({
        value: e.currentTarget.value,
        name: formElement.id,
        type: e.currentTarget.type,
        files: e.currentTarget.files
      });

      accepted = value => inputChangedHandler({
        value: value,
        name: formElement.id
      });
    } // ================================= INPUTS


    let Form = __jsx(Input["a" /* default */], {
      key: formElement.id,
      elementType: formElement.config.elementType,
      elementConfig: formElement.config.elementConfig,
      value: value,
      invalid: invalid,
      shouldValidate: shouldValidate,
      touched: touched,
      changed: changed,
      accepted: accepted,
      removeHandel: index => removeHandel(index, formElement.id),
      label: formElement.config.label,
      progress: state.progressPercentImage,
      dropDownData: dropDownData,
      checkSubmited: checkSubmited,
      disabled: disabled
    });

    return Form;
  }));
};

/* harmony default export */ var AddArtist_FormInputArtist = (FormInputArtist);
// EXTERNAL MODULE: ./panelAdmin/screen/Gallery/GalleryScreen/index.js + 6 modules
var GalleryScreen = __webpack_require__("sjK2");

// EXTERNAL MODULE: ./panelAdmin/component/UI/Modals/ModalBox/index.js
var ModalBox = __webpack_require__("Ol4B");

// EXTERNAL MODULE: ./panelAdmin/component/UI/Modals/ModalTrueFalse/index.js
var ModalTrueFalse = __webpack_require__("VnI6");

// EXTERNAL MODULE: ./panelAdmin/index.js + 104 modules
var panelAdmin = __webpack_require__("VGcP");

// EXTERNAL MODULE: ./pages/panelAdmin/gallery.js
var gallery = __webpack_require__("b51O");

// EXTERNAL MODULE: ./panelAdmin/utils/updateObject.js
var updateObject = __webpack_require__("ge5p");

// CONCATENATED MODULE: ./panelAdmin/screen/Artist/AddArtist/index.js
var AddArtist_jsx = external_react_default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }











const AddArtist = () => {
  const states = panelAdmin["a" /* default */].utils.consts.states;
  const onChanges = panelAdmin["a" /* default */].utils.onChanges;
  const {
    0: data,
    1: setData
  } = Object(external_react_["useState"])(_objectSpread({}, states.addArtist));
  const {
    0: state,
    1: setState
  } = Object(external_react_["useState"])({
    progressPercentImage: null,
    remove: {
      value: "",
      name: ""
    }
  });
  const {
    0: Loading,
    1: setLoading
  } = Object(external_react_["useState"])(false);
  const {
    0: editData,
    1: setEditData
  } = Object(external_react_["useState"])(false);
  const {
    0: ModalInpts,
    1: setModalInpts
  } = Object(external_react_["useState"])({
    show: false,
    kindOf: false,
    data: {
      src: false,
      type: false,
      name: false
    },
    name: null
  });
  const {
    0: imageAccepted,
    1: setImageAccepted
  } = Object(external_react_["useState"])({
    web: null,
    phone: null
  });
  const {
    0: checkSubmitted,
    1: setCheckSubmitted
  } = Object(external_react_["useState"])(false); // ======================================== modal

  const onHideModal = () => {
    setModalInpts(_objectSpread({}, ModalInpts, {
      show: false,
      kindOf: false
    })); // handelgetApi();

    setEditData();
  }; // console.log(ModalInpts);


  const onShowModal = event => {
    if (event ? event.kindOf === "question" : false) setModalInpts(_objectSpread({}, ModalInpts, {
      show: true,
      kindOf: event.kindOf,
      name: null
    }));else if (event ? event.kindOf === "showGallery" : false) setModalInpts(_objectSpread({}, ModalInpts, {
      show: true,
      kindOf: event.kindOf,
      name: event.name
    }));
  }; // ============================= remove


  const __returnPrevstep = value => {
    onHideModal();
    setState(_objectSpread({}, state, {
      remove: {
        value: "",
        name: ""
      }
    }));
    if (value) inputChangedHandler({
      name: state.remove.name,
      value: state.remove.value
    });
  };

  const removeHandel = (value, name) => {
    onShowModal({
      kindOf: "question"
    });
    setState(_objectSpread({}, state, {
      remove: {
        value,
        name
      }
    }));
  }; // =========================== End remove  ====================


  const acceptedImage = ({
    index,
    data
  }) => {
    let images = data.image.value;
    imageAccepted.web === images.web ? setImageAccepted({
      web: null,
      phone: null
    }) : setImageAccepted(images);
  };

  const acceptedImageFinal = () => {
    onHideModal();
    inputChangedHandler({
      name: "avatar",
      value: imageAccepted
    });
  };

  const renderModalInputs = AddArtist_jsx("div", {
    className: "width80"
  }, AddArtist_jsx(ModalBox["a" /* default */], {
    onHideModal: onHideModal,
    showModal: ModalInpts.show
  }, ModalInpts.kindOf === "showGallery" && AddArtist_jsx(gallery["default"] // clickedParent={(value) => inputChangedHandler({ name: ModalInpts.name, value })}
  , {
    parentTrue: true,
    acceptedCardInfo: {
      handelAcceptedImage: acceptedImage,
      acceptedCard: imageAccepted
    }
  }) // <ShowListData data={""}/>
  , ModalInpts.kindOf === "question" && AddArtist_jsx(ModalTrueFalse["a" /* default */], {
    modalHeadline: "آیا مطمئن به حذف آن هستید !",
    modalAcceptTitle: "بله",
    modalCanselTitle: "خیر",
    modalAccept: __returnPrevstep
  }), ModalInpts.kindOf === "question" ? "" : AddArtist_jsx("div", {
    className: "btns-container"
  }, AddArtist_jsx("button", {
    disabled: !imageAccepted.web,
    className: "btns btns-primary",
    onClick: e => acceptedImageFinal(e)
  }, "\u062A\u0627\u06CC\u06CC\u062F", " "), AddArtist_jsx("button", {
    className: "btns btns-warning",
    onClick: onHideModal
  }, "\u0628\u0633\u062A\u0646", " ")))); // ========================= End modal =================
  // ============================= submited


  const _onSubmitted = async e => {
    setCheckSubmitted(!checkSubmitted);
    e.preventDefault();
    const formData = {};

    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;

    const initialStateBirthday = Object(updateObject["a" /* default */])(states.addArtist.Form["birthday"], {
      value: {
        month: "",
        day: "",
        year: ""
      }
    }); // const initialStateFeatures = updateObject(states.addArtist.Form["features"], { value: [] });

    const birthDay = formData.birthday;
    if (parseInt(birthDay.day) < 10) formData.birthday.day = "0" + formData.birthday.day;
    if (parseInt(birthDay.month) < 10) formData.birthday.month = "0" + formData.birthday.month;
    console.log({
      formData
    });
    const updatedForm = Object(updateObject["a" /* default */])(_objectSpread({}, states.addArtist.Form), {
      ["birthday"]: initialStateBirthday
    });
    if (await api["post"].artist(formData)) setData({
      Form: updatedForm,
      formIsValid: false
    });
  };

  console.log({
    moj1: states.addArtist,
    moj2: data
  }); // ========================= End submited =================

  const inputChangedHandler = async event => await onChanges.globalChange({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType: "thumbnail"
  });

  const stateArray = [];

  for (let key in data.Form) stateArray.push({
    id: key,
    config: data.Form[key]
  });

  let form = AddArtist_jsx(AddArtist_FormInputArtist, {
    removeHandel: removeHandel,
    _onSubmited: _onSubmitted,
    stateArray: stateArray,
    data: data,
    state: state,
    setData: setData,
    Loading: Loading,
    setLoading: setLoading,
    inputChangedHandler: inputChangedHandler,
    checkSubmited: checkSubmitted,
    showModal: onShowModal
  });

  return AddArtist_jsx("div", {
    className: "countainer-main  "
  }, renderModalInputs, AddArtist_jsx("div", {
    style: {
      marginTop: "20px"
    },
    className: "form-countainer"
  }, AddArtist_jsx("div", {
    className: "form-subtitle"
  }, "\u0627\u0641\u0632\u0648\u062F\u0646 \u0647\u0646\u0631\u0645\u0646\u062F \u062C\u062F\u06CC\u062F"), AddArtist_jsx("div", {
    className: "row-give-information"
  }, form, AddArtist_jsx("div", {
    className: "btns-container"
  }, AddArtist_jsx("button", {
    className: "btns btns-primary",
    disabled: !data.formIsValid,
    onClick: _onSubmitted
  }, "\u0627\u0641\u0632\u0648\u062F\u0646", " ")))));
};

/* harmony default export */ var Artist_AddArtist = (AddArtist);
// CONCATENATED MODULE: ./pages/panelAdmin/addArtist.js
var addArtist_jsx = external_react_default.a.createElement;





const addArtist = () => {
  const dispatch = Object(external_react_redux_["useDispatch"])();
  const reduxActions = panelAdmin["a" /* default */].actions.reduxActions;
  const sagaActions = panelAdmin["a" /* default */].actions.sagaActions;
  const store = Object(external_react_redux_["useSelector"])(state => {
    return state;
  });
  console.log({
    store
  });
  Object(external_react_["useEffect"])(() => {
    dispatch(reduxActions.setPageName(panelAdmin["a" /* default */].values.strings.ADD_ARTIST));
  }, []);
  return addArtist_jsx(Artist_AddArtist, null);
};

/* harmony default export */ var panelAdmin_addArtist = __webpack_exports__["default"] = (addArtist);

/***/ }),

/***/ "xYbV":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/InputPush/index.js
var __jsx = external_react_default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }




const InputPush = props => {
  const {
    accepted,
    inputType,
    className,
    value,
    removeHandel,
    name,
    elementConfig,
    disabled,
    checkSubmited
  } = props;
  const {
    0: Typing,
    1: setTyping
  } = Object(external_react_["useState"])("");

  const acceptedClick = () => {
    if (Typing.length > 0) {
      accepted(Typing);
      setTyping("");
    }
  };

  Object(external_react_["useEffect"])(() => {
    setTyping("");
  }, [checkSubmited]);
  const submitRef = Object(external_react_["useRef"])(null);
  const inputClasses = [className, "transition0-3"];

  const handelOnkeyDown = e => {
    if (e.key === "Enter") {
      submitRef.current.click();
    }
  };

  return __jsx(external_react_["Fragment"], null, value.length > 0 && __jsx("div", {
    className: "data-show-array"
  }, value && value.map((data, index) => {
    return __jsx("span", {
      onClick: () => removeHandel(data),
      key: index
    }, data);
  })), __jsx("div", {
    className: "input-push-wrapper"
  }, __jsx("input", _extends({
    disabled: disabled,
    type: inputType,
    className: inputClasses.join(" "),
    onKeyDown: handelOnkeyDown,
    value: Typing,
    onChange: e => setTyping(e.currentTarget.value),
    autoComplete: "off"
  }, elementConfig)), __jsx("span", {
    ref: submitRef,
    type: "submited",
    onClick: acceptedClick
  }, "\u062B\u0628\u062A")));
};

/* harmony default export */ var Inputs_InputPush = (InputPush);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/InputFile/index.js
var InputFile_jsx = external_react_default.a.createElement;


const InputFile = props => {
  const {
    onChange,
    inputLabel,
    name,
    value,
    progress,
    disabled,
    accepted,
    cancelUpload,
    placeholder
  } = props;
  console.log({
    disabled,
    accepted
  });

  const elements = InputFile_jsx(external_react_default.a.Fragment, null, InputFile_jsx("div", null, value ? value : "  انتخاب نمایید ..."), InputFile_jsx("label", null, InputFile_jsx("span", {
    onClick: disabled ? accepted : null
  }, progress ? progress + "%" : inputLabel), InputFile_jsx("input", {
    disabled: progress ? true : disabled,
    type: "file",
    onChange: onChange,
    name: name
  })));

  return InputFile_jsx("div", {
    className: "addFileModalContainer"
  }, elements);
};

/* harmony default export */ var Inputs_InputFile = (InputFile);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/SearchDropDown/AcceptedChild/index.js
var AcceptedChild_jsx = external_react_default.a.createElement;


const AcceptedChild = ({
  data
}) => {
  return data ? AcceptedChild_jsx("div", {
    className: "serached-item"
  }, data.image ? AcceptedChild_jsx("div", {
    className: "searched-image"
  }, AcceptedChild_jsx("img", {
    src: data.image,
    alt: "search title"
  })) : "", AcceptedChild_jsx("div", {
    className: "searched-text"
  }, AcceptedChild_jsx("span", {
    className: "searched-title"
  }, data.title), AcceptedChild_jsx("span", {
    className: "searched-description"
  }, data.description))) : "";
};

/* harmony default export */ var SearchDropDown_AcceptedChild = (AcceptedChild);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/SearchDropDown/index.js
var SearchDropDown_jsx = external_react_default.a.createElement;

function SearchDropDown_extends() { SearchDropDown_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return SearchDropDown_extends.apply(this, arguments); }




const SearchDropDown = props => {
  const {
    onKeyDown,
    dropDownData,
    onChange,
    accepted,
    disabled,
    checkSubmited,
    className,
    elementConfig,
    value,
    staticTitle
  } = props;
  const {
    0: search,
    1: setSearch
  } = Object(external_react_["useState"])([]);
  const {
    0: searchTitle,
    1: setSearchTitle
  } = Object(external_react_["useState"])("");
  const {
    0: acceptedSave,
    1: setAcceptedSave
  } = Object(external_react_["useState"])();
  const {
    0: typing,
    1: setTping
  } = Object(external_react_["useState"])(false);
  const {
    0: lock,
    1: setLock
  } = Object(external_react_["useState"])(false);
  Object(external_react_["useEffect"])(() => {
    if (dropDownData && dropDownData.length > 0) setSearch(dropDownData);
  }, [dropDownData]);
  Object(external_react_["useEffect"])(() => {
    setSearch([]);
    setSearchTitle("");
    setAcceptedSave();
    setTping(false);
  }, [checkSubmited]);

  const handleBlur = e => {
    if (e.nativeEvent.explicitOriginalTarget && e.nativeEvent.explicitOriginalTarget === e.nativeEvent.originalTarget) return;
    if (search.length > 0) setTimeout(() => {
      setSearch([]);
    }, 300);
  };

  const handelOnChange = e => {
    let value = e.currentTarget.value;
    onChange(e);
    setSearchTitle(value);
    setTping(true);
    setLock(false);
  };

  const clickedElement = index => {
    setAcceptedSave(search[index]);
    setSearchTitle(search[index].title);
    accepted(search[index].value);
    setLock(true);
  };

  const searchContainer = SearchDropDown_jsx("div", {
    className: "text-search-dropDown"
  }, SearchDropDown_jsx("div", null, SearchDropDown_jsx("input", SearchDropDown_extends({
    onKeyDown: onKeyDown,
    disabled: disabled,
    value: searchTitle ? searchTitle : typing ? "" : staticTitle,
    onBlur: handleBlur,
    onChange: handelOnChange,
    className: className
  }, elementConfig)), !lock && search && search.length > 0 && SearchDropDown_jsx("div", {
    className: "searched-dropDown felxRow"
  }, search.map((searched, index) => {
    return SearchDropDown_jsx("div", {
      key: index,
      onClick: () => clickedElement(index),
      className: "serached-item"
    }, searched.image ? SearchDropDown_jsx("div", {
      className: "searched-image"
    }, " ", SearchDropDown_jsx("img", {
      src: searched.image,
      alt: "search title"
    })) : "", SearchDropDown_jsx("div", {
      className: "searched-text"
    }, SearchDropDown_jsx("span", {
      className: "searched-title"
    }, searched.title), SearchDropDown_jsx("span", {
      className: "searched-description"
    }, searched.description)));
  }))), SearchDropDown_jsx("div", null, SearchDropDown_jsx(SearchDropDown_AcceptedChild, {
    data: acceptedSave
  })));

  return searchContainer;
};

/* harmony default export */ var Inputs_SearchDropDown = (SearchDropDown);
// EXTERNAL MODULE: external "react-bootstrap"
var external_react_bootstrap_ = __webpack_require__("IZS3");

// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/InputDropDownSearch/index.js
var InputDropDownSearch_jsx = external_react_default.a.createElement;


const CustomMenu = external_react_default.a.forwardRef(({
  children,
  style,
  className,
  "aria-labelledby": labeledBy
}, ref) => {
  const {
    0: value,
    1: setValue
  } = Object(external_react_["useState"])("");
  return InputDropDownSearch_jsx("div", {
    ref: ref,
    style: style,
    className: className,
    "aria-labelledby": labeledBy
  }, InputDropDownSearch_jsx(external_react_bootstrap_["FormControl"], {
    autoFocus: true,
    className: "mx-3 my-2 w-auto",
    placeholder: "\u062C\u0633\u062A\u062C\u0648 ...",
    onChange: e => setValue(e.target.value),
    value: value
  }), InputDropDownSearch_jsx("ul", {
    className: "list-unstyled"
  }, external_react_default.a.Children.toArray(children).filter(child => !value || child.props.children.toLowerCase().startsWith(value))));
});

const InputDropDownSearch = props => {
  const {
    accepted,
    dropDownData,
    value,
    className,
    checkSubmited,
    disabled
  } = props;
  console.log({
    dropDownData
  });
  let propsVal, index;
  index = dropDownData.findIndex(d => d.value === value);
  if (index >= 0) propsVal = dropDownData[index].title;
  const {
    0: Title,
    1: setTitle
  } = Object(external_react_["useState"])("");
  Object(external_react_["useEffect"])(() => {
    setTitle();
  }, [checkSubmited]);

  const clickedElement = (value, title) => {
    setTitle(title);
    accepted(value);
  };

  const CustomToggle = external_react_default.a.forwardRef(({
    children,
    onClick
  }, ref) => InputDropDownSearch_jsx("a", {
    className: className ? className : "",
    href: "",
    ref: ref,
    onClick: e => {
      e.preventDefault();
      return disabled ? "" : onClick(e);
    }
  }, children, "\u25BC"));
  return InputDropDownSearch_jsx(external_react_bootstrap_["Dropdown"], null, InputDropDownSearch_jsx(external_react_bootstrap_["Dropdown"].Toggle, {
    as: CustomToggle,
    id: "dropdown-custom-components"
  }, Title ? Title : propsVal ? propsVal : value ? value : "انتخاب نمایید  "), InputDropDownSearch_jsx(external_react_bootstrap_["Dropdown"].Menu, {
    as: CustomMenu
  }, dropDownData && dropDownData.map((info, index) => {
    return InputDropDownSearch_jsx(external_react_bootstrap_["Dropdown"].Item, {
      key: index + "m",
      active: value === info.value,
      onClick: () => clickedElement(info.value, info.title),
      eventKey: index + 1
    }, info.title);
  })));
};

/* harmony default export */ var Inputs_InputDropDownSearch = (InputDropDownSearch);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/InputFileArray/index.js
var InputFileArray_jsx = external_react_default.a.createElement;


const InputFileArray = props => {
  const {
    onChange,
    inputLabel,
    name,
    value,
    progress,
    disabled,
    cancelUpload,
    onKeyDown,
    removeHandel
  } = props;
  const {
    0: state,
    1: setState
  } = Object(external_react_["useState"])({
    Form: {},
    formIsValid: false
  });

  const elements = InputFileArray_jsx(external_react_["Fragment"], null, value.length > 0 && InputFileArray_jsx("div", {
    className: "data-show-array"
  }, value.map((data, index) => {
    return InputFileArray_jsx("div", {
      onClick: () => removeHandel(data),
      key: index + "moj"
    }, InputFileArray_jsx("span", null, data));
  })), InputFileArray_jsx("div", {
    className: "addFileModalContainer"
  }, InputFileArray_jsx("div", null, "  انتخاب نمایید ..."), InputFileArray_jsx("label", null, InputFileArray_jsx("span", null, progress ? progress + "%" : inputLabel), InputFileArray_jsx("input", {
    onKeyDown: onKeyDown,
    disabled: progress ? true : disabled,
    type: "file",
    onChange: onChange,
    name: name
  }))));

  return elements;
};

/* harmony default export */ var Inputs_InputFileArray = (InputFileArray);
// EXTERNAL MODULE: ./panelAdmin/component/UI/Inputs/DropdownBoot/index.js
var DropdownBoot = __webpack_require__("TI7Q");

// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/DateInput/index.js
var DateInput_jsx = external_react_default.a.createElement;

const DateInput = Object(external_react_["memo"])(props => {
  const {
    checkSubmited,
    onKeyDown,
    disabled,
    className,
    elementConfig,
    value,
    onChange
  } = props;

  const changedDate = event => {
    let name = event.currentTarget.name;
    let value = event.currentTarget.value;
    const valid = ["month", "day"]; // if (value < 10 && value.length < 2 && valid.includes(name)) {
    //   event.currentTarget.value = 0 + value;
    // } else if (value < 10 == 0 && value.length === 2 && value.charAt(0) == 0 && valid.includes(name)) {
    //   event.currentTarget.value = value.slice(0, 0);
    // }

    onChange(event, name);
  };

  return DateInput_jsx("div", {
    className: "date-input-entry-data"
  }, DateInput_jsx("div", null, DateInput_jsx("label", null, "\u0633\u0627\u0644"), DateInput_jsx("input", {
    maxLength: "4",
    name: "year",
    value: value.year,
    onChange: changedDate,
    className: className,
    onKeyDown: onKeyDown
  })), DateInput_jsx("div", null, DateInput_jsx("label", null, "\u0645\u0627\u0647"), DateInput_jsx("input", {
    maxLength: 2,
    name: "month",
    value: value.month,
    onChange: changedDate,
    className: className,
    onKeyDown: onKeyDown
  })), DateInput_jsx("div", null, DateInput_jsx("label", null, "\u0631\u0648\u0632"), DateInput_jsx("input", {
    maxLength: 2,
    name: "day",
    value: value.day,
    onChange: changedDate,
    className: className,
    onKeyDown: onKeyDown
  })));
});
/* harmony default export */ var Inputs_DateInput = (DateInput); // const [state, setState] = useState({ year: "1300", month: "0", day: "0" });
// useEffect(() => {
//   if (value) changeValue();
// }, [value]);
// const changeValue = () => {
//   setState({ year: value.split("/")[0], month: value.split("/")[1], day: value.split("/")[2] });
// };
// const onChangeElement = (event) => {
//   let name = event.currentTarget.name;
//   let value = event.currentTarget.value;
//   let newState = { ...state };
//   let patt1 = /[0-9]/g;
//   let result = value.match(patt1);
//   if (result) newState[name] = value;
//   setState({ ...state, [name]: value });
//   if (!Object.values(state).includes("")) console.log("etelaat poor ast");
//   event.currentTarget.value = newState.year + "/" + newState.month + "/" + newState.day;
//   onChange(event);
// };
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/SearchDropDownArray/AcceptedChild/index.js
var SearchDropDownArray_AcceptedChild_jsx = external_react_default.a.createElement;


const AcceptedChild_AcceptedChild = ({
  data,
  removeHandel
}) => {
  console.log({
    data
  });
  return data.length ? data.map((data, index) => {
    return SearchDropDownArray_AcceptedChild_jsx("div", {
      onClick: () => removeHandel(data.value),
      key: index + "sadasd",
      className: "serached-item"
    }, data.image ? SearchDropDownArray_AcceptedChild_jsx("div", {
      className: "searched-image"
    }, SearchDropDownArray_AcceptedChild_jsx("img", {
      src: data.image,
      alt: "search title"
    })) : "", SearchDropDownArray_AcceptedChild_jsx("div", {
      className: "searched-text"
    }, SearchDropDownArray_AcceptedChild_jsx("span", {
      className: "searched-title"
    }, data.title)));
  }) : "";
};

/* harmony default export */ var SearchDropDownArray_AcceptedChild = (AcceptedChild_AcceptedChild);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/SearchDropDownArray/index.js
var SearchDropDownArray_jsx = external_react_default.a.createElement;

function SearchDropDownArray_extends() { SearchDropDownArray_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return SearchDropDownArray_extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




const SearchDropDownArray = props => {
  const {
    removeHandel,
    onKeyDown,
    dropDownData,
    onChange,
    accepted,
    disabled,
    checkSubmited,
    className,
    elementConfig,
    value,
    staticTitle
  } = props;
  const {
    0: search,
    1: setSearch
  } = Object(external_react_["useState"])([]);
  const {
    0: searchTitle,
    1: setSearchTitle
  } = Object(external_react_["useState"])("");
  const {
    0: acceptedSave,
    1: setAcceptedSave
  } = Object(external_react_["useState"])([]);
  const {
    0: typing,
    1: setTping
  } = Object(external_react_["useState"])(false);
  const {
    0: lock,
    1: setLock
  } = Object(external_react_["useState"])(false);
  const {
    0: state,
    1: setState
  } = Object(external_react_["useState"])({
    removeIndex: null,
    prevValueLength: null
  });
  let propsVal, index;
  Object(external_react_["useEffect"])(() => {
    if (value.length !== state.prevValueLength) {
      console.log(state.prevValueLength, value.length);
      let newState = acceptedSave;
      newState.splice(state.removeIndex, 1);
      setAcceptedSave(newState);
    }
  }, [value.length]); // index = dropDownData.findIndex((d) => {
  //   console.log({ dropDownData });
  //   console.log({ valiiiiiiiid: d.value.includes(value) });
  //   return d.value.includes(value);
  // });
  // if (index >= 0) propsVal = dropDownData[index].title;
  // console.log({ indexArrrraaaay: index, propsVal });

  Object(external_react_["useEffect"])(() => {
    if (dropDownData && dropDownData.length > 0) setSearch(dropDownData);
  }, [dropDownData]);
  Object(external_react_["useEffect"])(() => {
    setSearch([]);
    setSearchTitle("");
    setAcceptedSave([]);
    setTping(false);
  }, [checkSubmited]);

  const handleBlur = e => {
    if (e.nativeEvent.explicitOriginalTarget && e.nativeEvent.explicitOriginalTarget === e.nativeEvent.originalTarget) return;
    if (search.length > 0) setTimeout(() => {
      setSearch([]);
    }, 300);
  };

  const handelOnChange = e => {
    let value = e.currentTarget.value;
    onChange(e);
    setSearchTitle(value);
    setTping(true);
    setLock(false);
  };

  const clickedElement = index => {
    let newState = acceptedSave;
    newState.push({
      title: search[index].title,
      value: search[index].value
    });
    setAcceptedSave(newState);
    setSearchTitle("");
    accepted(search[index].value);
    setLock(true);
    setState(_objectSpread({}, state, {
      prevValueLength: acceptedSave.length
    }));
  };

  console.log({
    acceptedSave
  });

  const _removeHandel = data => {
    index = acceptedSave.findIndex(d => {
      return d.value.includes(data);
    });
    removeHandel(data);

    if (index >= 0) {
      setState(_objectSpread({}, state, {
        removeIndex: index
      }));
    }
  };

  const searchContainer = SearchDropDownArray_jsx("div", {
    className: "text-search-dropDown"
  }, SearchDropDownArray_jsx("div", null, SearchDropDownArray_jsx("input", SearchDropDownArray_extends({
    onKeyDown: onKeyDown,
    disabled: disabled,
    value: searchTitle ? searchTitle : typing ? "" : staticTitle,
    onBlur: handleBlur,
    onChange: handelOnChange,
    className: className
  }, elementConfig)), !lock && search && search.length > 0 && SearchDropDownArray_jsx("div", {
    className: "searched-dropDown felxRow"
  }, search.map((searched, index) => {
    return SearchDropDownArray_jsx("div", {
      key: index,
      onClick: () => clickedElement(index),
      className: "serached-item"
    }, searched.image ? SearchDropDownArray_jsx("div", {
      className: "searched-image"
    }, " ", SearchDropDownArray_jsx("img", {
      src: searched.image,
      alt: "search title"
    })) : "", SearchDropDownArray_jsx("div", {
      className: "searched-text"
    }, SearchDropDownArray_jsx("span", {
      className: "searched-title"
    }, searched.title), SearchDropDownArray_jsx("span", {
      className: "searched-description"
    }, searched.description)));
  }))), SearchDropDownArray_jsx("div", null, SearchDropDownArray_jsx(SearchDropDownArray_AcceptedChild, {
    data: acceptedSave,
    removeHandel: _removeHandel
  })));

  return searchContainer;
};

/* harmony default export */ var Inputs_SearchDropDownArray = (SearchDropDownArray);
// EXTERNAL MODULE: ./panelAdmin/index.js + 104 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/InputDropDownSearchArray/index.js
var InputDropDownSearchArray_jsx = external_react_default.a.createElement;


const InputDropDownSearchArray_CustomMenu = external_react_default.a.forwardRef(({
  children,
  style,
  className,
  "aria-labelledby": labeledBy
}, ref) => {
  const {
    0: value,
    1: setValue
  } = Object(external_react_["useState"])("");
  return InputDropDownSearchArray_jsx("div", {
    ref: ref,
    style: style,
    className: className,
    "aria-labelledby": labeledBy
  }, InputDropDownSearchArray_jsx(external_react_bootstrap_["FormControl"], {
    autoFocus: true,
    className: "mx-3 my-2 w-auto",
    placeholder: "\u062C\u0633\u062A\u062C\u0648 ...",
    onChange: e => setValue(e.target.value),
    value: value
  }), InputDropDownSearchArray_jsx("ul", {
    className: "list-unstyled"
  }, external_react_default.a.Children.toArray(children).filter(child => !value || child.props.children.toLowerCase().startsWith(value))));
});

const InputDropDownSearchArray = props => {
  const {
    accepted,
    dropDownData,
    value,
    className,
    checkSubmited,
    disabled
  } = props;
  console.log({
    accepted,
    dropDownData,
    value,
    className,
    checkSubmited,
    disabled
  });
  let propsVal;
  propsVal = dropDownData.filter(d => {
    console.log({
      hasan: d.value,
      value
    }, value.includes(d.value));
    return value.includes(d.value);
  });
  const {
    0: Title,
    1: setTitle
  } = Object(external_react_["useState"])("");
  console.log({
    checkSubmited,
    Title
  });
  Object(external_react_["useEffect"])(() => {
    setTitle();
  }, [checkSubmited]);

  const clickedElement = (value, title) => {
    let newTitle = Title ? Title + "," + title : title;
    setTitle(newTitle);
    accepted(value);
  };

  const CustomToggle = external_react_default.a.forwardRef(({
    children,
    onClick
  }, ref) => InputDropDownSearchArray_jsx("a", {
    className: className ? className : "",
    href: "",
    ref: ref,
    onClick: e => {
      e.preventDefault();
      return disabled ? "" : onClick(e);
    }
  }, children, "\u25BC"));
  return InputDropDownSearchArray_jsx(external_react_bootstrap_["Dropdown"], null, InputDropDownSearchArray_jsx(external_react_bootstrap_["Dropdown"].Toggle, {
    as: CustomToggle,
    id: "dropdown-custom-components"
  }, propsVal.length ? propsVal.map(val => {
    return val.title + ",";
  }) : value.length ? value : "انتخاب نمایید  "), InputDropDownSearchArray_jsx(external_react_bootstrap_["Dropdown"].Menu, {
    as: InputDropDownSearchArray_CustomMenu
  }, dropDownData && dropDownData.map((info, index) => {
    return InputDropDownSearchArray_jsx(external_react_bootstrap_["Dropdown"].Item, {
      key: index + "m",
      active: value.includes(info.value),
      onClick: () => clickedElement(info.value, info.title),
      eventKey: index + 1
    }, info.title);
  })));
};

/* harmony default export */ var Inputs_InputDropDownSearchArray = (InputDropDownSearchArray);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/Input.js
var Input_jsx = external_react_default.a.createElement;

function Input_extends() { Input_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return Input_extends.apply(this, arguments); }






 // import DateCelander from "./DateCelander";






const handleKey = panelAdmin["a" /* default */].utils.handleKey;

const Inputs = props => {
  const {
    progress,
    elementType,
    elementConfig,
    value,
    changed,
    accepted,
    label,
    invalid,
    shouldValidate,
    touched,
    removeHandel,
    dropDownData,
    defaultInputDesable,
    checkSubmited,
    disabled,
    display,
    searchAccepted,
    setSearchAccepted,
    staticTitle
  } = props;
  let inputElement = null;
  const inputClasses = ["InputElement"];

  if (invalid && shouldValidate && touched) {
    inputClasses.push("Invalid");
  }

  switch (elementType) {
    case "input":
      inputElement = Input_jsx("input", Input_extends({
        onKeyDown: handleKey,
        disabled: disabled,
        className: inputClasses.join(" ")
      }, elementConfig, {
        value: value,
        onChange: changed
      }));
      break;

    case "inputPush":
      inputElement = Input_jsx(Inputs_InputPush, {
        checkSubmited: checkSubmited,
        disabled: disabled,
        className: inputClasses.join(" "),
        accepted: accepted,
        value: value,
        elementConfig: elementConfig,
        removeHandel: removeHandel
      });
      break;

    case "inputFile":
      inputElement = Input_jsx(Inputs_InputFile, Input_extends({
        accepted: accepted,
        disabled: disabled,
        progress: progress,
        className: inputClasses.join(" "),
        onChange: changed,
        value: value
      }, elementConfig, {
        inputLabel: "انتخاب",
        label: label
      }));
      break;

    case "InputFileArray":
      inputElement = Input_jsx(Inputs_InputFileArray, Input_extends({
        onKeyDown: handleKey,
        removeHandel: removeHandel,
        disabled: disabled,
        progress: progress,
        className: inputClasses.join(" "),
        onChange: changed,
        value: value
      }, elementConfig, {
        inputLabel: "انتخاب",
        label: label
      }));
      break;

    case "inputSearch":
      inputElement = Input_jsx(Inputs_SearchDropDown, {
        onKeyDown: handleKey,
        checkSubmited: checkSubmited,
        dropDownData: dropDownData,
        accepted: accepted,
        className: inputClasses.join(" "),
        onChange: changed,
        value: value,
        label: label,
        elementConfig: elementConfig,
        disabled: disabled,
        searchAccepted: searchAccepted,
        setSearchAccepted: setSearchAccepted,
        staticTitle: staticTitle
      });
      break;

    case "inputSearchArray":
      inputElement = Input_jsx(Inputs_SearchDropDownArray, {
        onKeyDown: handleKey,
        checkSubmited: checkSubmited,
        dropDownData: dropDownData,
        accepted: accepted,
        className: inputClasses.join(" "),
        onChange: changed,
        value: value,
        label: label,
        elementConfig: elementConfig,
        disabled: disabled,
        searchAccepted: searchAccepted,
        setSearchAccepted: setSearchAccepted,
        staticTitle: staticTitle,
        removeHandel: removeHandel
      });
      break;

    case "inputDropDownSearch":
      inputElement = Input_jsx(Inputs_InputDropDownSearch, {
        dropDownData: dropDownData,
        disabled: disabled,
        className: inputClasses.join(" "),
        accepted: accepted,
        value: value,
        elementConfig: elementConfig,
        checkSubmited: checkSubmited
      });
      break;

    case "inputDropDownSearchArray":
      inputElement = Input_jsx(Inputs_InputDropDownSearchArray, {
        dropDownData: dropDownData,
        disabled: disabled,
        className: inputClasses.join(" "),
        accepted: accepted,
        value: value,
        elementConfig: elementConfig,
        checkSubmited: checkSubmited
      });
      break;

    case "textarea":
      inputElement = Input_jsx("textarea", Input_extends({
        onKeyDown: handleKey,
        disabled: disabled,
        className: inputClasses.join(" ")
      }, elementConfig, {
        value: value,
        onChange: changed
      }));
      break;

    case "select":
      inputElement = Input_jsx("select", {
        disabled: disabled,
        className: inputClasses.join(" "),
        value: value,
        onChange: changed
      }, elementConfig.options.map(option => Input_jsx("option", {
        key: option.value,
        value: option.value
      }, option.displayValue)));
      break;

    case "inputDropDown":
      inputElement = Input_jsx(DropdownBoot["a" /* default */], {
        dropDownData: dropDownData,
        disabled: disabled,
        className: inputClasses.join(" "),
        accepted: accepted,
        value: value,
        elementConfig: elementConfig,
        checkSubmited: checkSubmited
      });
      break;
    // case "date":
    //   inputElement = (
    //     <DateCelander
    //       checkSubmited={checkSubmited}
    //       onKeyDown={handleKey}
    //       disabled={disabled}
    //       className={inputClasses.join(" ")}
    //       elementConfig={elementConfig}
    //       value={value}
    //       accepted={accepted}
    //     />
    //   );
    //   break;

    case "dateInput":
      inputElement = Input_jsx(Inputs_DateInput, {
        checkSubmited: checkSubmited,
        onKeyDown: handleKey,
        disabled: disabled,
        className: inputClasses.join(" "),
        elementConfig: elementConfig,
        value: value,
        onChange: changed
      });
      break;

    default:
      return defaultInputDesable ? "" : inputElement = Input_jsx("input", Input_extends({
        onKeyDown: handleKey,
        className: inputClasses.join(" ")
      }, elementConfig, {
        value: value,
        onChange: changed
      }));
  }

  return Input_jsx("div", {
    className: "Input",
    style: {
      display
    }
  }, Input_jsx("label", {
    className: "Label"
  }, label), inputElement);
};

/* harmony default export */ var Input = __webpack_exports__["a"] = (Inputs);

/***/ }),

/***/ "zr5I":
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ })

/******/ });