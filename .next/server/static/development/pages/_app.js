module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "../next-server/lib/router-context":
/*!**************************************************************!*\
  !*** external "next/dist/next-server/lib/router-context.js" ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/router-context.js");

/***/ }),

/***/ "../next-server/lib/utils":
/*!*****************************************************!*\
  !*** external "next/dist/next-server/lib/utils.js" ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/utils.js");

/***/ }),

/***/ "./components/AwesomeScroll/index.js":
/*!*******************************************!*\
  !*** ./components/AwesomeScroll/index.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-perfect-scrollbar */ "react-perfect-scrollbar");
/* harmony import */ var react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_useragent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next-useragent */ "next-useragent");
/* harmony import */ var next_useragent__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_useragent__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/components/AwesomeScroll/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const AwesomeScroll = props => {
  const {
    data,
    Row,
    ua,
    scrollBar
  } = props; // console.log({ data, Row });
  // console.log({ ua });

  const scrollRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null); // console.log({ AwesomeScroll: data });

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    if (scrollRef.current) {
      const slider = scrollRef.current.children[0];
      let isDown = false;
      let startX;
      let scrollLeft;
      slider.addEventListener("mousedown", e => {
        isDown = true; // slider.classList.add("active");

        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
      });
      slider.addEventListener("mouseleave", () => {
        isDown = false; // slider.classList.remove("active");
      });
      slider.addEventListener("mouseup", () => {
        isDown = false; // slider.classList.remove("active");
      });
      slider.addEventListener("mousemove", e => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = x - startX; //scroll-fast
        // console.log({ x, walk }, (slider.scrollLeft = scrollLeft - walk));

        slider.scrollLeft = scrollLeft - walk;
      });
    }
  }, []);
  return __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 5
    }
  }, !scrollBar ? __jsx("div", {
    ref: scrollRef,
    className: "awesome-scroll-container",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 9
    }
  }, props.children) : ua && ua.isMobile ? __jsx("div", {
    className: "awesome-scroll-container",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 9
    }
  }, props.children) : __jsx("div", {
    ref: scrollRef,
    className: "awesome-scroll-container",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 9
    }
  }, __jsx(react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1___default.a, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59,
      columnNumber: 11
    }
  }, props.children)));
};

AwesomeScroll.getInitialProps = async ctx => {
  return {
    useragent: ctx.ua.source
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(next_useragent__WEBPACK_IMPORTED_MODULE_2__["withUserAgent"])(AwesomeScroll));

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/interopRequireDefault.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/interopRequireWildcard.js":
/*!***********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/interopRequireWildcard.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! ../helpers/typeof */ "./node_modules/@babel/runtime/helpers/typeof.js");

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/typeof.js":
/*!*******************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/typeof.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof2(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

function _typeof(obj) {
  if (typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return _typeof2(obj);
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "./node_modules/next/app.js":
/*!**********************************!*\
  !*** ./node_modules/next/app.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/pages/_app */ "./node_modules/next/dist/pages/_app.js")


/***/ }),

/***/ "./node_modules/next/dist/client/link.js":
/*!***********************************************!*\
  !*** ./node_modules/next/dist/client/link.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js");

var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime/helpers/interopRequireWildcard */ "./node_modules/@babel/runtime/helpers/interopRequireWildcard.js");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(/*! react */ "react"));

var _url = __webpack_require__(/*! url */ "url");

var _utils = __webpack_require__(/*! ../next-server/lib/utils */ "../next-server/lib/utils");

var _router = _interopRequireDefault(__webpack_require__(/*! ./router */ "./node_modules/next/dist/client/router.js"));

var _router2 = __webpack_require__(/*! ../next-server/lib/router/router */ "./node_modules/next/dist/next-server/lib/router/router.js");

function isLocal(href) {
  var url = (0, _url.parse)(href, false, true);
  var origin = (0, _url.parse)((0, _utils.getLocationOrigin)(), false, true);
  return !url.host || url.protocol === origin.protocol && url.host === origin.host;
}

function memoizedFormatUrl(formatFunc) {
  var lastHref = null;
  var lastAs = null;
  var lastResult = null;
  return (href, as) => {
    if (lastResult && href === lastHref && as === lastAs) {
      return lastResult;
    }

    var result = formatFunc(href, as);
    lastHref = href;
    lastAs = as;
    lastResult = result;
    return result;
  };
}

function formatUrl(url) {
  return url && typeof url === 'object' ? (0, _utils.formatWithValidation)(url) : url;
}

var observer;
var listeners = new Map();
var IntersectionObserver = false ? undefined : null;
var prefetched = {};

function getObserver() {
  // Return shared instance of IntersectionObserver if already created
  if (observer) {
    return observer;
  } // Only create shared IntersectionObserver if supported in browser


  if (!IntersectionObserver) {
    return undefined;
  }

  return observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (!listeners.has(entry.target)) {
        return;
      }

      var cb = listeners.get(entry.target);

      if (entry.isIntersecting || entry.intersectionRatio > 0) {
        observer.unobserve(entry.target);
        listeners.delete(entry.target);
        cb();
      }
    });
  }, {
    rootMargin: '200px'
  });
}

var listenToIntersections = (el, cb) => {
  var observer = getObserver();

  if (!observer) {
    return () => {};
  }

  observer.observe(el);
  listeners.set(el, cb);
  return () => {
    try {
      observer.unobserve(el);
    } catch (err) {
      console.error(err);
    }

    listeners.delete(el);
  };
};

class Link extends _react.Component {
  constructor(props) {
    super(props);
    this.p = void 0;

    this.cleanUpListeners = () => {};

    this.formatUrls = memoizedFormatUrl((href, asHref) => {
      return {
        href: (0, _router2.addBasePath)(formatUrl(href)),
        as: asHref ? (0, _router2.addBasePath)(formatUrl(asHref)) : asHref
      };
    });

    this.linkClicked = e => {
      var {
        nodeName,
        target
      } = e.currentTarget;

      if (nodeName === 'A' && (target && target !== '_self' || e.metaKey || e.ctrlKey || e.shiftKey || e.nativeEvent && e.nativeEvent.which === 2)) {
        // ignore click for new tab / new window behavior
        return;
      }

      var {
        href,
        as
      } = this.formatUrls(this.props.href, this.props.as);

      if (!isLocal(href)) {
        // ignore click if it's outside our scope (e.g. https://google.com)
        return;
      }

      var {
        pathname
      } = window.location;
      href = (0, _url.resolve)(pathname, href);
      as = as ? (0, _url.resolve)(pathname, as) : href;
      e.preventDefault(); //  avoid scroll for urls with anchor refs

      var {
        scroll
      } = this.props;

      if (scroll == null) {
        scroll = as.indexOf('#') < 0;
      } // replace state instead of push if prop is present


      _router.default[this.props.replace ? 'replace' : 'push'](href, as, {
        shallow: this.props.shallow
      }).then(success => {
        if (!success) return;

        if (scroll) {
          window.scrollTo(0, 0);
          document.body.focus();
        }
      });
    };

    if (true) {
      if (props.prefetch) {
        console.warn('Next.js auto-prefetches automatically based on viewport. The prefetch attribute is no longer needed. More: https://err.sh/zeit/next.js/prefetch-true-deprecated');
      }
    }

    this.p = props.prefetch !== false;
  }

  componentWillUnmount() {
    this.cleanUpListeners();
  }

  getPaths() {
    var {
      pathname
    } = window.location;
    var {
      href: parsedHref,
      as: parsedAs
    } = this.formatUrls(this.props.href, this.props.as);
    var resolvedHref = (0, _url.resolve)(pathname, parsedHref);
    return [resolvedHref, parsedAs ? (0, _url.resolve)(pathname, parsedAs) : resolvedHref];
  }

  handleRef(ref) {
    if (this.p && IntersectionObserver && ref && ref.tagName) {
      this.cleanUpListeners();
      var isPrefetched = prefetched[this.getPaths().join( // Join on an invalid URI character
      '%')];

      if (!isPrefetched) {
        this.cleanUpListeners = listenToIntersections(ref, () => {
          this.prefetch();
        });
      }
    }
  } // The function is memoized so that no extra lifecycles are needed
  // as per https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html


  prefetch(options) {
    if (!this.p || true) return; // Prefetch the JSON page if asked (only in the client)

    var paths = this.getPaths(); // We need to handle a prefetch error here since we may be
    // loading with priority which can reject but we don't
    // want to force navigation since this is only a prefetch

    _router.default.prefetch(paths[
    /* href */
    0], paths[
    /* asPath */
    1], options).catch(err => {
      if (true) {
        // rethrow to show invalid URL errors
        throw err;
      }
    });

    prefetched[paths.join( // Join on an invalid URI character
    '%')] = true;
  }

  render() {
    var {
      children
    } = this.props;
    var {
      href,
      as
    } = this.formatUrls(this.props.href, this.props.as); // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

    if (typeof children === 'string') {
      children = _react.default.createElement("a", null, children);
    } // This will return the first child, if multiple are provided it will throw an error


    var child = _react.Children.only(children);

    var props = {
      ref: el => {
        this.handleRef(el);

        if (child && typeof child === 'object' && child.ref) {
          if (typeof child.ref === 'function') child.ref(el);else if (typeof child.ref === 'object') {
            child.ref.current = el;
          }
        }
      },
      onMouseEnter: e => {
        if (child.props && typeof child.props.onMouseEnter === 'function') {
          child.props.onMouseEnter(e);
        }

        this.prefetch({
          priority: true
        });
      },
      onClick: e => {
        if (child.props && typeof child.props.onClick === 'function') {
          child.props.onClick(e);
        }

        if (!e.defaultPrevented) {
          this.linkClicked(e);
        }
      }
    }; // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
    // defined, we specify the current 'href', so that repetition is not needed by the user

    if (this.props.passHref || child.type === 'a' && !('href' in child.props)) {
      props.href = as || href;
    } // Add the ending slash to the paths. So, we can serve the
    // "<page>/index.html" directly.


    if (false) { var rewriteUrlForNextExport; }

    return _react.default.cloneElement(child, props);
  }

}

if (true) {
  var warn = (0, _utils.execOnce)(console.error); // This module gets removed by webpack.IgnorePlugin

  var PropTypes = __webpack_require__(/*! prop-types */ "prop-types");

  var exact = __webpack_require__(/*! prop-types-exact */ "prop-types-exact"); // @ts-ignore the property is supported, when declaring it on the class it outputs an extra bit of code which is not needed.


  Link.propTypes = exact({
    href: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
    as: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    prefetch: PropTypes.bool,
    replace: PropTypes.bool,
    shallow: PropTypes.bool,
    passHref: PropTypes.bool,
    scroll: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.element, (props, propName) => {
      var value = props[propName];

      if (typeof value === 'string') {
        warn("Warning: You're using a string directly inside <Link>. This usage has been deprecated. Please add an <a> tag as child of <Link>");
      }

      return null;
    }]).isRequired
  });
}

var _default = Link;
exports.default = _default;

/***/ }),

/***/ "./node_modules/next/dist/client/router.js":
/*!*************************************************!*\
  !*** ./node_modules/next/dist/client/router.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime/helpers/interopRequireWildcard */ "./node_modules/@babel/runtime/helpers/interopRequireWildcard.js");

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _router2 = _interopRequireWildcard(__webpack_require__(/*! ../next-server/lib/router/router */ "./node_modules/next/dist/next-server/lib/router/router.js"));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__(/*! ../next-server/lib/router-context */ "../next-server/lib/router-context");

var _withRouter = _interopRequireDefault(__webpack_require__(/*! ./with-router */ "./node_modules/next/dist/client/with-router.js"));

exports.withRouter = _withRouter.default;
/* global window */

var singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

var urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components', 'isFallback', 'basePath'];
var routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
var coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

Object.defineProperty(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  Object.defineProperty(singletonRouter, field, {
    get() {
      var router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = function () {
    var router = getRouter();
    return router[field](...arguments);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, function () {
      var eventField = "on" + event.charAt(0).toUpperCase() + event.substring(1);
      var _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...arguments);
        } catch (err) {
          // tslint:disable-next-line:no-console
          console.error("Error when running the Router event: " + eventField); // tslint:disable-next-line:no-console

          console.error(err.message + "\n" + err.stack);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    var message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


var createRouter = function createRouter() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  var _router = router;
  var instance = {};

  for (var property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = Object.assign({}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = function () {
      return _router[field](...arguments);
    };
  });
  return instance;
}

/***/ }),

/***/ "./node_modules/next/dist/client/with-router.js":
/*!******************************************************!*\
  !*** ./node_modules/next/dist/client/with-router.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.default = withRouter;

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _router = __webpack_require__(/*! ./router */ "./node_modules/next/dist/client/router.js");

function withRouter(ComposedComponent) {
  function WithRouterWrapper(props) {
    return _react.default.createElement(ComposedComponent, Object.assign({
      router: (0, _router.useRouter)()
    }, props));
  }

  WithRouterWrapper.getInitialProps = ComposedComponent.getInitialProps // This is needed to allow checking for custom getInitialProps in _app
  ;
  WithRouterWrapper.origGetInitialProps = ComposedComponent.origGetInitialProps;

  if (true) {
    var name = ComposedComponent.displayName || ComposedComponent.name || 'Unknown';
    WithRouterWrapper.displayName = "withRouter(" + name + ")";
  }

  return WithRouterWrapper;
}

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/mitt.js":
/*!********************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/mitt.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/*
MIT License

Copyright (c) Jason Miller (https://jasonformat.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

Object.defineProperty(exports, "__esModule", {
  value: true
});

function mitt() {
  const all = Object.create(null);
  return {
    on(type, handler) {
      ;
      (all[type] || (all[type] = [])).push(handler);
    },

    off(type, handler) {
      if (all[type]) {
        // tslint:disable-next-line:no-bitwise
        all[type].splice(all[type].indexOf(handler) >>> 0, 1);
      }
    },

    emit(type, ...evts) {
      // eslint-disable-next-line array-callback-return
      ;
      (all[type] || []).slice().map(handler => {
        handler(...evts);
      });
    }

  };
}

exports.default = mitt;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/router.js":
/*!*****************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/router.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__(/*! url */ "url");

const mitt_1 = __importDefault(__webpack_require__(/*! ../mitt */ "./node_modules/next/dist/next-server/lib/mitt.js"));

const utils_1 = __webpack_require__(/*! ../utils */ "./node_modules/next/dist/next-server/lib/utils.js");

const is_dynamic_1 = __webpack_require__(/*! ./utils/is-dynamic */ "./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js");

const route_matcher_1 = __webpack_require__(/*! ./utils/route-matcher */ "./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js");

const route_regex_1 = __webpack_require__(/*! ./utils/route-regex */ "./node_modules/next/dist/next-server/lib/router/utils/route-regex.js");

const basePath =  false || '';

function addBasePath(path) {
  return path.indexOf(basePath) !== 0 ? basePath + path : path;
}

exports.addBasePath = addBasePath;

function delBasePath(path) {
  return path.indexOf(basePath) === 0 ? path.substr(basePath.length) || '/' : path;
}

function toRoute(path) {
  return path.replace(/\/$/, '') || '/';
}

const prepareRoute = path => toRoute(!path || path === '/' ? '/index' : path);

function fetchNextData(pathname, query, isServerRender, cb) {
  let attempts = isServerRender ? 3 : 1;

  function getResponse() {
    return fetch(utils_1.formatWithValidation({
      // @ts-ignore __NEXT_DATA__
      pathname: `/_next/data/${__NEXT_DATA__.buildId}${pathname}.json`,
      query
    }), {
      // Cookies are required to be present for Next.js' SSG "Preview Mode".
      // Cookies may also be required for `getServerSideProps`.
      //
      // > `fetch` won’t send cookies, unless you set the credentials init
      // > option.
      // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
      //
      // > For maximum browser compatibility when it comes to sending &
      // > receiving cookies, always supply the `credentials: 'same-origin'`
      // > option instead of relying on the default.
      // https://github.com/github/fetch#caveats
      credentials: 'same-origin'
    }).then(res => {
      if (!res.ok) {
        if (--attempts > 0 && res.status >= 500) {
          return getResponse();
        }

        throw new Error(`Failed to load static props`);
      }

      return res.json();
    });
  }

  return getResponse().then(data => {
    return cb ? cb(data) : data;
  }).catch(err => {
    // We should only trigger a server-side transition if this was caused
    // on a client-side transition. Otherwise, we'd get into an infinite
    // loop.
    if (!isServerRender) {
      ;
      err.code = 'PAGE_LOAD_ERROR';
    }

    throw err;
  });
}

class Router {
  constructor(pathname, query, as, {
    initialProps,
    pageLoader,
    App,
    wrapApp,
    Component,
    err,
    subscription,
    isFallback
  }) {
    // Static Data Cache
    this.sdc = {};

    this.onPopState = e => {
      if (!e.state) {
        // We get state as undefined for two reasons.
        //  1. With older safari (< 8) and older chrome (< 34)
        //  2. When the URL changed with #
        //
        // In the both cases, we don't need to proceed and change the route.
        // (as it's already changed)
        // But we can simply replace the state with the new changes.
        // Actually, for (1) we don't need to nothing. But it's hard to detect that event.
        // So, doing the following for (1) does no harm.
        const {
          pathname,
          query
        } = this;
        this.changeState('replaceState', utils_1.formatWithValidation({
          pathname,
          query
        }), utils_1.getURL());
        return;
      } // Make sure we don't re-render on initial load,
      // can be caused by navigating back from an external site


      if (e.state && this.isSsr && e.state.as === this.asPath && url_1.parse(e.state.url).pathname === this.pathname) {
        return;
      } // If the downstream application returns falsy, return.
      // They will then be responsible for handling the event.


      if (this._bps && !this._bps(e.state)) {
        return;
      }

      const {
        url,
        as,
        options
      } = e.state;

      if (true) {
        if (typeof url === 'undefined' || typeof as === 'undefined') {
          console.warn('`popstate` event triggered but `event.state` did not have `url` or `as` https://err.sh/zeit/next.js/popstate-state-empty');
        }
      }

      this.replace(url, as, options);
    };

    this._getStaticData = asPath => {
      const pathname = prepareRoute(url_1.parse(asPath).pathname);
      return  false ? undefined : fetchNextData(pathname, null, this.isSsr, data => this.sdc[pathname] = data);
    };

    this._getServerData = asPath => {
      let {
        pathname,
        query
      } = url_1.parse(asPath, true);
      pathname = prepareRoute(pathname);
      return fetchNextData(pathname, query, this.isSsr);
    }; // represents the current component key


    this.route = toRoute(pathname); // set up the component cache (by route keys)

    this.components = {}; // We should not keep the cache, if there's an error
    // Otherwise, this cause issues when when going back and
    // come again to the errored page.

    if (pathname !== '/_error') {
      this.components[this.route] = {
        Component,
        props: initialProps,
        err,
        __N_SSG: initialProps && initialProps.__N_SSG,
        __N_SSP: initialProps && initialProps.__N_SSP
      };
    }

    this.components['/_app'] = {
      Component: App
    }; // Backwards compat for Router.router.events
    // TODO: Should be remove the following major version as it was never documented

    this.events = Router.events;
    this.pageLoader = pageLoader;
    this.pathname = pathname;
    this.query = query; // if auto prerendered and dynamic route wait to update asPath
    // until after mount to prevent hydration mismatch

    this.asPath = // @ts-ignore this is temporarily global (attached to window)
    is_dynamic_1.isDynamicRoute(pathname) && __NEXT_DATA__.autoExport ? pathname : as;
    this.basePath = basePath;
    this.sub = subscription;
    this.clc = null;
    this._wrapApp = wrapApp; // make sure to ignore extra popState in safari on navigating
    // back from external site

    this.isSsr = true;
    this.isFallback = isFallback;

    if (false) {}
  } // @deprecated backwards compatibility even though it's a private method.


  static _rewriteUrlForNextExport(url) {
    if (false) {} else {
      return url;
    }
  }

  update(route, mod) {
    const Component = mod.default || mod;
    const data = this.components[route];

    if (!data) {
      throw new Error(`Cannot update unavailable route: ${route}`);
    }

    const newData = Object.assign(Object.assign({}, data), {
      Component,
      __N_SSG: mod.__N_SSG,
      __N_SSP: mod.__N_SSP
    });
    this.components[route] = newData; // pages/_app.js updated

    if (route === '/_app') {
      this.notify(this.components[this.route]);
      return;
    }

    if (route === this.route) {
      this.notify(newData);
    }
  }

  reload() {
    window.location.reload();
  }
  /**
   * Go back in history
   */


  back() {
    window.history.back();
  }
  /**
   * Performs a `pushState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  push(url, as = url, options = {}) {
    return this.change('pushState', url, as, options);
  }
  /**
   * Performs a `replaceState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  replace(url, as = url, options = {}) {
    return this.change('replaceState', url, as, options);
  }

  change(method, _url, _as, options) {
    return new Promise((resolve, reject) => {
      if (!options._h) {
        this.isSsr = false;
      } // marking route changes as a navigation start entry


      if (utils_1.ST) {
        performance.mark('routeChange');
      } // If url and as provided as an object representation,
      // we'll format them into the string version here.


      let url = typeof _url === 'object' ? utils_1.formatWithValidation(_url) : _url;
      let as = typeof _as === 'object' ? utils_1.formatWithValidation(_as) : _as;
      url = addBasePath(url);
      as = addBasePath(as); // Add the ending slash to the paths. So, we can serve the
      // "<page>/index.html" directly for the SSR page.

      if (false) {}

      this.abortComponentLoad(as); // If the url change is only related to a hash change
      // We should not proceed. We should only change the state.
      // WARNING: `_h` is an internal option for handing Next.js client-side
      // hydration. Your app should _never_ use this property. It may change at
      // any time without notice.

      if (!options._h && this.onlyAHashChange(as)) {
        this.asPath = as;
        Router.events.emit('hashChangeStart', as);
        this.changeState(method, url, as, options);
        this.scrollToHash(as);
        Router.events.emit('hashChangeComplete', as);
        return resolve(true);
      }

      const {
        pathname,
        query,
        protocol
      } = url_1.parse(url, true);

      if (!pathname || protocol) {
        if (true) {
          throw new Error(`Invalid href passed to router: ${url} https://err.sh/zeit/next.js/invalid-href-passed`);
        }

        return resolve(false);
      } // If asked to change the current URL we should reload the current page
      // (not location.reload() but reload getInitialProps and other Next.js stuffs)
      // We also need to set the method = replaceState always
      // as this should not go into the history (That's how browsers work)
      // We should compare the new asPath to the current asPath, not the url


      if (!this.urlIsNew(as)) {
        method = 'replaceState';
      }

      const route = toRoute(pathname);
      const {
        shallow = false
      } = options;

      if (is_dynamic_1.isDynamicRoute(route)) {
        const {
          pathname: asPathname
        } = url_1.parse(as);
        const routeRegex = route_regex_1.getRouteRegex(route);
        const routeMatch = route_matcher_1.getRouteMatcher(routeRegex)(asPathname);

        if (!routeMatch) {
          const missingParams = Object.keys(routeRegex.groups).filter(param => !query[param]);

          if (missingParams.length > 0) {
            if (true) {
              console.warn(`Mismatching \`as\` and \`href\` failed to manually provide ` + `the params: ${missingParams.join(', ')} in the \`href\`'s \`query\``);
            }

            return reject(new Error(`The provided \`as\` value (${asPathname}) is incompatible with the \`href\` value (${route}). ` + `Read more: https://err.sh/zeit/next.js/incompatible-href-as`));
          }
        } else {
          // Merge params into `query`, overwriting any specified in search
          Object.assign(query, routeMatch);
        }
      }

      Router.events.emit('routeChangeStart', as); // If shallow is true and the route exists in the router cache we reuse the previous result

      this.getRouteInfo(route, pathname, query, as, shallow).then(routeInfo => {
        const {
          error
        } = routeInfo;

        if (error && error.cancelled) {
          return resolve(false);
        }

        Router.events.emit('beforeHistoryChange', as);
        this.changeState(method, url, as, options);

        if (true) {
          const appComp = this.components['/_app'].Component;
          window.next.isPrerendered = appComp.getInitialProps === appComp.origGetInitialProps && !routeInfo.Component.getInitialProps;
        }

        this.set(route, pathname, query, as, routeInfo);

        if (error) {
          Router.events.emit('routeChangeError', error, as);
          throw error;
        }

        Router.events.emit('routeChangeComplete', as);
        return resolve(true);
      }, reject);
    });
  }

  changeState(method, url, as, options = {}) {
    if (true) {
      if (typeof window.history === 'undefined') {
        console.error(`Warning: window.history is not available.`);
        return;
      }

      if (typeof window.history[method] === 'undefined') {
        console.error(`Warning: window.history.${method} is not available`);
        return;
      }
    }

    if (method !== 'pushState' || utils_1.getURL() !== as) {
      window.history[method]({
        url,
        as,
        options
      }, // Most browsers currently ignores this parameter, although they may use it in the future.
      // Passing the empty string here should be safe against future changes to the method.
      // https://developer.mozilla.org/en-US/docs/Web/API/History/replaceState
      '', as);
    }
  }

  getRouteInfo(route, pathname, query, as, shallow = false) {
    const cachedRouteInfo = this.components[route]; // If there is a shallow route transition possible
    // If the route is already rendered on the screen.

    if (shallow && cachedRouteInfo && this.route === route) {
      return Promise.resolve(cachedRouteInfo);
    }

    const handleError = (err, loadErrorFail) => {
      return new Promise(resolve => {
        if (err.code === 'PAGE_LOAD_ERROR' || loadErrorFail) {
          // If we can't load the page it could be one of following reasons
          //  1. Page doesn't exists
          //  2. Page does exist in a different zone
          //  3. Internal error while loading the page
          // So, doing a hard reload is the proper way to deal with this.
          window.location.href = as; // Changing the URL doesn't block executing the current code path.
          // So, we need to mark it as a cancelled error and stop the routing logic.

          err.cancelled = true; // @ts-ignore TODO: fix the control flow here

          return resolve({
            error: err
          });
        }

        if (err.cancelled) {
          // @ts-ignore TODO: fix the control flow here
          return resolve({
            error: err
          });
        }

        resolve(this.fetchComponent('/_error').then(res => {
          const {
            page: Component
          } = res;
          const routeInfo = {
            Component,
            err
          };
          return new Promise(resolve => {
            this.getInitialProps(Component, {
              err,
              pathname,
              query
            }).then(props => {
              routeInfo.props = props;
              routeInfo.error = err;
              resolve(routeInfo);
            }, gipErr => {
              console.error('Error in error page `getInitialProps`: ', gipErr);
              routeInfo.error = err;
              routeInfo.props = {};
              resolve(routeInfo);
            });
          });
        }).catch(err => handleError(err, true)));
      });
    };

    return new Promise((resolve, reject) => {
      if (cachedRouteInfo) {
        return resolve(cachedRouteInfo);
      }

      this.fetchComponent(route).then(res => resolve({
        Component: res.page,
        __N_SSG: res.mod.__N_SSG,
        __N_SSP: res.mod.__N_SSP
      }), reject);
    }).then(routeInfo => {
      const {
        Component,
        __N_SSG,
        __N_SSP
      } = routeInfo;

      if (true) {
        const {
          isValidElementType
        } = __webpack_require__(/*! react-is */ "react-is");

        if (!isValidElementType(Component)) {
          throw new Error(`The default export is not a React Component in page: "${pathname}"`);
        }
      }

      return this._getData(() => __N_SSG ? this._getStaticData(as) : __N_SSP ? this._getServerData(as) : this.getInitialProps(Component, // we provide AppTree later so this needs to be `any`
      {
        pathname,
        query,
        asPath: as
      })).then(props => {
        routeInfo.props = props;
        this.components[route] = routeInfo;
        return routeInfo;
      });
    }).catch(handleError);
  }

  set(route, pathname, query, as, data) {
    this.isFallback = false;
    this.route = route;
    this.pathname = pathname;
    this.query = query;
    this.asPath = as;
    this.notify(data);
  }
  /**
   * Callback to execute before replacing router state
   * @param cb callback to be executed
   */


  beforePopState(cb) {
    this._bps = cb;
  }

  onlyAHashChange(as) {
    if (!this.asPath) return false;
    const [oldUrlNoHash, oldHash] = this.asPath.split('#');
    const [newUrlNoHash, newHash] = as.split('#'); // Makes sure we scroll to the provided hash if the url/hash are the same

    if (newHash && oldUrlNoHash === newUrlNoHash && oldHash === newHash) {
      return true;
    } // If the urls are change, there's more than a hash change


    if (oldUrlNoHash !== newUrlNoHash) {
      return false;
    } // If the hash has changed, then it's a hash only change.
    // This check is necessary to handle both the enter and
    // leave hash === '' cases. The identity case falls through
    // and is treated as a next reload.


    return oldHash !== newHash;
  }

  scrollToHash(as) {
    const [, hash] = as.split('#'); // Scroll to top if the hash is just `#` with no value

    if (hash === '') {
      window.scrollTo(0, 0);
      return;
    } // First we check if the element by id is found


    const idEl = document.getElementById(hash);

    if (idEl) {
      idEl.scrollIntoView();
      return;
    } // If there's no element with the id, we check the `name` property
    // To mirror browsers


    const nameEl = document.getElementsByName(hash)[0];

    if (nameEl) {
      nameEl.scrollIntoView();
    }
  }

  urlIsNew(asPath) {
    return this.asPath !== asPath;
  }
  /**
   * Prefetch page code, you may wait for the data during page rendering.
   * This feature only works in production!
   * @param url the href of prefetched page
   * @param asPath the as path of the prefetched page
   */


  prefetch(url, asPath = url, options = {}) {
    return new Promise((resolve, reject) => {
      const {
        pathname,
        protocol
      } = url_1.parse(url);

      if (!pathname || protocol) {
        if (true) {
          throw new Error(`Invalid href passed to router: ${url} https://err.sh/zeit/next.js/invalid-href-passed`);
        }

        return;
      } // Prefetch is not supported in development mode because it would trigger on-demand-entries


      if (true) {
        return;
      }

      const route = delBasePath(toRoute(pathname));
      Promise.all([this.pageLoader.prefetchData(url, delBasePath(asPath)), this.pageLoader[options.priority ? 'loadPage' : 'prefetch'](route)]).then(() => resolve(), reject);
    });
  }

  async fetchComponent(route) {
    let cancelled = false;

    const cancel = this.clc = () => {
      cancelled = true;
    };

    route = delBasePath(route);
    const componentResult = await this.pageLoader.loadPage(route);

    if (cancelled) {
      const error = new Error(`Abort fetching component for route: "${route}"`);
      error.cancelled = true;
      throw error;
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    return componentResult;
  }

  _getData(fn) {
    let cancelled = false;

    const cancel = () => {
      cancelled = true;
    };

    this.clc = cancel;
    return fn().then(data => {
      if (cancel === this.clc) {
        this.clc = null;
      }

      if (cancelled) {
        const err = new Error('Loading initial props cancelled');
        err.cancelled = true;
        throw err;
      }

      return data;
    });
  }

  getInitialProps(Component, ctx) {
    const {
      Component: App
    } = this.components['/_app'];

    const AppTree = this._wrapApp(App);

    ctx.AppTree = AppTree;
    return utils_1.loadGetInitialProps(App, {
      AppTree,
      Component,
      router: this,
      ctx
    });
  }

  abortComponentLoad(as) {
    if (this.clc) {
      const e = new Error('Route Cancelled');
      e.cancelled = true;
      Router.events.emit('routeChangeError', e, as);
      this.clc();
      this.clc = null;
    }
  }

  notify(data) {
    this.sub(data, this.components['/_app'].Component);
  }

}

exports.default = Router;
Router.events = mitt_1.default();

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js":
/*!***************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
}); // Identify /[param]/ in route string

const TEST_ROUTE = /\/\[[^/]+?\](?=\/|$)/;

function isDynamicRoute(route) {
  return TEST_ROUTE.test(route);
}

exports.isDynamicRoute = isDynamicRoute;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js":
/*!******************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteMatcher(routeRegex) {
  const {
    re,
    groups
  } = routeRegex;
  return pathname => {
    const routeMatch = re.exec(pathname);

    if (!routeMatch) {
      return false;
    }

    const decode = param => {
      try {
        return decodeURIComponent(param);
      } catch (_) {
        const err = new Error('failed to decode param');
        err.code = 'DECODE_FAILED';
        throw err;
      }
    };

    const params = {};
    Object.keys(groups).forEach(slugName => {
      const g = groups[slugName];
      const m = routeMatch[g.pos];

      if (m !== undefined) {
        params[slugName] = ~m.indexOf('/') ? m.split('/').map(entry => decode(entry)) : g.repeat ? [decode(m)] : decode(m);
      }
    });
    return params;
  };
}

exports.getRouteMatcher = getRouteMatcher;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/route-regex.js":
/*!****************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/route-regex.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteRegex(normalizedRoute) {
  // Escape all characters that could be considered RegEx
  const escapedRoute = (normalizedRoute.replace(/\/$/, '') || '/').replace(/[|\\{}()[\]^$+*?.-]/g, '\\$&');
  const groups = {};
  let groupIndex = 1;
  const parameterizedRoute = escapedRoute.replace(/\/\\\[([^/]+?)\\\](?=\/|$)/g, (_, $1) => {
    const isCatchAll = /^(\\\.){3}/.test($1);
    groups[$1 // Un-escape key
    .replace(/\\([|\\{}()[\]^$+*?.-])/g, '$1').replace(/^\.{3}/, '') // eslint-disable-next-line no-sequences
    ] = {
      pos: groupIndex++,
      repeat: isCatchAll
    };
    return isCatchAll ? '/(.+?)' : '/([^/]+?)';
  });
  return {
    re: new RegExp('^' + parameterizedRoute + '(?:/)?$', 'i'),
    groups
  };
}

exports.getRouteRegex = getRouteRegex;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/utils.js":
/*!*********************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/utils.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__(/*! url */ "url");
/**
 * Utils
 */


function execOnce(fn) {
  let used = false;
  let result;
  return (...args) => {
    if (!used) {
      used = true;
      result = fn(...args);
    }

    return result;
  };
}

exports.execOnce = execOnce;

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

exports.getLocationOrigin = getLocationOrigin;

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

exports.getURL = getURL;

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

exports.getDisplayName = getDisplayName;

function isResSent(res) {
  return res.finished || res.headersSent;
}

exports.isResSent = isResSent;

async function loadGetInitialProps(App, ctx) {
  var _a;

  if (true) {
    if ((_a = App.prototype) === null || _a === void 0 ? void 0 : _a.getInitialProps) {
      const message = `"${getDisplayName(App)}.getInitialProps()" is defined as an instance method - visit https://err.sh/zeit/next.js/get-initial-props-as-an-instance-method for more information.`;
      throw new Error(message);
    }
  } // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!App.getInitialProps) {
    if (ctx.ctx && ctx.Component) {
      // @ts-ignore pageProps default
      return {
        pageProps: await loadGetInitialProps(ctx.Component, ctx.ctx)
      };
    }

    return {};
  }

  const props = await App.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(App)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (true) {
    if (Object.keys(props).length === 0 && !ctx.ctx) {
      console.warn(`${getDisplayName(App)} returned an empty object from \`getInitialProps\`. This de-optimizes and prevents automatic static optimization. https://err.sh/zeit/next.js/empty-object-getInitialProps`);
    }
  }

  return props;
}

exports.loadGetInitialProps = loadGetInitialProps;
exports.urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];

function formatWithValidation(url, options) {
  if (true) {
    if (url !== null && typeof url === 'object') {
      Object.keys(url).forEach(key => {
        if (exports.urlObjectKeys.indexOf(key) === -1) {
          console.warn(`Unknown key passed via urlObject into url.format: ${key}`);
        }
      });
    }
  }

  return url_1.format(url, options);
}

exports.formatWithValidation = formatWithValidation;
exports.SP = typeof performance !== 'undefined';
exports.ST = exports.SP && typeof performance.mark === 'function' && typeof performance.measure === 'function';

/***/ }),

/***/ "./node_modules/next/dist/pages/_app.js":
/*!**********************************************!*\
  !*** ./node_modules/next/dist/pages/_app.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.Container = Container;
exports.createUrl = createUrl;
exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _utils = __webpack_require__(/*! ../next-server/lib/utils */ "../next-server/lib/utils");

exports.AppInitialProps = _utils.AppInitialProps;
/**
* `App` component is used for initialize of pages. It allows for overwriting and full control of the `page` initialization.
* This allows for keeping state between navigation, custom error handling, injecting additional data.
*/

async function appGetInitialProps(_ref) {
  var {
    Component,
    ctx
  } = _ref;
  var pageProps = await (0, _utils.loadGetInitialProps)(Component, ctx);
  return {
    pageProps
  };
}

class App extends _react.default.Component {
  // Kept here for backwards compatibility.
  // When someone ended App they could call `super.componentDidCatch`.
  // @deprecated This method is no longer needed. Errors are caught at the top level
  componentDidCatch(error, _errorInfo) {
    throw error;
  }

  render() {
    var {
      router,
      Component,
      pageProps,
      __N_SSG,
      __N_SSP
    } = this.props;
    return _react.default.createElement(Component, Object.assign({}, pageProps, // we don't add the legacy URL prop if it's using non-legacy
    // methods like getStaticProps and getServerSideProps
    !(__N_SSG || __N_SSP) ? {
      url: createUrl(router)
    } : {}));
  }

}

exports.default = App;
App.origGetInitialProps = appGetInitialProps;
App.getInitialProps = appGetInitialProps;
var warnContainer;
var warnUrl;

if (true) {
  warnContainer = (0, _utils.execOnce)(() => {
    console.warn("Warning: the `Container` in `_app` has been deprecated and should be removed. https://err.sh/zeit/next.js/app-container-deprecated");
  });
  warnUrl = (0, _utils.execOnce)(() => {
    console.error("Warning: the 'url' property is deprecated. https://err.sh/zeit/next.js/url-deprecated");
  });
} // @deprecated noop for now until removal


function Container(p) {
  if (true) warnContainer();
  return p.children;
}

function createUrl(router) {
  // This is to make sure we don't references the router object at call time
  var {
    pathname,
    asPath,
    query
  } = router;
  return {
    get query() {
      if (true) warnUrl();
      return query;
    },

    get pathname() {
      if (true) warnUrl();
      return pathname;
    },

    get asPath() {
      if (true) warnUrl();
      return asPath;
    },

    back: () => {
      if (true) warnUrl();
      router.back();
    },
    push: (url, as) => {
      if (true) warnUrl();
      return router.push(url, as);
    },
    pushTo: (href, as) => {
      if (true) warnUrl();
      var pushRoute = as ? href : '';
      var pushUrl = as || href;
      return router.push(pushRoute, pushUrl);
    },
    replace: (url, as) => {
      if (true) warnUrl();
      return router.replace(url, as);
    },
    replaceTo: (href, as) => {
      if (true) warnUrl();
      var replaceRoute = as ? href : '';
      var replaceUrl = as || href;
      return router.replace(replaceRoute, replaceUrl);
    }
  };
}

/***/ }),

/***/ "./node_modules/next/link.js":
/*!***********************************!*\
  !*** ./node_modules/next/link.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/client/link */ "./node_modules/next/dist/client/link.js")


/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/app */ "./node_modules/next/app.js");
/* harmony import */ var next_app__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_app__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _public_styles_index_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../public/styles/index.scss */ "./public/styles/index.scss");
/* harmony import */ var _public_styles_index_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_public_styles_index_scss__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _panelAdmin_screen_PanelScreen__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../panelAdmin/screen/PanelScreen */ "./panelAdmin/screen/PanelScreen.js");
/* harmony import */ var _website_screen_WebsiteScreen__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../website/screen/WebsiteScreen */ "./website/screen/WebsiteScreen.js");
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next-redux-wrapper */ "next-redux-wrapper");
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_redux_wrapper__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../store */ "./store/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _website_baseComponents_ErrorHandler__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../website/baseComponents/ErrorHandler */ "./website/baseComponents/ErrorHandler/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/pages/_app.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }











class RimtalApp extends next_app__WEBPACK_IMPORTED_MODULE_1___default.a {
  static async getInitialProps({
    Component,
    ctx
  }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps({
        ctx
      });
    }

    return {
      pageProps
    };
  }

  render() {
    const {
      Component,
      pageProps,
      store,
      router
    } = this.props;
    let body;

    if (router.asPath.includes("/panelAdmin")) {
      body = __jsx(_panelAdmin_screen_PanelScreen__WEBPACK_IMPORTED_MODULE_4__["default"], {
        router: router,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 25,
          columnNumber: 9
        }
      }, __jsx(Component, _extends({}, pageProps, {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 26,
          columnNumber: 11
        }
      })));
    } else {
      body = __jsx(_website_screen_WebsiteScreen__WEBPACK_IMPORTED_MODULE_5__["default"], {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 31,
          columnNumber: 9
        }
      }, __jsx(Component, _extends({}, pageProps, {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 32,
          columnNumber: 11
        }
      })));
    }

    return __jsx("div", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37,
        columnNumber: 7
      }
    }, __jsx(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38,
        columnNumber: 9
      }
    }, __jsx("title", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39,
        columnNumber: 11
      }
    }, "Rimtal"), __jsx("meta", {
      name: "description",
      content: "\u062C\u0627\u0645\u0639 \u062A\u0631\u06CC\u0646 \u067E\u0644\u062A\u0641\u0631\u0645 \u0645\u0648\u0633\u06CC\u0642\u06CC \u0627\u06CC\u0631\u0627\u0646 .",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40,
        columnNumber: 11
      }
    }), __jsx("meta", {
      name: "keywords",
      content: "\u0631\u06CC\u0645\u062A\u0627\u0644\u060Crimtal,RIMTAL ,\u0645\u0648\u0633\u06CC\u0642\u06CC \u0648 ,IPTV,\u0645\u0648\u0633\u06CC\u0642\u06CC,\u067E\u0631\u062F\u0627\u062E\u062A \u0627\u062C\u062A\u0645\u0627\u0639\u06CC\u060C\u067E\u062E\u0634 \u0632\u0646\u062F\u0647\u060C\u0627\u067E\u0644\u06CC\u06A9\u06CC\u0634\u0646,\u0633\u0631\u06AF\u0631\u0645\u06CC,",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41,
        columnNumber: 11
      }
    }), __jsx("meta", {
      content: "width=device-width, initial-scale=1",
      name: "viewport",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 45,
        columnNumber: 11
      }
    }), __jsx("script", {
      src: "https://code.jquery.com/jquery-3.4.1.slim.min.js",
      integrity: "sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n",
      crossOrigin: "anonymous",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46,
        columnNumber: 11
      }
    }), __jsx("script", {
      src: "https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js",
      integrity: "sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo",
      crossOrigin: "anonymous",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51,
        columnNumber: 11
      }
    }), __jsx("script", {
      src: "https://code.jquery.com/jquery-3.3.1.slim.min.js",
      integrity: "sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo",
      crossOrigin: "anonymous",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 11
      }
    }), __jsx("script", {
      src: "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js",
      integrity: "sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6",
      crossOrigin: "anonymous",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 63,
        columnNumber: 11
      }
    }), __jsx("link", {
      href: "/styles/css/styles.css",
      rel: "stylesheet",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70,
        columnNumber: 11
      }
    })), __jsx("div", {
      className: "base-page",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 9
      }
    }, __jsx(react_redux__WEBPACK_IMPORTED_MODULE_8__["Provider"], {
      store: store,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 73,
        columnNumber: 11
      }
    }, body, " ", __jsx(_website_baseComponents_ErrorHandler__WEBPACK_IMPORTED_MODULE_9__["default"], {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 74,
        columnNumber: 20
      }
    }))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (next_redux_wrapper__WEBPACK_IMPORTED_MODULE_6___default()(_store__WEBPACK_IMPORTED_MODULE_7__["default"])(RimtalApp));

/***/ }),

/***/ "./panelAdmin/api/Delete/index.js":
/*!****************************************!*\
  !*** ./panelAdmin/api/Delete/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// import owner from "./owner";
// import discount from "./discount";
// import club from "./club";
// import slider from "./slider";
// import banner from "./banner";
// import category from "./category";
const Delete = {};
/* harmony default export */ __webpack_exports__["default"] = (Delete);

/***/ }),

/***/ "./panelAdmin/api/Get/albums.js":
/*!**************************************!*\
  !*** ./panelAdmin/api/Get/albums.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const albums = async ({
  page
}) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(strings.ALBUM + "/" + page);
};

/* harmony default export */ __webpack_exports__["default"] = (albums);

/***/ }),

/***/ "./panelAdmin/api/Get/artistSearch.js":
/*!********************************************!*\
  !*** ./panelAdmin/api/Get/artistSearch.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const artistSearch = async ({
  title,
  page
}) => {
  console.log({
    cattitle: title
  });
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(strings.ARTIST_SEARCH + "/" + title + "/" + page); // .then((artistSearch) => {
  //   // console.log({ artistSearch });
  //   returnData(artistSearch.data);
  //   // loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   // else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ __webpack_exports__["default"] = (artistSearch);

/***/ }),

/***/ "./panelAdmin/api/Get/artists.js":
/*!***************************************!*\
  !*** ./panelAdmin/api/Get/artists.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const artists = async ({
  page
}) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(strings.ARTIST + "/" + page); // .then((artists) => {
  //   console.log({ artists });
  //   // return artists;
  //   // returnData(artists.data);
  //   // loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ __webpack_exports__["default"] = (artists);

/***/ }),

/***/ "./panelAdmin/api/Get/country.js":
/*!***************************************!*\
  !*** ./panelAdmin/api/Get/country.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const country = async ({
  page
}) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.COUNTRY;
  const axiosData = page ? strings + "/" + page : strings;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(axiosData); // .then((country) => {
  //   console.log({ country });
  //   returnData(country.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ __webpack_exports__["default"] = (country);

/***/ }),

/***/ "./panelAdmin/api/Get/countrySearch.js":
/*!*********************************************!*\
  !*** ./panelAdmin/api/Get/countrySearch.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const countrySearch = async (title, page, returnData) => {
  console.log({
    title,
    page
  });
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.COUNTRY;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(strings + "/" + title + "/" + page).then(countrySearch => {
    // console.log({ countrySearch });
    returnData(countrySearch.data); // loading(false);
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ __webpack_exports__["default"] = (countrySearch);

/***/ }),

/***/ "./panelAdmin/api/Get/genreSearch.js":
/*!*******************************************!*\
  !*** ./panelAdmin/api/Get/genreSearch.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const genreSearch = async (param, page, returnData) => {
  // console.log({ param, page, returnData });
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(strings.GENRES + "/" + param + "/" + page).then(genreSearch => {
    console.log({
      genreSearch
    });
    returnData(genreSearch.data); // loading(false);
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ __webpack_exports__["default"] = (genreSearch);

/***/ }),

/***/ "./panelAdmin/api/Get/genres.js":
/*!**************************************!*\
  !*** ./panelAdmin/api/Get/genres.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const genres = async ({
  page
}) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  const axiosData = page ? strings.GENRES + "/" + page : strings.GENRES;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(axiosData); // .then((genres) => {
  //   console.log({ genres });
  //   returnData(genres.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ __webpack_exports__["default"] = (genres);

/***/ }),

/***/ "./panelAdmin/api/Get/hashtag.js":
/*!***************************************!*\
  !*** ./panelAdmin/api/Get/hashtag.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const hashtag = async ({
  page
}) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.HASHTAG;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(strings + "/" + page); // .then((hashtag) => {
  //   console.log({ hashtag });
  //   returnData(hashtag.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ __webpack_exports__["default"] = (hashtag);

/***/ }),

/***/ "./panelAdmin/api/Get/index.js":
/*!*************************************!*\
  !*** ./panelAdmin/api/Get/index.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _genres__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./genres */ "./panelAdmin/api/Get/genres.js");
/* harmony import */ var _genreSearch__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./genreSearch */ "./panelAdmin/api/Get/genreSearch.js");
/* harmony import */ var _artistSearch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./artistSearch */ "./panelAdmin/api/Get/artistSearch.js");
/* harmony import */ var _country__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./country */ "./panelAdmin/api/Get/country.js");
/* harmony import */ var _countrySearch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./countrySearch */ "./panelAdmin/api/Get/countrySearch.js");
/* harmony import */ var _instrument__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./instrument */ "./panelAdmin/api/Get/instrument.js");
/* harmony import */ var _instrumentSearch__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./instrumentSearch */ "./panelAdmin/api/Get/instrumentSearch.js");
/* harmony import */ var _artists__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./artists */ "./panelAdmin/api/Get/artists.js");
/* harmony import */ var _mood__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./mood */ "./panelAdmin/api/Get/mood.js");
/* harmony import */ var _hashtag__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./hashtag */ "./panelAdmin/api/Get/hashtag.js");
/* harmony import */ var _albums__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./albums */ "./panelAdmin/api/Get/albums.js");
// import categoreis from "./categoreis";
// import sliders from "./sliders";
// import Owners from "./Owners";
// import discounts from "./discounts";
// import discount from "./discount";
// import categoreisSearch from "./categoreisSearch";
// import ownersSearch from "./ownersSearch";
// import discountSearch from "./discountSearch";
 // import club from "./club";

 // import category from "./category";
// import members from "./members";
// import banners from "./banners";
// import scenarios from "./scenarios";





 // import gallery from "./gallery";
// import song from "./song";





const get = {
  instrument: _instrument__WEBPACK_IMPORTED_MODULE_5__["default"],
  instrumentSearch: _instrumentSearch__WEBPACK_IMPORTED_MODULE_6__["default"],
  // categoreis,
  // sliders,
  // Owners,
  // discounts,
  // categoreisSearch,
  // category,
  // ownersSearch,
  // discountSearch,
  genres: _genres__WEBPACK_IMPORTED_MODULE_0__["default"],
  genreSearch: _genreSearch__WEBPACK_IMPORTED_MODULE_1__["default"],
  // discount,
  // club,
  // members,
  // banners,
  // scenarios,
  artistSearch: _artistSearch__WEBPACK_IMPORTED_MODULE_2__["default"],
  country: _country__WEBPACK_IMPORTED_MODULE_3__["default"],
  countrySearch: _countrySearch__WEBPACK_IMPORTED_MODULE_4__["default"],
  // gallery,
  // song,
  artists: _artists__WEBPACK_IMPORTED_MODULE_7__["default"],
  mood: _mood__WEBPACK_IMPORTED_MODULE_8__["default"],
  hashtag: _hashtag__WEBPACK_IMPORTED_MODULE_9__["default"],
  albums: _albums__WEBPACK_IMPORTED_MODULE_10__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (get);

/***/ }),

/***/ "./panelAdmin/api/Get/instrument.js":
/*!******************************************!*\
  !*** ./panelAdmin/api/Get/instrument.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const instrument = async ({
  page
}) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.INSTRUMENT;
  const axiosData = page ? strings + "/" + page : strings;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(axiosData); // .then((instrument) => {
  //   console.log({ instrument });
  //   returnData(instrument.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ __webpack_exports__["default"] = (instrument);

/***/ }),

/***/ "./panelAdmin/api/Get/instrumentSearch.js":
/*!************************************************!*\
  !*** ./panelAdmin/api/Get/instrumentSearch.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const instrumentSearch = async (param, page, returnData) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.INSTRUMENT;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(strings + "/" + param + "/" + page).then(instrumentSearch => {
    // console.log({ instrumentSearch });
    returnData(instrumentSearch.data); // loading(false);
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ __webpack_exports__["default"] = (instrumentSearch);

/***/ }),

/***/ "./panelAdmin/api/Get/mood.js":
/*!************************************!*\
  !*** ./panelAdmin/api/Get/mood.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const mood = async ({
  page
}) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.MOOD;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(strings + "/" + page); // .then((mood) => {
  //   console.log({ mood });
  //   returnData(mood.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ __webpack_exports__["default"] = (mood);

/***/ }),

/***/ "./panelAdmin/api/Patch/index.js":
/*!***************************************!*\
  !*** ./panelAdmin/api/Patch/index.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// import category from "./category";
// import editDiscount from "./discount";
// import club from "./club";
// import owner from "./owner";
// import slider from "./slider";
// import banner from "./banner";
const Patch = {// category,
  // editDiscount,
  // club,
  // owner,
  // slider,
  // banner,
};
/* harmony default export */ __webpack_exports__["default"] = (Patch);

/***/ }),

/***/ "./panelAdmin/api/Post/artist.js":
/*!***************************************!*\
  !*** ./panelAdmin/api/Post/artist.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const artist = async (param, setLoading) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  let URL = strings.ARTIST;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (artist);

/***/ }),

/***/ "./panelAdmin/api/Post/country.js":
/*!****************************************!*\
  !*** ./panelAdmin/api/Post/country.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const country = async param => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  let URL = strings.COUNTRY;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (country);

/***/ }),

/***/ "./panelAdmin/api/Post/genres.js":
/*!***************************************!*\
  !*** ./panelAdmin/api/Post/genres.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const genres = async param => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  let URL = strings.GENRES;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (genres);

/***/ }),

/***/ "./panelAdmin/api/Post/hashtag.js":
/*!****************************************!*\
  !*** ./panelAdmin/api/Post/hashtag.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const hashtag = async param => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  let URL = strings.HASHTAG;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (hashtag);

/***/ }),

/***/ "./panelAdmin/api/Post/imageUpload.js":
/*!********************************************!*\
  !*** ./panelAdmin/api/Post/imageUpload.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const imageUpload = async (files, setLoading, type, setState, imageName) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_2__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_2__["default"].values.apiString;
  console.log({
    files,
    setLoading,
    type,
    setState,
    imageName
  });
  setLoading(true); // ============================================= const

  const CancelToken = axios__WEBPACK_IMPORTED_MODULE_1___default.a.CancelToken;
  const source = CancelToken.source();
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total);
      setState(prev => _objectSpread({}, prev, {
        progressPercentImage: percentCompleted
      }));
    },
    cancelToken: source.token
  };
  const URL = strings.IMAGE_UPLOAD;
  const formData = new FormData();
  formData.append("imageName", imageName);
  formData.append("imageType", type);
  formData.append("image", files); //=============================================== axios

  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].post(URL, formData, settings).then(Response => {
    console.log({
      Response
    });
    setLoading(false);
    return Response.data;
  }).catch(error => {
    console.log({
      error
    });
    setLoading(false);
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (imageUpload);

/***/ }),

/***/ "./panelAdmin/api/Post/index.js":
/*!**************************************!*\
  !*** ./panelAdmin/api/Post/index.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _imageUpload__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./imageUpload */ "./panelAdmin/api/Post/imageUpload.js");
/* harmony import */ var _artist__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./artist */ "./panelAdmin/api/Post/artist.js");
/* harmony import */ var _voiceUpload__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./voiceUpload */ "./panelAdmin/api/Post/voiceUpload.js");
/* harmony import */ var _videoUpload__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./videoUpload */ "./panelAdmin/api/Post/videoUpload.js");
/* harmony import */ var _genres__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./genres */ "./panelAdmin/api/Post/genres.js");
/* harmony import */ var _country__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./country */ "./panelAdmin/api/Post/country.js");
/* harmony import */ var _instrument__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./instrument */ "./panelAdmin/api/Post/instrument.js");
/* harmony import */ var _mood__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./mood */ "./panelAdmin/api/Post/mood.js");
/* harmony import */ var _hashtag__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./hashtag */ "./panelAdmin/api/Post/hashtag.js");
 // import owner from "./owner";
// import category from "./category";
// import slider from "./slider";
// import login from "./login";
// import album from "./album";
// import club from "./club";
// import banner from "./banner";








 // import gallery from "./gallery";

const post = {
  imageUpload: _imageUpload__WEBPACK_IMPORTED_MODULE_0__["default"],
  // owner,
  // category,
  // slider,
  // login,
  // album,
  // club,
  // banner,
  artist: _artist__WEBPACK_IMPORTED_MODULE_1__["default"],
  voiceUpload: _voiceUpload__WEBPACK_IMPORTED_MODULE_2__["default"],
  videoUpload: _videoUpload__WEBPACK_IMPORTED_MODULE_3__["default"],
  genres: _genres__WEBPACK_IMPORTED_MODULE_4__["default"],
  country: _country__WEBPACK_IMPORTED_MODULE_5__["default"],
  instrument: _instrument__WEBPACK_IMPORTED_MODULE_6__["default"],
  // gallery,
  mood: _mood__WEBPACK_IMPORTED_MODULE_7__["default"],
  hashtag: _hashtag__WEBPACK_IMPORTED_MODULE_8__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (post);

/***/ }),

/***/ "./panelAdmin/api/Post/instrument.js":
/*!*******************************************!*\
  !*** ./panelAdmin/api/Post/instrument.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const instrument = async (param, setLoading) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  let URL = strings.INSTRUMENT;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (instrument);

/***/ }),

/***/ "./panelAdmin/api/Post/mood.js":
/*!*************************************!*\
  !*** ./panelAdmin/api/Post/mood.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const mood = async param => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  let URL = strings.MOOD;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].post(URL, param).then(Response => {
    console.log({
      Response
    });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (mood);

/***/ }),

/***/ "./panelAdmin/api/Post/videoUpload.js":
/*!********************************************!*\
  !*** ./panelAdmin/api/Post/videoUpload.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const videoUpload = async (files, setLoading, type, setState, cancelUpload) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_2__["default"].utils.toastify;
  let Strings = ___WEBPACK_IMPORTED_MODULE_2__["default"].values.ApiString; // //console.log({ update });

  setLoading(true); // ============================================= const

  const CancelToken = axios__WEBPACK_IMPORTED_MODULE_1___default.a.CancelToken;
  const source = CancelToken.source();
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total);
      setState(prev => _objectSpread({}, prev, {
        progressPercentVideo: percentCompleted
      }));
    },
    cancelToken: source.token
  };
  const formData = new FormData();
  const URL = Strings.ApiString.VIDEO_UPLOAD;
  formData.append("video", files); // ============================================== End const ==============================================
  // ============================================== log
  //console.log("Sending ...");
  //console.log({ AxiosCancelUpload: cancelUpload });
  //console.log({ formData });
  // ============================================== End log ==============================================
  // if (cancelUpload) {
  //   source.cancel();
  // }
  //=============================================== axios

  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].post(URL, formData, settings).then(Response => {
    console.log({
      Response
    });
    setLoading(false);
    return Response.data.videoUrl; // update[name] = Response.data.videoUrl;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (videoUpload);

/***/ }),

/***/ "./panelAdmin/api/Post/voiceUpload.js":
/*!********************************************!*\
  !*** ./panelAdmin/api/Post/voiceUpload.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const voiceUpload = async (files, setLoading, type, setState, songName) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_2__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_2__["default"].values.strings;
  setLoading(true);
  console.log({
    files,
    setLoading,
    type,
    setState,
    songName
  }); // ============================================= const

  const CancelToken = axios__WEBPACK_IMPORTED_MODULE_1___default.a.CancelToken;
  const source = CancelToken.source();
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total);
      setState(prev => _objectSpread({}, prev, {
        progressPercentSongs: percentCompleted
      }));
    },
    cancelToken: source.token
  };
  const formData = new FormData();
  formData.append("songName", songName);
  formData.append("song", files);
  const URL = strings.ApiString.SONG_UPLOAD;

  for (var pair of formData.entries()) {
    console.log(pair[0] + ", " + pair[1]);
  } // ============================================== End const ==============================================
  // ============================================== log
  //console.log("Sending ...");
  //console.log({ AxiosCancelUpload: cancelUpload });


  console.log({
    formData
  }); //console.log({ update });
  // ============================================== End log ==============================================
  // if (cancelUpload) {
  //   source.cancel();
  // }
  //=============================================== axios

  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].post(URL, formData, settings).then(Response => {
    console.log({
      Response
    });
    setLoading(false); // update[name] = Response.data.voiceUrl;
    // return Response.data.songUrl;

    return true;
  }).catch(error => {
    console.log({
      error
    });
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (voiceUpload);

/***/ }),

/***/ "./panelAdmin/api/Put/index.js":
/*!*************************************!*\
  !*** ./panelAdmin/api/Put/index.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// import editSection from "./editSection";
// import scenario from "./scenario";
const put = {};
/* harmony default export */ __webpack_exports__["default"] = (put);

/***/ }),

/***/ "./panelAdmin/api/axios-orders.js":
/*!****************************************!*\
  !*** ./panelAdmin/api/axios-orders.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
 // import Cookie from "js-cookie";

const instance = axios__WEBPACK_IMPORTED_MODULE_0___default.a.create({
  baseURL: "https://rimtal.com/api/v1"
});
instance.defaults.headers.common["Authorization"] = "Bearer ";
/* harmony default export */ __webpack_exports__["default"] = (instance);

/***/ }),

/***/ "./panelAdmin/api/index.js":
/*!*********************************!*\
  !*** ./panelAdmin/api/index.js ***!
  \*********************************/
/*! exports provided: get, post, put, patch, deletes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Get__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Get */ "./panelAdmin/api/Get/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "get", function() { return _Get__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _Put__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Put */ "./panelAdmin/api/Put/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "put", function() { return _Put__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _Post__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Post */ "./panelAdmin/api/Post/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "post", function() { return _Post__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _Patch__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Patch */ "./panelAdmin/api/Patch/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "patch", function() { return _Patch__WEBPACK_IMPORTED_MODULE_3__["default"]; });

/* harmony import */ var _Delete__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Delete */ "./panelAdmin/api/Delete/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "deletes", function() { return _Delete__WEBPACK_IMPORTED_MODULE_4__["default"]; });








/***/ }),

/***/ "./panelAdmin/assets/Images/icons/user.png":
/*!*************************************************!*\
  !*** ./panelAdmin/assets/Images/icons/user.png ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/user-4ab57b30d27e4dd0fa03a9c0a9273140.png";

/***/ }),

/***/ "./panelAdmin/assets/img/Rimtal.png":
/*!******************************************!*\
  !*** ./panelAdmin/assets/img/Rimtal.png ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAXCAYAAADHqJcNAAAGwUlEQVRogb2aeahVRRzHP/pepZlttlFR2WpiWUm9tL2o1GwjbKesBNMwzIoMaTda7I8kWohKIgiL9sUWyHYqLZdS20XL1MQ2l3pu78UPvhM/pjnnnnPufX7gcu/MnTNnzpzf/LaZToz5g4i9gBOBXsCW8Z9iPbAY+Aj4LKON50zgGuAd4C5gQ0a7PsBoYD4wqUC/RdkKuAPYBrhZYw/YMw4DdmrQvX4CntIcpWgGTgUGAUcAPYF5wHTgFc1pWYZpfp8B7gbaynbgBWEX4FbgAmDrEn28C9wOvJfxv03+t8DOKh8LfJho1wR8Ahyu8nnAsyXGkcd9wLX6f4qeMfAQMLJB9wnYfNySqB+s+iNyrn0TuAH4suC9TIAXAN1UPgSYU3bAnfW9LzANGFFSCIwTgLeASzP+7+4GaWyf0a5ZwhgwoexSciwprM/hrn6PqM2BDbhHzAGJuiuBV2sIgTFQi+r4gvey97WFK/eoMuBmvaiXExNiq3hl4pp2YHNgbyc0Vn4cWJTQDNZ+oytnqa32yGTYeIZKzdbDSGmlQGyWbPXdnzOB7RLeHVU2lb9Q9Sl+kUbwmBl4OKoz8/QpsALYDTga2E7/2ffzQH/guxrP3qb5bXbl0tjFY4He7kLTDOOBmcC6jA47aWWZlI9Tnan2B6Xa/64ymATjNSFV+zO1OSoxdo/Z5gHRqvLYC78amKi6pUALsCbRl7E2Km+reQm0yTyYSfrd1e8J3ARcobIJ3z3A2TWeMRbI1Jhq0lkvM2CO3xBJapYQhJvb6r8RuM3V95apqEr8UKZiL6+jP3OidijYdm3GZ11iLlpVl7om5lw5hAFbOBMiIUDzaSbsSVd3hmx+LbK0U2E6R3bZXuo/Jfu4E/jBlU+uYzypBxob+RhF2UYruRHEq6ypRJ+D3O9ZclzzGOdMcmc5zWWorBECFj7MqNDH+iiE7JnTtgo9I61VlBGyvTFVJsoLaJnrzdnt68ovFFi9y4D3XXlIDcGrWxsQCUJrTnxfC69FNq/YR94DjY0cvlp064CQsAr7RFHKzIJ9POd+9yoQ2VQV1P/wgtCpaifRdQ2RUOADl5TZVSu8KJcoMYaEe0nGWItS1SE71K1m8ym+L3jd2848mEN/Wk7b9kYLQj3kOZZl8A80RZnIwPUuKZVHF5c8Qkmq1xo0vrL4nMFCfYqwTA574OycF9yQqKG5QJt9ldzYXdKdWvH9q9w8ge+7Vd71QJXN+79KKeI8hkolB+5RNjOwKX2Eg9zv2Tlp5xQmvKeovp9MxNflhl2cPEHoosTIqJJeexmPOg9LdH0srXCS2pndfyRS9Z4tXF4DTdzrkede1fyVxfY39nPXTC95vY37Xr2HZoXlWYLQYabBOntM6rhs6JbKRlYhjG2Cu9a0wqNA14z+JkbJsbBx1SgTGCgy2fvLtwmUjcgWyKwFshJLsY9QiSyNYLHrRa7cpoGtrzEJy7XL10gsZf0ScJb6PE2bVvcqbN0gFTxGu3qBOS45U/eKiSjSx2Gu3coCqeIUb7gE3VHKPi6K2sWCUMTc/4/4otChT8sulWDMiPYMUpSxgTF53u9oZdhCJNBPW65rJKTdo75WKUvXmrjPpvIRWtzvhVokZXlL2/ZN0oKDE3sWfwJ/uTkwjTi17I1ilblBpsDHrZO0Alv1ovM+9ZI14YulCeLt1W4JIVgsNfq5q2tUSFuUTtIIgdkVN4Pm6trA6Yk2qyNtc3EVPy0WhHY5XF5T/Fy20w7CDqsco42ZHxO3WKJdxJYo7IzZFBphV/kIgSKHd1K0ySwGjnS7oB6fgOpbILL6H7EgNEnNrHB1w3N25jqS1ISvkvN4sFbcEG3MtOh00zUZEUW9GqFsrH6AoobAF3Xc+xVnkrfL2MuxrfpvXNkE4YmMcxFJYh+hSTc1KbxOdSco923x+It1PFAj+VsbOLMK9rmpTUMf9/tXne2oynyp/mCuzwGejvparRzLVLdoLwPOl4CsdsIbf5spHZ/lYU7UDcMGUos2TN7QyaGyMXFRGu3ddwRFxuUzivPk0FVlg3IKQRCOk2aID5tO0xG8yW5fpqvS3LVY5U2DF4rlOnA6N+pgkOzd5Ch7l4dN3Gbu/7yJ9GNoVGKKyARultMuCz/mWuFZcyQIpc8PJvCauIeEIavdAGn0MhuIy5vdAYkV0Yr8Ssenhiuc3Fb19oIulOPygGxRKkwLbJRghWNtqcMb6N4r3JnGRp1yQnF8eM74QEgRbHc1XPdbjQssFW/RTGjfCO1pEZD1Y+l+w06ZeyfSY6bEoibzo+wdWe7BNIMPz/33EuCJfwFWG3TGQDr4PQAAAABJRU5ErkJggg=="

/***/ }),

/***/ "./panelAdmin/component/Header/HeaderProfile/index.js":
/*!************************************************************!*\
  !*** ./panelAdmin/component/Header/HeaderProfile/index.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _assets_Images_icons_user_png__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../assets/Images/icons/user.png */ "./panelAdmin/assets/Images/icons/user.png");
/* harmony import */ var _assets_Images_icons_user_png__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_assets_Images_icons_user_png__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/component/Header/HeaderProfile/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


 // import "./index.scss";

 // import Cookies from "js-cookie";

const HeaderProfile = () => {
  const {
    0: state,
    1: setState
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    showModal: false,
    clickedComponent: false
  });
  const wrapperRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  const showModalprofile = () => {
    // let noting = ;
    setState(prev => _objectSpread({}, prev, {
      showModal: !state.showModal,
      clickedComponent: true
    }));
  }; // console.log({ state: state.showModal });
  // const logOut = () => Cookies.remove("RimtalToken");


  const adminTitleModal = [{
    title: "پروفایل",
    iconClass: "fas fa-user",
    href: "#",
    onClick: null
  }, {
    title: "پیام ها",
    iconClass: "fas fa-envelope",
    href: "#",
    value: "",
    onClick: null
  }, {
    title: "تنظیمات",
    iconClass: "fas fa-cog",
    href: "#",
    onClick: null
  }, {
    title: "خروج",
    iconClass: " fas fa-sign-out-alt",
    href: "#",
    onClick: null
  }];

  const handleClickOutside = event => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      showModalprofile();
    }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    if (state.showModal) {
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }
  });

  const adminTitleModal_map = __jsx("ul", {
    className: `profile-modal-container ${state.showModal ? "actived" : state.clickedComponent ? "deActive" : "showOutStatic"}`,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62,
      columnNumber: 5
    }
  }, adminTitleModal.map((admin, index) => {
    return __jsx("li", {
      onClick: admin.onClick,
      key: index,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 65,
        columnNumber: 11
      }
    }, __jsx("i", {
      className: admin.iconClass,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 66,
        columnNumber: 13
      }
    }), __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
      href: admin.href,
      as: admin.href,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 68,
        columnNumber: 13
      }
    }, __jsx("a", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69,
        columnNumber: 15
      }
    }, admin.title)), admin.value ? __jsx("span", {
      className: "show-modal-icon-value",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71,
        columnNumber: 28
      }
    }, admin.value) : "");
  }));

  return __jsx("ul", {
    onClick: showModalprofile,
    ref: wrapperRef,
    className: "panel-navbar-profile ",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 78,
      columnNumber: 5
    }
  }, __jsx("li", {
    className: "pointer hoverColorblack normalTransition",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "centerAll",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 80,
      columnNumber: 9
    }
  }, __jsx("i", {
    className: "fas fa-angle-down",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81,
      columnNumber: 11
    }
  })), __jsx("div", {
    className: "admin-profile-name  icon-up-dir ",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83,
      columnNumber: 9
    }
  }, __jsx("span", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 11
    }
  }, "\u0627\u062F\u0645\u06CC\u0646"), " "), __jsx("div", {
    className: "admin-profile-image",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 87,
      columnNumber: 9
    }
  }, __jsx("img", {
    src: _assets_Images_icons_user_png__WEBPACK_IMPORTED_MODULE_1___default.a,
    alt: "profile",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 88,
      columnNumber: 11
    }
  }))), adminTitleModal_map);
};

/* harmony default export */ __webpack_exports__["default"] = (HeaderProfile);

/***/ }),

/***/ "./panelAdmin/component/Header/index.js":
/*!**********************************************!*\
  !*** ./panelAdmin/component/Header/index.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _HeaderProfile__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HeaderProfile */ "./panelAdmin/component/Header/HeaderProfile/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/component/Header/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const Header = ({
  _handelSidebarToggle
}) => {
  const NavbarStore = Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["useSelector"])(state => {
    return state.panelNavbar;
  });
  console.log({
    headerState: NavbarStore
  });
  return __jsx("nav", {
    className: "panelAdmin-navbar-container",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "panel-navbar-box",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "panel-navbar-side-element smallDisplay",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 9
    }
  }, __jsx("i", {
    onClick: _handelSidebarToggle,
    className: "fas fa-bars",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 11
    }
  }), __jsx("span", {
    className: "page-accepted-name",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 11
    }
  }, NavbarStore.pageName)), __jsx("div", {
    className: "panel-navbar-side-element",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 9
    }
  }, __jsx("ul", {
    className: "panel-navbar-notifications",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 11
    }
  }, __jsx("li", {
    className: "pointer hoverColorblack normalTransition",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 13
    }
  }, __jsx("i", {
    className: "icon-search",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 15
    }
  })), __jsx("li", {
    className: "navbar-icon-massege pointer hoverColorblack normalTransition",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "massege-icon",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 15
    }
  }, __jsx("i", {
    className: "far fa-bell",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 17
    }
  }), __jsx("span", {
    className: "show-modal-icon-value",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 17
    }
  }, "4")))), __jsx(_HeaderProfile__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 11
    }
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./panelAdmin/component/SideMenu/MainMenu/index.js":
/*!*********************************************************!*\
  !*** ./panelAdmin/component/SideMenu/MainMenu/index.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _MenuTitle__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../MenuTitle */ "./panelAdmin/component/SideMenu/MenuTitle/index.js");
/* harmony import */ var _Menu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Menu */ "./panelAdmin/component/SideMenu/Menu/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/component/SideMenu/MainMenu/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const MainMenu = ({
  mainMenus,
  windowLocation
}) => {
  const {
    0: showLi,
    1: setShowLi
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");
  const {
    0: selectedMenuTitle,
    1: setMenuTitle
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(); // console.log({ windowLocation });

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    checkMenu();
  }, []);

  const checkMenu = () => {
    mainMenus.map(menu => {
      return menu.menus.map(menu => {
        return menu.subMenu.map(subMenu => {
          if (subMenu.route === "/" + windowLocation.substr(windowLocation.indexOf("panelAdmin"))) {
            setMenuTitle(menu.menuTitle);
            setShowLi(menu.menuTitle);
            return true;
          }
        });
      });
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    checkMenu();
  }, [windowLocation]); // //console.log({ showLi, selectedMenuTitle });

  return mainMenus.map((mainMenu, index) => {
    return __jsx("ul", {
      key: index + "m",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 7
      }
    }, __jsx(_MenuTitle__WEBPACK_IMPORTED_MODULE_1__["default"], {
      title: mainMenu.title,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34,
        columnNumber: 9
      }
    }), __jsx(_Menu__WEBPACK_IMPORTED_MODULE_2__["default"], {
      windowLocation: windowLocation,
      menus: mainMenu.menus,
      showLi: showLi,
      setShowLi: setShowLi,
      selectedMenuTitle: selectedMenuTitle,
      setMenuTitle: setMenuTitle,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 9
      }
    }));
  });
};

/* harmony default export */ __webpack_exports__["default"] = (MainMenu);

/***/ }),

/***/ "./panelAdmin/component/SideMenu/Menu/index.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/component/SideMenu/Menu/index.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _SubMenu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../SubMenu */ "./panelAdmin/component/SideMenu/SubMenu/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/component/SideMenu/Menu/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const Menu = ({
  menus,
  showLi,
  setShowLi,
  selectedMenuTitle,
  setMenuTitle,
  selectedMenu,
  windowLocation
}) => {
  const location = windowLocation;

  const activedSideTitle = name => {
    let newName = name;

    if (showLi === name) {
      newName = "";
    } else {
      newName = name;
    }

    setShowLi(newName);
  };

  const classNameForMenu = (length, title) => {
    let classes;
    classes = [length ? "icon-right-open " : "", // selectedMenuTitle === title ? (showLi === title ? "" : "activeMenu") : "",
    showLi === title ? "arrowRotate" : "unsetRotate"].join(" ");
    return classes;
  };

  const _handelStateNull = () => {
    setShowLi("");
    setMenuTitle("");
  };

  return menus.map((menu, index) => {
    return __jsx("li", {
      key: "menus-" + index,
      className: "side-iteme",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42,
        columnNumber: 7
      }
    }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
      href: !menu.subMenu.length > 0 ? menu.route : "#",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43,
        columnNumber: 9
      }
    }, __jsx("a", {
      onClick: menu.route ? _handelStateNull : () => activedSideTitle(menu.subMenu && menu.menuTitle),
      id: location.includes(menu.route) ? "activedSide" : selectedMenuTitle === menu.menuTitle ? showLi === menu.menuTitle ? "" : "activedSide" : "",
      className: `side-link ${classNameForMenu(menu.subMenu.length, menu.menuTitle)}`,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44,
        columnNumber: 11
      }
    }, menu.menuIconImg ? __jsx("img", {
      src: menu.menuIconImg,
      alt: "icon menu",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 65,
        columnNumber: 15
      }
    }) : __jsx("i", {
      className: menu.menuIconClass,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 67,
        columnNumber: 15
      }
    }), __jsx("span", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69,
        columnNumber: 13
      }
    }, menu.menuTitle), menu.subMenu.length ? __jsx("div", {
      className: "menu-arrow-icon",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71,
        columnNumber: 15
      }
    }, __jsx("i", {
      className: "fas fa-angle-left",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 17
      }
    })) : "")), __jsx(_SubMenu__WEBPACK_IMPORTED_MODULE_2__["default"], {
      windowLocation: windowLocation,
      menu: menu,
      setMenuTitle: setMenuTitle,
      showLi: showLi,
      selectedMenu: selectedMenu,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79,
        columnNumber: 9
      }
    }));
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Menu);

/***/ }),

/***/ "./panelAdmin/component/SideMenu/MenuTitle/index.js":
/*!**********************************************************!*\
  !*** ./panelAdmin/component/SideMenu/MenuTitle/index.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/component/SideMenu/MenuTitle/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const MenuTitle = ({
  title
}) => {
  return __jsx("li", {
    className: "side-header change-position",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5,
      columnNumber: 3
    }
  }, __jsx("h6", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 4
    }
  }, title));
};

/* harmony default export */ __webpack_exports__["default"] = (MenuTitle);

/***/ }),

/***/ "./panelAdmin/component/SideMenu/SubMenu/index.js":
/*!********************************************************!*\
  !*** ./panelAdmin/component/SideMenu/SubMenu/index.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/component/SideMenu/SubMenu/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const SubMenu = ({
  menu,
  setMenuTitle,
  showLi,
  selectedMenu,
  windowLocation
}) => {
  const location = windowLocation;
  const sideMenuLi = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  const onSubMenuClicked = (subMenu, menu) => {
    setMenuTitle(menu.menuTitle);
  };

  let liWidth = 0;
  if (sideMenuLi.current) liWidth = sideMenuLi.current.clientHeight;
  return __jsx("ul", {
    style: {
      height: showLi === menu.menuTitle ? menu.subMenu.length * liWidth + "px" : ""
    },
    className: `side-child-navigation transition0-3 ${showLi === menu.menuTitle && menu.subMenu.length ? "showIn" : "showOut"}`,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 5
    }
  }, menu.subMenu.map((child, i) => __jsx("li", {
    ref: sideMenuLi,
    className: `side-iteme`,
    key: "subMenu-" + i,
    onClick: () => onSubMenuClicked(child, menu),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 9
    }
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
    href: child.route,
    as: child.route,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 11
    }
  }, __jsx("a", {
    className: `side-link side-child-ling ${child.route === selectedMenu || location.includes(child.route) ? "activedSideChild" : ""} `,
    id: "sideChildTitle",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 13
    }
  }, child.title)))));
};

/* harmony default export */ __webpack_exports__["default"] = (SubMenu);

/***/ }),

/***/ "./panelAdmin/component/SideMenu/index.js":
/*!************************************************!*\
  !*** ./panelAdmin/component/SideMenu/index.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dom */ "react-dom");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _assets_img_Rimtal_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../assets/img/Rimtal.png */ "./panelAdmin/assets/img/Rimtal.png");
/* harmony import */ var _assets_img_Rimtal_png__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_assets_img_Rimtal_png__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _MainMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./MainMenu */ "./panelAdmin/component/SideMenu/MainMenu/index.js");
/* harmony import */ var react_scrollbars_custom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-scrollbars-custom */ "react-scrollbars-custom");
/* harmony import */ var react_scrollbars_custom__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_scrollbars_custom__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-perfect-scrollbar */ "react-perfect-scrollbar");
/* harmony import */ var react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/component/SideMenu/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




 // import "./index.scss";
// import "react-perfect-scrollbar/dist/css/styles.css";

 // import ScrollArea from "react-scrollbar";

 // const ScrollArea = require("react-scrollbar");



const SideMenu = ({
  windowLocation
}) => {
  // return useMemo(() => {
  return __jsx("div", {
    className: `panelAdmin-sideBar-container `,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 5
    }
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
    href: "/panelAdmin",
    as: "/panelAdmin",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 7
    }
  }, __jsx("a", {
    className: "lgHolder",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 9
    }
  }, __jsx("img", {
    src: _assets_img_Rimtal_png__WEBPACK_IMPORTED_MODULE_3___default.a,
    alt: "logo",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 11
    }
  }))), __jsx(react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6___default.a, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 7
    }
  }, __jsx(_MainMenu__WEBPACK_IMPORTED_MODULE_4__["default"], {
    mainMenus: ___WEBPACK_IMPORTED_MODULE_7__["default"].menuFormat,
    windowLocation: windowLocation,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 9
    }
  }))); // }, []);
};

/* harmony default export */ __webpack_exports__["default"] = (SideMenu);

/***/ }),

/***/ "./panelAdmin/component/UI/BackgrandCover/index.js":
/*!*********************************************************!*\
  !*** ./panelAdmin/component/UI/BackgrandCover/index.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/component/UI/BackgrandCover/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const BackgrandCover = props => {
  return __jsx("div", {
    id: "coverContainer",
    onClick: props.onClick,
    className: props.fadeIn ? " fadeIn" : " fadeOut",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5,
      columnNumber: 5
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (BackgrandCover);

/***/ }),

/***/ "./panelAdmin/index.js":
/*!*****************************!*\
  !*** ./panelAdmin/index.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_menuFormat__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils/menuFormat */ "./panelAdmin/utils/menuFormat.js");
/* harmony import */ var _values__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./values */ "./panelAdmin/values/index.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils */ "./panelAdmin/utils/index.js");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./api */ "./panelAdmin/api/index.js");
/* harmony import */ var _panelAdmin_storeAdmin_reducer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../panelAdmin/storeAdmin/reducer */ "./panelAdmin/storeAdmin/reducer/index.js");
/* harmony import */ var _panelAdmin_storeAdmin_actions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../panelAdmin/storeAdmin/actions */ "./panelAdmin/storeAdmin/actions/index.js");
/* harmony import */ var _panelAdmin_storeAdmin_actionTypes__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../panelAdmin/storeAdmin/actionTypes */ "./panelAdmin/storeAdmin/actionTypes/index.js");
/* harmony import */ var _panelAdmin_storeAdmin_saga_index__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../panelAdmin/storeAdmin/saga/index */ "./panelAdmin/storeAdmin/saga/index.js");








const panelAdmin = {
  menuFormat: _utils_menuFormat__WEBPACK_IMPORTED_MODULE_0__["default"],
  values: _values__WEBPACK_IMPORTED_MODULE_1__["default"],
  utils: _utils__WEBPACK_IMPORTED_MODULE_2__["default"],
  api: _api__WEBPACK_IMPORTED_MODULE_3__,
  reducer: _panelAdmin_storeAdmin_reducer__WEBPACK_IMPORTED_MODULE_4__["default"],
  actions: _panelAdmin_storeAdmin_actions__WEBPACK_IMPORTED_MODULE_5__["default"],
  actionTypes: _panelAdmin_storeAdmin_actionTypes__WEBPACK_IMPORTED_MODULE_6__["default"],
  saga: _panelAdmin_storeAdmin_saga_index__WEBPACK_IMPORTED_MODULE_7__
};
/* harmony default export */ __webpack_exports__["default"] = (panelAdmin);

/***/ }),

/***/ "./panelAdmin/screen/PanelScreen.js":
/*!******************************************!*\
  !*** ./panelAdmin/screen/PanelScreen.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _component_SideMenu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../component/SideMenu */ "./panelAdmin/component/SideMenu/index.js");
/* harmony import */ var _component_Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../component/Header */ "./panelAdmin/component/Header/index.js");
/* harmony import */ var _component_UI_BackgrandCover__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../component/UI/BackgrandCover */ "./panelAdmin/component/UI/BackgrandCover/index.js");
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-toastify */ "react-toastify");
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_toastify__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next-redux-wrapper */ "next-redux-wrapper");
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_redux_wrapper__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_scrollbars_custom__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-scrollbars-custom */ "react-scrollbars-custom");
/* harmony import */ var react_scrollbars_custom__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_scrollbars_custom__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/screen/PanelScreen.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;









const PanelScreen = props => {
  const {
    router
  } = props;
  const {
    0: sidebarToggle,
    1: setSidebarToggle
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);

  const _handelSidebarToggle = () => {
    setSidebarToggle(!sidebarToggle);
  };

  return __jsx("div", {
    className: `panelAdmin-wrapper  ${sidebarToggle ? "fadeIn" : "fadeOut"}`,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 5
    }
  }, __jsx("div", {
    style: {
      direction: "rtl",
      display: "flex"
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 7
    }
  }, __jsx(_component_SideMenu__WEBPACK_IMPORTED_MODULE_1__["default"], {
    windowLocation: router.asPath,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 9
    }
  }), __jsx("div", {
    className: "panelAdmin-container",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 9
    }
  }, __jsx(_component_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
    _handelSidebarToggle: _handelSidebarToggle,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 11
    }
  }), __jsx(react_scrollbars_custom__WEBPACK_IMPORTED_MODULE_7___default.a, {
    style: {
      width: "100%",
      height: " 100% "
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "panelAdmin-content",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 13
    }
  }, props.children)))), __jsx(_component_UI_BackgrandCover__WEBPACK_IMPORTED_MODULE_3__["default"], {
    fadeIn: sidebarToggle,
    onClick: _handelSidebarToggle,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }
  }), __jsx(react_toastify__WEBPACK_IMPORTED_MODULE_4__["ToastContainer"], {
    rtl: true,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 7
    }
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (PanelScreen);

/***/ }),

/***/ "./panelAdmin/storeAdmin/actionTypes/index.js":
/*!****************************************************!*\
  !*** ./panelAdmin/storeAdmin/actionTypes/index.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./redux */ "./panelAdmin/storeAdmin/actionTypes/redux/index.js");
/* harmony import */ var _saga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./saga */ "./panelAdmin/storeAdmin/actionTypes/saga/index.js");


const actionTypes = {
  atRedux: _redux__WEBPACK_IMPORTED_MODULE_0__["default"],
  atSaga: _saga__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (actionTypes);

/***/ }),

/***/ "./panelAdmin/storeAdmin/actionTypes/redux/index.js":
/*!**********************************************************!*\
  !*** ./panelAdmin/storeAdmin/actionTypes/redux/index.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const atRedux = {
  //======================================================== Redux
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX",
  // ======================================================== NAVBAR
  SET_PAGE_NAME: "SET_PAGE_NAME",
  // ======================================================== UPLOAD
  SET_UPLOAD_IMAGE: "SET_UPLOAD_IMAGE_REDUX",
  // ======================================================== GALLERY
  SET_GALLERY_DATA: "SET_GALLERY_DATA_REDUX",
  CHANGE_ADD_GALLERY_DATA: "CHANGE_ADD_GALLERY_DATA_REDUX",
  // ======================================================== ARTIST
  SET_ARTIST_DATA: "SET_GALLERY_DATA_REDUX",
  START_ARTIST_DATA: "START_GALLERY_DATA_REDUX",
  // ======================= SEARCH ARTIST
  SET_SEARCH_ARTIST_DATA: "SET_SEARCH_ARTIST_DATA_REDUX",
  START_SEARCH_ARTIST_DATA: "START_SEARCH_ARTIST_DATA_REDUX",
  // ======================================================== END ARTIST
  // ======================================================== INSTRUMENT
  SET_INSTRUMENT_DATA: "SET_GALLERY_DATA_REDUX",
  START_INSTRUMENT_DATA: "START_INSTRUMENT_DATA_REDUX",
  // ======================= SEARCH INSTRUMENT
  SET_SEARCH_INSTRUMENT_DATA: "SET_SEARCH_INSTRUMENT_DATA_REDUX",
  START_SEARCH_INSTRUMENT_DATA: "START_SEARCH_INSTRUMENT_DATA_REDUX",
  // ======================================================== END INSTRUMENT
  // ======================================================== COUNTRY
  SET_COUNTRY_DATA: "SET_GALLERY_DATA_REDUX",
  START_COUNTRY_DATA: "START_COUNTRY_DATA_REDUX",
  // ======================= SEARCH COUNTRY
  SET_SEARCH_COUNTRY_DATA: "SET_SEARCH_COUNTRY_DATA_REDUX",
  START_SEARCH_COUNTRY_DATA: "START_SEARCH_COUNTRY_DATA_REDUX",
  // ======================================================== END COUNTRY
  // ======================================================== MOOD
  SET_MOOD_DATA: "SET_GALLERY_DATA_REDUX",
  START_MOOD_DATA: "START_MOOD_DATA_REDUX",
  // ======================= SEARCH MOOD
  SET_SEARCH_MOOD_DATA: "SET_SEARCH_MOOD_DATA_REDUX",
  START_SEARCH_MOOD_DATA: "START_SEARCH_MOOD_DATA_REDUX",
  // ======================================================== END MOOD
  // ======================================================== HASHTAG
  SET_HASHTAG_DATA: "SET_GALLERY_DATA_REDUX",
  START_HASHTAG_DATA: "START_HASHTAG_DATA_REDUX",
  // ======================= SEARCH HASHTAG
  SET_SEARCH_HASHTAG_DATA: "SET_SEARCH_HASHTAG_DATA_REDUX",
  START_SEARCH_HASHTAG_DATA: "START_SEARCH_HASHTAG_DATA_REDUX",
  // ======================================================== END HASHTAG
  // ======================================================== GENRE
  SET_GENRE_DATA: "SET_GALLERY_DATA_REDUX",
  START_GENRE_DATA: "START_GENRE_DATA_REDUX",
  // ======================= SEARCH GENRE
  SET_SEARCH_GENRE_DATA: "SET_SEARCH_GENRE_DATA_REDUX",
  START_SEARCH_GENRE_DATA: "START_SEARCH_GENRE_DATA_REDUX" // ======================================================== END GENRE

};
/* harmony default export */ __webpack_exports__["default"] = (atRedux);

/***/ }),

/***/ "./panelAdmin/storeAdmin/actionTypes/saga/index.js":
/*!*********************************************************!*\
  !*** ./panelAdmin/storeAdmin/actionTypes/saga/index.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const atSaga = {
  POST_UPLOAD_IMAGE: "POST_UPLOAD_IMAGE_SAGA",
  GET_GALLERY_DATA: "GET_GALLERY_DATA_SAGA",
  //============================================================= ARTIST
  GET_ARTIST_DATA: "GET_ARTIST_DATA_SAGA",
  GET_SEARCH_ARTIST_DATA: "GET_SEARCH_ARTIST_DATA_SAGA",
  //============================================================= INSTRUMENT
  GET_INSTRUMENT_DATA: "GET_INSTRUMENT_DATA_SAGA",
  GET_SEARCH_INSTRUMENT_DATA: "GET_SEARCH_INSTRUMENT_DATA_SAGA",
  //============================================================= COUNTRY
  GET_COUNTRY_DATA: "GET_COUNTRY_DATA_SAGA",
  GET_SEARCH_COUNTRY_DATA: "GET_SEARCH_COUNTRY_DATA_SAGA",
  //============================================================= ALBUM
  GET_ALBUM_DATA: "GET_ALBUM_DATA_SAGA",
  GET_SEARCH_ALBUM_DATA: "GET_SEARCH_ALBUM_DATA_SAGA",
  //============================================================= MOOD
  GET_MOOD_DATA: "GET_MOOD_DATA_SAGA",
  GET_SEARCH_MOOD_DATA: "GET_SEARCH_MOOD_DATA_SAGA",
  //============================================================= HASHTAG
  GET_HASHTAG_DATA: "GET_HASHTAG_DATA_SAGA",
  GET_SEARCH_HASHTAG_DATA: "GET_SEARCH_HASHTAG_DATA_SAGA",
  //============================================================= GENRE
  GET_GENRE_DATA: "GET_GENRE_DATA_SAGA",
  GET_SEARCH_GENRE_DATA: "GET_SEARCH_GENRE_DATA_SAGA"
};
/* harmony default export */ __webpack_exports__["default"] = (atSaga);

/***/ }),

/***/ "./panelAdmin/storeAdmin/actions/index.js":
/*!************************************************!*\
  !*** ./panelAdmin/storeAdmin/actions/index.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./redux */ "./panelAdmin/storeAdmin/actions/redux/index.js");
/* harmony import */ var _saga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./saga */ "./panelAdmin/storeAdmin/actions/saga/index.js");


const actions = {
  reduxActions: _redux__WEBPACK_IMPORTED_MODULE_0__,
  sagaActions: _saga__WEBPACK_IMPORTED_MODULE_1__
};
/* harmony default export */ __webpack_exports__["default"] = (actions);

/***/ }),

/***/ "./panelAdmin/storeAdmin/actions/redux/index.js":
/*!******************************************************!*\
  !*** ./panelAdmin/storeAdmin/actions/redux/index.js ***!
  \******************************************************/
/*! exports provided: setPageName, setUploadImage, setGalleryData, changeAddGalleryData, setArtistData, setSearchArtistData, startGetArtist, startSearchArtist, setInstrumentData, setSearchInstrumentData, startGetInstrument, startSearchInstrument, setCountryData, setSearchCountryData, startGetCountry, startSearchCountry, setMoodData, setSearchMoodData, startGetMood, startSearchMood, setHashtagData, setSearchHashtagData, startGetHashtag, startSearchHashtag, setGenreData, setSearchGenreData, startGetGenre, startSearchGenre */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setPageName", function() { return setPageName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setUploadImage", function() { return setUploadImage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setGalleryData", function() { return setGalleryData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changeAddGalleryData", function() { return changeAddGalleryData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setArtistData", function() { return setArtistData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setSearchArtistData", function() { return setSearchArtistData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startGetArtist", function() { return startGetArtist; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startSearchArtist", function() { return startSearchArtist; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setInstrumentData", function() { return setInstrumentData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setSearchInstrumentData", function() { return setSearchInstrumentData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startGetInstrument", function() { return startGetInstrument; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startSearchInstrument", function() { return startSearchInstrument; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setCountryData", function() { return setCountryData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setSearchCountryData", function() { return setSearchCountryData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startGetCountry", function() { return startGetCountry; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startSearchCountry", function() { return startSearchCountry; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setMoodData", function() { return setMoodData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setSearchMoodData", function() { return setSearchMoodData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startGetMood", function() { return startGetMood; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startSearchMood", function() { return startSearchMood; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setHashtagData", function() { return setHashtagData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setSearchHashtagData", function() { return setSearchHashtagData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startGetHashtag", function() { return startGetHashtag; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startSearchHashtag", function() { return startSearchHashtag; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setGenreData", function() { return setGenreData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setSearchGenreData", function() { return setSearchGenreData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startGetGenre", function() { return startGetGenre; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startSearchGenre", function() { return startSearchGenre; });
/* harmony import */ var _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../actionTypes/redux */ "./panelAdmin/storeAdmin/actionTypes/redux/index.js");
 // =================================================== NAVBAR

function setPageName(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_PAGE_NAME,
    data
  };
} // =================================================== UPLOAD

function setUploadImage(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_UPLOAD_IMAGE,
    data
  };
} // =================================================== GALLERY

function setGalleryData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_GALLERY_DATA,
    data
  };
}
function changeAddGalleryData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].CHANGE_ADD_GALLERY_DATA,
    data
  };
} // =================================================== ARTIST

function setArtistData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_ARTIST_DATA,
    data
  };
}
function setSearchArtistData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_SEARCH_ARTIST_DATA,
    data
  };
}
function startGetArtist(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].START_ARTIST_DATA,
    data
  };
}
function startSearchArtist(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].START_SEARCH_ARTIST_DATA,
    data
  };
} // =================================================== INSTRUMENT

function setInstrumentData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_INSTRUMENT_DATA,
    data
  };
}
function setSearchInstrumentData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_SEARCH_INSTRUMENT_DATA,
    data
  };
}
function startGetInstrument(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].START_INSTRUMENT_DATA,
    data
  };
}
function startSearchInstrument(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].START_SEARCH_INSTRUMENT_DATA,
    data
  };
} // =================================================== COUNTRY

function setCountryData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_COUNTRY_DATA,
    data
  };
}
function setSearchCountryData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_SEARCH_COUNTRY_DATA,
    data
  };
}
function startGetCountry(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].START_COUNTRY_DATA,
    data
  };
}
function startSearchCountry(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].START_SEARCH_COUNTRY_DATA,
    data
  };
} // =================================================== MOOD

function setMoodData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_MOOD_DATA,
    data
  };
}
function setSearchMoodData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_SEARCH_MOOD_DATA,
    data
  };
}
function startGetMood(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].START_MOOD_DATA,
    data
  };
}
function startSearchMood(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].START_SEARCH_MOOD_DATA,
    data
  };
} // =================================================== HASHTAG

function setHashtagData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_HASHTAG_DATA,
    data
  };
}
function setSearchHashtagData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_SEARCH_HASHTAG_DATA,
    data
  };
}
function startGetHashtag(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].START_HASHTAG_DATA,
    data
  };
}
function startSearchHashtag(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].START_SEARCH_HASHTAG_DATA,
    data
  };
} // =================================================== GENRE

function setGenreData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_GENRE_DATA,
    data
  };
}
function setSearchGenreData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_SEARCH_GENRE_DATA,
    data
  };
}
function startGetGenre(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].START_GENRE_DATA,
    data
  };
}
function startSearchGenre(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].START_SEARCH_GENRE_DATA,
    data
  };
}

/***/ }),

/***/ "./panelAdmin/storeAdmin/actions/saga/index.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/storeAdmin/actions/saga/index.js ***!
  \*****************************************************/
/*! exports provided: uploadImageData, getGalleryData, getArtistData, getSearchArtistData, getInstrumentData, getSearchInstrumentData, getCountryData, getSearchCountryData, getMoodData, getSearchMoodData, getAlbumData, getSearchAlbumData, getHashtagData, getSearchHashtagData, getGenreData, getSearchGenreData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "uploadImageData", function() { return uploadImageData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getGalleryData", function() { return getGalleryData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getArtistData", function() { return getArtistData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSearchArtistData", function() { return getSearchArtistData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getInstrumentData", function() { return getInstrumentData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSearchInstrumentData", function() { return getSearchInstrumentData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCountryData", function() { return getCountryData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSearchCountryData", function() { return getSearchCountryData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMoodData", function() { return getMoodData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSearchMoodData", function() { return getSearchMoodData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAlbumData", function() { return getAlbumData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSearchAlbumData", function() { return getSearchAlbumData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getHashtagData", function() { return getHashtagData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSearchHashtagData", function() { return getSearchHashtagData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getGenreData", function() { return getGenreData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSearchGenreData", function() { return getSearchGenreData; });
/* harmony import */ var _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../actionTypes/saga */ "./panelAdmin/storeAdmin/actionTypes/saga/index.js");

function uploadImageData(data) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].POST_UPLOAD_IMAGE,
    data: data
  };
}
function getGalleryData(data) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_GALLERY_DATA,
    data: data
  };
} // ========================================================= ARTIST

function getArtistData({
  page
}) {
  console.log({
    mojtaba1: page
  });
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_ARTIST_DATA,
    page
  };
}
function getSearchArtistData({
  title,
  page
}) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_SEARCH_ARTIST_DATA,
    page,
    title
  };
} // ========================================================= INSTRUMENT

function getInstrumentData({
  page
}) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_INSTRUMENT_DATA,
    page
  };
}
function getSearchInstrumentData({
  title,
  page
}) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_SEARCH_INSTRUMENT_DATA,
    page,
    title
  };
} // ========================================================= COUNTRY

function getCountryData({
  page
}) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_COUNTRY_DATA,
    page
  };
}
function getSearchCountryData({
  title,
  page
}) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_SEARCH_COUNTRY_DATA,
    page,
    title
  };
} // ========================================================= MOOD

function getMoodData({
  page
}) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_MOOD_DATA,
    page
  };
}
function getSearchMoodData({
  title,
  page
}) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_SEARCH_MOOD_DATA,
    page,
    title
  };
} // ========================================================= ALBUM

function getAlbumData({
  page
}) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_ALBUM_DATA,
    page
  };
}
function getSearchAlbumData({
  title,
  page
}) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_SEARCH_ALBUM_DATA,
    page,
    title
  };
} // ========================================================= HASHTAG

function getHashtagData({
  page
}) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_HASHTAG_DATA,
    page
  };
}
function getSearchHashtagData({
  title,
  page
}) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_SEARCH_HASHTAG_DATA,
    page,
    title
  };
} // ========================================================= GENRE

function getGenreData({
  page
}) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_GENRE_DATA,
    page
  };
}
function getSearchGenreData({
  title,
  page
}) {
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_SEARCH_GENRE_DATA,
    page,
    title
  };
}

/***/ }),

/***/ "./panelAdmin/storeAdmin/reducer/albumReducer.js":
/*!*******************************************************!*\
  !*** ./panelAdmin/storeAdmin/reducer/albumReducer.js ***!
  \*******************************************************/
/*! exports provided: albumInitialState, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "albumInitialState", function() { return albumInitialState; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const albumInitialState = {
  albumData: null,
  searchAlbumData: null,
  loading: false,
  searchLoading: false
};

function albumReducer(state = albumInitialState, action) {
  const atRedux = ___WEBPACK_IMPORTED_MODULE_0__["default"].actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_ALBUM_DATA:
      return _objectSpread({}, state, {}, {
        albumData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_ALBUM_DATA:
      return _objectSpread({}, state, {}, {
        searchAlbumData: action.data,
        searchLoading: false
      });

    case atRedux.START_ALBUM_DATA:
      console.log("START_ALBUM_DATA");
      return _objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_ALBUM_DATA:
      console.log("START_SEARCH_ALBUM_DATA");
      return _objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (albumReducer);

/***/ }),

/***/ "./panelAdmin/storeAdmin/reducer/artistReducer.js":
/*!********************************************************!*\
  !*** ./panelAdmin/storeAdmin/reducer/artistReducer.js ***!
  \********************************************************/
/*! exports provided: artistInitialState, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "artistInitialState", function() { return artistInitialState; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const artistInitialState = {
  artistData: null,
  searchArtistData: null,
  loading: false,
  searchLoading: false
};

function artistReducer(state = artistInitialState, action) {
  const atRedux = ___WEBPACK_IMPORTED_MODULE_0__["default"].actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_ARTIST_DATA:
      return _objectSpread({}, state, {}, {
        artistData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_ARTIST_DATA:
      return _objectSpread({}, state, {}, {
        searchArtistData: action.data,
        searchLoading: false
      });

    case atRedux.START_ARTIST_DATA:
      console.log("START_ARTIST_DATA");
      return _objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_ARTIST_DATA:
      console.log("START_SEARCH_ARTIST_DATA");
      return _objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (artistReducer);

/***/ }),

/***/ "./panelAdmin/storeAdmin/reducer/countryReducer.js":
/*!*********************************************************!*\
  !*** ./panelAdmin/storeAdmin/reducer/countryReducer.js ***!
  \*********************************************************/
/*! exports provided: countryInitialState, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "countryInitialState", function() { return countryInitialState; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const countryInitialState = {
  countryData: null,
  searchCountryData: null,
  loading: false,
  searchLoading: false
};

function countryReducer(state = countryInitialState, action) {
  const atRedux = ___WEBPACK_IMPORTED_MODULE_0__["default"].actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_COUNTRY_DATA:
      return _objectSpread({}, state, {}, {
        countryData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_COUNTRY_DATA:
      return _objectSpread({}, state, {}, {
        searchCountryData: action.data,
        searchLoading: false
      });

    case atRedux.START_COUNTRY_DATA:
      return _objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_COUNTRY_DATA:
      return _objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (countryReducer);

/***/ }),

/***/ "./panelAdmin/storeAdmin/reducer/galleryReducer.js":
/*!*********************************************************!*\
  !*** ./panelAdmin/storeAdmin/reducer/galleryReducer.js ***!
  \*********************************************************/
/*! exports provided: galleryInitialState, setGalleryData, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "galleryInitialState", function() { return galleryInitialState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setGalleryData", function() { return setGalleryData; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const galleryInitialState = {
  galleryData: []
};
const setGalleryData = (state, action) => {
  return _objectSpread({}, state, {
    galleryData: action.data
  });
};

function galleryReducer(state = galleryInitialState, action) {
  const atRedux = ___WEBPACK_IMPORTED_MODULE_0__["default"].actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_GALLERY_DATA:
      return setGalleryData(state, action);

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (galleryReducer);

/***/ }),

/***/ "./panelAdmin/storeAdmin/reducer/genreReducer.js":
/*!*******************************************************!*\
  !*** ./panelAdmin/storeAdmin/reducer/genreReducer.js ***!
  \*******************************************************/
/*! exports provided: genreInitialState, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "genreInitialState", function() { return genreInitialState; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const genreInitialState = {
  genreData: null,
  searchGenreData: null,
  loading: false,
  searchLoading: false
};

function genreReducer(state = genreInitialState, action) {
  const atRedux = ___WEBPACK_IMPORTED_MODULE_0__["default"].actionTypes.atRedux;
  console.log({
    action
  });

  switch (action.type) {
    case atRedux.SET_GENRE_DATA:
      return _objectSpread({}, state, {}, {
        genreData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_GENRE_DATA:
      return _objectSpread({}, state, {}, {
        searchGenreData: action.data,
        searchLoading: false
      });

    case atRedux.START_GENRE_DATA:
      return _objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_GENRE_DATA:
      return _objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (genreReducer);

/***/ }),

/***/ "./panelAdmin/storeAdmin/reducer/hashtagReducer.js":
/*!*********************************************************!*\
  !*** ./panelAdmin/storeAdmin/reducer/hashtagReducer.js ***!
  \*********************************************************/
/*! exports provided: hashtagInitialState, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hashtagInitialState", function() { return hashtagInitialState; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const hashtagInitialState = {
  hashtagData: null,
  searchMoodData: null,
  loading: false,
  searchLoading: false
};

function hashtagReducer(state = hashtagInitialState, action) {
  const atRedux = ___WEBPACK_IMPORTED_MODULE_0__["default"].actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_HASHTAG_DATA:
      return _objectSpread({}, state, {}, {
        hashtagData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_HASHTAG_DATA:
      return _objectSpread({}, state, {}, {
        searchMoodData: action.data,
        searchLoading: false
      });

    case atRedux.START_HASHTAG_DATA:
      return _objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_HASHTAG_DATA:
      return _objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (hashtagReducer);

/***/ }),

/***/ "./panelAdmin/storeAdmin/reducer/index.js":
/*!************************************************!*\
  !*** ./panelAdmin/storeAdmin/reducer/index.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _uploadReducer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./uploadReducer */ "./panelAdmin/storeAdmin/reducer/uploadReducer.js");
/* harmony import */ var _galleryReducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./galleryReducer */ "./panelAdmin/storeAdmin/reducer/galleryReducer.js");
/* harmony import */ var _artistReducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./artistReducer */ "./panelAdmin/storeAdmin/reducer/artistReducer.js");
/* harmony import */ var _panelNavbarReducer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./panelNavbarReducer */ "./panelAdmin/storeAdmin/reducer/panelNavbarReducer.js");
/* harmony import */ var _instrumentReducer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./instrumentReducer */ "./panelAdmin/storeAdmin/reducer/instrumentReducer.js");
/* harmony import */ var _countryReducer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./countryReducer */ "./panelAdmin/storeAdmin/reducer/countryReducer.js");
/* harmony import */ var _moodReducer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./moodReducer */ "./panelAdmin/storeAdmin/reducer/moodReducer.js");
/* harmony import */ var _hashtagReducer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./hashtagReducer */ "./panelAdmin/storeAdmin/reducer/hashtagReducer.js");
/* harmony import */ var _genreReducer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./genreReducer */ "./panelAdmin/storeAdmin/reducer/genreReducer.js");
/* harmony import */ var _albumReducer__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./albumReducer */ "./panelAdmin/storeAdmin/reducer/albumReducer.js");
// import galleryReducer from "./galleryReducer";










const reducer = {
  upload: _uploadReducer__WEBPACK_IMPORTED_MODULE_0__["default"],
  gallery: _galleryReducer__WEBPACK_IMPORTED_MODULE_1__["default"],
  artist: _artistReducer__WEBPACK_IMPORTED_MODULE_2__["default"],
  panelNavbar: _panelNavbarReducer__WEBPACK_IMPORTED_MODULE_3__["default"],
  instrument: _instrumentReducer__WEBPACK_IMPORTED_MODULE_4__["default"],
  country: _countryReducer__WEBPACK_IMPORTED_MODULE_5__["default"],
  mood: _moodReducer__WEBPACK_IMPORTED_MODULE_6__["default"],
  hashtag: _hashtagReducer__WEBPACK_IMPORTED_MODULE_7__["default"],
  genre: _genreReducer__WEBPACK_IMPORTED_MODULE_8__["default"],
  album: _albumReducer__WEBPACK_IMPORTED_MODULE_9__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (reducer);

/***/ }),

/***/ "./panelAdmin/storeAdmin/reducer/instrumentReducer.js":
/*!************************************************************!*\
  !*** ./panelAdmin/storeAdmin/reducer/instrumentReducer.js ***!
  \************************************************************/
/*! exports provided: instrumentInitialState, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "instrumentInitialState", function() { return instrumentInitialState; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const instrumentInitialState = {
  instrumentData: null,
  searchInstrumentData: null,
  loading: false,
  searchLoading: false
};

function instrumentReducer(state = instrumentInitialState, action) {
  const atRedux = ___WEBPACK_IMPORTED_MODULE_0__["default"].actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_INSTRUMENT_DATA:
      return _objectSpread({}, state, {}, {
        instrumentData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_INSTRUMENT_DATA:
      return _objectSpread({}, state, {}, {
        searchInstrumentData: action.data,
        searchLoading: false
      });

    case atRedux.START_INSTRUMENT_DATA:
      return _objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_INSTRUMENT_DATA:
      return _objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (instrumentReducer);

/***/ }),

/***/ "./panelAdmin/storeAdmin/reducer/moodReducer.js":
/*!******************************************************!*\
  !*** ./panelAdmin/storeAdmin/reducer/moodReducer.js ***!
  \******************************************************/
/*! exports provided: moodInitialState, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "moodInitialState", function() { return moodInitialState; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const moodInitialState = {
  moodData: null,
  searchMoodData: null,
  loading: false,
  searchLoading: false
};

function moodReducer(state = moodInitialState, action) {
  const atRedux = ___WEBPACK_IMPORTED_MODULE_0__["default"].actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_MOOD_DATA:
      return _objectSpread({}, state, {}, {
        moodData: action.data,
        loading: false,
        searchLoading: false
      });

    case atRedux.SET_SEARCH_MOOD_DATA:
      return _objectSpread({}, state, {}, {
        searchMoodData: action.data,
        searchLoading: false
      });

    case atRedux.START_MOOD_DATA:
      return _objectSpread({}, state, {}, {
        loading: true
      });

    case atRedux.START_SEARCH_MOOD_DATA:
      return _objectSpread({}, state, {}, {
        searchLoading: true
      });

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (moodReducer);

/***/ }),

/***/ "./panelAdmin/storeAdmin/reducer/panelNavbarReducer.js":
/*!*************************************************************!*\
  !*** ./panelAdmin/storeAdmin/reducer/panelNavbarReducer.js ***!
  \*************************************************************/
/*! exports provided: artistInitialState, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "artistInitialState", function() { return artistInitialState; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// import atRedux from "../../../store/actionTypes/redux";

const artistInitialState = {
  pageName: null
};

function panelNavbarReducer(state = artistInitialState, action) {
  const atRedux = ___WEBPACK_IMPORTED_MODULE_0__["default"].actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_PAGE_NAME:
      return _objectSpread({}, state, {}, {
        pageName: action.data
      });

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (panelNavbarReducer);

/***/ }),

/***/ "./panelAdmin/storeAdmin/reducer/uploadReducer.js":
/*!********************************************************!*\
  !*** ./panelAdmin/storeAdmin/reducer/uploadReducer.js ***!
  \********************************************************/
/*! exports provided: uploadInitialState, setUploadImageData, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "uploadInitialState", function() { return uploadInitialState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setUploadImageData", function() { return setUploadImageData; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const uploadInitialState = {
  uploadImageData: ""
};
const setUploadImageData = (state, action) => {
  return _objectSpread({}, state, {
    uploadData: action.data
  });
};

function uploadReducer(state = uploadInitialState, action) {
  const atRedux = ___WEBPACK_IMPORTED_MODULE_0__["default"].actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_UPLOAD_IMAGE:
      return setUploadImageData(state, action);

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (uploadReducer);

/***/ }),

/***/ "./panelAdmin/storeAdmin/saga/index.js":
/*!*********************************************!*\
  !*** ./panelAdmin/storeAdmin/saga/index.js ***!
  \*********************************************/
/*! exports provided: watchUploadImage, watchGallery, watchGetArtistData, watchSearchArtistData, watchGetInstrumentalData, watchSearchInstrumentalData, watchGetCountryData, watchSearchCountryData, watchGetMoodData, watchSearchMoodData, watchGetHashtagData, watchSearchHashtagData, watchGetGenreData, watchSearchGenreData, watchGetAlbumData, watchSearchAlbumData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchUploadImage", function() { return watchUploadImage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchGallery", function() { return watchGallery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchGetArtistData", function() { return watchGetArtistData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchSearchArtistData", function() { return watchSearchArtistData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchGetInstrumentalData", function() { return watchGetInstrumentalData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchSearchInstrumentalData", function() { return watchSearchInstrumentalData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchGetCountryData", function() { return watchGetCountryData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchSearchCountryData", function() { return watchSearchCountryData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchGetMoodData", function() { return watchGetMoodData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchSearchMoodData", function() { return watchSearchMoodData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchGetHashtagData", function() { return watchGetHashtagData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchSearchHashtagData", function() { return watchSearchHashtagData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchGetGenreData", function() { return watchGetGenreData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchSearchGenreData", function() { return watchSearchGenreData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchGetAlbumData", function() { return watchGetAlbumData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchSearchAlbumData", function() { return watchSearchAlbumData; });
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../actionTypes/saga */ "./panelAdmin/storeAdmin/actionTypes/saga/index.js");
/* harmony import */ var _webServices__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./webServices */ "./panelAdmin/storeAdmin/saga/webServices/index.js");


 // ======================================================================================= UPLOAD

function* watchUploadImage() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].UPLOAD_IMAGE_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].POST.uploadImage);
} // ======================================================================================= GALLERY

function* watchGallery() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_GALLERY_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.galleryData);
} // ======================================================================================= ARTIST
// export function* watchArtist() {
//   yield all([
//     takeEvery(atSaga.GET_ARTIST_DATA, webServices.GET.artistData),
//     takeEvery(atSaga.GET_SEARCH_ARTIST_DATA, webServices.GET.artistSearchData),
//   ]);
// }

function* watchGetArtistData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_ARTIST_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.artistData);
}
function* watchSearchArtistData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_SEARCH_ARTIST_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.artistSearchData);
} // ======================================================================================= INSTRUMENTAL

function* watchGetInstrumentalData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_INSTRUMENT_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.instrumentData);
}
function* watchSearchInstrumentalData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_SEARCH_INSTRUMENT_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.instrumentSearchData);
} // ======================================================================================= COUNTRY

function* watchGetCountryData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_COUNTRY_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.countryData);
}
function* watchSearchCountryData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_SEARCH_COUNTRY_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.countrySearchData);
} // ======================================================================================= MOOD

function* watchGetMoodData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_MOOD_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.moodData);
}
function* watchSearchMoodData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_SEARCH_MOOD_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.moodSearchData);
} // ======================================================================================= HASHTAG

function* watchGetHashtagData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_HASHTAG_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.hashtagData);
}
function* watchSearchHashtagData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_SEARCH_HASHTAG_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.hashtagSearchData);
} // ======================================================================================= GENRE

function* watchGetGenreData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_GENRE_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.genreData);
}
function* watchSearchGenreData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_SEARCH_GENRE_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.genreSearchData);
} // ======================================================================================= ALBUM

function* watchGetAlbumData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_ALBUM_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.albumsData);
}
function* watchSearchAlbumData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_SEARCH_ALBUM_DATA, _webServices__WEBPACK_IMPORTED_MODULE_2__["default"].GET.albumSearchData);
}

/***/ }),

/***/ "./panelAdmin/storeAdmin/saga/webServices/GET/albums.js":
/*!**************************************************************!*\
  !*** ./panelAdmin/storeAdmin/saga/webServices/GET/albums.js ***!
  \**************************************************************/
/*! exports provided: albumsData, albumSearchData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "albumsData", function() { return albumsData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "albumSearchData", function() { return albumSearchData; });
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../actions */ "./panelAdmin/storeAdmin/actions/index.js");
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../index */ "./panelAdmin/index.js");



function* albumsData({
  page
}) {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.startGetAlbum());

  try {
    const res = yield _index__WEBPACK_IMPORTED_MODULE_2__["default"].api.get.albums({
      page
    });
    console.log({
      resalbumData: res
    });
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setAlbumData(res.data));
  } catch (err) {
    console.log({
      erralbumData: err
    });
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setFailure(1090));
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setFailure(err.response.data.CODE));
  }
}
function* albumSearchData({
  page,
  title
}) {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.startSearchAlbum()); //   try {
  //     const res = yield panelAdmin.api.get.albumSearch({ page, title });
  //     console.log({ resAlbumSearchData: res });
  //     yield put(actions.reduxActions.setSearchAlbumData(res.data));
  //   } catch (err) {
  //     console.log({ errAlbumSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}

/***/ }),

/***/ "./panelAdmin/storeAdmin/saga/webServices/GET/artists.js":
/*!***************************************************************!*\
  !*** ./panelAdmin/storeAdmin/saga/webServices/GET/artists.js ***!
  \***************************************************************/
/*! exports provided: artistData, artistSearchData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "artistData", function() { return artistData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "artistSearchData", function() { return artistSearchData; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../actions */ "./panelAdmin/storeAdmin/actions/index.js");
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../index */ "./panelAdmin/index.js");




function* artistData({
  page
}) {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.startGetArtist());
  console.log({
    mojtaba3: page
  });

  try {
    const res = yield _index__WEBPACK_IMPORTED_MODULE_3__["default"].api.get.artists({
      page
    });
    console.log({
      resArtistData: res
    });
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setArtistData(res.data));
  } catch (err) {
    console.log({
      errArtistData: err
    });
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(1090));
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(err.response.data.CODE));
  }
}
function* artistSearchData({
  page,
  title
}) {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.startSearchArtist());

  try {
    const res = yield _index__WEBPACK_IMPORTED_MODULE_3__["default"].api.get.artistSearch({
      page,
      title
    });
    console.log({
      resArtistSearchData: res
    });
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setSearchArtistData(res.data));
  } catch (err) {
    console.log({
      errArtistSearchData: err
    });
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(1090));
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(err.response.data.CODE));
  }
}

/***/ }),

/***/ "./panelAdmin/storeAdmin/saga/webServices/GET/country.js":
/*!***************************************************************!*\
  !*** ./panelAdmin/storeAdmin/saga/webServices/GET/country.js ***!
  \***************************************************************/
/*! exports provided: countryData, countrySearchData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "countryData", function() { return countryData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "countrySearchData", function() { return countrySearchData; });
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../actions */ "./panelAdmin/storeAdmin/actions/index.js");
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../index */ "./panelAdmin/index.js");



function* countryData({
  page
}) {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.startGetCountry());
  console.log({
    mojtaba3: page
  });

  try {
    const res = yield _index__WEBPACK_IMPORTED_MODULE_2__["default"].api.get.country({
      page
    });
    console.log({
      resCountryData: res
    });
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setCountryData(res.data));
  } catch (err) {
    console.log({
      errCountryData: err
    });
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setFailure(1090));
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setFailure(err.response.data.CODE));
  }
}
function* countrySearchData({
  page,
  title
}) {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.startSearchCountry()); //   try {
  //     const res = yield panelAdmin.api.get.CountrySearch({ page, title });
  //     console.log({ resCountrySearchData: res });
  //     yield put(actions.reduxActions.setSearchCountryData(res.data));
  //   } catch (err) {
  //     console.log({ errCountrySearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}

/***/ }),

/***/ "./panelAdmin/storeAdmin/saga/webServices/GET/galleryApi.js":
/*!******************************************************************!*\
  !*** ./panelAdmin/storeAdmin/saga/webServices/GET/galleryApi.js ***!
  \******************************************************************/
/*! exports provided: galleryData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "galleryData", function() { return galleryData; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../actions */ "./panelAdmin/storeAdmin/actions/index.js");



function* galleryData({
  data
}) {
  try {
    const res = yield axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`https://rimtal.com/api/v1/admin/gallery/${data.type}/${data.page} `);
    console.log({
      res
    });
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setGalleryData(res.data)); //********** Level 5 **********//
  } catch (err) {
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(1090));
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(err.response.data.CODE)); //********** Level 6 **********//
  }
}

/***/ }),

/***/ "./panelAdmin/storeAdmin/saga/webServices/GET/genre.js":
/*!*************************************************************!*\
  !*** ./panelAdmin/storeAdmin/saga/webServices/GET/genre.js ***!
  \*************************************************************/
/*! exports provided: genreData, genreSearchData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "genreData", function() { return genreData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "genreSearchData", function() { return genreSearchData; });
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../actions */ "./panelAdmin/storeAdmin/actions/index.js");
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../index */ "./panelAdmin/index.js");



function* genreData({
  page
}) {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.startGetGenre());

  try {
    const res = yield _index__WEBPACK_IMPORTED_MODULE_2__["default"].api.get.genres({
      page
    });
    console.log({
      resGenreData: res
    });
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setGenreData(res.data));
  } catch (err) {
    console.log({
      errGenreData: err
    });
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setFailure(1090));
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setFailure(err.response.data.CODE));
  }
}
function* genreSearchData({
  page,
  title
}) {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.startSearchGenre()); //   try {
  //     const res = yield panelAdmin.api.get.GenreSearch({ page, title });
  //     console.log({ resGenreSearchData: res });
  //     yield put(actions.reduxActions.setSearchGenreData(res.data));
  //   } catch (err) {
  //     console.log({ errGenreSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}

/***/ }),

/***/ "./panelAdmin/storeAdmin/saga/webServices/GET/hashtag.js":
/*!***************************************************************!*\
  !*** ./panelAdmin/storeAdmin/saga/webServices/GET/hashtag.js ***!
  \***************************************************************/
/*! exports provided: hashtagData, hashtagSearchData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hashtagData", function() { return hashtagData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hashtagSearchData", function() { return hashtagSearchData; });
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../actions */ "./panelAdmin/storeAdmin/actions/index.js");
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../index */ "./panelAdmin/index.js");



function* hashtagData({
  page
}) {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.startGetHashtag());

  try {
    const res = yield _index__WEBPACK_IMPORTED_MODULE_2__["default"].api.get.hashtag({
      page
    });
    console.log({
      resArtistData: res
    });
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setHashtagData(res.data));
  } catch (err) {
    console.log({
      errArtistData: err
    });
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setFailure(1090));
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setFailure(err.response.data.CODE));
  }
}
function* hashtagSearchData({
  page,
  title
}) {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.startSearchHashtag()); //   try {
  //     const res = yield panelAdmin.api.get.artistSearch({ page, title });
  //     console.log({ resArtistSearchData: res });
  //     yield put(actions.reduxActions.setSearchArtistData(res.data));
  //   } catch (err) {
  //     console.log({ errArtistSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}

/***/ }),

/***/ "./panelAdmin/storeAdmin/saga/webServices/GET/homeData.js":
/*!****************************************************************!*\
  !*** ./panelAdmin/storeAdmin/saga/webServices/GET/homeData.js ***!
  \****************************************************************/
/*! exports provided: homeData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "homeData", function() { return homeData; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../actions */ "./panelAdmin/storeAdmin/actions/index.js");



function* homeData() {
  try {
    const res = yield axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("https://rimtal.com/api/v1/home");
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setHomeData(res.data)); //********** Level 5 **********//
  } catch (err) {
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(1090));
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(err.response.data.CODE)); //********** Level 6 **********//
  }
}

/***/ }),

/***/ "./panelAdmin/storeAdmin/saga/webServices/GET/index.js":
/*!*************************************************************!*\
  !*** ./panelAdmin/storeAdmin/saga/webServices/GET/index.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _homeData__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./homeData */ "./panelAdmin/storeAdmin/saga/webServices/GET/homeData.js");
/* harmony import */ var _galleryApi__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./galleryApi */ "./panelAdmin/storeAdmin/saga/webServices/GET/galleryApi.js");
/* harmony import */ var _artists__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./artists */ "./panelAdmin/storeAdmin/saga/webServices/GET/artists.js");
/* harmony import */ var _instrumental__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./instrumental */ "./panelAdmin/storeAdmin/saga/webServices/GET/instrumental.js");
/* harmony import */ var _country__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./country */ "./panelAdmin/storeAdmin/saga/webServices/GET/country.js");
/* harmony import */ var _mood__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./mood */ "./panelAdmin/storeAdmin/saga/webServices/GET/mood.js");
/* harmony import */ var _hashtag__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./hashtag */ "./panelAdmin/storeAdmin/saga/webServices/GET/hashtag.js");
/* harmony import */ var _genre__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./genre */ "./panelAdmin/storeAdmin/saga/webServices/GET/genre.js");
/* harmony import */ var _albums__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./albums */ "./panelAdmin/storeAdmin/saga/webServices/GET/albums.js");









const GET = {
  homeData: _homeData__WEBPACK_IMPORTED_MODULE_0__["homeData"],
  // ================================= GALLERY
  galleryData: _galleryApi__WEBPACK_IMPORTED_MODULE_1__["galleryData"],
  // ================================= ARTIST
  artistData: _artists__WEBPACK_IMPORTED_MODULE_2__["artistData"],
  artistSearchData: _artists__WEBPACK_IMPORTED_MODULE_2__["artistSearchData"],
  // ================================= INSTRUMENT
  instrumentData: _instrumental__WEBPACK_IMPORTED_MODULE_3__["instrumentData"],
  instrumentSearchData: _instrumental__WEBPACK_IMPORTED_MODULE_3__["instrumentSearchData"],
  // ================================= COUNTRY
  countryData: _country__WEBPACK_IMPORTED_MODULE_4__["countryData"],
  countrySearchData: _country__WEBPACK_IMPORTED_MODULE_4__["countrySearchData"],
  // ================================= MOOD
  moodData: _mood__WEBPACK_IMPORTED_MODULE_5__["moodData"],
  moodSearchData: _mood__WEBPACK_IMPORTED_MODULE_5__["moodSearchData"],
  // ================================= HASHTAG
  hashtagData: _hashtag__WEBPACK_IMPORTED_MODULE_6__["hashtagData"],
  hashtagSearchData: _hashtag__WEBPACK_IMPORTED_MODULE_6__["hashtagSearchData"],
  // ================================= GENRE
  genreData: _genre__WEBPACK_IMPORTED_MODULE_7__["genreData"],
  genreSearchData: _genre__WEBPACK_IMPORTED_MODULE_7__["genreSearchData"],
  // ================================= ALBUM
  albumsData: _albums__WEBPACK_IMPORTED_MODULE_8__["albumsData"],
  albumSearchData: _albums__WEBPACK_IMPORTED_MODULE_8__["albumSearchData"]
};
/* harmony default export */ __webpack_exports__["default"] = (GET);

/***/ }),

/***/ "./panelAdmin/storeAdmin/saga/webServices/GET/instrumental.js":
/*!********************************************************************!*\
  !*** ./panelAdmin/storeAdmin/saga/webServices/GET/instrumental.js ***!
  \********************************************************************/
/*! exports provided: instrumentData, instrumentSearchData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "instrumentData", function() { return instrumentData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "instrumentSearchData", function() { return instrumentSearchData; });
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../actions */ "./panelAdmin/storeAdmin/actions/index.js");
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../index */ "./panelAdmin/index.js");



function* instrumentData({
  page
}) {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.startGetInstrument());
  console.log({
    mojtaba3: page
  });

  try {
    const res = yield _index__WEBPACK_IMPORTED_MODULE_2__["default"].api.get.instrument({
      page
    });
    console.log({
      resArtistData: res
    });
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setArtistData(res.data));
  } catch (err) {
    console.log({
      errArtistData: err
    });
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setFailure(1090));
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setFailure(err.response.data.CODE));
  }
}
function* instrumentSearchData({
  page,
  title
}) {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.startSearchInstrument()); //   try {
  //     const res = yield panelAdmin.api.get.artistSearch({ page, title });
  //     console.log({ resArtistSearchData: res });
  //     yield put(actions.reduxActions.setSearchArtistData(res.data));
  //   } catch (err) {
  //     console.log({ errArtistSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}

/***/ }),

/***/ "./panelAdmin/storeAdmin/saga/webServices/GET/mood.js":
/*!************************************************************!*\
  !*** ./panelAdmin/storeAdmin/saga/webServices/GET/mood.js ***!
  \************************************************************/
/*! exports provided: moodData, moodSearchData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "moodData", function() { return moodData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "moodSearchData", function() { return moodSearchData; });
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../actions */ "./panelAdmin/storeAdmin/actions/index.js");
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../index */ "./panelAdmin/index.js");



function* moodData({
  page
}) {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.startGetMood());
  console.log({
    mojtaba3: page
  });

  try {
    const res = yield _index__WEBPACK_IMPORTED_MODULE_2__["default"].api.get.mood({
      page
    });
    console.log({
      resArtistData: res
    });
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setMoodData(res.data));
  } catch (err) {
    console.log({
      errArtistData: err
    });
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setFailure(1090));
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.setFailure(err.response.data.CODE));
  }
}
function* moodSearchData({
  page,
  title
}) {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(_actions__WEBPACK_IMPORTED_MODULE_1__["default"].reduxActions.startSearchMood()); //   try {
  //     const res = yield panelAdmin.api.get.artistSearch({ page, title });
  //     console.log({ resArtistSearchData: res });
  //     yield put(actions.reduxActions.setSearchArtistData(res.data));
  //   } catch (err) {
  //     console.log({ errArtistSearchData: err });
  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}

/***/ }),

/***/ "./panelAdmin/storeAdmin/saga/webServices/POST/index.js":
/*!**************************************************************!*\
  !*** ./panelAdmin/storeAdmin/saga/webServices/POST/index.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _uploadImage__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./uploadImage */ "./panelAdmin/storeAdmin/saga/webServices/POST/uploadImage.js");

const POST = {
  uploadImage: _uploadImage__WEBPACK_IMPORTED_MODULE_0__["uploadImage"]
};
/* harmony default export */ __webpack_exports__["default"] = (POST);

/***/ }),

/***/ "./panelAdmin/storeAdmin/saga/webServices/POST/uploadImage.js":
/*!********************************************************************!*\
  !*** ./panelAdmin/storeAdmin/saga/webServices/POST/uploadImage.js ***!
  \********************************************************************/
/*! exports provided: uploadImage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "uploadImage", function() { return uploadImage; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../actions */ "./panelAdmin/storeAdmin/actions/index.js");
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../index */ "./panelAdmin/index.js");




function* uploadImage(props) {
  console.log("uploaaaaaaaaad omad");
  const {
    imageName,
    type,
    files
  } = props;
  console.log({
    uploadUploadImage: props
  });
  const toastify = _index__WEBPACK_IMPORTED_MODULE_3__["default"].utils.toastify;
  const strings = _index__WEBPACK_IMPORTED_MODULE_3__["default"].values.strings;
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total); // setState((prev) => ({ ...prev, progressPercentImage: percentCompleted }));
      // yield put(actions.reduxActions.uploadImageProcess());

      console.log(percentCompleted);
    },
    cancelToken: source.token
  };
  const URL = strings.ApiString.IMAGE_UPLOAD;
  const formData = new FormData();
  formData.append("imageName", imageName);
  formData.append("imageType", type);
  formData.append("image", files);

  try {
    const res = yield axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(URL, formData, settings);
    return res; // yield put(actions.reduxActions.setUploadImage(res.data));
  } catch (err) {
    console.log({
      err
    });
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(1090)); // yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}

/***/ }),

/***/ "./panelAdmin/storeAdmin/saga/webServices/index.js":
/*!*********************************************************!*\
  !*** ./panelAdmin/storeAdmin/saga/webServices/index.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _GET__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./GET */ "./panelAdmin/storeAdmin/saga/webServices/GET/index.js");
/* harmony import */ var _POST__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./POST */ "./panelAdmin/storeAdmin/saga/webServices/POST/index.js");


const webServices = {
  GET: _GET__WEBPACK_IMPORTED_MODULE_0__["default"],
  POST: _POST__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (webServices);

/***/ }),

/***/ "./panelAdmin/utils/CelanderConvert/convertToCelander.js":
/*!***************************************************************!*\
  !*** ./panelAdmin/utils/CelanderConvert/convertToCelander.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const convertToCelander = value => {
  let valDate,
      day,
      month,
      year,
      selectedDay = null;

  if (value) {
    valDate = value.split("/");
    day = Number(valDate[2]);
    month = Number(valDate[1]);
    year = Number(valDate[0]);
    selectedDay = {
      day,
      month,
      year
    };
  }

  return selectedDay;
};

/* harmony default export */ __webpack_exports__["default"] = (convertToCelander);

/***/ }),

/***/ "./panelAdmin/utils/CelanderConvert/convertToDate.js":
/*!***********************************************************!*\
  !*** ./panelAdmin/utils/CelanderConvert/convertToDate.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const convertToDate = selectedDay => {
  let day = selectedDay.day;
  let month = selectedDay.month;
  let year = selectedDay.year;
  let date = year + "/" + month + "/" + day;
  return date;
};

/* harmony default export */ __webpack_exports__["default"] = (convertToDate);

/***/ }),

/***/ "./panelAdmin/utils/CelanderConvert/index.js":
/*!***************************************************!*\
  !*** ./panelAdmin/utils/CelanderConvert/index.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _convertToCelander__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./convertToCelander */ "./panelAdmin/utils/CelanderConvert/convertToCelander.js");
/* harmony import */ var _convertToDate__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./convertToDate */ "./panelAdmin/utils/CelanderConvert/convertToDate.js");


const CelanderConvert = {
  convertToCelander: _convertToCelander__WEBPACK_IMPORTED_MODULE_0__["default"],
  convertToDate: _convertToDate__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (CelanderConvert);

/***/ }),

/***/ "./panelAdmin/utils/checkValidity.js":
/*!*******************************************!*\
  !*** ./panelAdmin/utils/checkValidity.js ***!
  \*******************************************/
/*! exports provided: checkValidity */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "checkValidity", function() { return checkValidity; });
const checkValidity = (value, rules, array) => {
  let isValid = true;
  console.log({
    value,
    rules,
    array
  });
  console.log({
    mehrad: typeof value
  });

  if (!rules) {
    return true;
  }

  if (array) {
    return isValid = value.length > 0;
  }

  if (rules.required) {
    if (typeof value === "object") {
      isValid = true;
    } else {
      isValid = value.trim() !== "" && isValid;
    }
  }

  if (rules.minLength) {
    isValid = value.length >= rules.minLength && isValid;
  }

  if (rules.maxLength) {
    isValid = value.length <= rules.maxLength && isValid;
  }

  if (rules.isEmail) {
    const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isNumeric) {
    const pattern = /^\d+$/;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isPhone) {
    const pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isMobile) {
    const pattern = /[0,9]{2}\d{9}/g;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isEn) {
    const pattern = /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.isFa) {
    const pattern = /^[\u0600-\u06FF\s]+$/;
    isValid = pattern.test(value) && isValid;
  }

  return isValid;
};

/***/ }),

/***/ "./panelAdmin/utils/consts/card/artist.js":
/*!************************************************!*\
  !*** ./panelAdmin/utils/consts/card/artist.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const artist = (data, acceptedCard) => {
  const cardFormat = [];
  console.log({
    insss: data
  });

  for (let index in data) {
    let dataIndex = data[index];
    let web = dataIndex.web ? dataIndex.web : "";
    let phone = dataIndex.phone ? dataIndex.phone : "";
    let title = dataIndex.nameFa ? dataIndex.nameFa : "";
    let description = dataIndex.descriptionFa ? dataIndex.descriptionFa : "";
    let images = dataIndex.avatar ? dataIndex.avatar : ""; // console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      isActive: data[index].isActive,
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          style: {
            color: "#8c8181",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (artist);

/***/ }),

/***/ "./panelAdmin/utils/consts/card/country.js":
/*!*************************************************!*\
  !*** ./panelAdmin/utils/consts/card/country.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const country = data => {
  const cardFormat = [];
  console.log({
    data
  });

  for (let index in data) {
    let dataIndex = data[index];
    let description = dataIndex.titleFa ? dataIndex.titleFa : "";
    let price = dataIndex.price ? dataIndex.price : "";
    let title = dataIndex.titleFa ? dataIndex.titleFa : "";
    let images = data[index].flag ? data[index].flag : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }, {
        left: [{
          elementType: "price",
          value: price,
          direction: "ltr"
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (country);

/***/ }),

/***/ "./panelAdmin/utils/consts/card/gallery.js":
/*!*************************************************!*\
  !*** ./panelAdmin/utils/consts/card/gallery.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const instrument = (data, acceptedCard) => {
  const cardFormat = [];
  console.log({
    insss: data
  });

  for (let index in data) {
    let dataIndex = data[index];
    let web = dataIndex.web ? dataIndex.web : "";
    let phone = dataIndex.phone ? dataIndex.phone : "";
    let title = dataIndex.title ? dataIndex.title : ""; // let images = { web, phone };
    // console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? acceptedCard.web === web || acceptedCard.phone === phone ? "activeImage" : "" : "",
      image: {
        value: {
          web,
          phone
        }
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (instrument);

/***/ }),

/***/ "./panelAdmin/utils/consts/card/genre.js":
/*!***********************************************!*\
  !*** ./panelAdmin/utils/consts/card/genre.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../.. */ "./panelAdmin/index.js");



const genre = data => {
  const cardFormat = [];
  console.log({
    data
  });

  for (let index in data) {
    let noEntries = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.strings.NO_ENTRIES;
    let dataIndex = data[index];
    let description = dataIndex.titleFa ? dataIndex.titleFa : noEntries;
    let title = dataIndex.titleFa ? dataIndex.titleFa : noEntries; // let images = dataIndex.images ? dataIndex.images : "";

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      // image: { value: images },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (genre);

/***/ }),

/***/ "./panelAdmin/utils/consts/card/hashtag.js":
/*!*************************************************!*\
  !*** ./panelAdmin/utils/consts/card/hashtag.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../.. */ "./panelAdmin/index.js");



const hashtag = data => {
  const cardFormat = [];
  console.log({
    data
  });

  for (let index in data) {
    let noEntries = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.strings.NO_ENTRIES;
    let dataIndex = data[index];
    let description = dataIndex.titleFa ? dataIndex.titleFa : noEntries;
    let title = dataIndex.titleFa ? dataIndex.titleFa : noEntries; // let images = dataIndex.images ? dataIndex.images : "";

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      // image: { value: images },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (hashtag);

/***/ }),

/***/ "./panelAdmin/utils/consts/card/index.js":
/*!***********************************************!*\
  !*** ./panelAdmin/utils/consts/card/index.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _instrument__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./instrument */ "./panelAdmin/utils/consts/card/instrument.js");
/* harmony import */ var _gallery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./gallery */ "./panelAdmin/utils/consts/card/gallery.js");
/* harmony import */ var _country__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./country */ "./panelAdmin/utils/consts/card/country.js");
/* harmony import */ var _mood__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./mood */ "./panelAdmin/utils/consts/card/mood.js");
/* harmony import */ var _hashtag__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./hashtag */ "./panelAdmin/utils/consts/card/hashtag.js");
/* harmony import */ var _genre__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./genre */ "./panelAdmin/utils/consts/card/genre.js");
/* harmony import */ var _artist__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./artist */ "./panelAdmin/utils/consts/card/artist.js");







const card = {
  instrument: _instrument__WEBPACK_IMPORTED_MODULE_0__["default"],
  gallery: _gallery__WEBPACK_IMPORTED_MODULE_1__["default"],
  country: _country__WEBPACK_IMPORTED_MODULE_2__["default"],
  mood: _mood__WEBPACK_IMPORTED_MODULE_3__["default"],
  hashtag: _hashtag__WEBPACK_IMPORTED_MODULE_4__["default"],
  artist: _artist__WEBPACK_IMPORTED_MODULE_6__["default"],
  genre: _genre__WEBPACK_IMPORTED_MODULE_5__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (card);

/***/ }),

/***/ "./panelAdmin/utils/consts/card/instrument.js":
/*!****************************************************!*\
  !*** ./panelAdmin/utils/consts/card/instrument.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const instrument = data => {
  const cardFormat = [];

  for (let index in data) {
    let dataIndex = data[index];
    let description = dataIndex.descriptionFa ? dataIndex.descriptionFa : "";
    let price = dataIndex.price ? dataIndex.price : "";
    let title = dataIndex.titleFa ? dataIndex.titleFa : "";
    let images = data[index].thumbnail ? data[index].thumbnail : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }, {
        left: [{
          elementType: "price",
          value: price,
          direction: "ltr"
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (instrument);

/***/ }),

/***/ "./panelAdmin/utils/consts/card/mood.js":
/*!**********************************************!*\
  !*** ./panelAdmin/utils/consts/card/mood.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const mood = data => {
  const cardFormat = [];
  console.log({
    data
  });

  for (let index in data) {
    let dataIndex = data[index];
    let description = dataIndex.titleFa ? dataIndex.titleFa : "";
    let price = dataIndex.price ? dataIndex.price : "";
    let title = dataIndex.titleFa ? dataIndex.titleFa : "";
    let images = dataIndex.images ? dataIndex.images : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: description,
          title: description,
          style: {
            color: "#8c8181",
            fontSize: "0.6em",
            fontWeight: "500"
          }
        }]
      }, {
        left: [{
          elementType: "price",
          value: price,
          direction: "ltr"
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (mood);

/***/ }),

/***/ "./panelAdmin/utils/consts/galleryConstants.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/utils/consts/galleryConstants.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const galleryConstants = () => {
  const ConstantsEn = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.en;
  const string = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.strings;
  return [{
    value: ConstantsEn.ALBUM_CONSTANTS,
    title: string.ALBUM_CONSTANTS
  }, {
    value: ConstantsEn.PLAY_LIST_CONSTANTS,
    title: string.PLAY_LIST_CONSTANTS
  }, {
    value: ConstantsEn.FLAG_CONSTANTS,
    title: string.FLAG_CONSTANTS
  }, {
    value: ConstantsEn.SONG_CONSTANTS,
    title: string.SONG_CONSTANTS
  }, {
    value: ConstantsEn.ARTIST_CONSTANTS,
    title: string.ARTIST_CONSTANTS
  }, {
    value: ConstantsEn.INSTRUMENT_CONSTANTS,
    title: string.INSTRUMENT_CONSTANTS
  }, {
    value: ConstantsEn.MUSIC_VIDEO_CONSTANTS,
    title: string.MUSIC_VIDEO_CONSTANTS
  }, {
    value: ConstantsEn.MOOD_CONSTANTS,
    title: string.MOOD_CONSTANTS
  }];
};

/* harmony default export */ __webpack_exports__["default"] = (galleryConstants);

/***/ }),

/***/ "./panelAdmin/utils/consts/index.js":
/*!******************************************!*\
  !*** ./panelAdmin/utils/consts/index.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _table__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./table */ "./panelAdmin/utils/consts/table/index.js");
/* harmony import */ var _states__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./states */ "./panelAdmin/utils/consts/states/index.js");
/* harmony import */ var _galleryConstants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./galleryConstants */ "./panelAdmin/utils/consts/galleryConstants.js");
/* harmony import */ var _card__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./card */ "./panelAdmin/utils/consts/card/index.js");




const consts = {
  table: _table__WEBPACK_IMPORTED_MODULE_0__["default"],
  states: _states__WEBPACK_IMPORTED_MODULE_1__["default"],
  galleryConstants: _galleryConstants__WEBPACK_IMPORTED_MODULE_2__["default"],
  card: _card__WEBPACK_IMPORTED_MODULE_3__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (consts);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addAlbum.js":
/*!****************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addAlbum.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addAlbum = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    artist: {
      label: "هنرمند :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "هنرمند"
      },
      value: "",
      validation: {
        minLength: 1,
        required: true
      },
      valid: false,
      touched: false
    },
    genres: {
      label: "ژانر :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "ژانر"
      },
      value: "",
      validation: {
        minLength: 1,
        required: true
      },
      valid: false,
      touched: false
    },
    songs: {
      label: "آهنگ ها :",
      elementType: "InputFileArray",
      kindOf: "voice",
      value: [],
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: " عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addAlbum);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addArtist.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addArtist.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addArtist = {
  Form: {
    nameFa: {
      label: "نام فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام فارسی"
      },
      value: "",
      validation: {
        required: false,
        isFa: false
      },
      valid: true,
      touched: false
    },
    nameEn: {
      label: "نام انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " نام انگلیسی"
      },
      value: "",
      validation: {
        required: false,
        isEn: true
      },
      valid: true,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: false,
        isFa: false
      },
      valid: true,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: false,
        isEn: true
      },
      valid: true,
      touched: false
    },
    defaultGenre: {
      label: "ژانر اصلی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "ژانر اصلی"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    genre: {
      label: "ژانر های دیگر :",
      elementType: "inputDropDownSearchArray",
      elementConfig: {
        type: "text",
        placeholder: "ژانر های دیگر"
      },
      value: [],
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    birthPlaceCountry: {
      label: "متولد در کشور  :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "متولد در کشور "
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    birthPlaceCity: {
      label: "متولد در شهر  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "متولد در شهر "
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    birthday: {
      label: "تاریخ تولد :",
      elementType: "dateInput",
      elementConfig: {
        type: "text",
        placeholder: "بر روی تقویم کلیک نمایید "
      },
      value: {
        month: "",
        day: "",
        year: ""
      },
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    instruments: {
      label: "ساز ها :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "ساز ها"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    website: {
      label: "وب سایت :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "وب سایت"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    instagram: {
      label: "اینستاگرام :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "اینستاگرام"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    youTube: {
      label: "یوتیوب :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "یوتیوب"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    twitter: {
      label: "توییتر :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توییتر"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    avatar: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addArtist);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addBanner.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addBanner.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addBanner = {
  Form: {
    parentType: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد"
      },
      childValue: [{
        name: "باشگاه",
        value: "CLUB"
      }, {
        name: "تخفیف",
        value: "DISCOUNT"
      }],
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    discount: {
      label: "تخفیف :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "تخفیف"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    club: {
      label: "باشگاه :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "باشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addBanner);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addCategory.js":
/*!*******************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addCategory.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const owner = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    weight: {
      label: "وزن :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "وزن"
      },
      value: "",
      validation: {
        minLength: 1,
        isNumeric: true,
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (owner);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addClub.js":
/*!***************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addClub.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addClub = {
  Form: {
    membership: {
      label: "عضویت :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عضویت"
      },
      childValue: [{
        value: "APPLICATION"
      }, {
        value: "CARD"
      }],
      value: [],
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    owner: {
      label: "فروشنده :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشنده"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    aboutClub: {
      label: " درباره باشگاه :",
      elementType: "textarea",
      elementConfig: {
        type: "text",
        placeholder: " درباره باشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    percent: {
      label: "درصد :",
      elementType: "input",
      value: "",
      validation: {
        minLength: 1,
        maxLength: 3,
        isNumeric: true,
        required: true
      },
      valid: false,
      touched: false
    },
    slides: {
      label: "اسلایدس :",
      elementType: "InputFileArray",
      kindOf: "image",
      value: [],
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addClub);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addCountry.js":
/*!******************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addCountry.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addCountry = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    flag: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addCountry);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addGallery.js":
/*!******************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addGallery.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addGallery = {
  Form: {
    imageName: {
      label: "نام عکس :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام عکس"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    imageType: {
      label: "دسته عکس :",
      elementType: "inputDropDown",
      elementConfig: {
        type: "text",
        placeholder: " دسته عکس"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: " عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addGallery);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addGenres.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addGenres.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addGenres = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addGenres);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addHashtag.js":
/*!******************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addHashtag.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addHashtag = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addHashtag);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addMood.js":
/*!***************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addMood.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addMood = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    images: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addMood);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addScenario.js":
/*!*******************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addScenario.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addScenario = {
  Form: {
    name: {
      label: "نام :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    spinRepeatTime: {
      label: "زمان چرخش مجدد (روز):",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "روز"
      },
      value: "",
      validation: {
        required: true,
        minLength: 1,
        maxLength: 1,
        isNumeric: true
      },
      valid: false,
      touched: false
    },
    startDate: {
      label: "تاریخ شروع :",
      elementType: "date",
      elementConfig: {
        type: "text",
        placeholder: "بر روی تقویم کلیک نمایید "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    endDate: {
      label: "تاریخ پایان :",
      elementType: "date",
      elementConfig: {
        type: "text",
        placeholder: "بر روی تقویم کلیک نمایید "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    empty: {
      label: "پوچ ها :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "پوچ ها"
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true
      },
      value: [],
      valid: false,
      touched: false
    },
    gifts: {
      label: "جوایز :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "جوایز"
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true
      },
      value: [],
      valid: false,
      touched: false
    },
    winnersCount: {
      label: "تعداد برندگان :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "تعداد برندگان"
      },
      value: "",
      validation: {
        required: true,
        minLength: 1,
        isNumeric: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addScenario);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addSlider.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addSlider.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addSlider = {
  Form: {
    parentType: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد"
      },
      childValue: [{
        name: "باشگاه",
        value: "CLUB"
      }, {
        name: "تخفیف",
        value: "DISCOUNT"
      }],
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    type: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    discount: {
      label: "تخفیف :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "تخفیف"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    club: {
      label: "باشگاه :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "باشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addSlider);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addSong.js":
/*!***************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addSong.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addSong = {
  Form: {
    songName: {
      label: "نام آهنگ :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام آهنگ"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    song: {
      label: " آهنگ :",
      elementType: "inputFile",
      kindOf: "voice",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addSong);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/index.js":
/*!*************************************************!*\
  !*** ./panelAdmin/utils/consts/states/index.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _addGenres__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./addGenres */ "./panelAdmin/utils/consts/states/addGenres.js");
/* harmony import */ var _addCategory__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./addCategory */ "./panelAdmin/utils/consts/states/addCategory.js");
/* harmony import */ var _addArtist__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./addArtist */ "./panelAdmin/utils/consts/states/addArtist.js");
/* harmony import */ var _addSlider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./addSlider */ "./panelAdmin/utils/consts/states/addSlider.js");
/* harmony import */ var _addClub__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./addClub */ "./panelAdmin/utils/consts/states/addClub.js");
/* harmony import */ var _upload__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./upload */ "./panelAdmin/utils/consts/states/upload.js");
/* harmony import */ var _addBanner__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./addBanner */ "./panelAdmin/utils/consts/states/addBanner.js");
/* harmony import */ var _addScenario__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./addScenario */ "./panelAdmin/utils/consts/states/addScenario.js");
/* harmony import */ var _addAlbum__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./addAlbum */ "./panelAdmin/utils/consts/states/addAlbum.js");
/* harmony import */ var _addCountry__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./addCountry */ "./panelAdmin/utils/consts/states/addCountry.js");
/* harmony import */ var _instruments__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./instruments */ "./panelAdmin/utils/consts/states/instruments.js");
/* harmony import */ var _addGallery__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./addGallery */ "./panelAdmin/utils/consts/states/addGallery.js");
/* harmony import */ var _addSong__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./addSong */ "./panelAdmin/utils/consts/states/addSong.js");
/* harmony import */ var _addMood__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./addMood */ "./panelAdmin/utils/consts/states/addMood.js");
/* harmony import */ var _addHashtag__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./addHashtag */ "./panelAdmin/utils/consts/states/addHashtag.js");















const states = {
  addScenario: _addScenario__WEBPACK_IMPORTED_MODULE_7__["default"],
  addGenres: _addGenres__WEBPACK_IMPORTED_MODULE_0__["default"],
  addCategory: _addCategory__WEBPACK_IMPORTED_MODULE_1__["default"],
  addArtist: _addArtist__WEBPACK_IMPORTED_MODULE_2__["default"],
  addSlider: _addSlider__WEBPACK_IMPORTED_MODULE_3__["default"],
  addClub: _addClub__WEBPACK_IMPORTED_MODULE_4__["default"],
  upload: _upload__WEBPACK_IMPORTED_MODULE_5__["default"],
  addBanner: _addBanner__WEBPACK_IMPORTED_MODULE_6__["default"],
  addAlbum: _addAlbum__WEBPACK_IMPORTED_MODULE_8__["default"],
  addCountry: _addCountry__WEBPACK_IMPORTED_MODULE_9__["default"],
  instrument: _instruments__WEBPACK_IMPORTED_MODULE_10__["default"],
  addGallery: _addGallery__WEBPACK_IMPORTED_MODULE_11__["default"],
  addSong: _addSong__WEBPACK_IMPORTED_MODULE_12__["default"],
  addMood: _addMood__WEBPACK_IMPORTED_MODULE_13__["default"],
  addHashtag: _addHashtag__WEBPACK_IMPORTED_MODULE_14__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (states);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/instruments.js":
/*!*******************************************************!*\
  !*** ./panelAdmin/utils/consts/states/instruments.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addInstruments = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "عنوان انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی"
      },
      value: "",
      validation: {
        required: true,
        isFa: true
      },
      valid: false,
      touched: false
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی"
      },
      value: "",
      validation: {
        required: true,
        isEn: true
      },
      valid: false,
      touched: false
    },
    thumbnail: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addInstruments);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/upload.js":
/*!**************************************************!*\
  !*** ./panelAdmin/utils/consts/states/upload.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const upload = {
  Form: {
    file: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (upload);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/ShowClub.js":
/*!***************************************************!*\
  !*** ./panelAdmin/utils/consts/table/ShowClub.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/utils/consts/table/ShowClub.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const ShowClub = data => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "عضویت ", "دسته بندی ", " تخفیف", "فروش", "بازدید ", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let cardElement = __jsx("i", {
      style: {
        fontSize: "1.2em"
      },
      className: "fal fa-credit-card",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 7,
        columnNumber: 23
      }
    });

    let applicationElement = __jsx("i", {
      style: {
        fontSize: "1.2em"
      },
      className: "fal fa-mobile-android",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 8,
        columnNumber: 30
      }
    });

    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 9,
        columnNumber: 18
      }
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 20
      }
    });

    let NotEntered = "وارد نشده";
    let thumbnail = data[index].slides[0] ? data[index].slides[0] : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : 0;
    let percent = data[index].percent ? data[index].percent + " %" : NotEntered;
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : "";
    let membership = data[index].membership.length ? data[index].membership : NotEntered;
    let membershipData = membership.includes("CARD") && membership.includes("APPLICATION") ? __jsx("div", {
      style: {
        fontSize: "1.2em",
        fontWeight: "900",
        display: "flex",
        justifyContent: "space-around"
      },
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22,
        columnNumber: 9
      }
    }, " ", __jsx("div", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24,
        columnNumber: 11
      }
    }, cardElement, " "), __jsx("div", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25,
        columnNumber: 11
      }
    }, " ", applicationElement)) : membership.includes("APPLICATION") ? applicationElement : membership.includes("CARD") ? cardElement : "";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, membershipData, categoryTitleFa, percent, boughtCount, viewCount, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (ShowClub);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/ShowDiscount.js":
/*!*******************************************************!*\
  !*** ./panelAdmin/utils/consts/table/ShowDiscount.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _formatMoney__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../formatMoney */ "./panelAdmin/utils/formatMoney.js");
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/utils/consts/table/ShowDiscount.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const ShowDiscount = data => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "دسته بندی  ", "قیمت اصلی", "قیمت جدید", " تخفیف", "فروش", "بازدید ", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 9,
        columnNumber: 18
      }
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 20
      }
    });

    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let realPrice = data[index].realPrice ? Object(_formatMoney__WEBPACK_IMPORTED_MODULE_1__["default"])(data[index].realPrice) : NotEntered;
    let newPrice = data[index].newPrice ? Object(_formatMoney__WEBPACK_IMPORTED_MODULE_1__["default"])(data[index].newPrice) : NotEntered;
    let rating = data[index].rating ? data[index].rating : 0;
    let percent = data[index].percent ? "%" + data[index].percent : NotEntered;
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : NotEntered;
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, categoryTitleFa, realPrice, newPrice, percent, boughtCount, viewCount, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (ShowDiscount);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/ShowOwner.js":
/*!****************************************************!*\
  !*** ./panelAdmin/utils/consts/table/ShowOwner.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/utils/consts/table/ShowOwner.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const ShowOwner = scenario => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "محدوده  ", " شماره همراه", "موجودی", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < scenario.length; index++) {
    let NotEntered = "وارد نشده";

    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 8,
        columnNumber: 18
      }
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 9,
        columnNumber: 20
      }
    });

    let thumbnail = scenario[index].thumbnail ? scenario[index].thumbnail : NotEntered;
    let title = scenario[index].title ? scenario[index].title : NotEntered;
    let rating = scenario[index].rating ? scenario[index].rating : NotEntered;
    let district = scenario[index].district ? scenario[index].district : NotEntered;
    let phoneNumber = scenario[index].phoneNumber ? scenario[index].phoneNumber : NotEntered;
    let balance = scenario[index].balance ? scenario[index].balance : "0";
    let isActive = scenario[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, district, phoneNumber, balance, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (ShowOwner);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/ShowScenario.js":
/*!*******************************************************!*\
  !*** ./panelAdmin/utils/consts/table/ShowScenario.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/utils/consts/table/ShowScenario.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const ShowScenario = scenario => {
  const thead = ["#", "نام", "تاریخ شروع ", " تاریخ پایان", "تعداد برندگان ", "شركت كنندگان", "برندگان", "جوایز", "پوچ ها", "زمان چرخش مجدد (روز)", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < scenario.length; index++) {
    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 7,
        columnNumber: 18
      }
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 8,
        columnNumber: 20
      }
    });

    tbody.push({
      data: [scenario[index].name, scenario[index].startDate, scenario[index].endDate, scenario[index].winnersCount, {
        option: {
          eye: true,
          name: "participants"
        }
      }, {
        option: {
          eye: true,
          name: "winners"
        }
      }, {
        option: {
          eye: true,
          name: "gifts"
        }
      }, {
        option: {
          eye: true,
          name: "empty"
        }
      }, scenario[index].spinRepeatTime, scenario[index].isActive ? active : deActive, {
        option: {
          edit: true
        }
      }],
      style: {
        background: scenario[index].isActive ? "rgb(100, 221, 23,0.3)" : ""
      }
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (ShowScenario);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/ShowScenarioDataInModal.js":
/*!******************************************************************!*\
  !*** ./panelAdmin/utils/consts/table/ShowScenarioDataInModal.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const ShowScenarioDataInModal = (data, name) => {
  const NotEntered = "وارد نشده";
  let thead = null;
  const headGift = ["شماره", "نام", "شماره همراه", "جایزه", " تاریخ "];
  const headNotGift = ["شماره", "نام", "شماره همراه", " تاریخ "];
  name === "participants" ? thead = headNotGift : thead = headGift;
  let tbody = [];
  let body = null;

  for (let index = 0; index < data.length; index++) {
    let fullName = data[index].fullName ? data[index].fullName : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let gift = data[index].gift ? data[index].gift : NotEntered;
    let date = data[index].date ? data[index].date : NotEntered;
    const bodyGift = [fullName, phoneNumber, gift, date];
    const bodyNotGift = [fullName, phoneNumber, date];
    name === "participants" ? body = bodyNotGift : body = bodyGift;
    tbody.push({
      data: body,
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (ShowScenarioDataInModal);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/ShowScenarioDataInModalTwo.js":
/*!*********************************************************************!*\
  !*** ./panelAdmin/utils/consts/table/ShowScenarioDataInModalTwo.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const ShowScenarioDataInModalTwo = (data, name) => {
  const NotEntered = "وارد نشده";
  const thead = ["شماره", name];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    tbody.push({
      data: [data[index] ? data[index] : NotEntered],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (ShowScenarioDataInModalTwo);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/ShowTransactions.js":
/*!***********************************************************!*\
  !*** ./panelAdmin/utils/consts/table/ShowTransactions.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _formatMoney__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../formatMoney */ "./panelAdmin/utils/formatMoney.js");
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/utils/consts/table/ShowTransactions.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const ShowTransactions = data => {
  const thead = ["#", "نام", " شماره همراه ", " تاریخ (زمان) ", " کل مبلغ", "تخفیف", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let NotEntered = "وارد نشده";
    let user = data[index].user ? data[index].user : NotEntered;
    let userName = user ? user.name : NotEntered;
    let userPhoneNumber = user ? user.phoneNumber : NotEntered;
    let date = dateSplit ? __jsx("span", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 28
      }
    }, dateSplit[1] + " - " + dateSplit[0]) : NotEntered;
    let totalPrice = data[index].totalPrice ? Object(_formatMoney__WEBPACK_IMPORTED_MODULE_1__["default"])(data[index].totalPrice) : "0";
    let showDiscount = {
      option: {
        eye: true,
        name: "discounts"
      }
    };
    let paymentStatus = data[index].paymentStatus ? __jsx("span", {
      style: {
        color: "green"
      },
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16,
        columnNumber: 53
      }
    }, "پرداخت شده") : __jsx("span", {
      style: {
        color: "red"
      },
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16,
        columnNumber: 110
      }
    }, " ", "پرداخت نشده");
    tbody.push({
      data: [userName, userPhoneNumber, date, totalPrice, showDiscount, paymentStatus],
      style: {
        background: data[index].isActive ? "rgb(100, 221, 23,0.3)" : ""
      }
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (ShowTransactions);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/artist.js":
/*!*************************************************!*\
  !*** ./panelAdmin/utils/consts/table/artist.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const artist = data => {
  console.log({
    data
  });
  const thead = ["#", "نام فارسی", "نام انگلیسی", "توضیحات فارسی ", "توضیحات انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", " لایک", " شنوندگان", " ساز", " بازدید", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let nameFa = data[index].nameFa ? data[index].nameFa : NotEntered;
    let nameEn = data[index].nameEn ? data[index].nameEn : NotEntered;
    let descriptionFa = data[index].descriptionFa ? data[index].descriptionFa : NotEntered;
    let descriptionEn = data[index].descriptionEn ? data[index].descriptionEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albums && data[index].albums.length ? data[index].albums.length : "0";
    let singlesCount = data[index].singles && data[index].singles.length ? data[index].singles.length : "0";
    let musicVideoCounts = data[index].musicVideos && data[index].musicVideos.length ? data[index].musicVideos.length : "0";
    let instrumentCounts = data[index].instruments && data[index].instruments.length ? data[index].instruments.length : "0";
    let likeCount = data[index].likeCount ? data[index].likeCount : "0";
    let listenCount = data[index].listenCount ? data[index].listenCount : "0";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    tbody.push({
      data: [nameFa, nameEn, descriptionFa, descriptionEn, followersCount, albumsCount, singlesCount, likeCount, listenCount, instrumentCounts, viewCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ __webpack_exports__["default"] = (artist);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/country.js":
/*!**************************************************!*\
  !*** ./panelAdmin/utils/consts/table/country.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const country = data => {
  const thead = ["#", "عکس", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let flag = data[index].flag ? data[index].flag : NotEntered;
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [flag, titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ __webpack_exports__["default"] = (country);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/genres.js":
/*!*************************************************!*\
  !*** ./panelAdmin/utils/consts/table/genres.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const genres = data => {
  const thead = ["#", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ __webpack_exports__["default"] = (genres);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/index.js":
/*!************************************************!*\
  !*** ./panelAdmin/utils/consts/table/index.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _genres__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./genres */ "./panelAdmin/utils/consts/table/genres.js");
/* harmony import */ var _ShowScenario__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ShowScenario */ "./panelAdmin/utils/consts/table/ShowScenario.js");
/* harmony import */ var _ShowScenarioDataInModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ShowScenarioDataInModal */ "./panelAdmin/utils/consts/table/ShowScenarioDataInModal.js");
/* harmony import */ var _ShowScenarioDataInModalTwo__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ShowScenarioDataInModalTwo */ "./panelAdmin/utils/consts/table/ShowScenarioDataInModalTwo.js");
/* harmony import */ var _ShowOwner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ShowOwner */ "./panelAdmin/utils/consts/table/ShowOwner.js");
/* harmony import */ var _ShowDiscount__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ShowDiscount */ "./panelAdmin/utils/consts/table/ShowDiscount.js");
/* harmony import */ var _ShowClub__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ShowClub */ "./panelAdmin/utils/consts/table/ShowClub.js");
/* harmony import */ var _ShowTransactions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ShowTransactions */ "./panelAdmin/utils/consts/table/ShowTransactions.js");
/* harmony import */ var _transactionDiscount__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./transactionDiscount */ "./panelAdmin/utils/consts/table/transactionDiscount.js");
/* harmony import */ var _members__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./members */ "./panelAdmin/utils/consts/table/members.js");
/* harmony import */ var _memberTransaction__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./memberTransaction */ "./panelAdmin/utils/consts/table/memberTransaction.js");
/* harmony import */ var _memberDiscount__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./memberDiscount */ "./panelAdmin/utils/consts/table/memberDiscount.js");
/* harmony import */ var _country__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./country */ "./panelAdmin/utils/consts/table/country.js");
/* harmony import */ var _instrument__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./instrument */ "./panelAdmin/utils/consts/table/instrument.js");
/* harmony import */ var _song__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./song */ "./panelAdmin/utils/consts/table/song.js");
/* harmony import */ var _artist__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./artist */ "./panelAdmin/utils/consts/table/artist.js");
















const table = {
  instrument: _instrument__WEBPACK_IMPORTED_MODULE_13__["default"],
  country: _country__WEBPACK_IMPORTED_MODULE_12__["default"],
  memberDiscount: _memberDiscount__WEBPACK_IMPORTED_MODULE_11__["default"],
  memberTransaction: _memberTransaction__WEBPACK_IMPORTED_MODULE_10__["default"],
  members: _members__WEBPACK_IMPORTED_MODULE_9__["default"],
  transactionDiscount: _transactionDiscount__WEBPACK_IMPORTED_MODULE_8__["default"],
  showTransaction: _ShowTransactions__WEBPACK_IMPORTED_MODULE_7__["default"],
  ShowClub: _ShowClub__WEBPACK_IMPORTED_MODULE_6__["default"],
  ShowDiscount: _ShowDiscount__WEBPACK_IMPORTED_MODULE_5__["default"],
  showOwner: _ShowOwner__WEBPACK_IMPORTED_MODULE_4__["default"],
  showScenario: _ShowScenario__WEBPACK_IMPORTED_MODULE_1__["default"],
  genres: _genres__WEBPACK_IMPORTED_MODULE_0__["default"],
  ShowScenarioDataInModal: _ShowScenarioDataInModal__WEBPACK_IMPORTED_MODULE_2__["default"],
  ShowScenarioDataInModalTwo: _ShowScenarioDataInModalTwo__WEBPACK_IMPORTED_MODULE_3__["default"],
  song: _song__WEBPACK_IMPORTED_MODULE_14__["default"],
  artist: _artist__WEBPACK_IMPORTED_MODULE_15__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (table);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/instrument.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/utils/consts/table/instrument.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const instrument = data => {
  const thead = ["#", "عکس", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [thumbnail, titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ __webpack_exports__["default"] = (instrument);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/memberDiscount.js":
/*!*********************************************************!*\
  !*** ./panelAdmin/utils/consts/table/memberDiscount.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _formatMoney__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../formatMoney */ "./panelAdmin/utils/formatMoney.js");
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/utils/consts/table/memberDiscount.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const memberDiscount = data => {
  console.log({
    data
  }); // let data =datas.discount

  const thead = ["#", "نام", "قیمت ", "تاریخ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dataIndex = data[index];
    let discount = data[index].discount;
    let discountName = discount.title;
    let dateSplit = data[index].usedDate ? data[index].usedDate.split(" ") : "";
    let date = dateSplit ? __jsx("span", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16,
        columnNumber: 28
      }
    }, dateSplit[1] + " - " + dateSplit[0]) : "استفاده نشده";
    let price = dataIndex.price ? Object(_formatMoney__WEBPACK_IMPORTED_MODULE_1__["default"])(dataIndex.price) : "0";
    let isUsed = dataIndex.isUsed ? "استفاده شده" : "استفاده نشده";
    tbody.push({
      data: [discountName, price, date, isUsed],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (memberDiscount);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/memberTransaction.js":
/*!************************************************************!*\
  !*** ./panelAdmin/utils/consts/table/memberTransaction.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _formatMoney__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../formatMoney */ "./panelAdmin/utils/formatMoney.js");
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/utils/consts/table/memberTransaction.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const memberTransaction = data => {
  console.log({
    data
  }); // let data =datas.discount

  const thead = ["#", "تاریخ", "قیمت ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dataIndex = data[index];
    let paymentStatus = data[index].paymentStatus ? __jsx("span", {
      style: {
        color: "green"
      },
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 53
      }
    }, "پرداخت شده") : __jsx("span", {
      style: {
        color: "red"
      },
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 110
      }
    }, " ", "پرداخت نشده");
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let date = dateSplit ? __jsx("span", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15,
        columnNumber: 28
      }
    }, dateSplit[1] + " - " + dateSplit[0]) : NotEntered;
    let totalPrice = dataIndex.totalPrice ? Object(_formatMoney__WEBPACK_IMPORTED_MODULE_1__["default"])(dataIndex.totalPrice) : "0";
    tbody.push({
      data: [date, totalPrice, paymentStatus],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (memberTransaction);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/members.js":
/*!**************************************************!*\
  !*** ./panelAdmin/utils/consts/table/members.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/utils/consts/table/members.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const members = data => {
  const thead = ["#", "نام", "شماره همراه", "تراکنش ها", "تخفیف ها", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 8,
        columnNumber: 18
      }
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 9,
        columnNumber: 20
      }
    }); // let avatar = data[index].avatar ? data[index].avatar : NotEntered;


    let name = data[index].name ? data[index].name : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let showTransaction = {
      option: {
        eye: true,
        name: "transactions"
      }
    };
    let showDiscount = {
      option: {
        eye: true,
        name: "discounts"
      }
    };
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [name, phoneNumber, showTransaction, showDiscount, isActive],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (members);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/song.js":
/*!***********************************************!*\
  !*** ./panelAdmin/utils/consts/table/song.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const song = data => {
  const thead = ["#", "نام ", "پخش "];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let title = data[index].title ? data[index].title : NotEntered;
    tbody.push({
      data: [title, {
        option: {
          play: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ __webpack_exports__["default"] = (song);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/transactionDiscount.js":
/*!**************************************************************!*\
  !*** ./panelAdmin/utils/consts/table/transactionDiscount.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _formatMoney__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../formatMoney */ "./panelAdmin/utils/formatMoney.js");
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/panelAdmin/utils/consts/table/transactionDiscount.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const transactionDiscount = data => {
  // let data =datas.discount
  const thead = ["#", "عکس", "نام ", " امتیاز ", "دسته بندی  ", "قیمت اصلی", "قیمت جدید", " تخفیف", "فروش", "بازدید ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 18
      }
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11,
        columnNumber: 20
      }
    });

    let dataIndex = data[index].discount;
    let thumbnail = dataIndex.thumbnail ? dataIndex.thumbnail : NotEntered;
    let title = dataIndex.title ? dataIndex.title : NotEntered;
    let realPrice = dataIndex.realPrice ? Object(_formatMoney__WEBPACK_IMPORTED_MODULE_1__["default"])(dataIndex.realPrice) : NotEntered;
    let newPrice = dataIndex.newPrice ? Object(_formatMoney__WEBPACK_IMPORTED_MODULE_1__["default"])(dataIndex.newPrice) : NotEntered;
    let rating = dataIndex.rating ? dataIndex.rating : 0;
    let percent = dataIndex.percent ? "%" + dataIndex.percent : NotEntered;
    let boughtCount = dataIndex.boughtCount ? dataIndex.boughtCount : "0";
    let category = dataIndex.category ? dataIndex.category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : NotEntered;
    let viewCount = dataIndex.viewCount ? dataIndex.viewCount : "0";
    let isActive = dataIndex.isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, categoryTitleFa, realPrice, newPrice, percent, boughtCount, viewCount, isActive],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (transactionDiscount);

/***/ }),

/***/ "./panelAdmin/utils/formatMoney.js":
/*!*****************************************!*\
  !*** ./panelAdmin/utils/formatMoney.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const formatMoney = number => {
  return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

/* harmony default export */ __webpack_exports__["default"] = (formatMoney);

/***/ }),

/***/ "./panelAdmin/utils/handleKey.js":
/*!***************************************!*\
  !*** ./panelAdmin/utils/handleKey.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const handleKey = event => {
  const form = event.target.form;
  const index = Array.prototype.indexOf.call(form, event.target);
  let keyCode = event.keyCode;
  let numberAccepted = [8, 13];

  if (numberAccepted.includes(keyCode)) {
    if (keyCode === 13) {
      if (form.elements[index + 1]) return form.elements[index + 1].focus();
    } else if (keyCode === 8) if (form.elements[index].value) return form.elements[index].value.length - 1;else if (form.elements[index - 1]) form.elements[index - 1].focus();

    event.preventDefault();
  }
};

/* harmony default export */ __webpack_exports__["default"] = (handleKey);

/***/ }),

/***/ "./panelAdmin/utils/index.js":
/*!***********************************!*\
  !*** ./panelAdmin/utils/index.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _consts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./consts */ "./panelAdmin/utils/consts/index.js");
/* harmony import */ var _toastify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./toastify */ "./panelAdmin/utils/toastify.js");
/* harmony import */ var _CelanderConvert__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CelanderConvert */ "./panelAdmin/utils/CelanderConvert/index.js");
/* harmony import */ var _onChanges__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./onChanges */ "./panelAdmin/utils/onChanges/index.js");
/* harmony import */ var _handleKey__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./handleKey */ "./panelAdmin/utils/handleKey.js");
/* harmony import */ var _formatMoney__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./formatMoney */ "./panelAdmin/utils/formatMoney.js");






const utils = {
  consts: _consts__WEBPACK_IMPORTED_MODULE_0__["default"],
  toastify: _toastify__WEBPACK_IMPORTED_MODULE_1__["default"],
  CelanderConvert: _CelanderConvert__WEBPACK_IMPORTED_MODULE_2__["default"],
  onChanges: _onChanges__WEBPACK_IMPORTED_MODULE_3__["default"],
  handleKey: _handleKey__WEBPACK_IMPORTED_MODULE_4__["default"],
  formatMoney: _formatMoney__WEBPACK_IMPORTED_MODULE_5__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (utils);

/***/ }),

/***/ "./panelAdmin/utils/menuFormat.js":
/*!****************************************!*\
  !*** ./panelAdmin/utils/menuFormat.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! .. */ "./panelAdmin/index.js");
/* harmony import */ var _values_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../values/index */ "./panelAdmin/values/index.js");


const menuFormat = [{
  title: "عمومی",
  menus: [{
    route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_DASHBOARD,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.DASHBOARD,
    menuIconImg: false,
    menuIconClass: "fas fa-tachometer-slowest",
    subMenu: [// { title: "SubDashboard", route: "/dashboard1" },
      // { title: "SubDashboard", route: "/dashboard2" }
    ]
  }]
}, {
  title: "کاربردی",
  menus: [{
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ARTIST,
    menuIconImg: false,
    menuIconClass: "fas fa-user-music",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_ARTIST,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ARTIST
    }, {
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ADD_ARTIST,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ADD_ARTIST
    }]
  }, {
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ALBUM,
    menuIconImg: false,
    menuIconClass: "fas fa-album",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_ALBUM,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ALBUM
    }, {
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ADD_ALBUM,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ADD_ALBUM
    }]
  }, {
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.GENRES,
    menuIconImg: false,
    menuIconClass: "fas fa-album",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_GENRES,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_GENRE
    }, {
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ADD_GENRE,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ADD_GENRE
    }]
  }, {
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.COUNTRY,
    menuIconImg: false,
    menuIconClass: "fas fa-flag",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_COUNTRY,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_COUNTRY
    }, {
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ADD_COUNTRY,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ADD_COUNTRY
    }]
  }, {
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.INSTRUMENT,
    menuIconImg: false,
    menuIconClass: "fas fa-saxophone",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_INSTRUMENT,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_INSTRUMENT
    }, {
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ADD_INSTRUMENT,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ADD_INSTRUMENT
    }]
  }, {
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.GALLERY,
    menuIconImg: false,
    menuIconClass: "fad fa-images",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_GALLERIES,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_GALLERY
    }]
  }, {
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SONG,
    menuIconImg: false,
    menuIconClass: "fad fa-comment-music",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_SONG,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_SONG
    }]
  }, {
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.MOOD,
    menuIconImg: false,
    menuIconClass: "fas fa-album",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_MOOD,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_MOOD
    }, {
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ADD_MOOD,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ADD_MOOD
    }]
  }, {
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.HASHTAG,
    menuIconImg: false,
    menuIconClass: "fas fa-album",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_HASHTAG,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_HASHTAG
    }, {
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ADD_HASHTAG,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ADD_HASHTAG
    }]
  }]
}];
/* harmony default export */ __webpack_exports__["default"] = (menuFormat);

/***/ }),

/***/ "./panelAdmin/utils/onChanges/arrayOnchange.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/utils/onChanges/arrayOnchange.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const arrayOnchange = async props => {
  const {
    event: e,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity,
    updateObject,
    uploadChange
  } = props;
  console.log({
    validName
  });
  let changeValid,
      updatedForm,
      updatedFormElement = {};
  e.map(async event => {
    let value = data.Form[event.name].value;

    let update = _objectSpread({}, value);

    let eventValue = event.value;
    let typeofData = typeof value;
    let isArray, isObject, isString;
    typeofData === "object" && value != null ? value.length >= 0 ? isArray = true : isObject = true : isString = true;

    const remove = index => value.splice(index, 1)[0];

    const push = (val, newVal) => newVal != undefined ? val.push(newVal) : ""; // console.log({ eventValue, value });


    if (event.type === "file") {
      const uploadFile = await uploadChange(event, setLoading, imageType, setState);

      if (uploadFile) {
        if (isArray) value.includes(uploadFile) ? remove(value.findIndex(d => d === uploadFile)) : push(value, uploadFile);else value = uploadFile;
      } else return;
    } else if (isArray) value.includes(eventValue) ? remove(value.findIndex(d => d === eventValue)) : push(value, eventValue);else if (isObject) {
      value = eventValue;
      if (event.child) value = update[event.child] = eventValue;
    } else if (isString) value = eventValue;

    let checkValidValue;
    if (typeofData === "object") checkValidValue = eventValue;else checkValidValue = value;
    updatedFormElement[event.name] = updateObject(data.Form[event.name], {
      value: value,
      valid: typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray),
      touched: true
    });

    if (validName) {
      changeValid = updateObject(data.Form[validName], {
        valid: true
      });
      updatedForm = updateObject(data.Form, _objectSpread({}, updatedFormElement, {
        [validName]: changeValid
      }));
    } else updatedForm = updateObject(data.Form, _objectSpread({}, updatedFormElement));

    console.log({
      updatedFormElement,
      updatedForm
    });
    let formIsValid = true;

    for (let name in updatedForm) formIsValid = updatedForm[name].valid && formIsValid;

    return setData({
      Form: updatedForm,
      formIsValid: formIsValid
    });
  });
};

/* harmony default export */ __webpack_exports__["default"] = (arrayOnchange);

/***/ }),

/***/ "./panelAdmin/utils/onChanges/globalChange.js":
/*!****************************************************!*\
  !*** ./panelAdmin/utils/onChanges/globalChange.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _checkValidity__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../checkValidity */ "./panelAdmin/utils/checkValidity.js");
/* harmony import */ var _updateObject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../updateObject */ "./panelAdmin/utils/updateObject.js");
/* harmony import */ var _uploadChange__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./uploadChange */ "./panelAdmin/utils/onChanges/uploadChange.js");
/* harmony import */ var _handelOnchange__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./handelOnchange */ "./panelAdmin/utils/onChanges/handelOnchange.js");
/* harmony import */ var _arrayOnchange__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./arrayOnchange */ "./panelAdmin/utils/onChanges/arrayOnchange.js");






const globalChange = async props => {
  const {
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    fileName,
    dispatch
  } = props;
  let typeCheck = typeof event; // console.log(typeCheck === "object" && event.length > 0);

  if (typeCheck === "object" && event.length > 0) return Object(_arrayOnchange__WEBPACK_IMPORTED_MODULE_4__["default"])({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity: _checkValidity__WEBPACK_IMPORTED_MODULE_0__["checkValidity"],
    updateObject: _updateObject__WEBPACK_IMPORTED_MODULE_1__["default"],
    uploadChange: _uploadChange__WEBPACK_IMPORTED_MODULE_2__["default"],
    fileName,
    dispatch
  });else if (typeCheck === "object") return Object(_handelOnchange__WEBPACK_IMPORTED_MODULE_3__["default"])({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity: _checkValidity__WEBPACK_IMPORTED_MODULE_0__["checkValidity"],
    updateObject: _updateObject__WEBPACK_IMPORTED_MODULE_1__["default"],
    uploadChange: _uploadChange__WEBPACK_IMPORTED_MODULE_2__["default"],
    fileName,
    dispatch
  });
};

/* harmony default export */ __webpack_exports__["default"] = (globalChange);

/***/ }),

/***/ "./panelAdmin/utils/onChanges/handelOnchange.js":
/*!******************************************************!*\
  !*** ./panelAdmin/utils/onChanges/handelOnchange.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const handelOnchange = async ({
  event,
  data,
  setData,
  setState,
  setLoading,
  imageType,
  validName,
  checkValidity,
  updateObject,
  uploadChange,
  fileName
}) => {
  let changeValid,
      updatedForm,
      updatedFormElement = {};
  let formIsValid = true;
  let value = data.Form[event.name].value;

  let update = _objectSpread({}, value);

  let eventValue = event.value;
  let typeofData = typeof value;
  let isArray, isObject, isString;
  console.log({
    length: value.length
  });
  typeofData === "object" && value != undefined ? value && value.length >= 0 ? isArray = true : isObject = true : isString = true;
  console.log({
    typeofData,
    value,
    isArray,
    isObject,
    isString
  });

  const remove = index => value.splice(index, 1)[0];

  const push = (val, newVal) => newVal != undefined ? val.push(newVal) : "";

  console.log({
    eventValue,
    value
  });

  if (event.type === "file") {
    const uploadFile = await uploadChange({
      event,
      setLoading,
      imageType,
      setState,
      valid: data.Form[event.name].kindOf,
      fileName
    });

    if (uploadFile) {
      if (isArray) value.includes(uploadFile) ? remove(value.findIndex(d => d === uploadFile)) : push(value, uploadFile);else value = uploadFile;
    } else return;
  } else if (isArray) value.includes(eventValue) ? remove(value.findIndex(d => d === eventValue)) : push(value, eventValue);else if (isObject) {
    // value = eventValue;
    console.log({
      event
    });

    if (event.child) {
      console.log({
        eventValue,
        value
      });
      value[event.child] = eventValue;
    } else {
      value = eventValue;
    }
  } else if (isString) value = eventValue;

  let checkValidValue;
  if (typeofData === "object") checkValidValue = eventValue;else checkValidValue = value;
  updatedFormElement[event.name] = updateObject(data.Form[event.name], {
    value: value,
    valid: typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray),
    touched: true
  });

  if (validName) {
    changeValid = updateObject(data.Form[validName], {
      valid: true
    });
    updatedForm = updateObject(data.Form, _objectSpread({}, updatedFormElement, {
      [validName]: changeValid
    }));
  } else updatedForm = updateObject(data.Form, _objectSpread({}, updatedFormElement));

  console.log({
    updatedFormElement,
    updatedForm
  });

  for (let name in updatedForm) formIsValid = updatedForm[name].valid && formIsValid;

  return setData({
    Form: updatedForm,
    formIsValid: formIsValid
  });
};

/* harmony default export */ __webpack_exports__["default"] = (handelOnchange);

/***/ }),

/***/ "./panelAdmin/utils/onChanges/index.js":
/*!*********************************************!*\
  !*** ./panelAdmin/utils/onChanges/index.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalChange__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./globalChange */ "./panelAdmin/utils/onChanges/globalChange.js");
/* harmony import */ var _arrayOnchange__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./arrayOnchange */ "./panelAdmin/utils/onChanges/arrayOnchange.js");


const onChanges = {
  globalChange: _globalChange__WEBPACK_IMPORTED_MODULE_0__["default"],
  arrayOnchange: _arrayOnchange__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (onChanges);

/***/ }),

/***/ "./panelAdmin/utils/onChanges/uploadChange.js":
/*!****************************************************!*\
  !*** ./panelAdmin/utils/onChanges/uploadChange.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../api */ "./panelAdmin/api/index.js");
/* harmony import */ var _toastify__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../toastify */ "./panelAdmin/utils/toastify.js");
/* harmony import */ var _validUpload_voicevalid__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./validUpload/voicevalid */ "./panelAdmin/utils/onChanges/validUpload/voicevalid.js");
/* harmony import */ var _store_actions_saga__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../store/actions/saga */ "./store/actions/saga/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







const uploadChange = async props => {
  const {
    event,
    setLoading,
    imageType,
    setState,
    valid,
    fileName,
    dispatch
  } = props;
  console.log({
    eventUpload: event
  });
  let files = event.files[0];
  console.log({
    imageType,
    fileName
  });
  let returnData = false;

  if (files) {
    switch (valid) {
      case "image":
        if (files.type.includes("image")) {
          if (imageType && fileName) {
            if ( // dispatch(sagaActions.uploadImageData({ data: files, imageType, fileName }))
            await _api__WEBPACK_IMPORTED_MODULE_1__["post"].imageUpload(files, setLoading, imageType, setState, fileName)) returnData = fileName;
          } else {
            Object(_toastify__WEBPACK_IMPORTED_MODULE_2__["default"])("لطفا اطلاعات قبلی را پر کنید", "error");
          }
        }

        break;

      case "video":
        if (files.type.includes("video")) returnData = await _api__WEBPACK_IMPORTED_MODULE_1__["post"].videoUpload(files, setLoading, imageType, setState);
        break;

      case "voice":
        if (Object(_validUpload_voicevalid__WEBPACK_IMPORTED_MODULE_3__["default"])(files.type)) {
          if (fileName) {
            if (await _api__WEBPACK_IMPORTED_MODULE_1__["post"].voiceUpload(files, setLoading, imageType, setState, fileName)) returnData = fileName;
          } else {
            Object(_toastify__WEBPACK_IMPORTED_MODULE_2__["default"])("لطفا اطلاعات قبلی را پر کنید", "error");
          }
        }

        break;

      default:
        Object(_toastify__WEBPACK_IMPORTED_MODULE_2__["default"])("فایل شما نباید " + files.type + " باشد", "error");
        break;
    }
  }

  console.log({
    files: files,
    returnData,
    fileName
  });
  if (!returnData && files) Object(_toastify__WEBPACK_IMPORTED_MODULE_2__["default"])("فایل شما نباید " + files.type + " باشد", "error");
  setState(prev => _objectSpread({}, prev, {
    progressPercentImage: null,
    progressPercentVideo: null,
    progressPercentSongs: null
  }));
  return returnData;
};

/* harmony default export */ __webpack_exports__["default"] = (uploadChange);

/***/ }),

/***/ "./panelAdmin/utils/onChanges/validUpload/voicevalid.js":
/*!**************************************************************!*\
  !*** ./panelAdmin/utils/onChanges/validUpload/voicevalid.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
let formats = ["mp3", "voice", "audio"];

const voiceValid = type => {
  let valid = formats.map(format => {
    if (type.includes(format)) return true;else return false;
  });
  let finalValid = valid.includes(true) ? true : false;
  return finalValid;
};

/* harmony default export */ __webpack_exports__["default"] = (voiceValid);

/***/ }),

/***/ "./panelAdmin/utils/toastify.js":
/*!**************************************!*\
  !*** ./panelAdmin/utils/toastify.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-toastify */ "react-toastify");
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_toastify__WEBPACK_IMPORTED_MODULE_1__);



const toastify = async (text, type) => {
  react_toastify__WEBPACK_IMPORTED_MODULE_1__["toast"].configure();

  if (type === "error") {
    // alert("error");
    await react_toastify__WEBPACK_IMPORTED_MODULE_1__["toast"].error(text, {
      position: "top-right",
      autoClose: 5000
    });
  } else if (type === "success") {
    // alert("success");
    await react_toastify__WEBPACK_IMPORTED_MODULE_1__["toast"].success(text, {
      position: "top-right",
      autoClose: 5000
    });
  }
};

/* harmony default export */ __webpack_exports__["default"] = (toastify);

/***/ }),

/***/ "./panelAdmin/utils/updateObject.js":
/*!******************************************!*\
  !*** ./panelAdmin/utils/updateObject.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const updateObject = (oldObject, updatedProperties) => {
  return _objectSpread({}, oldObject, {}, updatedProperties);
};

/* harmony default export */ __webpack_exports__["default"] = (updateObject);

/***/ }),

/***/ "./panelAdmin/values/apiString.js":
/*!****************************************!*\
  !*** ./panelAdmin/values/apiString.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const ADMIN = "/admin";
const SONG_UPLOAD = ADMIN + "/songupload";
const UPLOAD = ADMIN + "/upload";
const ALBUM = ADMIN + "/album";
const LOGIN = ADMIN + "/login";
const IMAGE_UPLOAD = ADMIN + "/upload/image";
const GENRES = ADMIN + "/genre";
const ARTIST = ADMIN + "/artist";
const ARTIST_SEARCH = ARTIST + "/search";
const COUNTRY = ADMIN + "/country";
const INSTRUMENT = ADMIN + "/instrument";
const GALLERY = ADMIN + "/gallery";
const SONG = ADMIN + "/songlibrary";
const MOOD = ADMIN + "/mood";
const HASHTAG = ADMIN + "/hashtag";
const apiString = {
  SONG_UPLOAD,
  ALBUM,
  LOGIN,
  UPLOAD,
  IMAGE_UPLOAD,
  GENRES,
  ARTIST_SEARCH,
  COUNTRY,
  INSTRUMENT,
  GALLERY,
  SONG,
  ARTIST,
  MOOD,
  HASHTAG
};
/* harmony default export */ __webpack_exports__["default"] = (apiString);

/***/ }),

/***/ "./panelAdmin/values/index.js":
/*!************************************!*\
  !*** ./panelAdmin/values/index.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _strings__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./strings */ "./panelAdmin/values/strings/index.js");
/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./routes */ "./panelAdmin/values/routes/index.js");
/* harmony import */ var _apiString__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./apiString */ "./panelAdmin/values/apiString.js");
/* harmony import */ var _strings_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./strings/constants */ "./panelAdmin/values/strings/constants.js");
/* harmony import */ var _strings_fa__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./strings/fa */ "./panelAdmin/values/strings/fa/index.js");
/* harmony import */ var _strings_en__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./strings/en */ "./panelAdmin/values/strings/en/index.js");






const values = {
  routes: _routes__WEBPACK_IMPORTED_MODULE_1__["default"],
  strings: _strings__WEBPACK_IMPORTED_MODULE_0__["default"],
  apiString: _apiString__WEBPACK_IMPORTED_MODULE_2__["default"],
  constants: _strings_constants__WEBPACK_IMPORTED_MODULE_3__["default"],
  fa: _strings_fa__WEBPACK_IMPORTED_MODULE_4__["default"],
  en: _strings_en__WEBPACK_IMPORTED_MODULE_5__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (values);

/***/ }),

/***/ "./panelAdmin/values/routes/index.js":
/*!*******************************************!*\
  !*** ./panelAdmin/values/routes/index.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const GS_PANEL_ADMIN_TITLE = "/panelAdmin";
const GS_ADMIN_DASHBOARD = GS_PANEL_ADMIN_TITLE + "/dashboard";
const GS_ADMIN_ARTIST = GS_PANEL_ADMIN_TITLE + "/artist";
const GS_ADMIN_ADD_ARTIST = GS_PANEL_ADMIN_TITLE + "/addArtist";
const GS_ADMIN_ALBUM = GS_PANEL_ADMIN_TITLE + "/album";
const GS_ADMIN_ADD_ALBUM = GS_PANEL_ADMIN_TITLE + "/addAlbum";
const GS_ADMIN_GENRE = GS_PANEL_ADMIN_TITLE + "/genre";
const GS_ADMIN_ADD_GENRE = GS_PANEL_ADMIN_TITLE + "/addGenre";
const GS_ADMIN_COUNTRY = GS_PANEL_ADMIN_TITLE + "/country";
const GS_ADMIN_ADD_COUNTRY = GS_PANEL_ADMIN_TITLE + "/addCountry";
const GS_ADMIN_INSTRUMENT = GS_PANEL_ADMIN_TITLE + "/instrument";
const GS_ADMIN_ADD_INSTRUMENT = GS_PANEL_ADMIN_TITLE + "/addInstrument";
const GS_ADMIN_GALLERY = GS_PANEL_ADMIN_TITLE + "/gallery";
const GS_ADMIN_SONG = GS_PANEL_ADMIN_TITLE + "/song";
const GS_ADMIN_MOOD = GS_PANEL_ADMIN_TITLE + "/mood";
const GS_ADMIN_ADD_MOOD = GS_PANEL_ADMIN_TITLE + "/addMood";
const GS_ADMIN_HASHTAG = GS_PANEL_ADMIN_TITLE + "/hashtag";
const GS_ADMIN_ADD_HASHTAG = GS_PANEL_ADMIN_TITLE + "/addHashtag"; // const GS_ADMIN_GALLERY = GS_PANEL_ADMIN_TITLE + "/gallery";
// const GS_ADMIN_ADD_GALLERY = GS_PANEL_ADMIN_TITLE + "/addGallery";

const routes = {
  GS_ADMIN_DASHBOARD,
  GS_PANEL_ADMIN_TITLE,
  GS_ADMIN_ARTIST,
  GS_ADMIN_ADD_ARTIST,
  GS_ADMIN_ALBUM,
  GS_ADMIN_ADD_ALBUM,
  GS_ADMIN_GENRE,
  GS_ADMIN_ADD_GENRE,
  GS_ADMIN_COUNTRY,
  GS_ADMIN_ADD_COUNTRY,
  GS_ADMIN_INSTRUMENT,
  GS_ADMIN_ADD_INSTRUMENT,
  GS_ADMIN_GALLERY,
  GS_ADMIN_SONG,
  GS_ADMIN_MOOD,
  GS_ADMIN_ADD_MOOD,
  GS_ADMIN_HASHTAG,
  GS_ADMIN_ADD_HASHTAG
};
/* harmony default export */ __webpack_exports__["default"] = (routes);

/***/ }),

/***/ "./panelAdmin/values/strings/constants.js":
/*!************************************************!*\
  !*** ./panelAdmin/values/strings/constants.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const ALBUM_CONSTANTS = "album";
const PLAY_LIST_CONSTANTS = "playlist";
const SONG_CONSTANTS = "song";
const FLAG_CONSTANTS = "flag";
const ARTIST_CONSTANTS = "artist";
const INSTRUMENT_CONSTANTS = "instrument";
const MUSIC_VIDEO_CONSTANTS = "musicvideo";
const MOOD_CONSTANTS = "mood";
const constants = {
  ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS,
  FLAG_CONSTANTS,
  ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS
};
/* harmony default export */ __webpack_exports__["default"] = (constants);

/***/ }),

/***/ "./panelAdmin/values/strings/en/constants.js":
/*!***************************************************!*\
  !*** ./panelAdmin/values/strings/en/constants.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const ALBUM_CONSTANTS = "album";
const PLAY_LIST_CONSTANTS = "playlist";
const SONG_CONSTANTS = "song";
const FLAG_CONSTANTS = "flag";
const ARTIST_CONSTANTS = "artist";
const INSTRUMENT_CONSTANTS = "instrument";
const MUSIC_VIDEO_CONSTANTS = "musicvideo";
const MOOD_CONSTANTS = "mood";
const constants = {
  ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS,
  FLAG_CONSTANTS,
  ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS
};
/* harmony default export */ __webpack_exports__["default"] = (constants);

/***/ }),

/***/ "./panelAdmin/values/strings/en/global.js":
/*!************************************************!*\
  !*** ./panelAdmin/values/strings/en/global.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const TRACKS = "Tracks";
const NO_ENTRIES = "no entries";
const FOLLOWERS = "followers";
const global = {
  TRACKS,
  NO_ENTRIES: NO_ENTRIES,
  FOLLOWERS
};
/* harmony default export */ __webpack_exports__["default"] = (global);

/***/ }),

/***/ "./panelAdmin/values/strings/en/index.js":
/*!***********************************************!*\
  !*** ./panelAdmin/values/strings/en/index.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navbar_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navbar.js */ "./panelAdmin/values/strings/en/navbar.js");
/* harmony import */ var _sideMenu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sideMenu */ "./panelAdmin/values/strings/en/sideMenu.js");
/* harmony import */ var _global_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./global.js */ "./panelAdmin/values/strings/en/global.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./constants */ "./panelAdmin/values/strings/en/constants.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const en = _objectSpread({}, _navbar_js__WEBPACK_IMPORTED_MODULE_0__["default"], {}, _sideMenu__WEBPACK_IMPORTED_MODULE_1__["default"], {}, _global_js__WEBPACK_IMPORTED_MODULE_2__["default"], {}, _constants__WEBPACK_IMPORTED_MODULE_3__["default"]);

/* harmony default export */ __webpack_exports__["default"] = (en);

/***/ }),

/***/ "./panelAdmin/values/strings/en/navbar.js":
/*!************************************************!*\
  !*** ./panelAdmin/values/strings/en/navbar.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const APP_NAME = "Rimtal";
const EXPLORE = "Explore";
const TRACKS = "Tracks";
const PLAYLISTS = "Playlists";
const ALBUMS = "Albums";
const ARTISTS = "Artists";
const VIDEOS = "Videos";
const SIGN_IN = "Sign In";
const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN
};
/* harmony default export */ __webpack_exports__["default"] = (navbar);

/***/ }),

/***/ "./panelAdmin/values/strings/en/sideMenu.js":
/*!**************************************************!*\
  !*** ./panelAdmin/values/strings/en/sideMenu.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const DASHBOARD = "داشبورد";
const SIDEBAR_ONE_TITLE = "عمومی";
const SIDEBAR_TWO_TITLE = "کاربردی";
const SETTING_WEB = "تنظیمات سایت";
const CATEGORIES = "دسته بندی ها";
const SEE_ARTIST = "مشاهده هنرمندان";
const ARTIST = "هنرمند";
const ADD_ARTIST = "افزودن هنرمند";
const SEE_ALBUM = "مشاهده آلبوم ها";
const ALBUM = "آلبوم";
const ADD_ALBUM = "افزودن آلبوم";
const SEE_GENRES = "مشاهده ژانر ها";
const GENRES = "ژانر";
const ADD_GENRE = "افزودن ژانر";
const SEE_COUNTRY = "مشاهده کشور ها";
const COUNTRY = "کشور";
const ADD_COUNTRY = "افزودن کشور";
const SEE_INSTRUMENT = "مشاهده ساز ها";
const INSTRUMENT = "ساز";
const ADD_INSTRUMENT = "افزودن ساز";
const SEE_GALLERY = "مشاهده گالری ها";
const GALLERY = "گالری";
const SEE_SONG = "مشاهده آهنگ ها";
const SONG = "آهنگ";
const SEE_MOOD = "مشاهده حالت ها";
const MOOD = "حالت";
const sideMenu = {
  SEE_GALLERY,
  GALLERY,
  DASHBOARD,
  SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE,
  SETTING_WEB,
  CATEGORIES,
  SEE_ARTIST,
  ARTIST,
  ADD_ARTIST,
  ALBUM,
  SEE_ALBUM,
  ADD_ALBUM,
  SEE_GENRES,
  GENRES,
  ADD_GENRE,
  SEE_COUNTRY,
  COUNTRY,
  ADD_COUNTRY,
  SEE_INSTRUMENT,
  INSTRUMENT,
  ADD_INSTRUMENT,
  SEE_SONG,
  SONG,
  SEE_MOOD,
  MOOD
};
/* harmony default export */ __webpack_exports__["default"] = (sideMenu);

/***/ }),

/***/ "./panelAdmin/values/strings/fa/constants.js":
/*!***************************************************!*\
  !*** ./panelAdmin/values/strings/fa/constants.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const ALBUM_CONSTANTS = "آلبوم";
const PLAY_LIST_CONSTANTS = "لیست پخش";
const SONG_CONSTANTS = "موزیک";
const FLAG_CONSTANTS = "پرچم";
const ARTIST_CONSTANTS = "هنرمند";
const INSTRUMENT_CONSTANTS = "ساز";
const MUSIC_VIDEO_CONSTANTS = "موزیک ویدئو";
const MOOD_CONSTANTS = "حالت";
const constants = {
  ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS,
  FLAG_CONSTANTS,
  ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS
};
/* harmony default export */ __webpack_exports__["default"] = (constants);

/***/ }),

/***/ "./panelAdmin/values/strings/fa/global.js":
/*!************************************************!*\
  !*** ./panelAdmin/values/strings/fa/global.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const TRACKS = "آهنگ ها ";
const NO_ENTRIES = "وارد نشده";
const FOLLOWERS = "دنبال کنندگان";
const global = {
  TRACKS,
  NO_ENTRIES: NO_ENTRIES,
  FOLLOWERS
};
/* harmony default export */ __webpack_exports__["default"] = (global);

/***/ }),

/***/ "./panelAdmin/values/strings/fa/index.js":
/*!***********************************************!*\
  !*** ./panelAdmin/values/strings/fa/index.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navbar_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navbar.js */ "./panelAdmin/values/strings/fa/navbar.js");
/* harmony import */ var _sideMenu_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sideMenu.js */ "./panelAdmin/values/strings/fa/sideMenu.js");
/* harmony import */ var _global_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./global.js */ "./panelAdmin/values/strings/fa/global.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./constants */ "./panelAdmin/values/strings/fa/constants.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const fa = _objectSpread({}, _navbar_js__WEBPACK_IMPORTED_MODULE_0__["default"], {}, _sideMenu_js__WEBPACK_IMPORTED_MODULE_1__["default"], {}, _global_js__WEBPACK_IMPORTED_MODULE_2__["default"], {}, _constants__WEBPACK_IMPORTED_MODULE_3__["default"]);

/* harmony default export */ __webpack_exports__["default"] = (fa);

/***/ }),

/***/ "./panelAdmin/values/strings/fa/navbar.js":
/*!************************************************!*\
  !*** ./panelAdmin/values/strings/fa/navbar.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const APP_NAME = "ریمتال";
const EXPLORE = "کاوش کردن";
const TRACKS = "آهنگ ها";
const PLAYLISTS = "لیست های پخش";
const ALBUMS = "آلبوم ها";
const ARTISTS = "هنرمندان";
const VIDEOS = "ویدیو ها";
const SIGN_IN = "ورود";
const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN
};
/* harmony default export */ __webpack_exports__["default"] = (navbar);

/***/ }),

/***/ "./panelAdmin/values/strings/fa/sideMenu.js":
/*!**************************************************!*\
  !*** ./panelAdmin/values/strings/fa/sideMenu.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const DASHBOARD = "داشبورد";
const SIDEBAR_ONE_TITLE = "عمومی";
const SIDEBAR_TWO_TITLE = "کاربردی";
const SETTING_WEB = "تنظیمات سایت";
const CATEGORIES = "دسته بندی ها";
const SEE_ARTIST = "مشاهده هنرمندان";
const ARTIST = "هنرمند";
const ADD_ARTIST = "افزودن هنرمند";
const SEE_ALBUM = "مشاهده آلبوم ها";
const ALBUM = "آلبوم";
const ADD_ALBUM = "افزودن آلبوم";
const SEE_GENRES = "مشاهده ژانر ها";
const GENRES = "ژانر";
const ADD_GENRE = "افزودن ژانر";
const SEE_COUNTRY = "مشاهده کشور ها";
const COUNTRY = "کشور";
const ADD_COUNTRY = "افزودن کشور";
const SEE_INSTRUMENT = "مشاهده ساز ها";
const INSTRUMENT = "ساز";
const ADD_INSTRUMENT = "افزودن ساز";
const SEE_GALLERIES = "مشاهده گالری ها";
const GALLERY = "گالری";
const SEE_SONG = "مشاهده آهنگ ها";
const SONG = "آهنگ";
const SEE_MOOD = "مشاهده حالت ها";
const MOOD = "حالت";
const ADD_MOOD = "افزودن حالت";
const SEE_HASHTAG = "مشاهده هشتگ ها";
const HASHTAG = "هشتگ";
const ADD_HASHTAG = "افزودن هشتگ";
const sideMenu = {
  SEE_GALLERIES,
  GALLERY,
  DASHBOARD,
  SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE,
  SETTING_WEB,
  CATEGORIES,
  SEE_ARTIST,
  ARTIST,
  ADD_ARTIST,
  ALBUM,
  SEE_ALBUM,
  ADD_ALBUM,
  SEE_GENRES,
  GENRES,
  ADD_GENRE,
  SEE_COUNTRY,
  COUNTRY,
  ADD_COUNTRY,
  SEE_INSTRUMENT,
  INSTRUMENT,
  ADD_INSTRUMENT,
  SEE_SONG,
  SONG,
  SEE_MOOD,
  MOOD,
  ADD_MOOD,
  SEE_HASHTAG,
  HASHTAG,
  ADD_HASHTAG
};
/* harmony default export */ __webpack_exports__["default"] = (sideMenu);

/***/ }),

/***/ "./panelAdmin/values/strings/index.js":
/*!********************************************!*\
  !*** ./panelAdmin/values/strings/index.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _en__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./en */ "./panelAdmin/values/strings/en/index.js");
/* harmony import */ var _fa__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fa */ "./panelAdmin/values/strings/fa/index.js");


const Lang = "rtl";
let strings;
if (Lang === "rtl") strings = _fa__WEBPACK_IMPORTED_MODULE_1__["default"];else strings = _en__WEBPACK_IMPORTED_MODULE_0__["default"];
/* harmony default export */ __webpack_exports__["default"] = (strings);

/***/ }),

/***/ "./public/assets/images/Rimtal.png":
/*!*****************************************!*\
  !*** ./public/assets/images/Rimtal.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAXCAYAAADHqJcNAAAGwUlEQVRogb2aeahVRRzHP/pepZlttlFR2WpiWUm9tL2o1GwjbKesBNMwzIoMaTda7I8kWohKIgiL9sUWyHYqLZdS20XL1MQ2l3pu78UPvhM/pjnnnnPufX7gcu/MnTNnzpzf/LaZToz5g4i9gBOBXsCW8Z9iPbAY+Aj4LKON50zgGuAd4C5gQ0a7PsBoYD4wqUC/RdkKuAPYBrhZYw/YMw4DdmrQvX4CntIcpWgGTgUGAUcAPYF5wHTgFc1pWYZpfp8B7gbaynbgBWEX4FbgAmDrEn28C9wOvJfxv03+t8DOKh8LfJho1wR8Ahyu8nnAsyXGkcd9wLX6f4qeMfAQMLJB9wnYfNySqB+s+iNyrn0TuAH4suC9TIAXAN1UPgSYU3bAnfW9LzANGFFSCIwTgLeASzP+7+4GaWyf0a5ZwhgwoexSciwprM/hrn6PqM2BDbhHzAGJuiuBV2sIgTFQi+r4gvey97WFK/eoMuBmvaiXExNiq3hl4pp2YHNgbyc0Vn4cWJTQDNZ+oytnqa32yGTYeIZKzdbDSGmlQGyWbPXdnzOB7RLeHVU2lb9Q9Sl+kUbwmBl4OKoz8/QpsALYDTga2E7/2ffzQH/guxrP3qb5bXbl0tjFY4He7kLTDOOBmcC6jA47aWWZlI9Tnan2B6Xa/64ymATjNSFV+zO1OSoxdo/Z5gHRqvLYC78amKi6pUALsCbRl7E2Km+reQm0yTyYSfrd1e8J3ARcobIJ3z3A2TWeMRbI1Jhq0lkvM2CO3xBJapYQhJvb6r8RuM3V95apqEr8UKZiL6+jP3OidijYdm3GZ11iLlpVl7om5lw5hAFbOBMiIUDzaSbsSVd3hmx+LbK0U2E6R3bZXuo/Jfu4E/jBlU+uYzypBxob+RhF2UYruRHEq6ypRJ+D3O9ZclzzGOdMcmc5zWWorBECFj7MqNDH+iiE7JnTtgo9I61VlBGyvTFVJsoLaJnrzdnt68ovFFi9y4D3XXlIDcGrWxsQCUJrTnxfC69FNq/YR94DjY0cvlp064CQsAr7RFHKzIJ9POd+9yoQ2VQV1P/wgtCpaifRdQ2RUOADl5TZVSu8KJcoMYaEe0nGWItS1SE71K1m8ym+L3jd2848mEN/Wk7b9kYLQj3kOZZl8A80RZnIwPUuKZVHF5c8Qkmq1xo0vrL4nMFCfYqwTA574OycF9yQqKG5QJt9ldzYXdKdWvH9q9w8ge+7Vd71QJXN+79KKeI8hkolB+5RNjOwKX2Eg9zv2Tlp5xQmvKeovp9MxNflhl2cPEHoosTIqJJeexmPOg9LdH0srXCS2pndfyRS9Z4tXF4DTdzrkede1fyVxfY39nPXTC95vY37Xr2HZoXlWYLQYabBOntM6rhs6JbKRlYhjG2Cu9a0wqNA14z+JkbJsbBx1SgTGCgy2fvLtwmUjcgWyKwFshJLsY9QiSyNYLHrRa7cpoGtrzEJy7XL10gsZf0ScJb6PE2bVvcqbN0gFTxGu3qBOS45U/eKiSjSx2Gu3coCqeIUb7gE3VHKPi6K2sWCUMTc/4/4otChT8sulWDMiPYMUpSxgTF53u9oZdhCJNBPW65rJKTdo75WKUvXmrjPpvIRWtzvhVokZXlL2/ZN0oKDE3sWfwJ/uTkwjTi17I1ilblBpsDHrZO0Alv1ovM+9ZI14YulCeLt1W4JIVgsNfq5q2tUSFuUTtIIgdkVN4Pm6trA6Yk2qyNtc3EVPy0WhHY5XF5T/Fy20w7CDqsco42ZHxO3WKJdxJYo7IzZFBphV/kIgSKHd1K0ySwGjnS7oB6fgOpbILL6H7EgNEnNrHB1w3N25jqS1ISvkvN4sFbcEG3MtOh00zUZEUW9GqFsrH6AoobAF3Xc+xVnkrfL2MuxrfpvXNkE4YmMcxFJYh+hSTc1KbxOdSco923x+It1PFAj+VsbOLMK9rmpTUMf9/tXne2oynyp/mCuzwGejvparRzLVLdoLwPOl4CsdsIbf5spHZ/lYU7UDcMGUos2TN7QyaGyMXFRGu3ddwRFxuUzivPk0FVlg3IKQRCOk2aID5tO0xG8yW5fpqvS3LVY5U2DF4rlOnA6N+pgkOzd5Ch7l4dN3Gbu/7yJ9GNoVGKKyARultMuCz/mWuFZcyQIpc8PJvCauIeEIavdAGn0MhuIy5vdAYkV0Yr8Ssenhiuc3Fb19oIulOPygGxRKkwLbJRghWNtqcMb6N4r3JnGRp1yQnF8eM74QEgRbHc1XPdbjQssFW/RTGjfCO1pEZD1Y+l+w06ZeyfSY6bEoibzo+wdWe7BNIMPz/33EuCJfwFWG3TGQDr4PQAAAABJRU5ErkJggg=="

/***/ }),

/***/ "./public/styles/index.scss":
/*!**********************************!*\
  !*** ./public/styles/index.scss ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./store/actionTypes/redux/index.js":
/*!******************************************!*\
  !*** ./store/actionTypes/redux/index.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const atRedux = {
  //======================================================== Redux
  SET_HOME_DATA: "SET_HOME_DATA_REDUX",
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX" // // ======================================================== UPLOAD
  // SET_UPLOAD_IMAGE: "SET_UPLOAD_IMAGE_REDUX",
  // // ======================================================== GALLERY
  // SET_GALLERY_DATA: "SET_GALLERY_DATA_REDUX",
  // CHANGE_ADD_GALLERY_DATA: "CHANGE_ADD_GALLERY_DATA_REDUX",
  // // ======================================================== ARTIST
  // SET_ARTIST_DATA: "SET_GALLERY_DATA_REDUX",
  // START_ARTIST_DATA: "START_GALLERY_DATA_REDUX",
  // // ======================= SEARCH ARTIST
  // SET_SEARCH_ARTIST_DATA: "SET_SEARCH_ARTIST_DATA_REDUX",
  // START_SEARCH_ARTIST_DATA: "START_SEARCH_ARTIST_DATA_REDUX",
  // // ======================================================== END ARTIST
  // // ======================================================== NAVBAR
  // SET_PAGE_NAME: "SET_PAGE_NAME",

};
/* harmony default export */ __webpack_exports__["default"] = (atRedux);

/***/ }),

/***/ "./store/actionTypes/saga/index.js":
/*!*****************************************!*\
  !*** ./store/actionTypes/saga/index.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const atSaga = {
  GET_HOME_SCREEN_DATA: "GET_HOME_SCREEN_DATA_SAGA" // POST_UPLOAD_IMAGE: "POST_UPLOAD_IMAGE_SAGA",
  // GET_GALLERY_DATA: "GET_GALLERY_DATA_SAGA",
  // GET_ARTIST_DATA: "GET_ARTIST_DATA_SAGA",
  // GET_SEARCH_ARTIST_DATA: "GET_SEARCH_ARTIST_DATA_SAGA",

};
/* harmony default export */ __webpack_exports__["default"] = (atSaga);

/***/ }),

/***/ "./store/actions/index.js":
/*!********************************!*\
  !*** ./store/actions/index.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./redux */ "./store/actions/redux/index.js");
/* harmony import */ var _saga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./saga */ "./store/actions/saga/index.js");
/* harmony import */ var _panelAdmin__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../panelAdmin */ "./panelAdmin/index.js");



const actions = {
  reduxActions: _redux__WEBPACK_IMPORTED_MODULE_0__,
  sagaActions: _saga__WEBPACK_IMPORTED_MODULE_1__
};
/* harmony default export */ __webpack_exports__["default"] = (actions);

/***/ }),

/***/ "./store/actions/redux/index.js":
/*!**************************************!*\
  !*** ./store/actions/redux/index.js ***!
  \**************************************/
/*! exports provided: increment, setHomeData, setFailure */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "increment", function() { return increment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setHomeData", function() { return setHomeData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setFailure", function() { return setFailure; });
/* harmony import */ var _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../actionTypes/redux */ "./store/actionTypes/redux/index.js");

function increment() {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].INCREMENT
  };
} // // =================================================== NAVBAR
// export function setPageName(data) {
//   return { type: atRedux.SET_PAGE_NAME, data };
// }
// =================================================== HOME

function setHomeData(data) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_HOME_DATA,
    data
  };
} // =================================================== ERROR

function setFailure(error) {
  return {
    type: _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].ADD_FAILURE,
    error
  };
} // // =================================================== UPLOAD
// export function setUploadImage(data) {
//   return { type: atRedux.SET_UPLOAD_IMAGE, data };
// }
// // =================================================== GALLERY
// export function setGalleryData(data) {
//   return { type: atRedux.SET_GALLERY_DATA, data };
// }
// export function changeAddGalleryData(data) {
//   return { type: atRedux.CHANGE_ADD_GALLERY_DATA, data };
// }
// // =================================================== END GALLERY
// // =================================================== ARTIST
// export function setArtistData(data) {
//   return { type: atRedux.SET_ARTIST_DATA, data };
// }
// export function setSearchArtistData(data) {
//   return { type: atRedux.SET_SEARCH_ARTIST_DATA, data };
// }
// export function startGetArtist(data) {
//   return { type: atRedux.START_ARTIST_DATA, data };
// }
// export function startSearchArtist(data) {
//   return { type: atRedux.START_SEARCH_ARTIST_DATA, data };
// }
// // ================================================= END ARTIST

/***/ }),

/***/ "./store/actions/saga/index.js":
/*!*************************************!*\
  !*** ./store/actions/saga/index.js ***!
  \*************************************/
/*! exports provided: getHomeData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getHomeData", function() { return getHomeData; });
/* harmony import */ var _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../actionTypes/saga */ "./store/actionTypes/saga/index.js");

function getHomeData() {
  console.log("getHomeData");
  return {
    type: _actionTypes_saga__WEBPACK_IMPORTED_MODULE_0__["default"].GET_HOME_SCREEN_DATA
  };
} // export function uploadImageData(data) {
//   return { type: atSaga.POST_UPLOAD_IMAGE, data: data };
// }
// export function getGalleryData(data) {
//   return { type: atSaga.GET_GALLERY_DATA, data: data };
// }
// export function getArtistData({ page }) {
//   return { type: atSaga.GET_ARTIST_DATA, page };
// }
// export function getSearchArtistData({ page }) {
//   return { type: atSaga.GET_SEARCH_ARTIST_DATA, page };
// }

/***/ }),

/***/ "./store/index.js":
/*!************************!*\
  !*** ./store/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga */ "redux-saga");
/* harmony import */ var redux_saga__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _reducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./reducer */ "./store/reducer/index.js");
/* harmony import */ var _saga__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./saga */ "./store/saga/index.js");
/* harmony import */ var _panelAdmin__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../panelAdmin */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







const allWatchers = _objectSpread({}, _saga__WEBPACK_IMPORTED_MODULE_3__, {}, _panelAdmin__WEBPACK_IMPORTED_MODULE_4__["default"].saga);

console.log({
  allWatchers
});

const bindMiddleware = middleware => {
  if (true) {
    const {
      composeWithDevTools
    } = __webpack_require__(/*! redux-devtools-extension */ "redux-devtools-extension");

    return composeWithDevTools(Object(redux__WEBPACK_IMPORTED_MODULE_0__["applyMiddleware"])(...middleware));
  }

  return Object(redux__WEBPACK_IMPORTED_MODULE_0__["applyMiddleware"])(...middleware);
}; //for redux devTools


function configureStore() {
  const sagaMiddleware = redux_saga__WEBPACK_IMPORTED_MODULE_1___default()();
  const store = Object(redux__WEBPACK_IMPORTED_MODULE_0__["createStore"])(_reducer__WEBPACK_IMPORTED_MODULE_2__["default"], bindMiddleware([sagaMiddleware]));

  for (let watcher in allWatchers) {
    store.sagaTask = sagaMiddleware.run(allWatchers[watcher]);
  }

  return store;
}

/* harmony default export */ __webpack_exports__["default"] = (configureStore);

/***/ }),

/***/ "./store/reducer/errorReducer.js":
/*!***************************************!*\
  !*** ./store/reducer/errorReducer.js ***!
  \***************************************/
/*! exports provided: errorInitialState, addFailure, removeFailure, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "errorInitialState", function() { return errorInitialState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addFailure", function() { return addFailure; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeFailure", function() { return removeFailure; });
/* harmony import */ var _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actionTypes/redux */ "./store/actionTypes/redux/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const errorInitialState = {
  error: null
};
const addFailure = (state, action) => {
  return _objectSpread({}, state, {
    error: action.error
  });
};
const removeFailure = state => {
  return _objectSpread({}, state, {
    error: null
  });
};

function errorReducer(state = errorInitialState, action) {
  switch (action.type) {
    case _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].ADD_FAILURE:
      return addFailure(state, action);

    case _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].REMOVE_FAILURE:
      return removeFailure(state);

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (errorReducer);

/***/ }),

/***/ "./store/reducer/index.js":
/*!********************************!*\
  !*** ./store/reducer/index.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _errorReducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./errorReducer */ "./store/reducer/errorReducer.js");
/* harmony import */ var _themeColorReducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./themeColorReducer */ "./store/reducer/themeColorReducer.js");
/* harmony import */ var _panelAdmin__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../panelAdmin */ "./panelAdmin/index.js");
/* harmony import */ var _website__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../website */ "./website/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const rootReducer = Object(redux__WEBPACK_IMPORTED_MODULE_0__["combineReducers"])(_objectSpread({
  error: _errorReducer__WEBPACK_IMPORTED_MODULE_1__["default"],
  themeColor: _themeColorReducer__WEBPACK_IMPORTED_MODULE_2__["default"]
}, _panelAdmin__WEBPACK_IMPORTED_MODULE_3__["default"].reducer, {}, _website__WEBPACK_IMPORTED_MODULE_4__["default"].reducer));
/* harmony default export */ __webpack_exports__["default"] = (rootReducer);

/***/ }),

/***/ "./store/reducer/themeColorReducer.js":
/*!********************************************!*\
  !*** ./store/reducer/themeColorReducer.js ***!
  \********************************************/
/*! exports provided: webThemeColorInitialstate, setThemeColor, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "webThemeColorInitialstate", function() { return webThemeColorInitialstate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setThemeColor", function() { return setThemeColor; });
/* harmony import */ var _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actionTypes/redux */ "./store/actionTypes/redux/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const webThemeColorInitialstate = {
  light: {
    primaryColor: "#0070ef",
    accentColor: "#666666",
    mainColor: "#ffff",
    titleColor: "#333333"
  },
  dark: {
    primaryColor: "#0070ef",
    accentColor: "#666666",
    mainColor: "#000",
    titleColor: "#ffff"
  },
  currentTheme: "light"
};
const setThemeColor = (state, action) => {
  return _objectSpread({}, state, {
    homeData: action.data
  });
};

function themeColorReducer(state = webThemeColorInitialstate, action) {
  //   switch (action.type) {
  //     case atRedux.:
  //       return setThemeColor(state, action);
  //     default:
  //       return state;
  //   }
  return state;
}

/* harmony default export */ __webpack_exports__["default"] = (themeColorReducer);

/***/ }),

/***/ "./store/saga/index.js":
/*!*****************************!*\
  !*** ./store/saga/index.js ***!
  \*****************************/
/*! exports provided: watchGetHomeData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "watchGetHomeData", function() { return watchGetHomeData; });
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../actionTypes/saga */ "./store/actionTypes/saga/index.js");
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../actions */ "./store/actions/index.js");
/* harmony import */ var _webServices__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./webServices */ "./store/saga/webServices/index.js");



 // ============================= HOME

function* watchGetHomeData() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"])(_actionTypes_saga__WEBPACK_IMPORTED_MODULE_1__["default"].GET_HOME_SCREEN_DATA, _webServices__WEBPACK_IMPORTED_MODULE_3__["default"].GET.homeData);
} // ============================= HOME

/***/ }),

/***/ "./store/saga/webServices/GET/artists.js":
/*!***********************************************!*\
  !*** ./store/saga/webServices/GET/artists.js ***!
  \***********************************************/
/*! exports provided: artistData, artistSearchData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "artistData", function() { return artistData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "artistSearchData", function() { return artistSearchData; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../actions */ "./store/actions/index.js");
/* harmony import */ var _panelAdmin__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../panelAdmin */ "./panelAdmin/index.js");




function* artistData({
  page
}) {
  try {
    const res = yield _panelAdmin__WEBPACK_IMPORTED_MODULE_3__["default"].api.get.artists({
      page
    });
    console.log({
      resArtistData: res
    });
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setArtistData(res.data));
  } catch (err) {
    console.log({
      errArtistData: err
    });
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(1090));
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(err.response.data.CODE));
  }
}
function* artistSearchData({
  page,
  title
}) {
  try {
    const res = yield _panelAdmin__WEBPACK_IMPORTED_MODULE_3__["default"].api.get.artistSearch({
      page,
      title
    });
    console.log({
      resArtistSearchData: res
    });
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setSearchArtistData(res.data));
  } catch (err) {
    console.log({
      errArtistSearchData: err
    });
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(1090));
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(err.response.data.CODE));
  }
}

/***/ }),

/***/ "./store/saga/webServices/GET/galleryApi.js":
/*!**************************************************!*\
  !*** ./store/saga/webServices/GET/galleryApi.js ***!
  \**************************************************/
/*! exports provided: galleryData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "galleryData", function() { return galleryData; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../actions */ "./store/actions/index.js");



function* galleryData({
  data
}) {
  try {
    const res = yield axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`https://rimtal.com/api/v1/admin/gallery/${data.type}/${data.page} `);
    console.log({
      res
    });
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setGalleryData(res.data)); //********** Level 5 **********//
  } catch (err) {
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(1090));
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(err.response.data.CODE)); //********** Level 6 **********//
  }
}

/***/ }),

/***/ "./store/saga/webServices/GET/homeData.js":
/*!************************************************!*\
  !*** ./store/saga/webServices/GET/homeData.js ***!
  \************************************************/
/*! exports provided: homeData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "homeData", function() { return homeData; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../actions */ "./store/actions/index.js");



function* homeData() {
  console.log("home get khord");

  try {
    const res = yield axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("https://rimtal.com/api/v1/home");
    console.log({
      res
    });
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setHomeData(res.data)); //********** Level 5 **********//
  } catch (err) {
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(1090));
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(err.response.data.CODE)); //********** Level 6 **********//
  }
}

/***/ }),

/***/ "./store/saga/webServices/GET/index.js":
/*!*********************************************!*\
  !*** ./store/saga/webServices/GET/index.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _homeData__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./homeData */ "./store/saga/webServices/GET/homeData.js");
/* harmony import */ var _galleryApi__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./galleryApi */ "./store/saga/webServices/GET/galleryApi.js");
/* harmony import */ var _artists__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./artists */ "./store/saga/webServices/GET/artists.js");



const GET = {
  homeData: _homeData__WEBPACK_IMPORTED_MODULE_0__["homeData"],
  galleryData: _galleryApi__WEBPACK_IMPORTED_MODULE_1__["galleryData"],
  artistData: _artists__WEBPACK_IMPORTED_MODULE_2__["artistData"],
  artistSearchData: _artists__WEBPACK_IMPORTED_MODULE_2__["artistSearchData"]
};
/* harmony default export */ __webpack_exports__["default"] = (GET);

/***/ }),

/***/ "./store/saga/webServices/POST/index.js":
/*!**********************************************!*\
  !*** ./store/saga/webServices/POST/index.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _uploadImage__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./uploadImage */ "./store/saga/webServices/POST/uploadImage.js");

const POST = {
  uploadImage: _uploadImage__WEBPACK_IMPORTED_MODULE_0__["uploadImage"]
};
/* harmony default export */ __webpack_exports__["default"] = (POST);

/***/ }),

/***/ "./store/saga/webServices/POST/uploadImage.js":
/*!****************************************************!*\
  !*** ./store/saga/webServices/POST/uploadImage.js ***!
  \****************************************************/
/*! exports provided: uploadImage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "uploadImage", function() { return uploadImage; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../actions */ "./store/actions/index.js");
/* harmony import */ var _panelAdmin__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../panelAdmin */ "./panelAdmin/index.js");




function* uploadImage(props) {
  console.log("uploaaaaaaaaad omad");
  const {
    imageName,
    type,
    files
  } = props;
  console.log({
    uploadUploadImage: props
  });
  const toastify = _panelAdmin__WEBPACK_IMPORTED_MODULE_3__["default"].utils.toastify;
  const strings = _panelAdmin__WEBPACK_IMPORTED_MODULE_3__["default"].values.strings;
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total); // setState((prev) => ({ ...prev, progressPercentImage: percentCompleted }));
      // yield put(actions.reduxActions.uploadImageProcess());

      console.log(percentCompleted);
    },
    cancelToken: source.token
  };
  const URL = strings.ApiString.IMAGE_UPLOAD;
  const formData = new FormData();
  formData.append("imageName", imageName);
  formData.append("imageType", type);
  formData.append("image", files);

  try {
    const res = yield axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(URL, formData, settings);
    return res; // yield put(actions.reduxActions.setUploadImage(res.data));
  } catch (err) {
    console.log({
      err
    });
    if (!err.response) yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])(_actions__WEBPACK_IMPORTED_MODULE_2__["default"].reduxActions.setFailure(1090)); // yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}

/***/ }),

/***/ "./store/saga/webServices/index.js":
/*!*****************************************!*\
  !*** ./store/saga/webServices/index.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _GET__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./GET */ "./store/saga/webServices/GET/index.js");
/* harmony import */ var _POST__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./POST */ "./store/saga/webServices/POST/index.js");


const webServices = {
  GET: _GET__WEBPACK_IMPORTED_MODULE_0__["default"],
  POST: _POST__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (webServices);

/***/ }),

/***/ "./website/baseComponents/ErrorHandler/index.js":
/*!******************************************************!*\
  !*** ./website/baseComponents/ErrorHandler/index.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const ErrorHandler = () => {
  const error = Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["useSelector"])(state => {
    // console.log({ErrorHandler: state.error});
    return state.error;
  });
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    switch (error.error) {
      case 1098:
        alert("خطایی در سرور رخ داده");
        break;

      case 1090:
        alert("اتصال به اینترنت را بررسی کنید");
        break;
    }
  }, [error]);
  return __jsx(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null);
};

/* harmony default export */ __webpack_exports__["default"] = (ErrorHandler);

/***/ }),

/***/ "./website/components/Header/HamburgerMenu/index.js":
/*!**********************************************************!*\
  !*** ./website/components/Header/HamburgerMenu/index.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/website/components/Header/HamburgerMenu/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
 // import "./index.scss";
// import { useDispatch, useSelector } from "react-redux";
// import * as actions from "../../../../store/actions";

const HamburgerMenu = () => {
  // const dispatch = useDispatch();
  // const user = useSelector((state) => {
  //   console.log({ state });
  //   return state;
  // });
  // console.log({ user });
  // const getUser = () => dispatch(actions.setUserInfo("tooooooooooooooooooken"));
  return __jsx("div", {
    className: "HamburgerMenu",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "border-hamburger-menu",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 7
    }
  }, __jsx("i", {
    // onClick={getUser}
    className: "fas fa-bars",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 9
    }
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (HamburgerMenu);

/***/ }),

/***/ "./website/components/Header/HeaderMiddleElement/index.js":
/*!****************************************************************!*\
  !*** ./website/components/Header/HeaderMiddleElement/index.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/website/components/Header/HeaderMiddleElement/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }



 // import "./index.scss";

const HeaderMiddleElement = ({
  icon,
  text,
  color,
  location
}) => {
  const router = Object(next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"])();
  let locationActive = false;
  if (router.pathname === location.href) locationActive = true;
  return __jsx(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, _extends({}, location, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 5
    }
  }), __jsx("a", {
    className: `${locationActive ? "url-active-in-page" : ""} navbar-link-to-other-page`,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "text-icon-wrapper pointer ",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 9
    }
  }, __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "header-icon-middle",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 13
    }
  }, __jsx("i", {
    style: {
      color
    },
    className: icon,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 15
    }
  })), __jsx("div", {
    className: "header-text-middle ",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 13
    }
  }, text)), __jsx("div", {
    className: "active-page",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 11
    }
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (HeaderMiddleElement);

/***/ }),

/***/ "./website/components/Header/LoginElement/index.js":
/*!*********************************************************!*\
  !*** ./website/components/Header/LoginElement/index.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/website/components/Header/LoginElement/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
 // import "./index.scss";

const LoginElement = ({
  color,
  strings
}) => {
  return __jsx("div", {
    className: "login-element ",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "login-wrapper",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "login-element-container pointer",
    style: {
      backgroundColor: color.mainColor,
      color: color.white
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 9
    }
  }, __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 11
    }
  }, __jsx("i", {
    className: "fas fa-user-alt",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 13
    }
  })), __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 13
    }
  }, strings.SIGN_IN))), __jsx("div", {
    className: "for-small-display login-element-container pointer ",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 9
    }
  }, __jsx("i", {
    className: "far fa-user-circle",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 11
    }
  }), __jsx("div", {
    className: "chevron-down-icon",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 11
    }
  }, " ", __jsx("i", {
    className: "fal fa-chevron-down",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 13
    }
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (LoginElement);

/***/ }),

/***/ "./website/components/Header/index/index.js":
/*!**************************************************!*\
  !*** ./website/components/Header/index/index.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _HamburgerMenu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../HamburgerMenu */ "./website/components/Header/HamburgerMenu/index.js");
/* harmony import */ var _public_assets_images_Rimtal_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../public/assets/images/Rimtal.png */ "./public/assets/images/Rimtal.png");
/* harmony import */ var _public_assets_images_Rimtal_png__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_public_assets_images_Rimtal_png__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _HeaderMiddleElement__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../HeaderMiddleElement */ "./website/components/Header/HeaderMiddleElement/index.js");
/* harmony import */ var _values_strings__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../values/strings */ "./website/values/strings/index.js");
/* harmony import */ var _LoginElement__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../LoginElement */ "./website/components/Header/LoginElement/index.js");
/* harmony import */ var _values_theme_themeColor__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../values/theme/themeColor */ "./website/values/theme/themeColor.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../.. */ "./website/index.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _components_AwesomeScroll__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../components/AwesomeScroll */ "./components/AwesomeScroll/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/website/components/Header/index/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }




 // import consts from "../../../../public/values/consts";


 // import "./index.scss";






const Header = () => {
  return __jsx("header", {
    style: {
      color: _values_theme_themeColor__WEBPACK_IMPORTED_MODULE_6__["default"].textColor
    },
    className: "header-wrapper ",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "header-container  row-main-wrapper",
    onDragStart: e => e.preventDefault(),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "header-side-elements",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 9
    }
  }, __jsx(_HamburgerMenu__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 11
    }
  }), __jsx("div", {
    className: "header-search-icon centerAll pointer for-small-display",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 11
    }
  }, __jsx("i", {
    className: "fal fa-search",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 13
    }
  })), __jsx(next_link__WEBPACK_IMPORTED_MODULE_8___default.a, {
    href: "/",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 11
    }
  }, __jsx("a", {
    className: "logo-website",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 13
    }
  }, __jsx("img", {
    src: _public_assets_images_Rimtal_png__WEBPACK_IMPORTED_MODULE_2___default.a,
    alt: "logo",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 15
    }
  })))), __jsx("div", {
    className: "logo-website for-small-display",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 9
    }
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_8___default.a, {
    href: "/",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 11
    }
  }, __jsx("a", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 13
    }
  }, __jsx("img", {
    src: _public_assets_images_Rimtal_png__WEBPACK_IMPORTED_MODULE_2___default.a,
    alt: "logo",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 15
    }
  })))), __jsx("div", {
    className: "header-middle-elements ",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 9
    }
  }, __jsx(_components_AwesomeScroll__WEBPACK_IMPORTED_MODULE_9__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 11
    }
  }, __jsx("div", {
    onDragStart: e => e.preventDefault(),
    className: "header-middle-into-elemen",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 13
    }
  }, ___WEBPACK_IMPORTED_MODULE_7__["default"].values.consts.headerMiddle.map((middle, index) => {
    return __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40,
        columnNumber: 19
      }
    }, __jsx(_HeaderMiddleElement__WEBPACK_IMPORTED_MODULE_3__["default"], _extends({}, middle, {
      key: "header-" + index,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41,
        columnNumber: 21
      }
    })));
  })))), __jsx("div", {
    className: "header-side-elements flex-end-for-small",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "header-search-icon centerAll pointer",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 11
    }
  }, __jsx("i", {
    className: "fal fa-search",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 13
    }
  })), __jsx("div", {
    className: "header-bell-icon pointer",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 11
    }
  }, __jsx("i", {
    className: "far fa-bell",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 13
    }
  })), __jsx(_LoginElement__WEBPACK_IMPORTED_MODULE_5__["default"], {
    strings: _values_strings__WEBPACK_IMPORTED_MODULE_4__["default"],
    color: _values_theme_themeColor__WEBPACK_IMPORTED_MODULE_6__["default"],
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 11
    }
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./website/index.js":
/*!**************************!*\
  !*** ./website/index.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _values__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./values */ "./website/values/index.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utils */ "./website/utils/index.js");
/* harmony import */ var _storeWebsite_reducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./storeWebsite/reducer */ "./website/storeWebsite/reducer/index.js");



const website = {
  values: _values__WEBPACK_IMPORTED_MODULE_0__["default"],
  utils: _utils__WEBPACK_IMPORTED_MODULE_1__["default"],
  reducer: _storeWebsite_reducer__WEBPACK_IMPORTED_MODULE_2__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (website);

/***/ }),

/***/ "./website/screen/WebsiteScreen.js":
/*!*****************************************!*\
  !*** ./website/screen/WebsiteScreen.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_Header_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/Header/index */ "./website/components/Header/index/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/rimtal-website-main/website/screen/WebsiteScreen.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const WebsiteScreen = props => {
  return __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 5
    }
  }, __jsx(_components_Header_index__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 7
    }
  }), props.children);
};

/* harmony default export */ __webpack_exports__["default"] = (WebsiteScreen);

/***/ }),

/***/ "./website/storeWebsite/actionTypes/redux/index.js":
/*!*********************************************************!*\
  !*** ./website/storeWebsite/actionTypes/redux/index.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const atRedux = {
  //======================================================== Redux
  SET_HOME_DATA: "SET_HOME_DATA_REDUX",
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX",
  SET_FILTER_TYPE: "SET_FILTER_TYPE_REDUX",
  REMOVE_FILTER_TYPE: "REMOVE_FILTER_TYPE_REDUX"
};
/* harmony default export */ __webpack_exports__["default"] = (atRedux);

/***/ }),

/***/ "./website/storeWebsite/reducer/errorReducer.js":
/*!******************************************************!*\
  !*** ./website/storeWebsite/reducer/errorReducer.js ***!
  \******************************************************/
/*! exports provided: errorInitialState, addFailure, removeFailure, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "errorInitialState", function() { return errorInitialState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addFailure", function() { return addFailure; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeFailure", function() { return removeFailure; });
/* harmony import */ var _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actionTypes/redux */ "./website/storeWebsite/actionTypes/redux/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const errorInitialState = {
  error: null
};
const addFailure = (state, action) => {
  return _objectSpread({}, state, {
    error: action.error
  });
};
const removeFailure = state => {
  return _objectSpread({}, state, {
    error: null
  });
};

function errorReducer(state = errorInitialState, action) {
  switch (action.type) {
    case _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].ADD_FAILURE:
      return addFailure(state, action);

    case _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].REMOVE_FAILURE:
      return removeFailure(state);

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (errorReducer);

/***/ }),

/***/ "./website/storeWebsite/reducer/filterReducer.js":
/*!*******************************************************!*\
  !*** ./website/storeWebsite/reducer/filterReducer.js ***!
  \*******************************************************/
/*! exports provided: filterInisialState, setFilter, removeFilter, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "filterInisialState", function() { return filterInisialState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setFilter", function() { return setFilter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeFilter", function() { return removeFilter; });
/* harmony import */ var _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actionTypes/redux */ "./website/storeWebsite/actionTypes/redux/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const filterInisialState = {
  filterData: []
};
const setFilter = (state, action) => {
  if (state.filterData.length > 0) {
    const filter = state.filterData.filter(item => {
      if (item.type !== action.data.type) return item;
    });
    return _objectSpread({}, state, {
      filterData: [...filter, action.data]
    });
  } else {
    return _objectSpread({}, state, {
      filterData: [...state.filterData, action.data]
    });
  }
};
const removeFilter = (state, action) => {
  return _objectSpread({}, state, {
    filterData: []
  });
};

function filterReducer(state = filterInisialState, action) {
  switch (action.type) {
    case _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_FILTER_TYPE:
      return setFilter(state, action);

    case _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].REMOVE_FILTER_TYPE:
      return removeFilter();

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (filterReducer);

/***/ }),

/***/ "./website/storeWebsite/reducer/homeReducer.js":
/*!*****************************************************!*\
  !*** ./website/storeWebsite/reducer/homeReducer.js ***!
  \*****************************************************/
/*! exports provided: homeInitialState, setHomeData, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "homeInitialState", function() { return homeInitialState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setHomeData", function() { return setHomeData; });
/* harmony import */ var _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actionTypes/redux */ "./website/storeWebsite/actionTypes/redux/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const homeInitialState = {
  homeData: []
};
const setHomeData = (state, action) => {
  return _objectSpread({}, state, {
    homeData: action.data
  });
};

function homeReducer(state = homeInitialState, action) {
  switch (action.type) {
    case _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__["default"].SET_HOME_DATA:
      return setHomeData(state, action);

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (homeReducer);

/***/ }),

/***/ "./website/storeWebsite/reducer/index.js":
/*!***********************************************!*\
  !*** ./website/storeWebsite/reducer/index.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _errorReducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./errorReducer */ "./website/storeWebsite/reducer/errorReducer.js");
/* harmony import */ var _homeReducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./homeReducer */ "./website/storeWebsite/reducer/homeReducer.js");
/* harmony import */ var _themeColorReducer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./themeColorReducer */ "./website/storeWebsite/reducer/themeColorReducer.js");
/* harmony import */ var _filterReducer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./filterReducer */ "./website/storeWebsite/reducer/filterReducer.js");





const rootReducer = {
  error: _errorReducer__WEBPACK_IMPORTED_MODULE_1__["default"],
  home: _homeReducer__WEBPACK_IMPORTED_MODULE_2__["default"],
  themeColor: _themeColorReducer__WEBPACK_IMPORTED_MODULE_3__["default"],
  filter: _filterReducer__WEBPACK_IMPORTED_MODULE_4__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (rootReducer);

/***/ }),

/***/ "./website/storeWebsite/reducer/themeColorReducer.js":
/*!***********************************************************!*\
  !*** ./website/storeWebsite/reducer/themeColorReducer.js ***!
  \***********************************************************/
/*! exports provided: webThemeColorInitialstate, setThemeColor, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "webThemeColorInitialstate", function() { return webThemeColorInitialstate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setThemeColor", function() { return setThemeColor; });
/* harmony import */ var _actionTypes_redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actionTypes/redux */ "./website/storeWebsite/actionTypes/redux/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const webThemeColorInitialstate = {
  light: {
    primaryColor: "#0070ef",
    accentColor: "#666666",
    mainColor: "#ffff",
    titleColor: "#333333"
  },
  dark: {
    primaryColor: "#0070ef",
    accentColor: "#666666",
    mainColor: "#000",
    titleColor: "#ffff"
  },
  currentTheme: "light"
};
const setThemeColor = (state, action) => {
  return _objectSpread({}, state, {
    homeData: action.data
  });
};

function themeColorReducer(state = webThemeColorInitialstate, action) {
  //   switch (action.type) {
  //     case atRedux.:
  //       return setThemeColor(state, action);
  //     default:
  //       return state;
  //   }
  return state;
}

/* harmony default export */ __webpack_exports__["default"] = (themeColorReducer);

/***/ }),

/***/ "./website/utils/chunkArray.js":
/*!*************************************!*\
  !*** ./website/utils/chunkArray.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const chunkArray = (myArray, chunkSize) => {
  let results = [];

  while (myArray.length) {
    results.push(myArray.splice(0, chunkSize));
  }

  return results;
};

/* harmony default export */ __webpack_exports__["default"] = (chunkArray);

/***/ }),

/***/ "./website/utils/convert/PlaylistForAutuminCard.js":
/*!*********************************************************!*\
  !*** ./website/utils/convert/PlaylistForAutuminCard.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./website/index.js");


const PlaylistForAutuminCard = (data, direction) => {
  let convertData = []; // console.log({PAPPAAPA:data});

  let noEntries = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.strings.NO_ENTRIED;

  for (const index in data) {
    let title = data[index].title ? data[index].title : noEntries;
    let publisher = data[index].publisher ? data[index].publisher : noEntries;
    let tracksCount = data[index].tracksCount ? data[index].tracksCount : "0";
    let genres = data[index].genres ? data[index].genres : "0";
    convertData.push({
      titleTop: title,
      titleMiddle: publisher,
      titleBottom: [tracksCount + " " + ___WEBPACK_IMPORTED_MODULE_0__["default"].values.strings.TRACKS, genres[0] + " " + ___WEBPACK_IMPORTED_MODULE_0__["default"].values.strings.FOLLOWERS],
      images: data[index].images,
      location: {
        href: "/playlist",
        as: `/playlist#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ __webpack_exports__["default"] = (PlaylistForAutuminCard);

/***/ }),

/***/ "./website/utils/convert/albumCard.js":
/*!********************************************!*\
  !*** ./website/utils/convert/albumCard.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _chunkArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../chunkArray */ "./website/utils/chunkArray.js");


const albumCard = (data, chunkNumber, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (let index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleMiddle: data[index].artist,
        titleBottom: [data[index].releaseDate, data[index].genres[0]],
        images: data[index].images,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    if (chunkNumber) convertData = Object(_chunkArray__WEBPACK_IMPORTED_MODULE_0__["default"])(convertData, chunkNumber); // convertData = chunkArray(convertData, Math.ceil(data.length / 2));

    return convertData;
  }
};

/* harmony default export */ __webpack_exports__["default"] = (albumCard);

/***/ }),

/***/ "./website/utils/convert/comingSoonCard.js":
/*!*************************************************!*\
  !*** ./website/utils/convert/comingSoonCard.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./website/index.js");


const comingSoonCard = (data, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (const index in data) {
      let noEntries = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.strings.NO_ENTRIED;
      let releaseDate = data[index].releaseDate;
      let year = releaseDate.year + "/" + releaseDate.month + "/" + releaseDate.day;
      let title = data[index].title ? data[index].title : noEntries;
      let artist = data[index].artist ? data[index].artist : noEntries;
      let genres = data[index].genres.length ? data[index].genres[0] : noEntries;
      convertData.push({
        titleTop: title,
        titleMiddle: artist,
        titleBottom: [year, genres],
        images: data[index].images,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    return convertData;
  }
};

/* harmony default export */ __webpack_exports__["default"] = (comingSoonCard);

/***/ }),

/***/ "./website/utils/convert/index.js":
/*!****************************************!*\
  !*** ./website/utils/convert/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sliderCard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./sliderCard */ "./website/utils/convert/sliderCard.js");
/* harmony import */ var _albumCard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./albumCard */ "./website/utils/convert/albumCard.js");
/* harmony import */ var _topTracksCard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./topTracksCard */ "./website/utils/convert/topTracksCard.js");
/* harmony import */ var _singleCard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./singleCard */ "./website/utils/convert/singleCard.js");
/* harmony import */ var _comingSoonCard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./comingSoonCard */ "./website/utils/convert/comingSoonCard.js");
/* harmony import */ var _moodCard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./moodCard */ "./website/utils/convert/moodCard.js");
/* harmony import */ var _playlistCard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./playlistCard */ "./website/utils/convert/playlistCard.js");
/* harmony import */ var _videoCard__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./videoCard */ "./website/utils/convert/videoCard.js");
/* harmony import */ var _PlaylistForAutuminCard__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./PlaylistForAutuminCard */ "./website/utils/convert/PlaylistForAutuminCard.js");








 // import moreExploreCard from "./moreExploreCard";

const convert = {
  sliderCard: _sliderCard__WEBPACK_IMPORTED_MODULE_0__["default"],
  albumCard: _albumCard__WEBPACK_IMPORTED_MODULE_1__["default"],
  singleCard: _singleCard__WEBPACK_IMPORTED_MODULE_3__["default"],
  comingSoonCard: _comingSoonCard__WEBPACK_IMPORTED_MODULE_4__["default"],
  moodCard: _moodCard__WEBPACK_IMPORTED_MODULE_5__["default"],
  playlistCard: _playlistCard__WEBPACK_IMPORTED_MODULE_6__["default"],
  videoCard: _videoCard__WEBPACK_IMPORTED_MODULE_7__["default"],
  PlaylistForAutuminCard: _PlaylistForAutuminCard__WEBPACK_IMPORTED_MODULE_8__["default"],
  topTrackCard: _topTracksCard__WEBPACK_IMPORTED_MODULE_2__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (convert);

/***/ }),

/***/ "./website/utils/convert/moodCard.js":
/*!*******************************************!*\
  !*** ./website/utils/convert/moodCard.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const moodCard = (data, direction) => {
  let convertData = []; // console.log({ mood: data });

  let dir = false;
  if (direction === "rtl") dir = true;

  for (const index in data) {
    convertData.push({
      title: data[index].title,
      images: data[index].images,
      location: {
        href: "/slider",
        as: `/slider#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ __webpack_exports__["default"] = (moodCard);

/***/ }),

/***/ "./website/utils/convert/playlistCard.js":
/*!***********************************************!*\
  !*** ./website/utils/convert/playlistCard.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./website/index.js");


const playlistCard = (data, direction) => {
  let convertData = []; // console.log({PAPPAAPA:data});

  let noEntries = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.strings.NO_ENTRIED;

  for (const index in data) {
    let title = data[index].title ? data[index].title : noEntries;
    let publisher = data[index].publisher ? data[index].publisher : noEntries;
    let tracksCount = data[index].tracksCount ? data[index].tracksCount : "0";
    let genres = data[index].genres ? data[index].genres : "0";
    convertData.push({
      titleTop: title,
      titleMiddle: publisher,
      titleBottom: [tracksCount + " " + ___WEBPACK_IMPORTED_MODULE_0__["default"].values.strings.TRACKS, genres[0] + " " + ___WEBPACK_IMPORTED_MODULE_0__["default"].values.strings.FOLLOWERS],
      images: data[index].images,
      location: {
        href: "/playlist",
        as: `/playlist#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ __webpack_exports__["default"] = (playlistCard);

/***/ }),

/***/ "./website/utils/convert/singleCard.js":
/*!*********************************************!*\
  !*** ./website/utils/convert/singleCard.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _chunkArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../chunkArray */ "./website/utils/chunkArray.js");


const singleCard = (data, direction) => {
  if (data) {
    // console.log({ data });
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (const index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleMiddle: data[index].artist,
        titleBottom: [data[index].releaseDate, data[index].genres[0]],
        images: data[index].images,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    convertData = Object(_chunkArray__WEBPACK_IMPORTED_MODULE_0__["default"])(convertData, 2); // convertData = chunkArray(convertData, Math.ceil(data.length / 2));
    // console.log({ convertData });

    return convertData;
  }
};

/* harmony default export */ __webpack_exports__["default"] = (singleCard);

/***/ }),

/***/ "./website/utils/convert/sliderCard.js":
/*!*********************************************!*\
  !*** ./website/utils/convert/sliderCard.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./website/index.js");


const sliderCard = (data, direction) => {
  let convertData = []; // console.log({PAPPAAPA:data});

  for (const index in data) {
    let noEntries = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.strings.NO_ENTRIED;
    let title = data[index].title ? data[index].title : noEntries;
    let parentName = data[index].parentName ? data[index].parentName : noEntries;
    let parentArtist = data[index].parentArtist ? data[index].parentArtist : noEntries;
    convertData.push({
      titleTop: title,
      titleMiddle: parentName,
      titleBottom: parentArtist,
      images: data[index].images,
      location: {
        href: "/slider",
        as: `/slider#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ __webpack_exports__["default"] = (sliderCard);

/***/ }),

/***/ "./website/utils/convert/topTracksCard.js":
/*!************************************************!*\
  !*** ./website/utils/convert/topTracksCard.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _chunkArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../chunkArray */ "./website/utils/chunkArray.js");


const topTracksCard = (data, chunkNumber, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (let index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleBottom: [data[index].artist, ", ", data[index].artist],
        image: data[index].cover,
        location: {
          href: "#",
          as: `#`
        }
      });
    }

    if (chunkNumber) convertData = Object(_chunkArray__WEBPACK_IMPORTED_MODULE_0__["default"])(convertData, chunkNumber); // convertData = chunkArray(convertData, Math.ceil(data.length / 2));

    return convertData;
  }
};

/* harmony default export */ __webpack_exports__["default"] = (topTracksCard);

/***/ }),

/***/ "./website/utils/convert/videoCard.js":
/*!********************************************!*\
  !*** ./website/utils/convert/videoCard.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../.. */ "./website/index.js");


const videoCard = data => {
  let convertData = []; // console.log({ mood: data });

  for (const index in data) {
    let noEntries = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.strings.NO_ENTRIED;
    let title = data[index].title ? data[index].title : noEntries;
    let artist = data[index].artist ? data[index].artist : noEntries;
    let releaseDate = data[index].releaseDate ? data[index].releaseDate : "0";
    let genres = data[index].genres ? data[index].genres : "0";
    convertData.push({
      titleTop: title,
      titleMiddle: artist,
      titleBottom: [releaseDate, genres[0]],
      images: data[index].images,
      location: {
        href: "/video",
        as: `/video#`
      }
    });
  }

  return convertData;
};

/* harmony default export */ __webpack_exports__["default"] = (videoCard);

/***/ }),

/***/ "./website/utils/index.js":
/*!********************************!*\
  !*** ./website/utils/index.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _convert__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./convert */ "./website/utils/convert/index.js");

const utils = {
  convert: _convert__WEBPACK_IMPORTED_MODULE_0__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (utils);

/***/ }),

/***/ "./website/values/consts/constJson.js":
/*!********************************************!*\
  !*** ./website/values/consts/constJson.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const constJson = [{
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI8dfxNQMO5mcaq3AC4IxG5N-u2GQtfT-gS7Va9HT9IICnHwEhTA&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqOcS2_vcejs-tokmulPMRfv88eRcAvGAUHBmT7W-vz0UuYxsVrw&s"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://cdn.wallpapersafari.com/29/68/VAyJbY.jpg"
}, {
  text: "Top New ALBUM",
  subTitle: "Late Night Feeling",
  title: "Mark Ronson",
  image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
}];
/* harmony default export */ __webpack_exports__["default"] = (constJson);

/***/ }),

/***/ "./website/values/consts/constJsonDouble.js":
/*!**************************************************!*\
  !*** ./website/values/consts/constJsonDouble.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const constJsonDouble = [{
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUTEhMVFhUVFxcXFxUXFRYVFxcXFxcXFxUXFxUYHSggGBolHRUVITEhJSkrLi4uFx81ODMtNygtLisBCgoKDg0OFxAQGi0dICUrLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKy0tLS0tLS0tLS0tLS0tLf/AABEIAMIBAwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAAEFBgcDBAj/xABJEAACAQIDBQUECAIHBQkBAAABAgMAEQQSIQUGMUFREyJhcZEHMoGhFCNCUmKx0fBywTNTgpKiwuFDY4OT8RYXJHOjsrPD0xX/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQIDBAX/xAAkEQACAgIDAAEEAwAAAAAAAAAAAQIRAyESMUETBBQyUSJhcf/aAAwDAQACEQMRAD8AgHY3Op4nr1pBz1PrTNxPmacCug4Qgx6n1ow56n1oFowKACDHqfU0QY9T60IFGBSGOCep9aIMep9aYCjAoAQY9TRBj1PrSAogKAECep9ae56n1pAUWWkMFXvpfXzornqfWozbmJVFAHvnVTqLfiNtba/I1ExbwyAd4i4uNLcR1Fjp46fCk2UoNlqBPU+tcMfKVika57qMePgag4N7kuBJGydSO8Pkb/KvVtfakb4WXIwuy5RYk8SBzA8eNFj4tFp9kmOzQhb+9Gh484SYW+QiPxrQVNYR7NttjDSBXvbPcWv7rrlkBJ05Iw/hrXo95MOzBFlRmJAABHPr00BPp1rzM0amz0ce4k5mNPc9a5g3ojWVlni23h5JMNNHEQJHidELEgBmUqCSL2te9ZG/s4xcSZey7T8SyKWv4Xtp4VtBa1NmrXHmePoyyY1M+bNpbr46FrNh58v3gjsPVb145FtoxytaxDM6nl1UWr6gU1zmgV1IdVYE8GUMONudbL6v9oh4H+z5pTBkoWDA5GUACUG+bMxvfj7tdYYZ42AjlljLR9oWV2AACNIR3GF9ANfGvoDFbp4CUkPhIbaE2QLrrY922vH1qHm9nmAJDKkiM6mMlJG0TJlsA1wO6AK0+4i+0T8UkZJht9Npworri5CMzJlktJ7qoSe8Dp3xzq07O9sbBVE+GzGwu8clrnrkZdPK9TWO9lMEi9mmJlURs1iyo984Um9svCwHwqG/7oZUHdxMbnlmRl/mam8M+yryRRPbJ9qWDnkSLLOjuyqoZQRmYgDVWPM9Ku5Y/s1k+xvZzi4cdh5nVGjSRWdlccFuQbHXjataK1zZoQi/4muOTa2AxNC1+tdGFMwrGiwE4U1dEXSlQFmQMNT5n86cCnYanzP509q908YdRRBaQFdAKQDAUQFOBRqKBjAUYFJRRgUDGAowKa1FSAYCuWJkygkECwvc6ADmTXPG4+OEZpDYdbacbcaou3d5ZJrqvdj1sBoxHK55UmzSMWzybTxZZ7qzMdSSbnTy5DjpXlGfmH425gX8+tcIiR3gSPEafCpbC7WZkETG1rhWBI4/ZYDRh000rNtnQkiOkUi+oPiDfx+FdYnciwN78r3ufLiDSxKsrEHW3O1cTGdDlPna3zpAEC3Dn0vU5u9jUjkHbRqy3Gjq5UEkC4KHSwubm/Co6ePMobQW0J5npm/K9FH7ttT+E/5T+/jSltDWnZ9AbsYvDvf6LPnjFs0RYsY25ZSdQOII1FxpU/evn7dfbsmEYN2QZAbZrWy66DPlNutvGtW2BvimKB7MAFdCruFbwIHTx0rhnBx/w6IyUiyy3oVNNHJm/Pr6HnXSMa1lYw1olHu+f6mgJ0rsF1UdAf5CihjH7R/fD/Why6qOg/QUZ90+Jt6m1I+8x6D9TT8A4wjQnqzH52FPaniFlHkPnrXS1UloTZxIoCK7EUDCk0FgWpmWuhFMakLEi6UqQlUaFgD0JApqoVmQMNT5mnApNxPnRAV7J5AgK6AUwFGBQMQWjUUlFGBQAgKILTgUVIoYm1Q+19oWjYrowNiLXIKkMNByIv8ACpY4hLEE8tb6fnVF3jxvfKi+ZdA9+KG5Ct1tfQ0rLjEiNoYt5DqW46LckD4VwfBNoPtHl++dS6bNKZHkuCfs81FtP7R105DjTr3pwwFltlAHTl8f1rFzOlQo8EWBJUgDgLk+P7/KvIcMeJrTN3NjRlGJVXYnS/uKNNT94+FebamwCSQBwBNx1J5j48P0rD7hWdH27qygWbTXU6cflXrIKizXuDbXl4a8ONe3aGzDGx8OP76muCxtLlQk2HA6m3QE1ryTVmXBp0c8N3ja3/Tp4cqGeNraFsvi3ytfSrCuz8sZIFgBlv1IvcanSoWddMvIcel/PlxtURyKT0aTxOK2eM4xiDe5Btm10vwzW5Gu2zSqyrnJCn7Wvyym/EAUo0yhlYWOh8COh+Vc8ehFuNrX05edaaejCq2a1udtOWPEjDs/aRSAlDnD5CADbNp1tYi/ia0VUr502FtN1kja7WHA3UZSRYHMRwva4Nb1sbHAxCR5FK5ST7wyZQC2YubjjfXhbpXHOHFmylaJIiuwHePgB8yf0rGN8Pa27O0eAsiD/bst2bxRDoo8Tc+VUmLeDGTzR9pi8Q2d1B+te1i1j3QbDS9VHBJ7YfIkfTltF8Tf5XoJuD+n8q+cdm74Y+GOSWPFS/0iBQ7GRRmEjEZXuOCAfGtO3e9ogkkOFxahHVY2aYECO5EZcMD7nfew5eVEsLSGppmgk8qe1CK6EVAAGhIroRQGkMC1RO8G2kwyHgXylgCbKqjjJIfsoPnwFcN5N448Mr95cyi7u3uRA8M1tWY8kGprBt7N6XxTMqlhGWzHMe/Kw4NIR05INF5da0x4uWyJTromsbvsrSMRAs1yfrZHdWfqco0UdByFhSqlwjQUq6uETGzUmGp86ICkRqfP+dEBXScQ6ijAplFdAKB0OoolFICjFIBAUQFOBTgUFUQO8yIy2JyuPcP536iqVGbydQuvW4HAk8emtXzei4gJyhrEEg6gqD3h6XPwqg7MhLElb2sT45V1P5AVnM2xkx2jSsAx4XJ1vqTYADmbDX4U8EYDDlpqSfy8fGoqDEtwvwJ4G2vnzqY2UoLasVA5gXPpXNNNI7MbTZYtn4goFHDoP1FTcb5xwGoOvH46XHrUDIq5hYlh5EC19AL61O4F0tbmOQvl8unKuGR6MYkRjNmPKRHrkFr2AvwIHer1bN3VsLgfZ4+J/KpGWYrfLqTbT5D871O7IwgdFDAmwuVJbLc8AVva3h4U+UnoXBJ8im7dwGWOKCLUk2aTldtDlbnz1rybxbvDCwRrmHaSnNlA91Rc8eXL1rRsbswGRXfgvK3T8gKpm0pVnxLSufqoxZb89baDncggdbGhNrQOKlsoeKwJjytJYZuvK518tOVefa+HKItwAyqnC/Frn8hUztnaInm1UkKwPIC+oy/sVz2WwxWLKy+4SRYdFBCgetdcZOrZwzgrqJ5tywjv2UiB1DBrcTZtNANTY8vKrDvKpw+CxEUU1w7Il9RZNZMgJNwTlKm/3QKqJQ4fFfVkjKRbip68v2bVeJNmDaOAm7ASGWPvXJHeyWJUgm5bKTbjrSn+al4RH8WvTIq9+xzaUMR7qu392NiPnarBs3dQte4J0HIjU/CtR3c3HiGFivGCXQ3JHEMT/KtnlXSI+KVWzEYo2aKKMf7WY+oEar/72r34yUE4+S9wziMeTTdoPlBVkxm74TEQkKQsbvJblYSyPb+6i1UZ4CmFsR3nnNz/AOXGP5zmqjJS6JcXHs2b2M7wticK0MjZnw5CgniY2ByX8rMPICtEFZH7AcC4TFTEWRjHGviVzM3pmX1rXAK5siqTo1j0Mar+923VwsRObKxUsW49mg0L25kkhVHNiKsJrE/bRj2zMlz35Qp/hhjQhfi0pbzApQjykKTpFD3k3gfFPzWJSSkd76ni7n7Uh5sfLhUPEhYhQLkkADqToBTVfdyN3SlsRKLMR9Wp5A/bPj0/6V2peGEnSJ/Y278MUKI6Kzgd4/iJJPzNqVSycKVaUjn5SIsjU+ZowKTDU+ZohVGY6iui0KiugFIpDgUQFIUQoGPaipKKcCgCsb9YgpCACO8bEWJJ9OH+tVfCYvIl107QGPrZbC48+fxq078QRmMF1ObgrcuttOB/Ws/RsrZPHS3U25VnNWbY3SPZhAOut+Hh1qf2ZAxYBbj98yah9mwlpcqqTcC1vHn5aVqGw9hroZHVV01JA/u9T41z5X4dmBeg7H2E0pHdY/eYkKunlxq14rAxxRhbagacNfLp5n516cLtCJAI4ihA07p187866rhDKbyHh8/0rk+M7vk98IPZOzu0ckjQai3C/nVuwuC7NeH75UEcKrqLAAcKafbUIsC4q4xUeyJzc+jx7Zw6MjdwMSNdSF+J4W9apaYcODlUE8SxFtRwCg6KANB/rV6faMDK13vcHj5cq8GJSIxdwroPDlzPQ1Eop7TKjKlTRjG8adiWBVhrcdB4i2h8zUHsbEkShr8xr5G4qzb+2DXGvhfTlVQwisxLHhbXkPAACuvErhs4szrJok8Ue1kzA6s2nrx/OtL9k2EX6VPIAxyrlDDhZjcg+N+XnwrKsIxuABcXGnh+9a2z2RwFIZWJuXcEnxUWNx6fvjGVUkhRd2zntXa+y1lkCYmKKQMQ6OHRS6Eq2ViuUajlpVt2XtvCNGoixMD5EA7sqNYheGh46GsTwG0ZMRKUg2pKGdmYRyQM0a5mJsGzOAovbgOWmtqlNtbLxMKWeXZs7ZyG7aKGLXKuQBsi66vrm10tWiwRjtekPPJqn4WyWDML5b9y3Ak6rbTT8VQg9nsuKVEb6qNXdndh3+8w0VepCrqeFxx4VDZp4+3ePCuO8FVsFipCWGe+aySSKpCoOCjjatG9neMlljlMrTnKYltOQXDGJZH72UEnvqDccVrH4ZY1aZrLMsmqJ/Y+yosLCkEC5Y0FgOZ5kk8yTqTXsAogKcCpJOZFZD7bNlkqZAODJJ8GHZP/AIhF61sVqgN7cIGjV7XyMA2l+6/dvbwbIfhVY3UiZ9GKbpbo2tNiV14pEfkzj/L61c2NE1+B4jSgNegkcEpNnRDpSpR8KVAiPPE+ZogKTDU+Z/OiUUxBCjAoVowKBhAUYFCKMUDCFPakKIUDPFtXCpLGUdSw/D7w8RzvWV4/DBZHtfT3c9wza2uP0rYSL1RN89jpGhdc2p93iAeJJvyqWVB7KthsY5AsbZb6g2NvPnUlFNKwKx52spYm7BdBc89TVg9lexUnL51vY21HxrUMHusYTeEgKfeUgEG3A6jjqawc0nVHZHG2rujEcNi8VFdwr2QZmIzd0XsCb308a1L2abytjPqmPeUXv1FXHDbGRFKhI1U6kKigE/AV4th7PhjxbGMDNlNzp9or0/hFRNxfhrjjOPbs9m8WHdI+6SSeQ51nEuwMbLKq2KZtc7BmsPLh8PDlWw4i2YXp3w+YaEjyNqlR2W5vjRiEewNpLP2KnXORcppkv7x0tbLrfNbW3GrJs7ZGPRrOiMo0LIwsPNT+YrRU2a3ORiOhtXqSAKLD51U6fSoiDce3Zi2826plkFhxuWFuGnG3wqlbX2aMNLlvYjRtdL+FfRe0411OUE29axnezAGfGEJzsbHlew/SsotxdPo0nFTVpbKsQEVW6sNL20A1s1ud63bdyIwbOZ2Iv2TyGx7ijISoQW0WwB0HMnnWP7R2QXxS4VLERWVyTZSzHveNgMvxJrXtro2H2TMtu0KQZAtj3r2RUsupGoGmtObtxRjVJlL3Vw+KTvzT4SaMAIRaINYAllLSRKQdF0J5V7toxSlosPJg45o2YPJLHmsgVsyqFje5IsLmwBPLjaP2VjoJUlk+jWdMskqZnGZjrlCSI1gSGU25W11qxbZeOSIstwVjMkVo0ktmUhSLlWOgawuL2tXWzjTKJvVsXC4VEVosQhmlveORW76L3bdooNj2raX0txrWNwUH0VmDMweeaxYWNkfsgALnT6vTrx0rMtl7OxROESHGsVt2xVzLGXiMi3sCCraWFr/a8a1rc6JlwOGzkFzErMRaxZxnYi2mpY1lmejXES9qe1PalXObAkV5dpQdpDIn3kYfG2nztXrtQyaAnoDQIy3EHW/3grfFlBPzJrga9E49z+BfnqPkRXEivQj0jz5dhR8KVEi6UqoR4SNT5miApm4nzohQIMUYFCtGoplDiugoBRikAQFEKaiFAx7VwxuEWWNo2GjAjxB6ivQKIUDKxuNIcJjJYGOpysDwDAj9+lbHg5swF7Vhu+kjQ4qGdemXlyN/1rQd2d5RIgPUfsVyZlTs9D6eSlGvS64h7KT0FQG6OHYs0jixJtboOOvjrTbc2p2eGllH2UYjzsbVQNzvapHGHXEqQSxKsovcHkehHzrNJvaNm1HTNixg9afBYjiDxFZhtT2sKZQIoHkTS7AEG/S1qv8AsctJCsrAo0gzFTxW/AHxtahppiVOJN9pXCaSo44ll0avFi8ceA/etHIFAPaWK0NqyHb+1Po2LaW2ZhcKp0F+APkLVo+NxJtpwFibdTyrIN8ZzJiyoBIGugv8bDxvSgrkGSXGOjvurhsViMSHiuZM+dnI7gN7jOeFvDjpWte0V2XZjDtFiZ3hXtLuqqc4Y2ZQWHukDSvB7NcIohXKVJ4mxBN/EcRXT2xPGMLBHIJCsk40iy5+6jkWzAg620oi+WTowlqBAbn4PHRNIJsUsquoyg4nMQRmawEwBViFta1tdfCzAYgSMWQNGRdcsSO4Ol1LAWCj3ha5tw4VRH2/hJcsMwxCHgqtCq2a9kbuOCALE8Na6bJx8ky5UxOHlZjljJSWFgQrsScyFb3KHoBeuuSZyo9u0N4UV58N9HAJw5kQqMrCRzl0LZgmjKLganiNNNfw0IjRUHBFVR5KAB+VZBhosW+0GTtu0gMuHj0kSUEdpG0pKXJTRHHAca2Q1hlXRti6GNI0qasjQVRm8WK7OBre8/cW/VtGJ6ALc17cXikiUvIwVRz/ACAHM+ArMd5d5PpGLXDLo1iXXj2MI1Ksf62Q5Qw+yptxOlQjyZMpUh52BYkcNAP4QAF+QFcSK7OKAiu5I4HsJBpTUUY0pU6CzwnifOiApm4nzNEKBBLRChFGopjQQoxQiiFIYQo1oKKgYQp70IpwaAK9v7hO0wjMOMZD/AcfkTVf3G2iyyBb6HUDl5VoLqCCDqCCCPOsq2phHwM+XXJmzI1jqOg8eVZzjaNsUuLL1tLfaMg4fEIyKwIvbunl6VTX2LgHl7mMCIeN1Jt5HTSrvh8Rhto4fLKAWHA6Ai/Cx5VHYbYuPwr/APh4Enj4r3QGHhrXNCl/TO+uTXLaLDu2dm4VcsUxnPG0UbSnxuFHGp6PeeIkLFFimbhY4eVRwtqWAA9ar2Bn23IcownZA8ywC+gNW3ZuyJYgHxEhdrXIuAo62AFZySWzf+NVFjbNfFO318QjGpALKxtyvbgfia6YjDAtc8tL17J8aNL+YPDS3yqs7e3hWEEn4G/HXlUqn0Zu/Tz7w7TTDwk8zwB5nUWrENq4h+3Y3IZTa40II8fWrnHjGx+MQH3UOY+QuRf41G787tSxH6SoLRSM1yBfs31zA2+zoSDXRjSXZz5ZcuujlszfnGRWuyyAcpUDN/f9751Z/wDt3LiYpZGgQth0FgWd0BldVLAFwyWCkXU373KsvU/sVKbF2xJhw/Zsoz2DBkjcEDwdTprWsYqznk9FzwO98EzAYvCKcisVkRmJjAUsbBybcOR4mpTAbc2ZE8H0aKSNXZiBlzKTYRsHJdWyki9vw1UcLtwFZC+FwjWTiIezY5mRLExMunePpXtjxOEzRn6MylMO8l452soZZGtllV7nvjW/EjpWjRBN+z6KH6Yk4xCylppJTZGia6QyCwR9NDiVOhPEVsK7YjP2Zf8AlOfyFfMu3xAsUAhMhB7R8siqD3mCe8psdYugqHjmI4Fh5EisZw5M1jKkfWMm2EH2JT/wmHzawqu7a3/w8AN5IkI5PIJH/wCVCWPqRXzc0pI1JPmSfzrmTUrEh8maDvX7TJJiRh86nUds9g4H+6jHdi/i1bxFF7M8H3Jp2uS7ZATxIHeY353JHpWdohYgAXJIAHUnQCtq2Fs/6Ph44uar3v4ySzfMmtYJGWR6PawoGFdDQGtTnY6cKVFGNKVAjwHifM060zcT50QpjCFGKFaMUDCFPTU4pAEDT0N6V6Bh3pXoL0E0qqpZiFUcWJAA+JoA6568G24IJYiuIKhPvMQuU9QTzqs7a35RLrh1zn77XCDyHFvlVI2ptWbENeVy1uA4Kvko0FS2i4wZK7A2uMNMQGzx5iAeFxfRrHhy9a2Ddze9HsMwsLD1H/X0rAEr0Q4l11ViPIkcOFc88fLZ148vHT2j6Wk3ijDFS3AXseNeXEbwLfvN3e9x6af618/Da8/324W48qCTGSMLMxI6XrN4JP02X1K/RfN5t9C75YfcUnXraqljtoy4h7XJJ0AF7V59n7PeVgqj9K03c/c5Y7O4u3lw8q0UY40YucsjC3I3eMEWZh3m1P6Vc4MMPohuNRLGR59tHb87fGvSYAq2r0dn3I0+9NF8cjiU/KM1Ftuy+lRgu/eyUw2PxECKBGGDIOiuqvYdACxA8qjdlbXmwylYpmRSxJW+hPDVToeHSrZ7YkttR/GKE/Ij+VVjZ23MTEoSKd1XU5A111OvcNx8q7Y7SOKXbPcN45OykMkeHkIZAM+Hi1Bzk3KgE+4vOvVPtCC85fCIMkaRns5JIybmNCLMWUWseA4CrFuzh/puFvI0Xbdsf6XCRMjxKApKdkFYsC471+ZHjVbx+0MOwnLYVTmnCho3liLW7Rr5XzAH3dLfa8KQFe3meLPGIw6hYkBDsGILDP7wAv7/AEFQ5NTG8UatiZclwFbJYkMe53OIA07ulPs7Y8MtgcVHGekiMP8AF7p9ahxZaaSIS9FGhYgKCSdAALknoBzrQMB7P4Tq2JMg/wB2FA/vXarPsvYmHw39FGA33z3nP9o6+lHETmkV7czdMwkTzj6z7Cfc/E34vDl58LjTGmq0qMW2xzTUjTGmI6oNKemQ6UqQjwEanzNOKZuJ86cVQghRigFEDQUHSoQae9ABUqjtq7Zhww+sbvHgi6sfG3IeJsKpu1d7p5brH9UngbuR4vy+HrQNKy2ba3hhw1wTnk5Rrxv+I/ZHz8KoG2drTYk3kOn2UGir8OZ8TXjA/f8AOnAootaPI8dcJI7VIOtcpI6mUTRM8aGuq1zkS1HBqQOFRRQYqZ2Fsd8QwsO7zNS+xN0DIAx4Vf8AY2yxEAAo9BUSnXRUY8g93N2VjUWX41dMNhgo4cK82BQgCveVJFY9m/R5pBmYV0U5sUicoYzI38UpMcf+FZvUV6cLh+Zqgbwb/R4SGVoiHxeKcsi3zCGIARwM/Q5FDhOrm+lXGNmcpFK9rGMWXac2U3EapGT4ot29CxHwqFw23p0RUzIyKNFeGGQaDh3kJ+dRTMTdmJJa5JJuSTqSTzJNTH/9ePg+Dw7WAF17WNviUcD5V1pUjkk7ZLYPeZlOHUwQgAPKOzEkOU3kDMqxOFBKx2Omt64YWXCyfR80MiZ5XkOWbOL3jVi2dSx9w8+vWpbF7EjUZ2wsgRMNmMkOKSRFVkN1s63zDOTa+vGoxIcMMpjmcdnA7hZIhoGEjXLox1u45dKkGVeUhmZhezMSLgA2J5gG16ApTqdKerGNA7RnNGzI3VSVPyq0bH3zdbLiRnX+sAGceYGjfI+dVi9MVpUJ7Ndw+ISRQ6MGU8GGoNdL1mWwNtPhX5tGfeS/+JejD51o8UyuqupurAEHqDwpENUdb0jQXpwaCTtHwp6aMaU9AzwtxPmacUzDU+dOBTEEKKhpUAEKqu8G9oS8eH1YaGTQqD+EfaPjw868u923iWMELWAuJGHEnmoPIDn6VUlWg0jH0KR2ZizMWZjckm5J8SeNOKa1EKZQ9KlSoENT20pGlQM4tHXEw17rUNqVD5ElsDenFYTRCHT7kgzD4Hiv5eFXbZ/tSh07bCOD1jdWHo2W3rWcWprVDxxZSyNGwx+1jAAf0OKH9iL/APSuWK9sWGA+qwk7H8bRoP8ACWrJLUqPiiHysue3fanj8SpjjCYdGBB7O7SEHQjtG4fAA1SUitRkimzCrUUiG2wkYXW+ouLgGxIuLgG2htUzIMAb2bFR3NjdIZhf4FLjSojB4ftJEQMi5j7ztlUWBOp5cKncNu1OWTL2Ugzgns54X0uOQa/XlQSya2ntJJYsRGuIw+U9nHaSCWHJkuoHcDIx+rJDcio5aVD7Q2W6DEOGjYJFHH3JEY6GJWut8w0VjqOdcpdhYgL9ZDKrSzC90awGupNrWvKfSvDjpLpK39ZiOPgvaEj/ANRPlU0VdkeKc01PViFSFMaEUAItVv3H2pxw7Hq0f+df5+tUy9ejBYkxusi8UII+HEfEXHxpA0azenBrlFIGAYcGAI8iLj86O9IyPVG2lKhiJtSoGeVuJ86cUjxPmaQpiFXk2rjOxhkk+6pt/EdF+ZFes1VN/sXZI4QdXOdvJdAPU3/s0DStlMF+J1PM9SeNPTUiaDU6CnNcYW1I+IruaYMYU9MKRNAhU4FNenoGFTU6mhoEI0jSp6AGBpWpjSBoAWShKCulMKBigUBwegJ+VWXdDCJJiY1kAZcrue+EICgm4PM6DT9Khdl4x4ZC8ZF8tu8quCCfuuCOVTMe3rh+0gw72j/qhGbsVW14iunePpR4Q9smtu7PXAthfo0sxibtJe1EgMbqBmsCti1gijhbU6m+lV2ptGaSCBZZC+rv3rFuIQd46/ZbnzqTbF4Y+9h2TJh+Mcx0Eg4BZA2v1x586idtiIGIRZ7CJdHAuMxLjVePvdBUop9kbSvSNKqAY01O1A1IYJ404NIimFAzRt0sRnwqdULJ6HT5EVMA1Vtwpfq5V6Orf3lt/lq0XpGMtM9EZ0pUMfClQAZUXNOFHSmpUAIqOlUDfgD6V/w1/NqalTLh2QYUdBSyjoKVKgoB1GZdBXpyi3ClSpIb8BCjoKcqOgpqVMTHyjoKdlHQUqVMBKo6CllHQUqVIXoig6CnCjoKVKmN9A5R0FNlHQUqVAMIKOgpFR0FKlQIUajvaDgPzr2YdBlk0HBOX4hT0qQju6i8+n+yh/8AprzbWUdrwH9HF/8AGtKlQM8eUdBSKjoKalTGM6joKHKOlKlSGDkFhoKQUdBSpUDfRatwx3pvJPzareFF6VKgwl2dFFNSpUhH/9k="
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRF9IIwP_vI4I30S2hhzXBtsMQQ5IDg4mUlCJG5YADjyk1iZtzD&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ2lWkUYUjSisqChVAJ2uaMwWIVb2aSP55jx-MTTxL2OIHY1TWX&usqp=CAU"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQNTGU7NdDxgOUwnkGQ8kd34i6wi4II1pChL_fnD4zsxj1aE-aT&usqp=CAU"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}, {
  data: [{
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://ccra.scdn.co/services-marketplace/_next/static/images/socialcard-fe20c182cde6b4edff212b909bfc6778.jpg"
  }, {
    text: "Top New ALBUM",
    subTitle: "Late Night Feeling",
    title: "Mark Ronson",
    image: "https://lh6.googleusercontent.com/proxy/7Xwk3SO_zBGoFnUg6H8jfu1Dmt5yhEwaakpvMajBTxwVZP-_JcMO42geZEJ5w7hI5ZyQeaJnW6CkvNXYznCIpyoHYJx0qIsFISjoIq3YuBCbba02HhH4AUDLe1NEH2380kOpCnzXPEzLJezgbZGQnm1p6frrQaGU3O1kYLKU5g"
  }]
}];
/* harmony default export */ __webpack_exports__["default"] = (constJsonDouble);

/***/ }),

/***/ "./website/values/consts/headerMiddle.js":
/*!***********************************************!*\
  !*** ./website/values/consts/headerMiddle.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _strings__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../strings */ "./website/values/strings/index.js");
/* harmony import */ var _theme_themeColor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../theme/themeColor */ "./website/values/theme/themeColor.js");


let color = _theme_themeColor__WEBPACK_IMPORTED_MODULE_1__["default"].mainColor;
const headerMiddle = [{
  text: _strings__WEBPACK_IMPORTED_MODULE_0__["default"].EXPLORE,
  icon: "far fa-compass",
  location: {
    href: "#"
  },
  color
}, {
  text: _strings__WEBPACK_IMPORTED_MODULE_0__["default"].TRACKS,
  icon: "far fa-music",
  location: {
    href: "#"
  },
  color
}, {
  text: _strings__WEBPACK_IMPORTED_MODULE_0__["default"].PLAYLISTS,
  icon: "far fa-list-music",
  location: {
    href: "#"
  },
  color
}, {
  text: _strings__WEBPACK_IMPORTED_MODULE_0__["default"].ALBUMS,
  icon: "far fa-album",
  location: {
    href: "/albums"
  },
  color
}, {
  text: _strings__WEBPACK_IMPORTED_MODULE_0__["default"].ARTISTS,
  icon: "far fa-user-music",
  location: {
    href: "#"
  },
  color
}, {
  text: _strings__WEBPACK_IMPORTED_MODULE_0__["default"].VIDEOS,
  icon: "far fa-video",
  location: {
    href: "#"
  },
  color
}];
/* harmony default export */ __webpack_exports__["default"] = (headerMiddle);

/***/ }),

/***/ "./website/values/consts/index.js":
/*!****************************************!*\
  !*** ./website/values/consts/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _headerMiddle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./headerMiddle */ "./website/values/consts/headerMiddle.js");
/* harmony import */ var _constJson__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./constJson */ "./website/values/consts/constJson.js");
/* harmony import */ var _constJsonDouble__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./constJsonDouble */ "./website/values/consts/constJsonDouble.js");
/* harmony import */ var _moreExploreConst__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./moreExploreConst */ "./website/values/consts/moreExploreConst.js");
/* harmony import */ var _instrumentals__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./instrumentals */ "./website/values/consts/instrumentals.js");





const consts = {
  headerMiddle: _headerMiddle__WEBPACK_IMPORTED_MODULE_0__["default"],
  constJson: _constJson__WEBPACK_IMPORTED_MODULE_1__["default"],
  constJsonDouble: _constJsonDouble__WEBPACK_IMPORTED_MODULE_2__["default"],
  moreExploreConst: _moreExploreConst__WEBPACK_IMPORTED_MODULE_3__["default"],
  instrumentals: _instrumentals__WEBPACK_IMPORTED_MODULE_4__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (consts);

/***/ }),

/***/ "./website/values/consts/instrumentals.js":
/*!************************************************!*\
  !*** ./website/values/consts/instrumentals.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const instrumentals = [{
  titleFa: "پیانو",
  titleEn: "Piano",
  icon: "far fa-piano"
}, {
  titleFa: "چمپن استیک",
  titleEn: "Chapman Stick",
  icon: "far fa-guitar-electric"
}, {
  titleFa: "گیتار",
  titleEn: "Guitar",
  icon: "fal fa-guitar"
}, {
  titleFa: "ساکسیفون",
  titleEn: "Saxophone",
  icon: "fal fa-saxophone"
}, {
  titleFa: "پیانو",
  titleEn: "Piano",
  icon: "far fa-piano"
}, {
  titleFa: "چمپن استیک",
  titleEn: "Chapman Stick",
  icon: "far fa-guitar-electric"
}, {
  titleFa: "گیتار",
  titleEn: "Guitar",
  icon: "fal fa-guitar"
}, {
  titleFa: "ساکسیفون",
  titleEn: "Saxophone",
  icon: "fal fa-saxophone"
}, {
  titleFa: "پیانو",
  titleEn: "Piano",
  icon: "far fa-piano"
}, {
  titleFa: "چمپن استیک",
  titleEn: "Chapman Stick",
  icon: "far fa-guitar-electric"
}, {
  titleFa: "گیتار",
  titleEn: "Guitar",
  icon: "fal fa-guitar"
}, {
  titleFa: "ساکسیفون",
  titleEn: "Saxophone",
  icon: "fal fa-saxophone"
}];
/* harmony default export */ __webpack_exports__["default"] = (instrumentals);

/***/ }),

/***/ "./website/values/consts/moreExploreConst.js":
/*!***************************************************!*\
  !*** ./website/values/consts/moreExploreConst.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const moreExploreConst = [{
  title: "Geners"
}, {
  title: "Instruments"
}, {
  title: "Videos"
}, {
  title: "Artists"
}, {
  title: "PlayList"
}, {
  title: "example"
}, {
  title: "example2"
}, {
  title: "example3"
}, {
  title: "example4"
}, {
  title: "example5"
}, {
  title: "example6"
}];
/* harmony default export */ __webpack_exports__["default"] = (moreExploreConst);

/***/ }),

/***/ "./website/values/index.js":
/*!*********************************!*\
  !*** ./website/values/index.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _consts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./consts */ "./website/values/consts/index.js");
/* harmony import */ var _strings__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./strings */ "./website/values/strings/index.js");
/* harmony import */ var _theme_themeColor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./theme/themeColor */ "./website/values/theme/themeColor.js");



const values = {
  consts: _consts__WEBPACK_IMPORTED_MODULE_0__["default"],
  strings: _strings__WEBPACK_IMPORTED_MODULE_1__["default"],
  themeColor: _theme_themeColor__WEBPACK_IMPORTED_MODULE_2__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (values);

/***/ }),

/***/ "./website/values/strings/en/global.js":
/*!*********************************************!*\
  !*** ./website/values/strings/en/global.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const TRACKS = "Tracks";
const NO_ENTRIED = "no entried";
const FOLLOWERS = "followers";
const global = {
  TRACKS,
  NO_ENTRIED,
  FOLLOWERS
};
/* harmony default export */ __webpack_exports__["default"] = (global);

/***/ }),

/***/ "./website/values/strings/en/homePage.js":
/*!***********************************************!*\
  !*** ./website/values/strings/en/homePage.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const VIEW_ALL = "View All";
const SINGLES = "Singles";
const COMING_SOON = "Coming Soon";
const PLAY_LISTS = "Playlists";
const VIDEOS = "Videos";
const PLAYLIST_FOR_AUTUMN = "Playlist For Autumn";
const MOOD = "Mood";
const COMMING_SOON = "Comming Soon";
const homePage = {
  MOOD,
  SINGLES,
  VIEW_ALL,
  COMING_SOON,
  PLAY_LISTS,
  VIDEOS,
  MOOD,
  PLAYLIST_FOR_AUTUMN,
  COMMING_SOON
};
/* harmony default export */ __webpack_exports__["default"] = (homePage);

/***/ }),

/***/ "./website/values/strings/en/index.js":
/*!********************************************!*\
  !*** ./website/values/strings/en/index.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navbar_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navbar.js */ "./website/values/strings/en/navbar.js");
/* harmony import */ var _homePage_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./homePage.js */ "./website/values/strings/en/homePage.js");
/* harmony import */ var _global_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./global.js */ "./website/values/strings/en/global.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const en = _objectSpread({}, _navbar_js__WEBPACK_IMPORTED_MODULE_0__["default"], {}, _homePage_js__WEBPACK_IMPORTED_MODULE_1__["default"], {}, _global_js__WEBPACK_IMPORTED_MODULE_2__["default"]);

/* harmony default export */ __webpack_exports__["default"] = (en);

/***/ }),

/***/ "./website/values/strings/en/navbar.js":
/*!*********************************************!*\
  !*** ./website/values/strings/en/navbar.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const APP_NAME = "Rimtal";
const EXPLORE = "Explore";
const TRACKS = "Tracks";
const PLAYLISTS = "Playlists";
const ALBUMS = "Albums";
const ARTISTS = "Artists";
const VIDEOS = "Videos";
const SIGN_IN = "Sign In";
const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN
};
/* harmony default export */ __webpack_exports__["default"] = (navbar);

/***/ }),

/***/ "./website/values/strings/fa/global.js":
/*!*********************************************!*\
  !*** ./website/values/strings/fa/global.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const TRACKS = "آهنگ ها ";
const NO_ENTRIED = "وارد نشده";
const FOLLOWERS = "دنبال کنندگان";
const global = {
  TRACKS,
  NO_ENTRIED,
  FOLLOWERS
};
/* harmony default export */ __webpack_exports__["default"] = (global);

/***/ }),

/***/ "./website/values/strings/fa/homePage.js":
/*!***********************************************!*\
  !*** ./website/values/strings/fa/homePage.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const VIEW_ALL = "مشاهده همه";
const SINGLES = "تک آهنگ ها";
const COMING_SOON = "بزودی";
const PLAY_LISTS = "لیست پخش";
const VIDEOS = "ویدیو ها";
const PLAYLIST_FOR_AUTUMN = "لیست پخش پیشنهادی ";
const MOOD = "حالت";
const COMMING_SOON = "بزودی";
const homePage = {
  MOOD,
  SINGLES,
  VIEW_ALL,
  SINGLES,
  COMING_SOON,
  PLAY_LISTS,
  VIDEOS,
  PLAYLIST_FOR_AUTUMN,
  COMMING_SOON
};
/* harmony default export */ __webpack_exports__["default"] = (homePage);

/***/ }),

/***/ "./website/values/strings/fa/index.js":
/*!********************************************!*\
  !*** ./website/values/strings/fa/index.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navbar_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navbar.js */ "./website/values/strings/fa/navbar.js");
/* harmony import */ var _homePage_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./homePage.js */ "./website/values/strings/fa/homePage.js");
/* harmony import */ var _global_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./global.js */ "./website/values/strings/fa/global.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const fa = _objectSpread({}, _navbar_js__WEBPACK_IMPORTED_MODULE_0__["default"], {}, _homePage_js__WEBPACK_IMPORTED_MODULE_1__["default"], {}, _global_js__WEBPACK_IMPORTED_MODULE_2__["default"]);

/* harmony default export */ __webpack_exports__["default"] = (fa);

/***/ }),

/***/ "./website/values/strings/fa/navbar.js":
/*!*********************************************!*\
  !*** ./website/values/strings/fa/navbar.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const APP_NAME = "ریمتال";
const EXPLORE = "کاوش کردن";
const TRACKS = "آهنگ ها";
const PLAYLISTS = "لیست های پخش";
const ALBUMS = "آلبوم ها";
const ARTISTS = "هنرمندان";
const VIDEOS = "ویدیو ها";
const SIGN_IN = "ورود";
const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN
};
/* harmony default export */ __webpack_exports__["default"] = (navbar);

/***/ }),

/***/ "./website/values/strings/index.js":
/*!*****************************************!*\
  !*** ./website/values/strings/index.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _en__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./en */ "./website/values/strings/en/index.js");
/* harmony import */ var _fa__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fa */ "./website/values/strings/fa/index.js");


const Lang = "ltr";
let string;
if (Lang === "rtl") string = _fa__WEBPACK_IMPORTED_MODULE_1__["default"];else string = _en__WEBPACK_IMPORTED_MODULE_0__["default"];
/* harmony default export */ __webpack_exports__["default"] = (string);

/***/ }),

/***/ "./website/values/theme/themeColor.js":
/*!********************************************!*\
  !*** ./website/values/theme/themeColor.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  mainColor: "#0070ef",
  accentColor: "#666666",
  white: "#ffff",
  black: "#000",
  textColor: "#666666"
});

/***/ }),

/***/ 0:
/*!****************************************!*\
  !*** multi private-next-pages/_app.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! private-next-pages/_app.js */"./pages/_app.js");


/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "next-redux-wrapper":
/*!*************************************!*\
  !*** external "next-redux-wrapper" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-redux-wrapper");

/***/ }),

/***/ "next-useragent":
/*!*********************************!*\
  !*** external "next-useragent" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-useragent");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "prop-types-exact":
/*!***********************************!*\
  !*** external "prop-types-exact" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types-exact");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-dom":
/*!****************************!*\
  !*** external "react-dom" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-dom");

/***/ }),

/***/ "react-is":
/*!***************************!*\
  !*** external "react-is" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-is");

/***/ }),

/***/ "react-perfect-scrollbar":
/*!******************************************!*\
  !*** external "react-perfect-scrollbar" ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-perfect-scrollbar");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "react-scrollbars-custom":
/*!******************************************!*\
  !*** external "react-scrollbars-custom" ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-scrollbars-custom");

/***/ }),

/***/ "react-toastify":
/*!*********************************!*\
  !*** external "react-toastify" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-toastify");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),

/***/ "redux-devtools-extension":
/*!*******************************************!*\
  !*** external "redux-devtools-extension" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-devtools-extension");

/***/ }),

/***/ "redux-saga":
/*!*****************************!*\
  !*** external "redux-saga" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-saga");

/***/ }),

/***/ "redux-saga/effects":
/*!*************************************!*\
  !*** external "redux-saga/effects" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-saga/effects");

/***/ }),

/***/ "url":
/*!**********************!*\
  !*** external "url" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("url");

/***/ })

/******/ });
//# sourceMappingURL=_app.js.map