import * as reduxActions from "./redux";
import * as sagaActions from "./saga";
import panelAdmin from "../../panelAdmin";

const actions = {
  reduxActions,
  sagaActions,
};

export default actions;
