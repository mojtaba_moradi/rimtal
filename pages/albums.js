import React, { Fragment } from "react";
import AlbumScreen from "../website/screen/AlbumScreen";

const Albums = () => {
  return (
    <div className="row-container padding-top">
      <AlbumScreen />
    </div>
  );
};

export default Albums;
