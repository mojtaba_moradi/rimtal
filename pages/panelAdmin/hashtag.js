import React, { useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";
import HashtagScreen from "../../panelAdmin/screen/Hashtag/HashtagScreen";
import { connect } from "react-redux";

const hashtag = (props) => {
  const sagaActions = panelAdmin.actions.sagaActions;
  const reduxActions = panelAdmin.actions.reduxActions;
  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return state;
  });
  console.log({ store });

  useEffect(() => {
    props.dispatch(sagaActions.getHashtagData({ page: 1 }));
    props.dispatch(reduxActions.setPageName(panelAdmin.values.strings.HASHTAG));
  }, []);
  const onDataChange = ({ page = 1 }) => {
    props.dispatch(sagaActions.getHashtagData({ page }));
  };
  const onDataSearch = ({ title, page = 1 }) => {
    props.dispatch(sagaActions.getSearchHashtagData({ title, page }));
  };
  return <HashtagScreen onDataChange={onDataChange} onDataSearch={onDataSearch} />;
};
hashtag.getInitialProps = async (props) => {
  const { store, isServer } = props.ctx;
  // store.dispatch(sagaActions.getHomeData());
  return { isServer };
};
export default connect()(hashtag);
