import React, { useEffect } from "react";
import GalleryScreen from "../../panelAdmin/screen/Gallery/GalleryScreen";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";
import { connect } from "react-redux";

const gallery = (props) => {
  const { acceptedCardInfo, parentTrue } = props;
  const sagaActions = panelAdmin.actions.sagaActions;
  const reduxActions = panelAdmin.actions.reduxActions;
  const filters = panelAdmin.utils.consts.galleryConstants();
  console.log({ filters });

  const store = useSelector((state) => {
    return state;
  });

  useEffect(() => {
    props.dispatch(sagaActions.getGalleryData({ type: filters[0].value, page: 1 }));
    if (!parentTrue) props.dispatch(reduxActions.setPageName(panelAdmin.values.strings.GALLERY));
  }, []);

  const onDataChange = (filterName, page = 1) => {
    props.dispatch(sagaActions.getGalleryData({ type: filterName, page }));
  };

  return <GalleryScreen acceptedCardInfo={acceptedCardInfo} filters={filters} onDataChange={onDataChange} />;
};
gallery.getInitialProps = async (props) => {
  const { store, isServer } = props.ctx;
  // store.dispatch(sagaActions.getHomeData());
  return { isServer };
};
export default connect()(gallery);
