import React, { useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";
import MoodScreen from "../../panelAdmin/screen/Mood/MoodScreen";
import { connect } from "react-redux";

const mood = (props) => {
  const sagaActions = panelAdmin.actions.sagaActions;
  const reduxActions = panelAdmin.actions.reduxActions;
  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return state;
  });
  console.log({ store });

  useEffect(() => {
    props.dispatch(sagaActions.getMoodData({ page: 1 }));
    props.dispatch(reduxActions.setPageName(panelAdmin.values.strings.MOOD));
  }, []);
  const onDataChange = ({ page = 1 }) => {
    props.dispatch(sagaActions.getMoodData({ page }));
  };
  const onDataSearch = ({ title, page = 1 }) => {
    props.dispatch(sagaActions.getSearchMoodData({ title, page }));
  };
  return <MoodScreen onDataChange={onDataChange} onDataSearch={onDataSearch} />;
};
mood.getInitialProps = async (props) => {
  const { store, isServer } = props.ctx;
  // store.dispatch(sagaActions.getHomeData());
  return { isServer };
};
export default connect()(mood);
