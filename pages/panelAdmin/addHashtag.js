import React, { useEffect } from "react";
import AddHashtag from "../../panelAdmin/screen/Hashtag/AddHashtag";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";
const addHashtag = () => {
  const dispatch = useDispatch();
  const reduxActions = panelAdmin.actions.reduxActions;
  const sagaActions = panelAdmin.actions.sagaActions;
  useEffect(() => {
    dispatch(reduxActions.setPageName(panelAdmin.values.strings.ADD_HASHTAG));
  }, []);
  return <AddHashtag />;
};

export default addHashtag;
