import React, { useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";
import CountryScreen from "../../panelAdmin/screen/Country/CountryScreen";
import { connect } from "react-redux";

const country = (props) => {
  const sagaActions = panelAdmin.actions.sagaActions;
  const reduxActions = panelAdmin.actions.reduxActions;
  const store = useSelector((state) => {
    return state;
  });
  console.log({ store });

  useEffect(() => {
    props.dispatch(sagaActions.getCountryData({ page: 1 }));
    props.dispatch(reduxActions.setPageName(panelAdmin.values.strings.COUNTRY));
  }, []);
  const onDataChange = ({ page = 1 }) => {
    props.dispatch(sagaActions.getCountryData({ page }));
  };
  const onDataSearch = ({ title, page = 1 }) => {
    props.dispatch(sagaActions.getSearchCountryData({ title, page }));
  };
  return <CountryScreen onDataChange={onDataChange} onDataSearch={onDataSearch} />;
};
country.getInitialProps = async (props) => {
  const { store, isServer } = props.ctx;
  // store.dispatch(sagaActions.getHomeData());
  return { isServer };
};

export default connect()(country);
