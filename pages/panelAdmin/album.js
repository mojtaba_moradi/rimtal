import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";
import AlbumScreen from "../../panelAdmin/screen/Album/AlbumScreen";
import { connect } from "react-redux";
const album = (props) => {
  const { acceptedCardInfo, parentTrue } = props;
  const sagaActions = panelAdmin.actions.sagaActions;
  const reduxActions = panelAdmin.actions.reduxActions;

  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return state;
  });
  console.log(store);

  useEffect(() => {
    props.dispatch(sagaActions.getAlbumData({ page: 1 }));
    if (!parentTrue) props.dispatch(reduxActions.setPageName(panelAdmin.values.strings.ALBUM));
  }, []);

  const onDataChange = (filterName, page = 1) => {
    props.dispatch(sagaActions.getGalleryData({ type: filterName, page }));
  };

  return <AlbumScreen acceptedCardInfo={acceptedCardInfo} onDataChange={onDataChange} />;
};
album.getInitialProps = async (props) => {
  const { store, isServer } = props.ctx;
  // store.dispatch(sagaActions.getHomeData());
  return { isServer };
};

export default connect()(album);
