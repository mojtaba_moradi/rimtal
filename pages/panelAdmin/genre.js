import React, { useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import panelAdmin from "../../panelAdmin";
import GenresScreen from "../../panelAdmin/screen/Genres/GenresScreen";
import { connect } from "react-redux";

const genre = (props) => {
  const sagaActions = panelAdmin.actions.sagaActions;
  const reduxActions = panelAdmin.actions.reduxActions;
  const store = useSelector((state) => {
    return state;
  });
  console.log({ store });

  useEffect(() => {
    props.dispatch(sagaActions.getGenreData({ page: 1 }));
    props.dispatch(reduxActions.setPageName(panelAdmin.values.strings.GENRES));
  }, []);
  const onDataChange = ({ page = 1 }) => {
    props.dispatch(sagaActions.getGenreData({ page }));
  };
  const onDataSearch = ({ title, page = 1 }) => {
    props.dispatch(sagaActions.getSearchGenreData({ title, page }));
  };
  return <GenresScreen onDataChange={onDataChange} onDataSearch={onDataSearch} />;
};
genre.getInitialProps = async (props) => {
  const { store, isServer } = props.ctx;
  // store.dispatch(sagaActions.getHomeData());
  return { isServer };
};
export default connect()(genre);
